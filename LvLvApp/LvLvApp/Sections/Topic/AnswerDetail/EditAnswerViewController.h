//
//  EditAnswerViewController.h
//  LvLvApp
//
//  Created by jim on 15/12/29.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^BackBlock)(void);
typedef void (^SaveBlock)(NSString *string);
@interface EditAnswerViewController : BaseViewController
@property (nonatomic,copy) NSString * UserID;
@property (nonatomic,copy) NSString * TID;//问题编号
@property (nonatomic,copy) NSString *answerID;
@property (nonatomic,strong) UITextView * textView;
@property (nonatomic,copy) NSMutableString *content;
@property (nonatomic,copy) NSAttributedString *texts;
@property (nonatomic,strong) NSMutableArray *imageList;
@property (nonatomic,strong) BackBlock backBlock;
@property (nonatomic,strong) SaveBlock draftSaveBlock;
@end
