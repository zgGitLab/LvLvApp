//
//  S_MyFansBaseClass.m
//
//  Created by IOS8  on 15/12/17
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_MyFansBaseClass.h"
#import "S_MyFansData.h"


NSString *const kS_MyFansBaseClassCode = @"Code";
NSString *const kS_MyFansBaseClassMessage = @"Message";
NSString *const kS_MyFansBaseClassData = @"Data";


@interface S_MyFansBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_MyFansBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_MyFansBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_MyFansBaseClassMessage fromDictionary:dict];
    NSObject *receivedS_MyFansData = [dict objectForKey:kS_MyFansBaseClassData];
    NSMutableArray *parsedS_MyFansData = [NSMutableArray array];
    if ([receivedS_MyFansData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_MyFansData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_MyFansData addObject:[S_MyFansData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_MyFansData isKindOfClass:[NSDictionary class]]) {
       [parsedS_MyFansData addObject:[S_MyFansData modelObjectWithDictionary:(NSDictionary *)receivedS_MyFansData]];
    }

    self.data = [NSArray arrayWithArray:parsedS_MyFansData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_MyFansBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_MyFansBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kS_MyFansBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_MyFansBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_MyFansBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_MyFansBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_MyFansBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_MyFansBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_MyFansBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_MyFansBaseClass *copy = [[S_MyFansBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
