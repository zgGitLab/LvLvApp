//
//  S_PerHotData.h
//
//  Created by IOS8  on 15/12/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_PerHotData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *tID;
@property (nonatomic, assign) double userid;
@property (nonatomic, strong) NSString *dataDescription;
@property (nonatomic, strong) id timagesUrl;
@property (nonatomic, strong) NSString *logoUrl;
@property (nonatomic, strong) NSString *questionContent;
@property (nonatomic, assign) double collectCount;
@property (nonatomic, strong) NSString *sortTime;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, assign) double counts;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *answerContent;
@property (nonatomic, assign) double commentCount;
@property (nonatomic, strong) NSString *times;
@property (nonatomic, assign) double types;
@property (nonatomic, strong) NSString *answerID;
@property (nonatomic, strong) NSString *tUserID;
@property (nonatomic, strong) NSString *aUserID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
