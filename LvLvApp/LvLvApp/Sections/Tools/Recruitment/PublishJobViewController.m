//
//  PublishJobViewController.m
//  LvLvApp
//
//  Created by Sunny on 16/3/30.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "PublishJobViewController.h"
#import <MessageUI/MessageUI.h>
@interface PublishJobViewController ()<MFMailComposeViewControllerDelegate>

@end

@implementation PublishJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发布职位";
}
- (IBAction)sendEmail:(UIButton *)sender {
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto://1@qq.com"]];
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (!mailClass) {
        //        [self alertWithMessage:@"当前系统版本不支持应用内发送邮件功能，您可以使用mailto方法代替"];
        [MBProgressHUD showOlnyMessage:@"当前系统版本不支持应用内发送邮件功能，您可以使用mailto方法代替" toView:self.view];
        return;
    }
    if (![mailClass canSendMail]) {
        [MBProgressHUD showOlnyMessage:@"您没有设置邮件账户" toView:self.view];
        return;
    }

    [self displayMFMailComposeViewController];
   
    
}

- (void)displayMFMailComposeViewController
{
    MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
    mailPicker.mailComposeDelegate = self;
    [mailPicker setSubject: @"招聘信息"];
    NSArray *toRecipients = @[@"hr@lvlvlaw.com"];
    [mailPicker setToRecipients: toRecipients];
    [mailPicker setMessageBody:@"长按添加更多信息" isHTML:YES];
    [self presentViewController:mailPicker animated:YES completion:nil];
}

#pragma mark MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (error) {
        NSLog(@"error :%@",error);
    }
    if (result == MFMailComposeResultSent) {
        
    }
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"取消发送！");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"发送失败！");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"保存发送！");
            break;
        case MFMailComposeResultSent:
            NSLog(@"发送成功!");
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
