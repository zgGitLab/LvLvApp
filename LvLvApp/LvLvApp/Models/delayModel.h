//
//  delayModel.h
//  Tools
//
//  Created by IOS8 on 15/9/15.
//  Copyright (c) 2015年 IOS8. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface delayModel : NSObject

@property (nonatomic, assign) double soInterest;        // 一般利息
@property (nonatomic, assign) double delayInterest;     // 延时利息
@property (nonatomic, assign) double countInterest;     // 总利息
@property (nonatomic, assign) double sum;               // 本利和
@property (nonatomic, assign) int soInterestDays;       // 一般利息天数
@property (nonatomic, assign) int delayInterestDays;    // 延时利息天数
@property (nonatomic, assign) double money;             // 欠款金额
@property (nonatomic, assign) double liLv;              // 利率
@property (nonatomic, assign) double contractDate;      // 履行期限
@property (nonatomic, copy) NSString *finshDate;         // 清偿日期
@property (nonatomic, copy) NSString *startDate;         // 计息起始日
@property (nonatomic, copy) NSString *effectDate;        //  判决生效日期

- (void)computeResult;
@end
