//
//  UserViews.m
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "UserViews.h"
#import "UserSTableViewCell.h"
#import "USERBaseClass.h"
#import "USERData.h"
#import "USERDataModels.h"
@implementation UserViews

- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0.5, [UIScreen mainScreen].bounds.size.width,  [UIScreen mainScreen].bounds.size.height);
        self.backgroundColor = [UIColor whiteColor];
        
    }
    return self;
}
- (id)initWithString:(NSString*)str {
    self = [self init];
    self.userarrayss =[[NSMutableArray alloc]init];
    [self userTableView];
    [self.userarrayss removeAllObjects];
    
    [self request:str];
    self.usertableView .mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.userarrayss removeAllObjects];
        [self request:str];
        [self layoutSubviews];
        
    }];
    self.usertableView.mj_footer =[MJRefreshAutoFooter footerWithRefreshingBlock:^{
        [self.userarrayss removeAllObjects];
        [self request:str];
        [self layoutSubviews];
    }];
    
    
    return self;
}
- (void)getusrSearText:(NSString *)str
{
    NSLog(@"%@",str);
    self.searString =[NSString stringWithFormat:@"%@",str];
    NSLog(@"%@",self.searString);
    [self request:str];
    [self.userarrayss removeAllObjects];
    
    [self.usertableView reloadData];
    
}
-(void)request:(NSString *)TopicNames
{
    NSInteger pageIndex =1;
    NSString *TopicName =TopicNames;
    NSLog(@"%@",TopicName);
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:[NSString stringWithFormat:@"%d",(int)pageIndex] forKey:@"PageIndex"];
    [params setValue:[NSNumber numberWithInteger:K_PAGESIZE] forKey:@"Pagesize"];
    [params setValue:TopicName forKey:@"FuzzyStr"];
    [LVHTTPRequestTool post:[NSString stringWithFormat:kseacrUesrUrl,K_IP] params:params success:^(id json) {
        NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"dict %@",dict);
        NSString *info =[[NSString alloc]initWithData:json encoding:NSUTF8StringEncoding];
        NSLog(@"`````````%@````",info);
        USERBaseClass *usedata=[[USERBaseClass alloc]initWithDictionary:dict];
        [self.userarrayss addObjectsFromArray:usedata.data];
        [self.usertableView reloadData];
        if (self.userarrays) {
            self.userarrays(self.userarrayss);
        }
        [self.usertableView.mj_header endRefreshing];
        [self.usertableView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        [self.usertableView.mj_header endRefreshing];
        [self.usertableView.mj_footer endRefreshing];
    }];
    
}

- (void)userTableView
{
    self.usertableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-100)style:UITableViewStylePlain];
    self.usertableView.backgroundColor =[UIColor whiteColor];
    [self.usertableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    self.usertableView.delegate = self;
    self.usertableView.dataSource = self;
    [self addSubview:self.usertableView];
    [self setExtraCellLineHidden:self.usertableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.userarrayss.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier =@"userID";
    
    
    UserSTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"UserSTableViewCell" owner:self options:nil][0];
    }
    self.userData =self.userarrayss[indexPath.row];
    NSLog(@"%.f",self.userData.iDProperty);
    NSString *url = [NSString stringWithFormat:@"http://%@/%@",K_IP,self.userData.image];
    [cell.HeadPortrait sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"头像"]];
    
    cell.UserNames.text =self.userData.nickName;
    cell.UserIdograph.text =self.userData.introduction;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 76;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserSTableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    cell.selected=NO;
    if (_UserDelegate) {
        [_UserDelegate Userclick:indexPath.row];
    }
    
}
-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [UIView new];
    
    view.backgroundColor = [UIColor clearColor];
    
    [tableView setTableFooterView:view];
}
@end
