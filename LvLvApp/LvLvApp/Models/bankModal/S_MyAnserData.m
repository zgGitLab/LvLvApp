//
//  S_MyAnserData.m
//
//  Created by IOS8  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_MyAnserData.h"


NSString *const kS_MyAnserDataTID = @"TID";
NSString *const kS_MyAnserDataUserid = @"Userid";
NSString *const kS_MyAnserDataDescription = @"Description";
NSString *const kS_MyAnserDataTimagesUrl = @"TimagesUrl";
NSString *const kS_MyAnserDataLogoUrl = @"LogoUrl";
NSString *const kS_MyAnserDataQuestionContent = @"QuestionContent";
NSString *const kS_MyAnserDataCollectCount = @"collectCount";
NSString *const kS_MyAnserDataSortTime = @"sortTime";
NSString *const kS_MyAnserDataNickname = @"Nickname";
NSString *const kS_MyAnserDataCounts = @"counts";
NSString *const kS_MyAnserDataTitle = @"Title";
NSString *const kS_MyAnserDataAnswerContent = @"AnswerContent";
NSString *const kS_MyAnserDataCommentCount = @"commentCount";
NSString *const kS_MyAnserDataTimes = @"times";
NSString *const kS_MyAnserDataTypes = @"types";
NSString *const kS_MyAnserDataAnswerID = @"Answer_ID";
NSString *const kS_MyAnserDataAUserID = @"AUserID";
NSString *const kS_MyAnserDataTUserID = @"TUserID";


@interface S_MyAnserData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_MyAnserData

@synthesize tID = _tID;
@synthesize userid = _userid;
@synthesize dataDescription = _dataDescription;
@synthesize timagesUrl = _timagesUrl;
@synthesize logoUrl = _logoUrl;
@synthesize questionContent = _questionContent;
@synthesize collectCount = _collectCount;
@synthesize sortTime = _sortTime;
@synthesize nickname = _nickname;
@synthesize counts = _counts;
@synthesize title = _title;
@synthesize answerContent = _answerContent;
@synthesize commentCount = _commentCount;
@synthesize times = _times;
@synthesize types = _types;
@synthesize answerID = _answerID;
@synthesize AUserID = _AUserID;
@synthesize TUserID = _TUserID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tID = [self objectOrNilForKey:kS_MyAnserDataTID fromDictionary:dict];
            self.userid = [[self objectOrNilForKey:kS_MyAnserDataUserid fromDictionary:dict] doubleValue];
            self.dataDescription = [self objectOrNilForKey:kS_MyAnserDataDescription fromDictionary:dict];
            self.timagesUrl = [self objectOrNilForKey:kS_MyAnserDataTimagesUrl fromDictionary:dict];
            self.logoUrl = [self objectOrNilForKey:kS_MyAnserDataLogoUrl fromDictionary:dict];
            self.questionContent = [self objectOrNilForKey:kS_MyAnserDataQuestionContent fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kS_MyAnserDataCollectCount fromDictionary:dict] doubleValue];
            self.sortTime = [self objectOrNilForKey:kS_MyAnserDataSortTime fromDictionary:dict];
            self.nickname = [self objectOrNilForKey:kS_MyAnserDataNickname fromDictionary:dict];
            self.counts = [[self objectOrNilForKey:kS_MyAnserDataCounts fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kS_MyAnserDataTitle fromDictionary:dict];
            self.answerContent = [self objectOrNilForKey:kS_MyAnserDataAnswerContent fromDictionary:dict];
            self.commentCount = [[self objectOrNilForKey:kS_MyAnserDataCommentCount fromDictionary:dict] doubleValue];
            self.times = [self objectOrNilForKey:kS_MyAnserDataTimes fromDictionary:dict];
            self.types = [[self objectOrNilForKey:kS_MyAnserDataTypes fromDictionary:dict] doubleValue];
            self.answerID = [self objectOrNilForKey:kS_MyAnserDataAnswerID fromDictionary:dict];
            self.AUserID = [self objectOrNilForKey:kS_MyAnserDataAUserID fromDictionary:dict];
            self.TUserID = [self objectOrNilForKey:kS_MyAnserDataTUserID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tID forKey:kS_MyAnserDataTID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userid] forKey:kS_MyAnserDataUserid];
    [mutableDict setValue:self.dataDescription forKey:kS_MyAnserDataDescription];
    [mutableDict setValue:self.timagesUrl forKey:kS_MyAnserDataTimagesUrl];
    [mutableDict setValue:self.logoUrl forKey:kS_MyAnserDataLogoUrl];
    [mutableDict setValue:self.questionContent forKey:kS_MyAnserDataQuestionContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kS_MyAnserDataCollectCount];
    [mutableDict setValue:self.sortTime forKey:kS_MyAnserDataSortTime];
    [mutableDict setValue:self.nickname forKey:kS_MyAnserDataNickname];
    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kS_MyAnserDataCounts];
    [mutableDict setValue:self.title forKey:kS_MyAnserDataTitle];
    [mutableDict setValue:self.answerContent forKey:kS_MyAnserDataAnswerContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kS_MyAnserDataCommentCount];
    [mutableDict setValue:self.times forKey:kS_MyAnserDataTimes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.types] forKey:kS_MyAnserDataTypes];
    [mutableDict setValue:self.answerID forKey:kS_MyAnserDataAnswerID];
    [mutableDict setValue:self.AUserID forKey:kS_MyAnserDataAUserID];
    [mutableDict setValue:self.TUserID forKey:kS_MyAnserDataTUserID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tID = [aDecoder decodeObjectForKey:kS_MyAnserDataTID];
    self.userid = [aDecoder decodeDoubleForKey:kS_MyAnserDataUserid];
    self.dataDescription = [aDecoder decodeObjectForKey:kS_MyAnserDataDescription];
    self.timagesUrl = [aDecoder decodeObjectForKey:kS_MyAnserDataTimagesUrl];
    self.logoUrl = [aDecoder decodeObjectForKey:kS_MyAnserDataLogoUrl];
    self.questionContent = [aDecoder decodeObjectForKey:kS_MyAnserDataQuestionContent];
    self.collectCount = [aDecoder decodeDoubleForKey:kS_MyAnserDataCollectCount];
    self.sortTime = [aDecoder decodeObjectForKey:kS_MyAnserDataSortTime];
    self.nickname = [aDecoder decodeObjectForKey:kS_MyAnserDataNickname];
    self.counts = [aDecoder decodeDoubleForKey:kS_MyAnserDataCounts];
    self.title = [aDecoder decodeObjectForKey:kS_MyAnserDataTitle];
    self.answerContent = [aDecoder decodeObjectForKey:kS_MyAnserDataAnswerContent];
    self.commentCount = [aDecoder decodeDoubleForKey:kS_MyAnserDataCommentCount];
    self.times = [aDecoder decodeObjectForKey:kS_MyAnserDataTimes];
    self.types = [aDecoder decodeDoubleForKey:kS_MyAnserDataTypes];
    self.answerID = [aDecoder decodeObjectForKey:kS_MyAnserDataAnswerID];
    self.AUserID = [aDecoder decodeObjectForKey:kS_MyAnserDataAUserID];
    self.TUserID = [aDecoder decodeObjectForKey:kS_MyAnserDataTUserID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tID forKey:kS_MyAnserDataTID];
    [aCoder encodeDouble:_userid forKey:kS_MyAnserDataUserid];
    [aCoder encodeObject:_dataDescription forKey:kS_MyAnserDataDescription];
    [aCoder encodeObject:_timagesUrl forKey:kS_MyAnserDataTimagesUrl];
    [aCoder encodeObject:_logoUrl forKey:kS_MyAnserDataLogoUrl];
    [aCoder encodeObject:_questionContent forKey:kS_MyAnserDataQuestionContent];
    [aCoder encodeDouble:_collectCount forKey:kS_MyAnserDataCollectCount];
    [aCoder encodeObject:_sortTime forKey:kS_MyAnserDataSortTime];
    [aCoder encodeObject:_nickname forKey:kS_MyAnserDataNickname];
    [aCoder encodeDouble:_counts forKey:kS_MyAnserDataCounts];
    [aCoder encodeObject:_title forKey:kS_MyAnserDataTitle];
    [aCoder encodeObject:_answerContent forKey:kS_MyAnserDataAnswerContent];
    [aCoder encodeDouble:_commentCount forKey:kS_MyAnserDataCommentCount];
    [aCoder encodeObject:_times forKey:kS_MyAnserDataTimes];
    [aCoder encodeDouble:_types forKey:kS_MyAnserDataTypes];
    [aCoder encodeObject:_answerID forKey:kS_MyAnserDataAnswerID];
    [aCoder encodeObject:_AUserID forKey:kS_MyAnserDataAUserID];
    [aCoder encodeObject:_TUserID forKey:kS_MyAnserDataTUserID];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_MyAnserData *copy = [[S_MyAnserData alloc] init];
    
    if (copy) {

        copy.tID = [self.tID copyWithZone:zone];
        copy.userid = self.userid;
        copy.dataDescription = [self.dataDescription copyWithZone:zone];
        copy.timagesUrl = [self.timagesUrl copyWithZone:zone];
        copy.logoUrl = [self.logoUrl copyWithZone:zone];
        copy.questionContent = [self.questionContent copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.sortTime = [self.sortTime copyWithZone:zone];
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.counts = self.counts;
        copy.title = [self.title copyWithZone:zone];
        copy.answerContent = [self.answerContent copyWithZone:zone];
        copy.commentCount = self.commentCount;
        copy.times = [self.times copyWithZone:zone];
        copy.types = self.types;
        copy.answerID = [self.answerID copyWithZone:zone];
        copy.AUserID = [self.AUserID copyWithZone:zone];
        copy.TUserID = [self.TUserID copyWithZone:zone];
    }
    
    return copy;
}


@end
