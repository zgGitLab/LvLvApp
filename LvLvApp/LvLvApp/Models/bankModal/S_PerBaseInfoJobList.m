//
//  S_PerBaseInfoJobList.m
//
//  Created by IOS8  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_PerBaseInfoJobList.h"


NSString *const kS_PerBaseInfoJobListUserID = @"UserID";
NSString *const kS_PerBaseInfoJobListLacation = @"Lacation";
NSString *const kS_PerBaseInfoJobListEndYear = @"EndYear";
NSString *const kS_PerBaseInfoJobListBeginYear = @"BeginYear";
NSString *const kS_PerBaseInfoJobListPosition = @"Position";
NSString *const kS_PerBaseInfoJobListDepartment = @"Department";
NSString *const kS_PerBaseInfoJobListID = @"ID";
NSString *const kS_PerBaseInfoJobListCompany = @"Company";


@interface S_PerBaseInfoJobList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_PerBaseInfoJobList

@synthesize userID = _userID;
@synthesize lacation = _lacation;
@synthesize endYear = _endYear;
@synthesize beginYear = _beginYear;
@synthesize position = _position;
@synthesize department = _department;
@synthesize iDProperty = _iDProperty;
@synthesize company = _company;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userID = [[self objectOrNilForKey:kS_PerBaseInfoJobListUserID fromDictionary:dict] doubleValue];
            self.lacation = [self objectOrNilForKey:kS_PerBaseInfoJobListLacation fromDictionary:dict];
            self.endYear = [self objectOrNilForKey:kS_PerBaseInfoJobListEndYear fromDictionary:dict];
            self.beginYear = [self objectOrNilForKey:kS_PerBaseInfoJobListBeginYear fromDictionary:dict];
            self.position = [self objectOrNilForKey:kS_PerBaseInfoJobListPosition fromDictionary:dict];
            self.department = [self objectOrNilForKey:kS_PerBaseInfoJobListDepartment fromDictionary:dict];
            self.iDProperty = [self objectOrNilForKey:kS_PerBaseInfoJobListID fromDictionary:dict];
            self.company = [self objectOrNilForKey:kS_PerBaseInfoJobListCompany fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kS_PerBaseInfoJobListUserID];
    [mutableDict setValue:self.lacation forKey:kS_PerBaseInfoJobListLacation];
    [mutableDict setValue:self.endYear forKey:kS_PerBaseInfoJobListEndYear];
    [mutableDict setValue:self.beginYear forKey:kS_PerBaseInfoJobListBeginYear];
    [mutableDict setValue:self.position forKey:kS_PerBaseInfoJobListPosition];
    [mutableDict setValue:self.department forKey:kS_PerBaseInfoJobListDepartment];
    [mutableDict setValue:self.iDProperty forKey:kS_PerBaseInfoJobListID];
    [mutableDict setValue:self.company forKey:kS_PerBaseInfoJobListCompany];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userID = [aDecoder decodeDoubleForKey:kS_PerBaseInfoJobListUserID];
    self.lacation = [aDecoder decodeObjectForKey:kS_PerBaseInfoJobListLacation];
    self.endYear = [aDecoder decodeObjectForKey:kS_PerBaseInfoJobListEndYear];
    self.beginYear = [aDecoder decodeObjectForKey:kS_PerBaseInfoJobListBeginYear];
    self.position = [aDecoder decodeObjectForKey:kS_PerBaseInfoJobListPosition];
    self.department = [aDecoder decodeObjectForKey:kS_PerBaseInfoJobListDepartment];
    self.iDProperty = [aDecoder decodeObjectForKey:kS_PerBaseInfoJobListID];
    self.company = [aDecoder decodeObjectForKey:kS_PerBaseInfoJobListCompany];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_userID forKey:kS_PerBaseInfoJobListUserID];
    [aCoder encodeObject:_lacation forKey:kS_PerBaseInfoJobListLacation];
    [aCoder encodeObject:_endYear forKey:kS_PerBaseInfoJobListEndYear];
    [aCoder encodeObject:_beginYear forKey:kS_PerBaseInfoJobListBeginYear];
    [aCoder encodeObject:_position forKey:kS_PerBaseInfoJobListPosition];
    [aCoder encodeObject:_department forKey:kS_PerBaseInfoJobListDepartment];
    [aCoder encodeObject:_iDProperty forKey:kS_PerBaseInfoJobListID];
    [aCoder encodeObject:_company forKey:kS_PerBaseInfoJobListCompany];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_PerBaseInfoJobList *copy = [[S_PerBaseInfoJobList alloc] init];
    
    if (copy) {

        copy.userID = self.userID;
        copy.lacation = [self.lacation copyWithZone:zone];
        copy.endYear = [self.endYear copyWithZone:zone];
        copy.beginYear = [self.beginYear copyWithZone:zone];
        copy.position = [self.position copyWithZone:zone];
        copy.department = [self.department copyWithZone:zone];
        copy.iDProperty = [self.iDProperty copyWithZone:zone];
        copy.company = [self.company copyWithZone:zone];
    }
    
    return copy;
}


@end
