//
//  S_LawDetailSelectArticles.m
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_LawDetailSelectArticles.h"


NSString *const kS_LawDetailSelectArticlesCategory = @"category";
NSString *const kS_LawDetailSelectArticlesPubDate = @"pub_date";
NSString *const kS_LawDetailSelectArticlesId = @"id";
NSString *const kS_LawDetailSelectArticlesPublishNum = @"publish_num";
NSString *const kS_LawDetailSelectArticlesEffectiveness = @"effectiveness";
NSString *const kS_LawDetailSelectArticlesTitle = @"title";
NSString *const kS_LawDetailSelectArticlesPublishDep = @"publish_dep";
NSString *const kS_LawDetailSelectArticlesEffectiveLevel = @"effective_level";
NSString *const kS_LawDetailSelectArticlesEffectiveDate = @"effective_date";
NSString *const kS_LawDetailSelectArticlesBody = @"body";


@interface S_LawDetailSelectArticles ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_LawDetailSelectArticles

@synthesize category = _category;
@synthesize pubDate = _pubDate;
@synthesize selectArticlesIdentifier = _selectArticlesIdentifier;
@synthesize publishNum = _publishNum;
@synthesize effectiveness = _effectiveness;
@synthesize title = _title;
@synthesize publishDep = _publishDep;
@synthesize effectiveLevel = _effectiveLevel;
@synthesize effectiveDate = _effectiveDate;
@synthesize body = _body;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.category = [self objectOrNilForKey:kS_LawDetailSelectArticlesCategory fromDictionary:dict];
            self.pubDate = [self objectOrNilForKey:kS_LawDetailSelectArticlesPubDate fromDictionary:dict];
            self.selectArticlesIdentifier = [[self objectOrNilForKey:kS_LawDetailSelectArticlesId fromDictionary:dict] doubleValue];
            self.publishNum = [self objectOrNilForKey:kS_LawDetailSelectArticlesPublishNum fromDictionary:dict];
            self.effectiveness = [self objectOrNilForKey:kS_LawDetailSelectArticlesEffectiveness fromDictionary:dict];
            self.title = [self objectOrNilForKey:kS_LawDetailSelectArticlesTitle fromDictionary:dict];
            self.publishDep = [self objectOrNilForKey:kS_LawDetailSelectArticlesPublishDep fromDictionary:dict];
            self.effectiveLevel = [self objectOrNilForKey:kS_LawDetailSelectArticlesEffectiveLevel fromDictionary:dict];
            self.effectiveDate = [self objectOrNilForKey:kS_LawDetailSelectArticlesEffectiveDate fromDictionary:dict];
            self.body = [self objectOrNilForKey:kS_LawDetailSelectArticlesBody fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.category forKey:kS_LawDetailSelectArticlesCategory];
    [mutableDict setValue:self.pubDate forKey:kS_LawDetailSelectArticlesPubDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.selectArticlesIdentifier] forKey:kS_LawDetailSelectArticlesId];
    [mutableDict setValue:self.publishNum forKey:kS_LawDetailSelectArticlesPublishNum];
    [mutableDict setValue:self.effectiveness forKey:kS_LawDetailSelectArticlesEffectiveness];
    [mutableDict setValue:self.title forKey:kS_LawDetailSelectArticlesTitle];
    [mutableDict setValue:self.publishDep forKey:kS_LawDetailSelectArticlesPublishDep];
    [mutableDict setValue:self.effectiveLevel forKey:kS_LawDetailSelectArticlesEffectiveLevel];
    [mutableDict setValue:self.effectiveDate forKey:kS_LawDetailSelectArticlesEffectiveDate];
    [mutableDict setValue:self.body forKey:kS_LawDetailSelectArticlesBody];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.category = [aDecoder decodeObjectForKey:kS_LawDetailSelectArticlesCategory];
    self.pubDate = [aDecoder decodeObjectForKey:kS_LawDetailSelectArticlesPubDate];
    self.selectArticlesIdentifier = [aDecoder decodeDoubleForKey:kS_LawDetailSelectArticlesId];
    self.publishNum = [aDecoder decodeObjectForKey:kS_LawDetailSelectArticlesPublishNum];
    self.effectiveness = [aDecoder decodeObjectForKey:kS_LawDetailSelectArticlesEffectiveness];
    self.title = [aDecoder decodeObjectForKey:kS_LawDetailSelectArticlesTitle];
    self.publishDep = [aDecoder decodeObjectForKey:kS_LawDetailSelectArticlesPublishDep];
    self.effectiveLevel = [aDecoder decodeObjectForKey:kS_LawDetailSelectArticlesEffectiveLevel];
    self.effectiveDate = [aDecoder decodeObjectForKey:kS_LawDetailSelectArticlesEffectiveDate];
    self.body = [aDecoder decodeObjectForKey:kS_LawDetailSelectArticlesBody];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_category forKey:kS_LawDetailSelectArticlesCategory];
    [aCoder encodeObject:_pubDate forKey:kS_LawDetailSelectArticlesPubDate];
    [aCoder encodeDouble:_selectArticlesIdentifier forKey:kS_LawDetailSelectArticlesId];
    [aCoder encodeObject:_publishNum forKey:kS_LawDetailSelectArticlesPublishNum];
    [aCoder encodeObject:_effectiveness forKey:kS_LawDetailSelectArticlesEffectiveness];
    [aCoder encodeObject:_title forKey:kS_LawDetailSelectArticlesTitle];
    [aCoder encodeObject:_publishDep forKey:kS_LawDetailSelectArticlesPublishDep];
    [aCoder encodeObject:_effectiveLevel forKey:kS_LawDetailSelectArticlesEffectiveLevel];
    [aCoder encodeObject:_effectiveDate forKey:kS_LawDetailSelectArticlesEffectiveDate];
    [aCoder encodeObject:_body forKey:kS_LawDetailSelectArticlesBody];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_LawDetailSelectArticles *copy = [[S_LawDetailSelectArticles alloc] init];
    
    if (copy) {

        copy.category = [self.category copyWithZone:zone];
        copy.pubDate = [self.pubDate copyWithZone:zone];
        copy.selectArticlesIdentifier = self.selectArticlesIdentifier;
        copy.publishNum = [self.publishNum copyWithZone:zone];
        copy.effectiveness = [self.effectiveness copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.publishDep = [self.publishDep copyWithZone:zone];
        copy.effectiveLevel = [self.effectiveLevel copyWithZone:zone];
        copy.effectiveDate = [self.effectiveDate copyWithZone:zone];
        copy.body = [self.body copyWithZone:zone];
    }
    
    return copy;
}


@end
