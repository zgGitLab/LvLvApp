//
//  AppDelegate.m
//  LawApp
//
//  Created by yfzx_sh_gaoyy on 15/10/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "AppDelegate.h"

#import "RequestManager.h"
#import "LoginViewController.h"
#import "ToolViewController.h"
#import "CustomNavigationController.h"
#import "LoginViewController.h"
#import "GambitAnswerViewController.h"//问题详情
#import "AnswerDetailViewController.h"//回答详情
#import "DetailViewController.h" //法条详情
#import "APService.h"
#import "OtherPerMainViewController.h"
#import "PerMainPageViewController.h"

#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
//#import "WeiboSDK.h"//微博
#import "WXApi.h"//微信
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>//QQ
#import <MOBFoundation/MOBFoundation.h>

#import "imageViewController.h"//引导页

@interface AppDelegate ()<WXApiDelegate,UIAlertViewDelegate>
{
    NSInteger numberOfNotification;
    BOOL isEnterFromIcon;
    NSDictionary *_userInfo;
    UIImageView *_adView;
    NSInteger _timNum;
    UIButton *_timerBtn;
    NSTimer *_timer;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    _adView = [[UIImageView alloc]init];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.window = [[UIWindow  alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = [[UIViewController alloc]init];
    [self IsFirstRun];
    [self.window makeKeyAndVisible];
    [UINavigationBar appearance].barTintColor=[UIColor colorWithRed:247/255.0  green:249/255.0  blue:250/255.0 alpha:1];
    NSLog(@"%@",NSHomeDirectory());
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //可以添加自定义categories
        [APService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |UIUserNotificationTypeSound |UIUserNotificationTypeAlert)categories:nil];
    } else {
        //categories 必须为nil
        [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound |UIRemoteNotificationTypeAlert)
                                           categories:nil];
    }
    

    [APService setupWithOption:launchOptions];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    numberOfNotification = 0;
    isEnterFromIcon = YES;
    [APService setLogOFF];

    [self checkNetwork];//检测网络状态
    
    
    //分享设置
    [self UMShareSet];
    
    
    //友盟统计配置
    [self setUMStatics];
    

    //回答内容的字体大小
    [SingeData sharedInstance].answerContentSize = 16;

    NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"lvlv.sqlite" ofType:nil];
    NSString *targetPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/lvlv.sqlite"];
    NSFileManager *file = [NSFileManager defaultManager];
    
     self.db = [[DataBaseCenter alloc]init];
    if (![file fileExistsAtPath:targetPath]) {
        
        [file copyItemAtPath:sourcePath toPath:targetPath error:nil];
    }
    if ([self.db getDb]) {
        [self getAlldata];
    }

    [self LocalSave];   // 本地缓存数据库
    

    // 获取版本号
    [self initSystemSetting];
    
    return YES;
}

//微信的代理方法
-(void)onResp:(BaseResp *)resp
{
    NSLog(@"The response of wechat.");
}


#pragma mark -- 设置友盟分享

- (void)UMShareSet{
    [ShareSDK registerApp:@"dbab071b4a52"
          activePlatforms:@[@(SSDKPlatformTypeSinaWeibo),
                            @(SSDKPlatformTypeMail),
                            @(SSDKPlatformTypeSMS),
                            @(SSDKPlatformTypeCopy),
                            @(SSDKPlatformTypeWechat),
                            @(SSDKPlatformTypeQQ),
                            @(SSDKPlatformTypeYouDaoNote),
                            @(SSDKPlatformTypeYinXiang),
                            @(SSDKPlatformTypeEvernote),
                            @(SSDKPlatformTypeYinXiang),
                            @(SSDKPlatformTypePinterest)]
                 onImport:^(SSDKPlatformType platformType) {
                     switch (platformType)
                     {
                         case SSDKPlatformTypeWechat:
                             //                                                        [ShareSDKConnector connectWeChat:[WXApi class]];
                             [ShareSDKConnector connectWeChat:[WXApi class] delegate:self];
                             break;
                         case SSDKPlatformTypeQQ:
                             [ShareSDKConnector connectQQ:[QQApiInterface class]
                                        tencentOAuthClass:[TencentOAuth class]];
                             break;
                             //                                    case SSDKPlatformTypeSinaWeibo:
                             //                                        [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                             //                                        break;
                         default:
                             break;
                     }
                     
                 } onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
                     switch (platformType)
                     {
                         case SSDKPlatformTypeSinaWeibo:
                             //设置新浪微博应用信息,其中authType设置为使用SSO形式授权
                             [appInfo SSDKSetupSinaWeiboByAppKey:@"2995300530"
                                                       appSecret:@"66b370517eb9487a478c8f2bf5c956ae"
                                                     redirectUri:@"http://www.lvlvlaw.com"
                                                        authType:SSDKAuthTypeSSO];
                             break;
                             //微信
                         case SSDKPlatformTypeWechat:
                             [appInfo SSDKSetupWeChatByAppId:@"wxb51f0848e1f85832"
                                                   appSecret:@"78abe792c99861433a4d5819c72a35f7"];
                             break;
                             //qq
                         case SSDKPlatformTypeQQ:
                             [appInfo SSDKSetupQQByAppId:@"1104973623"
                                                  appKey:@"3fYsvIKtF91ymFR0"
                                                authType:SSDKAuthTypeBoth];
                             break;
                             
                             //有道云笔记
                         case SSDKPlatformTypeYouDaoNote:
                             [appInfo SSDKSetupYouDaoNoteByConsumerKey:@"7256673ec08484eefb87e512fd3cd370"
                                                        consumerSecret:@"a665e98a55767257ec336a4c6e83c5dd"
                                                         oauthCallback:@"http://www.lvlvlaw.com"];
                             break;
                             //印象笔记分为国内版和国际版，注意区分平台
                             //设置印象笔记（中国版）应用信息
                         case SSDKPlatformTypeYinXiang:
                             
                             //设置印象笔记（国际版）应用信息
                         case SSDKPlatformTypeEvernote:
                             [appInfo SSDKSetupEvernoteByConsumerKey:@"lvlvlaw"
                                                      consumerSecret:@"a6731d6bd22aef5e"
                                                             sandbox:NO];
                             break;
                         default:
                             break;
                     }
                 }];
}


#pragma mark -- 设置友盟统计界面

- (void)setUMStatics{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:version];
    [MobClick startWithAppkey:@"57201f98e0f55ac4c3000f41" reportPolicy:BATCH   channelId:@"AppStore"];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    [APService registerDeviceToken:deviceToken];
    NSLog(@"registrationID:%@",[APService registrationID]);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    // Required
    [APService handleRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    _userInfo = userInfo;
    // 取得 APNs 标准信息内容
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
//    NSString *content = [aps valueForKey:@"alert"]; //推送显示的内容
//    NSInteger badge = [[aps valueForKey:@"badge"] integerValue]; //badge数量
//    NSString *sound = [aps valueForKey:@"sound"]; //播放的声音
//    NSLog(@"aps:%@\n content:%@ badge:%d sound:%@",aps,content,badge,sound);
    
    NSString *userid = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"UserID"]];
    NSString *tid = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"ID"]];
    if (([Utils isBlankString:userid]== NO)&&([Utils isBlankString:tid]==NO)) {
        if (application.applicationState == UIApplicationStateActive) {
            UIAlertView *alert  =[[UIAlertView alloc]initWithTitle:@"新消息" message:[aps valueForKey:@"alert"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
            [alert show];
        }else{
            GambitAnswerViewController *answer = [[GambitAnswerViewController alloc]init];
            answer.UserID = [userInfo valueForKey:@"UserID"];
            answer.TID = [userInfo valueForKey:@"ID"];
            CustomTabBarController  *customTabBar = (CustomTabBarController *)self.window.rootViewController;
            [customTabBar.selectedViewController pushViewController:answer animated:YES];
        }
        
    }
    
    //修改badge值
    numberOfNotification = [UIApplication sharedApplication].applicationIconBadgeNumber;
    
    if (numberOfNotification) {
        [APService setBadge:--numberOfNotification];
        [UIApplication sharedApplication].applicationIconBadgeNumber=numberOfNotification;
    }else if (numberOfNotification == 0){
        [APService resetBadge];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
    
    isEnterFromIcon = NO;
    
    // IOS 7 Support Required
    [APService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}
//https://itunes.apple.com/cn/app/lu-lu/id1059306480?mt=8
#pragma mark --alertviewdelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 2) {
        if (buttonIndex == 1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/cn/app/lu-lu/id1059306480?mt=8"]];
        }
        
    }else{
        GambitAnswerViewController *answer = [[GambitAnswerViewController alloc]init];
        answer.UserID = [_userInfo valueForKey:@"UserID"];
        answer.TID = [_userInfo valueForKey:@"ID"];
        CustomTabBarController  *customTabBar = (CustomTabBarController *)self.window.rootViewController;
        [customTabBar.selectedViewController pushViewController:answer animated:YES];
    }
   
}



- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    isEnterFromIcon = YES;
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if (isEnterFromIcon) {
        [APService resetBadge];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)toLoginView
{
    
     LoginViewController *login=[[LoginViewController alloc]init];;
    UINavigationController *navLogin = [[UINavigationController alloc] initWithRootViewController:login];
    [self.window setRootViewController:navLogin];
    
}

- (void)LocalSave
{
    self.cacheDb = [[CacheDataBase alloc]init];
    if([self.cacheDb getDb])
    {
        [self.cacheDb createTables];
    }
}

- (void)getAlldata
{
    [RequestManager requestBankDataWithUrl:[NSString stringWithFormat:@"http://%@/api/Tool/getpbcinterest",K_IP]andBlock:^(NSString *message) {
        
    }];
    [RequestManager requestPerTaxDataWithUrl:[NSString stringWithFormat:@"http://%@/api/Tool/getinsurancefee",K_IP]];
    [RequestManager requestTaxThresholdWithUrl:[NSString stringWithFormat:@"http://%@/api/Tool/getInsurancetial",K_IP]];
}
// 检查版本更新
#pragma mark - 获取版本号
- (void)initSystemSetting {
    [RequestManager upadeversionWith:@"0" andBlock:^(NSString *message) {
        self.version = [NSString stringWithFormat:@"%@",message];
        if ([message isEqualToString:@""]) {
            return;
        }
        [self checkVersion];
    }];
}

#pragma mark - 检测版本号
- (void)checkVersion
{
    if ([self checkAppVersion]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"发现新版本" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"更新", nil];
        alert.tag = 2;
        [alert show];
    }
}


- (BOOL)checkAppVersion
{
    NSString *curVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    if ([self.version compare:curVer options:NSCaseInsensitiveSearch] == 1) {
        return YES;
    }else{
        return NO;
    }
}


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    NSString *str = [url absoluteString];
    NSLog(@"str %@",str);
    NSLog(@"URL scheme:%@", [url scheme]);
    NSLog(@"URL query: %@", [url query]);
    return YES;
}
/**
 *
 *
 *  @param BOOL
 *
 *  @return
 */
//通过scheme打开该app应用，接受参数的方法

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(id)annotation
{
    NSString * text=[[url host]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"text:%@",text);
    [self goToPage:text];
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    NSString * text=[[url host]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"text:%@",text);
    
    [self goToPage:text];

    
    
    return YES;
}

- (void)goToPage:(NSString *)text
{
    CustomTabBarController  *customTabBar = (CustomTabBarController *)self.window.rootViewController;
    
    NSLog(@"%@",customTabBar.navc1.viewControllers);
//    NSLog(@"%@",customTabBar.selectedViewController.navigationController.viewControllers);
    NSLog(@"----%@",[customTabBar.navc1.viewControllers.lastObject class]);
//    NSLog(@"++++%@",[customTabBar.selectedViewController.navigationController.viewControllers.lastObject class]);
    NSString *tempStr = nil;
    NSMutableArray *paramArray = [NSMutableArray arrayWithArray:[text componentsSeparatedByString:@"&"]];
    if (paramArray.count != 0) {
        tempStr = paramArray[0];
        [paramArray removeObjectAtIndex:0];
    }
    
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    for (int i = 0; i < paramArray.count; i++) {
        NSString *str = paramArray[i];
        NSArray *keyArray = [str componentsSeparatedByString:@"="];
        NSString *key = keyArray[0];
        NSString *value = keyArray[1];
        [paramsDic setObject:value forKey:key];
        NSLog(@"key:%@ ==== value:%@", key, value);
    }
    
    if ([tempStr isEqualToString:@"CommentsPage"]) {
        NSLog(@"回答");

            AnswerDetailViewController *commentVC = [[AnswerDetailViewController alloc]init];
            commentVC.UserID = [paramsDic objectForKey:@"UserID"];
            commentVC.UserID = [paramsDic objectForKey:@"AUserID"];
            commentVC.answerID = [paramsDic objectForKey:@"AnswerID"];
            commentVC.TID = [paramsDic objectForKey:@"TID"];
            [customTabBar.selectedViewController pushViewController:commentVC animated:YES];

 
    }else if ([tempStr isEqualToString:@"DetailPage"]){
        NSLog(@"案例和法条");
//        if (![NSStringFromClass([customTabBar.navc1.viewControllers.lastObject class]) isEqualToString:@"DetailViewController"]) {
            DetailViewController *detailVC = [[DetailViewController alloc]init];
            detailVC.iDProperty = 0;
            detailVC.IDArr = [NSMutableArray arrayWithObject:[paramsDic objectForKey:@"id"]];
            if ([[paramsDic objectForKey:@"category"] isEqualToString:@"0"]) {
                detailVC.type = 2;
                detailVC.typeArr = [NSMutableArray arrayWithObject:@"2"];
            }else{
                detailVC.type = 1;
                detailVC.typeArr = [NSMutableArray arrayWithObject:@"1"];
            }
            
            [customTabBar.selectedViewController pushViewController:detailVC animated:YES];
//        }
        
        
        
    }else if ([tempStr isEqualToString:@"OthersPagePhone"])
    {
        
        if ([[paramsDic objectForKey:@"UserID"] isEqualToString:[paramsDic objectForKey:@"OtherID"]]) {
            NSLog(@"个人主页");

            if ([[paramsDic objectForKey:@"UserID"] intValue] == [kUserID intValue]) {
                PerMainPageViewController *perVC = [[PerMainPageViewController alloc]init];
                perVC.userID = [paramsDic objectForKey:@"UserID"];
                [customTabBar.selectedViewController pushViewController:perVC animated:YES];
            }else{
                
                OtherPerMainViewController *otherPerVC = [[OtherPerMainViewController alloc]init];
                otherPerVC.userID = kUserID;
                otherPerVC.otherID = [paramsDic objectForKey:@"UserID"];
                [customTabBar.selectedViewController pushViewController:otherPerVC animated:YES];
                
            }
 
            
        }else{
            NSLog(@"他人主页");
            
            //            CustomTabBarController  *customTabBar = (CustomTabBarController *)self.window.rootViewController;
            
//            if (![NSStringFromClass([customTabBar.navc4.viewControllers.lastObject class]) isEqualToString:@"OtherPerMainViewController"]) {
                OtherPerMainViewController *otherPerVC = [[OtherPerMainViewController alloc]init];
                otherPerVC.userID = [paramsDic objectForKey:@"UserID"];
                otherPerVC.otherID = [paramsDic objectForKey:@"OtherID"];
                [customTabBar.selectedViewController pushViewController:otherPerVC animated:YES];
//            }
            
            
            
            
        }
        
    }else if ([tempStr isEqualToString:@"Mobile"]){
        
//        if (![NSStringFromClass([customTabBar.navc2.viewControllers.lastObject class]) isEqualToString:@"GambitAnswerViewController"]) {
        
            GambitAnswerViewController *answer = [[GambitAnswerViewController alloc]init];
            answer.UserID = [paramsDic objectForKey:@"TUserID"];
            answer.TID = [paramsDic objectForKey:@"TID"];
            answer.Answer_ID = [paramsDic objectForKey:@"AnswerID"];
            CustomTabBarController  *customTabBar = (CustomTabBarController *)self.window.rootViewController;
            [customTabBar.selectedViewController pushViewController:answer animated:YES];
        
//        }
       
        
    }

}

//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
//    NSLog(@"Calling Application Bundle ID: %@", sourceApplication);
//    NSLog(@"URL scheme:%@", [url scheme]);
//    NSLog(@"URL query: %@", [url query]);
//    return YES;
//}

#pragma mark --检测网络状态

-(void)checkNetwork
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    // 连接状态回调处理
    /* AFNetworking的Block内使用self须改为weakSelf, 避免循环强引用, 无法释放 */
    __weak typeof(self) weakSelf = self;
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         switch (status)
         {
             case AFNetworkReachabilityStatusUnknown:
                 // 回调处理
                 weakSelf.networkReachability = [NSNumber numberWithInt:3];
                 
                 
//                 [MBProgressHUD showOlnyMessage:@"当前网络为未知网络,请谨慎!" toView:_window];
                 break;
             case AFNetworkReachabilityStatusNotReachable:
                 // 回调处理
                 weakSelf.networkReachability = [NSNumber numberWithInt:0];
                 [MBProgressHUD showOlnyMessage:@"您网络已断开,请检查你的网络" toView:_window];
              
                 
                 break;
             case AFNetworkReachabilityStatusReachableViaWWAN:
                 // 回调处理
                 weakSelf.networkReachability = [NSNumber numberWithInt:1];
//                 [MBProgressHUD showOlnyMessage:@"您已切换到蜂窝移动网络" toView:_window];
               
                 
                 break;
             case AFNetworkReachabilityStatusReachableViaWiFi:
                 // 回调处理
                 weakSelf.networkReachability = [NSNumber numberWithInt:2];
//                 [MBProgressHUD showOlnyMessage:@"您已切换到WiFi" toView:_window];
               
                 
                 break;
             default:
                 break;
         }
     }];
}

-(void)networkReachabilityMessage:(NSString *)message
{
    if (_originalNetworkReachability)
    {
        if (![_originalNetworkReachability isEqualToNumber:_networkReachability])
        {
//            [Utils showMsg:message];
        }
    }
    
    _originalNetworkReachability = _networkReachability;
}


#pragma mark -- 判断是否是第一次安装

-(void)IsFirstRun
{
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if (![userDefault objectForKey:@"firstLoading"] || ![[userDefault objectForKey:@"firstLoading"] isEqualToString:version])//第一次启动(第一次安装程序进入引导页)
    {
        imageViewController *loading=[[imageViewController alloc] init];
        
        self.window.rootViewController = loading;
        
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(gotoMainViewController)
                                                    name:@"loadingFinish" object:nil];
        
    }
    else //判断是否有活动页
    {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        //申明返回的结果是json类型
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
        manager.requestSerializer.timeoutInterval = 2;
        [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
        //发送请求
        [manager GET:[NSString stringWithFormat:KadUrl,K_IP] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
            BaseDataModel *model = [[BaseDataModel alloc]init];
            [model setValuesForKeysWithDictionary:dict];
            if (![model.Code  isEqual: @200] || [model.Data[@"Show"] integerValue] == 0) {
                [self gotoMainViewController];
            }else {
                [self performSelector:@selector(loadAdImage:) withObject:model.Data[@"ImageUrl"]];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@",error);
            [self gotoMainViewController];
        }];
    }
}


//给图片添加模糊效果
- (UIImage*) blur:(UIImage*)theImage
{
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:theImage.CGImage];
    
    CIFilter *affineClampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    CGAffineTransform xform = CGAffineTransformMakeScale(1.0, 1.0);
    [affineClampFilter setValue:inputImage forKey:kCIInputImageKey];
    [affineClampFilter setValue:[NSValue valueWithBytes:&xform
                                               objCType:@encode(CGAffineTransform)]
                         forKey:@"inputTransform"];
    
    CIImage *extendedImage = [affineClampFilter valueForKey:kCIOutputImageKey];
    
    // setting up Gaussian Blur (could use one of many filters offered by Core Image)
    CIFilter *blurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [blurFilter setValue:extendedImage forKey:kCIInputImageKey];
    [blurFilter setValue:[NSNumber numberWithFloat:1.f] forKey:@"inputRadius"];
    CIImage *result = [blurFilter valueForKey:kCIOutputImageKey];
    
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage];
    //create a UIImage for this function to "return" so that ARC can manage the memory of the blur...
    //ARC can't manage CGImageRefs so we need to release it before this function "returns" and ends.
    CGImageRelease(cgImage);//release CGImageRef because ARC doesn't manage this on its own.
    
    return returnImage;
}


#pragma mark -- 加载广告页

- (void)loadAdImage:(NSString *)url{
    
    _adView.frame = CGRectMake(0, 0, kWidth, kHeight);
    [_adView sd_setImageWithURL:[NSURL URLWithString:url]];
    // 设定为缩放
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    // 动画选项设定
    animation.duration = 3; // 动画持续时间
    animation.repeatCount = 1; // 重复次数
    animation.autoreverses = YES; // 动画结束时执行逆动画
    
    // 缩放倍数
    animation.fromValue = [NSNumber numberWithFloat:1.0]; // 开始时的倍率
    animation.toValue = [NSNumber numberWithFloat:1.2]; // 结束时的倍率
    
    // 添加动画
    [_adView.layer addAnimation:animation forKey:@"scale-layer"];
    [APP.window addSubview:_adView];
    
    _timerBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _timerBtn.frame = CGRectMake(kWidth-70 , 40, 50, 40);
    _timerBtn.backgroundColor = [UIColor blackColor];
    [_timerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _timerBtn.alpha = 0.6;
    _timerBtn.clipsToBounds = YES;
    _timerBtn.layer.cornerRadius = 5;
    _timNum = 2;
    [_timerBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)_timNum] forState:UIControlStateNormal];
    [_timerBtn addTarget:self action:@selector(gotoMainViewController) forControlEvents:UIControlEventTouchUpInside];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];

    [_timer setFireDate:[NSDate distantPast]];
    [APP.window addSubview:_timerBtn];
}

//定时器事件，倒计时
- (void)timerAction{
    
    [_timerBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)_timNum] forState:UIControlStateNormal];
    if (_timNum <= 0) {
        [_timer invalidate];
        _timer = nil;
        [self gotoMainViewController];
    }
    -- _timNum;
}

//跳转到主界面
- (void)gotoMainViewController{
    [_adView removeFromSuperview];
    [_timer invalidate];
    _customTabBar = [[CustomTabBarController  alloc]init];
    [self.window setRootViewController:_customTabBar];
    
}
@end
