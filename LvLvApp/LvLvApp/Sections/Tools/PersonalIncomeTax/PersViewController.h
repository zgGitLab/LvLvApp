//
//  PersViewController.h
//  Tools
//
//  Created by IOS8 on 15/10/12.
//  Copyright © 2015年 IOS8. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *personalTable;

@end
