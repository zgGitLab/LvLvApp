//
//  EditNicNameViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/1.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "EditNicNameViewController.h"

@interface EditNicNameViewController ()<UITextViewDelegate>
{
    LPlaceholderTextView *textNic;
    UIView *bgVi;
    NSArray *hoderArray;
}
@end

@implementation EditNicNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.titleString;
    hoderArray = @[@"2-16个字符，支持中英文，数字",@"介绍一下自己......",@"请输入您的手机号码或邮箱"];
    self.view.backgroundColor = COLOR(235, 236, 237);
    UIButton *finshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    finshButton.frame = CGRectMake(0, 0, 40, 15);
    [finshButton setTitle:@"完成" forState:UIControlStateNormal];
    [finshButton setTitleColor:[UIColor colorWithRed:65/255.0 green:71/255.0 blue:83/255.0 alpha:1] forState:UIControlStateNormal];
    [finshButton addTarget:self action:@selector(finshBtn:) forControlEvents:UIControlEventTouchUpInside];
    finshButton.titleLabel.font = [UIFont systemFontOfSize:18];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:finshButton];
    [self createEditNicNameView];
    //添加一个监听者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)finshBtn:(UIButton *)btn
{
    if ([textNic.text isEqualToString:@""]) {
        K_ALERT(@"您还没有输入内容");
        return;
    }
    
    if (self.ID == 1) {     // 昵称；
        
        if (textNic.text.length >= 2 && textNic.text.length <= 16) {
            [RequestManager editNIcName:[self getUserId] andNicName:textNic.text andBlock:^(NSString *message) {
                if ([message isEqualToString:@"成功"]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [MBProgressHUD showOlnyMessage:message toView:self.view];
                }
                
            }];
        }else{
            K_ALERT(@"请输入2-16个字符");
            return;
        }
    }
    if (self.ID == 2){// 编辑简介
    
        [RequestManager editInfoMessage:[self getUserId] andInfoMessage:textNic.text andBlock:^(NSString *message) {
            if ([message isEqualToString:@"成功"]) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [MBProgressHUD showOlnyMessage:message toView:self.view];
            }
        }];
    
    }
    if (self.ID == 3){                  // 编辑电话号码
        
        [RequestManager editphoneNumber:[self getUserId] andphoneNumber:textNic.text andBlock:^(NSString *message) {
            if ([message isEqualToString:@"成功"]) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [MBProgressHUD showOlnyMessage:message toView:self.view];
            }
        }];
        
    }
}

- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}

- (void)createEditNicNameView
{
    bgVi = [[UIView alloc] initWithFrame:CGRectMake(0,20, SIZE.width, 180)];
    bgVi.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgVi];
    textNic = [[LPlaceholderTextView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, 180)];
    textNic.delegate = self;
    [textNic becomeFirstResponder];
    textNic.placeholderText = hoderArray[_ID-1];
    
    if ([self.concentString isEqualToString:@""]) {
        textNic.placeholderColor = COLOR(180,180,180);
    }else{
        textNic.placeholderColor = [UIColor clearColor];
        textNic.text = self.concentString;
    }
    
//    textNic.placeholderColor=[UIColor grayColor];
    textNic.font = [UIFont systemFontOfSize:16];
    textNic.textColor = COLOR(158,158,159);
    [bgVi addSubview:textNic];
    if (self.ID == 3) {
        textNic.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }

}


#pragma mark ----监听事件----
- (void)keyBoardFrameChange:(NSNotification* )notification
{
    //获取键盘上来的时间和键盘的大小
    double time = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect;
    [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&rect];
    static UIView *registVI = nil;
    if (registVI == nil) {
        registVI = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, SIZE.height)];
        registVI.backgroundColor = [UIColor colorWithRed:70/255.0 green:70/255.0 blue:70/255.0 alpha:0];
        //        registVI.alpha = 0.1;
        [self.view addSubview:registVI];
        
    }
    
    if (rect.origin.y == SIZE.height) {
        [registVI removeFromSuperview];
        registVI = nil;
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:7];
    [UIView setAnimationDuration:time];
    [UIView commitAnimations];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(void)textViewDidChange:(UITextView *)textView
{
    textNic.placeholderColor = [textView.text isEqualToString:@""] ? COLOR(180,180,180) : [UIColor clearColor];
}

@end
