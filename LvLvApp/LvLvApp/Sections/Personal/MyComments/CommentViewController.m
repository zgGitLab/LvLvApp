//
//  CommentViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "CommentViewController.h"
#import "CommentTableViewCell.h"


#import "M_CommBaseClass.h"
#import "M_CommData.h"
#import "M_CommDataModels.h"
#import "SCSwipeTableViewCell.h"



#import "AnswerDetailViewController.h"
@interface CommentViewController ()<SCSwipeTableViewCellDelegate>
{
     M_CommData*comdata;
     NSInteger _pageIndex;
    NSMutableArray *btnArr;
    CGFloat _titleHeight;
    CGFloat _contentHeight;
    CGFloat _bottomHeight;
    NSInteger MindexPathS;
    CGFloat titleSpace;//昵称和title的距离
    CGFloat commentSpace;//赞同和内容之间的距离
    CGFloat topSpace;//顶部和底部的距离
    CGFloat lineSpace;//行间距,这里的行间距是估算的，真实的行间距是3
    CGFloat doubleLineHeight1;
    CGFloat doubleLineHeight2;
    CGFloat doubleLineHeight3;
    CGFloat delheight;
}
@end

@implementation CommentViewController

- (void)viewDidLoad {
    titleSpace = 9;
    commentSpace = 7;
    topSpace = 18;
    lineSpace = 2;
  
    
    doubleLineHeight1 = 40;//
    doubleLineHeight2 = 35;//
    doubleLineHeight3 = 54;
    _pageIndex=1;
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.title = @"我的评论";
    self.commenArray =[[NSMutableArray alloc]init];
    [self.myTable setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    [self commerequesWithPageindex:_pageIndex andPageSize:10];
    [self.myTable reloadData];
   
     self.myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.commenArray removeAllObjects];
            [self commerequesWithPageindex:1 andPageSize:10];
            [self.view layoutSubviews];}];
    
    self.myTable .mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        [self.commenArray removeAllObjects];
    [self commerequesWithPageindex:++_pageIndex andPageSize:10];
        [self.view layoutSubviews];
    }];
     [self setExtraCellLineHidden:self.myTable];
}

-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [UIView new];
    
    view.backgroundColor = [UIColor clearColor];
    
    [tableView setTableFooterView:view];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem*butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(returnButton)];
    ;
    butItem.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
}
- (void)returnButton{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.commenArray.count;
}
#pragma mark UITableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UIButton *but3 =[UIButton buttonWithType:UIButtonTypeCustom];
    but3.backgroundColor =[UIColor clearColor];
    [but3 addTarget:self action:@selector(butss:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
    btn1.backgroundColor = [UIColor redColor];
    [btn1 setTitle:@"删除" forState:UIControlStateNormal];
    UIButton *btn2 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 0.1, 55)];
    [btn2 setTitle:@"" forState:UIControlStateNormal];
    [btn2 setTitle:@"" forState:UIControlStateNormal];
    btnArr = [[NSMutableArray alloc]initWithObjects:btn1,btn2, nil];

     SCSwipeTableViewCell * cell= [[SCSwipeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier withBtns:btnArr tableView:tableView];
     cell.delegate =self;
    if (self.commenArray.count > indexPath.row) {
       comdata =self.commenArray[indexPath.row];
        
        UILabel *nickLabel =[[UILabel alloc]initWithFrame:CGRectMake(15, topSpace, self.view.frame.size.width, 15)];
        nickLabel.textColor =setColor(171, 171, 171);
        nickLabel.font =[UIFont systemFontOfSize:11];
        nickLabel.text =comdata.times;
        nickLabel.numberOfLines =1;
        [cell.SCContentView addSubview:nickLabel];
        
        
//        if ([comdata.logoUrl isEqualToString:@""]||(comdata.logoUrl == nil)){
            CGFloat titleHeight = [Utils textHeightFromTextString:comdata.title width:kWidth-30 fontSize:16];
            if (titleHeight > 30) {
                titleHeight = doubleLineHeight1+lineSpace;
            }
            
            UILabel *titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(15, 30+titleSpace, kWidth-30, titleHeight)];
            titleLabel.textColor =setColor(81, 88, 99);
            titleLabel.font =[UIFont systemFontOfSize:16];
            titleLabel.text =comdata.title;
            titleLabel.numberOfLines =0;
            [cell.SCContentView addSubview:titleLabel];
            
            NSString *tit = [Utils replaceImageString:comdata.answerContent];
            tit = [tit stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            CGFloat contentHeight = [Utils textHeightFromTextString:tit width:kWidth-30 fontSize:14];
            if (contentHeight > 30) {
                contentHeight = doubleLineHeight2+lineSpace;
            }
            UILabel *contentLabel =[[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(titleLabel.frame)+commentSpace, kWidth-30, contentHeight)];
            contentLabel.textColor =setColor(122, 122, 122);
            contentLabel.font =[UIFont systemFontOfSize:14];
            contentLabel.numberOfLines =0;
            contentLabel.text =tit;
            [cell.SCContentView addSubview:contentLabel];
            
            UILabel *commentLabel =[[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(contentLabel.frame)+commentSpace, kWidth-30, 15)];
            commentLabel.textColor =setColor(171, 171, 171);
            commentLabel.font =[UIFont systemFontOfSize:11];
            commentLabel.text =[NSString stringWithFormat:@"赞同 %ld · 评论 %ld · 收藏 %ld",(long)comdata.counts,(long)comdata.commentCount, (long)comdata.collectCount];
            commentLabel.numberOfLines =1;
            [cell.SCContentView addSubview:commentLabel];
            
            CGFloat tiHeight = [Utils textHeightFromTextString:comdata.title width:kWidth-30 fontSize:16];
            if (tiHeight > 30) {
                tiHeight = doubleLineHeight1+lineSpace;
                cell.SCContentView.frame = CGRectMake(0, 0, SIZE.width, 13+CGRectGetMaxY(commentLabel.frame));
                btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, cell.SCContentView.frame.size.height);
                but3.frame =CGRectMake(15, 1, self.view.frame.size.width,cell.SCContentView.frame.size.height);
              
                
                
            }else {
                cell.SCContentView.frame = CGRectMake(0, 0, SIZE.width, 17+CGRectGetMaxY(commentLabel.frame));
                btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, cell.SCContentView.frame.size.height);
                but3.frame =CGRectMake(15, 1, self.view.frame.size.width,cell.SCContentView.frame.size.height);
                
                
            }
            [cell.SCContentView addSubview:but3];
        }

//           but3.frame =CGRectMake(0, 0, self.view.frame.size.width,cell.SCContentView.frame.size.height);
    
    return cell;
}
- (void)butss:(UIButton*)sender{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.isShowBtn) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SC_CELL_SHOULDCLOSE" object:nil userInfo:@{@"action":@"closeCell"}];
        app.isShowBtn = NO;
        return;
    }
    UIView *view = [[sender superview] superview];
    SCSwipeTableViewCell *cell = (SCSwipeTableViewCell *)[view superview];
    NSIndexPath *indexPath = [self.myTable indexPathForCell:cell];
    comdata =[self.commenArray objectAtIndex:indexPath.row];
    AnswerDetailViewController *answer =[[AnswerDetailViewController alloc]init];
   NSString *tuserID =[NSString stringWithFormat:@"%d",(int)comdata.tUserID];
    NSString *usr =[NSString stringWithFormat:@"%d",(int)comdata.aUserID];
    answer.UserID =usr;
    answer.TID =comdata.tID;
    answer.tUserID =tuserID;
    answer.answerID =comdata.answerID;
    [self.navigationController pushViewController:answer animated:YES];
    
    
}
//判断cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    CGFloat totalHeight = topSpace+15+titleSpace;
    if (self.commenArray.count > indexPath.row) {
        comdata =self.commenArray[indexPath.row];        //无图片
        if ([comdata.logoUrl isEqualToString:@""]||(comdata.logoUrl == nil)){
            CGFloat titleHeight = [Utils textHeightFromTextString:comdata.title width:kWidth-30 fontSize:16];
            if (titleHeight > 30) {
                titleHeight = doubleLineHeight2+lineSpace;
            }
            totalHeight += titleHeight+commentSpace;
            NSString *tit = [Utils replaceImageString:comdata.answerContent];
            tit = [tit stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            CGFloat contentHeight = [Utils textHeightFromTextString:tit width:kWidth-30 fontSize:14];
            if (contentHeight > 30) {
                contentHeight = doubleLineHeight2+lineSpace;
            }
            totalHeight += contentHeight+commentSpace;
            totalHeight += 15+15;

        }else{//有图片
            CGFloat titleHeight = [Utils textHeightFromTextString:comdata.title width:kWidth-30 fontSize:16];
            if (titleHeight > 30) {
                titleHeight = doubleLineHeight2+lineSpace;
            }
            totalHeight += titleHeight +commentSpace;
            NSString *tit = [Utils replaceImageString:comdata.answerContent];
            tit = [tit stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            CGFloat contentHeight = [Utils textHeightFromTextString:tit width:kWidth-30 fontSize:14];
            if (contentHeight > 30) {
                contentHeight = doubleLineHeight2+lineSpace;
            }
            totalHeight += contentHeight +commentSpace;
            totalHeight += 15+15;
        }

    }

    return totalHeight;
    

}

#pragma mark SCSwipeTableViewCellDelegate

- (void)SCSwipeTableViewCelldidSelectBtnWithTag:(NSInteger)tag andIndexPath:(NSIndexPath *)indexpath{
    
    if (self.commenArray.count>indexpath.row) {
        
        [self myDraftdeleteRequestWith:indexpath];
        
    }
}
- (void)cellOptionBtnDidShow{
    NSLog(@"cellOptionBtnDidShow");
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    app.isShowBtn = YES;
}
- (void)myDraftdeleteRequestWith:(NSIndexPath *)indexPath
{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    if (self.commenArray.count>indexPath.row) {
        comdata = self.commenArray[indexPath.row];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KdeleteCommet,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",UserID ]];
    NSLog(@"%@",UserID);
    [param appendString:[NSString stringWithFormat:@"&ID=%@",comdata.commentID]];
    
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError){
             [self.myTable.mj_header endRefreshing];
             
             [self.myTable.mj_footer endRefreshing];
             
             return;
         }
         
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         
         [self.commenArray removeObjectAtIndex:indexPath.row];
         [self.myTable reloadData];
         [self notDraftView];

     }];
    }
}
- (void)notDraftView
{
    if(self.commenArray.count ==0) {
        UIView *views =[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2-50, self.view.bounds.size.width, 30)];
        UILabel *labe =[[UILabel alloc]init];
        [labe setText:@"您暂无评论!"];
        labe.font=[UIFont systemFontOfSize:16.f];
        CGSize size = [labe.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
        NSLog(@"%lf",size.width);
        labe.frame=CGRectMake(SIZE.width/2-size.width/2, 2, size.width, 25);        [labe setTextColor:[UIColor colorWithRed:158.0/255.0 green:158.0/255.0 blue:158.0/255.0 alpha:1]];
        [views addSubview:labe];
        //self.myTable.tableFooterView=views;
        [self.view addSubview:views];
        return;
    }
    
}

//取消选择行
- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    [self.myTable scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

//跳入到另一个页面
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    CommentTableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    cell.selected =NO;
    comdata =[self.commenArray objectAtIndex:indexPath.row];
    AnswerDetailViewController *answer =[[AnswerDetailViewController alloc]init];
//    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
//    NSString*UserID = [defaults objectForKey:@"users"];
    NSString *tuserID =[NSString stringWithFormat:@"%d",(int)comdata.tUserID];
    NSString *usr =[NSString stringWithFormat:@"%d",(int)comdata.aUserID];
    answer.UserID =usr;
    answer.TID =comdata.tID;
    //answer.tUserID =comdata.tUserID;
    answer.tUserID =tuserID;
    answer.answerID =comdata.answerID;
    [self.navigationController pushViewController:answer animated:YES];

}
//数据请求
- (void)commerequesWithPageindex:(NSInteger)pageIndex andPageSize:(NSInteger)pageSize{
{

    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    // 1.url
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KcommuUrl,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",UserID]];
    [param appendString:[NSString stringWithFormat:@"&pageindex=%d",(int)pageIndex]];
    [param appendString:[NSString stringWithFormat:@"&pageSize=%d",(int)pageSize]];
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)
         {
             return ;
         }
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         M_CommBaseClass *commdata =[[M_CommBaseClass alloc]initWithDictionary:dict];
         [self.commenArray addObjectsFromArray:commdata.data];
        
//         [self performSelectorOnMainThread:@selector(refreshUI) withObject:nil waitUntilDone:YES];
         [self.myTable.mj_header endRefreshing];
         [self.myTable.mj_footer endRefreshing];
         [self.myTable reloadData];
         [self notDraftView];
     }];
}
}
- (void)refreshUI{
    
    [self.myTable reloadData];

}

@end
