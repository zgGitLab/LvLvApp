//
//  SetViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetViewController : BaseViewController
@property (nonatomic, strong)NSString *VIPstr;
@property (weak, nonatomic) IBOutlet UILabel *cacheLab;
@property (strong, nonatomic) IBOutlet UIImageView *vipImageView;

@property (strong, nonatomic)NSMutableArray *setArray;
@property (strong, nonatomic)UIImageView *ImageView;
@property (strong, nonatomic)NSString *Str;
@property (strong, nonatomic)NSString *ispassStr;//审核条件

@end
