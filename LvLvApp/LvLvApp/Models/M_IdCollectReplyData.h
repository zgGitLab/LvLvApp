//
//  M_IdCollectReplyData.h
//
//  Created by hope  on 16/1/7
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface M_IdCollectReplyData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) id userid;
@property (nonatomic, assign) double notifyID;
@property (nonatomic, strong) id dataDescription;
@property (nonatomic, assign) double aUserID;
@property (nonatomic, assign) double commentCount;
@property (nonatomic, strong) id timagesUrl;
@property (nonatomic, strong) id logoUrl;
@property (nonatomic, strong) id questionContent;
@property (nonatomic, strong) NSString *answerContent;
@property (nonatomic, assign) double collectCount;
@property (nonatomic, strong) NSString *sortTime;
@property (nonatomic, strong) NSString *times;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, assign) double types;
@property (nonatomic, assign) double counts;
@property (nonatomic, assign) double tUserID;
@property (nonatomic, strong) NSString *tID;
@property (nonatomic, strong) NSString *answerID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
