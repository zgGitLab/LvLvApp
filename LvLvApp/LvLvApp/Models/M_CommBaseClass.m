//
//  M_CommBaseClass.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_CommBaseClass.h"
#import "M_CommData.h"


NSString *const kM_CommBaseClassCode = @"Code";
NSString *const kM_CommBaseClassMessage = @"Message";
NSString *const kM_CommBaseClassData = @"Data";


@interface M_CommBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_CommBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_CommBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_CommBaseClassMessage fromDictionary:dict];
    NSObject *receivedM_CommData = [dict objectForKey:kM_CommBaseClassData];
    NSMutableArray *parsedM_CommData = [NSMutableArray array];
    if ([receivedM_CommData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedM_CommData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedM_CommData addObject:[M_CommData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedM_CommData isKindOfClass:[NSDictionary class]]) {
       [parsedM_CommData addObject:[M_CommData modelObjectWithDictionary:(NSDictionary *)receivedM_CommData]];
    }

    self.data = [NSArray arrayWithArray:parsedM_CommData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_CommBaseClassCode];
    [mutableDict setValue:self.message forKey:kM_CommBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kM_CommBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_CommBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_CommBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_CommBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_CommBaseClassCode];
    [aCoder encodeObject:_message forKey:kM_CommBaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_CommBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_CommBaseClass *copy = [[M_CommBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
