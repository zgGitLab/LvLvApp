//
//  ItemView.h
//  oc
//
//  Created by lyl on 15/11/24.
//  Copyright © 2015年 miruo. All rights reserved.
//

#import <UIKit/UIKit.h>



typedef void(^ItemViewBlock)(id obj);


@interface ItemView : UIView
{
    
    
}


@property (nonatomic, weak) id target;

@property (nonatomic, assign) SEL selecter;

@property (nonatomic,strong) NSArray *itemArray;

@property (nonatomic,assign) CGFloat itemHeith; 

@property (nonatomic,copy) __block ItemViewBlock block;






/**
 *  获取高度，应先有itemArray
 */
//- (CGFloat)getItemViewHeight;

//- (id)initWihtItemArray:(NSArray *)array;

- (void)ItemViewWithBlock:(ItemViewBlock)block;


@end

@protocol ItemViewDelegate <NSObject>



@end

