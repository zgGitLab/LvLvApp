//
//  S_MyAnserBaseClass.m
//
//  Created by IOS8  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_MyAnserBaseClass.h"
#import "S_MyAnserData.h"


NSString *const kS_MyAnserBaseClassCode = @"Code";
NSString *const kS_MyAnserBaseClassMessage = @"Message";
NSString *const kS_MyAnserBaseClassData = @"Data";


@interface S_MyAnserBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_MyAnserBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_MyAnserBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_MyAnserBaseClassMessage fromDictionary:dict];
    NSObject *receivedS_MyAnserData = [dict objectForKey:kS_MyAnserBaseClassData];
    NSMutableArray *parsedS_MyAnserData = [NSMutableArray array];
    if ([receivedS_MyAnserData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_MyAnserData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_MyAnserData addObject:[S_MyAnserData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_MyAnserData isKindOfClass:[NSDictionary class]]) {
       [parsedS_MyAnserData addObject:[S_MyAnserData modelObjectWithDictionary:(NSDictionary *)receivedS_MyAnserData]];
    }

    self.data = [NSArray arrayWithArray:parsedS_MyAnserData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_MyAnserBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_MyAnserBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kS_MyAnserBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_MyAnserBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_MyAnserBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_MyAnserBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_MyAnserBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_MyAnserBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_MyAnserBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_MyAnserBaseClass *copy = [[S_MyAnserBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
