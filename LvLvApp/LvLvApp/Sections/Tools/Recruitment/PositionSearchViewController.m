//
//  PositionSearchViewController.m
//  LvLvApp
//
//  Created by Mark on 16/3/28.
//  Copyright © 2016年 gyy. All rights reserved.
//
#define kfanbuButW 40.0
#define kfanbuButH 40.0

#import "PositionSearchViewController.h"

#import "EmploymentViewController.h"
@interface PositionSearchViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UITextFieldDelegate>
{
    UIButton *but;
    UISearchBar*seachBar;
    UIView *viewss;
    UISegmentedControl *segmented;
    UITextField *searchTextF;
    UIView *searChVi;
    UIView *searchView;
    NSMutableArray *mubtarray;
    UIView *hisFootVi;
    NSMutableArray *labedataarray;
    UIView *keyboardView;
    UITableViewCell *cell;
    

}


@end

@implementation PositionSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor whiteColor];
    labedataarray=[[NSMutableArray alloc]init];
    self.navigationController.navigationBar.hidden=YES;
    // 读取数据
   self.EmployData = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"seacheEmploy"]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (self.EmployData == nil) {
        self.EmployData =[[NSMutableArray alloc]init];
    }
  
    //判断数组是否有值
    if (self.EmployData.count != 0) {
        searChVi = [[UIView alloc] initWithFrame:CGRectMake(0, 64, SIZE.width, SIZE.height-64)];
        [self.view addSubview:searChVi];
        self.histotytablew =[[UITableView alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width,searChVi.bounds.size.height) style:UITableViewStyleGrouped];
        self.histotytablew.dataSource=self;
        self.histotytablew.delegate=self;
        [self.histotytablew setSeparatorInset:UIEdgeInsetsMake(0, 8, 0, 8)];
        self.histotytablew.backgroundColor =[UIColor whiteColor];
        [searChVi addSubview:self.histotytablew];
        
        hisFootVi = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, 100)];
        UIButton *clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        clearBtn.frame = CGRectMake(hisFootVi.center.x-70, 30, 120, 35);
        [clearBtn setBackgroundImage:[UIImage imageNamed:@"清除历史记录"] forState:UIControlStateNormal];
        [clearBtn setTitle:@"清除搜索历史" forState:UIControlStateNormal];
        clearBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [clearBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [clearBtn addTarget:self action:@selector(clearHistbut) forControlEvents:UIControlEventTouchUpInside];
        [hisFootVi addSubview:clearBtn];
        self.histotytablew.tableFooterView =hisFootVi;
    }
    //隐藏按钮的view
    keyboardView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 36)];
    [keyboardView setBackgroundColor:[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1]];
    
    UIButton *key = [UIButton buttonWithType:UIButtonTypeSystem];
    key.frame = CGRectMake(kWidth-26-15, 7, 26, 26);
    [key setImage:[[UIImage imageNamed:@"收键盘"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [key addTarget:self action:@selector(collectKeyBoard:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:key];
    
    [self views];
    [self button];
    
}
//取消返回事件
- (void)button
{    but =[[UIButton alloc]initWithFrame:CGRectMake(searchView.frame.origin.x+searchView.frame.size.width+5, 19,kfanbuButW, kfanbuButH)];
    [but setTitle:@"取消" forState: UIControlStateNormal];
    but.titleLabel.font =[UIFont systemFontOfSize:16.f];
    [but setBackgroundColor:[UIColor clearColor]];
    [but setTitleColor:[UIColor colorWithRed:75.0/255.0 green:156.0/255.0 blue:239.0/255.0 alpha:1] forState:UIControlStateNormal];
    [but addTarget:self action:@selector(fanhui)  forControlEvents:UIControlEventTouchUpInside];
    [viewss addSubview:but];
    //[searchTextF resignFirstResponder];
    
}
//键盘隐藏事件
- (void)collectKeyBoard:(UIButton *)button{
    [searchTextF resignFirstResponder];
}
//清除历史记录
- (void)clearHistbut
{
    NSLog(@"清除历史");
    [self.EmployData removeAllObjects];
    [searChVi removeFromSuperview];
    [hisFootVi removeFromSuperview];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"seacheEmploy"];
    [[NSUserDefaults standardUserDefaults] synchronize];
   
    
}
//搜索框view
- (void)views
{
    viewss = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 64)];
    
    viewss.backgroundColor=[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:233.0/255.0 alpha:1];
    [self.view addSubview:viewss];
    searchView = [[UIView alloc]initWithFrame:CGRectMake(10, 25, self.view.bounds.size.width-60, 28)];
    [viewss addSubview:searchView];
    UIImageView *viewssImage =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, searchView.frame.size.width, searchView.frame.size.height)];
    viewssImage.image =[UIImage imageNamed:@"搜索框"];
    [searchView addSubview:viewssImage];
    UIImageView *searchImage =[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 18, 18)];
    searchImage.image =[UIImage imageNamed:@"搜索图标"];
    [searchView addSubview:searchImage];
    searchTextF=[[UITextField alloc]initWithFrame:CGRectMake(searchImage.frame.size.width+searchImage.frame.origin.x +5, 0,searchView.frame.size.width-20,searchView.frame.size.height)];
    [searchTextF setPlaceholder:@"搜索问题、回答、标签或人"];
    [searchTextF setTextColor:[UIColor colorWithRed:78.0/255.0 green:88.0/255.0 blue:100.0/255.0 alpha:1]];
    [searchTextF setFont:[UIFont systemFontOfSize:14.f]];
    
    searchTextF.inputAccessoryView =keyboardView;
    [searchTextF setKeyboardType:UIKeyboardTypeWebSearch];
    [searchTextF becomeFirstResponder];
    searchTextF.returnKeyType =UIReturnKeyGoogle;
    searchTextF.delegate=self;
    [searchView addSubview:searchTextF];
}
#pragma mark textField 代理方法
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (![self.EmployData containsObject:searchTextF.text ])
    {
        [self.EmployData addObject:searchTextF.text];
        [[NSUserDefaults standardUserDefaults]setObject:self.EmployData forKey:@"seacheEmploy"];
        NSLog(@"datasss%@",searchTextF.text);
        [[NSUserDefaults standardUserDefaults] synchronize];
        //NSString *home =NSHomeDirectory();
      
    }
    EmploymentViewController *vc =[[EmploymentViewController alloc]init];
     vc.searString = searchTextF.text;
    [self.navigationController pushViewController:vc animated:YES];
    return NO;
}

//取消后的页面
- (void)fanhui
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.EmployData.count;
}
//head table
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *hisHeaderVi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SIZE.width, 15)];
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SIZE.width-10, 10)];
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.textColor = [UIColor grayColor];
    titleLab.text = @"   历史搜索";
    [hisHeaderVi addSubview:titleLab];
    return hisHeaderVi;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return 15;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier =@"ht";
    
    cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.textLabel.text= self.EmployData[indexPath.row];
    cell.textLabel.font =[UIFont systemFontOfSize:14];
    cell.textLabel.textColor=[UIColor colorWithRed:78.0/255.0 green:88.0/255.0 blue:100.0/255.0 alpha:1];
    return cell;
}
//向下页面传值
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //取消选择
    cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    //取值
    searchTextF.text= [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
