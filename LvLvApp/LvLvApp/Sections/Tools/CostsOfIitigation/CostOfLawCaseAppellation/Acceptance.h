//
//  Acceptance.h
//  LvLvApp
//
//  Created by hope on 15/10/19.
//  Copyright (c) 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseView.h"
@interface Acceptance : UIView <ChooseViewDelegate,UITextFieldDelegate>
@property (nonatomic, strong)  NSArray *dataArr;
- (id)initWithView:(UIScrollView *)scrollView;
@end
