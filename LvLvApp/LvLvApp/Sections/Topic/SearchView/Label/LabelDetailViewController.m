//
//  LabelDetailViewController.m
//  LvLvApp
//
//  Created by hope on 15/11/30.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "LabelDetailViewController.h"

#import "labelDetailHeaderView.h"
#import "LableTableViewCell.h"
#import "GambitAnswerViewController.h"

#import "M_labelDataModels.h"
#import "M_lableBaseClass.h"
#import "M_lableData.h"
#import "M_lableListResult.h"
#import "LABLEABaseClass.h"
#import "LABLEAData.h"

#import "AttionBaseClass.h"
#import "AttionData.h"
#import "AttionDataModels.h"

#import "AnswerDetailViewController.h"
#import "GambitAnswerViewController.h"
@interface LabelDetailViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *titleImag;
    M_lableBaseClass *labeclass;
    AttionData *attdata;
  
    NSInteger _pageIndex;
  
    CGFloat commentSpace;//赞同和内容之间的距离
    CGFloat topSpace;//顶部和底部的距离
    CGFloat lineSpace;//行间距,这里的行间距是估算的，真实的行间距是3
    CGFloat doubleLineHeight1;//动态标题两行高度
    CGFloat doubleLineHeight2;//动态内容两行高度
    CGFloat doubleLineHeight3;//热门内容三行高度
    
    M_lableListResult *listdata ;
    UILabel *contentLabel;

}
@property (nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation LabelDetailViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    commentSpace = 3+2;
    topSpace = 18;
    lineSpace = 5.0f+2;
    doubleLineHeight1 = 40;//
    doubleLineHeight2 = 35;//
    doubleLineHeight3 = 54;//

    self.view.backgroundColor =[UIColor whiteColor];
    [self request:self.Keywordstr];
    self.lalabarrays =[[NSMutableArray alloc]init];
    self.attlabel =[[NSMutableArray alloc]init];
    self.headerView =[[[NSBundle mainBundle]loadNibNamed:@"labelDetailHeaderView" owner:nil options:nil]firstObject];

    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"返回"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    self.navigationItem.leftBarButtonItem = leftItem;
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"分享"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(shareClick)];
//    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self createTitleImage];
    [self createTableView];
}

- (void)shareClick{
    NSArray *imageArray = @[@"微信好友",@"微信朋友圈",@"微博",@"QQ",@"邮件",@"复制链接",@"",@""];
    NSArray *titleArray = @[@"微信好友",@"微信朋友圈",@"微博",@"QQ",@"邮件",@"复制链接",@"",@""];
    titleArray = [Utils isClientInstalled:titleArray];
    imageArray = [Utils isClientInstalled:imageArray];
    NSArray  *editArray=@[];
    NSArray  *editArray1 = @[];
    
    NSString *title = @"分享标签";
    NSString *shareText = self.labName;
    NSString *shareUrl = @"";
    UIImage *shareImage = [[UIImage alloc]init];
    ShareView *share = [[ShareView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];

    share.dictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"imageArr1":imageArray,@"titleArr1":titleArray,@"imageArr2":editArray,@"titleArr2":editArray1,@"title":title,@"shareText":shareText,@"shareUrl":shareUrl,@"shareImage":shareImage,@"userID":self.UserID}];
    share.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [APP.window addSubview:share];
}



- (void)createTitleImage
{
    titleImag = [[UIImageView alloc] initWithFrame:CGRectMake(0, 19, 53, 53)];
    titleImag.image = [UIImage imageNamed:@"标签头"];
    UIView *vi = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 53, 53)];
    [vi addSubview:titleImag];
    self.navigationItem.titleView = vi;
}



#pragma mark -- 创建tableView

- (void)createTableView{
    self.labetableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-20) style:UITableViewStyleGrouped];
    self.labetableView.delegate = self;
    self.labetableView.dataSource = self;
     [self.labetableView setScrollEnabled:YES];
    [self setExtraCellLineHidden:self.labetableView];
    [self.view addSubview:self.labetableView];

}



#pragma mark -- tableview的delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.lalabarrays.count;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
  
    if (indexPath.row <self.lalabarrays.count) {
       listdata=self.lalabarrays[indexPath.row];
      if ([Utils isBlankString:listdata.logoUrl]){//无图片
          CGFloat titleHeight = [Utils textHeightFromTextString:listdata.title width:kWidth-30 fontSize:16];
          if (titleHeight > 35) {
              titleHeight = doubleLineHeight1+lineSpace;
          }
          UILabel *titleLabel = [Utils labelWithFrame:CGRectMake(15, topSpace, kWidth-30, titleHeight) andTextColor:setColor(81, 88, 99) andFont:[UIFont systemFontOfSize:16] andText:listdata.title withNumOfLines:2 withTarget:self andSEL:@selector(contentClick:)];
          [cell.contentView addSubview:titleLabel];
          
          if ([Utils isBlankString:listdata.answerContent]) {
              UILabel *commentLabel = [Utils labelWithFrame:CGRectMake(15, CGRectGetMaxY(titleLabel.frame)+commentSpace, kWidth-30, 15) andTextColor:setColor(171, 171, 171) andFont:[UIFont systemFontOfSize:11] andText:[NSString stringWithFormat:@"赞同 %d · 评论 %d · 收藏 %d",(int)listdata.counts,(int)listdata.commentCount,(int)listdata.collectCount] withNumOfLines:1 withTarget:self andSEL:@selector(contentClick:)] ;
              [cell.contentView addSubview:commentLabel];
          }else{
              CGFloat contentHeight = [Utils textHeightFromTextString:listdata.answerContent width:kWidth-30 fontSize:14];
              if (contentHeight > 40) {
                  contentHeight = doubleLineHeight3+lineSpace;
              }else if (contentHeight > 30){
                  contentHeight = doubleLineHeight1;
              }
              contentLabel = [Utils labelWithFrame:CGRectMake(15, CGRectGetMaxY(titleLabel.frame)+commentSpace, kWidth-30, contentHeight) andTextColor:setColor(122, 122, 122) andFont:[UIFont systemFontOfSize:14] andText:[Utils replaceImageString:listdata.answerContent] withNumOfLines:3 withTarget:(id)self andSEL:@selector(contentClick:)];
              contentLabel.tag = indexPath.row;
              [cell.contentView addSubview:contentLabel];
              UILabel *commentLabel = [Utils labelWithFrame:CGRectMake(15, CGRectGetMaxY(contentLabel.frame)+commentSpace, kWidth-30, 15) andTextColor:setColor(171, 171, 171) andFont:[UIFont systemFontOfSize:11] andText:[NSString stringWithFormat:@"赞同 %d · 评论 %d · 收藏 %d",(int)listdata.counts,(int)listdata.commentCount,(int)listdata.collectCount] withNumOfLines:1 withTarget:(id)self andSEL:@selector(contentClick:)] ;
              [cell.contentView addSubview:commentLabel];
          }
          
        }else{//有图片
            CGFloat titleHeight = [Utils textHeightFromTextString:listdata.title width:kWidth-125 fontSize:16];
            if (titleHeight > 35) {
                titleHeight = doubleLineHeight1+lineSpace;
            }
            UILabel *titleLabel = [Utils labelWithFrame:CGRectMake(15, topSpace, kWidth-125, titleHeight) andTextColor:setColor(81, 88, 99) andFont:[UIFont systemFontOfSize:16] andText:listdata.title withNumOfLines:2 withTarget:self andSEL:@selector(contentClick:)];
            [cell.contentView addSubview:titleLabel];
            CGFloat contentHeight = [Utils textHeightFromTextString:[Utils replaceImageString:listdata.answerContent] width:kWidth-125 fontSize:14];
            if (contentHeight > 40) {
                contentHeight = doubleLineHeight3+lineSpace;
            }
            UILabel *contentLabels = [Utils labelWithFrame:CGRectMake(15, CGRectGetMaxY(titleLabel.frame)+commentSpace, kWidth-125, contentHeight) andTextColor:setColor(122, 122, 122) andFont:[UIFont systemFontOfSize:14] andText:[Utils replaceImageString:listdata.answerContent] withNumOfLines:3 withTarget:self andSEL:@selector(contentClick:)];
            [cell.contentView addSubview:contentLabels];
//            titleLabel.backgroundColor = [UIColor greenColor];
//            contentLabels.backgroundColor = [UIColor redColor];
            UILabel *commentLabel = [Utils labelWithFrame:CGRectMake(15, CGRectGetMaxY(contentLabels.frame)+commentSpace, kWidth-30, 15) andTextColor:setColor(171, 171, 171) andFont:[UIFont systemFontOfSize:11] andText:[NSString stringWithFormat:@"赞同 %d · 评论 %d · 收藏 %d",(int)listdata.counts,(int)listdata.commentCount,(int)listdata.collectCount] withNumOfLines:1 withTarget:(id)nil andSEL:nil];
            [cell.contentView addSubview:commentLabel];
            UIImageView *imageView = [Utils imageWithFrame:CGRectMake(kWidth-95, titleLabel.frame.origin.y, 80, 80) andImageUrl:[NSString stringWithFormat:@"http://%@/%@",K_IP,listdata.logoUrl]];
            [cell.contentView addSubview:imageView];
            }
        }
    return cell;
 }

// 下部列表 赋值
- (void)request:(NSString *)TopicNames
{
    NSString *TopicName =TopicNames;
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*count = [defaults objectForKey:@"users"];
    NSString *userID =[NSString stringWithFormat:@"%@",count];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KlabelParticularUrl,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];

        [param appendString:[NSString stringWithFormat:@"UserID=%@",userID]];
        [param appendString:[NSString stringWithFormat:@"&FuzzyStr=%@",TopicName]];
     request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
     [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
//         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         M_lableBaseClass *label=[[M_lableBaseClass alloc]initWithDictionary:dict];
         labeclass = label;
         NSLog(@"%f",labeclass.data.isfcous);
         [self.lalabarrays addObjectsFromArray:label.data.listResult];
         [self.labetableView  reloadData];
     }];
     }
//头部Viw 赋值
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    self.headerView.frame=CGRectMake(0, 0, self.view.bounds.size.width, 0);
    //问题回答评论
    self.headerView.LablePrAnComment.text =[NSString stringWithFormat:@"问题 %d · 回答 %d · 评论 %d",(int)labeclass.data.totalHeader,(int)labeclass.data.totalAnswer,(int)labeclass.data.totalComment];
    //关注
    [self.headerView.LableAttention setBackgroundImage:[UIImage imageNamed:@"关注-1"] forState:UIControlStateNormal];
    [self.headerView.LableAttention.titleLabel setFont:[ UIFont systemFontOfSize:12.f]];
    
    if (labeclass.data.isfcous == 0) {
        NSString *str =[NSString stringWithFormat:@"   关注   ︱   %d  ",(int)labeclass.data.totalTag];
        [self.headerView.LableAttention setTitle:str forState: UIControlStateNormal];
        [self.headerView.LableAttention setBackgroundImage:[UIImage imageNamed:@"关注-1"] forState:UIControlStateNormal];
        [self.headerView.LableAttention setTitleColor:[UIColor whiteColor] forState: UIControlStateNormal];
         [self.headerView.LableAttention addTarget:self action:@selector(lablecation:)forControlEvents:UIControlEventTouchUpInside];
    }else
    {
        NSString *strr =[NSString stringWithFormat:@"  已关注  ︱   %d  ",(int)labeclass.data.totalTag];
        [self.headerView.LableAttention setTitle:strr forState: UIControlStateNormal];
        [self.headerView.LableAttention setBackgroundImage:[UIImage imageNamed:@"已关注"] forState:UIControlStateNormal];
        [self.headerView.LableAttention setTitleColor:[UIColor blackColor] forState: UIControlStateNormal];
         [self.headerView.LableAttention addTarget:self action:@selector(lablecation:)forControlEvents:UIControlEventTouchUpInside];
    }
    //标签名
    self.headerView.LableNamelabe.text=self.labName;
    //标签总数
    self.headerView.LableAttentionCount.text =[NSString stringWithFormat:@"%d",(int)labeclass.data.totalTag];
    self.AttonCount =(NSInteger)labeclass.data.totalTag;

    return  self.headerView;
}
//关注点击事件
- (void)lablecation:(UIButton *)btn
{
    if (labeclass.data.isfcous == 1)  {

        [self collectRequestWithT:@"0"andBtn:btn];
        
    }else {
        [self collectRequestWithT:@"1"andBtn:btn];

    }
}
- (void)collectRequestWithT:(NSString *)t andBtn:(UIButton *)Btn{
    NSLog(@"%@",t);
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*count = [defaults objectForKey:@"users"];
    
    NSString *userID =[NSString stringWithFormat:@"%@",count];
    NSString *tagName = self.labName;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KattentiontgaUrl,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",userID]];
    [param appendString:[NSString stringWithFormat:@"&TagName=%@",tagName]];
    [param appendString:[NSString stringWithFormat:@"&T=%@",t]];

    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         if ([t isEqualToString:@"1"]) {
             Btn.selected = !Btn.selected;
             NSString *ss =[NSString stringWithFormat:@"  已关注  ︱   %d  ",(int)labeclass.data.totalTag+1];
             NSString *nn =[NSString stringWithFormat:@"   关注   ︱   %d  ",(int)labeclass.data.totalTag];
             NSString *titlestr =Btn.selected?ss:nn;
             [self.headerView.LableAttention setTitleColor: Btn.selected?[UIColor blackColor]:[UIColor whiteColor] forState:UIControlStateNormal];
             [self.headerView.LableAttention setTitle:titlestr forState: UIControlStateNormal ];
             NSString *str =Btn.selected?@"已关注":@"关注-1";
             [self.headerView.LableAttention setBackgroundImage:[UIImage imageNamed:str] forState:UIControlStateNormal];
             
         }else{
             Btn.selected = !Btn.selected;
             NSString *ss =[NSString stringWithFormat:@"  关注  |  %d  ",(int)labeclass.data.totalTag-1];
             NSString *nn =[NSString stringWithFormat:@" 已关注  |  %d  ",(int)labeclass.data.totalTag];
             NSString *titlestr =Btn.selected?ss:nn;
             [self.headerView.LableAttention setTitleColor: Btn.selected?[UIColor whiteColor]:[UIColor blackColor] forState:UIControlStateNormal];
             [self.headerView.LableAttention setTitle:titlestr forState: UIControlStateNormal ];
             NSString *str =Btn.selected?@"关注-1":@"已关注";
             [self.headerView.LableAttention setBackgroundImage:[UIImage imageNamed:str] forState:UIControlStateNormal];
         
         
         }
        
}];
    }
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 185;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    LableTableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    cell.selected =NO;
      if (self.lalabarrays.count > indexPath.row) {
    listdata =[self.lalabarrays objectAtIndex:indexPath.row];
   
    GambitAnswerViewController *GambitAnswer =[[GambitAnswerViewController alloc]init];

    GambitAnswer.UserID =listdata.tUserID;
  //GambitAnswer.tUserID =listdata.tUserID;
    GambitAnswer.TID =listdata.tID;
    GambitAnswer.Answer_ID =listdata.answerID;
    [self.navigationController pushViewController:GambitAnswer animated:YES];
}
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat totalHeight = topSpace+15;
    if (indexPath.row < self.lalabarrays.count) {
        listdata =self.lalabarrays[indexPath.row];
            //无图片
            if ([Utils isBlankString:listdata.logoUrl]){
                CGFloat titleHeight = [Utils textHeightFromTextString:listdata.title width:kWidth-30 fontSize:16];
                if (titleHeight > 35) {
                    titleHeight = doubleLineHeight2+lineSpace;
                }
                totalHeight += titleHeight+commentSpace;
                if (listdata.answerContent==nil ||[listdata.answerContent isEqual:@""]) {
                }else{
                    CGFloat contentHeight = [Utils textHeightFromTextString:listdata.answerContent width:kWidth-30 fontSize:14];
                    if (contentHeight > 30) {
                        contentHeight = doubleLineHeight2+lineSpace+15;
                    }
                    totalHeight += contentHeight+commentSpace;
                }
                totalHeight += 15;
                
            }else{//有图片
                CGFloat titleHeight = [Utils textHeightFromTextString:listdata.title width:kWidth-125 fontSize:16];
                if (titleHeight > 35) {
                    titleHeight = doubleLineHeight2+lineSpace;
                }
                totalHeight += titleHeight +commentSpace;
                if (listdata.answerContent==nil ||[listdata.answerContent isEqual:@""]) {
                }else{
                    CGFloat contentHeight = [Utils textHeightFromTextString:[Utils replaceImageString:listdata.answerContent] width:kWidth-125 fontSize:14];
                    if (contentHeight > 30) {
                        contentHeight = doubleLineHeight2+lineSpace;
                    }
                    totalHeight += contentHeight +commentSpace;
                }
                
                totalHeight += 15+15;
            }
        }
    
    return totalHeight;

}

- (void)contentClick:(MyTapGestureRecognizer *)tap{
   // NSInteger tag =tap.label.tag;
    
    UIView *view = [tap.label superview];
    UITableViewCell *cell = (UITableViewCell *)[view superview];
    NSIndexPath *indexPath = [self.labetableView indexPathForCell:cell];
    if (self.lalabarrays.count > indexPath.row) {
        listdata =self.lalabarrays [indexPath.row];
    GambitAnswerViewController *GambitAnswer =[[GambitAnswerViewController alloc]init];
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    NSString *usr =[NSString stringWithFormat:@"%@",UserID];
    GambitAnswer.UserID =usr;
    //GambitAnswer.tUserID =listdata.tUserID;
    GambitAnswer.TID =listdata.tID;
    GambitAnswer.Answer_ID =listdata.answerID;
    [self.navigationController pushViewController:GambitAnswer animated:YES];
}
}
- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    [self.labetableView  scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat scal = scrollView.contentOffset.y;
    if (scal > 26) {
        scal = 26;
    }
    if (scal <= 0) {
        scal = 0;
    }
    //    NSLog(@"%f",scrollView.contentOffset.y);
    titleImag.frame = CGRectMake(26.5 - (53-scal)/2.0, 19,53 - scal,53 - scal);
}

- (void)backClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [UIView new];
    
    view.backgroundColor = [UIColor clearColor];
    
    [tableView setTableFooterView:view];
}
-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    if([self numberOfSectionsInTableView:tableView]==(section+1)){
        return [UIView new];
    }
    return nil;
}


@end




