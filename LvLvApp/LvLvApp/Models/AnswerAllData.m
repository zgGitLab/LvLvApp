//
//  AnswerAllData.m
//
//  Created by hope  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "AnswerAllData.h"
#import "AnswerAllDt.h"
#import "AnswerAllList.h"


NSString *const kAnswerAllDataDt = @"dt";
NSString *const kAnswerAllDataDscrption = @"dscrption";
NSString *const kAnswerAllDataTitle = @"title";
NSString *const kAnswerAllDataFocusID = @"FocusID";
NSString *const kAnswerAllDataAnswerID = @"AnswerID";
NSString *const kAnswerAllDataAnswerTotalCount = @"AnswerTotalCount";
NSString *const kAnswerAllDataAnswerContent = @"AnswerContent";
NSString *const kAnswerAllDataList = @"list";
NSString *const kAnswerAllDataState = @"state";
NSString *const kAnswerAllDataQuestionContent = @"QuestionContent";
NSString *const kAnswerAllDataCategory = @"Category";
NSString *const kAnswerAllDataContent = @"content";

@interface AnswerAllData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AnswerAllData

@synthesize dt = _dt;
@synthesize dscrption = _dscrption;
@synthesize title = _title;
@synthesize focusID = _focusID;
@synthesize answerID = _answerID;
@synthesize answerTotalCount = _answerTotalCount;
@synthesize questionContent = _questionContent;
@synthesize list = _list;
@synthesize state = _state;
@synthesize category = _category;
@synthesize content = _content;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedAnswerAllDt = [dict objectForKey:kAnswerAllDataDt];

    self.dt = [NSArray arrayWithArray:(NSArray *)receivedAnswerAllDt];
            self.dscrption = [self objectOrNilForKey:kAnswerAllDataDscrption fromDictionary:dict];
            self.title = [self objectOrNilForKey:kAnswerAllDataTitle fromDictionary:dict];
            self.focusID = [self objectOrNilForKey:kAnswerAllDataFocusID fromDictionary:dict];
            self.answerID = [self objectOrNilForKey:kAnswerAllDataAnswerID fromDictionary:dict];
            self.answerTotalCount = [[self objectOrNilForKey:kAnswerAllDataAnswerTotalCount fromDictionary:dict] doubleValue];
            self.questionContent = [self objectOrNilForKey:kAnswerAllDataQuestionContent fromDictionary:dict];
            self.category = [NSString stringWithFormat:@"%@",[self objectOrNilForKey:kAnswerAllDataCategory fromDictionary:dict]];
            self.content = [NSString stringWithFormat:@"%@",[self objectOrNilForKey:kAnswerAllDataContent fromDictionary:dict]];
        
    NSObject *receivedAnswerAllList = [dict objectForKey:kAnswerAllDataList];
    NSMutableArray *parsedAnswerAllList = [NSMutableArray array];
    if ([receivedAnswerAllList isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedAnswerAllList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedAnswerAllList addObject:[AnswerAllList modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedAnswerAllList isKindOfClass:[NSDictionary class]]) {
       [parsedAnswerAllList addObject:[AnswerAllList modelObjectWithDictionary:(NSDictionary *)receivedAnswerAllList]];
    }

    self.list = [NSArray arrayWithArray:parsedAnswerAllList];
            self.state = [self objectOrNilForKey:kAnswerAllDataState fromDictionary:dict];

    }
    
    return self;
    
}

//- (NSDictionary *)dictionaryRepresentation
//{
//    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
//    NSMutableArray *tempArrayForDt = [NSMutableArray array];
//    for (NSObject *subArrayObject in self.dt) {
//        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
//            // This class is a model object
//            [tempArrayForDt addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
//        } else {
//            // Generic object
//            [tempArrayForDt addObject:subArrayObject];
//        }
//    }
//    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDt] forKey:kAnswerAllDataDt];
//    [mutableDict setValue:self.dscrption forKey:kAnswerAllDataDscrption];
//    [mutableDict setValue:self.title forKey:kAnswerAllDataTitle];
//    [mutableDict setValue:self.focusID forKey:kAnswerAllDataFocusID];
//    [mutableDict setValue:self.answerID forKey:kAnswerAllDataAnswerID];
//    [mutableDict setValue:[NSNumber numberWithDouble:self.answerTotalCount] forKey:kAnswerAllDataAnswerTotalCount];
//    [mutableDict setValue:self.questionContent forKey:kAnswerAllDataQuestionContent];
//    NSMutableArray *tempArrayForList = [NSMutableArray array];
//    for (NSObject *subArrayObject in self.list) {
//        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
//            // This class is a model object
//            [tempArrayForList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
//        } else {
//            // Generic object
//            [tempArrayForList addObject:subArrayObject];
//        }
//    }
//    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForList] forKey:kAnswerAllDataList];
//    [mutableDict setValue:self.state forKey:kAnswerAllDataState];
//
//    return [NSDictionary dictionaryWithDictionary:mutableDict];
//}

//- (NSString *)description 
//{
//    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
//}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

//
//#pragma mark - NSCoding Methods
//
//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    self = [super init];
//
//    self.dt = [aDecoder decodeObjectForKey:kAnswerAllDataDt];
//    self.dscrption = [aDecoder decodeObjectForKey:kAnswerAllDataDscrption];
//    self.title = [aDecoder decodeObjectForKey:kAnswerAllDataTitle];
//    self.focusID = [aDecoder decodeObjectForKey:kAnswerAllDataFocusID];
//    self.answerID = [aDecoder decodeObjectForKey:kAnswerAllDataAnswerID];
//    self.answerTotalCount = [aDecoder decodeDoubleForKey:kAnswerAllDataAnswerTotalCount];
//    self.questionContent = [aDecoder decodeObjectForKey:kAnswerAllDataQuestionContent];
//    self.list = [aDecoder decodeObjectForKey:kAnswerAllDataList];
//    self.state = [aDecoder decodeObjectForKey:kAnswerAllDataState];
//    return self;
//}
//
//- (void)encodeWithCoder:(NSCoder *)aCoder
//{
//
//    [aCoder encodeObject:_dt forKey:kAnswerAllDataDt];
//    [aCoder encodeObject:_dscrption forKey:kAnswerAllDataDscrption];
//    [aCoder encodeObject:_title forKey:kAnswerAllDataTitle];
//    [aCoder encodeObject:_focusID forKey:kAnswerAllDataFocusID];
//    [aCoder encodeObject:_answerID forKey:kAnswerAllDataAnswerID];
//    [aCoder encodeDouble:_answerTotalCount forKey:kAnswerAllDataAnswerTotalCount];
//    [aCoder encodeObject:_questionContent forKey:kAnswerAllDataQuestionContent];
//    [aCoder encodeObject:_list forKey:kAnswerAllDataList];
//    [aCoder encodeObject:_state forKey:kAnswerAllDataState];
//}
//
//- (id)copyWithZone:(NSZone *)zone
//{
//    AnswerAllData *copy = [[AnswerAllData alloc] init];
//    
//    if (copy) {
//
//        copy.dt = [self.dt copyWithZone:zone];
//        copy.dscrption = [self.dscrption copyWithZone:zone];
//        copy.title = [self.title copyWithZone:zone];
//        copy.focusID = [self.focusID copyWithZone:zone];
//        copy.answerID = [self.answerID copyWithZone:zone];
//        copy.answerTotalCount = self.answerTotalCount;
//        copy.questionContent = [self.questionContent copyWithZone:zone];
//        copy.list = [self.list copyWithZone:zone];
//        copy.state = [self.state copyWithZone:zone];
//    }
//    
//    return copy;
//}


@end
