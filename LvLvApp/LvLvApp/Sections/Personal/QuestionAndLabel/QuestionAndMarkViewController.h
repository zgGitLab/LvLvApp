//
//  QuestionAndMarkViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDAttentionData.h"
#import "IDAttentionBaseClass.h"

#import "IDLablelaBaseClass.h"
#import "IDLablelaData.h"
#import "M_AttionsDataModels.h"
#import "M_AttionsData.h"
@interface QuestionAndMarkViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *myTable;
@property (strong, nonatomic) NSMutableArray *IdIssueArray;//问题的数组
@property (strong, nonatomic) NSMutableArray *IdLabelArray;//标签的数组
@property (strong, nonatomic) M_AttionsData *iddata;
@property (strong, nonatomic) IDLablelaData *labeldata;

@end
