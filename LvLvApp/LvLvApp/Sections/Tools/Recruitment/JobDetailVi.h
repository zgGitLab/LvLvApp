//
//  JobDetailVi.h
//  LvLvApp
//
//  Created by Sunny on 16/3/30.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface JobDetailVi : UIView

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *jobName;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *salary;
@property (weak, nonatomic) IBOutlet UILabel *experience;
@property (weak, nonatomic) IBOutlet UILabel *Location;


- (JobDetailVi *)loadVi;

- (void)getAllDataWithModel:(JbDetailModel *)Model;

@end
