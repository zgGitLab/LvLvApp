//
//  S_MYAskData.h
//
//  Created by IOS8  on 15/12/16
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_MYAskData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double userID;
@property (nonatomic, assign) double answerCount;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double tagCount;
@property (nonatomic, strong) NSString *tID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
