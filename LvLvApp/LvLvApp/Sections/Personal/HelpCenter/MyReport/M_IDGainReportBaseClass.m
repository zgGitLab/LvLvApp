//
//  M_IDGainReportBaseClass.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_IDGainReportBaseClass.h"
#import "M_IDGainReportData.h"


NSString *const kM_IDGainReportBaseClassCode = @"Code";
NSString *const kM_IDGainReportBaseClassMessage = @"Message";
NSString *const kM_IDGainReportBaseClassData = @"Data";


@interface M_IDGainReportBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_IDGainReportBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_IDGainReportBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_IDGainReportBaseClassMessage fromDictionary:dict];
    NSObject *receivedM_IDGainReportData = [dict objectForKey:kM_IDGainReportBaseClassData];
    NSMutableArray *parsedM_IDGainReportData = [NSMutableArray array];
    if ([receivedM_IDGainReportData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedM_IDGainReportData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedM_IDGainReportData addObject:[M_IDGainReportData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedM_IDGainReportData isKindOfClass:[NSDictionary class]]) {
       [parsedM_IDGainReportData addObject:[M_IDGainReportData modelObjectWithDictionary:(NSDictionary *)receivedM_IDGainReportData]];
    }

    self.data = [NSArray arrayWithArray:parsedM_IDGainReportData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_IDGainReportBaseClassCode];
    [mutableDict setValue:self.message forKey:kM_IDGainReportBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kM_IDGainReportBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_IDGainReportBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_IDGainReportBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_IDGainReportBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_IDGainReportBaseClassCode];
    [aCoder encodeObject:_message forKey:kM_IDGainReportBaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_IDGainReportBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_IDGainReportBaseClass *copy = [[M_IDGainReportBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
