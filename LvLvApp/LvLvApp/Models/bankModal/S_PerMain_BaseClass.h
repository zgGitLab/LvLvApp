//
//  S_PerMain_BaseClass.h
//
//  Created by IOS8  on 15/12/7
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_PerMain_BaseClass : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *qQ;
@property (nonatomic, strong) NSString *telephone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *introduction;
@property (nonatomic, strong) NSString *regDT;
@property (nonatomic, assign) double createID;
@property (nonatomic, strong) NSString *modiyDT;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *wXID;
@property (nonatomic, assign) double status;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *createDT;
@property (nonatomic, assign) double modiyID;
@property (nonatomic, assign) double isVIP;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
