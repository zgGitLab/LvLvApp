//
//  GambitTableViewController.h
//  法律app
//
//  Created by hope on 15/9/29.
//  Copyright (c) 2015年 mark.ma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GambithHeaderView.h"
#import "UIImageView+WebCache.h"

@interface TopicHomeViewController : BaseViewController< UISearchBarDelegate,GambitSearchViewDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) NSMutableArray *dataArray;//热门数据
@property (nonatomic,strong) NSMutableArray *dataArr;//动态数据
@property (nonatomic,strong) UITableView *tableView;


@property (nonatomic,assign) NSInteger *totalAnswerNum;//回答数据
@property (nonatomic,strong) NSString *titleName;//标题
@property (nonatomic,strong) NSString *contentText;//内容
@property (nonatomic,strong) NSString *UserID;//用户编号
@property (nonatomic,strong) NSString *TID;//问题编号

@end
