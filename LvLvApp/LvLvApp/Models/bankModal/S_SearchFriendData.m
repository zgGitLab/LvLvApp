//
//  S_SearchFriendData.m
//
//  Created by IOS8  on 15/12/22
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_SearchFriendData.h"


NSString *const kS_SearchFriendDataID = @"ID";
NSString *const kS_SearchFriendDataIntroduction = @"Introduction";
NSString *const kS_SearchFriendDataIsFocus = @"IsFocus";
NSString *const kS_SearchFriendDataImage = @"Image";
NSString *const kS_SearchFriendDataNickName = @"NickName";


@interface S_SearchFriendData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_SearchFriendData

@synthesize iDProperty = _iDProperty;
@synthesize introduction = _introduction;
@synthesize isFocus = _isFocus;
@synthesize image = _image;
@synthesize nickName = _nickName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kS_SearchFriendDataID fromDictionary:dict] doubleValue];
            self.introduction = [self objectOrNilForKey:kS_SearchFriendDataIntroduction fromDictionary:dict];
            self.isFocus = [[self objectOrNilForKey:kS_SearchFriendDataIsFocus fromDictionary:dict] doubleValue];
            self.image = [self objectOrNilForKey:kS_SearchFriendDataImage fromDictionary:dict];
            self.nickName = [self objectOrNilForKey:kS_SearchFriendDataNickName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kS_SearchFriendDataID];
    [mutableDict setValue:self.introduction forKey:kS_SearchFriendDataIntroduction];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isFocus] forKey:kS_SearchFriendDataIsFocus];
    [mutableDict setValue:self.image forKey:kS_SearchFriendDataImage];
    [mutableDict setValue:self.nickName forKey:kS_SearchFriendDataNickName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kS_SearchFriendDataID];
    self.introduction = [aDecoder decodeObjectForKey:kS_SearchFriendDataIntroduction];
    self.isFocus = [aDecoder decodeDoubleForKey:kS_SearchFriendDataIsFocus];
    self.image = [aDecoder decodeObjectForKey:kS_SearchFriendDataImage];
    self.nickName = [aDecoder decodeObjectForKey:kS_SearchFriendDataNickName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kS_SearchFriendDataID];
    [aCoder encodeObject:_introduction forKey:kS_SearchFriendDataIntroduction];
    [aCoder encodeDouble:_isFocus forKey:kS_SearchFriendDataIsFocus];
    [aCoder encodeObject:_image forKey:kS_SearchFriendDataImage];
    [aCoder encodeObject:_nickName forKey:kS_SearchFriendDataNickName];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_SearchFriendData *copy = [[S_SearchFriendData alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.introduction = [self.introduction copyWithZone:zone];
        copy.isFocus = self.isFocus;
        copy.image = [self.image copyWithZone:zone];
        copy.nickName = [self.nickName copyWithZone:zone];
    }
    
    return copy;
}


@end
