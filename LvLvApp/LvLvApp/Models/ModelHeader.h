//
//  ModelHeader.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/25.
//  Copyright © 2016年 gyy. All rights reserved.
//

#ifndef ModelHeader_h
#define ModelHeader_h

#import "BaseDataModel.h"
/** 案例和发条的Model */
#import "LawModel.h"
#import "ItemsDetailModel.h"
#import "FilterGroupsDetailModel.h"

#endif /* ModelHeader_h */
