//
//  EditJobInfoViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/2.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "S_PerBaseInfoJobList.h"
@interface EditJobInfoViewController : BaseViewController

@property(assign, nonatomic)S_PerBaseInfoJobList *jobModal;
@property (assign, nonatomic) BOOL ishide;
@property (weak, nonatomic) IBOutlet UITextField *companyName;
@property (weak, nonatomic) IBOutlet UITextField *jobName;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UIButton *areaBtn;
@property (weak, nonatomic) IBOutlet UIButton *removeBtn;

@end
