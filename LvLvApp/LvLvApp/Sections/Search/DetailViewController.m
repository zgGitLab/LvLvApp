//
//  DetailViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/4.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "DetailViewController.h"
#import "ShareView.h"
#import "LoginViewController.h"

@interface DetailViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    
    BOOL isHidde;/** 好的*/
    CGFloat _contentHeight;//文章高度
    int tempID;
    CGFloat headerHigth;
    CGFloat cellHigth;
    BOOL isCollection;
    NSString *sharTitle;
    NSString *context;
    
    UIButton *_indexItem;//目录按钮
    UIButton *_topBtn;//回到顶部按钮
    UIButton *_searchBtn;//搜索按钮
    UIView *_searchView;//搜索view
    UITextField *_searchBar;
    UITableView *_indexTableView;//目录列表
    NSInteger _index;//高亮关键字所在的范围索引
    NSDictionary *_attrDic;//原始属性
    UIView *_hideView;//遮罩view
    NSString *_contentText;//文本内容
    UILabel *_numLabel;
    
    S_CaseDetailSelectVerdicts *_CaseDataModal;//法条
    S_LawDetailSelectArticles  *_lawDataModal;//案例
}
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //初始化高亮关键字索引
    _index = 0;
    
    self.contentView.editable = NO;
    UITapGestureRecognizer *tap  = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchClick:)];
    [self.contentView addGestureRecognizer:tap];
    [self.contentView addSubview:self.detailHeaderView];
    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
    [self caseAndLawTitleTextWithType:self.type -1];
    tempID = [self.IDArr[self.iDProperty] intValue];
    if (self.userID == nil) {
        self.userID = [[self getUserId] isEqualToString:@"(null)"]?@"0":[self getUserId];
    }

    [self requestData];
    [self addTopButton];
    
    //增加键盘监听
    [self addKeyboardObserve];
}

//添加键盘监听
- (void)addKeyboardObserve{
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    //注册键盘消失的通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    //键盘高度
    CGRect keyBoardFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey]CGRectValue];
    CGFloat height = keyBoardFrame.size.height;
    NSLog(@"%f",height);
    
    //修改搜索框和另外3个图片的高度
    CGRect frame = _topBtn.frame;
    _topBtn.frame = CGRectMake(frame.origin.x, frame.origin.y-height, frame.size.width, frame.size.height);
    frame = _searchBtn.frame;
    _searchBtn.frame = CGRectMake(frame.origin.x, frame.origin.y-height, frame.size.width, frame.size.height);
    frame = _searchView.frame;
    _searchView.frame = CGRectMake(frame.origin.x, frame.origin.y-height, frame.size.width, frame.size.height);
    if (_indexItem) {
        frame = _indexItem.frame;
        _indexItem.frame = CGRectMake(frame.origin.x, frame.origin.y-height, frame.size.width, frame.size.height);
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    //键盘高度
    CGRect keyBoardFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey]CGRectValue];
    CGFloat height = keyBoardFrame.size.height;
    NSLog(@"%f",height);
    
    //修改搜索框和另外3个图片的高度
    CGRect frame = _topBtn.frame;
    _topBtn.frame = CGRectMake(frame.origin.x, kHeight - 100, frame.size.width, frame.size.height);
    
    frame = _searchView.frame;
    _searchView.frame = CGRectMake(frame.origin.x, kHeight-44, frame.size.width, frame.size.height);
    if (_indexItem) {
        frame = _searchBtn.frame;
        _searchBtn.frame = CGRectMake(frame.origin.x, kHeight - 200, frame.size.width, frame.size.height);
        frame = _indexItem.frame;
        _indexItem.frame = CGRectMake(frame.origin.x, kHeight - 150, frame.size.width, frame.size.height);
    }else{
        frame = _searchBtn.frame;
        _searchBtn.frame = CGRectMake(frame.origin.x, kHeight - 150, frame.size.width, frame.size.height);
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)touchClick:(UITapGestureRecognizer *)tap{
    [self.view endEditing:YES];
    [self removeIndexList];//移除目录
}

#pragma mark --根据type给法条或案例header赋值

- (void)caseAndLawTitleTextWithType:(int)aType
{
    NSArray *arr = @[@[@"发布时间:",@"生效日期:",@"类       别:",@"发文字号:",@"时  效  性:"],@[@"审理法院:",@"案件类型:",@"文件性质:",@"案       由:",@"审理程序:"]];
    NSArray *strTit = arr[aType];
    self.label1.text = strTit[0];
    self.label2.text = strTit[1];
    self.label3.text = strTit[2];
    self.label4.text = strTit[3];
    self.label5.text = strTit[4];
}

- (void)requestData
{
    _hideView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kWidth, kHeight-104)];
    _hideView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_hideView];
    __weak DetailViewController *VC = self;
    if (self.type == 2) {
         [MBProgressHUD showMessage:@"正在请求!" toView:self.view];
        NSLog(@"案例");
        [RequestManager detailPageDataWithUrl:tempID andUserID:self.userID andBlock:^(S_CaseDetailBaseClass *caseModal,NSString *info) {

            [MBProgressHUD hideHUDForView:VC.view];
            if (![info isEqualToString:@"请求成功"]) {
                tempID = VC.iDProperty;
                return;
            }
            isCollection = caseModal.isCollect == 0?NO:YES;
            [VC setCollectBtnImage:isCollection];
            _CaseDataModal = [caseModal.ds.selectVerdicts objectAtIndex:0];

            if (_CaseDataModal) {
                [VC performSelectorOnMainThread:@selector(refreshData:) withObject:_CaseDataModal waitUntilDone:YES];
            }

        }];
        
    }else{
        [MBProgressHUD showMessage:@"正在请求!" toView:VC.view ];
        
        //你的接口地址
        NSString *url = [NSString stringWithFormat:KCaseOfLawDetail,K_IP];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        [parameters setObject:VC.userID forKey:@"UserID"];
        [parameters setObject:[NSString stringWithFormat:@"%.0f",(double)tempID] forKey:@"ID"];

        [LVHTTPRequestTool post:url params:parameters success:^(id json) {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingAllowFragments error:nil];
            if ([dic[@"Code"] isEqualToNumber:@200]) {
                S_LawDetailBaseClass *baseClass = [[S_LawDetailBaseClass alloc] initWithDictionary:dic];
                S_LawDetailData *lawModel = baseClass.data;
                isCollection = lawModel.isCollect == 0?NO:YES;
                [VC setCollectBtnImage:isCollection];
                _lawDataModal = [lawModel.ds.selectArticles objectAtIndex:0];
                
                if (_lawDataModal) {
                    [VC performSelectorOnMainThread:@selector(lawRefreshData:) withObject:_lawDataModal waitUntilDone:YES];
                }
                [self addIndexItem];
            }else{
                tempID = VC.iDProperty;
                [MBProgressHUD showError:dic[@"Message"]];
            }
        } failureMessage:^(NSString *errorMessage) {
            [MBProgressHUD showError:errorMessage];
        }];
    }
}


#pragma mark -- 法条数据刷新

- (void)lawRefreshData:(S_LawDetailSelectArticles *)lawDataModal
{
    _contentText = lawDataModal.body;
    [self caseAndLawTitleTextWithType:self.type -1];
    
    self.titleLable.text = lawDataModal.title;       // 标题
    self.faYuan.text = lawDataModal.publishDep;      // 发文机关
    NSArray *pubDateArr= [lawDataModal.pubDate componentsSeparatedByString:@" "];
    NSArray *effectiveDateArr= [lawDataModal.effectiveDate componentsSeparatedByString:@" "];
    self.shenLiFaYuan.text = pubDateArr[0];     // 发布时间
    self.eventStal.text = effectiveDateArr[0];        // 生效日期
    self.bookProperty.text = lawDataModal.category;     // 类别
    self.cause.text = lawDataModal.publishNum; // 发文字号
    self.procedure.text = lawDataModal.effectiveness;  // 时效性
    context = lawDataModal.body;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;// 字体的行间距
    NSInteger fontSize = [SingeData sharedInstance].answerContentSize;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName:[UIColor colorWithRed:122/255.0 green:122/255.0 blue:122/255.0 alpha:1]};
    _attrDic = attributes;
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:lawDataModal.body attributes:attributes];
    self.contentView.attributedText= [self highlightKeyWords:attStr];
    
    _contentHeight = [attStr boundingRectWithSize:CGSizeMake(kWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size.height;
    [self changeFramWith:YES];
}


#pragma mark -- 案例数据刷新

- (void)refreshData:(S_CaseDetailSelectVerdicts *)modal
{
    
    [self caseAndLawTitleTextWithType:self.type -1];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;// 字体的行间距
    NSInteger fontSize = [SingeData sharedInstance].answerContentSize;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle};
    
    self.titleLable.text = modal.title;       // 标题
    self.faYuan.text = modal.court;           // 法院
    self.shenLiFaYuan.text = modal.court;     // 审理法院
    self.eventStal.text = modal.category;        // 案件类型
    self.bookProperty.text = modal.character;     // 文件性质
    self.cause.text = modal.reason;            // 案由
    self.procedure.text = modal.level;        // 审理程序
    context = modal.content;
    NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:modal.content attributes:attributes];
    self.contentView.attributedText= attStr;
    self.contentView.textColor = [UIColor colorWithRed:122/255.0 green:122/255.0 blue:122/255.0 alpha:1];
    _contentHeight = [attStr boundingRectWithSize:CGSizeMake(kWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size.height;
    [self changeFramWith:NO];
}


#pragma mark -- 计算frame

- (void)changeFramWith:(BOOL)law
{
    // 计算标题的高度
    CGSize size = [self.titleLable.text boundingRectWithSize:CGSizeMake(SIZE.width-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:21]} context:nil].size;
    self.titleLable.frame = CGRectMake(15, self.titleLable.frame.origin.y, SIZE.width-30, size.height);
    self.bgView1.frame = CGRectMake(0, 10, SIZE.width, size.height+35);
    self.faYuan.frame = CGRectMake(15, CGRectGetMaxY(self.titleLable.frame)+5, SIZE.width-30, 15);
    self.bgView2.frame = CGRectMake(0, CGRectGetMaxY(self.bgView1.frame), SIZE.width, 160);
    headerHigth = CGRectGetMaxY(self.bgView2.frame);
    self.detailHeaderView.frame = CGRectMake(0, 44, SIZE.width, headerHigth);
    
    self.contentView.textContainerInset = UIEdgeInsetsMake(headerHigth+44+7, 10, 20, 10);
    self.contentView.frame = CGRectMake(0, 0, kWidth, kHeight-44);
    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
//        
//        CGRect textFrame=[[self.contentView layoutManager]usedRectForTextContainer:[self.contentView textContainer]];
//        _contentHeight = textFrame.size.height;
//        
//    }else {
//        
//        _contentHeight = self.contentView.contentSize.height;
//    }
//    self.contentView.contentSize = CGSizeMake(kWidth, _contentHeight);
    if (law) {
        [self.contentView scrollRangeToVisible:NSMakeRange(0, self.contentView.text.length)];
        [self performSelector:@selector(scrollToTop) withObject:nil afterDelay:0.4];
    }else {
        [_hideView removeFromSuperview];
    }
}


- (void)scrollToTop{
    [self.contentView setContentOffset:CGPointMake(0, -20)];
    [_hideView removeFromSuperview];
    [MBProgressHUD hideHUDForView:self.view];
}


- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (scrollView == _indexTableView) {
        
    }else{
        if (velocity.y > 0.1 && isHidde == NO) {// 上滑
            
            NSLog(@"向上滑动了");
            [self changeTitleVIewAndTabBarFram];
            isHidde = !isHidde;
            
        }else if(velocity.y < -0.1 && isHidde == YES){
            NSLog(@"向下滑动了");
            [self changeTitleVIewAndTabBarFram];
            isHidde = !isHidde;
        }
        [self removeIndexList];
    }
}

- (void)changeTitleVIewAndTabBarFram
{
//    __weak DetailViewController *VC = self;
//    [UIView animateWithDuration:.3 animations:^{
//        
//        if (isHidde == NO) {
//            VC.titleView.frame = CGRectMake(0, -44,VC.titleView.frame.size.width ,VC.titleView.frame.size.height);
//            VC.bottomView.frame = CGRectMake(0, VC.bottomView.frame.origin.y + 44,VC.bottomView.frame.size.width ,VC.bottomView.frame.size.height);
//            
//        }else{
//            
//            VC.titleView.frame = CGRectMake(0, 0,VC.titleView.frame.size.width ,VC.titleView.frame.size.height);
//            VC.bottomView.frame = CGRectMake(0, VC.bottomView.frame.origin.y - 44,VC.bottomView.frame.size.width ,VC.bottomView.frame.size.height);
//        }
//
//    } completion:^(BOOL finished) {
//        
//    }];
}

#pragma mark 分享

- (IBAction)shareBtn:(UIButton *)sender {
    
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        NSArray *imageArray   = @[@"微信好友",@"微信朋友圈",@"印象笔记",@"有道云笔记",@"微博",@"QQ",@"邮件",@"复制链接"];
        NSArray *titleArray   = @[@"微信好友",@"微信朋友圈",@"印象笔记",@"有道云笔记",@"微博",@"QQ",@"邮件",@"复制链接"];
        titleArray            = [Utils isClientInstalled:titleArray];
        imageArray            = [Utils isClientInstalled:imageArray];
        NSArray  *editArray = @[@"字体大小",@"",@"",@""];
        NSArray  *editArray1 = @[@"字体大小",@"",@"",@""];
        
        NSString *title       = @"分享";
        NSString *shareTitle   = self.titleLable.text;
        int Category = self.type == 2?0:1;
        NSString *laws = self.type == 2?@"案例":@"法条";
        NSString *shareText = [NSString stringWithFormat:@"分享%@《%@》http://%@/mobile/DetailPage?category=%d&id=%d (分享自@律律App)",k_SHAREIP,laws,shareTitle,Category,tempID];

        NSString *shareUrl    = [NSString stringWithFormat:@"http://%@/mobile/DetailPage?category=%d&id=%d",k_SHAREIP,Category,tempID];

        UIImage *shareImage = [UIImage imageNamed:@"logos"];
        ShareView *share      = [[ShareView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
        share.tag             = 1000;
        share.dictionary      = [NSMutableDictionary dictionaryWithDictionary:@{@"imageArr1":imageArray,@"titleArr1":titleArray,@"imageArr2":editArray,@"titleArr2":editArray1,@"shareTitle":shareTitle,@"title":title,@"shareText":shareText,@"shareUrl":shareUrl,@"shareImage":shareImage,@"userID":self.userID}];
        
        share.noteDictionary  = [NSMutableDictionary dictionaryWithDictionary:@{@"imageArr1":imageArray,@"titleArr1":titleArray,@"imageArr2":editArray,@"titleArr2":editArray1,@"shareTitle":shareTitle,@"title":title,@"shareText":context,@"shareUrl":shareUrl,@"shareImage":shareImage,@"userID":self.userID}];
        
        share.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshUIs) name:@"refresh" object:nil];
        AppDelegate *app      = [UIApplication sharedApplication].delegate;
        [app.window addSubview:share];
    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }
}


- (void)refreshUIs{
    if (self.type == 2) {//案例
        [self performSelectorOnMainThread:@selector(refreshData:) withObject:_CaseDataModal waitUntilDone:YES];
    }else {//法条
        [self performSelectorOnMainThread:@selector(lawRefreshData:) withObject:_lawDataModal waitUntilDone:YES];
    }
}

- (IBAction)backBtn:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

// 收藏
- (IBAction)collectBtn:(UIButton *)sender {
    
//    NSString *userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"users"];
    if ([self.userID isEqualToString:@"0"]) {
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }else{
        if (self.type == 2) {
             NSLog(@"案例");
            if (isCollection) {
                [self collectionCaseWithLawOrCase:0 andIsCollect:0];
            }else{
                [self collectionCaseWithLawOrCase:0 andIsCollect:1];
            }
            
        }else{
             NSLog(@"法条");
            if (isCollection) {
                [self collectionLawWithLawOrCase:1 andIsCollect:0];
            }else{
                [self collectionLawWithLawOrCase:1 andIsCollect:1];
            }
            
        }
    }
    
}

- (void)setCollectBtnImage:(BOOL)isCollect
{
    NSString *imageName = isCollection?@"收藏2":@"收藏1";
    [self.collectionBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}
/**
 *  法条的收藏
 *
 *  @param isLaw   1表示法条0表示案例
 *  @param collect 1表示收藏0表示取消收藏
 */
- (void)collectionLawWithLawOrCase:(int)isLaw andIsCollect:(int)collect
{
    __weak DetailViewController *VC = self;
    [RequestManager collectionData:self.userID andID:tempID andIsYes:isLaw andIsCollect:collect andBlock:^(NSString *info) {
        
        if (![info isEqualToString:@"收藏失败"]) {
            isCollection = !isCollection;
            [VC setCollectBtnImage:isCollection];
        }
        [MBProgressHUD showOlnyMessage:info toView:VC.view];
    }];
}

/**
 *  案例的收藏
 *
 *  @param isLaw   1表示法条0表示案例
 *  @param collect 1表示收藏0表示取消收藏
 */
- (void)collectionCaseWithLawOrCase:(int)isLaw andIsCollect:(int)collect
{
    NSLog(@"%@",self.userID);
    __weak DetailViewController *VC = self;
    [RequestManager collectionCaseData:self.userID andID:tempID andIsYes:isLaw andIsCollect:collect andBlock:^(NSString *info) {
        
        if (![info isEqualToString:@"收藏失败"]) {
            isCollection = !isCollection;
            [VC setCollectBtnImage:isCollection];
        }
        [MBProgressHUD showOlnyMessage:info toView:VC.view];
    }];
}

- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}

- (IBAction)lastAndNextBtn:(UIButton *)sender {
    
    if (sender.tag == 1) {      // 上一条
        self.iDProperty -= 1;
        if (self.iDProperty < 0 || self.iDProperty >= self.IDArr.count) {
            self.iDProperty += 1;
            [MBProgressHUD showOlnyMessage:@"最后一条" toView:self.view];
            return;
        }
        if (self.typeArr.count != 0) {
           self.type = [self.typeArr[self.iDProperty] intValue];
        }
        tempID = [self.IDArr[self.iDProperty] intValue];
        [self removeIndexItem];
        [self removeIndexList];
    }else{
        self.iDProperty += 1;
        if (self.iDProperty < 0 || self.iDProperty >= self.IDArr.count) {
            [MBProgressHUD showOlnyMessage:@"最后一条" toView:self.view];
            self.iDProperty -= 1;
            return;
        }
        if (self.typeArr.count != 0) {
            self.type = [self.typeArr[self.iDProperty] intValue];
        }
        tempID = [self.IDArr[self.iDProperty] intValue];
        [self removeIndexItem];
        [self removeIndexList];
    }
    [self requestData];
}


#pragma mark -- 1、判断文章中是否有目录，如果有，则添加索引目录

- (void)addIndexItem{
    BOOL hasIndex = YES;
    self.dataArray = [[NSMutableArray alloc]init];
    NSString *urlString = self.contentView.text;
    NSString *matchedString = [urlString stringByMatching:@"目录([^。]+附则)\\s+"];
    if (matchedString.length > 0) {
        urlString = matchedString;
    }
    NSArray *arr = [urlString componentsMatchedByRegex:@"\\s+第一章\\s*\\S+"];
    
    //1.判断目录头是否存在
    if (arr.count >= 2) {//存在
        if (matchedString.length > 0) {//目录二字存在
            NSArray *tmpArr = [urlString componentsMatchedByRegex:@"\\s+(第[一二三四五六七八九十]+[章节]\\s*\\S+)"];
            [self.dataArray addObjectsFromArray:tmpArr];
        }else {//目录二字不存在
            //判断章节是否存在
            NSRange range = [urlString rangeOfRegex:@"\\s+(第[一二三四五六七八九十]+[章节]\\s*\\S+)"];
            if (range.location != NSNotFound) {//章节存在
                NSString *matchStr = [urlString stringByMatching:@"第一章[^。]+附则"];
                NSArray *tmpArr = [matchStr componentsMatchedByRegex:@"\\s*(第[一二三四五六七八九十]+[章节]\\s*\\S+)"];
                [self.dataArray addObjectsFromArray:tmpArr];
            }else{//目录不存在
                hasIndex = NO;
            }
        }
    }else {//不存在
        [self.dataArray addObjectsFromArray:[urlString componentsMatchedByRegex:@"\\s+(第[一二三四五六七八九十]+[章节]\\s*\\S+)"]];
//        [self.dataArray addObjectsFromArray:[urlString componentsMatchedByRegex:@"\\s+[一二三四五六七八九十]+、\\s*\\S+"]];
    }
    
    //去除换行
    for (int i = 0; i < self.dataArray.count; i ++) {
        
        self.dataArray[i] = [self.dataArray[i] stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    }

    if (self.dataArray.count == 0) {
        hasIndex = NO;
    }else{
        hasIndex = YES;
    }
    if (hasIndex) {
        //添加目录
        _indexItem = [UIButton buttonWithType:UIButtonTypeSystem];
        _indexItem.frame = CGRectMake(kWidth-55, kHeight-150, 40, 40);
        _indexItem.alpha = 0.8;
        [_indexItem setImage:[[UIImage imageNamed:@"caseIndex"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [_indexItem addTarget:self action:@selector(selectItem) forControlEvents:UIControlEventTouchUpInside];
        AppDelegate *app = [UIApplication sharedApplication].delegate;
        [app.window addSubview:_indexItem];
    }
    
    //添加搜索
    [self addSearchButton];
}


#pragma mark -- 2、目录定位事件
- (void)selectItem{
    CGRect frame = CGRectZero;
    //添加目录列表
    if (!_indexTableView) {
        if (IsIPhone4s||IsIPhone4) {
            frame = CGRectMake(_indexItem.frame.origin.x-220, kHeight-388-110+64, 215, 388-64);
        }else{
            frame = CGRectMake(_indexItem.frame.origin.x-220, kHeight-388-110, 215, 388);
        }
        
        if (self.dataArray.count <=5) {
            frame = CGRectMake(_indexItem.frame.origin.x-220, kHeight-200-110, 215, 200);
        }
        _indexTableView = [[UITableView alloc]initWithFrame:frame style:UITableViewStyleGrouped];
        _indexTableView.layer.borderWidth = 5;
        _indexTableView.layer.borderColor = COLOR(248, 248, 248).CGColor;
        _indexTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _indexTableView.backgroundColor = COLOR(236, 236, 236);
        _indexTableView.delegate = self;
        _indexTableView.dataSource = self;
        AppDelegate *app = [UIApplication sharedApplication].delegate;
        [app.window addSubview:_indexTableView];
    }else {
        [self removeIndexList];
    }
}


//返回顶部
- (void)toTopClick:(UIButton *)btn{

//    [self.contentView setContentOffset:CGPointMake(0, 1)];
//    if (isHidde == YES) {
//        [self changeTitleVIewAndTabBarFram];
//        isHidde = !isHidde;
//    }
    [self scrollToTop];
}


#pragma mark -- 3、滚动到指定位置
//本方法，根据目录字符串定位到指定的位置
- (void)scrollToIndexLocationWithString:(NSString *)string withIndex:(NSInteger)index{
    
    CGFloat textHight = 0.0;
    NSRange chapetRange;
    NSRange sectionRange;
    //如果有节的存在
    if ([self.dataArray[index] rangeOfString:@"节"].location != NSNotFound) {
        
        for (int i = (int)index ; i > 0; i--) {
            if ([self.dataArray[i] rangeOfString:@"章"].location != NSNotFound) {
                chapetRange.location = [self locateLastRangeInSuperString:self.contentView.text andSubString:self.dataArray[i]];//获取节所属的章的位置
                break;
            }
        }
        
        NSString *fromChapterStr = [self.contentView.text substringFromIndex:chapetRange.location];
        sectionRange = [fromChapterStr rangeOfString:string];
        
        NSAttributedString *str = [self.contentView.attributedText  attributedSubstringFromRange:NSMakeRange(0, chapetRange.location+sectionRange.location)];
        textHight = [str boundingRectWithSize:CGSizeMake(SIZE.width-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size.height;
        
    }else {//没有节的存在
        NSAttributedString *str = [self.contentView.attributedText  attributedSubstringFromRange:NSMakeRange(0, [self locateLastRangeInSuperString:self.contentView.text andSubString:string])];
        textHight = [str boundingRectWithSize:CGSizeMake(SIZE.width-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size.height;
    }
    
    //设置偏移量
//    [self.contentView setContentOffset:CGPointMake(0, isHidde?textHight+headerHigth+10:textHight+headerHigth-30) animated:YES];
    [self.contentView setContentOffset:CGPointMake(0,textHight+headerHigth-44) animated:YES];
    NSLog(@"%f   %f",self.contentView.contentSize.height,self.contentView.contentSize.width);
}

//找到子字符串在字符串中的最后一个位置
- (NSUInteger)locateLastRangeInSuperString:(NSString *)superStr andSubString:(NSString *)subStr {
    
    //判断子串是否存在
    NSRange range = [superStr rangeOfString:subStr];
    NSString *tmpStr = superStr;
    NSUInteger locate = range.location;
    while (range.location != NSNotFound) {//遍历子串
        tmpStr = [tmpStr substringFromIndex:range.location+range.length];
        range = [tmpStr rangeOfString:subStr];
        if (range.location != NSNotFound) {
            locate += range.location+range.length;
        }
    }
    return locate;
}

#pragma mark -- tableview delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    
    NSString *string = nil;
//    string = [self.dataArray[indexPath.row] stringByReplacingOccurrencesOfRegex:@"(第[一二三四五六七八九十]+[章节])([\\s\\S]*)" withString:@"$1 $2"];
//    string = self.dataArray[indexPath.row];
    string = [self.dataArray[indexPath.row] stringByReplacingOccurrencesOfRegex:@"(第[一二三四五六七八九十]+[章节])\\s*([\\S&&[^。]]+)" withString:@"$1 $2"];
    string = [string stringByReplacingOccurrencesOfRegex:@"(第[一二三四五六七八九十]+[节])([\\s\\S]*)" withString:@"　$1$2"];
    NSLog(@"string:%@",string);
    cell.textLabel.text = string;
    cell.backgroundColor = COLOR(236, 236, 236);
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.textColor = setColor(90, 90, 90);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self scrollToIndexLocationWithString:self.dataArray[indexPath.row] withIndex:indexPath.row];
    [self removeIndexList];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 1)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 50;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 50)];

    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.frame = CGRectMake(10, 12, 35, 35);
    [btn setImage:[[UIImage imageNamed:@"caseI"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [view addSubview:btn];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(45, 14, 80, 30)];
    label.text = @"目录索引";
    label.font = [UIFont boldSystemFontOfSize:17];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = setColor(90, 90, 90);
    label.numberOfLines = 1;
    [view addSubview:label];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(15, 49.5, 215-30, 0.5)];
    line.backgroundColor =setColor(90, 90, 90);
    [view addSubview:line];
    return view;
}


- (void)viewDidDisappear:(BOOL)animated{
    
    [self removeIndexItem];
    [self removeIndexList];
    [self removeToTopBtn];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self removeIndexList];
}

#pragma mark -- 索引按钮和回到顶部按钮，添加和移除
//添加topbutton
- (void)addTopButton{
    _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _topBtn.alpha = 0.8;
    [_topBtn setImage:[[UIImage imageNamed:@"caseTotop"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [_topBtn addTarget:self action:@selector(toTopClick:) forControlEvents:UIControlEventTouchUpInside];
    _topBtn.frame = CGRectMake(kWidth-55, kHeight - 100, 40, 40);
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app.window addSubview:_topBtn];
    
}

//添加搜索按钮
- (void)addSearchButton{
    _searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect frame = CGRectZero;
    if (_indexItem) {
        frame = CGRectMake(kWidth-55, kHeight - 200, 40, 40);
    }else {
        frame = CGRectMake(kWidth-55, kHeight - 150, 40, 40);
    }
    _searchBtn.frame = frame;
    [_searchBtn setImage:[[UIImage imageNamed:@"Search_detail_search"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    _searchBtn.alpha = 0.8;
    [_searchBtn addTarget:self action:@selector(addSearchView) forControlEvents:UIControlEventTouchUpInside];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app.window addSubview:_searchBtn];
}

//添加搜索view
- (void)addSearchView{
    if (_searchView) {
        [_searchView removeFromSuperview];
        
        self.contentView.attributedText = [self highlightKeyWords:[[NSMutableAttributedString alloc]initWithString:_contentText attributes:_attrDic]];
        _searchView = nil;
    }else {
        _searchView = [[UIView alloc]initWithFrame:CGRectMake(0, kHeight-44, kWidth, 44)];
        _searchView.backgroundColor = setColor(231, 231, 231);
        [self.view addSubview:_searchView];

        _searchBar = [[UITextField alloc]initWithFrame:CGRectMake(10, 8, kWidth - 50, 28)];
        _searchBar.placeholder = @"搜索关键字";
        [_searchBar setValue:[UIFont systemFontOfSize:14] forKeyPath:@"_placeholderLabel.font"];
        NSMutableParagraphStyle *style = [_searchBar.defaultTextAttributes[NSParagraphStyleAttributeName] mutableCopy];
        style.minimumLineHeight = _searchBar.font.lineHeight - (_searchBar.font.lineHeight - [UIFont systemFontOfSize:13.0f].lineHeight) / 2.0;
        //[UIFont systemFontOfSize:13.0f]是设置的placeholder的字体
        _searchBar.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"搜索关键字"
                                                                           attributes:@{NSParagraphStyleAttributeName : style}];
        _searchBar.borderStyle = UITextBorderStyleRoundedRect;
        
        //左侧图片
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Search_detail_searchicon"]];
        imageView.frame = CGRectMake(0, 0, 24*0.6, 24*0.6);
        _searchBar.leftView = imageView;
        _searchBar.delegate = self;
        _searchBar.leftViewMode = UITextFieldViewModeAlways;
        _searchBar.backgroundColor = [UIColor whiteColor];
        [_searchView addSubview:_searchBar];
        
        //右侧图片
        UIView *rightView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 48+5+50, 24)];
        
        _numLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 24)];
        _numLabel.text = @"0/0";
        _numLabel.font = [UIFont systemFontOfSize:12];
        _numLabel.textColor = KGRAYCOLOR;
        [rightView addSubview:_numLabel];
        
        UIButton *upBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        upBtn.frame = CGRectMake(50, 4, 24*0.8, 24*0.8);
        [upBtn setImage:[UIImage imageNamed:@"Search_detail_up"] forState:UIControlStateNormal];
        upBtn.tag = 10;
        [upBtn addTarget:self action:@selector(nextKeyWordsClick:) forControlEvents:UIControlEventTouchUpInside];
        [rightView addSubview:upBtn];
        
        UIButton *downBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        downBtn.frame = CGRectMake(CGRectGetMaxX(upBtn.frame)+5, 4, 24*0.8, 24*0.8);
        [downBtn setImage:[UIImage imageNamed:@"Search_detail_down"] forState:UIControlStateNormal];
        downBtn.tag = 11;
        [downBtn addTarget:self action:@selector(nextKeyWordsClick:) forControlEvents:UIControlEventTouchUpInside];
        [rightView addSubview:downBtn];
    
        _searchBar.rightView = rightView;
        _searchBar.rightViewMode = UITextFieldViewModeAlways;
        _searchBar.returnKeyType = UIReturnKeySearch;
        [_searchView addSubview:_searchBar];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setImage:[[UIImage imageNamed:@"Search_detail_x"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        cancelBtn.frame = CGRectMake(CGRectGetMaxX(_searchBar.frame)+10, 13, 24*0.8, 24*0.8);

        [cancelBtn addTarget:self action:@selector(cancelSearch:) forControlEvents:UIControlEventTouchUpInside];
        [_searchView addSubview:cancelBtn];
        
        [_searchBar becomeFirstResponder];
    }
}


#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self.contentView scrollRangeToVisible:NSMakeRange(0, self.contentView.text.length)];
    [self performSelector:@selector(scrollToTop) withObject:nil afterDelay:0.4];
    self.highlightedArray = [self calculateRangeArrayWithKeyWord:textField.text];
    self.contentView.attributedText = [self highlightKeyWords:[[NSMutableAttributedString alloc]initWithString:_contentText attributes:_attrDic]];
    self.contentView.attributedText = [self highlightedWords];
    _index = 0;
    
    _numLabel.text = [NSString stringWithFormat:@"%d/%ld",_highlightedArray.count == 0 ? 0:(_index+1),(long)_highlightedArray.count];
    return YES;
}


//移除搜索视图
- (void)cancelSearch:(UIButton *)btn{
    [_searchView removeFromSuperview];
//    self.contentView.attributedText = [[NSAttributedString alloc]initWithString:_contentText attributes:_attrDic];
    self.contentView.attributedText = [self highlightKeyWords:[[NSMutableAttributedString alloc]initWithString:_contentText attributes:_attrDic]];
}

//移除目录按钮
- (void)removeIndexItem{
    [_indexItem removeFromSuperview];
    _indexItem = nil;
    [_searchBtn removeFromSuperview];
    _searchBtn = nil;
}

//移除回到顶部按钮
- (void)removeToTopBtn{
    [_topBtn removeFromSuperview];
    _topBtn = nil;
}

//移除目录列表
- (void)removeIndexList{
    [_indexTableView removeFromSuperview];
    _indexTableView = nil;
}

//变蓝关键字
- (NSMutableAttributedString *)highlightKeyWords:(NSMutableAttributedString *)tmpStr{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (NSString *str in _emStrArr) {
        [self getContentRangeStr:_contentText findText:str andArray:arr];
    }
    for (int i = 0; i<arr.count; i++) {
        NSRange rang = [arr[i] rangeValue];
        // 设置富文本样式
        [tmpStr addAttribute:NSForegroundColorAttributeName
                        value:[UIColor colorWithRed:70/255.0 green:142/255.0 blue:239/255.0 alpha:1]
                        range:NSMakeRange(rang.location, rang.length)];
    }
    return tmpStr;
}

//高亮关键字
- (NSAttributedString *)highlightedWords{
    NSMutableAttributedString *tmpStr = [[NSMutableAttributedString alloc]initWithAttributedString:self.contentView.attributedText];
    MyAttributedStringBuilder *builder = [[MyAttributedStringBuilder alloc]initWithAttributedString:tmpStr];;
    for (int i = 0; i<_highlightedArray.count; i++) {
        NSRange rang = NSRangeFromString(_highlightedArray[i]);
        // 设置富文本样式
        [builder range:rang].backgroundColor = [UIColor yellowColor];
//        [builder range:rang].textColor = [UIColor redColor];
//        [tmpStr addAttribute:NSForegroundColorAttributeName
//                       value:[UIColor redColor]
//                       range:NSMakeRange(rang.location, rang.length)];
    }
    
    return builder.commit;
}

- (void)getContentRangeStr:(NSString *)text findText:(NSString *)findText andArray:(NSMutableArray *)arr
{
    NSUInteger lengtn = text.length;
    NSString *temp = nil;

    for(int i =0; i < lengtn; i++)
    {
        if (lengtn - i < findText.length) {
            break;
        }
        temp = [text substringWithRange:NSMakeRange(i, findText.length)];
        if ([temp isEqualToString:findText]) {
            NSRange rang = NSMakeRange(i, findText.length);
            [arr addObject:[NSValue valueWithRange:rang]];
        }
    }
}


//计算关键字数组
- (NSMutableArray *)calculateRangeArrayWithKeyWord:(NSString *)searchWord{
    
    NSMutableString *blankWord = [NSMutableString string];
    for (int i = 0; i < searchWord.length; i ++) {
        
        [blankWord appendString:@" "];
    }
    NSMutableArray *feedBackArray = [NSMutableArray array];
    
    NSMutableString *totalString = [NSMutableString stringWithString:_contentText];
    for (int i = 0; i < INT_MAX; i++){
        if ([totalString rangeOfString:searchWord options:1].location != NSNotFound){
            
            NSRange newRange = [totalString rangeOfString:searchWord options:1];
            
            [feedBackArray addObject:NSStringFromRange(newRange)];
            [totalString replaceCharactersInRange:newRange withString:blankWord];
            
        }else{
            break;
        }
    }
    return feedBackArray;
}


//上一个关键字和下一个关键字
- (void)nextKeyWordsClick:(UIButton *)btn{
    if (btn.tag == 10) {//上一个
        _index --;
    }else if(btn.tag == 11){//下一个
        _index ++;
    }

    if (_index < 0) {
        _index = 0;
    }
    if (_index > _highlightedArray.count-1) {
        _index = _highlightedArray.count-1;
    }
    _numLabel.text = [NSString stringWithFormat:@"%ld/%ld",_highlightedArray.count == 0 ? 0:(_index+1),(long)_highlightedArray.count];
    [self scrollToKeyWordsWithIndex:_index];
}

//根据索引值滚动到第n个关键字位置
- (void)scrollToKeyWordsWithIndex:(NSInteger)index{
    CGFloat textHight = 0;
    if (_highlightedArray.count == 0) {
        return;
    }
    NSAttributedString *str = [self.contentView.attributedText  attributedSubstringFromRange:NSMakeRange(0, NSRangeFromString(_highlightedArray[index]).location)];
    textHight = [str boundingRectWithSize:CGSizeMake(SIZE.width-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size.height;
    [self.contentView setContentOffset:CGPointMake(0,textHight+headerHigth-44) animated:YES];
}
@end
