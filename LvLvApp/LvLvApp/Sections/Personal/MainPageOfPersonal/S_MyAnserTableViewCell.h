//
//  S_MyAnserTableViewCell.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/11.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
static  NSString *homeIndentifier=@"anser";
@interface S_MyAnserTableViewCell : UITableViewCell
@property (nonatomic, strong) UIImageView *image;
@property (nonatomic, strong) UILabel *nicLab;
@property (nonatomic, strong) UILabel *anserTimeLab;
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UILabel *contentLab;
@property (nonatomic, strong) UILabel *otherLab;
//@property (nonatomic,strong) S_MyAnserData *anserModel;

@end
