//
//  VipConfirmViewController.m
//  LvLvApp
//
//  Created by hope on 15/12/16.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "VipConfirmViewController.h"

#import "ConfirmViewController.h"

#import "M_IDGainBaseClass.h"
#import "M_IDGainData.h"
#import "M_IDGainDataModels.h"
@interface VipConfirmViewController ()

@end

@implementation VipConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title =@"身份认证";
    self.vipIDArray =[[NSMutableArray alloc]init];
    self.ConfirmScrollView.alwaysBounceVertical = YES;
    self.ConfirmScrollView.contentSize = CGSizeMake(SIZE.width, CGRectGetMaxY(self.ConfirmApplyAlter.frame)+20);
    
   [self.ConfirmApplyAlter addTarget:self action:@selector(Confirmalter) forControlEvents:UIControlEventTouchUpInside];
    [self request];
    
}


//跳到下一个页
- (void)Confirmalter{
  
    ConfirmViewController *confirmView =[[ConfirmViewController alloc]init];
    
    [self.navigationController pushViewController:confirmView animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)request
{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*count = [defaults objectForKey:@"users"];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KuserfocususerUrl,K_IP]];
    //申明返回的结果是json类型
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",count]];

    
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         M_IDGainBaseClass *middata =[[M_IDGainBaseClass alloc]initWithDictionary:dict];
         [self.vipIDArray addObject:middata.data];
          self.vipName.text =middata.data.userName;
          self.vipStatus.text =middata.data.profession;
          self.vipCompany.text =middata.data.companyName;
          self.vipPapers =[Utils imageWithFrame:CGRectMake(20, 38
                                                       , self.view.frame.size.width-40, 180) andImageUrl:[NSString stringWithFormat:@"http://%@/%@",K_IP,middata.data.imageUrl]];
     
        
         
             }];
    
}




@end
