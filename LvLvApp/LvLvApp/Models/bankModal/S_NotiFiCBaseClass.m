//
//  S_NotiFiCBaseClass.m
//
//  Created by IOS8  on 15/12/21
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_NotiFiCBaseClass.h"
#import "S_NotiFiCData.h"


NSString *const kS_NotiFiCBaseClassCode = @"Code";
NSString *const kS_NotiFiCBaseClassMessage = @"Message";
NSString *const kS_NotiFiCBaseClassData = @"Data";


@interface S_NotiFiCBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_NotiFiCBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_NotiFiCBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_NotiFiCBaseClassMessage fromDictionary:dict];
    NSObject *receivedS_NotiFiCData = [dict objectForKey:kS_NotiFiCBaseClassData];
    NSMutableArray *parsedS_NotiFiCData = [NSMutableArray array];
    if ([receivedS_NotiFiCData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_NotiFiCData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_NotiFiCData addObject:[S_NotiFiCData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_NotiFiCData isKindOfClass:[NSDictionary class]]) {
       [parsedS_NotiFiCData addObject:[S_NotiFiCData modelObjectWithDictionary:(NSDictionary *)receivedS_NotiFiCData]];
    }

    self.data = [NSArray arrayWithArray:parsedS_NotiFiCData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_NotiFiCBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_NotiFiCBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kS_NotiFiCBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_NotiFiCBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_NotiFiCBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_NotiFiCBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_NotiFiCBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_NotiFiCBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_NotiFiCBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_NotiFiCBaseClass *copy = [[S_NotiFiCBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
