//
//  LoginViewController.h
//  LvLvApp
//
//  Created by hope on 15/11/9.
//  Copyright (c) 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
//logzhaop
@property (strong, nonatomic) IBOutlet UIView *denglu;
@property (strong, nonatomic) IBOutlet UIImageView *logImage;

//登录
@property (strong, nonatomic) IBOutlet UIButton *logBut;

//输入密码
@property (weak, nonatomic) IBOutlet UITextField *UserPassword;
//请输入邮箱/手机号码
@property (weak, nonatomic) IBOutlet UITextField *EmailOrMobile;

//忘记密码
- (IBAction)ForgetThePassword:(id)sender;

//注册帐号
- (IBAction)RegisterAnAccount:(id)sender;

//登录
- (IBAction)Login:(id)sender;
@property (nonatomic, strong) NSString *userID;

@end
