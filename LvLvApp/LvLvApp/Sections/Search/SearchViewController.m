//
//  SearchViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/4.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "SearchViewController.h"
#import "SearChLIstTableViewCell.h"
#import "CacheDataBase.h"
#import "MBProgressHUD.h"
#import "DetailViewController.h"
#import "CasusHearView01.h"
#define SPACEHIGHT 10    // 间距

@interface SearchViewController ()
{
    
    NSMutableArray *_historyArr;  // 查询条件记录
    int _pageIndex;                   // 第几页
    int type;         // 法条和案例1表示法条，2表示案例
    int orderID;      // 相关性，和发布日期
    UITextField *_inputView;
    UIButton *leftButton;
    UIView *selectView;
    
    CasusHearView01 *headerView;
    UIView *searChVi;
    
    UITableView *historyTab;
    NSMutableArray *viewArray;
    
    UIView *conditionVi;
    UIButton *_aboutBtn;         // 相关性
    UIButton *_dateBtn;          // 发布日期
    UIButton *_otherSelectBtn;   // 其他选择
    BOOL isExpansion;           // 其他选择是否展开状态；
    CGFloat setHight;
    NSMutableArray *listDataArray;
    BOOL isChange;              // 如果到列表页面，案例和法条切换要请求数据
    NSArray *otherSelectArray;  // 其他选择数据
    NSMutableArray *AllIDArr;
    double resultCount;
    NSMutableArray *_filterArray;//filter数组
    NSString *_sortContent;//相关性或发布日期
    
    NSArray *_emStrArr;//关键字拆分数组，为了高亮显示
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"案例和法条搜索";
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    AllIDArr = [NSMutableArray array];
    listDataArray = [NSMutableArray array];
    _historyArr = [NSMutableArray array]; // 查询历史记录
    self.keywordsArray = [NSMutableArray array];//关键字和筛选字段数组
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app.cacheDb getSearchString:_historyArr];
    
    _filterArray = [NSMutableArray array];
    
    _pageIndex = 1;
    orderID = 1;
    type = 1;
    viewArray = [NSMutableArray array];
    searChVi = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, SIZE.height-64)];
    searChVi.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:searChVi];
    if (_historyArr.count != 0) {
        historyTab = [[UITableView alloc] initWithFrame:searChVi.bounds style:UITableViewStyleGrouped];
        historyTab.backgroundColor = [UIColor whiteColor];
        historyTab.delegate = self;
        historyTab.dataSource = self;
        [searChVi addSubview:historyTab];
        UIView *hisFootVi = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, 230)];
        UIButton *clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        clearBtn.frame = CGRectMake(hisFootVi.center.x-60, 40, 120, 35);
        [clearBtn setBackgroundImage:[UIImage imageNamed:@"清除历史记录"] forState:UIControlStateNormal];
        [clearBtn setTitle:@"清除搜索历史" forState:UIControlStateNormal];
        clearBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [clearBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [clearBtn addTarget:self action:@selector(clearHistSave:) forControlEvents:UIControlEventTouchUpInside];
        [hisFootVi addSubview:clearBtn];
        historyTab.tableFooterView = hisFootVi;
        historyTab.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    }
    
    headerView = [[NSBundle mainBundle] loadNibNamed:@"CasusHearView01" owner:self options:nil][0];
    [headerView.selectBtn setImage:[UIImage imageNamed:@"箭头1"] forState:UIControlStateNormal];
    [headerView.selectBtn setImage:[UIImage imageNamed:@"箭头2"] forState:UIControlStateSelected];
    conditionVi = [headerView viewWithTag:24];  // 查询条件的View
    [self getBtn];
    
    self.navigationController.navigationBar.hidden = NO;
    _inputView = [[UITextField alloc] initWithFrame:CGRectMake(50, 30, SIZE.width-130, 30)];
    [_inputView setTintColor:[UIColor colorWithRed:0 green:0 blue:255/255.0 alpha:0.5]];
    
    _inputView.delegate = self;
    _inputView.clearButtonMode = UITextFieldViewModeAlways;
    _inputView.placeholder = @"请输入标题，发布部门，关键字等";
    _inputView.font = [UIFont systemFontOfSize:14];
    _inputView.textColor = [UIColor colorWithRed:87/255.0 green:93/255.0 blue:102/255.0 alpha:1];
    _inputView.backgroundColor = [UIColor whiteColor];
    _inputView.borderStyle = UITextBorderStyleRoundedRect;
    [_inputView becomeFirstResponder];
    _inputView.returnKeyType = UIReturnKeyYahoo;
    
    self.navigationItem.titleView = _inputView;
    
    leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = CGRectMake(0, 0, 40, 15);
    [leftButton setBackgroundImage:[UIImage imageNamed:@"法条-1"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(leftBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 40, 15);
    [rightButton setTitle:@"取消" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithRed:65/255.0 green:71/255.0 blue:83/255.0 alpha:1] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightBtn:) forControlEvents:UIControlEventTouchUpInside];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    //添加一个监听者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    // 上拉加载
    self.listTaable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self infinite];
    }];
    __weak SearchViewController *VC = self;
    self.listTaable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageIndex = 1;
        [VC requestDataWithString];
    }];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
    self.navigationController.navigationBar.hidden = NO;
}

- (void)infinite
{
    _pageIndex += 1;
    if (type == 2) {        // 案例
        [self requestCaseData];
    }else{                  // 法条
        [self requestLawDataWithIsAddData:1];
    }
}


#pragma mark -- 监听键盘

- (void)keyBoardFrameChange:(NSNotification* )notification
{
    //获取键盘上来的时间和键盘的大小
    double time = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect;
    [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&rect];
    static UIView *registVI = nil;
    if (registVI == nil) {
        registVI = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, SIZE.height)];
        registVI.backgroundColor = [UIColor colorWithRed:70/255.0 green:70/255.0 blue:70/255.0 alpha:.3];
        //        registVI.alpha = 0.1;
        [self.view addSubview:registVI];
        [self.view bringSubviewToFront:searChVi];
        
    }
    
    if (rect.origin.y == SIZE.height) {
        [registVI removeFromSuperview];
        registVI = nil;
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:7];
    [UIView setAnimationDuration:time];
    [UIView commitAnimations];
}

- (void)getBtn
{
    _aboutBtn = (UIButton *)[headerView viewWithTag:21];
    [_aboutBtn addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    _aboutBtn.selected = YES;
    _dateBtn = (UIButton *)[headerView viewWithTag:22];
    [_dateBtn addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    _otherSelectBtn = (UIButton *)[headerView viewWithTag:23];
    [_otherSelectBtn addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark -- 相关性 发布日期 其他选择

- (void)actionClick:(UIButton *)btn
{
    [selectView removeFromSuperview];
    [_inputView resignFirstResponder];
    [self.listTaable.mj_header beginRefreshing];
    if (btn.tag == 21) {//相关性
        _sortContent = @"相关性";
        if (btn.selected == YES) {
            return;
        }else{
            _pageIndex = 1;
            [btn setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
            btn.selected = YES;
            orderID = 1;
            if (type == 1) {
                [MBProgressHUD showMessage:@"正在请求!" toView:self.view];
                [self requestDataWithString];
            }else {
                if (![[[self stringWithViewArray:viewArray] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//                    [MBProgressHUD showMessage:@"正在请求!" toView:self.view];
                    [self requestDataWithString];
                }
            }
            
            [_dateBtn setTitleColor:[UIColor colorWithRed:90/255.0 green:90/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
            _dateBtn.selected = NO;
        }
    }else if (btn.tag == 22){//发布日期
        _sortContent = @"发布日期";
        if (btn.selected == YES) {
            return;
        }else{
            
            _pageIndex = 1;
            [btn setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
            btn.selected = YES;
            orderID = 2;
            if (type == 1) {//法条
                [self requestDataWithString];
            }else {
                if (![[[self stringWithViewArray:viewArray] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
                    [self requestDataWithString];
                }
            }
            
            [_aboutBtn setTitleColor:[UIColor colorWithRed:90/255.0 green:90/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
            _aboutBtn.selected = NO;
        }
    }else{
        
#pragma mark -- 其他选择
        static OtherSelectView *bgVi = nil;
        if (isExpansion == NO) {
            _otherSelectBtn.selected = YES;
            UIView *aboutVi = (UIView *)[headerView viewWithTag:20];
            [[self.view superview] bringSubviewToFront:headerView];
            bgVi = [[OtherSelectView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(aboutVi.frame)-setHight, SIZE.width, SIZE.height-CGRectGetMaxY(aboutVi.frame)-64+setHight) andType:type andDataArray:otherSelectArray andFilterDict:self.filterDict];
            bgVi.delegate = self;
            [bgVi showInView:self.view];
            
            isExpansion = YES;
        }else{
            _otherSelectBtn.selected = NO;
            [bgVi removeFromSuperview];
            isExpansion = NO;
        }
    }
}



/**
 *  otherSelectView的代理方法
 *  @param sender 选择的条件数组
 */
-(void)done:(id)sender andFilterDict:(NSMutableDictionary *)dict
{
    self.filterDict = dict;
    
    //移除所有关键字数组
    [self.keywordsArray removeAllObjects];
    
    //关键字数组赋值
    for (NSString *key in [self.filterDict allKeys]) {
        for (NSString *str in self.filterDict[key]) {
            [self.keywordsArray addObject:str];
        }
    }
    //设置筛选展开为NO
    isExpansion = NO;
    _otherSelectBtn.selected = NO;
    
    if (viewArray.count != 0) {
        [_otherSelectBtn setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
    }
    
    //创建标签数组
    NSMutableArray *arrs = [NSMutableArray array];
    for (NSString *key in [dict allKeys]) {
        for (NSString *title in dict[key]) {
            [arrs addObject:title];
        }
    }
    
    //移除所有标签页，并重新排列视图
    for (UIView *view  in viewArray) {
        [view removeFromSuperview];
    }
    [viewArray removeAllObjects];
    [self resetView:viewArray];
    
    
    for (NSString *str in arrs) {
        [self createView:str];
    }
    [self resetView:viewArray];
    _pageIndex = 1;
    
    if (type == 2) {//案例
        
        //        [_otherSelectBtn setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
        //        sender = [sender stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        //        NSArray *arr= [sender componentsSeparatedByString:@" "];
        //        for (NSString *str in arr) {
        //            [self createView:str];
        //        }
        //        [self resetView:viewArray];
        //        _pageIndex = 1;
        //        NSString *str = [NSString stringWithFormat:@"%@ %@",[self stringWithViewArray:viewArray],sender];
        //        [self requestCaseData:str andIsAddData:0];
        [self requestCaseData];
        
    }else {//法条
        //        if (viewArray.count != 0) {
        //            [_otherSelectBtn setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
        //        }
        //        NSMutableArray *arrs = [NSMutableArray array];
        //        for (NSString *key in [dict allKeys]) {
        //            for (NSString *title in dict[key]) {
        //                [arrs addObject:title];
        //            }
        //        }
        //
        //        //移除所有标签页，并重新排列视图
        //        for (UIView *view  in viewArray) {
        //            [view removeFromSuperview];
        //        }
        //        [viewArray removeAllObjects];
        //        [self resetView:viewArray];
        //
        //
        //        for (NSString *str in arrs) {
        //            [self createView:str];
        //        }
        //        [self resetView:viewArray];
        //        _pageIndex = 1;
        [self requestLawDataWithIsAddData:0];
    }
    
    
}


//清除历史记录
- (void)clearHistSave:(UIButton *)btn
{
    NSLog(@"清除历史记录");
    [_historyArr removeAllObjects];
    [historyTab removeFromSuperview];
    //    [historyTab reloadData];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app.cacheDb deleteAllRecord];
}


#pragma mark -- 选择法条或案例

- (void)leftBtn:(UIButton *)btn
{
    if (selectView.tag != 2000) {
        selectView = [[UIView alloc] initWithFrame:CGRectMake(3, 50, 130, 100)];
        selectView.backgroundColor = [UIColor clearColor];
        selectView.tag = 2000;
        AppDelegate *app = [UIApplication sharedApplication].delegate;
        
        UIImageView *bgimag = [[UIImageView alloc] init];
        bgimag.frame = CGRectMake(0, 0, selectView.frame.size.width, selectView.frame.size.height);
        bgimag.image = [UIImage imageNamed:@"选择框"];
        [selectView addSubview:bgimag];
        
        UIImageView *lawimag = [[UIImageView alloc] init];
        lawimag.frame = CGRectMake(20, 20, 18,25);
        lawimag.image = [UIImage imageNamed:@"法条"];
        [selectView addSubview:lawimag];
        
        UIImageView *caseimag = [[UIImageView alloc] init];
        caseimag.frame = CGRectMake(20, 60, 18,25);
        caseimag.image = [UIImage imageNamed:@"案例"];
        [selectView addSubview:caseimag];
        
        UIButton *lawbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        lawbutton.frame = CGRectMake(20, 23, 100, 20);
        lawbutton.tag = 1;
        lawbutton.backgroundColor = [UIColor clearColor];
        lawbutton.titleLabel.font = [UIFont systemFontOfSize:17];
        [lawbutton setTitle:@"法条" forState:UIControlStateNormal];
        [lawbutton addTarget:self action:@selector(changeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [selectView addSubview:lawbutton];
        
        UIButton *casebutton = [UIButton buttonWithType:UIButtonTypeCustom];
        casebutton.frame = CGRectMake(20, 64, 100, 20);
        casebutton.tag = 2;
        casebutton.backgroundColor = [UIColor clearColor];
        casebutton.titleLabel.font = [UIFont systemFontOfSize:17];
        [casebutton setTitle:@"案例" forState:UIControlStateNormal];
        [casebutton addTarget:self action:@selector(changeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [selectView addSubview:casebutton];
        
        [app.window addSubview:selectView];
    }else{
        [selectView removeFromSuperview];
        selectView.tag = 200;
    }
    
}

// 法条和案例切换
- (void)changeBtnClick:(UIButton *)btn
{
    
    type = (int)btn.tag;
    _pageIndex = 1;
    [selectView removeFromSuperview];
    
    
    [self removeAllLabels];//移除所有标签
    
    if (btn.tag == 1) {//法条
        if (isChange) {
            [MBProgressHUD showMessage:@"正在请求!" toView:self.view ];
            [self requestLawDataWithIsAddData:0];
        }
        
        [leftButton setBackgroundImage:[UIImage imageNamed:@"法条-1"] forState:UIControlStateNormal];
    }else{
        if (isChange) {
            [MBProgressHUD showMessage:@"正在请求!" toView:self.view ];
            [self requestCaseData];
        }
        
        [leftButton setBackgroundImage:[UIImage imageNamed:@"案例-1"] forState:UIControlStateNormal];
    }
    
    selectView.tag = 200;
}

- (void)rightBtn:(UIButton *)btn
{
    [selectView removeFromSuperview];
    selectView.tag = 200;
    [_inputView endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -- tableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView  == self.listTaable) {
        return listDataArray.count;
    }
    return _historyArr.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView  == self.listTaable) {
        return headerView;
    }
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SIZE.width-10, 40)];
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.textColor = [UIColor grayColor];
    titleLab.text = @"   历史搜索";
    return titleLab;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView  == self.listTaable) {
        return headerView.frame.size.height;
    }
    return 40;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //清空self.keywordsArray内重复的对象
//    NSString *keyString = _inputView.text;
//    NSArray *keyArr = [keyString componentsSeparatedByRegex:@"\\s+"];
    NSArray *keyArr = _emStrArr;
    NSSet *set = [NSSet setWithArray:keyArr];
    keyArr = [set allObjects];
    
    static NSString *identifier = @"cell";
    if (tableView  == self.listTaable) {
        SearChLIstTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[SearChLIstTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSMutableAttributedString *attributedString = nil;
        NSMutableAttributedString *tempStr = nil;
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        NSMutableArray *conentRangArr = [[NSMutableArray alloc] init];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;// 字体的行间距
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14],NSParagraphStyleAttributeName:paragraphStyle};
        if (type == 2) {            // 案例
            
            S_List *list = listDataArray[indexPath.row];
            
            //过滤空格和换行
            list.title = [Utils removeBlankString:list.title];
            list.content = [Utils removeBlankString:list.content];
            list.court = [Utils removeBlankString:list.court];
            NSLog(@"%@",list.content);
            for (NSString *str in keyArr) {
                [self getRangeStr:list.title findText:str andArray:arr];
                [self getConentRangeStr:list.content findText:str andArray:conentRangArr];
            }
            
            attributedString = [[NSMutableAttributedString alloc] initWithString:list.title attributes:attributes];
            
            tempStr = [[NSMutableAttributedString alloc] initWithString:list.content attributes:attributes];
            cell.court.text = list.court;
            if (indexPath.row == 0) {
                cell.resultCount.frame = CGRectMake(10, 15, SIZE.width-20, 15);
                cell.resultCount.text = [NSString stringWithFormat:@"搜索结果 %.0f 篇",resultCount];
            }
        }else{//法条
            if (listDataArray.count != 0) {
                if ([listDataArray[indexPath.row] isKindOfClass:[ItemsDetailModel class]]) {
                    ItemsDetailModel *lawList = listDataArray[indexPath.row];
                    //过滤空格和换行
                    lawList.title = [Utils removeBlankString:lawList.title];
                    lawList.body = [Utils removeBlankString:lawList.body];
                    lawList.publish_dep = [Utils removeBlankString:lawList.publish_dep];
                    
                    for (NSString *str in keyArr) {
                        [self getRangeStr:lawList.title findText:str andArray:arr];
                        [self getConentRangeStr:lawList.body findText:str andArray:conentRangArr];
                    }
                    attributedString = [[NSMutableAttributedString alloc] initWithString:lawList.title attributes:attributes];
                    
                    tempStr = [[NSMutableAttributedString alloc] initWithString:lawList.body attributes:attributes];
                    cell.court.text = lawList.publish_dep;
                    if (indexPath.row == 0) {
                        cell.resultCount.frame = CGRectMake(10, 15, SIZE.width-20, 15);
                        cell.resultCount.text = [NSString stringWithFormat:@"搜索结果 %.0f 篇",resultCount];
                    }
                }
            }
        }
        
        for (int i = 0; i<arr.count; i++) {
            NSRange rang = [arr[i] rangeValue];
            // 设置富文本样式
            [attributedString addAttribute:NSForegroundColorAttributeName
                                     value:[UIColor colorWithRed:70/255.0 green:142/255.0 blue:239/255.0 alpha:1]
                                     range:NSMakeRange(rang.location, rang.length)];
        }
        
        for (int i = 0; i<conentRangArr.count; i++) {
            NSRange rang = [conentRangArr[i] rangeValue];
            // 设置富文本样式
            [tempStr addAttribute:NSForegroundColorAttributeName
                            value:[UIColor colorWithRed:70/255.0 green:142/255.0 blue:239/255.0 alpha:1]
                            range:NSMakeRange(rang.location, rang.length)];
        }
        
        CGSize titleH = [attributedString boundingRectWithSize:CGSizeMake(SIZE.width-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
        
        // 显示搜索结果
        if (indexPath.row == 0) {
            cell.resultCount.hidden = NO;
            cell.title.frame = CGRectMake(10, 40, SIZE.width-20, titleH.height);
        }else{
            cell.resultCount.hidden = YES;
            cell.title.frame = CGRectMake(10, 15, SIZE.width-20, titleH.height);
        }
        cell.court.frame = CGRectMake(10, CGRectGetMaxY(cell.title.frame)+5, SIZE.width-20, 15);
        cell.content.frame = CGRectMake(10, CGRectGetMaxY(cell.court.frame)+10, SIZE.width-20, 40);
        cell.title.attributedText  =attributedString;
        cell.content.attributedText = tempStr;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else{
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell1"];
        static UIButton *histLab = nil;
        if (!cell) {
            cell = [[UITableViewCell alloc] init];
            histLab = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 0, 40)];
            histLab.tag = 1000;
            [cell addSubview:histLab];
        }
        histLab.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        histLab.titleEdgeInsets=UIEdgeInsetsMake(0,0,0,0);
        histLab.titleLabel.font = [UIFont systemFontOfSize:14];
        [histLab setTitleColor:[UIColor colorWithRed:77/255.0 green:84/255.0 blue:94/255.0 alpha:1] forState:UIControlStateNormal];
        [histLab setTitle: _historyArr[_historyArr.count - (indexPath.row+1)] forState:UIControlStateNormal];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:histLab.titleLabel.font , NSFontAttributeName, nil];
        CGFloat a = [histLab.titleLabel.text sizeWithAttributes:dictionary].width;
        histLab.frame = CGRectMake(10, 0, a, 40);
        histLab.userInteractionEnabled = NO;
        
        return cell;
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == historyTab) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIButton *btn = (UIButton *)[cell viewWithTag:1000];
        _inputView.text = [NSString stringWithFormat:@"%@ %@",_inputView.text,btn.titleLabel.text];
        [self textFieldShouldReturn:_inputView];
    }
    
    [selectView removeFromSuperview];
    selectView.tag = 200;
    if (tableView  == self.listTaable) {
        DetailViewController *detailVC = [[DetailViewController alloc] init];
        if (type == 2) {
            
            detailVC.iDProperty = (int)indexPath.row;
            detailVC.IDArr = AllIDArr;
        }else{
            
            detailVC.iDProperty = (int)indexPath.row;
            detailVC.IDArr = AllIDArr;
        }
        
        detailVC.type = (int)type;
        detailVC.emStrArr = _emStrArr;
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView  == self.listTaable) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;// 字体的行间距
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14],NSParagraphStyleAttributeName:paragraphStyle};
        if (type == 2) {// 案例
            S_List *list = listDataArray[indexPath.row];
            //过滤空格和换行
            list.title = [Utils removeBlankString:list.title];
            list.content = [Utils removeBlankString:list.content];
            list.court = [Utils removeBlankString:list.court];
            NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:list.title attributes:attributes];
            CGSize textHight = [attStr boundingRectWithSize:CGSizeMake(SIZE.width-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
            if (indexPath.row == 0) {
                return textHight.height + 125;
            }
            return textHight.height + 100;
        }else{          // 法条
            if (listDataArray.count != 0) {
                
                ItemsDetailModel *lawList = listDataArray[indexPath.row];
                //过滤空格和换行
                lawList.title = [Utils removeBlankString:lawList.title];
                NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:lawList.title attributes:attributes];
                CGSize textHight = [attStr boundingRectWithSize:CGSizeMake(SIZE.width-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
                if (indexPath.row == 0) {
                    return textHight.height + 125;
                }
                return textHight.height + 100;
            }
        }
        
    }
    return 40;
}


#pragma mark -- 判断搜索关键字在标题或内容中的位置
// 标题
- (void)getRangeStr:(NSString *)text findText:(NSString *)findText andArray:(NSMutableArray *)arr
{
    NSString *temp = nil;
    for(int i =0; i < [text length]; i++)
    {
        if (text.length - i < findText.length) {
            break;
        }
        temp = [text substringWithRange:NSMakeRange(i, findText.length)];
        if ([temp isEqualToString:findText]) {
            NSRange rang = NSMakeRange(i, findText.length);
            NSLog(@"标题第%d个字是:%@", i, temp);
            [arr addObject:[NSValue valueWithRange:rang]];
        }
    }
}

// 内容
- (void)getConentRangeStr:(NSString *)text findText:(NSString *)findText andArray:(NSMutableArray *)arr
{
    NSUInteger lengtn = text.length;
    NSString *temp = nil;
    if (lengtn > 100) {
        lengtn = 100;
    }
    for(int i =0; i < lengtn; i++)
    {
        if (lengtn - i < findText.length) {
            break;
        }
        temp = [text substringWithRange:NSMakeRange(i, findText.length)];
        if ([temp isEqualToString:findText]) {
            NSRange rang = NSMakeRange(i, findText.length);
            [arr addObject:[NSValue valueWithRange:rang]];
        }
    }
}

#pragma mark -- scrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    setHight = targetContentOffset->y;
}


#pragma mark -- textFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    _pageIndex = 1;
    NSString *fieldStr = [Utils removeBlankString:textField.text];
    
    //移除所有关键字和标签
    [self.keywordsArray removeAllObjects];
    [self.filterDict removeAllObjects];
    [self removeAllLabels];
    
    if (![fieldStr isEqualToString:@""]) {
        //添加历史记录
        if (![_historyArr containsObject:textField.text]) {
            [_historyArr addObject:textField.text];
            AppDelegate *app = [UIApplication sharedApplication].delegate;
            [app.cacheDb insertString:textField.text];
        }
        
        [selectView removeFromSuperview];
        selectView.tag = 200;
        isChange = YES;
        [textField endEditing:YES];
        [searChVi removeFromSuperview];
        [_otherSelectBtn setTitleColor:[UIColor colorWithRed:90/255.0 green:90/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
        
        //移除所有的标签
        for (UIView *tempVi in viewArray) {
            [tempVi removeFromSuperview];
        }
        [viewArray removeAllObjects];
        [self resetView:viewArray];
        
        [MBProgressHUD showMessage:@"正在请求!" toView:self.view ];
        [self requestDataWithString];
    }
    
    return NO;
}

//如果筛选展开，输入框无法编辑
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (isExpansion == NO) {
        return YES;
    }
    return NO;
}


//根据type判断请求法条或者案例
- (void)requestDataWithString
{
    _pageIndex = 1;
    if (type == 1) {            // 法条
        [self requestLawDataWithIsAddData:0];
    }else{                      // 案例
        [self requestCaseData];
    }
}


#pragma mark -- 案例检索请求

-(void)requestCaseData
{
    if ([_inputView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        [self.keywordsArray removeAllObjects];
        [self.filterDict removeAllObjects];
        [self removeAllLabels];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.listTaable.mj_header endRefreshing];
        [self.listTaable.mj_footer endRefreshing];
        [MBProgressHUD showError:@"请输入搜索内容"];
        return;
    }
    
    [self.keywordsArray addObjectsFromArray:[_inputView.text componentsSeparatedByRegex:@"\\s+"]];
    //去除关键字数组中重复的对象
    NSSet *set = [NSSet setWithArray:self.keywordsArray];
    self.keywordsArray = [NSMutableArray arrayWithArray:[set allObjects]];
    
    __weak SearchViewController *VC = self;
    
    NSMutableString *string = [NSMutableString string];
    for (NSString *keys in self.keywordsArray) {
        [string appendFormat:@"%@ ",keys];
    }
    
    NSString *url = [NSString stringWithFormat:KExampleOfLaw,K_IP];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:string forKey:@"txtKeyWords1"];
    [parameters setObject:[NSString stringWithFormat:@"%d",orderID] forKey:@"SortBy"];    // 条件
    [parameters setObject:[NSString stringWithFormat:@"%d",_pageIndex] forKey:@"pageindex"]; // 第几页
    [parameters setObject:[NSString stringWithFormat:@"%d",K_PAGESIZE] forKey:@"pageSize"];  // 条数
    
    [LVHTTPRequestTool post:url params:parameters success:^(id json) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingAllowFragments error:nil];
        [VC.listTaable.mj_header endRefreshing];
        [VC.listTaable.mj_footer endRefreshing];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if ([dic[@"Code"] isEqualToNumber:@200]) {
            NSDictionary *dict = dic[@"Data"];
            S_LISTCase *caseModal = [[S_LISTCase alloc] initWithDictionary:dict];
            resultCount = caseModal.count;
            if (_pageIndex != 1) {
                [listDataArray addObjectsFromArray:caseModal.list];
                
            }else{
                [listDataArray removeAllObjects];
                listDataArray = [NSMutableArray arrayWithArray:caseModal.list];
            }
            
            otherSelectArray = caseModal.search;
            [VC performSelectorOnMainThread:@selector(reFreshUI:) withObject:@"1" waitUntilDone:YES];
        }
    } failure:^(NSError *error) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:error.userInfo[@"com.alamofire.serialization.response.error.data"] options:NSJSONReadingMutableContainers error:nil];
        BaseDataModel *errorModel = [[BaseDataModel alloc]init];
        [errorModel setValuesForKeysWithDictionary:dict];
        
        [VC.listTaable.mj_header endRefreshing];
        [VC.listTaable.mj_footer endRefreshing];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [MBProgressHUD showError:errorModel.Message];
    }];
}


#pragma mark -- 法条检索请求

- (void)requestLawDataWithIsAddData:(int)isYes
{
    if ([_inputView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        [self.keywordsArray removeAllObjects];
        [self.filterDict removeAllObjects];
        [self removeAllLabels];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.listTaable.mj_header endRefreshing];
        [self.listTaable.mj_footer endRefreshing];
        [MBProgressHUD showError:@"请输入搜索内容"];
        return;
    }
    
    [self.keywordsArray addObjectsFromArray:[_inputView.text componentsSeparatedByRegex:@"\\s+"]];
    
    //去除关键字数组中重复的对象
    NSSet *set = [NSSet setWithArray:self.keywordsArray];
    self.keywordsArray = [NSMutableArray arrayWithArray:[set allObjects]];
    __weak SearchViewController *VC = self;
    
    //传入的参数
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[NSNumber numberWithInteger:_pageIndex] forKey:@"PageIndex"];
    [parameters setObject:[NSNumber numberWithInteger:K_PAGESIZE] forKey:@"PageSize"];
    [parameters setObject:_inputView.text forKey:@"Keywords"];
    [parameters setObject:_sortContent == nil? @"":_sortContent forKey:@"Sort"];
    [parameters setObject:@"desc" forKey:@"Order"];
    if (self.filterDict) {
        [parameters setObject:self.filterDict forKey:@"Filters"];
    }
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:KCaseOfLaw,K_IMGIP] params:parameters success:^(id json) {
        [VC.listTaable.mj_header endRefreshing];
        [VC.listTaable.mj_footer endRefreshing];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if (_pageIndex == 1) {
            [listDataArray removeAllObjects];
        }
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        
        BaseDataModel *model = [[BaseDataModel alloc]init];
        [model setValuesForKeysWithDictionary:dict];
        
        if ([model.Code isEqualToNumber:@200]) {
            LawModel *lawModel = [[LawModel alloc]init];
            [lawModel setValuesForKeysWithDictionary:model.Data];
            _emStrArr = lawModel.Tokens;
            resultCount = [lawModel.Total floatValue];//法条总数
            NSMutableArray *itemsArray = [NSMutableArray array];//items数组
            
            //items
            for (NSDictionary *dict in lawModel.Items) {
                ItemsDetailModel *itemsModel = [[ItemsDetailModel alloc]init];
                [itemsModel setValuesForKeysWithDictionary:dict];
                [itemsArray addObject:itemsModel];
            }
            
            [listDataArray addObjectsFromArray: itemsArray];
            //filterGroups
            [_filterArray removeAllObjects];
            for (NSDictionary *dict in lawModel.FilterGroups) {
                FilterGroupsDetailModel *filterModel = [[FilterGroupsDetailModel alloc]init];
                [filterModel setValuesForKeysWithDictionary:dict];
                [_filterArray addObject:filterModel];
            }
            otherSelectArray = _filterArray;
            [VC performSelectorOnMainThread:@selector(reFreshUI:) withObject:@"2" waitUntilDone:YES];
        }else {
            [MBProgressHUD showError:dict[@"Message"]];
        }
        
    } failureMessage:^(NSString *errorMessage) {
        [VC.listTaable.mj_header endRefreshing];
        [VC.listTaable.mj_footer endRefreshing];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [MBProgressHUD showError:errorMessage];
    }];
}

- (void)reFreshUI:(NSString *)types
{
    [AllIDArr removeAllObjects];
    if ([types isEqualToString:@"2"]) {
        for (ItemsDetailModel *list in listDataArray) {
            [AllIDArr addObject:[NSString stringWithFormat:@"%@",list.ID]];
        }
    }else{
        
        for (S_LawListList *lawList in listDataArray) {
            [AllIDArr addObject:[NSString stringWithFormat:@"%.0f",lawList.listIdentifier]];
        }
    }
    
    [self.listTaable reloadData];
}

// 重新排列视图
- (void)resetView:(NSArray *)arr
{
    float x = 0;
    float y = SPACEHIGHT;
    float allWith = 0;
    float with = 0;
    
    for (int i = 0; i < viewArray.count; i++) {
        
        UIView *aView = [viewArray objectAtIndex:i];
        allWith +=aView.frame.size.width+SPACEHIGHT;
        if (allWith > SIZE.width-10) {
            y = y + ((UIView *)[viewArray objectAtIndex:i-1]).frame.size.height+SPACEHIGHT;
            allWith = aView.frame.size.width+SPACEHIGHT;
            x = 0;
            with = 0;
        }
        aView.frame = CGRectMake(SPACEHIGHT+with, y, aView.frame.size.width, aView.frame.size.height);
        x++;
        with+=aView.frame.size.width+SPACEHIGHT;
        [conditionVi addSubview:aView];
        
    }
    UIView *temp = [viewArray lastObject];
    if (viewArray.count) {
        headerView.frame = CGRectMake(0, headerView.frame.origin.y, SIZE.width, CGRectGetMaxY(temp.frame)+50);
    }else{
        headerView.frame = CGRectMake(0, headerView.frame.origin.y, SIZE.width, 40);
    }
    
    [self.listTaable reloadData];

}


//移除所有标签
- (void)removeAllLabels{
    
    [listDataArray removeAllObjects];
    
    //清楚所有关键字和所有筛选条件
    [self.keywordsArray removeAllObjects];
    [self.filterDict removeAllObjects];
    for (NSMutableArray *arr in [self.filterDict allValues]) {
        [arr removeAllObjects];
    }
    //重新排列标签视图
    for (UIView *tempVi in viewArray) {
        [tempVi removeFromSuperview];
    }
    [viewArray removeAllObjects];
    [self resetView:viewArray];
}

// 创建标签视图,只显示筛选里面的标签，不显示关键字
- (void)createView:(NSString *)title
{
    CGFloat viewHight = 20;
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor clearColor];
    
    UIImageView *bgImage = [[UIImageView alloc] init];
    bgImage.image = [UIImage imageNamed:@"便签框"];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.tag = 10;
    titleLabel.font = [UIFont systemFontOfSize:13];
    titleLabel.textColor = [UIColor colorWithRed:74/255.0 green:116/255.0 blue:161/255.0 alpha:1];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:titleLabel.font,NSFontAttributeName, nil];
    CGFloat titleWith = [title sizeWithAttributes:dictionary].width;
    
    if (titleWith >= (SIZE.width - 47)) {
        
        titleWith = SIZE.width - 47;
        titleLabel.numberOfLines = 0;
        CGSize size3 = [title boundingRectWithSize:CGSizeMake(titleWith, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} context:nil].size;
        viewHight = size3.height;
    }
    
    
    bgView.frame = CGRectMake(0, 0, titleWith+27, viewHight);
    bgImage.frame = bgView.bounds;
    titleLabel.frame = CGRectMake(5, 0, titleWith, viewHight);
    titleLabel.text = title;
    titleLabel.tag = 2;
    UIButton *reButton = [UIButton buttonWithType:UIButtonTypeSystem];
    reButton.frame = CGRectMake(titleWith + 10, bgView.center.y-6, 12, 12);
    reButton.backgroundColor = [UIColor clearColor];
    [reButton setBackgroundImage:[UIImage imageNamed:@"标签删除"] forState:UIControlStateNormal];
    [reButton addTarget:self action:@selector(removeView:) forControlEvents:UIControlEventTouchUpInside];
    
    [bgView addSubview:bgImage];
    [bgView addSubview:titleLabel];
    [bgView addSubview:reButton];
    [viewArray addObject:bgView];
}

//移除标签
- (void)removeView:(UIButton *)btn
{
    UIView *reVI = btn.superview;
    UILabel *titleLabel = (UILabel *)[reVI viewWithTag:2];
    NSString *title = titleLabel.text;
    [reVI removeFromSuperview];
    [viewArray removeObject:reVI];
    
    if ([self.keywordsArray containsObject:title]) {
        [self.keywordsArray removeObject:title];
    }
    for (NSString *key in [self.filterDict allKeys]) {
        if ([self.filterDict[key] containsObject:title]) {
            [self.filterDict[key] removeObject:title];
            break;
        }
    }
    //重新请求数据
    [self requestDataWithString];
    if (viewArray.count == 0) {
        [_otherSelectBtn setTitleColor:setColor(90, 90, 90) forState:UIControlStateNormal];
    }
    if ([self.filterDict allKeys] == 0) {
        [_otherSelectBtn setTitleColor:setColor(90, 90, 90) forState:UIControlStateNormal];
    }
    [self resetView:viewArray];
}

- (NSString *)stringWithViewArray:(NSMutableArray *)arr
{
    NSString *str = @"";
    for (UIView *vi in viewArray) {
        UILabel *lab = (UILabel *)[vi viewWithTag:10];
        str = [NSString stringWithFormat:@"%@ %@",str,lab.text];
    }
    return str;
}

- (void)registKeybord:(UITapGestureRecognizer *)tapGes
{
    [_inputView endEditing:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [selectView removeFromSuperview];
    selectView.tag = 200;
    [_inputView endEditing:YES];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
