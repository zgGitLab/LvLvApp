//
//  main.m
//  LvLvApp
//
//  Created by yfzx_sh_gaoyy on 15/10/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
