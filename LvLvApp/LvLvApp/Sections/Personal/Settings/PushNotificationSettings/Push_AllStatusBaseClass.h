//
//  Push_AllStatusBaseClass.h
//
//  Created by IOS8  on 15/12/29
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Push_AllStatusBaseClass : NSObject

@property (nonatomic, assign) double userID;
@property (nonatomic, assign) double commentMe;
@property (nonatomic, assign) double sendMe;
@property (nonatomic, assign) double focusNewAnwser;
@property (nonatomic, assign) double praiseMe;
@property (nonatomic, assign) double focusMe;
@property (nonatomic, assign) double iDProperty;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
