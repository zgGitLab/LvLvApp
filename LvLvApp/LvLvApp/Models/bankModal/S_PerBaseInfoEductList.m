//
//  S_PerBaseInfoEductList.m
//
//  Created by IOS8  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_PerBaseInfoEductList.h"


NSString *const kS_PerBaseInfoEductListID = @"ID";
NSString *const kS_PerBaseInfoEductListUserID = @"UserID";
NSString *const kS_PerBaseInfoEductListUniversity = @"University";
NSString *const kS_PerBaseInfoEductListInstitute = @"Institute";
NSString *const kS_PerBaseInfoEductListAdmissionDate = @"AdmissionDate";


@interface S_PerBaseInfoEductList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_PerBaseInfoEductList

@synthesize iDProperty = _iDProperty;
@synthesize userID = _userID;
@synthesize university = _university;
@synthesize institute = _institute;
@synthesize admissionDate = _admissionDate;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [self objectOrNilForKey:kS_PerBaseInfoEductListID fromDictionary:dict];
            self.userID = [[self objectOrNilForKey:kS_PerBaseInfoEductListUserID fromDictionary:dict] doubleValue];
            self.university = [self objectOrNilForKey:kS_PerBaseInfoEductListUniversity fromDictionary:dict];
            self.institute = [self objectOrNilForKey:kS_PerBaseInfoEductListInstitute fromDictionary:dict];
            self.admissionDate = [self objectOrNilForKey:kS_PerBaseInfoEductListAdmissionDate fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.iDProperty forKey:kS_PerBaseInfoEductListID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kS_PerBaseInfoEductListUserID];
    [mutableDict setValue:self.university forKey:kS_PerBaseInfoEductListUniversity];
    [mutableDict setValue:self.institute forKey:kS_PerBaseInfoEductListInstitute];
    [mutableDict setValue:self.admissionDate forKey:kS_PerBaseInfoEductListAdmissionDate];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeObjectForKey:kS_PerBaseInfoEductListID];
    self.userID = [aDecoder decodeDoubleForKey:kS_PerBaseInfoEductListUserID];
    self.university = [aDecoder decodeObjectForKey:kS_PerBaseInfoEductListUniversity];
    self.institute = [aDecoder decodeObjectForKey:kS_PerBaseInfoEductListInstitute];
    self.admissionDate = [aDecoder decodeObjectForKey:kS_PerBaseInfoEductListAdmissionDate];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_iDProperty forKey:kS_PerBaseInfoEductListID];
    [aCoder encodeDouble:_userID forKey:kS_PerBaseInfoEductListUserID];
    [aCoder encodeObject:_university forKey:kS_PerBaseInfoEductListUniversity];
    [aCoder encodeObject:_institute forKey:kS_PerBaseInfoEductListInstitute];
    [aCoder encodeObject:_admissionDate forKey:kS_PerBaseInfoEductListAdmissionDate];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_PerBaseInfoEductList *copy = [[S_PerBaseInfoEductList alloc] init];
    
    if (copy) {

        copy.iDProperty = [self.iDProperty copyWithZone:zone];
        copy.userID = self.userID;
        copy.university = [self.university copyWithZone:zone];
        copy.institute = [self.institute copyWithZone:zone];
        copy.admissionDate = [self.admissionDate copyWithZone:zone];
    }
    
    return copy;
}


@end
