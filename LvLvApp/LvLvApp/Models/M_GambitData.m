//
//  M_GambitData.m
//
//  Created by jim  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_GambitData.h"
#import "M_GambitListAll.h"


NSString *const kM_GambitDataListAll = @"listAll";
NSString *const kM_GambitDataPagesize = @"pagesize";
NSString *const kM_GambitDataPageindex = @"pageindex";


@interface M_GambitData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_GambitData

@synthesize listAll = _listAll;
@synthesize pagesize = _pagesize;
@synthesize pageindex = _pageindex;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedM_GambitListAll = [dict objectForKey:kM_GambitDataListAll];
    NSMutableArray *parsedM_GambitListAll = [NSMutableArray array];
    if ([receivedM_GambitListAll isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedM_GambitListAll) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedM_GambitListAll addObject:[M_GambitListAll modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedM_GambitListAll isKindOfClass:[NSDictionary class]]) {
       [parsedM_GambitListAll addObject:[M_GambitListAll modelObjectWithDictionary:(NSDictionary *)receivedM_GambitListAll]];
    }

    self.listAll = [NSArray arrayWithArray:parsedM_GambitListAll];
            self.pagesize = [[self objectOrNilForKey:kM_GambitDataPagesize fromDictionary:dict] doubleValue];
            self.pageindex = [[self objectOrNilForKey:kM_GambitDataPageindex fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForListAll = [NSMutableArray array];
    for (NSObject *subArrayObject in self.listAll) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForListAll addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForListAll addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForListAll] forKey:kM_GambitDataListAll];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pagesize] forKey:kM_GambitDataPagesize];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pageindex] forKey:kM_GambitDataPageindex];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.listAll = [aDecoder decodeObjectForKey:kM_GambitDataListAll];
    self.pagesize = [aDecoder decodeDoubleForKey:kM_GambitDataPagesize];
    self.pageindex = [aDecoder decodeDoubleForKey:kM_GambitDataPageindex];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_listAll forKey:kM_GambitDataListAll];
    [aCoder encodeDouble:_pagesize forKey:kM_GambitDataPagesize];
    [aCoder encodeDouble:_pageindex forKey:kM_GambitDataPageindex];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_GambitData *copy = [[M_GambitData alloc] init];
    
    if (copy) {

        copy.listAll = [self.listAll copyWithZone:zone];
        copy.pagesize = self.pagesize;
        copy.pageindex = self.pageindex;
    }
    
    return copy;
}


@end
