//
//  S_LawDetailDs.h
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_LawDetailDs : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *selectArticles;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
