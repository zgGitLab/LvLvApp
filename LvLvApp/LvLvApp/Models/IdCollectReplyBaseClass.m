//
//  IdCollectReplyBaseClass.m
//
//  Created by hope  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IdCollectReplyBaseClass.h"
#import "IdCollectReplyData.h"


NSString *const kIdCollectReplyBaseClassCode = @"Code";
NSString *const kIdCollectReplyBaseClassMessage = @"Message";
NSString *const kIdCollectReplyBaseClassData = @"Data";


@interface IdCollectReplyBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IdCollectReplyBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kIdCollectReplyBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kIdCollectReplyBaseClassMessage fromDictionary:dict];
    NSObject *receivedIdCollectReplyData = [dict objectForKey:kIdCollectReplyBaseClassData];
    NSMutableArray *parsedIdCollectReplyData = [NSMutableArray array];
    if ([receivedIdCollectReplyData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedIdCollectReplyData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedIdCollectReplyData addObject:[IdCollectReplyData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedIdCollectReplyData isKindOfClass:[NSDictionary class]]) {
       [parsedIdCollectReplyData addObject:[IdCollectReplyData modelObjectWithDictionary:(NSDictionary *)receivedIdCollectReplyData]];
    }

    self.data = [NSArray arrayWithArray:parsedIdCollectReplyData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kIdCollectReplyBaseClassCode];
    [mutableDict setValue:self.message forKey:kIdCollectReplyBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kIdCollectReplyBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kIdCollectReplyBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kIdCollectReplyBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kIdCollectReplyBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kIdCollectReplyBaseClassCode];
    [aCoder encodeObject:_message forKey:kIdCollectReplyBaseClassMessage];
    [aCoder encodeObject:_data forKey:kIdCollectReplyBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    IdCollectReplyBaseClass *copy = [[IdCollectReplyBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
