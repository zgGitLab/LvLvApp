//
//  IDAttentionData.m
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IDAttentionData.h"


NSString *const kIDAttentionDataTID = @"TID";
NSString *const kIDAttentionDataFocusCount = @"FocusCount";
NSString *const kIDAttentionDataTitle = @"Title";
NSString *const kIDAttentionDataAnswerCount = @"AnswerCount";


@interface IDAttentionData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IDAttentionData

@synthesize tID = _tID;
@synthesize focusCount = _focusCount;
@synthesize title = _title;
@synthesize answerCount = _answerCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tID = [self objectOrNilForKey:kIDAttentionDataTID fromDictionary:dict];
            self.focusCount = [[self objectOrNilForKey:kIDAttentionDataFocusCount fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kIDAttentionDataTitle fromDictionary:dict];
            self.answerCount = [[self objectOrNilForKey:kIDAttentionDataAnswerCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tID forKey:kIDAttentionDataTID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.focusCount] forKey:kIDAttentionDataFocusCount];
    [mutableDict setValue:self.title forKey:kIDAttentionDataTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.answerCount] forKey:kIDAttentionDataAnswerCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tID = [aDecoder decodeObjectForKey:kIDAttentionDataTID];
    self.focusCount = [aDecoder decodeDoubleForKey:kIDAttentionDataFocusCount];
    self.title = [aDecoder decodeObjectForKey:kIDAttentionDataTitle];
    self.answerCount = [aDecoder decodeDoubleForKey:kIDAttentionDataAnswerCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tID forKey:kIDAttentionDataTID];
    [aCoder encodeDouble:_focusCount forKey:kIDAttentionDataFocusCount];
    [aCoder encodeObject:_title forKey:kIDAttentionDataTitle];
    [aCoder encodeDouble:_answerCount forKey:kIDAttentionDataAnswerCount];
}

- (id)copyWithZone:(NSZone *)zone
{
    IDAttentionData *copy = [[IDAttentionData alloc] init];
    
    if (copy) {

        copy.tID = [self.tID copyWithZone:zone];
        copy.focusCount = self.focusCount;
        copy.title = [self.title copyWithZone:zone];
        copy.answerCount = self.answerCount;
    }
    
    return copy;
}


@end
