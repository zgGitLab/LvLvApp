//
//  FilterGroupsDetailModel.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/26.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterGroupsDetailModel : BaseDataModel

@property (nonatomic,copy) NSString *name;
@property (nonatomic,strong) NSArray *Groups;
@property (nonatomic,copy) NSString *count;

@end
