//
//  SearchViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/4.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtherSelectView.h"

@interface SearchViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,doneShure,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *listTaable;
@property (nonatomic,strong) NSMutableDictionary *filterDict;//筛选字段
@property (nonatomic,strong) NSMutableArray *keywordsArray;//关键字和筛选字段数组
@end
