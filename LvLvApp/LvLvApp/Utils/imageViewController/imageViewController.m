//
//  imageViewController.m
//  UIScrollView
//
//  Created by mac on 14-5-5.
//  Copyright (c) 2014年 weilanglang. All rights reserved.
//

#import "imageViewController.h"

@interface imageViewController (){
    UIScrollView *scrollview;
    UIPageControl *pageControl;
    NSInteger width;
    NSInteger hight;
}

@end

@implementation imageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    width=self.view.bounds.size.width;
    hight=self.view.bounds.size.height;
    scrollview=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, width, hight)];
    [scrollview setDelegate:self];
    for (int i=1; i<6; i++) {
        NSString *fileName=[NSString stringWithFormat:@"guide%d.jpg",i];
        UIImageView *imageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:fileName]];
        imageView.userInteractionEnabled = YES;
        [imageView setFrame:CGRectMake((i-1)*width, 0, width, hight)];
        if (i == 5) {
            UISwipeGestureRecognizer *swip = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(finishLoading)];
            [swip setDirection:UISwipeGestureRecognizerDirectionLeft];
            [scrollview.panGestureRecognizer requireGestureRecognizerToFail:swip];
            [imageView addGestureRecognizer:swip];
        }
        [scrollview  addSubview:imageView];
    }
    [scrollview setContentSize:CGSizeMake(width*5, hight)];
    [scrollview setBounces:NO];
    
    //滚动时是否水平显示滚动条
    [scrollview setShowsHorizontalScrollIndicator:NO];
    //分页显示
    [scrollview setPagingEnabled:YES];
    
    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, kHeight-30, kWidth, 20)];
    pageControl.alpha = 0.8;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor = KMAINCOLOR;
    pageControl.pageIndicatorTintColor = [UIColor grayColor];
    pageControl.numberOfPages = 5;
    
    [pageControl addTarget:self action:@selector(click) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:scrollview];
    [self.view addSubview:pageControl];
    
    UIButton *_topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _topBtn.frame = CGRectMake(kWidth-70, 20, 60, 40);
    [_topBtn setTitle:@"跳过>" forState:UIControlStateNormal];
    [_topBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _topBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    _topBtn.clipsToBounds = YES;
    _topBtn.layer.cornerRadius = 5;
    _topBtn.alpha = 0.6;
    [_topBtn addTarget:self action:@selector(finishLoading) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_topBtn];
    
}


- (void) click{
    NSLog(@"xxx");

}


-(void)finishLoading
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    //保存服务器的版本号到本地
    [userDefault setObject:version forKey:@"firstLoading"];
    [userDefault synchronize];
    //进入主页
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingFinish" object:nil];

}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:@"loadingFinish"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//滚动是就会触发
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    NSLog(@"正在滚动");
//}
//
////开始拖拽视图
//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
//{
//    NSLog(@"scrollViewWillBeginDragging");
//}
//完成拖拽
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
//{
//    NSLog(@"scrollViewDidEndDragging");
//}
////将开始降速时
//- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView;
//{
//    NSLog(@"scrollViewWillBeginDecelerating");
//}
//
////减速停止了时执行，手触摸时执行执行
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
{
   int index= scrollview.contentOffset.x/width;
    [pageControl setCurrentPage:index];

}
////滚动动画停止时执行,代码改变时出发,也就是setContentOffset改变时
//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
//{
//    NSLog(@"scrollViewDidEndScrollingAnimation");
//}
////设置放大缩小的视图，要是uiscrollview的subview
//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;
//{
//    NSLog(@"viewForZoomingInScrollView");
//    return viewA;
//}
////完成放大缩小时调用
//- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale;
//{
//   
//    NSLog(@"scale between minimum and maximum. called after any 'bounce' animations");
//}// scale between minimum and maximum. called after any 'bounce' animations
//
////如果你不是完全滚动到滚轴视图的顶部，你可以轻点状态栏，那个可视的滚轴视图会一直滚动到顶部，那是默认行为，你可以通过该方法返回NO来关闭它
//- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView;
//{
//    NSLog(@"scrollViewShouldScrollToTop");
//    return YES;
//}
//
//- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView;
//{
//    NSLog(@"scrollViewDidScrollToTop");
//}

@end
