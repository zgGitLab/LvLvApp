//
//  CountModal.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/3.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

// 案例首页，统计数量

@interface CountModal : NSObject
@property (strong,nonatomic)NSString *A_DayAddCount; // 法条新增
@property (strong,nonatomic)NSString *ArticlesCount; // 法条收录
@property (strong,nonatomic)NSString *V_DayAddCount; // 案例新增
@property (strong,nonatomic)NSString *VerdictsCount; // 案例收录

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
@end
