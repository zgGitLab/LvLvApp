//
//  DetailModel.h
//  Tools
//
//  Created by IOS8 on 15/9/15.
//  Copyright (c) 2015年 IOS8. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetailModel : NSObject

@property (nonatomic, copy) NSString *debtTime;         // 欠款时间
@property (nonatomic, copy) NSString *dayCount;         // 实际天数
@property (nonatomic, copy) NSString *liLv;             // 利率
@property (nonatomic, assign) double liLi;              // 利息
@property (nonatomic, copy) NSString *benJIn;           // 本金

- (void)computeday:(NSString *)startday endDay:(NSString *)endday andLiLv:(NSString *)lilv;

@end
