//
//  MyButton.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/24.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyButton : UIButton
@property(assign,nonatomic)double ID;
@property(strong,nonatomic)NSString *NowUserID;
@property(strong,nonatomic)NSString *AUserID;
@property(strong,nonatomic)NSString *TUserID;
@property(strong,nonatomic)NSString *TID;
@property(strong,nonatomic)NSString *AnserID;
@end
