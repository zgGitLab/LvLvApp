//
//  SearChLIstTableViewCell.h
//  LvLvApp
//
//  Created by Sunny on 16/1/15.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearChLIstTableViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *title;//法条或案例标题
@property (strong, nonatomic) UILabel *court;//法院
@property (strong, nonatomic) UILabel *content;//法条或案例内容
@property (strong, nonatomic) UILabel *resultCount;//搜索结果数量
@end
