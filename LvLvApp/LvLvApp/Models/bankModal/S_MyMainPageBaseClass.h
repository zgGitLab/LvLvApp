//
//  S_MyMainPageBaseClass.h
//
//  Created by IOS8  on 15/12/10
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_MyMainPageBaseClass : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *position;
@property (nonatomic, assign) double shareCount;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, assign) double answerCount;
@property (nonatomic, strong) id introduction;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, assign) double responseCount;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *telPhone;
@property (nonatomic, assign) double fansCount;
@property (nonatomic, assign) double questionCount;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, assign) double collectCount;
@property (nonatomic, assign) double questionFocus;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, assign) double isV;
@property (nonatomic, assign) double IsFocus;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
