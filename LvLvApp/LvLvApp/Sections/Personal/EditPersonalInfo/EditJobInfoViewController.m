//
//  EditJobInfoViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/2.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "EditJobInfoViewController.h"
#import "MyCityList.h"
#import "EditTimeView.h"
@interface EditJobInfoViewController ()<PerTimeDatePickViewDelegat,PerareaPickViewDelegat>

@end

@implementation EditJobInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"编辑工作信息";
    if (!self.ishide) {
        [self getTitleName];
    }
    
    UIButton *finshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    finshButton.frame = CGRectMake(0, 0, 40, 15);
    [finshButton setTitle:@"完成" forState:UIControlStateNormal];
    [finshButton setTitleColor:[UIColor colorWithRed:65/255.0 green:71/255.0 blue:83/255.0 alpha:1] forState:UIControlStateNormal];
    [finshButton addTarget:self action:@selector(finshBtn:) forControlEvents:UIControlEventTouchUpInside];
    finshButton.titleLabel.font = [UIFont systemFontOfSize:18];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:finshButton];
    self.removeBtn.hidden = self.ishide;
    //添加一个监听者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)getTitleName
{
    self.companyName.text = _jobModal.company;
    self.jobName.text = _jobModal.position;
    if (![_jobModal.beginYear isEqualToString:@""]) {
         [self.timeBtn setTitle:[NSString stringWithFormat:@"%@ - %@",[_jobModal.beginYear stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],[_jobModal.endYear stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] forState:UIControlStateNormal];
    }
    [self.areaBtn setTitle:_jobModal.lacation forState:UIControlStateNormal];
}

#pragma mark ----监听事件----
- (void)keyBoardFrameChange:(NSNotification* )notification
{
    //获取键盘上来的时间和键盘的大小
    double time = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect;
    [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&rect];
    static UIView *registVI = nil;
    if (registVI == nil) {
        registVI = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, SIZE.height)];
        registVI.backgroundColor = [UIColor colorWithRed:70/255.0 green:70/255.0 blue:70/255.0 alpha:0];
        //        registVI.alpha = 0.1;
        [self.view addSubview:registVI];
        
    }
    
    if (rect.origin.y == SIZE.height) {
        [registVI removeFromSuperview];
        registVI = nil;
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:7];
    [UIView setAnimationDuration:time];
    [UIView commitAnimations];
}

- (IBAction)temeBtn:(UIButton *)sender {
    
    EditTimeView *editTime = [[EditTimeView alloc] initWithCount:2];
    editTime.delegate = self;
    [editTime showInView];
}
- (IBAction)areaBtn:(UIButton *)sender {
    
    MyCityList *cityListVi = [[MyCityList alloc] init];
    cityListVi.delegate = self;
    [cityListVi showInView];
}
// 删除工作信息
- (IBAction)removeBtn:(UIButton *)sender {
    [RequestManager removewJobMessageWithID:_jobModal.iDProperty andBlock:^(NSString *message) {
        
        if ([message isEqualToString:@"成功"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showOlnyMessage:message toView:self.view];
        }
        
    }];
}

- (void)finshBtn:(UIButton *)btn
{
    if ([self.companyName.text isEqualToString:@""]) {
        K_ALERT(@"请输入公司名称");
        return;
    }
    if (self.ishide) {  // 添加工作信息
        [RequestManager addJobMessageWithUser:[self getUserId] andCompanyName:_companyName.text andjobName:_jobName.text andJobTime:_timeBtn.titleLabel.text andArea:_areaBtn.titleLabel.text andBlock:^(NSString *message) {
            if ([message isEqualToString:@"成功"]) {
               [self.navigationController popViewControllerAnimated:YES];
            }else{
                [MBProgressHUD showOlnyMessage:message toView:self.view];
            }
//            [MBProgressHUD showOlnyMessage:message toView:self.view];
        }];

    }else{              // 修改工作信息
        
        [RequestManager changeJobMessageWithUser:self.jobModal.iDProperty andCompanyName:_companyName.text andjobName:_jobName.text andJobTime:_timeBtn.titleLabel.text andArea:_areaBtn.titleLabel.text andBlock:^(NSString *message) {
            if ([message isEqualToString:@"成功"]) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [MBProgressHUD showOlnyMessage:message toView:self.view];
            }
//            [MBProgressHUD showOlnyMessage:message toView:self.view];
        }];

    }
}

- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}

#pragma mark PerTimeDatePickViewDelegat
- (void)selectTimeDate:(NSString *)timeDate
{
    [self.timeBtn setTitle:timeDate forState:UIControlStateNormal];
//    self.timeBtn.titleLabel.text = timeDate;
}
#pragma mark PerareaPickViewDelegat
- (void)selectareaStr:(NSString *)areaStr
{
    [self.areaBtn setTitle:areaStr forState:UIControlStateNormal];
//    self.areaBtn.titleLabel.text = areaStr;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
