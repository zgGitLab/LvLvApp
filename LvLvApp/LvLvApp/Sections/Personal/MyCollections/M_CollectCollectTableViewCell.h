//
//  M_CollectCollectTableViewCell.h
//  LvLvApp
//
//  Created by Mark on 16/1/14.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface M_CollectCollectTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *Collectitle;
@property (strong, nonatomic) IBOutlet UILabel *SellWholesaleUnits;
@end
