//
//  DraftViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "DraftViewController.h"
#import "DraftTableViewCell.h"

#import "IDDraftData.h"
#import "IDDraftBaseClass.h"
#import "IDDraftDataModels.h"
#import "SCSwipeTableViewCell.h"
#import "AnswerQuestionViewController.h"
#import "DraftPublishViewController.h"
@interface DraftViewController ()<SCSwipeTableViewCellDelegate>
{
    NSInteger _pageIndex;
    IDDraftData *daraftData;
    CGFloat titleSpace;//昵称和title的距离
    CGFloat commentSpace;//赞同和内容之间的距离
    CGFloat topSpace;//顶部和底部的距离
    CGFloat lineSpace;//行间距,这里的行间距是估算的，真实的行间距是3
    CGFloat doubleLineHeight1;
    NSMutableArray *btnArr;
    SCSwipeTableViewCell *cells;
}

@end

@implementation DraftViewController

- (void)viewDidLoad {
    _pageIndex =1;
    titleSpace = 9;
    commentSpace = 7;
    topSpace = 18;
    lineSpace = 2;
    doubleLineHeight1 = 40;//
    [super viewDidLoad];
    _pageIndex =1;
    self.navigationController.navigationBarHidden = NO;
    
    self.title = @"我的草稿";
    self.draftArray =[[NSMutableArray alloc]init];
    
    self.myTable.tableFooterView = [[UIView alloc] init];
    [self.myTable setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    [self.draftArray removeAllObjects];
    self.myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.draftArray removeAllObjects];
        [self myDraftViewRequestWithPageindex:1 andPageSize:10];
        [self.view layoutSubviews];
        
    }];
    self.myTable.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        [self myDraftViewRequestWithPageindex:++_pageIndex andPageSize:10];
        [self.view layoutSubviews];
        
    }];
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.draftArray removeAllObjects];
    [self myDraftViewRequestWithPageindex:1 andPageSize:10];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem*butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    ;
    butItem.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
    
}
- (void)back{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
//
//        [self myDraftdeleteRequestWith:indexPath];
//        editingStyle = UITableViewCellEditingStyleDelete;
//}

//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    return @"删除";
//}
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return UITableViewCellEditingStyleDelete;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.draftArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cells";
    UIButton *but3 =[UIButton buttonWithType:UIButtonTypeCustom];
    but3.backgroundColor =[UIColor clearColor];
    [but3 addTarget:self action:@selector(butss:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
    btn1.backgroundColor = [UIColor redColor];
    [btn1 setTitle:@"删除" forState:UIControlStateNormal];
    UIButton *btn2 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 0.1, 55)];
    [btn2 setTitle:@"" forState:UIControlStateNormal];
    btnArr = [[NSMutableArray alloc]initWithObjects:btn1,btn2, nil];
    cells = [[SCSwipeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:identifier
                                               withBtns:btnArr
                                              tableView:tableView];
    cells.delegate =self;
    daraftData =self.draftArray[indexPath.row];
    CGFloat titleHeight = [Utils textHeightFromTextString:daraftData.title width:kWidth-30 fontSize:16];
    if (titleHeight > 30) {
        titleHeight = doubleLineHeight1+lineSpace;
    }
    UILabel *titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(15,13, kWidth-30, titleHeight)];
    titleLabel.textColor =setColor(81, 88, 99);
    titleLabel.font =[UIFont systemFontOfSize:16];
    titleLabel.text =daraftData.title;
    titleLabel.numberOfLines =0;
    [cells.SCContentView addSubview:titleLabel];
    
    UILabel *SellWholesealeUnits =[[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(titleLabel.frame)+commentSpace, kWidth-30, 15)];
    SellWholesealeUnits.textColor =setColor(122, 122, 122);
    SellWholesealeUnits.font =[UIFont systemFontOfSize:14];
    SellWholesealeUnits.text =[Utils replaceImageString:daraftData.content];
    SellWholesealeUnits.numberOfLines =1;
    [cells.SCContentView addSubview:SellWholesealeUnits];
    
    //            [cells.SCContentView addSubview:but3];
    
    CGFloat tiHeight = [Utils textHeightFromTextString:daraftData.title width:kWidth-30 fontSize:16];
    if (tiHeight > 30) {
        tiHeight = doubleLineHeight1+lineSpace;
        cells.SCContentView.frame = CGRectMake(0, 0, SIZE.width, 19+CGRectGetMaxY(SellWholesealeUnits.frame));
        btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, cells.SCContentView.frame.size.height);
        but3.frame =CGRectMake(15, 1, self.view.frame.size.width,cells.SCContentView.frame.size.height);
        
    }else {
        cells.SCContentView.frame = CGRectMake(0, 0, SIZE.width, 14+CGRectGetMaxY(SellWholesealeUnits.frame));
        btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, cells.SCContentView.frame.size.height);
        but3.frame =CGRectMake(15, 1, self.view.frame.size.width,cells.SCContentView.frame.size.height);
        
    }
    [cells.SCContentView addSubview:but3];
    
    return cells;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (self.draftArray.count> indexPath.row) {
    //        daraftData =self.draftArray[indexPath.row];
    //    }
    CGSize size = [daraftData.title boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
    NSLog(@"%lf",size.width);
    if (size.width > 2*(kWidth - 30)) {
        return 96;
    }else if(size.width >3*(kWidth -30)){
        return 96;
    }
    else if (size.width >(kWidth - 30)){
        return 96;
    }else{
        return 68;
    }
}

#pragma mark SCSwipeTableViewCellDelegate

- (void)SCSwipeTableViewCelldidSelectBtnWithTag:(NSInteger)tag andIndexPath:(NSIndexPath *)indexpath{
    
    if (self.draftArray.count>indexpath.row) {
        
        [self myDraftdeleteRequestWith:indexpath];
        [self.draftArray removeObjectAtIndex:indexpath.row];
    }
}


- (void)cellOptionBtnWillHide{
}

//删除草稿请求的
- (void)myDraftdeleteRequestWith:(NSIndexPath *)indexPath
{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    
    NSLog(@"%@",daraftData.answerID);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:kdeleteDraftUrl,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",UserID ]];
    NSLog(@"%@",UserID);
    [param appendString:[NSString stringWithFormat:@"&AnswerID=%@",daraftData.answerID]];
    
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError){
             [self.myTable.mj_header endRefreshing];
             
             [self.myTable.mj_footer endRefreshing];
             
             return;
         }
         
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         
         [self.myTable reloadData];
         [self notDraftView];
         [self.myTable.mj_header endRefreshing];
         
         [self.myTable.mj_footer endRefreshing];
     }];
}


//数据请求
- (void)myDraftViewRequestWithPageindex:(NSInteger)pageIndex andPageSize:(NSInteger)pageSize
{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    // 1.url
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KmyDraft,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",UserID ]];
    [param appendString:[NSString stringWithFormat:@"&PageIndex=%d",(int)pageIndex]];
    [param appendString:[NSString stringWithFormat:@"&PageSize=%d",(int)pageSize]];
    
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         IDDraftBaseClass *draftdata=[[IDDraftBaseClass alloc]initWithDictionary:dict];
         [self.draftArray addObjectsFromArray:draftdata.data];
         [self.myTable.mj_header endRefreshing];
         [self.myTable.mj_footer endRefreshing];
         [self.myTable reloadData];
         
         [self notDraftView];
         
     }];
}

- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    [self.myTable scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}
- (void)notDraftView
{
    if(self.draftArray.count ==0) {
        UIView *views =[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2-50, self.view.bounds.size.width, 30)];
        UILabel *labe =[[UILabel alloc]init];
        [labe setText:@"您暂无草稿!"];
        labe.font=[UIFont systemFontOfSize:16.f];
        CGSize size = [labe.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
        NSLog(@"%lf",size.width);
        labe.frame=CGRectMake(SIZE.width/2-size.width/2, 2, size.width, 25);        [labe setTextColor:[UIColor colorWithRed:158.0/255.0 green:158.0/255.0 blue:158.0/255.0 alpha:1]];
        [views addSubview:labe];
        //self.myTable.tableFooterView=views;
        [self.view addSubview:views];
        return;
    }
    
}
- (void)cellOptionBtnDidShow{
    NSLog(@"cellOptionBtnDidShow");
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    app.isShowBtn = YES;
}
- (void)butss:(UIButton*)sender{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.isShowBtn) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SC_CELL_SHOULDCLOSE" object:nil userInfo:@{@"action":@"closeCell"}];
        app.isShowBtn = NO;
        return;
    }
    UIView *view = [[sender superview] superview];
    SCSwipeTableViewCell *cell = (SCSwipeTableViewCell *)[view superview];
    NSIndexPath *indexPath = [self.myTable indexPathForCell:cell];
    daraftData =[self.draftArray objectAtIndex:indexPath.row];
    DraftPublishViewController *answerView = [[DraftPublishViewController alloc]init];
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    NSString *usr =[NSString stringWithFormat:@"%@",UserID];
    answerView.UserID =usr;
    answerView.TID =daraftData.tID;
    answerView.textContent =daraftData.content;
    NSLog(@"%@",answerView.textContent);
    answerView.PushanswerID =daraftData.answerID;
    NSLog(@"%@",answerView.PushanswerID);
    [self.navigationController pushViewController:answerView animated:YES];
    
    
}
//跳入到另一个页面
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    DraftTableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
//    cell.selected =NO;
//     daraftData =[self.draftArray objectAtIndex:indexPath.row];
//    if (self.draftArray.count >indexPath.row) {
//
//        DraftPublishViewController *answerView = [[DraftPublishViewController alloc]init];
//
//        NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
//        NSString*UserID = [defaults objectForKey:@"users"];
//        NSString *usr =[NSString stringWithFormat:@"%@",UserID];
//        answerView.UserID =usr;
//        answerView.TID =daraftData.tID;
//        answerView.textContent =daraftData.content;
//        NSLog(@"%@",answerView.textContent);
//        answerView.PushanswerID =daraftData.answerID;
//        NSLog(@"%@",answerView.PushanswerID);
//        [self.navigationController pushViewController:answerView animated:YES];
//
//    }
//
//}
@end
