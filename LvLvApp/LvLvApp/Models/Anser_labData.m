//
//  Anser_labData.m
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Anser_labData.h"


NSString *const kAnser_labDataTagID = @"TagID";
NSString *const kAnser_labDataCommentCount = @"CommentCount";
NSString *const kAnser_labDataIsFocus = @"IsFocus";
NSString *const kAnser_labDataQuestionCount = @"QuestionCount";
NSString *const kAnser_labDataTagName = @"TagName";
NSString *const kAnser_labDataFocusTag = @"FocusTag";


@interface Anser_labData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Anser_labData

@synthesize tagID = _tagID;
@synthesize commentCount = _commentCount;
@synthesize isFocus = _isFocus;
@synthesize questionCount = _questionCount;
@synthesize tagName = _tagName;
@synthesize focusTag = _focusTag;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tagID = [[self objectOrNilForKey:kAnser_labDataTagID fromDictionary:dict] doubleValue];
            self.commentCount = [[self objectOrNilForKey:kAnser_labDataCommentCount fromDictionary:dict] doubleValue];
            self.isFocus = [[self objectOrNilForKey:kAnser_labDataIsFocus fromDictionary:dict] doubleValue];
            self.questionCount = [[self objectOrNilForKey:kAnser_labDataQuestionCount fromDictionary:dict] doubleValue];
            self.tagName = [self objectOrNilForKey:kAnser_labDataTagName fromDictionary:dict];
            self.focusTag = [[self objectOrNilForKey:kAnser_labDataFocusTag fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tagID] forKey:kAnser_labDataTagID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kAnser_labDataCommentCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isFocus] forKey:kAnser_labDataIsFocus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.questionCount] forKey:kAnser_labDataQuestionCount];
    [mutableDict setValue:self.tagName forKey:kAnser_labDataTagName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.focusTag] forKey:kAnser_labDataFocusTag];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tagID = [aDecoder decodeDoubleForKey:kAnser_labDataTagID];
    self.commentCount = [aDecoder decodeDoubleForKey:kAnser_labDataCommentCount];
    self.isFocus = [aDecoder decodeDoubleForKey:kAnser_labDataIsFocus];
    self.questionCount = [aDecoder decodeDoubleForKey:kAnser_labDataQuestionCount];
    self.tagName = [aDecoder decodeObjectForKey:kAnser_labDataTagName];
    self.focusTag = [aDecoder decodeDoubleForKey:kAnser_labDataFocusTag];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_tagID forKey:kAnser_labDataTagID];
    [aCoder encodeDouble:_commentCount forKey:kAnser_labDataCommentCount];
    [aCoder encodeDouble:_isFocus forKey:kAnser_labDataIsFocus];
    [aCoder encodeDouble:_questionCount forKey:kAnser_labDataQuestionCount];
    [aCoder encodeObject:_tagName forKey:kAnser_labDataTagName];
    [aCoder encodeDouble:_focusTag forKey:kAnser_labDataFocusTag];
}

- (id)copyWithZone:(NSZone *)zone
{
    Anser_labData *copy = [[Anser_labData alloc] init];
    
    if (copy) {

        copy.tagID = self.tagID;
        copy.commentCount = self.commentCount;
        copy.isFocus = self.isFocus;
        copy.questionCount = self.questionCount;
        copy.tagName = [self.tagName copyWithZone:zone];
        copy.focusTag = self.focusTag;
    }
    
    return copy;
}


@end
