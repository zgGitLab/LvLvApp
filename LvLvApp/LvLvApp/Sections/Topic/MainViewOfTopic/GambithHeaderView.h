//
//  GambithHeaderView.h
//  法律app
//
//  Created by hope on 15/10/8.
//  Copyright (c) 2015年 mark.ma. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchTopicViewController;

@protocol GambitSearchViewDelegate <NSObject>

-(void)doSearchAction;
-(void)doAddAction:(UIButton *)btn;
-(void)doHotspotAction:(UIButton *)btns;
-(void)doDynameicAction:(UIButton *)btnn;
@end
@interface GambithHeaderView : UIView
@property (nonatomic, strong) UISearchBar *searchBarbut;
@property(nonatomic,strong)UIViewController  *viewController;
@property (strong, nonatomic) IBOutlet UIView *ViewHD;

- (IBAction)dynamicBut:(id)sender;
- (IBAction)hotBut:(id)sender;



@property (strong, nonatomic) IBOutlet UIButton *oneBtn;

@property (weak, nonatomic) IBOutlet UIButton *twoBtn;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property(nonatomic, weak) id<GambitSearchViewDelegate> delegate;
@end
