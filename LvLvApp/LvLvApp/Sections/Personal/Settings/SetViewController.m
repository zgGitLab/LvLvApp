//
//  SetViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "SetViewController.h"
#import "IndividualCenter.h"

#import "ChangePwdViewController.h"
#import "ConfirmViewController.h"
#import "CheckViewController.h"
#import "FailedViewController.h"

#import "VipConfirmViewController.h"

#import "M_messagePushSetViewController.h"
#import "FontSizeViewController.h"

#import "SetDataModels.h"
#import "SetViewBaseClass.h"
#import "SetViewData.h"

@interface SetViewController (){
    
    NSInteger ispass;
    
    SetViewBaseClass *setbase;
    
    //IsPass=0 未提交 IsPass=1 提交未审核 IsPass=2审核成功 IsPass=3审核失败
}

@end

@implementation SetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    
    self.setArray =[[NSMutableArray alloc]init];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
    
    self.cacheLab.text = [NSString stringWithFormat:@"%.2fMB",[[SDImageCache sharedImageCache] getSize]/ 1024.0 / 1024.0];
    [self setviewdata];
    
    // [self imageView];
    [self ConfirmRequest];
    
}
- (void)imageView{
    if ([self.VIPstr isEqualToString:@"1"]) {
        self.vipImageView.image =[UIImage imageNamed:@"加V"];
    }else {
        self.vipImageView.image =[UIImage imageNamed:@""];
    }
}


- (void)ConfirmRequest
{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:UserID forKey:@"UserID"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:kIgetauthentication,K_IP] params:params success:^(id json) {
        NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        
        setbase=[[SetViewBaseClass alloc]initWithDictionary:dict];
        [self.setArray addObject:setbase.data];
        self.ispassStr =[NSString stringWithFormat:@"%d",(int)setbase.data.isPass];
        [self imageView];
    } failure:^(NSError *error) {
        
    }];
}

//进入大V身份认证
- (IBAction)IdentityConfirmBtn:(UIButton *)sender {
    //IsPass=0 未提交 IsPass=1 提交审核中 IsPass=2审核成功 IsPass=3审核失败
    self.ispassStr = @"3";
    if ([self.ispassStr isEqualToString:@"0"]) {
        ConfirmViewController *confView= [[ConfirmViewController alloc]init];
        [self.navigationController pushViewController:confView animated:YES];
    }else if([self.ispassStr isEqualToString:@"1"]){
        CheckViewController *checkView =[[CheckViewController alloc]init];
        [self.navigationController pushViewController:checkView animated:YES];
    }else if([self.ispassStr isEqualToString:@"2"]){
        VipConfirmViewController *ViPwdVC = [[VipConfirmViewController alloc] init];
        [self.navigationController pushViewController:ViPwdVC  animated:YES];
    }else if([self.ispassStr isEqualToString:@"3"]){
        FailedViewController *failedController = [[FailedViewController alloc]init];
        [self.navigationController pushViewController:failedController animated:YES];
    }
}

- (void)setviewdata
{
    SetViewData *setdata =[self.setArray  observationInfo];
    self.ispassStr =[NSString stringWithFormat:@"%ld",(long)setdata.isPass];
    NSLog(@"%@",self.ispassStr);
}

- (IBAction)changePwdBtn:(UIButton *)sender {
    
    ChangePwdViewController *changePwdVC = [[ChangePwdViewController alloc] init];
    [self.navigationController pushViewController:changePwdVC animated:YES];
    
}

- (IBAction)pushBtn:(UIButton *)sender {
    
    M_messagePushSetViewController *push =[[M_messagePushSetViewController alloc]init];
    [self.navigationController pushViewController:push animated:YES];
    
}

- (IBAction)fontSizeBtn:(UIButton *)sender {
    FontSizeViewController *font =[[FontSizeViewController alloc]init];
    [self.navigationController pushViewController:font animated:YES];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem*butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    
    butItem.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
}

- (void)back {
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (IBAction)clearCache:(UIButton *)sender {
    
    [self clearTmpPics];
}


- (void)clearTmpPics
{
    [MBProgressHUD showMessage:@"正在清除" toView:self.view ];
    [[SDImageCache sharedImageCache] clearMemory];//可有可无
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showSuccess:@"清除成功" toView:self.view];
        
    }];
    float tmpSize = [[SDImageCache sharedImageCache] getSize]/ 1024.0 / 1024.0;
    self.cacheLab.text = [NSString stringWithFormat:@"%.2fM",tmpSize];
}


- (IBAction)exitUser:(UIButton *)sender {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"users"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KAccount];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KPwd];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MJRefreshHeaderLastUpdatedTimeKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popViewControllerAnimated:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addBlankView" object:nil];
    [MobClick profileSignOff];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}




@end
