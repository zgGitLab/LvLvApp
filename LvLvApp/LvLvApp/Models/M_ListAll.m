//
//  M_ListAll.m
//
//  Created by jim  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_ListAll.h"


NSString *const kM_ListAllTID = @"TID";
NSString *const kM_ListAllQuestionContent = @"QuestionContent";
NSString *const kM_ListAllLogoUrl = @"LogoUrl";
NSString *const kM_ListAllAnswerContent = @"AnswerContent";
NSString *const kM_ListAllTagCount = @"TagCount";
NSString *const kM_ListAllCollectCount = @"collectCount";
NSString *const kM_ListAllCounts = @"counts";
NSString *const kM_ListAllTitle = @"Title";
NSString *const kM_ListAllCommentCount = @"commentCount";
NSString *const kM_ListAllAUserID = @"AUserID";
NSString *const kM_ListAllTUserID = @"TUserID";
NSString *const kM_ListAllUserID = @"UserID";
NSString *const kM_ListAllAnswerID = @"Answer_ID";


@interface M_ListAll ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_ListAll

@synthesize tID = _tID;
@synthesize questionContent = _questionContent;
@synthesize logoUrl = _logoUrl;
@synthesize answerContent = _answerContent;
@synthesize tagCount = _tagCount;
@synthesize collectCount = _collectCount;
@synthesize counts = _counts;
@synthesize title = _title;
@synthesize commentCount = _commentCount;
@synthesize aUserID = _aUserID;
@synthesize tUserID = _tUserID;
@synthesize userID = _userID;
@synthesize answerID = _answerID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tID = [self objectOrNilForKey:kM_ListAllTID fromDictionary:dict];
            self.questionContent = [self objectOrNilForKey:kM_ListAllQuestionContent fromDictionary:dict];
            self.logoUrl = [self objectOrNilForKey:kM_ListAllLogoUrl fromDictionary:dict];
            self.answerContent = [self objectOrNilForKey:kM_ListAllAnswerContent fromDictionary:dict];
            self.tagCount = [[self objectOrNilForKey:kM_ListAllTagCount fromDictionary:dict] doubleValue];
            self.collectCount = [[self objectOrNilForKey:kM_ListAllCollectCount fromDictionary:dict] doubleValue];
            self.counts = [[self objectOrNilForKey:kM_ListAllCounts fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kM_ListAllTitle fromDictionary:dict];
            self.commentCount = [[self objectOrNilForKey:kM_ListAllCommentCount fromDictionary:dict] doubleValue];
            self.aUserID = [[self objectOrNilForKey:kM_ListAllAUserID fromDictionary:dict] doubleValue];
            self.tUserID = [[self objectOrNilForKey:kM_ListAllTUserID fromDictionary:dict] doubleValue];
            self.userID = [[self objectOrNilForKey:kM_ListAllUserID fromDictionary:dict] doubleValue];
            self.answerID = [self objectOrNilForKey:kM_ListAllAnswerID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tID forKey:kM_ListAllTID];
    [mutableDict setValue:self.questionContent forKey:kM_ListAllQuestionContent];
    [mutableDict setValue:self.logoUrl forKey:kM_ListAllLogoUrl];
    [mutableDict setValue:self.answerContent forKey:kM_ListAllAnswerContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tagCount] forKey:kM_ListAllTagCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kM_ListAllCollectCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kM_ListAllCounts];
    [mutableDict setValue:self.title forKey:kM_ListAllTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kM_ListAllCommentCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.aUserID] forKey:kM_ListAllAUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tUserID] forKey:kM_ListAllTUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kM_ListAllUserID];
    [mutableDict setValue:self.answerID forKey:kM_ListAllAnswerID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tID = [aDecoder decodeObjectForKey:kM_ListAllTID];
    self.questionContent = [aDecoder decodeObjectForKey:kM_ListAllQuestionContent];
    self.logoUrl = [aDecoder decodeObjectForKey:kM_ListAllLogoUrl];
    self.answerContent = [aDecoder decodeObjectForKey:kM_ListAllAnswerContent];
    self.tagCount = [aDecoder decodeDoubleForKey:kM_ListAllTagCount];
    self.collectCount = [aDecoder decodeDoubleForKey:kM_ListAllCollectCount];
    self.counts = [aDecoder decodeDoubleForKey:kM_ListAllCounts];
    self.title = [aDecoder decodeObjectForKey:kM_ListAllTitle];
    self.commentCount = [aDecoder decodeDoubleForKey:kM_ListAllCommentCount];
    self.aUserID = [aDecoder decodeDoubleForKey:kM_ListAllAUserID];
    self.tUserID = [aDecoder decodeDoubleForKey:kM_ListAllTUserID];
    self.userID = [aDecoder decodeDoubleForKey:kM_ListAllUserID];
    self.answerID = [aDecoder decodeObjectForKey:kM_ListAllAnswerID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tID forKey:kM_ListAllTID];
    [aCoder encodeObject:_questionContent forKey:kM_ListAllQuestionContent];
    [aCoder encodeObject:_logoUrl forKey:kM_ListAllLogoUrl];
    [aCoder encodeObject:_answerContent forKey:kM_ListAllAnswerContent];
    [aCoder encodeDouble:_tagCount forKey:kM_ListAllTagCount];
    [aCoder encodeDouble:_collectCount forKey:kM_ListAllCollectCount];
    [aCoder encodeDouble:_counts forKey:kM_ListAllCounts];
    [aCoder encodeObject:_title forKey:kM_ListAllTitle];
    [aCoder encodeDouble:_commentCount forKey:kM_ListAllCommentCount];
    [aCoder encodeDouble:_aUserID forKey:kM_ListAllAUserID];
    [aCoder encodeDouble:_tUserID forKey:kM_ListAllTUserID];
    [aCoder encodeDouble:_userID forKey:kM_ListAllUserID];
    [aCoder encodeObject:_answerID forKey:kM_ListAllAnswerID];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_ListAll *copy = [[M_ListAll alloc] init];
    
    if (copy) {

        copy.tID = [self.tID copyWithZone:zone];
        copy.questionContent = [self.questionContent copyWithZone:zone];
        copy.logoUrl = [self.logoUrl copyWithZone:zone];
        copy.answerContent = [self.answerContent copyWithZone:zone];
        copy.tagCount = self.tagCount;
        copy.collectCount = self.collectCount;
        copy.counts = self.counts;
        copy.title = [self.title copyWithZone:zone];
        copy.commentCount = self.commentCount;
        copy.aUserID = self.aUserID;
        copy.tUserID = self.tUserID;
        copy.userID = self.userID;
        copy.answerID = [self.answerID copyWithZone:zone];
    }
    
    return copy;
}


@end
