//
//  RTFTextView.m
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/27.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "RTFTextView.h"

@implementation RTFTextView

- (id)initWithFrame:(CGRect)frame andTextColor:(UIColor *)color andFont:(CGFloat)fontSize andText:(NSString *)text withNumOfLines:(NSInteger)lines andImageArray:(NSMutableArray *)imageArray{
    CGFloat height=0;
    NSMutableString *strings = [NSMutableString stringWithString:text];
    
    if (self = [super init]) {
        //创建NSMutableAttributedString实例，并将text传入
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:text];
        //设置行距
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
        [style setLineSpacing:5.0f];
        
        //根据给定长度与style设置attStr式样
        [attStr addAttributes:@{NSForegroundColorAttributeName:color,NSParagraphStyleAttributeName:style,NSFontAttributeName:[UIFont systemFontOfSize:fontSize]} range:NSMakeRange(0, text.length)];
        
        for (int i = 0; i < imageArray.count; i ++) {
            if (![imageArray[i] isEqualToString:@""]) {
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,imageArray[i]]];
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                CGFloat rate = (kWidth-20)/image.size.width;
                image = [Utils thumbnailWithImageWithoutScale:image size:CGSizeMake(kWidth-20, rate*image.size.height)];
                height += image.size.height;
                NSTextAttachment *attatchment = [[NSTextAttachment alloc]init];
                attatchment.image = image;
                NSAttributedString * textAttachmentString = [NSAttributedString attributedStringWithAttachment:attatchment];
                //length = 51
                NSUInteger length = [@"<img src=\"/upload/answer/2015121016061261388.jpg\"/>" length];
                //利用正则表达式匹配字符串
                NSError *error;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<i[^>]+>" options:0 error:&error];
                if (regex != nil) {
                    NSTextCheckingResult  *firstMatch = [regex firstMatchInString:strings options:0 range:NSMakeRange(0, [strings length])];
                    if (firstMatch) {
                        NSRange resultRange = [firstMatch rangeAtIndex:0];
                        NSRange range1 = NSMakeRange(resultRange.location+i, resultRange.length);
                        if (resultRange.length == length) {
                            [strings deleteCharactersInRange:resultRange];
                            [attStr replaceCharactersInRange:range1 withAttributedString:textAttachmentString];
                        }
                        
                    }
                }
                
            }else {
                NSRange range = [strings rangeOfString:@"<img src"];
                if (range.location != NSNotFound) {
                    //length1 = 18
                    NSUInteger length1 = [@"<img src=\"1.jpg\"/>" length];
                    [strings deleteCharactersInRange:NSMakeRange(range.location, length1) ];
                    [attStr replaceCharactersInRange:NSMakeRange(range.location, length1) withAttributedString:[[NSAttributedString alloc] initWithString:@""]];
                }
            }
        }
        
        height += [Utils textHeightFromTextString:text width:frame.size.width fontSize:fontSize];
        //根据给定长度与style设置attStr式样
        [attStr addAttributes:@{NSForegroundColorAttributeName:color,NSParagraphStyleAttributeName:style,NSFontAttributeName:[UIFont systemFontOfSize:fontSize]} range:NSMakeRange(0, attStr.length)];
        //label获取attStr式样
        self.attributedText = attStr;
        CGSize si = [attStr boundingRectWithSize:CGSizeMake(frame.size.width-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
        if (imageArray.count == 0) {
            self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, si.height+20);
        }else{
            self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, si.height+40);//如果高度设的低的话，图片会显示不出来。
        }
//        self.textAlignment = NSTextAlignmentJustified;
        self.textAlignment = NSTextAlignmentLeft;
        self.contentSize = CGSizeMake(self.frame.size.width, si.height+40);
    }
    return self;
}



-(UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;   //返回的就是已经改变的图片
}



@end
