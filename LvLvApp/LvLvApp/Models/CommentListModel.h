//
//  CommentListModel.h
//  LvLvApp
//
//  Created by hope on 15/12/2.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentListModel : NSObject
@property (nonatomic,copy) NSString *IconUrl;
@property (nonatomic,copy) NSString *NickName;
@property (nonatomic,copy) NSString *CommentContent;
@property (nonatomic,copy) NSString *UserID;
@property (nonatomic,copy) NSString *DatePick;
@property (nonatomic,copy) NSString *CommentDT;
@end
