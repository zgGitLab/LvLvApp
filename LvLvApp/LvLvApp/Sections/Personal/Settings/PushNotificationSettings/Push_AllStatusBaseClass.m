//
//  Push_AllStatusBaseClass.m
//
//  Created by IOS8  on 15/12/29
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Push_AllStatusBaseClass.h"


NSString *const kPush_AllStatusBaseClassUserID = @"UserID";
NSString *const kPush_AllStatusBaseClassCommentMe = @"CommentMe";
NSString *const kPush_AllStatusBaseClassSendMe = @"SendMe";
NSString *const kPush_AllStatusBaseClassFocusNewAnwser = @"FocusNewAnwser";
NSString *const kPush_AllStatusBaseClassPraiseMe = @"PraiseMe";
NSString *const kPush_AllStatusBaseClassFocusMe = @"FocusMe";
NSString *const kPush_AllStatusBaseClassID = @"ID";


@interface Push_AllStatusBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Push_AllStatusBaseClass

@synthesize userID = _userID;
@synthesize commentMe = _commentMe;
@synthesize sendMe = _sendMe;
@synthesize focusNewAnwser = _focusNewAnwser;
@synthesize praiseMe = _praiseMe;
@synthesize focusMe = _focusMe;
@synthesize iDProperty = _iDProperty;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userID = [[self objectOrNilForKey:kPush_AllStatusBaseClassUserID fromDictionary:dict] doubleValue];
            self.commentMe = [[self objectOrNilForKey:kPush_AllStatusBaseClassCommentMe fromDictionary:dict] doubleValue];
            self.sendMe = [[self objectOrNilForKey:kPush_AllStatusBaseClassSendMe fromDictionary:dict] doubleValue];
            self.focusNewAnwser = [[self objectOrNilForKey:kPush_AllStatusBaseClassFocusNewAnwser fromDictionary:dict] doubleValue];
            self.praiseMe = [[self objectOrNilForKey:kPush_AllStatusBaseClassPraiseMe fromDictionary:dict] doubleValue];
            self.focusMe = [[self objectOrNilForKey:kPush_AllStatusBaseClassFocusMe fromDictionary:dict] doubleValue];
            self.iDProperty = [[self objectOrNilForKey:kPush_AllStatusBaseClassID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}


#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}



@end
