//
//  IDAttentionBaseClass.m
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IDAttentionBaseClass.h"
#import "IDAttentionData.h"


NSString *const kIDAttentionBaseClassCode = @"Code";
NSString *const kIDAttentionBaseClassMessage = @"Message";
NSString *const kIDAttentionBaseClassData = @"Data";


@interface IDAttentionBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IDAttentionBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kIDAttentionBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kIDAttentionBaseClassMessage fromDictionary:dict];
    NSObject *receivedIDAttentionData = [dict objectForKey:kIDAttentionBaseClassData];
    NSMutableArray *parsedIDAttentionData = [NSMutableArray array];
    if ([receivedIDAttentionData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedIDAttentionData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedIDAttentionData addObject:[IDAttentionData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedIDAttentionData isKindOfClass:[NSDictionary class]]) {
       [parsedIDAttentionData addObject:[IDAttentionData modelObjectWithDictionary:(NSDictionary *)receivedIDAttentionData]];
    }

    self.data = [NSArray arrayWithArray:parsedIDAttentionData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kIDAttentionBaseClassCode];
    [mutableDict setValue:self.message forKey:kIDAttentionBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kIDAttentionBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kIDAttentionBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kIDAttentionBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kIDAttentionBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kIDAttentionBaseClassCode];
    [aCoder encodeObject:_message forKey:kIDAttentionBaseClassMessage];
    [aCoder encodeObject:_data forKey:kIDAttentionBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    IDAttentionBaseClass *copy = [[IDAttentionBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
