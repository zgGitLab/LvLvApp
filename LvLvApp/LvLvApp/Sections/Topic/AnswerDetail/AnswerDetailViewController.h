//
//  AnswerKyeViewController.h
//  LvLvApp
//
//  Created by hope on 15/10/15.
//  Copyright (c) 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^BackBlock)(void);
@interface AnswerDetailViewController : BaseViewController
@property (nonatomic,strong) UIView *mainView;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *write;
@property (nonatomic, strong)   UIScrollView *contentView;
@property (nonatomic, strong) UITextView *textView;

@property (nonatomic,copy) NSString * UserID;//相当于auserid
@property (nonatomic,copy) NSString * answerID;//回答编号
@property (nonatomic,copy) NSString * TID;//问题编号
@property (nonatomic,copy) NSString *tUserID;//问题的UserID
@property (nonatomic,copy) NSString * isEnterFromQuestionPage;
@property (nonatomic,strong) BackBlock backBlock;
@end
