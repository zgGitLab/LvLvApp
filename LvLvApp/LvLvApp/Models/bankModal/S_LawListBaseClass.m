//
//  S_LawListBaseClass.m
//
//  Created by IOS8  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_LawListBaseClass.h"
#import "S_LawListSearch.h"
#import "S_LawListList.h"


NSString *const kS_LawListBaseClassSearch = @"search";
NSString *const kS_LawListBaseClassCount = @"count";
NSString *const kS_LawListBaseClassList = @"list";
NSString *const kS_LawListBaseClassPageindex = @"pageindex";
NSString *const kS_LawListBaseClassPagesize = @"pagesize";


@interface S_LawListBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_LawListBaseClass

@synthesize search = _search;
@synthesize count = _count;
@synthesize list = _list;
@synthesize pageindex = _pageindex;
@synthesize pagesize = _pagesize;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedS_LawListSearch = [dict objectForKey:kS_LawListBaseClassSearch];
    NSMutableArray *parsedS_LawListSearch = [NSMutableArray array];
    if ([receivedS_LawListSearch isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_LawListSearch) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_LawListSearch addObject:[S_LawListSearch modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_LawListSearch isKindOfClass:[NSDictionary class]]) {
       [parsedS_LawListSearch addObject:[S_LawListSearch modelObjectWithDictionary:(NSDictionary *)receivedS_LawListSearch]];
    }

    self.search = [NSArray arrayWithArray:parsedS_LawListSearch];
            self.count = [[self objectOrNilForKey:kS_LawListBaseClassCount fromDictionary:dict] doubleValue];
    NSObject *receivedS_LawListList = [dict objectForKey:kS_LawListBaseClassList];
    NSMutableArray *parsedS_LawListList = [NSMutableArray array];
    if ([receivedS_LawListList isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_LawListList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_LawListList addObject:[S_LawListList modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_LawListList isKindOfClass:[NSDictionary class]]) {
       [parsedS_LawListList addObject:[S_LawListList modelObjectWithDictionary:(NSDictionary *)receivedS_LawListList]];
    }

    self.list = [NSArray arrayWithArray:parsedS_LawListList];
            self.pageindex = [[self objectOrNilForKey:kS_LawListBaseClassPageindex fromDictionary:dict] doubleValue];
            self.pagesize = [[self objectOrNilForKey:kS_LawListBaseClassPagesize fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForSearch = [NSMutableArray array];
    for (NSObject *subArrayObject in self.search) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSearch addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSearch addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSearch] forKey:kS_LawListBaseClassSearch];
    [mutableDict setValue:[NSNumber numberWithDouble:self.count] forKey:kS_LawListBaseClassCount];
    NSMutableArray *tempArrayForList = [NSMutableArray array];
    for (NSObject *subArrayObject in self.list) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForList addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForList] forKey:kS_LawListBaseClassList];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pageindex] forKey:kS_LawListBaseClassPageindex];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pagesize] forKey:kS_LawListBaseClassPagesize];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.search = [aDecoder decodeObjectForKey:kS_LawListBaseClassSearch];
    self.count = [aDecoder decodeDoubleForKey:kS_LawListBaseClassCount];
    self.list = [aDecoder decodeObjectForKey:kS_LawListBaseClassList];
    self.pageindex = [aDecoder decodeDoubleForKey:kS_LawListBaseClassPageindex];
    self.pagesize = [aDecoder decodeDoubleForKey:kS_LawListBaseClassPagesize];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_search forKey:kS_LawListBaseClassSearch];
    [aCoder encodeDouble:_count forKey:kS_LawListBaseClassCount];
    [aCoder encodeObject:_list forKey:kS_LawListBaseClassList];
    [aCoder encodeDouble:_pageindex forKey:kS_LawListBaseClassPageindex];
    [aCoder encodeDouble:_pagesize forKey:kS_LawListBaseClassPagesize];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_LawListBaseClass *copy = [[S_LawListBaseClass alloc] init];
    
    if (copy) {

        copy.search = [self.search copyWithZone:zone];
        copy.count = self.count;
        copy.list = [self.list copyWithZone:zone];
        copy.pageindex = self.pageindex;
        copy.pagesize = self.pagesize;
    }
    
    return copy;
}


@end
