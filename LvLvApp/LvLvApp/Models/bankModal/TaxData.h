//
//  TaxData.h
//
//  Created by IOS8  on 15/10/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface TaxData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double perInjury;
@property (nonatomic, assign) double perJobless;
@property (nonatomic, assign) double houseMinAmount;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, assign) double comJobless;
@property (nonatomic, assign) double insuranceMinAmount;
@property (nonatomic, assign) double comMedical;
@property (nonatomic, assign) double comHouse;
@property (nonatomic, assign) double perHouse;
@property (nonatomic, assign) double comRetire;
@property (nonatomic, strong) NSString *cityZimu;
@property (nonatomic, assign) double perMediccal;
@property (nonatomic, assign) double comBear;
@property (nonatomic, assign) double perRetire;
@property (nonatomic, assign) double otherAmount;
@property (nonatomic, assign) double houseMaxAmount;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, assign) double insuranceMaxAmount;
@property (nonatomic, assign) double comInjury;
@property (nonatomic, assign) double perBear;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
