//
//  AnwerViews.h
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchTopicViewController.h"

#import "M_AnwersData.h"

typedef void (^anwerarrayBlock)(NSMutableArray *);
@protocol AnwerViewDelegate<NSObject>

- (void)Anwerclick:(NSInteger)indxe;

@end
@interface AnwerViews : UIView <UITableViewDataSource,UITableViewDelegate>
typedef  void (^AnwercellClick)(AnwerViews *);
@property (nonatomic, strong)NSString *keyStr;
@property (nonatomic, strong)AnwercellClick AnwercellClick;
@property(nonatomic, unsafe_unretained) id<AnwerViewDelegate>AnwerDelegate;
@property (nonatomic, strong)UITableView *AnwerTableView;
@property (nonatomic, strong)NSMutableArray * AnwerArray;
@property (nonatomic, strong)NSString *searString;
@property (nonatomic,copy) anwerarrayBlock anwBlock;
@property (nonatomic ,strong)M_AnwersData *ANdata;
@property (nonatomic,assign)NSInteger pageIndex;
@property (nonatomic,assign)NSInteger commentSpace;
@property (nonatomic,assign)NSInteger doubleLineHeight1;
@property (nonatomic, assign)NSInteger doubleLineHeight2;
@property (nonatomic, assign)NSInteger  lineSpace ;
@property (nonatomic, assign)NSInteger topSpace;
@property (nonatomic, assign)NSInteger titleSpace;
@property (nonatomic, assign)NSInteger totalHeight;



- (void)getAnwerSearText:(NSString *)strs;
- (id)initWithString:(NSString*)str ;
@end
