//
//  S_CaseDetailSelectVerdicts.m
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_CaseDetailSelectVerdicts.h"


NSString *const kS_CaseDetailSelectVerdictsReason = @"reason";
NSString *const kS_CaseDetailSelectVerdictsContent = @"content";
NSString *const kS_CaseDetailSelectVerdictsCategory = @"category";
NSString *const kS_CaseDetailSelectVerdictsId = @"id";
NSString *const kS_CaseDetailSelectVerdictsCharacter = @"character";
NSString *const kS_CaseDetailSelectVerdictsLevel = @"level";
NSString *const kS_CaseDetailSelectVerdictsTitle = @"title";
NSString *const kS_CaseDetailSelectVerdictsCourt = @"court";


@interface S_CaseDetailSelectVerdicts ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_CaseDetailSelectVerdicts

@synthesize reason = _reason;
@synthesize content = _content;
@synthesize category = _category;
@synthesize selectVerdictsIdentifier = _selectVerdictsIdentifier;
@synthesize character = _character;
@synthesize level = _level;
@synthesize title = _title;
@synthesize court = _court;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.reason = [self objectOrNilForKey:kS_CaseDetailSelectVerdictsReason fromDictionary:dict];
            self.content = [self objectOrNilForKey:kS_CaseDetailSelectVerdictsContent fromDictionary:dict];
            self.category = [self objectOrNilForKey:kS_CaseDetailSelectVerdictsCategory fromDictionary:dict];
            self.selectVerdictsIdentifier = [[self objectOrNilForKey:kS_CaseDetailSelectVerdictsId fromDictionary:dict] doubleValue];
            self.character = [self objectOrNilForKey:kS_CaseDetailSelectVerdictsCharacter fromDictionary:dict];
            self.level = [self objectOrNilForKey:kS_CaseDetailSelectVerdictsLevel fromDictionary:dict];
            self.title = [self objectOrNilForKey:kS_CaseDetailSelectVerdictsTitle fromDictionary:dict];
            self.court = [self objectOrNilForKey:kS_CaseDetailSelectVerdictsCourt fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.reason forKey:kS_CaseDetailSelectVerdictsReason];
    [mutableDict setValue:self.content forKey:kS_CaseDetailSelectVerdictsContent];
    [mutableDict setValue:self.category forKey:kS_CaseDetailSelectVerdictsCategory];
    [mutableDict setValue:[NSNumber numberWithDouble:self.selectVerdictsIdentifier] forKey:kS_CaseDetailSelectVerdictsId];
    [mutableDict setValue:self.character forKey:kS_CaseDetailSelectVerdictsCharacter];
    [mutableDict setValue:self.level forKey:kS_CaseDetailSelectVerdictsLevel];
    [mutableDict setValue:self.title forKey:kS_CaseDetailSelectVerdictsTitle];
    [mutableDict setValue:self.court forKey:kS_CaseDetailSelectVerdictsCourt];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.reason = [aDecoder decodeObjectForKey:kS_CaseDetailSelectVerdictsReason];
    self.content = [aDecoder decodeObjectForKey:kS_CaseDetailSelectVerdictsContent];
    self.category = [aDecoder decodeObjectForKey:kS_CaseDetailSelectVerdictsCategory];
    self.selectVerdictsIdentifier = [aDecoder decodeDoubleForKey:kS_CaseDetailSelectVerdictsId];
    self.character = [aDecoder decodeObjectForKey:kS_CaseDetailSelectVerdictsCharacter];
    self.level = [aDecoder decodeObjectForKey:kS_CaseDetailSelectVerdictsLevel];
    self.title = [aDecoder decodeObjectForKey:kS_CaseDetailSelectVerdictsTitle];
    self.court = [aDecoder decodeObjectForKey:kS_CaseDetailSelectVerdictsCourt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_reason forKey:kS_CaseDetailSelectVerdictsReason];
    [aCoder encodeObject:_content forKey:kS_CaseDetailSelectVerdictsContent];
    [aCoder encodeObject:_category forKey:kS_CaseDetailSelectVerdictsCategory];
    [aCoder encodeDouble:_selectVerdictsIdentifier forKey:kS_CaseDetailSelectVerdictsId];
    [aCoder encodeObject:_character forKey:kS_CaseDetailSelectVerdictsCharacter];
    [aCoder encodeObject:_level forKey:kS_CaseDetailSelectVerdictsLevel];
    [aCoder encodeObject:_title forKey:kS_CaseDetailSelectVerdictsTitle];
    [aCoder encodeObject:_court forKey:kS_CaseDetailSelectVerdictsCourt];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_CaseDetailSelectVerdicts *copy = [[S_CaseDetailSelectVerdicts alloc] init];
    
    if (copy) {

        copy.reason = [self.reason copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
        copy.category = [self.category copyWithZone:zone];
        copy.selectVerdictsIdentifier = self.selectVerdictsIdentifier;
        copy.character = [self.character copyWithZone:zone];
        copy.level = [self.level copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.court = [self.court copyWithZone:zone];
    }
    
    return copy;
}


@end
