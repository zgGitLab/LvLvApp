//
//  FontSizeViewController.m
//  LvLvApp
//
//  Created by jim on 15/12/21.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "FontSizeViewController.h"

@interface FontSizeViewController ()
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic,strong) UILabel *label;
@property (nonatomic,assign) CGFloat width;
@end

@implementation FontSizeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"字体大小";
    [self createExampleView];
    [self createSlideView];
}

//举例view
- (void)createExampleView{

    _label = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, kWidth, 100)];
    _label.text = @"对于法律援助你怎么看";
    _label.textColor = [UIColor colorWithRed:80.0/255.0 green:149.0/255.0 blue:241.0/255.0 alpha:1];
    _label.font = [UIFont systemFontOfSize:[SingeData sharedInstance].answerContentSize];
    _label.textAlignment = NSTextAlignmentCenter;

    [self.view addSubview:_label];
    UILabel *slip = [[UILabel alloc]initWithFrame:CGRectMake(0, 200, kWidth, 50)];
    slip.text = @"按住圆点左右滑动";
    slip.font = [UIFont systemFontOfSize:14];
    slip.textAlignment = NSTextAlignmentCenter;
    slip.textColor = [UIColor lightGrayColor];
    [self.view addSubview:slip];
}


//滑动view
- (void)createSlideView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 514/2, kWidth, 150/2)];

    //背景图片
    UIImage *image = [UIImage imageNamed:@"字体调整"];
    NSLog(@"%f,%f",image.size.width,image.size.height);
    CGSize size ;
    NSLog(@"%f",kWidth);
    size.width = kWidth - 70*2;
    if (kWidth == 320) {
        size.height = 4;
    }else{
        size.height = 6;
    }
    
    self.width = size.width;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((kWidth-size.width)/2, (view.frame.size.height-size.height)/2, size.width, size.height)];
    imageView.image = image;
    CGFloat leading = (kWidth-20*2-10*2-size.width)/2;
    
    
    //滑块设置
    _slider = [[UISlider alloc] initWithFrame:CGRectMake(imageView.frame.origin.x-14, imageView.frame.origin.y-13, size.width+28,33)];
    _slider.minimumValue = 15;
    _slider.maximumValue = 18;
    _slider.value = [SingeData sharedInstance].answerContentSize;
    _slider.minimumTrackTintColor = [UIColor clearColor];
    _slider.maximumTrackTintColor = [UIColor clearColor];
    [_slider addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventValueChanged];
    UITapGestureRecognizer *tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [_slider addGestureRecognizer:tap];
    [view addSubview:_slider];
    
    //大小字
    UILabel *small = [[UILabel alloc]initWithFrame:CGRectMake(leading, 65/2-5, 20, 20)];
    small.text = @"小";
    small.textColor = [UIColor lightGrayColor];
    small.font = [UIFont systemFontOfSize:15];
    [view addSubview:small];
    [view addSubview:imageView];
    UILabel *max = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame)+10, 65/2-5, 20, 20)];
    max.text = @"大";
    max.textColor = [UIColor lightGrayColor];
    max.font = [UIFont systemFontOfSize:18];
    [view addSubview:max];
    
    //上下长线
    UIImageView *line = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 0.5)];
    line.image = [UIImage imageNamed:@"长线"];
    [view addSubview:line];

    UIImageView *line1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 150/2-1, kWidth, 0.5)];
    line1.image = [UIImage imageNamed:@"长线"];

    [view addSubview:line1];
    [self.view addSubview:view];
}

- (void)valueChange:(UISlider *)slider{
    if (slider.value < 15.5f) {
        slider.value = 15.f;
        [SingeData sharedInstance].answerContentSize = 15;
        _label.font = [UIFont systemFontOfSize:15];
    }else if(slider.value <=16.5){
        slider.value = 16.f;
        [SingeData sharedInstance].answerContentSize = 16;
        _label.font = [UIFont systemFontOfSize:16];
    }else if(slider.value <17.5){
        slider.value = 17.f;
        [SingeData sharedInstance].answerContentSize = 17;
        _label.font = [UIFont systemFontOfSize:17];
    }else{
        slider.value = 18.f;
        [SingeData sharedInstance].answerContentSize = 18;
        _label.font = [UIFont systemFontOfSize:18];
    }
    NSLog(@"slider.value%f",slider.value);
}


- (void)tapAction:(UITapGestureRecognizer *)tap{
    //取得点击点
    CGPoint p = [tap locationInView:_slider];
    CGFloat wid =self.width/3;
    CGFloat wid1=14+wid/2;
    NSLog(@"%lf",p.x);
    if (p.x < wid1) {
        _slider.value = 15.f;
        [SingeData sharedInstance].answerContentSize = 15;
        _label.font = [UIFont systemFontOfSize:15];
    }else if(p.x <=wid1+wid){
        _slider.value = 16.f;
        [SingeData sharedInstance].answerContentSize = 16;
        _label.font = [UIFont systemFontOfSize:16];
    }else if(p.x <wid1+wid*2){
        _slider.value = 17.f;
        [SingeData sharedInstance].answerContentSize = 17;
        _label.font = [UIFont systemFontOfSize:17];
    }else{
        _slider.value = 18.f;
        [SingeData sharedInstance].answerContentSize = 18;
        _label.font = [UIFont systemFontOfSize:18];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
