//
//  Anser_labData.h
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Anser_labData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double tagID;
@property (nonatomic, assign) double commentCount;
@property (nonatomic, assign) double isFocus;
@property (nonatomic, assign) double questionCount;
@property (nonatomic, strong) NSString *tagName;
@property (nonatomic, assign) double focusTag;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
