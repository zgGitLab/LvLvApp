//
//  AnswerAndLabelViewController.m
//  LvLvApp
//
//  Created by hope on 15/11/30.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "AnswerAndLabelViewController.h"
#import "AnwerTableViewCell.h"
#import "LableTableViewCell.h"
#import "Anser_labDatamodal.h"
#import "LabelDetailViewController.h"
#import "LabelViewController.h"

@interface AnswerAndLabelViewController ()<UITableViewDataSource,UITableViewDelegate>{

    UIButton *labelBtn;
}

@property (nonatomic,strong) UITableView *labelTableView;
@property (nonatomic,strong) NSArray *dataArray;



@end

@implementation AnswerAndLabelViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
    self.dataArray = [[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden=NO;
    UIBarButtonItem *item =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"返回箭头"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = item;
    if ([[NSString stringWithFormat:@"%@",self.tUserID] isEqualToString:kUserID]) {
        UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"加"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]style:UIBarButtonItemStylePlain target:self action:@selector(addLabel)];
        self.navigationItem.rightBarButtonItem = rightItem;
    }
    
    
    self.title = @"问题的标签";
    [self getData];
    [self createTableView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark --请求数据
- (void)getData{
    
    //传入的参数
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:self.userID forKey:@"UserID"];
    [parameters setObject:self.TID forKey:@"TID"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:kAnswer_LabUrl,K_IP] params:parameters success:^(id json) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        Anser_labBaseClass *dataModel  = [[Anser_labBaseClass alloc]initWithDictionary:dict];
        _dataArray = dataModel.data;
        [self performSelectorOnMainThread:@selector(refreshUI) withObject:nil waitUntilDone:YES];
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark -- 关注请求
- (void)concernRequestWith:(NSInteger)T andTagName:(NSString *)tagname{

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:tagname forKey:@"TagName"];
    [parameters setObject:kUserID forKey:@"UserID"];
    [parameters setObject:[NSNumber numberWithInteger:T] forKey:@"T"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:kConcernLabelUrl,K_IP] params:parameters success:^(id json) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"message :%@",dict[@"Message"]);
    } failure:^(NSError *error) {
    }];
}

- (void)refreshUI
{
    [self.labelTableView reloadData];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -- 创建tableview
- (void)createTableView{
    self.labelTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight) style:UITableViewStylePlain];
    _labelTableView.delegate = self;
    _labelTableView.dataSource = self;
    [self.view addSubview:_labelTableView];
}


#pragma mark -tableViewdelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LableTableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"LableID"];
    if (!cell2) {
        cell2 = [[NSBundle mainBundle]loadNibNamed:@"LableTableViewCell" owner:self options:nil][0];
    }
    if (_dataArray.count > 0) {
        Anser_labData *labData = [_dataArray objectAtIndex:indexPath.row];
        cell2.LabetagName.text = labData.tagName;
        cell2.AttentionProblem.text = [NSString stringWithFormat:@"关注 %.0f · 问题 %.0f · 评论 %.0f",labData.focusTag,labData.questionCount,labData.commentCount];
        
        cell2.lableAttention.tag = indexPath.row;
        [cell2.lableAttention addTarget:self action:@selector(concernClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell2.lableAttention setImage:[UIImage imageNamed:@"关注"] forState:UIControlStateNormal];
        [cell2.lableAttention setImage:[UIImage imageNamed:@"已关注1"] forState:UIControlStateSelected];
        if(labData.isFocus == 0){
            cell2.lableAttention.selected = NO;
        }else{
            cell2.lableAttention.selected = YES;
        }

    }
        return cell2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *v = [[UIView alloc]init];
    v.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1];
    return v;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LableTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    if ([kUserID integerValue] == 0) {
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }else{
        LabelDetailViewController *labelDetail = [[LabelDetailViewController alloc]init];
        Anser_labData *labData = [_dataArray objectAtIndex:indexPath.row];
        labelDetail.labName = labData.tagName;
        labelDetail.Keywordstr = labData.tagName;
        labelDetail.UserID = self.userID;
        [self.navigationController pushViewController:labelDetail animated:YES];
    }
    
}

//关注事件
- (void)concernClick:(UIButton *)button{
    if ([kUserID integerValue] == 0) {
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }else{
        Anser_labData *labData = [_dataArray objectAtIndex:button.tag];
        if (button.selected) {//取消关注
            [self concernRequestWith:0 andTagName:labData.tagName];
            
        }else{
            [self concernRequestWith:1 andTagName:labData.tagName];
        }
        button.selected = !button.selected;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//进入添加标签页
- (void)addLabel{
    if ([kUserID integerValue] == 0) {
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }else{
        LabelViewController *label = [[LabelViewController alloc]init];
        label.tID = self.TID;
        label.userID = self.userID;
        label.refreshEvents = ^{
            [self getData];
            [self performSelector:@selector(refreshUI) withObject:nil];
        };
        [self.navigationController pushViewController:label animated:YES];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
