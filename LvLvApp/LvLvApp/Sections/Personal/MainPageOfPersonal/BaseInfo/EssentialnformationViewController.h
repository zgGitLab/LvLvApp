//
//  EssentialnformationViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EssentialnformationViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)  NSString *userID;
@property (weak, nonatomic) IBOutlet UITableView *infoTab;

@end
