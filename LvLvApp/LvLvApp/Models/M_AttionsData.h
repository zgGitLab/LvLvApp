//
//  M_AttionsData.h
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface M_AttionsData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double userID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double answerCount;
@property (nonatomic, assign) double tUserID;
@property (nonatomic, assign) double tagCount;
@property (nonatomic, strong) NSString *tID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
