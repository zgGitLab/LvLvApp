//
//  M_IDGainData.m
//
//  Created by hope  on 15/12/25
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_IDGainData.h"


NSString *const kM_IDGainDataImageUrl = @"ImageUrl";
NSString *const kM_IDGainDataUserID = @"UserID";
NSString *const kM_IDGainDataIsCheck = @"IsCheck";
NSString *const kM_IDGainDataRemark = @"Remark";
NSString *const kM_IDGainDataProfession = @"Profession";
NSString *const kM_IDGainDataIsPass = @"IsPass";
NSString *const kM_IDGainDataCompanyName = @"CompanyName";
NSString *const kM_IDGainDataUserName = @"UserName";


@interface M_IDGainData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_IDGainData

@synthesize imageUrl = _imageUrl;
@synthesize userID = _userID;
@synthesize isCheck = _isCheck;
@synthesize remark = _remark;
@synthesize profession = _profession;
@synthesize isPass = _isPass;
@synthesize companyName = _companyName;
@synthesize userName = _userName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.imageUrl = [self objectOrNilForKey:kM_IDGainDataImageUrl fromDictionary:dict];
            self.userID = [[self objectOrNilForKey:kM_IDGainDataUserID fromDictionary:dict] doubleValue];
            self.isCheck = [[self objectOrNilForKey:kM_IDGainDataIsCheck fromDictionary:dict] doubleValue];
            self.remark = [self objectOrNilForKey:kM_IDGainDataRemark fromDictionary:dict];
            self.profession = [self objectOrNilForKey:kM_IDGainDataProfession fromDictionary:dict];
            self.isPass = [[self objectOrNilForKey:kM_IDGainDataIsPass fromDictionary:dict] doubleValue];
            self.companyName = [self objectOrNilForKey:kM_IDGainDataCompanyName fromDictionary:dict];
            self.userName = [self objectOrNilForKey:kM_IDGainDataUserName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.imageUrl forKey:kM_IDGainDataImageUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kM_IDGainDataUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isCheck] forKey:kM_IDGainDataIsCheck];
    [mutableDict setValue:self.remark forKey:kM_IDGainDataRemark];
    [mutableDict setValue:self.profession forKey:kM_IDGainDataProfession];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isPass] forKey:kM_IDGainDataIsPass];
    [mutableDict setValue:self.companyName forKey:kM_IDGainDataCompanyName];
    [mutableDict setValue:self.userName forKey:kM_IDGainDataUserName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.imageUrl = [aDecoder decodeObjectForKey:kM_IDGainDataImageUrl];
    self.userID = [aDecoder decodeDoubleForKey:kM_IDGainDataUserID];
    self.isCheck = [aDecoder decodeDoubleForKey:kM_IDGainDataIsCheck];
    self.remark = [aDecoder decodeObjectForKey:kM_IDGainDataRemark];
    self.profession = [aDecoder decodeObjectForKey:kM_IDGainDataProfession];
    self.isPass = [aDecoder decodeDoubleForKey:kM_IDGainDataIsPass];
    self.companyName = [aDecoder decodeObjectForKey:kM_IDGainDataCompanyName];
    self.userName = [aDecoder decodeObjectForKey:kM_IDGainDataUserName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_imageUrl forKey:kM_IDGainDataImageUrl];
    [aCoder encodeDouble:_userID forKey:kM_IDGainDataUserID];
    [aCoder encodeDouble:_isCheck forKey:kM_IDGainDataIsCheck];
    [aCoder encodeObject:_remark forKey:kM_IDGainDataRemark];
    [aCoder encodeObject:_profession forKey:kM_IDGainDataProfession];
    [aCoder encodeDouble:_isPass forKey:kM_IDGainDataIsPass];
    [aCoder encodeObject:_companyName forKey:kM_IDGainDataCompanyName];
    [aCoder encodeObject:_userName forKey:kM_IDGainDataUserName];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_IDGainData *copy = [[M_IDGainData alloc] init];
    
    if (copy) {

        copy.imageUrl = [self.imageUrl copyWithZone:zone];
        copy.userID = self.userID;
        copy.isCheck = self.isCheck;
        copy.remark = [self.remark copyWithZone:zone];
        copy.profession = [self.profession copyWithZone:zone];
        copy.isPass = self.isPass;
        copy.companyName = [self.companyName copyWithZone:zone];
        copy.userName = [self.userName copyWithZone:zone];
    }
    
    return copy;
}


@end
