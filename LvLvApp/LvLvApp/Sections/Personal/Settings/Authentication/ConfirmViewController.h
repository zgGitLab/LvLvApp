//
//  ConfirmViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/10.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmViewController : BaseViewController<PerPickViewDelegat,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *ComImportName;//真实姓名
@property (strong, nonatomic) IBOutlet UITextField *ComImportComrpany;//工作单位

@property (strong, nonatomic) IBOutlet UIButton *ComImportComrpanys;//身份

@property (strong, nonatomic) IBOutlet UIImageView *ComUpImageView;//证件照片


- (IBAction)ComReferBut:(UIButton *)sender;

- (IBAction)ComUpImagAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *comupimage;
@property (strong, nonatomic) IBOutlet UIScrollView *ConScrollView;

@property (weak, nonatomic) IBOutlet UIButton *ConBut;

@property (nonatomic, strong) NSDictionary *userMessage;

@end
