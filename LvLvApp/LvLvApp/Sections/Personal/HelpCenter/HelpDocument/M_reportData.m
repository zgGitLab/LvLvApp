//
//  M_reportData.m
//
//  Created by hope  on 15/12/25
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_reportData.h"


NSString *const kM_reportDataUserID = @"UserID";
NSString *const kM_reportDataReportDate = @"ReportDate";
NSString *const kM_reportDataCategory = @"Category";
NSString *const kM_reportDataDisposeResult = @"DisposeResult";
NSString *const kM_reportDataRemarkContent = @"RemarkContent";


@interface M_reportData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_reportData

@synthesize userID = _userID;
@synthesize reportDate = _reportDate;
@synthesize category = _category;
@synthesize disposeResult = _disposeResult;
@synthesize remarkContent = _remarkContent;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userID = [[self objectOrNilForKey:kM_reportDataUserID fromDictionary:dict] doubleValue];
            self.reportDate = [self objectOrNilForKey:kM_reportDataReportDate fromDictionary:dict];
            self.category = [self objectOrNilForKey:kM_reportDataCategory fromDictionary:dict];
            self.disposeResult = [self objectOrNilForKey:kM_reportDataDisposeResult fromDictionary:dict];
            self.remarkContent = [self objectOrNilForKey:kM_reportDataRemarkContent fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kM_reportDataUserID];
    [mutableDict setValue:self.reportDate forKey:kM_reportDataReportDate];
    [mutableDict setValue:self.category forKey:kM_reportDataCategory];
    [mutableDict setValue:self.disposeResult forKey:kM_reportDataDisposeResult];
    [mutableDict setValue:self.remarkContent forKey:kM_reportDataRemarkContent];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userID = [aDecoder decodeDoubleForKey:kM_reportDataUserID];
    self.reportDate = [aDecoder decodeObjectForKey:kM_reportDataReportDate];
    self.category = [aDecoder decodeObjectForKey:kM_reportDataCategory];
    self.disposeResult = [aDecoder decodeObjectForKey:kM_reportDataDisposeResult];
    self.remarkContent = [aDecoder decodeObjectForKey:kM_reportDataRemarkContent];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_userID forKey:kM_reportDataUserID];
    [aCoder encodeObject:_reportDate forKey:kM_reportDataReportDate];
    [aCoder encodeObject:_category forKey:kM_reportDataCategory];
    [aCoder encodeObject:_disposeResult forKey:kM_reportDataDisposeResult];
    [aCoder encodeObject:_remarkContent forKey:kM_reportDataRemarkContent];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_reportData *copy = [[M_reportData alloc] init];
    
    if (copy) {

        copy.userID = self.userID;
        copy.reportDate = [self.reportDate copyWithZone:zone];
        copy.category = [self.category copyWithZone:zone];
        copy.disposeResult = [self.disposeResult copyWithZone:zone];
        copy.remarkContent = [self.remarkContent copyWithZone:zone];
    }
    
    return copy;
}


@end
