//
//  M_GambitListAll.m
//
//  Created by jim  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_GambitListAll.h"


NSString *const kM_GambitListAllTitle = @"Title";
NSString *const kM_GambitListAllUserid = @"Userid";
NSString *const kM_GambitListAllNotifyID = @"NotifyID";
NSString *const kM_GambitListAllDescription = @"Description";
NSString *const kM_GambitListAllAUserID = @"AUserID";
NSString *const kM_GambitListAllCommentCount = @"commentCount";
NSString *const kM_GambitListAllTimagesUrl = @"TimagesUrl";
NSString *const kM_GambitListAllLogoUrl = @"LogoUrl";
NSString *const kM_GambitListAllQuestionContent = @"QuestionContent";
NSString *const kM_GambitListAllAnswerContent = @"AnswerContent";
NSString *const kM_GambitListAllCollectCount = @"collectCount";
NSString *const kM_GambitListAllSortTime = @"sortTime";
NSString *const kM_GambitListAllTimes = @"times";
NSString *const kM_GambitListAllNickname = @"Nickname";
NSString *const kM_GambitListAllTypes = @"types";
NSString *const kM_GambitListAllCounts = @"counts";
NSString *const kM_GambitListAllTUserID = @"TUserID";
NSString *const kM_GambitListAllTID = @"TID";
NSString *const kM_GambitListAllAnswerID = @"Answer_ID";


@interface M_GambitListAll ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_GambitListAll

@synthesize title = _title;
@synthesize userid = _userid;
@synthesize notifyID = _notifyID;
@synthesize listAllDescription = _listAllDescription;
@synthesize aUserID = _aUserID;
@synthesize commentCount = _commentCount;
@synthesize timagesUrl = _timagesUrl;
@synthesize logoUrl = _logoUrl;
@synthesize questionContent = _questionContent;
@synthesize answerContent = _answerContent;
@synthesize collectCount = _collectCount;
@synthesize sortTime = _sortTime;
@synthesize times = _times;
@synthesize nickname = _nickname;
@synthesize types = _types;
@synthesize counts = _counts;
@synthesize tUserID = _tUserID;
@synthesize tID = _tID;
@synthesize answerID = _answerID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.title = [self objectOrNilForKey:kM_GambitListAllTitle fromDictionary:dict];
            self.userid = [[self objectOrNilForKey:kM_GambitListAllUserid fromDictionary:dict] doubleValue];
            self.notifyID = [[self objectOrNilForKey:kM_GambitListAllNotifyID fromDictionary:dict] doubleValue];
            self.listAllDescription = [self objectOrNilForKey:kM_GambitListAllDescription fromDictionary:dict];
            self.aUserID = [[self objectOrNilForKey:kM_GambitListAllAUserID fromDictionary:dict] doubleValue];
            self.commentCount = [[self objectOrNilForKey:kM_GambitListAllCommentCount fromDictionary:dict] doubleValue];
            self.timagesUrl = [self objectOrNilForKey:kM_GambitListAllTimagesUrl fromDictionary:dict];
            self.logoUrl = [self objectOrNilForKey:kM_GambitListAllLogoUrl fromDictionary:dict];
            self.questionContent = [self objectOrNilForKey:kM_GambitListAllQuestionContent fromDictionary:dict];
            self.answerContent = [self objectOrNilForKey:kM_GambitListAllAnswerContent fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kM_GambitListAllCollectCount fromDictionary:dict] doubleValue];
            self.sortTime = [self objectOrNilForKey:kM_GambitListAllSortTime fromDictionary:dict];
            self.times = [self objectOrNilForKey:kM_GambitListAllTimes fromDictionary:dict];
            self.nickname = [self objectOrNilForKey:kM_GambitListAllNickname fromDictionary:dict];
            self.types = [[self objectOrNilForKey:kM_GambitListAllTypes fromDictionary:dict] doubleValue];
            self.counts = [[self objectOrNilForKey:kM_GambitListAllCounts fromDictionary:dict] doubleValue];
            self.tUserID = [[self objectOrNilForKey:kM_GambitListAllTUserID fromDictionary:dict] doubleValue];
            self.tID = [self objectOrNilForKey:kM_GambitListAllTID fromDictionary:dict];
            self.answerID = [self objectOrNilForKey:kM_GambitListAllAnswerID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.title forKey:kM_GambitListAllTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userid] forKey:kM_GambitListAllUserid];
    [mutableDict setValue:[NSNumber numberWithDouble:self.notifyID] forKey:kM_GambitListAllNotifyID];
    [mutableDict setValue:self.listAllDescription forKey:kM_GambitListAllDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.aUserID] forKey:kM_GambitListAllAUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kM_GambitListAllCommentCount];
    [mutableDict setValue:self.timagesUrl forKey:kM_GambitListAllTimagesUrl];
    [mutableDict setValue:self.logoUrl forKey:kM_GambitListAllLogoUrl];
    [mutableDict setValue:self.questionContent forKey:kM_GambitListAllQuestionContent];
    [mutableDict setValue:self.answerContent forKey:kM_GambitListAllAnswerContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kM_GambitListAllCollectCount];
    [mutableDict setValue:self.sortTime forKey:kM_GambitListAllSortTime];
    [mutableDict setValue:self.times forKey:kM_GambitListAllTimes];
    [mutableDict setValue:self.nickname forKey:kM_GambitListAllNickname];
    [mutableDict setValue:[NSNumber numberWithDouble:self.types] forKey:kM_GambitListAllTypes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kM_GambitListAllCounts];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tUserID] forKey:kM_GambitListAllTUserID];
    [mutableDict setValue:self.tID forKey:kM_GambitListAllTID];
    [mutableDict setValue:self.answerID forKey:kM_GambitListAllAnswerID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.title = [aDecoder decodeObjectForKey:kM_GambitListAllTitle];
    self.userid = [aDecoder decodeDoubleForKey:kM_GambitListAllUserid];
    self.notifyID = [aDecoder decodeDoubleForKey:kM_GambitListAllNotifyID];
    self.listAllDescription = [aDecoder decodeObjectForKey:kM_GambitListAllDescription];
    self.aUserID = [aDecoder decodeDoubleForKey:kM_GambitListAllAUserID];
    self.commentCount = [aDecoder decodeDoubleForKey:kM_GambitListAllCommentCount];
    self.timagesUrl = [aDecoder decodeObjectForKey:kM_GambitListAllTimagesUrl];
    self.logoUrl = [aDecoder decodeObjectForKey:kM_GambitListAllLogoUrl];
    self.questionContent = [aDecoder decodeObjectForKey:kM_GambitListAllQuestionContent];
    self.answerContent = [aDecoder decodeObjectForKey:kM_GambitListAllAnswerContent];
    self.collectCount = [aDecoder decodeDoubleForKey:kM_GambitListAllCollectCount];
    self.sortTime = [aDecoder decodeObjectForKey:kM_GambitListAllSortTime];
    self.times = [aDecoder decodeObjectForKey:kM_GambitListAllTimes];
    self.nickname = [aDecoder decodeObjectForKey:kM_GambitListAllNickname];
    self.types = [aDecoder decodeDoubleForKey:kM_GambitListAllTypes];
    self.counts = [aDecoder decodeDoubleForKey:kM_GambitListAllCounts];
    self.tUserID = [aDecoder decodeDoubleForKey:kM_GambitListAllTUserID];
    self.tID = [aDecoder decodeObjectForKey:kM_GambitListAllTID];
    self.answerID = [aDecoder decodeObjectForKey:kM_GambitListAllAnswerID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_title forKey:kM_GambitListAllTitle];
    [aCoder encodeDouble:_userid forKey:kM_GambitListAllUserid];
    [aCoder encodeDouble:_notifyID forKey:kM_GambitListAllNotifyID];
    [aCoder encodeObject:_listAllDescription forKey:kM_GambitListAllDescription];
    [aCoder encodeDouble:_aUserID forKey:kM_GambitListAllAUserID];
    [aCoder encodeDouble:_commentCount forKey:kM_GambitListAllCommentCount];
    [aCoder encodeObject:_timagesUrl forKey:kM_GambitListAllTimagesUrl];
    [aCoder encodeObject:_logoUrl forKey:kM_GambitListAllLogoUrl];
    [aCoder encodeObject:_questionContent forKey:kM_GambitListAllQuestionContent];
    [aCoder encodeObject:_answerContent forKey:kM_GambitListAllAnswerContent];
    [aCoder encodeDouble:_collectCount forKey:kM_GambitListAllCollectCount];
    [aCoder encodeObject:_sortTime forKey:kM_GambitListAllSortTime];
    [aCoder encodeObject:_times forKey:kM_GambitListAllTimes];
    [aCoder encodeObject:_nickname forKey:kM_GambitListAllNickname];
    [aCoder encodeDouble:_types forKey:kM_GambitListAllTypes];
    [aCoder encodeDouble:_counts forKey:kM_GambitListAllCounts];
    [aCoder encodeDouble:_tUserID forKey:kM_GambitListAllTUserID];
    [aCoder encodeObject:_tID forKey:kM_GambitListAllTID];
    [aCoder encodeObject:_answerID forKey:kM_GambitListAllAnswerID];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_GambitListAll *copy = [[M_GambitListAll alloc] init];
    
    if (copy) {

        copy.title = [self.title copyWithZone:zone];
        copy.userid = self.userid;
        copy.notifyID = self.notifyID;
        copy.listAllDescription = [self.listAllDescription copyWithZone:zone];
        copy.aUserID = self.aUserID;
        copy.commentCount = self.commentCount;
        copy.timagesUrl = [self.timagesUrl copyWithZone:zone];
        copy.logoUrl = [self.logoUrl copyWithZone:zone];
        copy.questionContent = [self.questionContent copyWithZone:zone];
        copy.answerContent = [self.answerContent copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.sortTime = [self.sortTime copyWithZone:zone];
        copy.times = [self.times copyWithZone:zone];
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.types = self.types;
        copy.counts = self.counts;
        copy.tUserID = self.tUserID;
        copy.tID = [self.tID copyWithZone:zone];
        copy.answerID = [self.answerID copyWithZone:zone];
    }
    
    return copy;
}


@end
