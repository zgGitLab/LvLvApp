//
//  JobTableViewCell.m
//  LvLvApp
//
//  Created by Sunny on 16/3/29.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "JobTableViewCell.h"

@implementation JobTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setModel:(JobLIstModel *)model
{
     _model = model;
    _moneyLab.text = _model.Salary;
    _timeLab.text = [_model.CreateTime componentsSeparatedByString:@" "][0];
    _locationLab.text = _model.AddressLocation;
    if (!self.isSearchList) {
        _jobNameLab.text = _model.JobName;
        _companyName.text = _model.CompanyName;
    }else{
       
       
        
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
