//
//  CustomTabBarController.h
//  LawApp
//
//  Created by yfzx_sh_gaoyy on 15/10/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CJNavigationController.h"
@interface CustomTabBarController : UITabBarController
@property (nonatomic, strong) CJNavigationController *navc1;
@property (nonatomic, strong) CJNavigationController *navc2;
@property (nonatomic, strong) CJNavigationController *navc4;
@end
