//
//  S_ChatListData.h
//
//  Created by IOS8  on 15/12/23
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_ChatListData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double recID;
@property (nonatomic, assign) double sendID;
@property (nonatomic, strong) NSString *sendTime;
@property (nonatomic, strong) NSString *textContent;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
