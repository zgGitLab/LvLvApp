//
//  EditeducationInfoViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/10.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "EditeducationInfoViewController.h"
#import "EditTimeView.h"
@interface EditeducationInfoViewController ()<PerTimeDatePickViewDelegat>

@end

@implementation EditeducationInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"编辑教育信息";
    UIButton *finshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    finshButton.frame = CGRectMake(0, 0, 40, 15);
    [finshButton setTitle:@"完成" forState:UIControlStateNormal];
    [finshButton setTitleColor:[UIColor colorWithRed:65/255.0 green:71/255.0 blue:83/255.0 alpha:1] forState:UIControlStateNormal];
    [finshButton addTarget:self action:@selector(finshBtn:) forControlEvents:UIControlEventTouchUpInside];
    finshButton.titleLabel.font = [UIFont systemFontOfSize:18];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:finshButton];
    if (!self.ishide) {
        [self getTitleName];
    }
    self.removeBtn.hidden = self.ishide;
    //添加一个监听者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)getTitleName
{
    self.schoolName.text = self.eduModal.university;
    if (![self.eduModal.admissionDate isEqualToString:@""] && self.eduModal.admissionDate != nil) {
        [self.timeBtn setTitle:self.eduModal.admissionDate forState:UIControlStateNormal];
    }
    self.categorySchool.text = self.eduModal.institute;
    
}

- (void)finshBtn:(UIButton *)btn
{
    if ([self.schoolName.text isEqualToString:@""]) {
        K_ALERT(@"请输入学校名称");
        return;
    }
    if (self.ishide) {
        [RequestManager addEduMessageWithUser:[self getUserId] andSchoolName:_schoolName.text andstartTime:_timeBtn.titleLabel.text andInstitute:_categorySchool.text andBlock:^(NSString *message) {
            if ([message isEqualToString:@"成功"]) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [MBProgressHUD showOlnyMessage:message toView:self.view];
            }
        }];
    }else{
        [RequestManager changeEduMessageWithUser:_eduModal.iDProperty andSchoolName:_schoolName.text andstartTime:_timeBtn.titleLabel.text andInstitute:_categorySchool.text andBlock:^(NSString *message) {
            if ([message isEqualToString:@"成功"]) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [MBProgressHUD showOlnyMessage:message toView:self.view];
            }
        }];
    }
   
}

- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}

- (IBAction)startTimeBtn:(UIButton *)sender
{
    EditTimeView *editTime = [[EditTimeView alloc] initWithCount:1];
    editTime.delegate = self;
    [editTime showInView];
    
}
- (IBAction)removeBtnClick:(UIButton *)sender {
    
    [RequestManager removeEduMessageWithUser:self.eduModal.iDProperty andBlock:^(NSString *message) {
        if ([message isEqualToString:@"删除成功"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showOlnyMessage:message toView:self.view];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark PerTimeDatePickViewDelegat
- (void)selectTimeDate:(NSString *)timeDate
{
    [self.timeBtn setTitle:timeDate forState:UIControlStateNormal];
    //    self.timeBtn.titleLabel.text = timeDate;
}

#pragma mark ----监听事件----
- (void)keyBoardFrameChange:(NSNotification* )notification
{
    //获取键盘上来的时间和键盘的大小
    double time = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect;
    [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&rect];
    static UIView *registVI = nil;
    if (registVI == nil) {
        registVI = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, SIZE.height)];
        registVI.backgroundColor = [UIColor colorWithRed:70/255.0 green:70/255.0 blue:70/255.0 alpha:0];
        //        registVI.alpha = 0.1;
        [self.view addSubview:registVI];
        
    }
    
    if (rect.origin.y == SIZE.height) {
        [registVI removeFromSuperview];
        registVI = nil;
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:7];
    [UIView setAnimationDuration:time];
    [UIView commitAnimations];
}

@end
