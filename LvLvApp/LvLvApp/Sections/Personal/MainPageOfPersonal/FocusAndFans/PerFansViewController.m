//
//  PerFansViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "PerFansViewController.h"
#import "FriendTableViewCell.h"
#import "OtherPerMainViewController.h"
#import "PerMainPageViewController.h"
@interface PerFansViewController ()
{
    NSMutableArray *dataArray;
    int page;
}
@end

@implementation PerFansViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"粉丝";
    dataArray = [NSMutableArray array];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
    UIView *footVi = [[UIView alloc] init];
    self.fansTab.tableFooterView = footVi;
    self.fansTab.separatorInset = UIEdgeInsetsMake(0, 5, 0, 8);
    __weak PerFansViewController *VC = self;
    // 上拉加载
    self.fansTab.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 结束刷新
        [VC infinite];
    }];
    self.fansTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        page = 1;
        NSString *pageIndex = [NSString stringWithFormat:@"%d",page];
        [VC requestFansData:pageIndex];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    page = 1;
//    NSString *pageIndex = [NSString stringWithFormat:@"%d",page];
    [self requestFansData:@"1"];
}

- (void)infinite
{
    page += 1;
    NSString *pageIndex = [NSString stringWithFormat:@"%d",page];
    [self requestFansData:pageIndex];
}

- (void)requestFansData:(NSString *)pageIndex
{
    __weak PerFansViewController *VC = self;
    [RequestManager myFansWithUser:self.userID andPageIndex:pageIndex andPageSize:@"10" andBlock:^(NSArray *dataArr,NSString *message) {
        [VC.fansTab.mj_header endRefreshing];
        [VC.fansTab.mj_footer endRefreshing];
        if ([pageIndex isEqualToString:@"1"]) {
            [dataArray removeAllObjects];
            dataArray = [NSMutableArray arrayWithArray:dataArr];
        }else{
            [dataArray addObjectsFromArray:dataArr];
        }
        [VC performSelectorOnMainThread:@selector(refreshAnserUI) withObject:nil waitUntilDone:YES];
        
    }];
}

- (void)refreshAnserUI
{
    [self.fansTab reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"friendcell";
    FriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil][0];
        // 如果是自己的粉丝列表，则显示是否关注，如果不是则不显示；
        if (![self.userID isEqualToString:[self getUserId]]) {
            cell.privateMessBtn.hidden = YES;
        }

    }
    S_MyFansData *fansData = dataArray[indexPath.row];
    NSString *imageName = fansData.isFocus == 1?@"已关注1":@"关注";
    [cell.privateMessBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    cell.privateMessBtn.selected = fansData.isFocus == 1?YES:NO;
    [cell.privateMessBtn addTarget:self action:@selector(fansBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.privateMessBtn.tag = fansData.iDProperty;
    [cell.titleIcon sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,fansData.image]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"头像"]];
    cell.titleIcon.layer.cornerRadius = 26.5;
    cell.titleIcon.clipsToBounds = YES;
    cell.titleStr.text = fansData.nickName;
    cell.conStr.text = fansData.introduction;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    S_MyFansData *fansData = dataArray[indexPath.row];
    if ([[self getUserId] isEqualToString:[NSString stringWithFormat:@"%.f",fansData.iDProperty]]) {
        PerMainPageViewController *perVC = [[PerMainPageViewController alloc] init];
        perVC.userID = [NSString stringWithFormat:@"%.f",fansData.iDProperty];
        [self.navigationController pushViewController:perVC animated:YES];
    }else{
        OtherPerMainViewController *otherVC = [[OtherPerMainViewController alloc] init];
        otherVC.otherID = [NSString stringWithFormat:@"%.f",fansData.iDProperty];
        otherVC.userID = [self getUserId];
        [self.navigationController pushViewController:otherVC animated:YES];
    }
    
}

- (void)fansBtnClick:(UIButton *)btn
{
    NSString *otherID = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    if (btn.selected) {
        
        [RequestManager perConcernOther:[self getUserId] andOtherID:otherID andIsConcern:@"0" andBlock:^( NSString *message) {
            if ([message isEqualToString:@"成功"]) {
                [btn setImage:[UIImage imageNamed:@"关注"] forState:UIControlStateNormal];
                btn.selected = NO;
            }else{
                 [MBProgressHUD showOlnyMessage:message toView:self.view];
            }
            
        }];
    }else{
        [RequestManager perConcernOther:[self getUserId] andOtherID:otherID andIsConcern:@"1" andBlock:^( NSString *message) {
            
            if ([message isEqualToString:@"成功"]) {
                [btn setImage:[UIImage imageNamed:@"已关注1"] forState:UIControlStateNormal];
                btn.selected = YES;
            }else{
                 [MBProgressHUD showOlnyMessage:message toView:self.view];
            }
            
        }];
        
    }

}

- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}

@end
