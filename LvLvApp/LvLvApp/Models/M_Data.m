//
//  M_Data.m
//
//  Created by jim  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_Data.h"
#import "M_ListAll.h"


NSString *const kM_DataListAll = @"listAll";
NSString *const kM_DataPagesize = @"pagesize";
NSString *const kM_DataPageindex = @"pageindex";


@interface M_Data ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_Data

@synthesize listAll = _listAll;
@synthesize pagesize = _pagesize;
@synthesize pageindex = _pageindex;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedM_ListAll = [dict objectForKey:kM_DataListAll];
    NSMutableArray *parsedM_ListAll = [NSMutableArray array];
    if ([receivedM_ListAll isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedM_ListAll) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedM_ListAll addObject:[M_ListAll modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedM_ListAll isKindOfClass:[NSDictionary class]]) {
       [parsedM_ListAll addObject:[M_ListAll modelObjectWithDictionary:(NSDictionary *)receivedM_ListAll]];
    }

    self.listAll = [NSArray arrayWithArray:parsedM_ListAll];
            self.pagesize = [[self objectOrNilForKey:kM_DataPagesize fromDictionary:dict] doubleValue];
            self.pageindex = [[self objectOrNilForKey:kM_DataPageindex fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForListAll = [NSMutableArray array];
    for (NSObject *subArrayObject in self.listAll) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForListAll addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForListAll addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForListAll] forKey:kM_DataListAll];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pagesize] forKey:kM_DataPagesize];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pageindex] forKey:kM_DataPageindex];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.listAll = [aDecoder decodeObjectForKey:kM_DataListAll];
    self.pagesize = [aDecoder decodeDoubleForKey:kM_DataPagesize];
    self.pageindex = [aDecoder decodeDoubleForKey:kM_DataPageindex];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_listAll forKey:kM_DataListAll];
    [aCoder encodeDouble:_pagesize forKey:kM_DataPagesize];
    [aCoder encodeDouble:_pageindex forKey:kM_DataPageindex];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_Data *copy = [[M_Data alloc] init];
    
    if (copy) {

        copy.listAll = [self.listAll copyWithZone:zone];
        copy.pagesize = self.pagesize;
        copy.pageindex = self.pageindex;
    }
    
    return copy;
}


@end
