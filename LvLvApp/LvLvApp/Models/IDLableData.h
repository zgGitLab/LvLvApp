//
//  IDLableData.h
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface IDLableData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *tagname;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, strong) NSString *createdt;
@property (nonatomic, assign) double tagid;
@property (nonatomic, assign) double userid;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
