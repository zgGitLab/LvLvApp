//
//  CustomTabBarController.m
//  LawApp
//
//  Created by yfzx_sh_gaoyy on 15/10/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "CustomTabBarController.h"
#import "CustomNavigationController.h"
#import "ToolViewController.h"
#import "IndividualCenter.h"
#import "TopicHomeViewController.h"
#import "CasusViewController.h"


@interface CustomTabBarController ()

@end

@implementation CustomTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setViewControllers:[self getViewControllers] animated:false];
}
//CJNavigationController

-(NSArray *)getViewControllers
{
    CasusViewController *vc3 = [[CasusViewController alloc]init];
    ToolViewController *vc2 = [[ToolViewController alloc]init];
    TopicHomeViewController *vc1 = [[TopicHomeViewController alloc]init];
    IndividualCenter *vc4 = [[IndividualCenter alloc]init];
    
    vc1.tabBarItem.image = [UIImage imageNamed:@"话题1"];
    UIImage *seclectImage3 = [UIImage imageNamed:@"话题2"];
    seclectImage3 = [seclectImage3 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc1.tabBarItem.selectedImage =seclectImage3;
    vc1.tabBarItem.title = @"话题";
    _navc1 = [self getNavigtionWithRootVC:vc3 navcTitle:nil];
    
    vc2.tabBarItem.image = [UIImage imageNamed:@"工具1"];
    UIImage *seclectImage1 = [UIImage imageNamed:@"工具2"];
    seclectImage1 = [seclectImage1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc2.tabBarItem.selectedImage=seclectImage1;
    
    vc2.tabBarItem.title = @"工具";
    
    _navc2 = [self getNavigtionWithRootVC:vc1 navcTitle:nil];
    vc3.tabBarItem.image = [UIImage imageNamed:@"检索1"];
    UIImage *seclectImage = [UIImage imageNamed:@"检索2"];
    seclectImage = [seclectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc3.tabBarItem.selectedImage =seclectImage;
    
    vc3.tabBarItem.title = @"检索";
    CJNavigationController *navc3 = [self getNavigtionWithRootVC:vc2 navcTitle:nil];
    
    vc4.tabBarItem.image = [UIImage imageNamed:@"个人1"];
    UIImage *seclectImage4 = [UIImage imageNamed:@"个人2"];
    seclectImage4 = [seclectImage4 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc4.tabBarItem.selectedImage =seclectImage4;
    vc4.tabBarItem.title = @"个人";
    _navc4 = [self getNavigtionWithRootVC:vc4 navcTitle:nil];
    
    return [NSMutableArray  arrayWithObjects:self.navc1,_navc2,navc3,_navc4,nil];
}

-(CJNavigationController *)getNavigtionWithRootVC:(UIViewController *)rootvc navcTitle:(NSString *)navcTitle
{
   
    CJNavigationController *navc = [[CJNavigationController  alloc]initWithRootViewController:rootvc];
    navc.title = navcTitle;
    return navc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
