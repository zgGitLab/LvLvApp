//
//  HLableListResult.m
//
//  Created by hope  on 15/12/5
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "HLableListResult.h"


NSString *const kHLableListResultTID = @"TID";
NSString *const kHLableListResultUserid = @"Userid";
NSString *const kHLableListResultDescription = @"Description";
NSString *const kHLableListResultTimagesUrl = @"TimagesUrl";
NSString *const kHLableListResultLogoUrl = @"LogoUrl";
NSString *const kHLableListResultQuestionContent = @"QuestionContent";
NSString *const kHLableListResultCollectCount = @"collectCount";
NSString *const kHLableListResultSortTime = @"sortTime";
NSString *const kHLableListResultNickname = @"Nickname";
NSString *const kHLableListResultCounts = @"counts";
NSString *const kHLableListResultTitle = @"Title";
NSString *const kHLableListResultAnswerContent = @"AnswerContent";
NSString *const kHLableListResultCommentCount = @"commentCount";
NSString *const kHLableListResultTimes = @"times";
NSString *const kHLableListResultTypes = @"types";
NSString *const kHLableListResultAnswerID = @"Answer_ID";


@interface HLableListResult ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HLableListResult

@synthesize tID = _tID;
@synthesize userid = _userid;
@synthesize listResultDescription = _listResultDescription;
@synthesize timagesUrl = _timagesUrl;
@synthesize logoUrl = _logoUrl;
@synthesize questionContent = _questionContent;
@synthesize collectCount = _collectCount;
@synthesize sortTime = _sortTime;
@synthesize nickname = _nickname;
@synthesize counts = _counts;
@synthesize title = _title;
@synthesize answerContent = _answerContent;
@synthesize commentCount = _commentCount;
@synthesize times = _times;
@synthesize types = _types;
@synthesize answerID = _answerID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tID = [self objectOrNilForKey:kHLableListResultTID fromDictionary:dict];
            self.userid = [self objectOrNilForKey:kHLableListResultUserid fromDictionary:dict];
            self.listResultDescription = [self objectOrNilForKey:kHLableListResultDescription fromDictionary:dict];
            self.timagesUrl = [self objectOrNilForKey:kHLableListResultTimagesUrl fromDictionary:dict];
            self.logoUrl = [self objectOrNilForKey:kHLableListResultLogoUrl fromDictionary:dict];
            self.questionContent = [self objectOrNilForKey:kHLableListResultQuestionContent fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kHLableListResultCollectCount fromDictionary:dict] doubleValue];
            self.sortTime = [self objectOrNilForKey:kHLableListResultSortTime fromDictionary:dict];
            self.nickname = [self objectOrNilForKey:kHLableListResultNickname fromDictionary:dict];
            self.counts = [[self objectOrNilForKey:kHLableListResultCounts fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kHLableListResultTitle fromDictionary:dict];
            self.answerContent = [self objectOrNilForKey:kHLableListResultAnswerContent fromDictionary:dict];
            self.commentCount = [[self objectOrNilForKey:kHLableListResultCommentCount fromDictionary:dict] doubleValue];
            self.times = [self objectOrNilForKey:kHLableListResultTimes fromDictionary:dict];
            self.types = [[self objectOrNilForKey:kHLableListResultTypes fromDictionary:dict] doubleValue];
            self.answerID = [self objectOrNilForKey:kHLableListResultAnswerID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tID forKey:kHLableListResultTID];
    [mutableDict setValue:self.userid forKey:kHLableListResultUserid];
    [mutableDict setValue:self.listResultDescription forKey:kHLableListResultDescription];
    [mutableDict setValue:self.timagesUrl forKey:kHLableListResultTimagesUrl];
    [mutableDict setValue:self.logoUrl forKey:kHLableListResultLogoUrl];
    [mutableDict setValue:self.questionContent forKey:kHLableListResultQuestionContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kHLableListResultCollectCount];
    [mutableDict setValue:self.sortTime forKey:kHLableListResultSortTime];
    [mutableDict setValue:self.nickname forKey:kHLableListResultNickname];
    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kHLableListResultCounts];
    [mutableDict setValue:self.title forKey:kHLableListResultTitle];
    [mutableDict setValue:self.answerContent forKey:kHLableListResultAnswerContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kHLableListResultCommentCount];
    [mutableDict setValue:self.times forKey:kHLableListResultTimes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.types] forKey:kHLableListResultTypes];
    [mutableDict setValue:self.answerID forKey:kHLableListResultAnswerID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tID = [aDecoder decodeObjectForKey:kHLableListResultTID];
    self.userid = [aDecoder decodeObjectForKey:kHLableListResultUserid];
    self.listResultDescription = [aDecoder decodeObjectForKey:kHLableListResultDescription];
    self.timagesUrl = [aDecoder decodeObjectForKey:kHLableListResultTimagesUrl];
    self.logoUrl = [aDecoder decodeObjectForKey:kHLableListResultLogoUrl];
    self.questionContent = [aDecoder decodeObjectForKey:kHLableListResultQuestionContent];
    self.collectCount = [aDecoder decodeDoubleForKey:kHLableListResultCollectCount];
    self.sortTime = [aDecoder decodeObjectForKey:kHLableListResultSortTime];
    self.nickname = [aDecoder decodeObjectForKey:kHLableListResultNickname];
    self.counts = [aDecoder decodeDoubleForKey:kHLableListResultCounts];
    self.title = [aDecoder decodeObjectForKey:kHLableListResultTitle];
    self.answerContent = [aDecoder decodeObjectForKey:kHLableListResultAnswerContent];
    self.commentCount = [aDecoder decodeDoubleForKey:kHLableListResultCommentCount];
    self.times = [aDecoder decodeObjectForKey:kHLableListResultTimes];
    self.types = [aDecoder decodeDoubleForKey:kHLableListResultTypes];
    self.answerID = [aDecoder decodeObjectForKey:kHLableListResultAnswerID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tID forKey:kHLableListResultTID];
    [aCoder encodeObject:_userid forKey:kHLableListResultUserid];
    [aCoder encodeObject:_listResultDescription forKey:kHLableListResultDescription];
    [aCoder encodeObject:_timagesUrl forKey:kHLableListResultTimagesUrl];
    [aCoder encodeObject:_logoUrl forKey:kHLableListResultLogoUrl];
    [aCoder encodeObject:_questionContent forKey:kHLableListResultQuestionContent];
    [aCoder encodeDouble:_collectCount forKey:kHLableListResultCollectCount];
    [aCoder encodeObject:_sortTime forKey:kHLableListResultSortTime];
    [aCoder encodeObject:_nickname forKey:kHLableListResultNickname];
    [aCoder encodeDouble:_counts forKey:kHLableListResultCounts];
    [aCoder encodeObject:_title forKey:kHLableListResultTitle];
    [aCoder encodeObject:_answerContent forKey:kHLableListResultAnswerContent];
    [aCoder encodeDouble:_commentCount forKey:kHLableListResultCommentCount];
    [aCoder encodeObject:_times forKey:kHLableListResultTimes];
    [aCoder encodeDouble:_types forKey:kHLableListResultTypes];
    [aCoder encodeObject:_answerID forKey:kHLableListResultAnswerID];
}

- (id)copyWithZone:(NSZone *)zone
{
    HLableListResult *copy = [[HLableListResult alloc] init];
    
    if (copy) {

        copy.tID = [self.tID copyWithZone:zone];
        copy.userid = [self.userid copyWithZone:zone];
        copy.listResultDescription = [self.listResultDescription copyWithZone:zone];
        copy.timagesUrl = [self.timagesUrl copyWithZone:zone];
        copy.logoUrl = [self.logoUrl copyWithZone:zone];
        copy.questionContent = [self.questionContent copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.sortTime = [self.sortTime copyWithZone:zone];
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.counts = self.counts;
        copy.title = [self.title copyWithZone:zone];
        copy.answerContent = [self.answerContent copyWithZone:zone];
        copy.commentCount = self.commentCount;
        copy.times = [self.times copyWithZone:zone];
        copy.types = self.types;
        copy.answerID = [self.answerID copyWithZone:zone];
    }
    
    return copy;
}


@end
