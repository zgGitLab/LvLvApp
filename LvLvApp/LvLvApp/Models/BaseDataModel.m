//
//  BaseDataModel.m
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/25.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "BaseDataModel.h"

@implementation BaseDataModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    NSLog(@"%@:该字段不存在",key);
}

@end
