//
//  myCollectCaseBaseClass.m
//
//  Created by hope  on 15/12/17
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "myCollectCaseBaseClass.h"
#import "myCollectCaseData.h"


NSString *const kmyCollectCaseBaseClassCode = @"Code";
NSString *const kmyCollectCaseBaseClassMessage = @"Message";
NSString *const kmyCollectCaseBaseClassData = @"Data";


@interface myCollectCaseBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation myCollectCaseBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kmyCollectCaseBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kmyCollectCaseBaseClassMessage fromDictionary:dict];
    NSObject *receivedmyCollectCaseData = [dict objectForKey:kmyCollectCaseBaseClassData];
    NSMutableArray *parsedmyCollectCaseData = [NSMutableArray array];
    if ([receivedmyCollectCaseData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedmyCollectCaseData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedmyCollectCaseData addObject:[myCollectCaseData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedmyCollectCaseData isKindOfClass:[NSDictionary class]]) {
       [parsedmyCollectCaseData addObject:[myCollectCaseData modelObjectWithDictionary:(NSDictionary *)receivedmyCollectCaseData]];
    }

    self.data = [NSArray arrayWithArray:parsedmyCollectCaseData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kmyCollectCaseBaseClassCode];
    [mutableDict setValue:self.message forKey:kmyCollectCaseBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kmyCollectCaseBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kmyCollectCaseBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kmyCollectCaseBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kmyCollectCaseBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kmyCollectCaseBaseClassCode];
    [aCoder encodeObject:_message forKey:kmyCollectCaseBaseClassMessage];
    [aCoder encodeObject:_data forKey:kmyCollectCaseBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    myCollectCaseBaseClass *copy = [[myCollectCaseBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
