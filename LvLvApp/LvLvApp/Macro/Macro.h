//
//  Macro.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/26.
//  Copyright © 2016年 gyy. All rights reserved.
//

#ifndef Macro_h
#define Macro_h

#define Kuserss [[[NSUserDefaults standardUserDefaults] objectForKey:@"users"] integerValue]
#define kUserID Kuserss?[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]]:@"0"

//当前self.view的宽度
#define kWidth  [UIScreen mainScreen].bounds.size.width
//当前self.view的高度
#define kHeight [UIScreen mainScreen].bounds.size.height
//设置颜色
#define setColor(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
//是否是iOS 7 及 以上版本
#define iOS_Version_7  [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
//是不是iOS 9.0的版本
#define iOS_Version_9 [[[UIDevice currentDevice] systemVersion] floatValue]>=9.0
//是否是 iPhone5 屏幕尺寸
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
//判断手机设备
#define IsIPhone4  [[Utils getCurrentDeviceModel] isEqualToString:@"iPhone4"]
#define IsIPhone4s [[Utils getCurrentDeviceModel] isEqualToString:@"iPhone4S"]
#define IsITouch4  [[Utils getCurrentDeviceModel] isEqualToString:@"iPodTouch4G"]
#define IsIPhone5  [[Utils getCurrentDeviceModel] isEqualToString:@"iPhone5"]
#define IsIPhone5c  [[Utils getCurrentDeviceModel] isEqualToString:@"iPhone5c"]
#define IsIPhone5s  [[Utils getCurrentDeviceModel] isEqualToString:@"iPhone5s"]
#define IsITouch5  [[Utils getCurrentDeviceModel] isEqualToString:@"iPodTouch5G"]
#define IsIPhone6  [[Utils getCurrentDeviceModel] isEqualToString:@"iPhone6"]
#define IsIPhone6p  [[Utils getCurrentDeviceModel] isEqualToString:@"iPhone6Plus"]
#define IsIPhone6s  [[Utils getCurrentDeviceModel] isEqualToString:@"iPhone6s"]
#define IsIPhone6sp  [[Utils getCurrentDeviceModel] isEqualToString:@"iPhone6sPlus"]
#define IsIPhoneSimulator  [[Utils getCurrentDeviceModel] isEqualToString:@"iPhoneSimulator"]
//判断是否为空
#define ISEMPTY(x)	(((x) == nil ||[(x) isKindOfClass:[NSNull class]] ||([(x) isKindOfClass:[NSString class]] &&  [(NSString*)(x) length] == 0) || ([(x) isKindOfClass:[NSArray class]] && [(NSArray*)(x) count] == 0))|| ([(x) isKindOfClass:[NSDictionary class]] && [(NSDictionary*)(x) count] == 0))

#define APP [UIApplication sharedApplication].delegate

/**IP地址的设定*/

//#define JOBK_IP @"10.0.0.51"
//#define K_IP @"192.168.1.18"

//测试IP
//#define K_IP @"123.59.135.169/api"
//#define K_IMGIP @"123.59.135.169"
//#define k_SHAREIP @"123.59.135.169"

//#define K_IP @"123.59.135.172/api"
//#define K_IMGIP @"123.59.135.172"
//#define k_SHAREIP @"123.59.135.172"

//最新测试ip
//#define K_IP @"175.102.8.55:18888"
//#define K_IMGIP @"175.102.8.55:18888"
//#define k_SHAREIP @"175.102.8.55:18888"

//域名iP
#define K_IP @"www.lvlvlaw.com/api"
#define K_IMGIP @"www.lvlvlaw.com"
#define k_SHAREIP @"www.lvlvlaw.com"

#define SPACE 15
#define TitHeight 43
#define ConHeight 37
#define K_PAGESIZE 20
#define COLOR(r,g,b)[UIColor colorWithRed:r/255.00f green:g/255.00f blue:b/255.00f alpha:1]

#define SIZE [UIScreen mainScreen].bounds.size
//屏幕的大小
#define KSCREENBOUNDS [UIScreen mainScreen].bounds

#define K_ALERT(_message_) UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:_message_ delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];\
[alert show];

//注册登录时候的用户名密码和昵称
#define KAccount @"account"
#define KPwd @"pwdKey"
#define KNickname @"nickname"

//RGB的颜色转换
#define kUIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

/**主题颜色设定*/
//#499AF2
#define KMAINCOLOR [UIColor colorWithRed:73/255.f green:154/255.f blue:242/255.f alpha:1]
//空界面，提示文字颜色
#define KGRAYCOLOR [UIColor colorWithRed:171/255.0 green:171/255.0  blue:171/255.0  alpha:1]
#define KFONT16 [UIFont systemFontOfSize:16]
//placeholder字体颜色
#define KPHCOLOR [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1]

//cell标题颜色  81, 88, 99
#define KTITLECOLOR [UIColor colorWithRed:81.0/255.0 green:88.0/255.0 blue:99.0/255.0 alpha:1]

//7a7a7a  122,122,122 cell中标题下面的内容颜色
#define KCONTENTCOLOR [UIColor colorWithRed:122/255.0 green:122/255.0  blue:122/255.0  alpha:1]

#endif /* Macro_h */
