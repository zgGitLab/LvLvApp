//
//  S_SearchFriendBaseClass.m
//
//  Created by IOS8  on 15/12/22
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_SearchFriendBaseClass.h"
#import "S_SearchFriendData.h"


NSString *const kS_SearchFriendBaseClassCode = @"Code";
NSString *const kS_SearchFriendBaseClassMessage = @"Message";
NSString *const kS_SearchFriendBaseClassData = @"Data";


@interface S_SearchFriendBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_SearchFriendBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_SearchFriendBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_SearchFriendBaseClassMessage fromDictionary:dict];
    NSObject *receivedS_SearchFriendData = [dict objectForKey:kS_SearchFriendBaseClassData];
    NSMutableArray *parsedS_SearchFriendData = [NSMutableArray array];
    if ([receivedS_SearchFriendData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_SearchFriendData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_SearchFriendData addObject:[S_SearchFriendData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_SearchFriendData isKindOfClass:[NSDictionary class]]) {
       [parsedS_SearchFriendData addObject:[S_SearchFriendData modelObjectWithDictionary:(NSDictionary *)receivedS_SearchFriendData]];
    }

    self.data = [NSArray arrayWithArray:parsedS_SearchFriendData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_SearchFriendBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_SearchFriendBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kS_SearchFriendBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_SearchFriendBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_SearchFriendBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_SearchFriendBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_SearchFriendBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_SearchFriendBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_SearchFriendBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_SearchFriendBaseClass *copy = [[S_SearchFriendBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
