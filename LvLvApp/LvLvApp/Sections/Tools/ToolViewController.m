//
//  ToolViewController.m
//  LvLvApp
//
//  Created by hope on 15/10/15.
//  Copyright (c) 2015年 gyy. All rights reserved.
//
#define kViewKUAN (self.view.bounds.size.width/320.0)*70.0
#define kImageKuan (self.view.bounds.size.width/320.0)*50.0
#define kheighCHUS 12
#define kLABELHEIGH 30
#define kjianjuHeigh 8
#import "PersViewController.h"
#import "InterestViewController.h"
#import "DelayInterestBtnViewController.h"
#import "ToolViewController.h"
#import "LZWCustomActionSheet.h"
#import "SSFViewController.h"
#import "EmploymentViewController.h"

@interface ToolViewController ()
@property (strong, nonatomic)UIButton *interestBtn;
@property (strong, nonatomic)UIButton *delayInterestBtn;
@property (strong, nonatomic)UIButton *personalBtn;
@property (strong, nonatomic)UIButton *lawsuitBtn;
@property (strong, nonatomic)UIImage *imagetou;
@property (strong, nonatomic)UILabel *labeltitl;
@property (strong, nonatomic)UIView *lixijisuansView;
@property (strong, nonatomic)UILabel *lixijisuanLa;
@property (strong, nonatomic)UIImageView *images;
@property (strong, nonatomic)UIView *yanchilixijisuanView;
@property (strong, nonatomic)UIImageView *yanchilixiImage;
@property (strong, nonatomic)UIImageView *yanchilixiImge;
@property (strong, nonatomic)UILabel *yanchilixiLA;
@property (strong, nonatomic)UIButton *yanchibut;

@property (strong, nonatomic)UIView *geshujisuanView;
@property (strong, nonatomic)UIImageView *geshuiImage;
@property (strong, nonatomic)UILabel *geshuiLa;
@property (strong, nonatomic)UIButton *geshuBut;

@property (strong, nonatomic)UIView *ssFView;
@property (strong, nonatomic)UIImageView *ssFImage;
@property (strong, nonatomic)UILabel *ssFLa;
@property (strong, nonatomic)UIButton *ssFBut;


@property (strong, nonatomic)UIView *EmploymentView;
@property (strong, nonatomic)UIImageView *EmploymentImage;
@property (strong, nonatomic)UILabel *EmploymentLalbe;
@property (strong, nonatomic)UIButton *EmploymentBut;


@end

@implementation ToolViewController
{
    UIScrollView *mainView;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    mainView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.view = mainView;
    mainView.showsVerticalScrollIndicator = NO;
    mainView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    self.navigationItem.title=@"工具";
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor =[UIColor colorWithRed:244.0/255.0 green:245.0/255.0 blue:246.0/255.0 alpha:1];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    rightButton.frame = CGRectMake(0, 0, 70, 15);
//    [rightButton setTitle:@"更新数据" forState:UIControlStateNormal];
//    [rightButton setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
//    [rightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//    [rightButton addTarget:self action:@selector(updatan) forControlEvents:UIControlEventTouchUpInside];
//    rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];

}

//- (void)updatan
//{
//    [MBProgressHUD showMessage:@"正在更新" toView:self.view ];
//    [RequestManager requestBankDataWithUrl:[NSString stringWithFormat:@"http://%@/api/Tool/getpbcinterest",K_IP]andBlock:^(NSString *message) {
//        
//        [MBProgressHUD hideHUDForView:self.view];
//        [MBProgressHUD showSuccess:message toView:self.view];
//        
//    }];
//
////   AppDelegate *app = [UIApplication sharedApplication].delegate;
////    [app getAlldata];
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden =NO;
    _lixijisuansView =[[UIView alloc]initWithFrame:CGRectMake(0,kheighCHUS,self.view.bounds.size.width,kViewKUAN )];
    [_lixijisuansView setBackgroundColor:[UIColor colorWithRed:253.0/255.0 green:253.0/255.0 blue:253.0/255.0 alpha:1]];
    [mainView addSubview:_lixijisuansView];
    _images =[[UIImageView alloc]initWithFrame:CGRectMake(10, _lixijisuansView.frame.size.height/2-kImageKuan/2, kImageKuan,kImageKuan)];
    [_images setImage:[UIImage imageNamed:@"利息"]];
    [_lixijisuansView addSubview:_images];
    _lixijisuanLa =[[UILabel alloc] initWithFrame:CGRectMake(_images.bounds.size.width+20,_lixijisuansView.frame.size.height/2-kLABELHEIGH/2, 120, kLABELHEIGH)];
    [_lixijisuanLa setText:@"利息计算"];
    [_lixijisuanLa setTextColor:[UIColor blackColor]];
    _lixijisuanLa.font =[UIFont systemFontOfSize:16.0f];
    [_lixijisuansView addSubview:_lixijisuanLa];
    
    _interestBtn =[[UIButton alloc] initWithFrame:CGRectMake(0, 0,_lixijisuansView.bounds.size.width, kViewKUAN)];
    [_interestBtn setBackgroundColor:[UIColor clearColor]];
    [_interestBtn addTarget:self action:@selector(interestBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_lixijisuansView addSubview:_interestBtn];
    //延迟利息计算
    
    _yanchilixijisuanView =[[UIView alloc]initWithFrame:CGRectMake(0, kViewKUAN+_lixijisuansView.frame.origin.y+kjianjuHeigh ,self.view.bounds.size.width, kViewKUAN )];
    [_yanchilixijisuanView setBackgroundColor:[UIColor colorWithRed:253.0/255.0 green:253.0/255.0 blue:253.0/255.0 alpha:1]];
    [mainView addSubview:_yanchilixijisuanView];
    
    _yanchilixiImage =[[UIImageView alloc]initWithFrame:CGRectMake(10, _yanchilixijisuanView.frame.size.height/2-kImageKuan/2, kImageKuan,kImageKuan)];
    [_yanchilixiImage setImage:[UIImage imageNamed:@"延迟利息"]];
    [_yanchilixijisuanView addSubview:_yanchilixiImage];
    
    _yanchilixiLA =[[UILabel alloc] initWithFrame:CGRectMake(_images.bounds.size.width+20,_lixijisuansView.frame.size.height/2-kLABELHEIGH/2, 120, kLABELHEIGH)];
    
    [_yanchilixiLA setText:@"延迟利息计算"];
    [_yanchilixiLA setTextColor:[UIColor blackColor]];
    _yanchilixiLA.font =[UIFont systemFontOfSize:16.0f];
    [_yanchilixijisuanView  addSubview:_yanchilixiLA];
    
    _yanchibut  =[[UIButton alloc] initWithFrame:CGRectMake(0, 0,_lixijisuansView.bounds.size.width, kViewKUAN)];
    [_yanchibut  setBackgroundColor:[UIColor clearColor]];
    [_yanchibut  addTarget:self action:@selector(delayInterestBtn:)
          forControlEvents:UIControlEventTouchUpInside];
    [_yanchilixijisuanView addSubview:_yanchibut];
    
    //个税计算
    
    _geshujisuanView=[[UIView alloc]initWithFrame:CGRectMake(0, kViewKUAN+_yanchilixijisuanView.frame.origin.y+kjianjuHeigh ,self.view.bounds.size.width,kViewKUAN )];
    [_geshujisuanView setBackgroundColor:[UIColor colorWithRed:253.0/255.0 green:253.0/255.0 blue:253.0/255.0 alpha:1]];
    [mainView addSubview:_geshujisuanView];
    
    _geshuiImage =[[UIImageView alloc]initWithFrame:CGRectMake(10, _yanchilixijisuanView.frame.size.height/2-kImageKuan/2, kImageKuan,kImageKuan)];
    [_geshuiImage setImage:[UIImage imageNamed:@"个税"]];
    [_geshujisuanView  addSubview:_geshuiImage];
    
    _geshuiLa =[[UILabel alloc] initWithFrame:CGRectMake(_images.bounds.size.width+20,_lixijisuansView.frame.size.height/2-kLABELHEIGH/2, 120, kLABELHEIGH)];
    ;
    [_geshuiLa setText:@"个税计算"];
    [_geshuiLa setTextColor:[UIColor blackColor]];
    _yanchilixiLA.font =[UIFont systemFontOfSize:16.0f];
    [_geshujisuanView  addSubview:_geshuiLa];
    
    _geshuBut  =[[UIButton alloc] initWithFrame:CGRectMake(0, 0,_geshujisuanView.bounds.size.width, 80)];
    [_geshuBut  setBackgroundColor:[UIColor clearColor]];
    [_geshuBut  addTarget:self action:@selector(personalBtn:)
         forControlEvents:UIControlEventTouchUpInside];
    [_geshujisuanView   addSubview:_geshuBut];
    
    //诉讼费
    
    _ssFView=[[UIView alloc]initWithFrame:CGRectMake(0, kViewKUAN+_geshujisuanView.frame.origin.y+kjianjuHeigh ,self.view.bounds.size.width,kViewKUAN )];
    [_ssFView setBackgroundColor:[UIColor colorWithRed:253.0/255.0 green:253.0/255.0 blue:253.0/255.0 alpha:1]];
    [mainView addSubview:_ssFView];
    
    _ssFImage =[[UIImageView alloc]initWithFrame:CGRectMake(10, _yanchilixijisuanView.frame.size.height/2-kImageKuan/2, kImageKuan,kImageKuan)];
    [_ssFImage setImage:[UIImage imageNamed:@"诉讼费"]];
    [ _ssFView addSubview:_ssFImage];
    
    _ssFLa =[[UILabel alloc] initWithFrame:CGRectMake(_images.bounds.size.width+20,_lixijisuansView.frame.size.height/2-kLABELHEIGH/2, 120, kLABELHEIGH)];
    ;
    [_ssFLa setText:@"诉讼费用计算"];
    [_ssFLa setTextColor:[UIColor blackColor]];
    _yanchilixiLA.font =[UIFont systemFontOfSize:16.0f];
    [ _ssFView addSubview:_ssFLa];
    
    _ssFBut  =[[UIButton alloc] initWithFrame:CGRectMake(0, 0,_lixijisuansView.bounds.size.width, kViewKUAN)];
    [_ssFBut  setBackgroundColor:[UIColor clearColor]];
    [_ssFBut addTarget:self action:@selector(lawsuitBtn:)
      forControlEvents:UIControlEventTouchUpInside];
    [ _ssFView   addSubview:_ssFBut ];
    //招聘信息
    _EmploymentView =[[UIView alloc]initWithFrame:CGRectMake(0, kViewKUAN+_ssFView.frame.origin.y +kjianjuHeigh, [[UIScreen mainScreen]bounds].size.width, kViewKUAN)];
    [_EmploymentView setBackgroundColor:[UIColor colorWithRed:253.0/255.0 green:253.0/255.0 blue:253.0/255.0 alpha:1]];
    [mainView addSubview:_EmploymentView];
    _EmploymentImage =[[UIImageView alloc]initWithFrame:CGRectMake(10, _yanchilixijisuanView.frame.size.height/2-kImageKuan/2, kImageKuan,kImageKuan)];
    [_EmploymentImage setImage:[UIImage imageNamed:@"zp_107X107"]];
    [_EmploymentView addSubview:_EmploymentImage];
    _EmploymentLalbe =[[UILabel alloc] initWithFrame:CGRectMake(_images.bounds.size.width+20,_lixijisuansView.frame.size.height/2-kLABELHEIGH/2, 120, kLABELHEIGH)];
    _EmploymentLalbe.text =@"招聘信息";
    _EmploymentLalbe.textColor =[UIColor blackColor];
    _EmploymentLalbe.font =[UIFont systemFontOfSize:16.0f];
    [_EmploymentView addSubview:_EmploymentLalbe];
    
    _EmploymentBut =[[UIButton alloc]initWithFrame:CGRectMake(0, 0,_lixijisuansView.bounds.size.width, kViewKUAN)];
    _EmploymentBut.backgroundColor =[UIColor clearColor];
    [_EmploymentBut addTarget:self action:@selector(EmploymentBtn:)
             forControlEvents:UIControlEventTouchUpInside];
    [_EmploymentView addSubview:_EmploymentBut];
    
    
    self.navigationItem.leftBarButtonItem = nil;

}

- (void)interestBtn:(UIButton *)sender {
    
    InterestViewController *interVC = [[InterestViewController alloc] init];
    [self.navigationController pushViewController:interVC animated:YES];
    
    
}
- (void)delayInterestBtn:(UIButton *)sender {
    
    DelayInterestBtnViewController *delaVC = [[DelayInterestBtnViewController alloc] init];
    [self.navigationController pushViewController:delaVC animated:YES];
    
}

- (void)personalBtn:(UIButton *)sender {
    
    NSLog(@"111");
    PersViewController *peVC = [[PersViewController alloc] init];
    [self.navigationController pushViewController:peVC animated:YES];
    
}

- (void)lawsuitBtn:(UIButton *)sender{
   SSFViewController* but =[[SSFViewController alloc]init];
    [self.navigationController pushViewController:but animated:YES];

}
- (void)EmploymentBtn:(UIButton *)sender
{
    EmploymentViewController *ve =[[EmploymentViewController alloc]init];
    [self.navigationController pushViewController:ve animated:YES];
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
