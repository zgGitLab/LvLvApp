//
//  RegisterViewController.m
//  LawApp
//
//  Created by hope on 15/11/9.
//  Copyright (c) 2015年 gyy. All rights reserved.
//
#define ktextfielW  320*self.view.frame.size.width/320
#define ktextfileY  12
#define kbutW  self.view.frame.size.width-65
#define kX 35


#import "RegisterViewController.h"
#import "MBProgressHUD+MJ.h"
#import "MBProgressHUD.h"
#import "CustomTabBarController.h"
#import "NSString+RichText.h"
#import "TTTAttributedLabel.h"
#import "TTTAttributeLabelView.h"
#import "LoginViewController.h"
#import "HelpDocumentViewController.h"

@interface RegisterViewController ()<TTTAttributeLabelViewDelegate>
{
    UIBarButtonItem *butItem;
    UITextField *accountText;
    UITextField *codeText;
    UITextField *pwdsText;
    UIButton *_authCodeBtn;
    UITextField *nickname;
    UIButton *sureBtn;
    NSString *yanZhengMas;
    NSTimer *_timer;
    NSInteger _timeNum;
    
}
@property (nonatomic, strong) TTTAttributeLabelView  *attributeLabelView;

@end

@implementation RegisterViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem*butItems =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(fanhuiBut)];
    
    butItems.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItems;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title=@"注册";
    self.view.backgroundColor=[UIColor whiteColor];
    NSLog(@"%f",ktextfielW);
    butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    ;
    butItem.tintColor=[UIColor colorWithRed:55.0/255.0 green:130.0/255.0 blue:242.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
    
    [self initcomponents];
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    self.accountText1 = [defaults objectForKey:KAccount];
//    self.pwdsText1= [defaults objectForKey:KPwd];
//    self.nickname1 =[defaults objectForKey:KNickname];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -- 初始化所有组件

-(void)initcomponents
{
    accountText = [[UITextField alloc] initWithFrame:CGRectMake(70, ktextfileY, self.view.bounds.size.width-140,  40)];
    [accountText setPlaceholder:@"请输入邮箱/手机号"];
    accountText.font=[UIFont systemFontOfSize:15.0f];
    [accountText  setBorderStyle:UITextBorderStyleNone];
    [accountText setKeyboardType:UIKeyboardTypeNamePhonePad];
    [accountText setSecureTextEntry:NO];
    accountText.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:accountText];
    UIImageView *biaoImage =[[UIImageView alloc]initWithFrame:CGRectMake(kX,ktextfileY+9,20, 20)];
    [biaoImage setImage:[UIImage imageNamed:@"账号"]];
    [self.view addSubview:biaoImage];
    
    UIImageView *backImage =[[UIImageView alloc]initWithFrame:CGRectMake(kX,accountText.frame.origin.y+accountText.frame.size.height-2,kbutW, 1)];
    [backImage setImage:[UIImage imageNamed:@"线"]];
    [self.view addSubview:backImage];
    codeText = [[UITextField alloc] initWithFrame:CGRectMake(70,backImage.frame.origin.y+5, kWidth-180,  40)];
    [codeText setBorderStyle:UITextBorderStyleNone];
    [codeText setPlaceholder:@"请输入验证码"];
    codeText.font=[UIFont systemFontOfSize:15.0f];
    [codeText setKeyboardType:UIKeyboardTypeNumberPad];
    [codeText setSecureTextEntry:NO];
    [self.view addSubview:codeText];
    
    _authCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_authCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    _authCodeBtn.titleLabel.font=[UIFont systemFontOfSize:15.0f];
    [_authCodeBtn setTitleColor:[UIColor colorWithRed:73.0/255.0 green:154.0/255.0 blue:242.0/255.0 alpha:1 ]forState:UIControlStateNormal];
    [_authCodeBtn setFrame:CGRectMake(backImage.frame.size.width-40,backImage.frame.origin.y+5,80,40)];
    [_authCodeBtn addTarget:self action:@selector(_authCodeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_authCodeBtn];
    
    //密码头标
    UIImageView *pwdImage =[[UIImageView alloc]initWithFrame:CGRectMake(kX,backImage.frame.origin.y+15,20, 20)];
    [pwdImage setImage:[UIImage imageNamed:@"验证码"]];
    [self.view addSubview:pwdImage];
    
    UIImageView *upassbackImage =[[UIImageView alloc]initWithFrame:CGRectMake(kX,backImage.frame.origin.y+45,kbutW,1)];
    [upassbackImage setImage:[UIImage imageNamed:@"线"]];
    [self.view addSubview:upassbackImage];
    
    pwdsText= [[UITextField alloc] initWithFrame:CGRectMake(70, upassbackImage.frame.origin.y+5,  kWidth-140,  40)];
    [pwdsText setPlaceholder:@"密码6-20位数字或字母"];
    pwdsText.font=[UIFont systemFontOfSize:15.0f];
    [pwdsText  setBorderStyle:UITextBorderStyleNone];
    [pwdsText setKeyboardType:UIKeyboardTypeNamePhonePad];
    [pwdsText setSecureTextEntry:YES];
    pwdsText.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:pwdsText];
    UIImageView *pwdImages =[[UIImageView alloc]initWithFrame:CGRectMake(kX,upassbackImage.frame.origin.y+15,20, 20)];
    [pwdImages setImage:[UIImage imageNamed:@"密码"]];
    [self.view addSubview:pwdImages];
    UIImageView *xianpwd =[[UIImageView alloc]initWithFrame:CGRectMake(kX,upassbackImage.frame.origin.y+45, kbutW, 1)];
    [xianpwd setImage:[UIImage imageNamed:@"线"]];
    [self.view addSubview:xianpwd];
    
    nickname= [[UITextField alloc] initWithFrame:CGRectMake(70, xianpwd.frame.origin.y+5,  kWidth-140,  40)];
    [nickname setPlaceholder:@"昵称2-16个字符"];
    nickname.font=[UIFont systemFontOfSize:15.0f];
    [nickname  setBorderStyle:UITextBorderStyleNone];
    [nickname setKeyboardType:UIKeyboardTypeNamePhonePad];
    [nickname setSecureTextEntry:NO];
    nickname.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:nickname];
    UIImageView *nicknameImages =[[UIImageView alloc]initWithFrame:CGRectMake(kX,xianpwd.frame.origin.y+15,20, 20)];
    [nicknameImages setImage:[UIImage imageNamed:@"昵称"]];
    [self.view addSubview:nicknameImages];
    UIImageView *xianniclepwds =[[UIImageView alloc]initWithFrame:CGRectMake(kX,xianpwd.frame.origin.y+45, kbutW, 1)];
    [xianniclepwds setImage:[UIImage imageNamed:@"线"]];
    [self.view addSubview:xianniclepwds];
    
    
    sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureBtn setBackgroundImage:[UIImage imageNamed:@"确定1"] forState:UIControlStateNormal];
    [sureBtn setFrame:CGRectMake(kX,xianniclepwds.frame.origin.y+xianniclepwds.frame.size.height+17,kbutW,40)];
    [sureBtn addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sureBtn];
    
    NSString *strings = @"注册代表你同意律律协议";
    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    style.alignment                = NSTextAlignmentCenter;
    NSAttributedString *attributedString = [strings createAttributedStringAndConfig:@[[ConfigAttributedString foregroundColor:[UIColor lightGrayColor] range:strings.range],[ConfigAttributedString paragraphStyle:style range:strings.range],[ConfigAttributedString font:[UIFont systemFontOfSize:15] range:strings.range]]];
    // 初始化对象
    self.attributeLabelView                    = [[TTTAttributeLabelView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(sureBtn.frame)+40, kWidth, 0)];
    self.attributeLabelView.attributedString   = attributedString;
    self.attributeLabelView.delegate           = self;
    self.attributeLabelView.linkColor          = [UIColor colorWithRed:73.0/255.0 green:154.0/255.0 blue:242.0/255.0 alpha:1 ];
    // 添加超链接
    NSRange range1 = [strings rangeOfString:@"律律协议"];
    [self.attributeLabelView addLinkStringRange:range1 flag:@"link"];
    
    // 进行渲染
    [self.attributeLabelView render];
    [self.attributeLabelView resetSize];
    UIView *bgv = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(sureBtn.frame)+100, kWidth, 0)];
    self.attributeLabelView.center = bgv.center;
    [bgv addSubview:self.attributeLabelView];
    [self.view addSubview:self.attributeLabelView];
}

//点击律律协议跳转链接
- (void)TTTAttributeLabelView:(TTTAttributeLabelView *)attributeLabelView linkFlag:(NSString *)flag{
    NSLog(@"link");
    HelpDocumentViewController *help = [[HelpDocumentViewController alloc]init];
    [self.navigationController pushViewController:help animated:YES];
}


#pragma mark -- 注册请求

- (void)sureAction:(UIButton *)button
{
    
    if (accountText.text.length==0) {
        [MBProgressHUD showError:@"请输入邮箱或手机号"];
        return;
    }
    NSString* zucode= codeText.text;
    if (zucode.length ==0) {
        [MBProgressHUD showError:@"请输入验证密码"];
        return;
    }
    NSString *zupwd =pwdsText.text;
    if (![Utils validatePassword:zupwd]) {
        [MBProgressHUD showError:@"请输入6-20数字或字母密码"];
        return;
    }

    NSString *zunickname=nickname.text;
    if (zunickname.length==0) {
        [MBProgressHUD showError:@"请输入昵称"];
        return;
    }
    if(zunickname.length <2 ||zunickname.length >16){
        [MBProgressHUD showError:@"昵称请输入2-16个字符"];
        return;
    }
    NSString *yanzhengma =[NSString stringWithFormat:@"%@",yanZhengMas];
    if (![codeText.text isEqualToString:yanzhengma]) {
        // 密码错误
        [MBProgressHUD showError:@"验证码错误"];
        return;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:accountText.text forKey:@"EmailOrMobile"];
    [params setValue:zunickname forKey:@"Nickname"];
    [params setValue:zupwd forKey:@"UserPassword"];
    [params setValue:zucode forKey:@"TextCode"];
    [params setValue:yanzhengma forKey:@"SuccessCode"];
    NSLog(@"%@",[NSString stringWithFormat:KregiseterUrl,K_IP]);
    [LVHTTPRequestTool post:[NSString stringWithFormat:KregiseterUrl,K_IP] params:params success:^(id json) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        
        if ([dict[@"Code"] isEqualToNumber:@200]) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:accountText.text  forKey:KAccount];
            [defaults setObject:codeText.text forKey:KPwd];
            [defaults setObject:nickname.text forKey:KNickname];
            [defaults synchronize];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [MBProgressHUD showError:dict[@"Message"]];
        }
    } failureMessage:^(NSString *errorMessage) {
        [MBProgressHUD showError:errorMessage];
    }];
}


#pragma mark -- 邮箱验证

- (void)validateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if ([emailTest evaluateWithObject:email]) {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KmailUrl,K_IP]];
        [self requestDataWithUrl:url];
    }else{
        [MBProgressHUD showError:@"请输入正确邮箱号"];
    }
}


#pragma mark --手机号格式验证

- (void)validatePhone:(NSString *)phone
{
    NSString *phoneRegex = @"^1[345678]\\d{9}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    
    if ([phoneTest evaluateWithObject:phone]) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KmobilePhoneUrl,K_IP]];
        [self phonerequestDataWithUrl:url];
    }else{
        [MBProgressHUD showError:@"请输入正确的手机号"];
    }
}
//获取验证码
- (void)_authCodeBtnAction
{
    
    if (accountText.text.length==0) {
        [MBProgressHUD showError:@"请输入邮箱或手机号"];
        return;
    }
    
    if ([accountText.text rangeOfString:@"@"].location != NSNotFound) {
        [self validateEmail:accountText.text];
        
    }else{
        [self validatePhone:accountText.text];
    }
}

- (void)phonerequestDataWithUrl:(NSURL *)url
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:accountText.text forKey:@"Mobile"];
    [LVHTTPRequestTool post:[NSString stringWithFormat:@"%@",url] params:params success:^(id json) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableLeaves error:nil];
        if ([dict[@"Code"] isEqualToNumber:@200]) {
            NSDictionary *dic = [dict objectForKey:@"Data"];
            yanZhengMas = [dic objectForKey:@"VaData"];
            NSLog(@"%@",yanZhengMas);
            [MBProgressHUD showSuccess:dict[@"Message"]];
            [self setTimer];
        }else {
            [MBProgressHUD showError:dict[@"Message"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}


#pragma mark -- 验证码获取

- (void)requestDataWithUrl:(NSURL *)url
{
    // 创建一个请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 设置请求体
    NSString *param = [NSString stringWithFormat:@"email=%@",accountText.text];
    // NSString --> NSData
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError || data == nil) { // 一般请求超时就会来到这
             [MBProgressHUD showError:@"请求失败"];
             return;}
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"%@",dict);
         NSDictionary *dic = [dict objectForKey:@"Data"];
         yanZhengMas = [dic objectForKey:@"SuccessCode"];
         NSLog(@"%@",yanZhengMas);
         if ([dict[@"Code"] isEqualToNumber:@200]) {
             [MBProgressHUD showSuccess:@"获取验证成功!"];
             [self setTimer];
             return;
         }else {
             [MBProgressHUD hideHUD];
             [MBProgressHUD showError:@"获取验证码失败!"];
             return;
         }
     }];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


- (void)fanhuiBut{
    [self.navigationController popViewControllerAnimated:YES];
    
}

//创建定时器
- (void)setTimer{
    _authCodeBtn.enabled = NO;
    [_authCodeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    _timeNum = 60;
    _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(countNum) userInfo:nil repeats:YES];
    [[NSRunLoop  currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    
}

- (void)countNum{
    if (_timeNum >0) {
        [_authCodeBtn setTitle:[NSString stringWithFormat:@"(%lds)重发",(long)_timeNum] forState:UIControlStateNormal];
        _timeNum --;
    }else {
        [_timer invalidate];
        _timer = nil;
        [_authCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_authCodeBtn setTitleColor:[UIColor colorWithRed:73.0/255.0 green:154.0/255.0 blue:242.0/255.0 alpha:1 ]forState:UIControlStateNormal];
        _authCodeBtn.enabled = YES;
    }
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
