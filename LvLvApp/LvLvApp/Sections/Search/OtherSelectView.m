//
//  OtherSelectView.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/2.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "OtherSelectView.h"
@implementation OtherSelectView

//type 法条和案例1表示法条，2表示案例
//_type 0代表法条，1代表案例
-(id)initWithFrame:(CGRect)frame andType:(NSInteger)type andDataArray:(NSArray *)dataArray andFilterDict:(NSMutableDictionary *)dict
{

    self.dataDict = dict;
    
    self = [super initWithFrame:frame];
    if (self) {
        self.userStringArr = [NSMutableArray array];
        self.indexArr = [NSMutableArray array];
        self.type = (int)type-1;

        NSMutableArray *temp = [NSMutableArray array];
        
        NSMutableArray *arr1 = [NSMutableArray array];
        NSMutableArray *arr2 = [NSMutableArray array];
        NSMutableArray *arr3 = [NSMutableArray array];
        NSMutableArray *arr4 = [NSMutableArray array];
        NSMutableArray *arr5 = [NSMutableArray array];
        
        if (type == 2) {//案例
            _titleArray = [NSMutableArray arrayWithArray:@[@"法院",@"裁定年份",@"审级",@"案由",@"地域"]];
            for (S_Search *sear in dataArray) {
                if (sear.type == 1) {
                    [arr1 addObject:sear];
                }else if (sear.type == 2){
                    [arr2 addObject:sear];
                }else if (sear.type == 3){
                    [arr3 addObject:sear];
                }else if (sear.type == 4){
                    [arr4 addObject:sear];
                }else{
                    [arr5 addObject:sear];
                }
            }
            [temp addObject:arr1];
            [temp addObject:arr2];
            [temp addObject:arr3];
            [temp addObject:arr4];
            [temp addObject:arr5];
        }else{//法条
            
            _titleArray = [NSMutableArray array];
            for (FilterGroupsDetailModel *model in dataArray) {
                [_titleArray addObject:model.name];
                NSMutableArray *array = [NSMutableArray array];
                for (NSDictionary *dict in model.Groups) {
                    FilterGroupsDetailModel *detailModel = [[FilterGroupsDetailModel alloc]init];
                    [detailModel setValuesForKeysWithDictionary:dict];
                    [array addObject:detailModel];
                }
                [temp addObject:array];
            }
        }
        self.dataArr = temp;
        self.frame = frame;
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
        
        static UIView *bgVi = nil;
        if (dataArray.count != 0) {
            
            //创建UICollectionView
            self.flowLayout=[[UICollectionViewFlowLayout alloc ]init];
            self.flowLayout.minimumLineSpacing = 20;
            // 设置边界
            _flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
            _collectVI=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 0) collectionViewLayout:_flowLayout];
            [_collectVI registerNib:[UINib nibWithNibName:@"OtherCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"UICollectionViewCell"];
            [self addSubview:_collectVI];
            _collectVI.backgroundColor = [UIColor whiteColor];
            _collectVI.delegate = self;
            _collectVI.dataSource = self;
            //注册头视图
            [self.collectVI registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
            //注册尾视图
            [self.collectVI registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
            for (int i = (int)(self.dataArr.count-1); i >=  0; i--) {
                NSArray *sectionArr = [self.dataArr objectAtIndex:i];
                if (sectionArr.count == 0) {
                    [self.dataArr removeObjectAtIndex:i];
                    NSMutableArray *muArr = [NSMutableArray arrayWithArray: _titleArray[_type]];
                    [muArr removeObjectAtIndex:i];
                }
            }
        }else {
            bgVi = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 0)];
            bgVi.backgroundColor = [UIColor whiteColor];
            UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(bgVi.center.x-100, 80, 200, 50)];
            lab.text = @"没有更多条件";
            lab.textAlignment = NSTextAlignmentCenter;
            [bgVi addSubview:lab];
            [self addSubview:bgVi];
        }
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(0, 100 , SIZE.width, 50);
        [button setBackgroundImage:[UIImage imageNamed:@"确定1"] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setTitle:@"确定" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(confirmClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        [UIView animateWithDuration:0.4 animations:^{
            button.frame = CGRectMake(0, frame.size.height-50 , SIZE.width, 50);
            [_collectVI setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
            bgVi.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        } completion:^(BOOL finished) {
            
        }];
        
    }
    return self;
}


#pragma mark -- CollectionViewDelegate
//每个组头的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize size = CGSizeMake(SIZE.width, 30);
    return size;
}

//每个组尾的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    CGSize size = CGSizeMake(SIZE.width, 6);
    if (section == self.dataArr.count-1) {
        size = CGSizeMake(SIZE.width, 70);
    }
    
    return size;
}

//组头和组尾的内容
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *view1;
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"forIndexPath:indexPath];
        for (UIView *vi in view.subviews) {
            [vi removeFromSuperview];
        }
        UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 10,100, 20)];
        titleLab.text = @"";
        titleLab.text = _titleArray[indexPath.section];
        titleLab.textColor = [UIColor colorWithRed:55/255.0 green:64/255.0 blue:76/255.0 alpha:1];
        titleLab.font = [UIFont systemFontOfSize:14];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(SIZE.width-50, 8, 40, 25);
        [button setBackgroundColor:[UIColor whiteColor]];
        NSString *imagName =_orOpen[indexPath.section]?@"上箭头":@"下箭头";
        [button setImage:[UIImage imageNamed:imagName] forState:UIControlStateNormal];
        button.tag = indexPath.section;
        [button addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:titleLab];
        [view addSubview:button];
        view1 = view;
        
    }
    
    
    if (kind == UICollectionElementKindSectionFooter){
        UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"forIndexPath:indexPath];
        for (UIView *vi in view.subviews) {
            [vi removeFromSuperview];
        }
        UIView *colorVi = [[UIView alloc] initWithFrame:CGRectMake(10, 5, SIZE.width-20, 1)];
        colorVi.backgroundColor = [UIColor lightGrayColor];
        colorVi.alpha = .3;
        view.backgroundColor = [UIColor whiteColor];
        [view addSubview:colorVi];
        view1 = view;
    }
    return view1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.dataArr.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(_orOpen[section]){
        NSArray *arr = self.dataArr[section];
        return arr.count;
    }else {
        NSArray *arr = self.dataArr[section];
        return arr.count>2?2:arr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"UICollectionViewCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"OtherCollectionViewCell" owner:self options:nil][0];
    }
    NSArray *arr = self.dataArr[indexPath.section];
    
    
    if (_type == 1) {//案例
        S_Search *searchModal = arr[indexPath.row];
        UIButton *titleBtn = (UIButton *)[cell.contentView viewWithTag:1];
        UILabel *titlelab = (UILabel *)[cell.contentView viewWithTag:2];
        titlelab.textColor = [UIColor colorWithRed:80/255.0 green:80/255.0 blue:80/255.0 alpha:1];
        [titleBtn setBackgroundImage:[UIImage imageNamed:@"灰"] forState:UIControlStateNormal];

        //如果dataDict包含该cell，则选中该cell
        for (NSString *key in [self.dataDict allKeys]) {
            if ([key isEqualToString:_titleArray[indexPath.section]]) {
                if ([self.dataDict[key] containsObject:searchModal.name]) {
                    [titleBtn setBackgroundImage:[UIImage imageNamed:@"蓝"] forState:UIControlStateNormal];
                    titlelab.textColor = [UIColor whiteColor];
                }
            }
        }
        
        titlelab.text = [NSString stringWithFormat:@"%@ (%.0f)",searchModal.name,searchModal.count];
        
        [titleBtn addTarget:self action:@selector(otherBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        titleBtn.titleLabel.numberOfLines = 2;
    }else {
        FilterGroupsDetailModel *searchModal = arr[indexPath.row];
        NSLog(@"%@",arr[indexPath.row]);
        UIButton *titleBtn = (UIButton *)[cell.contentView viewWithTag:1];
        UILabel *titlelab = (UILabel *)[cell.contentView viewWithTag:2];
        titlelab.textColor = [UIColor colorWithRed:80/255.0 green:80/255.0 blue:80/255.0 alpha:1];
        [titleBtn setBackgroundImage:[UIImage imageNamed:@"灰"] forState:UIControlStateNormal];
        
        //如果dataDict包含该cell，则选中该cell
        for (NSString *key in [self.dataDict allKeys]) {
            if ([key isEqualToString:_titleArray[indexPath.section]]) {
                if ([self.dataDict[key] containsObject:searchModal.name]) {
                    [titleBtn setBackgroundImage:[UIImage imageNamed:@"蓝"] forState:UIControlStateNormal];
                    titlelab.textColor = [UIColor whiteColor];
                }
            }
        }
        
        titlelab.text = [NSString stringWithFormat:@"%@ (%@)",searchModal.name,searchModal.count];
        [titleBtn addTarget:self action:@selector(otherBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        titleBtn.titleLabel.numberOfLines = 2;
    }
    
    return cell;
}

//每个Item的大小，根据大小返回每行特定的个数
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;
    size = CGSizeMake((SIZE.width-31)/2.0, 30);
    return size;
}


-(void)btnPressed:(UIButton *)btn{
    _orOpen[btn.tag]=!_orOpen[btn.tag];
    [self.collectVI reloadSections:[NSIndexSet indexSetWithIndex:btn.tag]];
}

- (void)otherBtnClick:(UIButton *)btn
{
    UICollectionViewCell *cell = (UICollectionViewCell *)btn.superview.superview;
    NSIndexPath *index = [_collectVI indexPathForCell:cell];
    UILabel *titlelab = (UILabel *)[cell.contentView viewWithTag:2];

//    if (_type == 0) {//法条
        if (self.dataDict.count == 0) {
            self.dataDict = [NSMutableDictionary dictionary];
            for (int i = 0; i < _titleArray.count; i++) {
                NSMutableArray *array = [NSMutableArray array];
                [self.dataDict setObject:array forKey:_titleArray[i]];
            }
        }
        
        //处理数据字典
        NSArray *arr= [titlelab.text componentsSeparatedByString:@" "];

        for (NSString *key in [self.dataDict allKeys]) {
            if([key isEqualToString:_titleArray[index.section]]){//锁定所在分组
                if ([self.dataDict[key] containsObject:arr[0]]) {
                    [self.dataDict[key] removeObject:arr[0]];
                    
                    titlelab.textColor = [UIColor colorWithRed:80/255.0 green:80/255.0 blue:80/255.0 alpha:1];
                    [btn setBackgroundImage:[UIImage imageNamed:@"灰"] forState:UIControlStateNormal];
                }else {
                    [self.dataDict[key] addObject:arr[0]];
                    
                    titlelab.textColor = [UIColor whiteColor];
                    [btn setBackgroundImage:[UIImage imageNamed:@"蓝"] forState:UIControlStateNormal];
                }
            }
        }

}

//确定按钮事件
- (void)confirmClick:(UIButton *)btn
{
    NSString *myStr = @"";
    for (NSString *str in self.userStringArr) {
        myStr = [NSString stringWithFormat:@"%@ %@",myStr,str];
    }
    
    [self removeFromSuperview];
    [self.delegate done:myStr andFilterDict:self.dataDict];
}

-(void)showInView:(UIView *)view
{
    [view addSubview:self];
}


@end
