//
//  M_AttionsBaseClass.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_AttionsBaseClass.h"
#import "M_AttionsData.h"


NSString *const kM_AttionsBaseClassCode = @"Code";
NSString *const kM_AttionsBaseClassMessage = @"Message";
NSString *const kM_AttionsBaseClassData = @"Data";


@interface M_AttionsBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_AttionsBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_AttionsBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_AttionsBaseClassMessage fromDictionary:dict];
    NSObject *receivedM_AttionsData = [dict objectForKey:kM_AttionsBaseClassData];
    NSMutableArray *parsedM_AttionsData = [NSMutableArray array];
    if ([receivedM_AttionsData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedM_AttionsData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedM_AttionsData addObject:[M_AttionsData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedM_AttionsData isKindOfClass:[NSDictionary class]]) {
       [parsedM_AttionsData addObject:[M_AttionsData modelObjectWithDictionary:(NSDictionary *)receivedM_AttionsData]];
    }

    self.data = [NSArray arrayWithArray:parsedM_AttionsData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_AttionsBaseClassCode];
    [mutableDict setValue:self.message forKey:kM_AttionsBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kM_AttionsBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_AttionsBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_AttionsBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_AttionsBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_AttionsBaseClassCode];
    [aCoder encodeObject:_message forKey:kM_AttionsBaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_AttionsBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_AttionsBaseClass *copy = [[M_AttionsBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
