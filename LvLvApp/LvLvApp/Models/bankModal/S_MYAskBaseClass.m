//
//  S_MYAskBaseClass.m
//
//  Created by IOS8  on 15/12/16
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_MYAskBaseClass.h"
#import "S_MYAskData.h"


NSString *const kS_MYAskBaseClassCode = @"Code";
NSString *const kS_MYAskBaseClassMessage = @"Message";
NSString *const kS_MYAskBaseClassData = @"Data";


@interface S_MYAskBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_MYAskBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_MYAskBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_MYAskBaseClassMessage fromDictionary:dict];
    NSObject *receivedS_MYAskData = [dict objectForKey:kS_MYAskBaseClassData];
    NSMutableArray *parsedS_MYAskData = [NSMutableArray array];
    if ([receivedS_MYAskData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_MYAskData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_MYAskData addObject:[S_MYAskData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_MYAskData isKindOfClass:[NSDictionary class]]) {
       [parsedS_MYAskData addObject:[S_MYAskData modelObjectWithDictionary:(NSDictionary *)receivedS_MYAskData]];
    }

    self.data = [NSArray arrayWithArray:parsedS_MYAskData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_MYAskBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_MYAskBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kS_MYAskBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_MYAskBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_MYAskBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_MYAskBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_MYAskBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_MYAskBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_MYAskBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_MYAskBaseClass *copy = [[S_MYAskBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
