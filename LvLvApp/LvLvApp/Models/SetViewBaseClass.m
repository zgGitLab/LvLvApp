//
//  SetViewBaseClass.m
//
//  Created by hope  on 15/12/22
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "SetViewBaseClass.h"
#import "SetViewData.h"


NSString *const kSetViewBaseClassCode = @"Code";
NSString *const kSetViewBaseClassMessage = @"Message";
NSString *const kSetViewBaseClassData = @"Data";


@interface SetViewBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SetViewBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kSetViewBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kSetViewBaseClassMessage fromDictionary:dict];
            self.data = [SetViewData modelObjectWithDictionary:[dict objectForKey:kSetViewBaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kSetViewBaseClassCode];
    [mutableDict setValue:self.message forKey:kSetViewBaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kSetViewBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kSetViewBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kSetViewBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kSetViewBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kSetViewBaseClassCode];
    [aCoder encodeObject:_message forKey:kSetViewBaseClassMessage];
    [aCoder encodeObject:_data forKey:kSetViewBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    SetViewBaseClass *copy = [[SetViewBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
