//
//  CaseData.m
//
//  Created by IOS8  on 15/11/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "CaseData.h"
#import "CaseSelectVerdicts.h"


NSString *const kCaseDataSelectVerdicts = @"Select_verdicts";


@interface CaseData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CaseData

@synthesize selectVerdicts = _selectVerdicts;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedCaseSelectVerdicts = [dict objectForKey:kCaseDataSelectVerdicts];
    NSMutableArray *parsedCaseSelectVerdicts = [NSMutableArray array];
    if ([receivedCaseSelectVerdicts isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedCaseSelectVerdicts) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedCaseSelectVerdicts addObject:[CaseSelectVerdicts modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedCaseSelectVerdicts isKindOfClass:[NSDictionary class]]) {
       [parsedCaseSelectVerdicts addObject:[CaseSelectVerdicts modelObjectWithDictionary:(NSDictionary *)receivedCaseSelectVerdicts]];
    }

    self.selectVerdicts = [NSArray arrayWithArray:parsedCaseSelectVerdicts];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForSelectVerdicts = [NSMutableArray array];
    for (NSObject *subArrayObject in self.selectVerdicts) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSelectVerdicts addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSelectVerdicts addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSelectVerdicts] forKey:kCaseDataSelectVerdicts];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.selectVerdicts = [aDecoder decodeObjectForKey:kCaseDataSelectVerdicts];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_selectVerdicts forKey:kCaseDataSelectVerdicts];
}

- (id)copyWithZone:(NSZone *)zone
{
    CaseData *copy = [[CaseData alloc] init];
    
    if (copy) {

        copy.selectVerdicts = [self.selectVerdicts copyWithZone:zone];
    }
    
    return copy;
}


@end
