//
//  LVLVHTTP.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/26.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

// 网络提示框的出现时机，若干秒后网络数据还未返回则出现提示框
typedef NS_ENUM(NSUInteger, NetworkRequestGraceTimeType)
{
    NetworkRequestGraceTimeTypeNormal,  // 0.5s
    NetworkRequestGraceTimeTypeLong,    // 1s
    NetworkRequestGraceTimeTypeShort,   // 0.1s
    NetworkRequestGraceTimeTypeNone,     // 没有提示框
    NetworkRequestGraceTimeTypeAlways   // 总是有提示框
};


typedef void(^successBlock)(NSDictionary * respondsobject, AFHTTPRequestOperation * operation);
typedef void(^failureBlock)(NSError *error,AFHTTPRequestOperation * operation);


@interface HTTPRequestTool : NSObject

+ (id)sharedManager;

/**检测网络*/
+ (BOOL)checkNetWork;

/**
 *  GET请求一般常用方法
 *
 *  hudText                 : hud的提示内容
 *  graceTime               : 默认是NetworkRequestGraceTimeTypeNormal
 *  isHTTPRequestSerializer : 默认为YES
 *  isHTTPResponseSerializer: 默认是NO Get:hudText:parameters:success:failure:
 */
+ (void)Get:(NSString *)urlStr
    hudText:(NSString *)text
hudSuperView:(UIView *)hudSuperView
 parameters:(NSDictionary *)parameters
    success:(successBlock)success
    failure:(failureBlock)failure;

/**
 *  GET请求目标方法
 *
 *  hudText                         hud的提示内容
 *  @param urlStr                   网络请求的url
 *  @param parameters               请求体参数
 *  @param graceTime                网络提示框是否出现\什么时候出现-枚举
 *  @param isHTTPRequestSerializer  YES代表是HTTP请求序列化器，NO代表是JSON请求序列化器
 *  @param isHTTPResponseSerializer YES代表是HTTP响应序列化器，NO代表是JSON响应序列化器
 *  @param success                  请求成功回调
 *  @param failure                  请求失败回调
 */
+ (void)Get:(NSString *)urlStr
    hudText:(NSString *)text
hudSuperView:(UIView *)hudSuperView
 parameters:(NSDictionary *)parameters
  graceTime:(NetworkRequestGraceTimeType)graceTime
    success:(successBlock)success
    failure:(failureBlock)failure;

/**
 *  POST请求一般常用方法
 *
 *  hudText                 : hud的提示内容
 *  graceTime               : 默认是NetworkRequestGraceTimeTypeNormal
 *  isHTTPRequestSerializer : 默认为YES
 *  isHTTPResponseSerializer: 默认是NO
 */
+ (void)Post:(NSString *)urlStr
     hudText:(NSString *)text
hudSuperView:(UIView *)hudSuperView
  parameters:(NSDictionary *)parameters
     success:(successBlock)success
     failure:(failureBlock)failure;

/**
 *  POST请求目标方法
 *  urlStr                   : 网络请求的url
 *  hudText                  : hud的提示内容
 *  hudSuperView             : 承载hud
 *  parameters               : 请求体参数
 *  isHTTPRequestSerializer  : YES代表是HTTP请求序列化器，NO代表是JSON请求序列化器
 *  isHTTPResponseSerializer : YES代表是HTTP响应序列化器，NO代表是JSON响应序列化器
 *  graceTime                : 网络提示框是否出现\什么时候出现枚举
 *  success                  : 请求成功回调
 *  failure                  : 请求失败回调
 */
+ (void )Post:(NSString *)urlStr
      hudText:(NSString *)text
 hudSuperView:(UIView *)hudSuperView
   parameters:(NSDictionary *)parameters
    graceTime:(NetworkRequestGraceTimeType)graceTime
      success:(successBlock)success
      failure:(failureBlock)failure;


/**
 *  Post请求一般常用方法上传包含图片的请求。
 *
 *  @param urlStr           网络请求的url
 *  @param text             hud的提示内容
 *  @param hudSuperView     承载hud
 *  @param parameters       请求体参数
 *  @param success          请求成功回调
 *  @param failure          请求失败回调
 */
+(void)PostUpload:(NSString *)urlStr
          hudText:(NSString *)text
     hudSuperView:(UIView *)hudSuperView
       parameters:(NSDictionary *)parameters
          success:(successBlock)success
          failure:(failureBlock)failure;


/**
 *  Post请求目标方法，上传包含图片的请求。
 *
 *  @param urlStr                   网络请求的url
 *  @param text                     hud的提示内容
 *  @param hudSuperView             承载hud
 *  @param parameters               请求体参数
 *  @param graceTime                网络提示框是否出现\什么时候出现枚举
 *  @param isHTTPRequestSerializer  YES代表是HTTP请求序列化器，NO代表是JSON请求序列化器
 *  @param isHTTPResponseSerializer YES代表是HTTP响应序列化器，NO代表是JSON响应序列化器
 *  @param success                  请求成功回调
 *  @param failure                  请求失败回调
 */
+(void)PostUpload:(NSString *)urlStr
          hudText:(NSString *)text
     hudSuperView:(UIView *)hudSuperView
       parameters:(NSDictionary *)parameters
        graceTime:(NetworkRequestGraceTimeType)graceTime
          success:(successBlock)success
          failure:(failureBlock)failure;


@end
