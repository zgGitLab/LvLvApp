//
//  EditPerInfoTableViewCell.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/20.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditPerInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titliLab;
@property (weak, nonatomic) IBOutlet UIButton *userIcon;
@property (weak, nonatomic) IBOutlet UITextField *infoText;

@end
