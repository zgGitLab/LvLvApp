//
//  MarkTableViewCell.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/20.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarkTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *IDlabletitle;
@property (strong, nonatomic) IBOutlet UILabel *IDlableAtten;
@property (strong, nonatomic) IBOutlet UIButton *IDlableAttenBut;

@end
