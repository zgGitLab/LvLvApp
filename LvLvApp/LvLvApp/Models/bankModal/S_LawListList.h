//
//  S_LawListList.h
//
//  Created by IOS8  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_LawListList : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *pubDate;
@property (nonatomic, strong) NSString *abstract;
@property (nonatomic, assign) double listIdentifier;
@property (nonatomic, strong) NSString *court;
@property (nonatomic, strong) NSString *effectiveness;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *titleHighLighter;
@property (nonatomic, strong) NSString *content;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
