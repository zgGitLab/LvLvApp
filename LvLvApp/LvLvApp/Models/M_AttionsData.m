//
//  M_AttionsData.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_AttionsData.h"


NSString *const kM_AttionsDataUserID = @"UserID";
NSString *const kM_AttionsDataTitle = @"Title";
NSString *const kM_AttionsDataAnswerCount = @"AnswerCount";
NSString *const kM_AttionsDataTUserID = @"TUserID";
NSString *const kM_AttionsDataTagCount = @"TagCount";
NSString *const kM_AttionsDataTID = @"TID";


@interface M_AttionsData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_AttionsData

@synthesize userID = _userID;
@synthesize title = _title;
@synthesize answerCount = _answerCount;
@synthesize tUserID = _tUserID;
@synthesize tagCount = _tagCount;
@synthesize tID = _tID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userID = [[self objectOrNilForKey:kM_AttionsDataUserID fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kM_AttionsDataTitle fromDictionary:dict];
            self.answerCount = [[self objectOrNilForKey:kM_AttionsDataAnswerCount fromDictionary:dict] doubleValue];
            self.tUserID = [[self objectOrNilForKey:kM_AttionsDataTUserID fromDictionary:dict] doubleValue];
            self.tagCount = [[self objectOrNilForKey:kM_AttionsDataTagCount fromDictionary:dict] doubleValue];
            self.tID = [self objectOrNilForKey:kM_AttionsDataTID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kM_AttionsDataUserID];
    [mutableDict setValue:self.title forKey:kM_AttionsDataTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.answerCount] forKey:kM_AttionsDataAnswerCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tUserID] forKey:kM_AttionsDataTUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tagCount] forKey:kM_AttionsDataTagCount];
    [mutableDict setValue:self.tID forKey:kM_AttionsDataTID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userID = [aDecoder decodeDoubleForKey:kM_AttionsDataUserID];
    self.title = [aDecoder decodeObjectForKey:kM_AttionsDataTitle];
    self.answerCount = [aDecoder decodeDoubleForKey:kM_AttionsDataAnswerCount];
    self.tUserID = [aDecoder decodeDoubleForKey:kM_AttionsDataTUserID];
    self.tagCount = [aDecoder decodeDoubleForKey:kM_AttionsDataTagCount];
    self.tID = [aDecoder decodeObjectForKey:kM_AttionsDataTID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_userID forKey:kM_AttionsDataUserID];
    [aCoder encodeObject:_title forKey:kM_AttionsDataTitle];
    [aCoder encodeDouble:_answerCount forKey:kM_AttionsDataAnswerCount];
    [aCoder encodeDouble:_tUserID forKey:kM_AttionsDataTUserID];
    [aCoder encodeDouble:_tagCount forKey:kM_AttionsDataTagCount];
    [aCoder encodeObject:_tID forKey:kM_AttionsDataTID];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_AttionsData *copy = [[M_AttionsData alloc] init];
    
    if (copy) {

        copy.userID = self.userID;
        copy.title = [self.title copyWithZone:zone];
        copy.answerCount = self.answerCount;
        copy.tUserID = self.tUserID;
        copy.tagCount = self.tagCount;
        copy.tID = [self.tID copyWithZone:zone];
    }
    
    return copy;
}


@end
