//
//  FTiaoBaseClass.m
//
//  Created by hope  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "FTiaoBaseClass.h"
#import "FTiaoData.h"


NSString *const kFTiaoBaseClassCode = @"Code";
NSString *const kFTiaoBaseClassMessage = @"Message";
NSString *const kFTiaoBaseClassData = @"Data";


@interface FTiaoBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FTiaoBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kFTiaoBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kFTiaoBaseClassMessage fromDictionary:dict];
    NSObject *receivedFTiaoData = [dict objectForKey:kFTiaoBaseClassData];
    NSMutableArray *parsedFTiaoData = [NSMutableArray array];
    if ([receivedFTiaoData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedFTiaoData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedFTiaoData addObject:[FTiaoData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedFTiaoData isKindOfClass:[NSDictionary class]]) {
       [parsedFTiaoData addObject:[FTiaoData modelObjectWithDictionary:(NSDictionary *)receivedFTiaoData]];
    }

    self.data = [NSArray arrayWithArray:parsedFTiaoData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kFTiaoBaseClassCode];
    [mutableDict setValue:self.message forKey:kFTiaoBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kFTiaoBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kFTiaoBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kFTiaoBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kFTiaoBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kFTiaoBaseClassCode];
    [aCoder encodeObject:_message forKey:kFTiaoBaseClassMessage];
    [aCoder encodeObject:_data forKey:kFTiaoBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    FTiaoBaseClass *copy = [[FTiaoBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
