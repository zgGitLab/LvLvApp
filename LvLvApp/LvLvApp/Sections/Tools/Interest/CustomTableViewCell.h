//
//  CustomTableViewCell.h
//  Tools
//
//  Created by IOS8 on 15/9/11.
//  Copyright (c) 2015年 IOS8. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timesLab;
@property (weak, nonatomic) IBOutlet UILabel *daysLab;
@property (weak, nonatomic) IBOutlet UILabel *liLv;
@property (weak, nonatomic) IBOutlet UILabel *lixiLab;

@end
