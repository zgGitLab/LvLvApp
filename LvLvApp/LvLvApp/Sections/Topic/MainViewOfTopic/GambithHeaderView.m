//
//  GambithHeaderView.m
//  法律app
//
//  Created by hope on 15/10/8.
//  Copyright (c) 2015年 mark.ma. All rights reserved.
//

#import "GambithHeaderView.h"


@implementation GambithHeaderView

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    self.searchBar.userInteractionEnabled = NO;
}



- (IBAction)searchAction:(UIButton *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(doSearchAction)]) {
        [_delegate doSearchAction];
    }
}




- (IBAction)addBut:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(doAddAction:)]) {
        [_delegate doAddAction:sender];
    }
}

- (IBAction)dynamicBut:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(doHotspotAction:)]) {
        [_delegate doHotspotAction:sender];
    }
}

- (IBAction)hotBut:(id)sender {

    
    if (_delegate &&[_delegate respondsToSelector:@selector(doDynameicAction:)]) {
        [_delegate doDynameicAction:sender];
    }
}


@end
