//
//  ChangePwdViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/10.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "ChangePwdViewController.h"
#import "MBProgressHUD+MJ.h"
#import "MBProgressHUD.h"


@interface ChangePwdViewController ()

@end

@implementation ChangePwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改账号密码";
    self.RawPwd.secureTextEntry =YES;
    self.NewPwd.secureTextEntry =YES;
    self.SurePwd.secureTextEntry =YES;
}


- (IBAction)ReferPwd:(id)sender {
    if ([self.RawPwd.text isEqual:@"" ]||self.RawPwd.text==nil) {
        [MBProgressHUD showError:@"请输入原密码"];
        return;
    }
    if ( self.NewPwd.text.length<6) {
        [MBProgressHUD showError:@"请输入6-20数字或字母密码"];
        return;
    }
    if ([self.SurePwd.text isEqual:@""] || self.SurePwd.text ==nil) {
        [MBProgressHUD showError:@"请输入确定密码"];
        return;
    }
    NSString *pwdText =[NSString stringWithFormat:@"%@",self.SurePwd.text];//重新输入的密码
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString *count = [defaults objectForKey:KAccount];
    NSString *usernameText =[NSString stringWithFormat:@"%@",count];

    if (![self.RawPwd.text isEqualToString:[defaults objectForKey:KPwd]]) {//输入的原密码和本地保存的密码对比
        [MBProgressHUD showError:@"原密码错误"];
    }else{
        if (![self.NewPwd.text isEqualToString:pwdText]) {
            [MBProgressHUD showError:@"验证密码不一致"];
            return;
        }else{
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setValue:usernameText forKey:@"EmailOrMobile"];
            [params setValue:self.NewPwd.text forKey:@"UserPassword"];
            [LVHTTPRequestTool post:[NSString stringWithFormat:KChangePwd,K_IP] params:params success:^(id json) {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
                if ([dict[@"Code"] isEqualToNumber:@200]) {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:pwdText forKey:KPwd];
                    [MBProgressHUD showSuccess:@"修改密码成功!"];
                    [defaults synchronize];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }else {
                    [MBProgressHUD showError:dict[@"Message"]];
                }
            } failure:^(NSError *error) {
                
            }];
        }
    }
}

@end
