//
//  S_LawDetail.m
//
//  Created by IOS8  on 15/11/10
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_LawDetail.h"


NSString *const kS_LawDetailCategory = @"category";     // 类别
NSString *const kS_LawDetailPubDate = @"pub_date";      // 发布日期
NSString *const kS_LawDetailId = @"id";
NSString *const kS_LawDetailPublishNum = @"publish_num";    //发文字号
NSString *const kS_LawDetailEffectiveness = @"effectiveness";// 时效性
NSString *const kS_LawDetailTitle = @"title";       // 标题
NSString *const kS_LawDetailPublishDep = @"publish_dep";    // 发文机关
NSString *const kS_LawDetailEffectiveLevel = @"effective_level";
NSString *const kS_LawDetailEffectiveDate = @"effective_date";  // 生效日期
NSString *const kS_LawDetailBody = @"body";                 // 正文


@interface S_LawDetail ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_LawDetail

@synthesize category = _category;
@synthesize pubDate = _pubDate;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize publishNum = _publishNum;
@synthesize effectiveness = _effectiveness;
@synthesize title = _title;
@synthesize publishDep = _publishDep;
@synthesize effectiveLevel = _effectiveLevel;
@synthesize effectiveDate = _effectiveDate;
@synthesize body = _body;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.category = [self objectOrNilForKey:kS_LawDetailCategory fromDictionary:dict];
            self.pubDate = [self objectOrNilForKey:kS_LawDetailPubDate fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kS_LawDetailId fromDictionary:dict] doubleValue];
            self.publishNum = [self objectOrNilForKey:kS_LawDetailPublishNum fromDictionary:dict];
            self.effectiveness = [self objectOrNilForKey:kS_LawDetailEffectiveness fromDictionary:dict];
            self.title = [self objectOrNilForKey:kS_LawDetailTitle fromDictionary:dict];
            self.publishDep = [self objectOrNilForKey:kS_LawDetailPublishDep fromDictionary:dict];
            self.effectiveLevel = [self objectOrNilForKey:kS_LawDetailEffectiveLevel fromDictionary:dict];
            self.effectiveDate = [self objectOrNilForKey:kS_LawDetailEffectiveDate fromDictionary:dict];
            self.body = [self objectOrNilForKey:kS_LawDetailBody fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.category forKey:kS_LawDetailCategory];
    [mutableDict setValue:self.pubDate forKey:kS_LawDetailPubDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kS_LawDetailId];
    [mutableDict setValue:self.publishNum forKey:kS_LawDetailPublishNum];
    [mutableDict setValue:self.effectiveness forKey:kS_LawDetailEffectiveness];
    [mutableDict setValue:self.title forKey:kS_LawDetailTitle];
    [mutableDict setValue:self.publishDep forKey:kS_LawDetailPublishDep];
    [mutableDict setValue:self.effectiveLevel forKey:kS_LawDetailEffectiveLevel];
    [mutableDict setValue:self.effectiveDate forKey:kS_LawDetailEffectiveDate];
    [mutableDict setValue:self.body forKey:kS_LawDetailBody];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.category = [aDecoder decodeObjectForKey:kS_LawDetailCategory];
    self.pubDate = [aDecoder decodeObjectForKey:kS_LawDetailPubDate];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kS_LawDetailId];
    self.publishNum = [aDecoder decodeObjectForKey:kS_LawDetailPublishNum];
    self.effectiveness = [aDecoder decodeObjectForKey:kS_LawDetailEffectiveness];
    self.title = [aDecoder decodeObjectForKey:kS_LawDetailTitle];
    self.publishDep = [aDecoder decodeObjectForKey:kS_LawDetailPublishDep];
    self.effectiveLevel = [aDecoder decodeObjectForKey:kS_LawDetailEffectiveLevel];
    self.effectiveDate = [aDecoder decodeObjectForKey:kS_LawDetailEffectiveDate];
    self.body = [aDecoder decodeObjectForKey:kS_LawDetailBody];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_category forKey:kS_LawDetailCategory];
    [aCoder encodeObject:_pubDate forKey:kS_LawDetailPubDate];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kS_LawDetailId];
    [aCoder encodeObject:_publishNum forKey:kS_LawDetailPublishNum];
    [aCoder encodeObject:_effectiveness forKey:kS_LawDetailEffectiveness];
    [aCoder encodeObject:_title forKey:kS_LawDetailTitle];
    [aCoder encodeObject:_publishDep forKey:kS_LawDetailPublishDep];
    [aCoder encodeObject:_effectiveLevel forKey:kS_LawDetailEffectiveLevel];
    [aCoder encodeObject:_effectiveDate forKey:kS_LawDetailEffectiveDate];
    [aCoder encodeObject:_body forKey:kS_LawDetailBody];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_LawDetail *copy = [[S_LawDetail alloc] init];
    
    if (copy) {

        copy.category = [self.category copyWithZone:zone];
        copy.pubDate = [self.pubDate copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.publishNum = [self.publishNum copyWithZone:zone];
        copy.effectiveness = [self.effectiveness copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.publishDep = [self.publishDep copyWithZone:zone];
        copy.effectiveLevel = [self.effectiveLevel copyWithZone:zone];
        copy.effectiveDate = [self.effectiveDate copyWithZone:zone];
        copy.body = [self.body copyWithZone:zone];
    }
    
    return copy;
}


@end
