//
//  AskViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/11.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AskViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) NSString *myTitle;
@property (strong, nonatomic) NSString *userID;
@property (weak, nonatomic) IBOutlet UITableView *askTab;
@end
