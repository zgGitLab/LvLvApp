//
//  PersonalTax.h
//  Tools
//
//  Created by IOS8 on 15/10/15.
//  Copyright © 2015年 IOS8. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonalTax : NSObject   // 查询数据类

@property (nonatomic,strong)NSString *cityName;             // 城市名称
@property (nonatomic,assign)double PerRetire;               // 个人养老
@property (nonatomic,assign)double PerMediccal;             // 个人医疗
@property (nonatomic,assign)double PerJobless;              // 个人失业
@property (nonatomic,assign)double PerInjury;               // 个人工伤
@property (nonatomic,assign)double PerBear;                 // 个人生育
@property (nonatomic,assign)double PerHouse;                // 个人公积金
@property (nonatomic,assign)double ComRetire;               // 单位养老
@property (nonatomic,assign)double ComMedical;              // 单位医疗
@property (nonatomic,assign)double ComJobless;              // 单位失业
@property (nonatomic,assign)double ComInjury;               // 单位工伤
@property (nonatomic,assign)double ComBear;                 // 单位生育
@property (nonatomic,assign)double ComHouse;                // 单位公积金
@property (nonatomic,assign)double InsuranceMaxAmount;      // 社保基数上限
@property (nonatomic,assign)double InsuranceMinAmount;      // 社保基数下限
@property (nonatomic,assign)double HouseMaxAmount;          // 公积金上限
@property (nonatomic,assign)double HouseMinAmount;          // 公积金下限
@property (nonatomic,assign)double OtherAmount;             // 其他金额

@end
