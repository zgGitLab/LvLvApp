//
//  IDLablelaData.m
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IDLablelaData.h"


NSString *const kIDLablelaDataTagID = @"TagID";
NSString *const kIDLablelaDataCommentCount = @"CommentCount";
NSString *const kIDLablelaDataIsFocus = @"IsFocus";
NSString *const kIDLablelaDataQuestionCount = @"QuestionCount";
NSString *const kIDLablelaDataTagName = @"TagName";
NSString *const kIDLablelaDataFocusTag = @"FocusTag";


@interface IDLablelaData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IDLablelaData

@synthesize tagID = _tagID;
@synthesize commentCount = _commentCount;
@synthesize isFocus = _isFocus;
@synthesize questionCount = _questionCount;
@synthesize tagName = _tagName;
@synthesize focusTag = _focusTag;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tagID = [[self objectOrNilForKey:kIDLablelaDataTagID fromDictionary:dict] doubleValue];
            self.commentCount = [[self objectOrNilForKey:kIDLablelaDataCommentCount fromDictionary:dict] doubleValue];
            self.isFocus = [[self objectOrNilForKey:kIDLablelaDataIsFocus fromDictionary:dict] doubleValue];
            self.questionCount = [[self objectOrNilForKey:kIDLablelaDataQuestionCount fromDictionary:dict] doubleValue];
            self.tagName = [self objectOrNilForKey:kIDLablelaDataTagName fromDictionary:dict];
            self.focusTag = [[self objectOrNilForKey:kIDLablelaDataFocusTag fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tagID] forKey:kIDLablelaDataTagID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kIDLablelaDataCommentCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isFocus] forKey:kIDLablelaDataIsFocus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.questionCount] forKey:kIDLablelaDataQuestionCount];
    [mutableDict setValue:self.tagName forKey:kIDLablelaDataTagName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.focusTag] forKey:kIDLablelaDataFocusTag];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tagID = [aDecoder decodeDoubleForKey:kIDLablelaDataTagID];
    self.commentCount = [aDecoder decodeDoubleForKey:kIDLablelaDataCommentCount];
    self.isFocus = [aDecoder decodeDoubleForKey:kIDLablelaDataIsFocus];
    self.questionCount = [aDecoder decodeDoubleForKey:kIDLablelaDataQuestionCount];
    self.tagName = [aDecoder decodeObjectForKey:kIDLablelaDataTagName];
    self.focusTag = [aDecoder decodeDoubleForKey:kIDLablelaDataFocusTag];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_tagID forKey:kIDLablelaDataTagID];
    [aCoder encodeDouble:_commentCount forKey:kIDLablelaDataCommentCount];
    [aCoder encodeDouble:_isFocus forKey:kIDLablelaDataIsFocus];
    [aCoder encodeDouble:_questionCount forKey:kIDLablelaDataQuestionCount];
    [aCoder encodeObject:_tagName forKey:kIDLablelaDataTagName];
    [aCoder encodeDouble:_focusTag forKey:kIDLablelaDataFocusTag];
}

- (id)copyWithZone:(NSZone *)zone
{
    IDLablelaData *copy = [[IDLablelaData alloc] init];
    
    if (copy) {

        copy.tagID = self.tagID;
        copy.commentCount = self.commentCount;
        copy.isFocus = self.isFocus;
        copy.questionCount = self.questionCount;
        copy.tagName = [self.tagName copyWithZone:zone];
        copy.focusTag = self.focusTag;
    }
    
    return copy;
}


@end
