//
//  IdCollectReplyData.m
//
//  Created by hope  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IdCollectReplyData.h"


NSString *const kIdCollectReplyDataTID = @"TID";
NSString *const kIdCollectReplyDataUserid = @"Userid";
NSString *const kIdCollectReplyDataDescription = @"Description";
NSString *const kIdCollectReplyDataTimagesUrl = @"TimagesUrl";
NSString *const kIdCollectReplyDataLogoUrl = @"LogoUrl";
NSString *const kIdCollectReplyDataQuestionContent = @"QuestionContent";
NSString *const kIdCollectReplyDataCollectCount = @"collectCount";
NSString *const kIdCollectReplyDataSortTime = @"sortTime";
NSString *const kIdCollectReplyDataNickname = @"Nickname";
NSString *const kIdCollectReplyDataCounts = @"counts";
NSString *const kIdCollectReplyDataTitle = @"Title";
NSString *const kIdCollectReplyDataAnswerContent = @"AnswerContent";
NSString *const kIdCollectReplyDataCommentCount = @"commentCount";
NSString *const kIdCollectReplyDataTimes = @"times";
NSString *const kIdCollectReplyDataTypes = @"types";
NSString *const kIdCollectReplyDataAnswerID = @"Answer_ID";


@interface IdCollectReplyData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IdCollectReplyData

@synthesize tID = _tID;
@synthesize userid = _userid;
@synthesize dataDescription = _dataDescription;
@synthesize timagesUrl = _timagesUrl;
@synthesize logoUrl = _logoUrl;
@synthesize questionContent = _questionContent;
@synthesize collectCount = _collectCount;
@synthesize sortTime = _sortTime;
@synthesize nickname = _nickname;
@synthesize counts = _counts;
@synthesize title = _title;
@synthesize answerContent = _answerContent;
@synthesize commentCount = _commentCount;
@synthesize times = _times;
@synthesize types = _types;
@synthesize answerID = _answerID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tID = [self objectOrNilForKey:kIdCollectReplyDataTID fromDictionary:dict];
            self.userid = [self objectOrNilForKey:kIdCollectReplyDataUserid fromDictionary:dict];
            self.dataDescription = [self objectOrNilForKey:kIdCollectReplyDataDescription fromDictionary:dict];
            self.timagesUrl = [self objectOrNilForKey:kIdCollectReplyDataTimagesUrl fromDictionary:dict];
            self.logoUrl = [self objectOrNilForKey:kIdCollectReplyDataLogoUrl fromDictionary:dict];
            self.questionContent = [self objectOrNilForKey:kIdCollectReplyDataQuestionContent fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kIdCollectReplyDataCollectCount fromDictionary:dict] doubleValue];
            self.sortTime = [self objectOrNilForKey:kIdCollectReplyDataSortTime fromDictionary:dict];
            self.nickname = [self objectOrNilForKey:kIdCollectReplyDataNickname fromDictionary:dict];
            self.counts = [[self objectOrNilForKey:kIdCollectReplyDataCounts fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kIdCollectReplyDataTitle fromDictionary:dict];
            self.answerContent = [self objectOrNilForKey:kIdCollectReplyDataAnswerContent fromDictionary:dict];
            self.commentCount = [[self objectOrNilForKey:kIdCollectReplyDataCommentCount fromDictionary:dict] doubleValue];
            self.times = [self objectOrNilForKey:kIdCollectReplyDataTimes fromDictionary:dict];
            self.types = [[self objectOrNilForKey:kIdCollectReplyDataTypes fromDictionary:dict] doubleValue];
            self.answerID = [self objectOrNilForKey:kIdCollectReplyDataAnswerID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tID forKey:kIdCollectReplyDataTID];
    [mutableDict setValue:self.userid forKey:kIdCollectReplyDataUserid];
    [mutableDict setValue:self.dataDescription forKey:kIdCollectReplyDataDescription];
    [mutableDict setValue:self.timagesUrl forKey:kIdCollectReplyDataTimagesUrl];
    [mutableDict setValue:self.logoUrl forKey:kIdCollectReplyDataLogoUrl];
    [mutableDict setValue:self.questionContent forKey:kIdCollectReplyDataQuestionContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kIdCollectReplyDataCollectCount];
    [mutableDict setValue:self.sortTime forKey:kIdCollectReplyDataSortTime];
    [mutableDict setValue:self.nickname forKey:kIdCollectReplyDataNickname];
    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kIdCollectReplyDataCounts];
    [mutableDict setValue:self.title forKey:kIdCollectReplyDataTitle];
    [mutableDict setValue:self.answerContent forKey:kIdCollectReplyDataAnswerContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kIdCollectReplyDataCommentCount];
    [mutableDict setValue:self.times forKey:kIdCollectReplyDataTimes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.types] forKey:kIdCollectReplyDataTypes];
    [mutableDict setValue:self.answerID forKey:kIdCollectReplyDataAnswerID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tID = [aDecoder decodeObjectForKey:kIdCollectReplyDataTID];
    self.userid = [aDecoder decodeObjectForKey:kIdCollectReplyDataUserid];
    self.dataDescription = [aDecoder decodeObjectForKey:kIdCollectReplyDataDescription];
    self.timagesUrl = [aDecoder decodeObjectForKey:kIdCollectReplyDataTimagesUrl];
    self.logoUrl = [aDecoder decodeObjectForKey:kIdCollectReplyDataLogoUrl];
    self.questionContent = [aDecoder decodeObjectForKey:kIdCollectReplyDataQuestionContent];
    self.collectCount = [aDecoder decodeDoubleForKey:kIdCollectReplyDataCollectCount];
    self.sortTime = [aDecoder decodeObjectForKey:kIdCollectReplyDataSortTime];
    self.nickname = [aDecoder decodeObjectForKey:kIdCollectReplyDataNickname];
    self.counts = [aDecoder decodeDoubleForKey:kIdCollectReplyDataCounts];
    self.title = [aDecoder decodeObjectForKey:kIdCollectReplyDataTitle];
    self.answerContent = [aDecoder decodeObjectForKey:kIdCollectReplyDataAnswerContent];
    self.commentCount = [aDecoder decodeDoubleForKey:kIdCollectReplyDataCommentCount];
    self.times = [aDecoder decodeObjectForKey:kIdCollectReplyDataTimes];
    self.types = [aDecoder decodeDoubleForKey:kIdCollectReplyDataTypes];
    self.answerID = [aDecoder decodeObjectForKey:kIdCollectReplyDataAnswerID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tID forKey:kIdCollectReplyDataTID];
    [aCoder encodeObject:_userid forKey:kIdCollectReplyDataUserid];
    [aCoder encodeObject:_dataDescription forKey:kIdCollectReplyDataDescription];
    [aCoder encodeObject:_timagesUrl forKey:kIdCollectReplyDataTimagesUrl];
    [aCoder encodeObject:_logoUrl forKey:kIdCollectReplyDataLogoUrl];
    [aCoder encodeObject:_questionContent forKey:kIdCollectReplyDataQuestionContent];
    [aCoder encodeDouble:_collectCount forKey:kIdCollectReplyDataCollectCount];
    [aCoder encodeObject:_sortTime forKey:kIdCollectReplyDataSortTime];
    [aCoder encodeObject:_nickname forKey:kIdCollectReplyDataNickname];
    [aCoder encodeDouble:_counts forKey:kIdCollectReplyDataCounts];
    [aCoder encodeObject:_title forKey:kIdCollectReplyDataTitle];
    [aCoder encodeObject:_answerContent forKey:kIdCollectReplyDataAnswerContent];
    [aCoder encodeDouble:_commentCount forKey:kIdCollectReplyDataCommentCount];
    [aCoder encodeObject:_times forKey:kIdCollectReplyDataTimes];
    [aCoder encodeDouble:_types forKey:kIdCollectReplyDataTypes];
    [aCoder encodeObject:_answerID forKey:kIdCollectReplyDataAnswerID];
}

- (id)copyWithZone:(NSZone *)zone
{
    IdCollectReplyData *copy = [[IdCollectReplyData alloc] init];
    
    if (copy) {

        copy.tID = [self.tID copyWithZone:zone];
        copy.userid = [self.userid copyWithZone:zone];
        copy.dataDescription = [self.dataDescription copyWithZone:zone];
        copy.timagesUrl = [self.timagesUrl copyWithZone:zone];
        copy.logoUrl = [self.logoUrl copyWithZone:zone];
        copy.questionContent = [self.questionContent copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.sortTime = [self.sortTime copyWithZone:zone];
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.counts = self.counts;
        copy.title = [self.title copyWithZone:zone];
        copy.answerContent = [self.answerContent copyWithZone:zone];
        copy.commentCount = self.commentCount;
        copy.times = [self.times copyWithZone:zone];
        copy.types = self.types;
        copy.answerID = [self.answerID copyWithZone:zone];
    }
    
    return copy;
}


@end
