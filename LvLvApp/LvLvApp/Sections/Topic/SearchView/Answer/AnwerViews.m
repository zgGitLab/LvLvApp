//
//  AnwerViews.m
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "AnwerViews.h"
#import "AnwerTableViewCell.h"

#import "M_AnwersData.h"
#import "M_AnwersBaseClass.h"
#import "M_AnwersDataModels.h"
#import "UITableView+SDAutoTableViewCellHeight.h"
#import "UIView+SDAutoLayout.h"
@implementation AnwerViews

- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0.5, [UIScreen mainScreen].bounds.size.width,  [UIScreen mainScreen].bounds.size.height);
        self.backgroundColor = [UIColor whiteColor];
        
       
    }
     _pageIndex = 1;
    self.AnwerArray=[[NSMutableArray alloc]init];
    [self issueTableView];
    return self;
}
- (id)initWithString:(NSString*)str {
    
   
    self = [self init];
    
    self.keyStr = str;
    [self request:str WithPageindex:1 andPageSize:10];
    [self.AnwerArray removeAllObjects];
    [self.AnwerTableView reloadData];
    _AnwerTableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
     

        [self request:str WithPageindex:++_pageIndex andPageSize:10];
        [self layoutSubviews];}];

    return self;
}
- (void)issueTableView
{
    self.AnwerTableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-88)style:UITableViewStylePlain];
    self.AnwerTableView.backgroundColor =[UIColor whiteColor];
     [self.AnwerTableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    self.AnwerTableView.delegate = self;
    self.AnwerTableView.dataSource = self;
    [self addSubview:self.AnwerTableView];
    [self setExtraCellLineHidden:self.AnwerTableView];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.AnwerArray.count;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _commentSpace =7;
    _doubleLineHeight1 = 40;//
    _doubleLineHeight2 = 35;
    _lineSpace = 2;
    _topSpace= 18;
    _titleSpace =7;
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    if (self.AnwerArray.count >indexPath.row) {
        self.ANdata=self.AnwerArray[indexPath.row];
        
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        NSMutableArray *conentRangArr = [[NSMutableArray alloc] init];
        CGFloat titleHeight = [Utils textHeightFromTextString:self.ANdata.headerTitle width:kWidth-30 fontSize:16];
            if (titleHeight > 35) {
                titleHeight = _doubleLineHeight1 +_lineSpace;
            }
        [self getRangeStr:[Utils replaceImageString:self.ANdata.headerTitle] findText:self.keyStr andArray:arr];
        UILabel*titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(15, 15, kWidth-30, titleHeight)];
        titleLabel.textColor=setColor(81, 88, 99);
        titleLabel.font =[UIFont systemFontOfSize:16];
        titleLabel.numberOfLines=0;
      
        titleLabel.attributedText =[self getStrWith:arr andStr:[Utils replaceImageString:self.ANdata.headerTitle]];

        [cell.contentView addSubview:titleLabel];
        
       [self getRangeStr:[Utils replaceImageString:self.ANdata.answerContent] findText:self.keyStr andArray:conentRangArr];
        NSString *tit = [Utils replaceImageString:self.ANdata.answerContent];
        tit = [tit stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        CGFloat contentHeight = [Utils textHeightFromTextString:tit width:kWidth-30 fontSize:14];
        if (contentHeight > 30) {
                contentHeight = _doubleLineHeight2+_lineSpace;
            }
        UILabel *contentLabel =[[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(titleLabel.frame)+_commentSpace-3, kWidth-30, contentHeight)];
        contentLabel.textColor= setColor(122, 122, 122);
        contentLabel.font =[UIFont systemFontOfSize:14];
        contentLabel.numberOfLines=0;
        contentLabel.attributedText= [self getStrWith:conentRangArr andStr:[Utils replaceImageString:self.ANdata.answerContent]];

        [cell.contentView addSubview:contentLabel];
        
        UILabel *commentLabel = [Utils labelWithFrame:CGRectMake(15, CGRectGetMaxY(contentLabel.frame)+_commentSpace, kWidth-30, 15) andTextColor:setColor(171, 171, 171) andFont:[UIFont systemFontOfSize:11] andText:[NSString stringWithFormat:@"赞同 %ld · 评论 %ld · 收藏 %ld",(long)self.ANdata.responseCount,(long)self.ANdata.commentCount,(long)self.ANdata.commentCount] withNumOfLines:1 withTarget:(id)self andSEL:@selector(contentClick)];
        [cell.contentView addSubview:commentLabel];

    }
  return cell;
}
-(void)contentClick{

  

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    _totalHeight = _topSpace+_titleSpace+3;
    self.ANdata=self.AnwerArray[indexPath.row];
    CGFloat titleHeight = [Utils textHeightFromTextString:self.ANdata.headerTitle width:kWidth-30 fontSize:16];
    if (titleHeight > 35) {
        titleHeight = _doubleLineHeight1+_lineSpace;
    }
    _totalHeight += titleHeight+_commentSpace;
    NSString *tit = [Utils replaceImageString:self.ANdata.answerContent];
    tit = [tit stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    CGFloat contentHeight = [Utils textHeightFromTextString:tit width:kWidth-30 fontSize:14];
    if (contentHeight > 30) {
        contentHeight = _doubleLineHeight2+_lineSpace;
    }
 
    _totalHeight += contentHeight+_commentSpace;
    _totalHeight += 15;
    
        return _totalHeight;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   AnwerTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    if (_AnwerDelegate) {
        [_AnwerDelegate Anwerclick:indexPath.row];
    }

}

// 标题
- (void)getRangeStr:(NSString *)text findText:(NSString *)findText andArray:(NSMutableArray *)arr
{
    int lengtiStr = (int)[text length];
    if (lengtiStr>200) {
        lengtiStr = 200;
    }
    NSString *temp = nil;
    for(int i =0; i < lengtiStr; i++)
    {
        if (text.length - i < findText.length) {
            break;
        }
        temp = [text substringWithRange:NSMakeRange(i, findText.length)];
        if ([temp isEqualToString:findText]) {
            NSRange rang = NSMakeRange(i, findText.length);
            NSLog(@"标题第%d个字是:%@", i, temp);
            [arr addObject:[NSValue valueWithRange:rang]];
            
        }
    }
}

- (NSAttributedString *)getStrWith:(NSMutableArray *)arr andStr:(NSString *)str
{
     NSMutableAttributedString *attributedString = nil;
     attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    for (int i = 0; i<arr.count; i++) {
        NSRange rang = [arr[i] rangeValue];
        // 设置富文本样式
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor colorWithRed:70/255.0 green:142/255.0 blue:239/255.0 alpha:1]
                                 range:NSMakeRange(rang.location, rang.length)];
    }
    return attributedString;

}

-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [UIView new];
    
    view.backgroundColor = [UIColor clearColor];
    
    [tableView setTableFooterView:view];
}
- (void)getAnwerSearText:(NSString *)strs
{
   
   
    NSLog(@"%@",strs);
    self.searString =[NSString stringWithFormat:@"%@",strs];
    NSLog(@"%@",self.searString);
    [self.AnwerArray removeAllObjects];
    self.keyStr = strs;
    [self request:strs WithPageindex:_pageIndex andPageSize:10];
    _AnwerTableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
       
        [self request:strs WithPageindex:++_pageIndex andPageSize:10];
        [self layoutSubviews];}];
    
    
}
- (void)request:(NSString *)TopicNames WithPageindex:(NSInteger)pageIndex andPageSize:(NSInteger)pageSize
{
   
//    NSString *TopicName =TopicNames;
//    NSLog(@"%@",TopicName);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KseacrAnwerUrl,K_IP]];
    // 2.请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
     [param appendString:[NSString stringWithFormat:@"FuzzyStr=%@",TopicNames]];
    [param appendString:[NSString stringWithFormat:@"&PageIndex=%d",(int)pageIndex]];
    [param appendString:[NSString stringWithFormat:@"&Pagesize=%d",(int)pageSize]];
   
    
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
       [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         M_AnwersBaseClass *anwer =[[M_AnwersBaseClass alloc]initWithDictionary:dict];
         [self.AnwerArray addObjectsFromArray:anwer.data];
         //block 传数组值
        if (self.anwBlock) {
             self.anwBlock(self.AnwerArray);
         }
         [self.AnwerTableView reloadData];
         [ self.AnwerTableView .mj_footer endRefreshing];


     }];
    
}

@end
