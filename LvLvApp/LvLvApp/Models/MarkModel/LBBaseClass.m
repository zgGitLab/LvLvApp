//
//  LBBaseClass.m
//
//  Created by hope  on 15/12/1
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "LBBaseClass.h"
#import "LBData.h"


NSString *const kLBBaseClassCode = @"Code";
NSString *const kLBBaseClassMessage = @"Message";
NSString *const kLBBaseClassData = @"Data";


@interface LBBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LBBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kLBBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kLBBaseClassMessage fromDictionary:dict];
    NSObject *receivedLBData = [dict objectForKey:kLBBaseClassData];
    NSMutableArray *parsedLBData = [NSMutableArray array];
    if ([receivedLBData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedLBData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedLBData addObject:[LBData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedLBData isKindOfClass:[NSDictionary class]]) {
       [parsedLBData addObject:[LBData modelObjectWithDictionary:(NSDictionary *)receivedLBData]];
    }

    self.data = [NSArray arrayWithArray:parsedLBData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kLBBaseClassCode];
    [mutableDict setValue:self.message forKey:kLBBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kLBBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kLBBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kLBBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kLBBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kLBBaseClassCode];
    [aCoder encodeObject:_message forKey:kLBBaseClassMessage];
    [aCoder encodeObject:_data forKey:kLBBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    LBBaseClass *copy = [[LBBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
