//
//  S_LawDetailDs.m
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_LawDetailDs.h"
#import "S_LawDetailSelectArticles.h"


NSString *const kS_LawDetailDsSelectArticles = @"Select_articles";


@interface S_LawDetailDs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_LawDetailDs

@synthesize selectArticles = _selectArticles;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedS_LawDetailSelectArticles = [dict objectForKey:kS_LawDetailDsSelectArticles];
    NSMutableArray *parsedS_LawDetailSelectArticles = [NSMutableArray array];
    if ([receivedS_LawDetailSelectArticles isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_LawDetailSelectArticles) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_LawDetailSelectArticles addObject:[S_LawDetailSelectArticles modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_LawDetailSelectArticles isKindOfClass:[NSDictionary class]]) {
       [parsedS_LawDetailSelectArticles addObject:[S_LawDetailSelectArticles modelObjectWithDictionary:(NSDictionary *)receivedS_LawDetailSelectArticles]];
    }

    self.selectArticles = [NSArray arrayWithArray:parsedS_LawDetailSelectArticles];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForSelectArticles = [NSMutableArray array];
    for (NSObject *subArrayObject in self.selectArticles) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSelectArticles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSelectArticles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSelectArticles] forKey:kS_LawDetailDsSelectArticles];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.selectArticles = [aDecoder decodeObjectForKey:kS_LawDetailDsSelectArticles];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_selectArticles forKey:kS_LawDetailDsSelectArticles];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_LawDetailDs *copy = [[S_LawDetailDs alloc] init];
    
    if (copy) {

        copy.selectArticles = [self.selectArticles copyWithZone:zone];
    }
    
    return copy;
}


@end
