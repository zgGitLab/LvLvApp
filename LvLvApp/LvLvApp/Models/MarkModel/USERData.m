//
//  USERData.m
//
//  Created by hope  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "USERData.h"


NSString *const kUSERDataID = @"ID";
NSString *const kUSERDataIntroduction = @"Introduction";
NSString *const kUSERDataImage = @"Image";
NSString *const kUSERDataNickName = @"NickName";


@interface USERData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation USERData

@synthesize iDProperty = _iDProperty;
@synthesize introduction = _introduction;
@synthesize image = _image;
@synthesize nickName = _nickName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kUSERDataID fromDictionary:dict] doubleValue];
            self.introduction = [self objectOrNilForKey:kUSERDataIntroduction fromDictionary:dict];
            self.image = [self objectOrNilForKey:kUSERDataImage fromDictionary:dict];
            self.nickName = [self objectOrNilForKey:kUSERDataNickName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kUSERDataID];
    [mutableDict setValue:self.introduction forKey:kUSERDataIntroduction];
    [mutableDict setValue:self.image forKey:kUSERDataImage];
    [mutableDict setValue:self.nickName forKey:kUSERDataNickName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kUSERDataID];
    self.introduction = [aDecoder decodeObjectForKey:kUSERDataIntroduction];
    self.image = [aDecoder decodeObjectForKey:kUSERDataImage];
    self.nickName = [aDecoder decodeObjectForKey:kUSERDataNickName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kUSERDataID];
    [aCoder encodeObject:_introduction forKey:kUSERDataIntroduction];
    [aCoder encodeObject:_image forKey:kUSERDataImage];
    [aCoder encodeObject:_nickName forKey:kUSERDataNickName];
}

- (id)copyWithZone:(NSZone *)zone
{
    USERData *copy = [[USERData alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.introduction = [self.introduction copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
        copy.nickName = [self.nickName copyWithZone:zone];
    }
    
    return copy;
}


@end
