//
//  IDLableData.m
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IDLableData.h"


NSString *const kIDLableDataTagname = @"_tagname";
NSString *const kIDLableDataId = @"_id";
NSString *const kIDLableDataCreatedt = @"_createdt";
NSString *const kIDLableDataTagid = @"_tagid";
NSString *const kIDLableDataUserid = @"_userid";


@interface IDLableData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IDLableData

@synthesize tagname = _tagname;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize createdt = _createdt;
@synthesize tagid = _tagid;
@synthesize userid = _userid;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tagname = [self objectOrNilForKey:kIDLableDataTagname fromDictionary:dict];
            self.dataIdentifier = [[self objectOrNilForKey:kIDLableDataId fromDictionary:dict] doubleValue];
            self.createdt = [self objectOrNilForKey:kIDLableDataCreatedt fromDictionary:dict];
            self.tagid = [[self objectOrNilForKey:kIDLableDataTagid fromDictionary:dict] doubleValue];
            self.userid = [[self objectOrNilForKey:kIDLableDataUserid fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tagname forKey:kIDLableDataTagname];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kIDLableDataId];
    [mutableDict setValue:self.createdt forKey:kIDLableDataCreatedt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tagid] forKey:kIDLableDataTagid];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userid] forKey:kIDLableDataUserid];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tagname = [aDecoder decodeObjectForKey:kIDLableDataTagname];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kIDLableDataId];
    self.createdt = [aDecoder decodeObjectForKey:kIDLableDataCreatedt];
    self.tagid = [aDecoder decodeDoubleForKey:kIDLableDataTagid];
    self.userid = [aDecoder decodeDoubleForKey:kIDLableDataUserid];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tagname forKey:kIDLableDataTagname];
    [aCoder encodeDouble:_dataIdentifier forKey:kIDLableDataId];
    [aCoder encodeObject:_createdt forKey:kIDLableDataCreatedt];
    [aCoder encodeDouble:_tagid forKey:kIDLableDataTagid];
    [aCoder encodeDouble:_userid forKey:kIDLableDataUserid];
}

- (id)copyWithZone:(NSZone *)zone
{
    IDLableData *copy = [[IDLableData alloc] init];
    
    if (copy) {

        copy.tagname = [self.tagname copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.createdt = [self.createdt copyWithZone:zone];
        copy.tagid = self.tagid;
        copy.userid = self.userid;
    }
    
    return copy;
}


@end
