//
//  S_LawDetailSelectArticles.h
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_LawDetailSelectArticles : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *pubDate;
@property (nonatomic, assign) double selectArticlesIdentifier;
@property (nonatomic, strong) NSString *publishNum;
@property (nonatomic, strong) NSString *effectiveness;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *publishDep;
@property (nonatomic, strong) NSString *effectiveLevel;
@property (nonatomic, strong) NSString *effectiveDate;
@property (nonatomic, strong) NSString *body;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
