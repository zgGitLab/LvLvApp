//
//  AskTableViewCell.m
//  LvLvApp
//
//  Created by Sunny on 16/1/18.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "AskTableViewCell.h"
#import "UIView+SDAutoLayout.h"
#import "UITableView+SDAutoTableViewCellHeight.h"

@implementation AskTableViewCell
{
    UILabel *titLab;
    UILabel *otherLab;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUI];
    }
    return self;
}

- (void)setUI
{
    titLab= [[UILabel alloc] init];
    titLab.numberOfLines = 0;
    titLab.textColor = COLOR(81, 88, 99);
    titLab.font = [UIFont systemFontOfSize:16];
    otherLab = [[UILabel alloc] init];
    otherLab.numberOfLines = 1;
    otherLab.textColor = COLOR(156, 156, 156);
    otherLab.font = [UIFont systemFontOfSize:11];
    
    [self.contentView addSubview:titLab];
    [self.contentView addSubview:otherLab];

    titLab.sd_layout
    .topSpaceToView(self.contentView, 10)
    .rightSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView,10)
    .autoHeightRatio(0);
    
    otherLab.sd_layout
    .topSpaceToView(titLab, 10)
    .rightSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView,10)
    .autoHeightRatio(0);
    
}


- (void)setAskData:(S_MYAskData *)askData
{
    _askData = askData;
    titLab.text = askData.title;
    otherLab.text = [NSString stringWithFormat:@"%.0f 个关注 · %.0f 个回答",askData.tagCount,askData.answerCount];
     [self setupAutoHeightWithBottomView:otherLab bottomMargin:10];
}


- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
