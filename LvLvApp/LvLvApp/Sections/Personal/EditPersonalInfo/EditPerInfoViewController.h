//
//  EditPerInfoViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/20.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditPerInfoViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *myTable;

@end
