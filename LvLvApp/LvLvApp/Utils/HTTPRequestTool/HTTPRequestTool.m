//
//  LVLVHTTP.m
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/26.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "HTTPRequestTool.h"
#define RGBHUDColor  [UIColor colorWithRed:(40/255.0f) green:(40/255.0f) blue:(40/255.0f) alpha:0.2f]
#define kLastWindow [UIApplication sharedApplication].windows.lastObject

static HTTPRequestTool *manager = nil;

@implementation HTTPRequestTool

+ (id)sharedManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!manager) {
            manager = [[HTTPRequestTool alloc]init];
        }
    });
    
    return manager;
}

#pragma mark - API方法

+ (void)Get:(NSString *)urlStr
    hudText:(NSString *)text
hudSuperView:(UIView *)hudSuperView
 parameters:(NSDictionary *)parameters
    success:(successBlock)success
    failure:(failureBlock)failure{
    
    [self Get:urlStr hudText:text hudSuperView:hudSuperView parameters:parameters graceTime:NetworkRequestGraceTimeTypeNormal success:success failure:failure];
}


+ (void)Get:(NSString *)urlStr
    hudText:(NSString *)text
hudSuperView:(UIView *)hudSuperView
 parameters:(NSDictionary *)parameters
  graceTime:(NetworkRequestGraceTimeType)graceTime
    success:(successBlock)success
    failure:(failureBlock)failure
{
    
//    if (![self isConnectionAvailable])
//    {
//        [self showExceptionDialog];
//        return ;
//    }
    
    AFHTTPRequestOperationManager *manager = [self manager:YES isHTTPResponseSerializer:NO];
    
    MBProgressHUD *hud = [self hud:graceTime hudText:text hudSuperView:hudSuperView];
    
    [manager GET:urlStr parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
     {
//         DeBugLog(operation.request.URL);
         
         hud.taskInProgress = NO;
         [hud hide:YES];
         
         if (success)
         {
             success(responseObject,operation);
         }
         
     } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
         
         hud.taskInProgress = NO;
         [hud hide:YES];
         
         //      hud.labelText = error.localizedDescription;
         //      hud.taskInProgress=NO;
         //      [hud hide:YES afterDelay:1.5];
         
         if (failure) {
             failure(error,operation);
         }
         
     }];
}

+ (void)Post:(NSString *)urlStr
     hudText:(NSString *)text
hudSuperView:(UIView *)hudSuperView
  parameters:(NSDictionary *)parameters
     success:(successBlock)success
     failure:(failureBlock)failure{
    
    [self Post:urlStr hudText:text hudSuperView:hudSuperView parameters:parameters graceTime:NetworkRequestGraceTimeTypeNormal success:success failure:failure];
}

+ (void)Post:(NSString *)urlStr
     hudText:(NSString *)text
hudSuperView:(UIView *)hudSuperView
  parameters:(NSDictionary *)parameters
   graceTime:(NetworkRequestGraceTimeType)graceTime
     success:(successBlock)success
     failure:(failureBlock)failure{
    
//    if (![self isConnectionAvailable])
//    {
//        [self showExceptionDialog];
//        return ;
//    }
    
    AFHTTPRequestOperationManager *manager = [self manager:YES isHTTPResponseSerializer:NO];
    
    // 获取转圈控件
    MBProgressHUD *hud = [self hud:graceTime hudText:text hudSuperView:hudSuperView];
    
    // oper和operation指向同一个操作，但是状态不同，oper为isReady状态，operation是isFinished状态
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id responseObject) {
        
//        DeBugLog(operation.request.URL);
        // 任务结束，设置状态
        hud.taskInProgress = NO;
        [hud hide:YES];
        
        if (success)
        {
            success(responseObject,operation);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        hud.taskInProgress = NO;
        [hud hide:YES];
        
        if (failure) {
            failure(error,operation);
        }
    }];
    
}

+(void)PostUpload:(NSString *)urlStr
          hudText:(NSString *)text
     hudSuperView:(UIView *)hudSuperView
       parameters:(NSDictionary *)parameters
          success:(successBlock)success
          failure:(failureBlock)failure
{
    [self PostUpload:urlStr hudText:text  hudSuperView:hudSuperView parameters:parameters graceTime:NetworkRequestGraceTimeTypeNormal success:success failure:failure];
}

+(void)PostUpload:(NSString *)urlStr
          hudText:(NSString *)text
     hudSuperView:(UIView *)hudSuperView
       parameters:(NSDictionary *)parameters
        graceTime:(NetworkRequestGraceTimeType)graceTime
          success:(successBlock)success
          failure:(failureBlock)failure
{
//    if (![self isConnectionAvailable])
//    {
//        [self showExceptionDialog];
//        return ;
//    }
    
    AFHTTPRequestOperationManager *manager = [self manager:YES isHTTPResponseSerializer:NO];
    
    // 获取转圈控件
    MBProgressHUD *hud = [self hud:graceTime hudText:text hudSuperView:hudSuperView];
    
    NSURL *filePath = [NSURL fileURLWithPath:parameters[@"avatarimage"]];
    
    [manager POST:urlStr parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:filePath name:@"avatarImage" error:nil];
         
     } success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         // 任务结束，设置状态
         hud.taskInProgress = NO;
         [hud hide:YES];
         
         if (success)
         {
             success(responseObject,operation);
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         hud.taskInProgress = NO;
         [hud hide:YES];
         
         if (failure) {
             failure(error,operation);
         }
     }];
    
}


#pragma mark - 私有方法

+ (AFHTTPRequestOperationManager *)manager:(BOOL)isHTTPRequestSerializer
                  isHTTPResponseSerializer:(BOOL)isHTTPResponseSerializer{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // AFN默认请求序列化器为HTTP类型
    if (!isHTTPRequestSerializer) {
        manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求数据是json类型
    }
    
    // AFN默认响应序列化器为JSON类型
    if (isHTTPResponseSerializer)
    {
        //        manager.responseSerializer = [AFHTTPResponseSerializer serializer];//返回数据是json类型
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"application/xml",@"text/json",@"text/xml",@"text/html",@"text/plain",@"text/javascript", nil];
    }
    
    // 需要设置的请求头写在此处,如设置token
    //    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    manager.requestSerializer.timeoutInterval = 5;
    
    return manager;
}

+ (MBProgressHUD *)hud:(NetworkRequestGraceTimeType)graceTimeType hudText:(NSString *)text  hudSuperView:(UIView *)hudSuperView
{
    
    NSTimeInterval graceTime = 0;
    switch (graceTimeType) {
        case NetworkRequestGraceTimeTypeNone:
            return nil;
            break;
        case NetworkRequestGraceTimeTypeNormal:
            graceTime = 0.5;
            break;
        case NetworkRequestGraceTimeTypeLong:
            graceTime = 1.0;
            break;
        case NetworkRequestGraceTimeTypeShort:
            graceTime = 0.1;
            break;
        case NetworkRequestGraceTimeTypeAlways:
            graceTime = 0;
            break;
    }
    
    MBProgressHUD *hud = [self hud];
    if (hudSuperView)
    {
        [hudSuperView addSubview:hud];
    }
    else
    {
        [kLastWindow addSubview:hud];
    }
    
    hud.graceTime = graceTime;
    
    hud.labelText = text;
    
    // 设置该属性，graceTime才能生效
    hud.taskInProgress = YES;
    [hud show:YES];
    
    [hud hide:YES afterDelay:1.5];
    
    return hud;
}


// 网络请求频率很高，不必每次都创建\销毁一个hud，只需创建一个反复使用即可
+ (MBProgressHUD *)hud{
    MBProgressHUD *hud = objc_getAssociatedObject(self, _cmd);
    if (!hud)
    {
        // 参数kLastWindow仅仅是用到了其CGFrame，并没有将hud添加到上面
        hud = [[MBProgressHUD alloc] initWithWindow:kLastWindow];
        hud.color=RGBHUDColor;
        hud.layer.shadowColor = [UIColor blackColor].CGColor;
        hud.layer.shadowOffset = CGSizeZero;
        hud.layer.shadowOpacity = 0.5;
        hud.layer.shadowRadius = 5.0f;
        
        objc_setAssociatedObject(self, _cmd, hud, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
    }
    return hud;
}

////看看网络是不是给力
//+ (BOOL)isConnectionAvailable{
//    // Create zero addy
//    struct sockaddr_in zeroAddress;
//    bzero(&zeroAddress, sizeof(zeroAddress));
//    zeroAddress.sin_len = sizeof(zeroAddress);
//    zeroAddress.sin_family = AF_INET;
//    
//    // Recover reachability flags
//    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
//    SCNetworkReachabilityFlags flags;
//    
//    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
//    CFRelease(defaultRouteReachability);
//    
//    if (!didRetrieveFlags)
//    {
////        DeBugLog(@"Error. Could not recover network reachability flags");
//        return NO;
//    }
//    BOOL isReachable = ((flags & kSCNetworkFlagsReachable) != 0);
//    BOOL needsConnection = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
//    return (isReachable && !needsConnection) ? YES : NO;
//}

+ (void)showExceptionDialog
{
    [MBProgressHUD showError:@"哎呀，好像没有网络呢！"];
}



#pragma mark -- 检测网络

+ (BOOL)checkNetWork{
    
    return YES;
}

@end
