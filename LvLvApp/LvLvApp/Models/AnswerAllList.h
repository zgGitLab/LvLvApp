//
//  AnswerAllList.h
//
//  Created by hope  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface AnswerAllList : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *answerContentAll;
@property (nonatomic, strong) NSString *timeDescrption;
@property (nonatomic, assign) double counts;
@property (nonatomic, strong) NSString *userIamgeUrl;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *answerID;
@property (nonatomic, assign) double collectCount;
@property (nonatomic, assign) double commentCount;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
