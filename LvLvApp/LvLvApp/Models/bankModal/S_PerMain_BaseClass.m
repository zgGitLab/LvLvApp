//
//  S_PerMain_BaseClass.m
//
//  Created by IOS8  on 15/12/7
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_PerMain_BaseClass.h"


NSString *const kS_PerMain_BaseClassMobile = @"Mobile";
NSString *const kS_PerMain_BaseClassQQ = @"QQ";
NSString *const kS_PerMain_BaseClassTelephone = @"Telephone";
NSString *const kS_PerMain_BaseClassEmail = @"Email";
NSString *const kS_PerMain_BaseClassID = @"ID";
NSString *const kS_PerMain_BaseClassUserName = @"UserName";
NSString *const kS_PerMain_BaseClassIntroduction = @"Introduction";
NSString *const kS_PerMain_BaseClassRegDT = @"RegDT";
NSString *const kS_PerMain_BaseClassCreateID = @"CreateID";
NSString *const kS_PerMain_BaseClassModiyDT = @"ModiyDT";
NSString *const kS_PerMain_BaseClassPassword = @"Password";
NSString *const kS_PerMain_BaseClassNickname = @"Nickname";
NSString *const kS_PerMain_BaseClassGender = @"Gender";
NSString *const kS_PerMain_BaseClassWXID = @"WXID";
NSString *const kS_PerMain_BaseClassStatus = @"Status";
NSString *const kS_PerMain_BaseClassImage = @"Image";
NSString *const kS_PerMain_BaseClassCreateDT = @"CreateDT";
NSString *const kS_PerMain_BaseClassModiyID = @"ModiyID";
NSString *const kS_PerMain_BaseClassIsVIP = @"IsVIP";


@interface S_PerMain_BaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_PerMain_BaseClass

@synthesize mobile = _mobile;
@synthesize qQ = _qQ;
@synthesize telephone = _telephone;
@synthesize email = _email;
@synthesize iDProperty = _iDProperty;
@synthesize userName = _userName;
@synthesize introduction = _introduction;
@synthesize regDT = _regDT;
@synthesize createID = _createID;
@synthesize modiyDT = _modiyDT;
@synthesize password = _password;
@synthesize nickname = _nickname;
@synthesize gender = _gender;
@synthesize wXID = _wXID;
@synthesize status = _status;
@synthesize image = _image;
@synthesize createDT = _createDT;
@synthesize modiyID = _modiyID;
@synthesize isVIP = _isVIP;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.mobile = [self objectOrNilForKey:kS_PerMain_BaseClassMobile fromDictionary:dict];
            self.qQ = [self objectOrNilForKey:kS_PerMain_BaseClassQQ fromDictionary:dict];
            self.telephone = [self objectOrNilForKey:kS_PerMain_BaseClassTelephone fromDictionary:dict];
            self.email = [self objectOrNilForKey:kS_PerMain_BaseClassEmail fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kS_PerMain_BaseClassID fromDictionary:dict] doubleValue];
            self.userName = [self objectOrNilForKey:kS_PerMain_BaseClassUserName fromDictionary:dict];
            self.introduction = [self objectOrNilForKey:kS_PerMain_BaseClassIntroduction fromDictionary:dict];
            self.regDT = [self objectOrNilForKey:kS_PerMain_BaseClassRegDT fromDictionary:dict];
            self.createID = [[self objectOrNilForKey:kS_PerMain_BaseClassCreateID fromDictionary:dict] doubleValue];
            self.modiyDT = [self objectOrNilForKey:kS_PerMain_BaseClassModiyDT fromDictionary:dict];
            self.password = [self objectOrNilForKey:kS_PerMain_BaseClassPassword fromDictionary:dict];
            self.nickname = [self objectOrNilForKey:kS_PerMain_BaseClassNickname fromDictionary:dict];
            self.gender = [self objectOrNilForKey:kS_PerMain_BaseClassGender fromDictionary:dict];
            self.wXID = [self objectOrNilForKey:kS_PerMain_BaseClassWXID fromDictionary:dict];
            self.status = [[self objectOrNilForKey:kS_PerMain_BaseClassStatus fromDictionary:dict] doubleValue];
            self.image = [self objectOrNilForKey:kS_PerMain_BaseClassImage fromDictionary:dict];
            self.createDT = [self objectOrNilForKey:kS_PerMain_BaseClassCreateDT fromDictionary:dict];
            self.modiyID = [[self objectOrNilForKey:kS_PerMain_BaseClassModiyID fromDictionary:dict] doubleValue];
            self.isVIP = [[self objectOrNilForKey:kS_PerMain_BaseClassIsVIP fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.mobile forKey:kS_PerMain_BaseClassMobile];
    [mutableDict setValue:self.qQ forKey:kS_PerMain_BaseClassQQ];
    [mutableDict setValue:self.telephone forKey:kS_PerMain_BaseClassTelephone];
    [mutableDict setValue:self.email forKey:kS_PerMain_BaseClassEmail];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kS_PerMain_BaseClassID];
    [mutableDict setValue:self.userName forKey:kS_PerMain_BaseClassUserName];
    [mutableDict setValue:self.introduction forKey:kS_PerMain_BaseClassIntroduction];
    [mutableDict setValue:self.regDT forKey:kS_PerMain_BaseClassRegDT];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createID] forKey:kS_PerMain_BaseClassCreateID];
    [mutableDict setValue:self.modiyDT forKey:kS_PerMain_BaseClassModiyDT];
    [mutableDict setValue:self.password forKey:kS_PerMain_BaseClassPassword];
    [mutableDict setValue:self.nickname forKey:kS_PerMain_BaseClassNickname];
    [mutableDict setValue:self.gender forKey:kS_PerMain_BaseClassGender];
    [mutableDict setValue:self.wXID forKey:kS_PerMain_BaseClassWXID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kS_PerMain_BaseClassStatus];
    [mutableDict setValue:self.image forKey:kS_PerMain_BaseClassImage];
    [mutableDict setValue:self.createDT forKey:kS_PerMain_BaseClassCreateDT];
    [mutableDict setValue:[NSNumber numberWithDouble:self.modiyID] forKey:kS_PerMain_BaseClassModiyID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isVIP] forKey:kS_PerMain_BaseClassIsVIP];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.mobile = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassMobile];
    self.qQ = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassQQ];
    self.telephone = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassTelephone];
    self.email = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassEmail];
    self.iDProperty = [aDecoder decodeDoubleForKey:kS_PerMain_BaseClassID];
    self.userName = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassUserName];
    self.introduction = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassIntroduction];
    self.regDT = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassRegDT];
    self.createID = [aDecoder decodeDoubleForKey:kS_PerMain_BaseClassCreateID];
    self.modiyDT = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassModiyDT];
    self.password = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassPassword];
    self.nickname = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassNickname];
    self.gender = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassGender];
    self.wXID = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassWXID];
    self.status = [aDecoder decodeDoubleForKey:kS_PerMain_BaseClassStatus];
    self.image = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassImage];
    self.createDT = [aDecoder decodeObjectForKey:kS_PerMain_BaseClassCreateDT];
    self.modiyID = [aDecoder decodeDoubleForKey:kS_PerMain_BaseClassModiyID];
    self.isVIP = [aDecoder decodeDoubleForKey:kS_PerMain_BaseClassIsVIP];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_mobile forKey:kS_PerMain_BaseClassMobile];
    [aCoder encodeObject:_qQ forKey:kS_PerMain_BaseClassQQ];
    [aCoder encodeObject:_telephone forKey:kS_PerMain_BaseClassTelephone];
    [aCoder encodeObject:_email forKey:kS_PerMain_BaseClassEmail];
    [aCoder encodeDouble:_iDProperty forKey:kS_PerMain_BaseClassID];
    [aCoder encodeObject:_userName forKey:kS_PerMain_BaseClassUserName];
    [aCoder encodeObject:_introduction forKey:kS_PerMain_BaseClassIntroduction];
    [aCoder encodeObject:_regDT forKey:kS_PerMain_BaseClassRegDT];
    [aCoder encodeDouble:_createID forKey:kS_PerMain_BaseClassCreateID];
    [aCoder encodeObject:_modiyDT forKey:kS_PerMain_BaseClassModiyDT];
    [aCoder encodeObject:_password forKey:kS_PerMain_BaseClassPassword];
    [aCoder encodeObject:_nickname forKey:kS_PerMain_BaseClassNickname];
    [aCoder encodeObject:_gender forKey:kS_PerMain_BaseClassGender];
    [aCoder encodeObject:_wXID forKey:kS_PerMain_BaseClassWXID];
    [aCoder encodeDouble:_status forKey:kS_PerMain_BaseClassStatus];
    [aCoder encodeObject:_image forKey:kS_PerMain_BaseClassImage];
    [aCoder encodeObject:_createDT forKey:kS_PerMain_BaseClassCreateDT];
    [aCoder encodeDouble:_modiyID forKey:kS_PerMain_BaseClassModiyID];
    [aCoder encodeDouble:_isVIP forKey:kS_PerMain_BaseClassIsVIP];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_PerMain_BaseClass *copy = [[S_PerMain_BaseClass alloc] init];
    
    if (copy) {

        copy.mobile = [self.mobile copyWithZone:zone];
        copy.qQ = [self.qQ copyWithZone:zone];
        copy.telephone = [self.telephone copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.userName = [self.userName copyWithZone:zone];
        copy.introduction = [self.introduction copyWithZone:zone];
        copy.regDT = [self.regDT copyWithZone:zone];
        copy.createID = self.createID;
        copy.modiyDT = [self.modiyDT copyWithZone:zone];
        copy.password = [self.password copyWithZone:zone];
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.gender = [self.gender copyWithZone:zone];
        copy.wXID = [self.wXID copyWithZone:zone];
        copy.status = self.status;
        copy.image = [self.image copyWithZone:zone];
        copy.createDT = [self.createDT copyWithZone:zone];
        copy.modiyID = self.modiyID;
        copy.isVIP = self.isVIP;
    }
    
    return copy;
}


@end
