//
//  CacheDataBase.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/10.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "CacheDataBase.h"
#define FMDBQuickCheck(SomeBool) { if (!(SomeBool)) { NSLog(@"Failure on line %d", __LINE__);} }
@implementation CacheDataBase
- (id)init
{
    return self;
}
- (BOOL)getDb
{
    NSString* filePath = [self dataFilePath];
    self.cacheDb = [[FMDatabase alloc] initWithPath:filePath];
    if (![self.cacheDb open]) {
        NSLog(@"open db false");
        return false;
    }
    [self.cacheDb setShouldCacheStatements:YES];
    self.queue = [FMDatabaseQueue databaseQueueWithPath:filePath];
    FMDBQuickCheck(self.queue);
    return true;
    
}
- (NSString *)dataFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"CacheModal.sqlite"];
}
// 创建表
- (void)createTables
{
    // 搜索条件表
    [self.cacheDb executeUpdate:@"create table if not exists SearchCondition(Condition text)"];
//    [self.db executeUpdate:@"create table if not exists InsuranceFee(S_CITY text primary key,S_PerTax blob)"];
//    [self.db executeUpdate:@"create table if not exists InsuranceInitial(LocalAmount double,ForeignAmount double)"];
}

#pragma mark 搜索条件
- (void)insertString:(NSString *)searchString
{
    if (!searchString)
        return;
    [self.queue inDatabase:^(FMDatabase *db) {
        [self.cacheDb executeUpdate:@"insert into SearchCondition (Condition) values (?)" ,searchString];
        
    }];
}

//- (void)insertString:(NSString *)searchString
//{
//    if (!searchString)
//        return;
//    
//    [self.queue inDatabase:^(FMDatabase *db) {
//        
//        FMResultSet *rs = [self.cacheDb executeQuery:@"select * from SearchCondition where Condition = ?",searchString];
//        if([rs next]) {
//            [self.cacheDb executeUpdate:@"update InsuranceInitial set (Condition) values (?)",searchString];
//        }
//        else
//        {
//            [self.cacheDb executeUpdate:@"insert into SearchCondition (Condition) values (?)" ,searchString];
//        }
//        [rs close];
//    }];
//}

#pragma mark 查询搜索条件
- (void)getSearchString:(NSMutableArray *)stringArr
{
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [self.cacheDb executeQuery:@"select Condition from SearchCondition"];
        while([rs next])
        {
            NSString *str = [rs stringForColumn:@"Condition"];
            
            if (str) {
                [stringArr addObject:str];
            }
        }
        [rs close];
    }];
}

- (void)deleteAllRecord
{
    [self.queue inDatabase:^(FMDatabase *db) {
        [self.cacheDb executeUpdate:@"delete from SearchCondition"];
    }];
}

@end
