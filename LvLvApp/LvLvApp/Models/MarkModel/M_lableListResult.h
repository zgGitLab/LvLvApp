//
//  M_lableListResult.h
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface M_lableListResult : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) id userid;
@property (nonatomic, assign) double notifyID;
@property (nonatomic, strong) id listResultDescription;
@property (nonatomic, assign) double aUserID;
@property (nonatomic, assign) double commentCount;
@property (nonatomic, strong) id timagesUrl;
@property (nonatomic, strong) NSString *logoUrl;
@property (nonatomic, strong) id questionContent;
@property (nonatomic, strong) NSString *answerContent;
@property (nonatomic, assign) double collectCount;
@property (nonatomic, strong) NSString *sortTime;
@property (nonatomic, strong) id times;
@property (nonatomic, strong) id nickname;
@property (nonatomic, assign) double types;
@property (nonatomic, assign) double counts;
@property (nonatomic, strong) id tUserID;
@property (nonatomic, strong) NSString *tID;
@property (nonatomic, strong) NSString *answerID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
