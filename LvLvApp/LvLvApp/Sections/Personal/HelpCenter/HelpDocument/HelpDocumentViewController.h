//
//  HelpDocumentViewController.h
//  LvLvApp
//
//  Created by hope on 15/12/24.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpDocumentViewController : BaseViewController
@property (nonatomic, strong)NSMutableArray *helpeArray;
@property (nonatomic, copy)NSMutableString *textstr;
@end
