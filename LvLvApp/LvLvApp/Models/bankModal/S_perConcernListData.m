//
//  S_perConcernListData.m
//
//  Created by IOS8  on 15/12/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_perConcernListData.h"


NSString *const kS_perConcernListDataID = @"ID";
NSString *const kS_perConcernListDataIntroduction = @"Introduction";
NSString *const kS_perConcernListDataNickName = @"NickName";
NSString *const kS_perConcernListDataImage = @"Image";


@interface S_perConcernListData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_perConcernListData

@synthesize iDProperty = _iDProperty;
@synthesize introduction = _introduction;
@synthesize nickName = _nickName;
@synthesize image = _image;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kS_perConcernListDataID fromDictionary:dict] doubleValue];
            self.introduction = [self objectOrNilForKey:kS_perConcernListDataIntroduction fromDictionary:dict];
            self.nickName = [self objectOrNilForKey:kS_perConcernListDataNickName fromDictionary:dict];
            self.image = [self objectOrNilForKey:kS_perConcernListDataImage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kS_perConcernListDataID];
    [mutableDict setValue:self.introduction forKey:kS_perConcernListDataIntroduction];
    [mutableDict setValue:self.nickName forKey:kS_perConcernListDataNickName];
    [mutableDict setValue:self.image forKey:kS_perConcernListDataImage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kS_perConcernListDataID];
    self.introduction = [aDecoder decodeObjectForKey:kS_perConcernListDataIntroduction];
    self.nickName = [aDecoder decodeObjectForKey:kS_perConcernListDataNickName];
    self.image = [aDecoder decodeObjectForKey:kS_perConcernListDataImage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kS_perConcernListDataID];
    [aCoder encodeObject:_introduction forKey:kS_perConcernListDataIntroduction];
    [aCoder encodeObject:_nickName forKey:kS_perConcernListDataNickName];
    [aCoder encodeObject:_image forKey:kS_perConcernListDataImage];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_perConcernListData *copy = [[S_perConcernListData alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.introduction = [self.introduction copyWithZone:zone];
        copy.nickName = [self.nickName copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
    }
    
    return copy;
}


@end
