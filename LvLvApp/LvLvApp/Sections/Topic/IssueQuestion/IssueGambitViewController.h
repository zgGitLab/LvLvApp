//
//  addGambitViewController.h
//  LvLvApp
//
//  Created by hope on 15/10/10.
//  Copyright (c) 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^backBlock) (void);
@interface IssueGambitViewController : BaseViewController
@property (nonatomic, strong)UITableView *tablewView;
@property (nonatomic, strong)UILabel *labella;
@property (nonatomic,copy) backBlock addBlock;
@end
