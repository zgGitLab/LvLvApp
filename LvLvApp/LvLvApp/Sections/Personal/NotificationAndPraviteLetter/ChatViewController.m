//
//  ChatViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/24.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "ChatViewController.h"

@interface ChatViewController ()
{
    UIView *bgView;
    LZTextView *_sendTex;
    NSMutableArray *dataArray;

}
@end

@implementation ChatViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.titleStr;
    [self crreateSendView];
    self.viewArray = [NSMutableArray array];
    dataArray = [NSMutableArray array];
    self.chatTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    // 拖动表的时候让键盘下去
    self.chatTable.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    // 当键盘改变的时候，会发送通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyChangeFram:) name:UIKeyboardWillChangeFrameNotification object:nil];
    if (!self.mySelfImage) {
        [self getHeaderIMage];
    }
    [self chatRecord];
}

- (void)getHeaderIMage
{
    [RequestManager mainPerInfo:self.userID andBlock:^(S_PerMain_BaseClass *s_perModal,NSString *message) {
        self.mySelfImage = [NSString stringWithFormat:@"http://%@/%@",K_IP,s_perModal.image];
        [self.chatTable reloadData];
    }];
}

// 历史记录
- (void)chatRecord
{
    __weak ChatViewController *VC = self;
    [RequestManager chatListWithRecID:self.otherID andSendID:self.userID andBlock:^(NSArray *dataArr, NSString *message) {
//        [VC.chatTable.mj_header endRefreshing];
        dataArray = [NSMutableArray arrayWithArray:dataArr];
        if (dataArray.count != 0) {
            [VC performSelectorOnMainThread:@selector(refreshAnserUI) withObject:nil waitUntilDone:YES];
        }
        
    }];
}

- (void)refreshAnserUI
{
    
    for (int i = 0 ; i < dataArray.count; i++) {
        S_ChatListData *chatModal = dataArray[i];
        NSString *sendID = [NSString stringWithFormat:@"%.0f",chatModal.sendID];
        if ( ![sendID isEqualToString:self.userID]) {
            [self createView:chatModal.textContent andIsMself:NO andImagStr:[NSString stringWithFormat:@"http://%@/%@",K_IP,chatModal.image] andTimeStr:chatModal.sendTime];
        }else{
            [self createView:chatModal.textContent andIsMself:YES andImagStr:[NSString stringWithFormat:@"http://%@/%@",K_IP,chatModal.image] andTimeStr:chatModal.sendTime];
        }

    }
    [self.chatTable reloadData];
    NSIndexPath *indexPath =[NSIndexPath indexPathForRow:self.viewArray.count -1 inSection:0];
    //让表滚动到指定的位置
    [self.chatTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 40, 15);
    [rightButton setTitle:@"清空" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(removeBtn:) forControlEvents:UIControlEventTouchUpInside];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}


- (void)crreateSendView
{
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, SIZE.height -64- 40 , SIZE.width, 40)];
    _sendTex = [[LZTextView alloc] initWithFrame:CGRectMake(10, 0, SIZE.width - 60, 40)];
    _sendTex.font = [UIFont systemFontOfSize:16];
    _sendTex.placeholder = @"请输入私信内容";
    _sendTex.placeholderColor = KPHCOLOR;
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sendButton.frame = CGRectMake(SIZE.width-45, 0, 40, 40);
    sendButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [sendButton setTitle:@"发送" forState:UIControlStateNormal];
    [sendButton setTitleColor:COLOR(73, 158, 242) forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(sendBtn:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:sendButton];
    [bgView addSubview:_sendTex];
    [self.view addSubview:bgView];
    
}


- (void)keyChangeFram:(NSNotification *)info
{
    // 获取键盘动画的曲线
    double time = [[info.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] doubleValue];
    CGRect rect;
    // 获得键盘的freme
    [[info.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&rect];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:time];
    self.chatTable.frame = CGRectMake(0, 0, SIZE.width,rect.origin.y-64-40);
    bgView.frame = CGRectMake(0, rect.origin.y - 104, SIZE.width, 40);
    if (self.viewArray.count!=0) {
        NSIndexPath *index = [NSIndexPath indexPathForRow:self.viewArray.count-1 inSection:0];
        [self.chatTable scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    [UIView commitAnimations];
}


- (void)sendBtn:(UIButton *)btn
{
    if ([_sendTex.text isEqualToString:@""]||_sendTex == nil) {
        return;
    }
    [self createView:_sendTex.text andIsMself:YES andImagStr:self.mySelfImage andTimeStr:@""];
    [self.chatTable reloadData];
    NSIndexPath *indexPath =[NSIndexPath indexPathForRow:self.viewArray.count -1 inSection:0];
    //让表滚动到指定的位置
    [self.chatTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

    [RequestManager sendMessage:self.otherID andSendID:self.userID andTextConent:_sendTex.text andCategory:@"1" andBlock:^(NSString *textConent, NSString *message) {
        
    }];

    _sendTex.text = nil;
}


// 构建气泡
- (void)createView:(NSString *)sayString andIsMself:(BOOL)isMySelf andImagStr:(NSString *)imagstr andTimeStr:(NSString *)timeStr
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16] , NSFontAttributeName, nil];
    CGFloat strWith = [sayString sizeWithAttributes:dictionary].width;
    CGFloat strHiget = 0;
    CGFloat bgViewWith = 0;
    if (strWith > SIZE.width-170 ) {
        bgViewWith = SIZE.width-80;
        strWith = SIZE.width-170;
        // 计算文字的高度
        CGSize textSize = [sayString boundingRectWithSize:CGSizeMake(SIZE.width-170, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
        strHiget = textSize.height;
    }else{
        strHiget = 15;
        bgViewWith = strWith + 90;
    }
    
    UIView *bgVI = [[UIView alloc] init];
//    bgVI.backgroundColor = [UIColor yellowColor];
    bgVI.tag = 110;
    UIView *vi = [[UIView alloc] init];
//    vi.backgroundColor = [UIColor redColor];
    CGFloat minSpac = strHiget + 45;
    if (![timeStr isEqualToString:@""]) {
        minSpac += 50;
        UILabel *timeLab = [[UILabel alloc] initWithFrame:CGRectMake(SIZE.width/2.0-100, 10, 200, 15)];
        timeLab.textColor = COLOR(171, 171, 171);
        timeLab.font = [UIFont systemFontOfSize:11];
        timeLab.textAlignment = NSTextAlignmentCenter;
        timeLab.text = timeStr;
        [bgVI addSubview:timeLab];
         vi.frame = CGRectMake(isMySelf ?SIZE.width-bgViewWith  : 0, 35,bgViewWith, strHiget + 45);
    }else{
        // 创建最外层的view
        vi.frame = CGRectMake(isMySelf ?SIZE.width-bgViewWith  : 0, 0,bgViewWith, strHiget + 45);
    }
    
//    vi.backgroundColor = [UIColor redColor];
    // 创建label
    UILabel *textLabel= [[UILabel alloc] initWithFrame:CGRectMake(isMySelf?bgViewWith - 80-strWith:80,10 , strWith, strHiget)];
    NSLog(@"%f,%f",strWith,SIZE.width/2.0-110);
    textLabel.font = [UIFont systemFontOfSize:16];
    textLabel.text = sayString;
    textLabel.textColor = isMySelf?COLOR(255, 255, 255):COLOR(0, 0, 0);
    textLabel.numberOfLines = 0;
    
    // 背景图片
    NSString *imageName = isMySelf ? @"聊天-蓝" : @"聊天-白";
    UIImage *oldImage = [UIImage imageNamed:imageName];
    // 拉伸背景图片
    UIImage *newImage = [oldImage stretchableImageWithLeftCapWidth:25 topCapHeight:25];
    // 创建文本背景
    UIImageView *bjImage = [[UIImageView alloc] init];
    bjImage.frame = CGRectMake(isMySelf?bgViewWith-90-strWith:70, 0, strWith+20, strHiget+20);
    bjImage.image = newImage;
    bjImage.layer.cornerRadius = 5;
    bjImage.clipsToBounds = YES;
    
    // 创建头像
    UIImageView *headImage = [[UIImageView alloc] init];
    headImage.frame = CGRectMake(isMySelf ? vi.frame.size.width-60 : 10, 0, 40, 40);
    [headImage sd_setImageWithURL:[NSURL URLWithString:imagstr] placeholderImage:[UIImage imageNamed:@"头像"]];
    headImage.layer.cornerRadius = 20;
    headImage.clipsToBounds = YES;
    
    bgVI.frame = CGRectMake(0, 10, SIZE.width, CGRectGetMaxY(vi.frame)+20);
    
    [vi addSubview:bjImage];
    [vi addSubview:textLabel];
    [vi addSubview:headImage];
    [bgVI addSubview:vi];
     [self.viewArray addObject:bgVI];
    
}


- (void)removeBtn:(UIButton *)btn
{
    [RequestManager deleteAllMessageWithSendID:self.userID andRecID:self.otherID andBlock:^(NSString *message) {
        
        if ([message isEqualToString:@"删除成功"]) {
            [self.viewArray removeAllObjects];
            [self.chatTable reloadData];
        }else{
            K_ALERT(message);
        }
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    // 解决程勇问题，加载之前先去找用过的，然后把它从父视图移除
    [[cell viewWithTag:110] removeFromSuperview];
    UIView *vi = [self.viewArray objectAtIndex:indexPath.row];
    [cell addSubview:vi];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UIView *vi =[self.viewArray objectAtIndex:indexPath.row];
    return vi.frame.size.height;
    
}


@end
