//
//  LABLEABaseClass.m
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "LABLEABaseClass.h"
#import "LABLEAData.h"


NSString *const kLABLEABaseClassCode = @"Code";
NSString *const kLABLEABaseClassMessage = @"Message";
NSString *const kLABLEABaseClassData = @"Data";


@interface LABLEABaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LABLEABaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kLABLEABaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kLABLEABaseClassMessage fromDictionary:dict];
            self.data = [LABLEAData modelObjectWithDictionary:[dict objectForKey:kLABLEABaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kLABLEABaseClassCode];
    [mutableDict setValue:self.message forKey:kLABLEABaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kLABLEABaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kLABLEABaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kLABLEABaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kLABLEABaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kLABLEABaseClassCode];
    [aCoder encodeObject:_message forKey:kLABLEABaseClassMessage];
    [aCoder encodeObject:_data forKey:kLABLEABaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    LABLEABaseClass *copy = [[LABLEABaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
