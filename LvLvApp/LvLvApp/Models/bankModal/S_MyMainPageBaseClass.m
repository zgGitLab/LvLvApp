//
//  S_MyMainPageBaseClass.m
//
//  Created by IOS8  on 15/12/10
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_MyMainPageBaseClass.h"


NSString *const kS_MyMainPageBaseClassPosition = @"Position";
NSString *const kS_MyMainPageBaseClassShareCount = @"ShareCount";
NSString *const kS_MyMainPageBaseClassID = @"ID";
NSString *const kS_MyMainPageBaseClassAnswerCount = @"AnswerCount";
NSString *const kS_MyMainPageBaseClassIntroduction = @"Introduction";
NSString *const kS_MyMainPageBaseClassCompany = @"Company";
NSString *const kS_MyMainPageBaseClassResponseCount = @"ResponseCount";
NSString *const kS_MyMainPageBaseClassNickName = @"NickName";
NSString *const kS_MyMainPageBaseClassTelPhone = @"TelPhone";
NSString *const kS_MyMainPageBaseClassFansCount = @"FansCount";
NSString *const kS_MyMainPageBaseClassQuestionCount = @"QuestionCount";
NSString *const kS_MyMainPageBaseClassGender = @"Gender";
NSString *const kS_MyMainPageBaseClassCollectCount = @"CollectCount";
NSString *const kS_MyMainPageBaseClassQuestionFocus = @"QuestionFocus";
NSString *const kS_MyMainPageBaseClassImage = @"Image";
NSString *const kS_MyMainPageBaseClassIsV = @"IsV";
NSString *const kS_MyMainPageBaseClassIsFocus = @"IsFocus";


@interface S_MyMainPageBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_MyMainPageBaseClass

@synthesize position = _position;
@synthesize shareCount = _shareCount;
@synthesize iDProperty = _iDProperty;
@synthesize answerCount = _answerCount;
@synthesize introduction = _introduction;
@synthesize company = _company;
@synthesize responseCount = _responseCount;
@synthesize nickName = _nickName;
@synthesize telPhone = _telPhone;
@synthesize fansCount = _fansCount;
@synthesize questionCount = _questionCount;
@synthesize gender = _gender;
@synthesize collectCount = _collectCount;
@synthesize questionFocus = _questionFocus;
@synthesize image = _image;
@synthesize isV = _isV;
@synthesize IsFocus = _IsFocus;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.position = [self objectOrNilForKey:kS_MyMainPageBaseClassPosition fromDictionary:dict];
            self.shareCount = [[self objectOrNilForKey:kS_MyMainPageBaseClassShareCount fromDictionary:dict] doubleValue];
            self.iDProperty = [[self objectOrNilForKey:kS_MyMainPageBaseClassID fromDictionary:dict] doubleValue];
            self.answerCount = [[self objectOrNilForKey:kS_MyMainPageBaseClassAnswerCount fromDictionary:dict] doubleValue];
            self.introduction = [self objectOrNilForKey:kS_MyMainPageBaseClassIntroduction fromDictionary:dict];
            self.company = [self objectOrNilForKey:kS_MyMainPageBaseClassCompany fromDictionary:dict];
            self.responseCount = [[self objectOrNilForKey:kS_MyMainPageBaseClassResponseCount fromDictionary:dict] doubleValue];
            self.nickName = [self objectOrNilForKey:kS_MyMainPageBaseClassNickName fromDictionary:dict];
            self.telPhone = [self objectOrNilForKey:kS_MyMainPageBaseClassTelPhone fromDictionary:dict];
            self.fansCount = [[self objectOrNilForKey:kS_MyMainPageBaseClassFansCount fromDictionary:dict] doubleValue];
            self.questionCount = [[self objectOrNilForKey:kS_MyMainPageBaseClassQuestionCount fromDictionary:dict] doubleValue];
            self.gender = [self objectOrNilForKey:kS_MyMainPageBaseClassGender fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kS_MyMainPageBaseClassCollectCount fromDictionary:dict] doubleValue];
            self.questionFocus = [[self objectOrNilForKey:kS_MyMainPageBaseClassQuestionFocus fromDictionary:dict] doubleValue];
            self.image = [self objectOrNilForKey:kS_MyMainPageBaseClassImage fromDictionary:dict];
            self.isV = [[self objectOrNilForKey:kS_MyMainPageBaseClassIsV fromDictionary:dict] doubleValue];
            self.IsFocus = [[self objectOrNilForKey:kS_MyMainPageBaseClassIsFocus fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.position forKey:kS_MyMainPageBaseClassPosition];
    [mutableDict setValue:[NSNumber numberWithDouble:self.shareCount] forKey:kS_MyMainPageBaseClassShareCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kS_MyMainPageBaseClassID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.answerCount] forKey:kS_MyMainPageBaseClassAnswerCount];
    [mutableDict setValue:self.introduction forKey:kS_MyMainPageBaseClassIntroduction];
    [mutableDict setValue:self.company forKey:kS_MyMainPageBaseClassCompany];
    [mutableDict setValue:[NSNumber numberWithDouble:self.responseCount] forKey:kS_MyMainPageBaseClassResponseCount];
    [mutableDict setValue:self.nickName forKey:kS_MyMainPageBaseClassNickName];
    [mutableDict setValue:self.telPhone forKey:kS_MyMainPageBaseClassTelPhone];
    [mutableDict setValue:[NSNumber numberWithDouble:self.fansCount] forKey:kS_MyMainPageBaseClassFansCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.questionCount] forKey:kS_MyMainPageBaseClassQuestionCount];
    [mutableDict setValue:self.gender forKey:kS_MyMainPageBaseClassGender];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kS_MyMainPageBaseClassCollectCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.questionFocus] forKey:kS_MyMainPageBaseClassQuestionFocus];
    [mutableDict setValue:self.image forKey:kS_MyMainPageBaseClassImage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isV] forKey:kS_MyMainPageBaseClassIsV];
    [mutableDict setValue:[NSNumber numberWithDouble:self.IsFocus] forKey:kS_MyMainPageBaseClassIsFocus];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.position = [aDecoder decodeObjectForKey:kS_MyMainPageBaseClassPosition];
    self.shareCount = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassShareCount];
    self.iDProperty = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassID];
    self.answerCount = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassAnswerCount];
    self.introduction = [aDecoder decodeObjectForKey:kS_MyMainPageBaseClassIntroduction];
    self.company = [aDecoder decodeObjectForKey:kS_MyMainPageBaseClassCompany];
    self.responseCount = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassResponseCount];
    self.nickName = [aDecoder decodeObjectForKey:kS_MyMainPageBaseClassNickName];
    self.telPhone = [aDecoder decodeObjectForKey:kS_MyMainPageBaseClassTelPhone];
    self.fansCount = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassFansCount];
    self.questionCount = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassQuestionCount];
    self.gender = [aDecoder decodeObjectForKey:kS_MyMainPageBaseClassGender];
    self.collectCount = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassCollectCount];
    self.questionFocus = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassQuestionFocus];
    self.image = [aDecoder decodeObjectForKey:kS_MyMainPageBaseClassImage];
    self.isV = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassIsV];
    self.IsFocus = [aDecoder decodeDoubleForKey:kS_MyMainPageBaseClassIsFocus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_position forKey:kS_MyMainPageBaseClassPosition];
    [aCoder encodeDouble:_shareCount forKey:kS_MyMainPageBaseClassShareCount];
    [aCoder encodeDouble:_iDProperty forKey:kS_MyMainPageBaseClassID];
    [aCoder encodeDouble:_answerCount forKey:kS_MyMainPageBaseClassAnswerCount];
    [aCoder encodeObject:_introduction forKey:kS_MyMainPageBaseClassIntroduction];
    [aCoder encodeObject:_company forKey:kS_MyMainPageBaseClassCompany];
    [aCoder encodeDouble:_responseCount forKey:kS_MyMainPageBaseClassResponseCount];
    [aCoder encodeObject:_nickName forKey:kS_MyMainPageBaseClassNickName];
    [aCoder encodeObject:_telPhone forKey:kS_MyMainPageBaseClassTelPhone];
    [aCoder encodeDouble:_fansCount forKey:kS_MyMainPageBaseClassFansCount];
    [aCoder encodeDouble:_questionCount forKey:kS_MyMainPageBaseClassQuestionCount];
    [aCoder encodeObject:_gender forKey:kS_MyMainPageBaseClassGender];
    [aCoder encodeDouble:_collectCount forKey:kS_MyMainPageBaseClassCollectCount];
    [aCoder encodeDouble:_questionFocus forKey:kS_MyMainPageBaseClassQuestionFocus];
    [aCoder encodeObject:_image forKey:kS_MyMainPageBaseClassImage];
    [aCoder encodeDouble:_isV forKey:kS_MyMainPageBaseClassIsV];
    [aCoder encodeDouble:_IsFocus forKey:kS_MyMainPageBaseClassIsFocus];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_MyMainPageBaseClass *copy = [[S_MyMainPageBaseClass alloc] init];
    
    if (copy) {

        copy.position = [self.position copyWithZone:zone];
        copy.shareCount = self.shareCount;
        copy.iDProperty = self.iDProperty;
        copy.answerCount = self.answerCount;
        copy.introduction = [self.introduction copyWithZone:zone];
        copy.company = [self.company copyWithZone:zone];
        copy.responseCount = self.responseCount;
        copy.nickName = [self.nickName copyWithZone:zone];
        copy.telPhone = [self.telPhone copyWithZone:zone];
        copy.fansCount = self.fansCount;
        copy.questionCount = self.questionCount;
        copy.gender = [self.gender copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.questionFocus = self.questionFocus;
        copy.image = [self.image copyWithZone:zone];
        copy.isV = self.isV;
        copy.IsFocus = self.IsFocus;
    }
    
    return copy;
}


@end
