//
//  S_LawDetailBaseClass.m
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_LawDetailBaseClass.h"
#import "S_LawDetailData.h"


NSString *const kS_LawDetailBaseClassCode = @"Code";
NSString *const kS_LawDetailBaseClassMessage = @"Message";
NSString *const kS_LawDetailBaseClassData = @"Data";


@interface S_LawDetailBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_LawDetailBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_LawDetailBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_LawDetailBaseClassMessage fromDictionary:dict];
            self.data = [S_LawDetailData modelObjectWithDictionary:[dict objectForKey:kS_LawDetailBaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_LawDetailBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_LawDetailBaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kS_LawDetailBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_LawDetailBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_LawDetailBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_LawDetailBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_LawDetailBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_LawDetailBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_LawDetailBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_LawDetailBaseClass *copy = [[S_LawDetailBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
