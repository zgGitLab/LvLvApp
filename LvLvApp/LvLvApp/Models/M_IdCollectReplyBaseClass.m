//
//  M_IdCollectReplyBaseClass.m
//
//  Created by hope  on 16/1/7
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "M_IdCollectReplyBaseClass.h"
#import "M_IdCollectReplyData.h"


NSString *const kM_IdCollectReplyBaseClassCode = @"Code";
NSString *const kM_IdCollectReplyBaseClassMessage = @"Message";
NSString *const kM_IdCollectReplyBaseClassData = @"Data";


@interface M_IdCollectReplyBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_IdCollectReplyBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_IdCollectReplyBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_IdCollectReplyBaseClassMessage fromDictionary:dict];
    NSObject *receivedM_IdCollectReplyData = [dict objectForKey:kM_IdCollectReplyBaseClassData];
    NSMutableArray *parsedM_IdCollectReplyData = [NSMutableArray array];
    if ([receivedM_IdCollectReplyData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedM_IdCollectReplyData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedM_IdCollectReplyData addObject:[M_IdCollectReplyData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedM_IdCollectReplyData isKindOfClass:[NSDictionary class]]) {
       [parsedM_IdCollectReplyData addObject:[M_IdCollectReplyData modelObjectWithDictionary:(NSDictionary *)receivedM_IdCollectReplyData]];
    }

    self.data = [NSArray arrayWithArray:parsedM_IdCollectReplyData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_IdCollectReplyBaseClassCode];
    [mutableDict setValue:self.message forKey:kM_IdCollectReplyBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kM_IdCollectReplyBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_IdCollectReplyBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_IdCollectReplyBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_IdCollectReplyBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_IdCollectReplyBaseClassCode];
    [aCoder encodeObject:_message forKey:kM_IdCollectReplyBaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_IdCollectReplyBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_IdCollectReplyBaseClass *copy = [[M_IdCollectReplyBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
