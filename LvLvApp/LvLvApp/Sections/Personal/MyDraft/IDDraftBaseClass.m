//
//  IDDraftBaseClass.m
//
//  Created by hope  on 15/12/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IDDraftBaseClass.h"
#import "IDDraftData.h"


NSString *const kIDDraftBaseClassCode = @"Code";
NSString *const kIDDraftBaseClassMessage = @"Message";
NSString *const kIDDraftBaseClassData = @"Data";


@interface IDDraftBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IDDraftBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kIDDraftBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kIDDraftBaseClassMessage fromDictionary:dict];
    NSObject *receivedIDDraftData = [dict objectForKey:kIDDraftBaseClassData];
    NSMutableArray *parsedIDDraftData = [NSMutableArray array];
    if ([receivedIDDraftData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedIDDraftData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedIDDraftData addObject:[IDDraftData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedIDDraftData isKindOfClass:[NSDictionary class]]) {
       [parsedIDDraftData addObject:[IDDraftData modelObjectWithDictionary:(NSDictionary *)receivedIDDraftData]];
    }

    self.data = [NSArray arrayWithArray:parsedIDDraftData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kIDDraftBaseClassCode];
    [mutableDict setValue:self.message forKey:kIDDraftBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kIDDraftBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kIDDraftBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kIDDraftBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kIDDraftBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kIDDraftBaseClassCode];
    [aCoder encodeObject:_message forKey:kIDDraftBaseClassMessage];
    [aCoder encodeObject:_data forKey:kIDDraftBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    IDDraftBaseClass *copy = [[IDDraftBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
