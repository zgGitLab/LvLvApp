//
//  S_PerBaseInfoBaseClass.h
//
//  Created by IOS8  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_PerBaseInfoBaseClass : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double userID;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *introduction;
@property (nonatomic, strong) NSArray *jobList;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *telPhone;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSArray *eductList;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
