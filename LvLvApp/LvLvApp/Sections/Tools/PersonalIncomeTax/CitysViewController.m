//
//  CitysViewController.m
//  Tools
//
//  Created by IOS8 on 15/10/9.
//  Copyright © 2015年 IOS8. All rights reserved.
//

#import "CitysViewController.h"
#import "TaxData.h"
@interface CitysViewController ()
{
    NSMutableDictionary *cityDic;
    NSMutableArray *_cityName;
}
@end

@implementation CitysViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
       
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableArray *perTaxArray = [NSMutableArray array];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app.db getPersonallncomeTax:perTaxArray];
    [self getCityData:perTaxArray];
    // Do any additional setup after loading the view from its nib.
//    self.arrayHotCity = [NSMutableArray arrayWithObjects:@"广州市",@"北京市",@"上海市",@"天津市",@"西安市",@"重庆市",@"沈阳市",@"青岛市",@"济南市",@"深圳市",@"长沙市",@"无锡市", nil];
//    self.keys = [NSMutableArray array];
//    self.arrayCitys = [NSMutableArray array];
    self.title = @"城市列表";
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:249/255.0 green:250/255.0 blue:251/255.0 alpha:1];
//    [self getCityData];
    
}


#pragma mark - 获取城市数据
-(void)getCityData:(NSMutableArray *)array
{
    
    cityDic=[[NSMutableDictionary alloc]init];
    
//    NSString * pinyin=nil;
    
    NSMutableArray *arr=nil;
    
    for (TaxData *tax in array) {
        
//        pinyin=[[ChineseToPinyin pinyinFromChiniseString:city] substringToIndex:1];
        
        //如果包含key
        if([[cityDic allKeys]containsObject:tax.cityZimu]){
            
            arr=[cityDic objectForKey:tax.cityZimu];
            [arr addObject:tax.cityName];
            [cityDic setObject:arr forKey:tax.cityZimu];
            
        }else{
            
            arr= [[NSMutableArray alloc]initWithObjects:tax.cityName, nil];
            [cityDic setObject:arr forKey:tax.cityZimu];
            
        }
        
    }
    
    
     _cityName = [NSMutableArray arrayWithArray:[[cityDic allKeys] sortedArrayUsingSelector:@selector(compare:)]];
    NSLog(@"%@",_cityName);
    
//    NSString *path=[[NSBundle mainBundle] pathForResource:@"citydict"
//                                                   ofType:@"plist"];
//    self.cities = [NSMutableDictionary dictionaryWithContentsOfFile:path];
//    [self.keys addObjectsFromArray:[[self.cities allKeys] sortedArrayUsingSelector:@selector(compare:)]];
//    
//    //添加热门城市
//    NSString *strHot = @"热";
//    [self.keys insertObject:strHot atIndex:0];
//    [self.cities setObject:_arrayHotCity forKey:strHot];
}
 //索引
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return _cityName;//[cityDic allKeys];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [cityDic allKeys].count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = [_cityName objectAtIndex:section];
    NSArray *citySection = [cityDic objectForKey:key];
    return [citySection count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    bgView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 0, 250, 30)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = [UIFont systemFontOfSize:12];
    
//    NSString *key = [[cityDic allKeys] objectAtIndex:section];
//    NSArray *citySection = [cityDic objectForKey:key];
//    NSString *key = [_keys objectAtIndex:section];
//    if ([key rangeOfString:@"热"].location != NSNotFound) {
//        titleLabel.text = @"热门城市";
//    }else{
        titleLabel.text = [_cityName objectAtIndex:section];
//    }
    [bgView addSubview:titleLabel];
    
    return bgView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    NSString *key = [_cityName objectAtIndex:indexPath.section];
    NSArray *citySection = [cityDic objectForKey:key];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.textLabel setTextColor:[UIColor blackColor]];
        cell.textLabel.font = [UIFont systemFontOfSize:18];
    }
    cell.textLabel.text = citySection[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"100000000");
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeCity" object:cell.textLabel.text];
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"11111111111");
}
- (IBAction)backBtn:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)finshBtn:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
