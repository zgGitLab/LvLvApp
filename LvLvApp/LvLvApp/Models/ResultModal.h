//
//  ResultModal.h
//  Tools
//
//  Created by IOS8 on 15/10/15.
//  Copyright © 2015年 IOS8. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResultModal : NSObject   // 计算结果类

@property (nonatomic,assign)double PerRetirePay;               // 个人养老
@property (nonatomic,assign)double PerMediccalPay;             // 个人医疗
@property (nonatomic,assign)double PerJoblessPay;              // 个人失业
@property (nonatomic,assign)double PerInjuryPay;               // 个人工伤
@property (nonatomic,assign)double PerBearPay;                 // 个人生育
@property (nonatomic,assign)double PerHousePay;                // 个人公积金
@property (nonatomic,assign)double ComRetirePay;               // 单位养老
@property (nonatomic,assign)double ComMedicalPay;              // 单位医疗
@property (nonatomic,assign)double ComJoblessPay;              // 单位失业
@property (nonatomic,assign)double ComInjuryPay;               // 单位工伤
@property (nonatomic,assign)double ComBearPay;                 // 单位生育
@property (nonatomic,assign)double ComHousePay;                // 单位公积金
@property (nonatomic,assign)double InsuranceAmount;             // 社保基数
@property (nonatomic,assign)double HouseAmount;                 // 公积金基数
@property (nonatomic,assign)double perTaxPay;                 // 个税
@property (nonatomic,assign)double countTax;                 // 计税金额

@end
