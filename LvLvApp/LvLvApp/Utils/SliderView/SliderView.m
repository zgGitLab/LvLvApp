//
//  SliderView.m
//  LvLvApp
//
//  Created by jim on 15/12/27.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "SliderView.h"

@implementation SliderView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    UITapGestureRecognizer *taps = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(remove)];
    taps.numberOfTapsRequired = 1;
    [self addGestureRecognizer:taps];
    
    CGSize size ;
    NSLog(@"%f",kWidth);
    size.width = kWidth - 70*2;
    if (kWidth == 320) {
        size.height = 4;
    }else{
        size.height = 6;
    }
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height-50, kWidth, 50)];
    view.backgroundColor = [UIColor whiteColor];
    [self addSubview:view];
    //背景图片
    UIImage *image = [UIImage imageNamed:@"字体调整"];
    self.width = size.width;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((kWidth-size.width)/2, (view.frame.size.height-size.height)/2, size.width, size.height)];
    imageView.image = image;
    CGFloat leading = (kWidth-20*2-10*2-size.width)/2;
    //滑块设置
    _slider = [[UISlider alloc] initWithFrame:CGRectMake(imageView.frame.origin.x-14, imageView.frame.origin.y-13, size.width+28,33)];
    _slider.minimumValue = 15;
    _slider.maximumValue = 18;
    _slider.value = [SingeData sharedInstance].answerContentSize;
    _slider.minimumTrackTintColor = [UIColor clearColor];
    _slider.maximumTrackTintColor = [UIColor clearColor];
    [_slider addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventValueChanged];
    UITapGestureRecognizer *tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [_slider addGestureRecognizer:tap];
    [view addSubview:_slider];
    
    //大小字
    UILabel *small = [[UILabel alloc]initWithFrame:CGRectMake(leading, view.frame.size.height/2-10, 20, 20)];
    small.text = @"小";
    small.textColor = [UIColor lightGrayColor];
    small.font = [UIFont systemFontOfSize:14];
    [view addSubview:small];
    [view addSubview:imageView];
    UILabel *max = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame)+10, view.frame.size.height/2-10, 20, 20)];
    max.text = @"大";
    max.textColor = [UIColor lightGrayColor];
    max.font = [UIFont systemFontOfSize:17];
    [view addSubview:max];


}

- (void)valueChange:(UISlider *)slider{
    if (slider.value < 15.5f) {
        slider.value = 15.f;
        [SingeData sharedInstance].answerContentSize = 15;

    }else if(slider.value <=16.5){
        slider.value = 16.f;
        [SingeData sharedInstance].answerContentSize = 16;

    }else if(slider.value <17.5){
        slider.value = 17.f;
        [SingeData sharedInstance].answerContentSize = 17;

    }else{
        slider.value = 18.f;
        [SingeData sharedInstance].answerContentSize = 18;

    }
    
    NSLog(@"slider.value%f",slider.value);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
}


- (void)tapAction:(UITapGestureRecognizer *)tap{
    //取得点击点
    CGPoint p = [tap locationInView:_slider];
    CGFloat wid =self.width/3;
    CGFloat wid1=14+wid/2;
    NSLog(@"%lf",p.x);
    if (p.x < wid1) {
        _slider.value = 15.f;
        [SingeData sharedInstance].answerContentSize = 15;

    }else if(p.x <=wid1+wid){
        _slider.value = 16.f;
        [SingeData sharedInstance].answerContentSize = 16;

    }else if(p.x <wid1+wid*2){
        _slider.value = 17.f;
        [SingeData sharedInstance].answerContentSize = 17;

    }else{
        _slider.value = 18.f;
        [SingeData sharedInstance].answerContentSize = 18;

    }

    [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
}

- (void)remove{
    [self removeFromSuperview];
}

@end
