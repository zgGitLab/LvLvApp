//
//  IssueView.h
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IssueTableViewCell.h"
#import "SearchTopicViewController.h"
#import "M_IssueData.h"

typedef void (^arrayBlock)(NSMutableArray *);

@protocol IssueViewDelegate<NSObject>

- (void)click:(NSInteger)indxe;

@end
@interface IssueView : UIView <UITableViewDelegate,UITableViewDataSource>

typedef  void (^cellClick)(IssueView *);
@property (nonatomic, strong)UITableView *issueTableView;
@property(nonatomic, unsafe_unretained) id<IssueViewDelegate>IssueDelegate;
@property (nonatomic, strong)NSMutableArray *IssData;
@property (nonatomic,copy) arrayBlock myBlock;
@property (nonatomic, strong)NSString *keyStr;

@property (nonatomic,copy) cellClick clickBlock;
@property (nonatomic, strong)NSString *searString;
@property (nonatomic ,strong)M_IssueData *isdata;
@property (nonatomic,assign)NSInteger pageIndexs;
- (void)getSearText:(NSString *)str;
- (id)initWithString:(NSString*)str ;
@end

