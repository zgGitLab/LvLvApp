//
//  myCollectCaseData.h
//
//  Created by hope  on 15/12/17
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface myCollectCaseData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, strong) NSString *judgeDate;
@property (nonatomic, assign) double courtId;
@property (nonatomic, strong) NSString *court;
@property (nonatomic, assign) double docId;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *character;
@property (nonatomic, strong) NSString *content;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
