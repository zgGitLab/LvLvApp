//
//  LawModel.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/25.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LawModel : BaseDataModel

@property (nonatomic,copy) NSString *Total;//法条总数
@property (nonatomic,strong) NSArray *Items;//法条详情
@property (nonatomic,strong) NSArray *FilterGroups;//筛选字段
@property (nonatomic,strong) NSArray *Tokens;//拆分关键字数组
@end
