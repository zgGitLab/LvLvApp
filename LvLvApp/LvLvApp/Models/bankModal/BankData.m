//
//  BankData.m
//
//  Created by IOS8  on 15/10/27
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "BankData.h"

#import "DataModels.h"

NSString *const kBankDataBeginDT       = @"BeginDT";
NSString *const kBankDataOneYear       = @"OneYear";
NSString *const kBankDataFiveYear      = @"FiveYear";
NSString *const kBankDataHalfYear      = @"HalfYear";
NSString *const kBankDataEndDT         = @"EndDT";
NSString *const kBankDataAboveFiveYear = @"AboveFiveYear";
NSString *const kBankDataID            = @"ID";
NSString *const kBankDataThreeYear     = @"ThreeYear";


@interface BankData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BankData

@synthesize beginDT       = _beginDT;
@synthesize oneYear       = _oneYear;
@synthesize fiveYear      = _fiveYear;
@synthesize halfYear      = _halfYear;
@synthesize endDT         = _endDT;
@synthesize aboveFiveYear = _aboveFiveYear;
@synthesize iDProperty    = _iDProperty;
@synthesize threeYear     = _threeYear;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.beginDT       = [self objectOrNilForKey:kBankDataBeginDT fromDictionary:dict];
            self.oneYear       = [self objectOrNilForKey:kBankDataOneYear fromDictionary:dict];
            self.fiveYear      = [self objectOrNilForKey:kBankDataFiveYear fromDictionary:dict];
            self.halfYear      = [self objectOrNilForKey:kBankDataHalfYear fromDictionary:dict];
            self.endDT         = [self objectOrNilForKey:kBankDataEndDT fromDictionary:dict];
            self.aboveFiveYear = [self objectOrNilForKey:kBankDataAboveFiveYear fromDictionary:dict];
            self.iDProperty    = [self objectOrNilForKey:kBankDataID fromDictionary:dict];
            self.threeYear     = [self objectOrNilForKey:kBankDataThreeYear fromDictionary:dict];
    }
    AppDelegate *app =[UIApplication sharedApplication].delegate;
    [app.db searchBankData:self.iDProperty and:self];
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.beginDT forKey:kBankDataBeginDT];
    [mutableDict setValue:self.oneYear forKey:kBankDataOneYear];
    [mutableDict setValue:self.fiveYear forKey:kBankDataFiveYear];
    [mutableDict setValue:self.halfYear forKey:kBankDataHalfYear];
    [mutableDict setValue:self.endDT forKey:kBankDataEndDT];
    [mutableDict setValue:self.aboveFiveYear forKey:kBankDataAboveFiveYear];
    [mutableDict setValue:self.iDProperty forKey:kBankDataID];
    [mutableDict setValue:self.threeYear forKey:kBankDataThreeYear];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.beginDT       = [aDecoder decodeObjectForKey:kBankDataBeginDT];
    self.oneYear       = [NSString stringWithFormat:@"%f",[aDecoder decodeDoubleForKey:kBankDataOneYear]];
    self.fiveYear      = [NSString stringWithFormat:@"%f",[aDecoder decodeDoubleForKey:kBankDataFiveYear]];
    self.halfYear      = [NSString stringWithFormat:@"%f",[aDecoder decodeDoubleForKey:kBankDataHalfYear]];
    self.endDT         = [aDecoder decodeObjectForKey:kBankDataEndDT];
    self.aboveFiveYear = [NSString stringWithFormat:@"%f",[aDecoder decodeDoubleForKey:kBankDataAboveFiveYear]];
    self.iDProperty    = [NSString stringWithFormat:@"%f",[aDecoder decodeDoubleForKey:kBankDataID]];
    self.threeYear     = [NSString stringWithFormat:@"%f",[aDecoder decodeDoubleForKey:kBankDataThreeYear]];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_beginDT forKey:kBankDataBeginDT];
    [aCoder encodeDouble:[_oneYear doubleValue]forKey:kBankDataOneYear];
    [aCoder encodeDouble:[_fiveYear doubleValue] forKey:kBankDataFiveYear];
    [aCoder encodeDouble:[_halfYear doubleValue]forKey:kBankDataHalfYear];
    [aCoder encodeObject:_endDT forKey:kBankDataEndDT];
    [aCoder encodeDouble:[_aboveFiveYear doubleValue] forKey:kBankDataAboveFiveYear];
    [aCoder encodeDouble:[_iDProperty doubleValue]forKey:kBankDataID];
    [aCoder encodeDouble:[_threeYear doubleValue]forKey:kBankDataThreeYear];
}

- (id)copyWithZone:(NSZone *)zone
{
    BankData *copy = [[BankData alloc] init];
    
    if (copy) {

        copy.beginDT       = [self.beginDT copyWithZone:zone];
        copy.oneYear       = self.oneYear;
        copy.fiveYear      = self.fiveYear;
        copy.halfYear      = self.halfYear;
        copy.endDT         = [self.endDT copyWithZone:zone];
        copy.aboveFiveYear = self.aboveFiveYear;
        copy.iDProperty    = self.iDProperty;
        copy.threeYear     = self.threeYear;
    }
    
    return copy;
}


@end
