//
//  NotAndMessViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/20.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotAndMessViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) NSString *userID;
@property (weak, nonatomic) IBOutlet UITableView *bgTable;
@property (strong, nonatomic) NSString *mySelfImage;
@end
