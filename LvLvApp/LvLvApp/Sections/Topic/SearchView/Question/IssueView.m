//
//  IssueView.m
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "IssueView.h"
#import "IssueTableViewCell.h"
#import "GambitAnswerViewController.h"


#import "M_IssueBaseClass.h"
#import "M_IssueData.h"
#import "M_IssueDataModels.h"

//@interface IssueView ()
//{
//    NSInteger pageIndexs;
//}
//@end


@implementation IssueView 

- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0.5, [UIScreen mainScreen].bounds.size.width,  [UIScreen mainScreen].bounds.size.height );
      
    }
    _pageIndexs = 1;
    self.IssData=[[NSMutableArray alloc]init];
    [self issueTableViews];
    return self;
}

- (id)initWithString:(NSString*)str {
    
    
    self = [self init];
    
    self.keyStr = str;
    [self request:str WithPageindex:_pageIndexs andPageSize:10];
    [self.IssData removeAllObjects];
    [self.issueTableView reloadData];
    _issueTableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{

        [self request:str WithPageindex:++_pageIndexs andPageSize:10];
        [self layoutSubviews];}];
    

    return self;
}
- (void)issueTableViews
{
    self.issueTableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-88)style:UITableViewStylePlain];
    self.issueTableView.backgroundColor =[UIColor whiteColor];
    [self.issueTableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    self.issueTableView.delegate = self;
    self.issueTableView.dataSource = self;
    [self addSubview:self.issueTableView];
    [self setExtraCellLineHidden:self.issueTableView];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.IssData.count;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier =@"IssueID";
  
    
   IssueTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"IssueTableViewCell" owner:self options:nil][0];
    }
    if (self.IssData.count >indexPath.row) {
        self.isdata =self.IssData[indexPath.row];
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        [self getRangeStr:[Utils replaceImageString:self.isdata.title] findText:self.keyStr andArray:arr];
      
        cell.AttentionCount.text =[NSString stringWithFormat:@"关注 %d · 回答 %d",(int
                                                                               )self.isdata.focusCount,(int)self.isdata.answerCount];
        cell.headline.text =self.isdata.title;
        cell.headline.attributedText =[self getStrWith:arr  andStr:[Utils replaceImageString:self.isdata.title]];
        
        CGSize size = [self.isdata.title boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
        NSLog(@"%lf",size.width);
      
    
       
        

    }
   
     return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (self.IssData.count >indexPath.row) {
    self.isdata =self.IssData[indexPath.row];
    }
    
    CGSize size = [self.isdata.title boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
    NSLog(@"%lf",size.width);
        if (size.width >= (kWidth - 30)) {
            
            return 85;
        }else{
            return 65;
        }
}
- (NSAttributedString *)getStrWith:(NSMutableArray *)arr andStr:(NSString *)str
{
    NSMutableAttributedString *attributedString = nil;
    attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    for (int i = 0; i<arr.count; i++) {
        NSRange rang = [arr[i] rangeValue];
        // 设置富文本样式
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor colorWithRed:70/255.0 green:142/255.0 blue:239/255.0 alpha:1]
                                 range:NSMakeRange(rang.location, rang.length)];
    }
    return attributedString;
    
}

//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.issueTableView deselectRowAtIndexPath:indexPath animated:YES];
    IssueTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;

    if (_IssueDelegate) {
        [_IssueDelegate click:indexPath.row];
    }
}
//是没有数据的cell消失
-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [UIView new];
    
    view.backgroundColor = [UIColor clearColor];
    
    [tableView setTableFooterView:view];
}
//取消选择行
- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    [self.issueTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionBottom animated:YES];

}
-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    if([self numberOfSectionsInTableView:tableView]==(section+1)){
        return [UIView new];
    }
    return nil;
}

- (void)getSearText:(NSString *)str
{
    _pageIndexs = 1;
    NSLog(@"%@",str);
    self.searString =[NSString stringWithFormat:@"%@",str];
    NSLog(@"%@",self.searString);
    [self.IssData removeAllObjects];
    
    self.keyStr = str;
    
    [self request:str WithPageindex:1 andPageSize:10];
    _issueTableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
      
     
        [self request:str WithPageindex:++_pageIndexs
          andPageSize:10];
        [self layoutSubviews];}];
    
    
   
  
}
- (void)getRangeStr:(NSString *)text findText:(NSString *)findText andArray:(NSMutableArray *)arr
{
    int lengtiStr = (int)[text length];
    if (lengtiStr>100) {
        lengtiStr = 100;
    }
    NSString *temp = nil;
    for(int i =0; i < lengtiStr; i++)
    {
        if (text.length - i < findText.length) {
            break;
        }
        temp = [text substringWithRange:NSMakeRange(i, findText.length)];
        if ([temp isEqualToString:findText]) {
            NSRange rang = NSMakeRange(i, findText.length);
            NSLog(@"标题第%d个字是:%@", i, temp);
            [arr addObject:[NSValue valueWithRange:rang]];
            
        }
    }
}
- (void)request:(NSString *)TopicNames WithPageindex:(NSInteger)pageIndex andPageSize:(NSInteger)pageSize{
{
//    NSString *TopicName =TopicNames;
    // 1.url
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KsearcrIssueUrl,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
     request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"TopicName=%@",TopicNames]];
    [param appendString:[NSString stringWithFormat:@"&PageIndex=%ld",(long)pageIndex]];
   [param appendString:[NSString stringWithFormat:@"&PageSize=%ld",(long)pageSize]];
   
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
   // 5.发送请求
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
//     NSLog(@"data%@",data);
     if (data ==nil || connectionError)return ;
     NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//     NSLog(@"dict %@",dict);
     NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"`````````%@````",info);
        M_IssueBaseClass *iss =[[M_IssueBaseClass alloc] initWithDictionary:dict];
        [self.IssData addObjectsFromArray:iss.data];
         if (self.myBlock) {
             self.myBlock(self.IssData);
         }
//         NSLog(@"%@",self.IssData);
        [self.issueTableView reloadData];
        [self.issueTableView.mj_footer endRefreshing];
 }];
}
}
@end
