//
//  BaseDataModel.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/25.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseDataModel : NSObject

@property (nonatomic,strong) NSNumber *Code;
@property (nonatomic,copy) NSString *Message;
@property (nonatomic,strong) NSDictionary *Data;

- (void)setValue:(id)value forUndefinedKey:(NSString *)key;

@end
