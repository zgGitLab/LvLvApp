//
//  S_LawListList.m
//
//  Created by IOS8  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_LawListList.h"


NSString *const kS_LawListListCategory = @"category";
NSString *const kS_LawListListPubDate = @"pub_date";
NSString *const kS_LawListListAbstract = @"Abstract";
NSString *const kS_LawListListId = @"id";
NSString *const kS_LawListListPublishDep = @"publish_dep";
NSString *const kS_LawListListEffectiveness = @"effectiveness";
NSString *const kS_LawListListTitle = @"title";
NSString *const kS_LawListListCreatedAt = @"created_at";
NSString *const kS_LawListListTitleHighLighter = @"TitleHighLighter";
NSString *const kS_LawListListBody = @"body";


@interface S_LawListList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_LawListList

@synthesize category = _category;
@synthesize pubDate = _pubDate;
@synthesize abstract = _abstract;
@synthesize listIdentifier = _listIdentifier;
@synthesize court = _court;
@synthesize effectiveness = _effectiveness;
@synthesize title = _title;
@synthesize createdAt = _createdAt;
@synthesize titleHighLighter = _titleHighLighter;
@synthesize content = _content;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.category = [self objectOrNilForKey:kS_LawListListCategory fromDictionary:dict];
            self.pubDate = [self objectOrNilForKey:kS_LawListListPubDate fromDictionary:dict];
            self.abstract = [self objectOrNilForKey:kS_LawListListAbstract fromDictionary:dict];
            self.listIdentifier = [[self objectOrNilForKey:kS_LawListListId fromDictionary:dict] doubleValue];
            self.court = [self objectOrNilForKey:kS_LawListListPublishDep fromDictionary:dict];
            self.effectiveness = [self objectOrNilForKey:kS_LawListListEffectiveness fromDictionary:dict];
            self.title = [self objectOrNilForKey:kS_LawListListTitle fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kS_LawListListCreatedAt fromDictionary:dict];
            self.titleHighLighter = [self objectOrNilForKey:kS_LawListListTitleHighLighter fromDictionary:dict];
            self.content = [self objectOrNilForKey:kS_LawListListBody fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.category forKey:kS_LawListListCategory];
    [mutableDict setValue:self.pubDate forKey:kS_LawListListPubDate];
    [mutableDict setValue:self.abstract forKey:kS_LawListListAbstract];
    [mutableDict setValue:[NSNumber numberWithDouble:self.listIdentifier] forKey:kS_LawListListId];
    [mutableDict setValue:self.court forKey:kS_LawListListPublishDep];
    [mutableDict setValue:self.effectiveness forKey:kS_LawListListEffectiveness];
    [mutableDict setValue:self.title forKey:kS_LawListListTitle];
    [mutableDict setValue:self.createdAt forKey:kS_LawListListCreatedAt];
    [mutableDict setValue:self.titleHighLighter forKey:kS_LawListListTitleHighLighter];
    [mutableDict setValue:self.content forKey:kS_LawListListBody];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.category = [aDecoder decodeObjectForKey:kS_LawListListCategory];
    self.pubDate = [aDecoder decodeObjectForKey:kS_LawListListPubDate];
    self.abstract = [aDecoder decodeObjectForKey:kS_LawListListAbstract];
    self.listIdentifier = [aDecoder decodeDoubleForKey:kS_LawListListId];
    self.court = [aDecoder decodeObjectForKey:kS_LawListListPublishDep];
    self.effectiveness = [aDecoder decodeObjectForKey:kS_LawListListEffectiveness];
    self.title = [aDecoder decodeObjectForKey:kS_LawListListTitle];
    self.createdAt = [aDecoder decodeObjectForKey:kS_LawListListCreatedAt];
    self.titleHighLighter = [aDecoder decodeObjectForKey:kS_LawListListTitleHighLighter];
    self.content = [aDecoder decodeObjectForKey:kS_LawListListBody];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_category forKey:kS_LawListListCategory];
    [aCoder encodeObject:_pubDate forKey:kS_LawListListPubDate];
    [aCoder encodeObject:_abstract forKey:kS_LawListListAbstract];
    [aCoder encodeDouble:_listIdentifier forKey:kS_LawListListId];
    [aCoder encodeObject:_court forKey:kS_LawListListPublishDep];
    [aCoder encodeObject:_effectiveness forKey:kS_LawListListEffectiveness];
    [aCoder encodeObject:_title forKey:kS_LawListListTitle];
    [aCoder encodeObject:_createdAt forKey:kS_LawListListCreatedAt];
    [aCoder encodeObject:_titleHighLighter forKey:kS_LawListListTitleHighLighter];
    [aCoder encodeObject:_content forKey:kS_LawListListBody];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_LawListList *copy = [[S_LawListList alloc] init];
    
    if (copy) {

        copy.category = [self.category copyWithZone:zone];
        copy.pubDate = [self.pubDate copyWithZone:zone];
        copy.abstract = [self.abstract copyWithZone:zone];
        copy.listIdentifier = self.listIdentifier;
        copy.court = [self.court copyWithZone:zone];
        copy.effectiveness = [self.effectiveness copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.titleHighLighter = [self.titleHighLighter copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
    }
    
    return copy;
}


@end
