//
//  M_AnwersData.h
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface M_AnwersData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double userID;
@property (nonatomic, strong) NSString *headerID;
@property (nonatomic, assign) double responseCount;
@property (nonatomic, assign) double tuserID;
@property (nonatomic, strong) NSString *headerTitle;
@property (nonatomic, strong) NSString *answerID;
@property (nonatomic, strong) NSString *answerContent;
@property (nonatomic, assign) double collectCount;
@property (nonatomic, assign) double aUserID;
@property (nonatomic, assign) double commentCount;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
