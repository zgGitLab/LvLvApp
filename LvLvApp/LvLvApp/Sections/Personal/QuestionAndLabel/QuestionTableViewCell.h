//
//  QuestionTableViewCell.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *IndIssue;
@property (strong, nonatomic) IBOutlet UILabel *IndAttentionAnswer;
@property (strong, nonatomic) NSMutableArray *IdIssueArray;
@end
