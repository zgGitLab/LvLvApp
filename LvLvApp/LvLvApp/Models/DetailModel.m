//
//  DetailModel.m
//  Tools
//
//  Created by IOS8 on 15/9/15.
//  Copyright (c) 2015年 IOS8. All rights reserved.
//

#import "DetailModel.h"

@implementation DetailModel

// 计算天数，机利息
- (void)computeday:(NSString *)startday endDay:(NSString *)endday andLiLv:(NSString *)lilv
{
    self.debtTime = [NSString stringWithFormat:@"%@ 至 %@",startday,endday];
    
    startday = [NSString stringWithFormat:@"%@ %d:%d:%d",startday,00,00,00];
    endday = [NSString stringWithFormat:@"%@ %d:%d:%d",endday,23,59,59];
    NSDateFormatter *format=[[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSDate *fromdate=[format dateFromString:startday];
    NSTimeZone *fromzone = [NSTimeZone systemTimeZone];
    NSInteger frominterval = [fromzone secondsFromGMTForDate: fromdate];
    NSDate *fromDate = [fromdate  dateByAddingTimeInterval: frominterval];
    
    NSDate *todate=[format dateFromString:endday];
    NSTimeZone *tozone = [NSTimeZone systemTimeZone];
    NSInteger tointerval = [tozone secondsFromGMTForDate: todate];
    NSDate *toDate = [todate  dateByAddingTimeInterval: tointerval];
    
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSUInteger unitFlags = NSCalendarUnitDay | kCFCalendarUnitHour;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:fromDate toDate:toDate options:0];
    // double months = [components month];
    NSInteger days = [components day];
    NSLog(@"%ld",(long)days);
    NSInteger hours = [components hour];
    if (hours != 0) {
        days = days + 1;
    }
    double temp = [lilv doubleValue];
    self.dayCount = [NSString stringWithFormat:@"%ld",(long)days];
    self.liLi = days/360.00 * [lilv doubleValue]/100*[self.benJIn doubleValue];
    self.liLv = [NSString stringWithFormat:@"%.2f",temp];
}

@end
