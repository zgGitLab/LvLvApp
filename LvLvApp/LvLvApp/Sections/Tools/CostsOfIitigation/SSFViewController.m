//
//  SSFViewController.m
//  LvLvApp
//
//  Created by hope on 15/10/19.
//  Copyright (c) 2015年 gyy. All rights reserved.
//

#import "SSFViewController.h"
#import "LawCaseApply.h"
#import "Acceptance.h"
#import "ChooseView.h"
@interface SSFViewController ()<ChooseViewDelegate>
{
    UIScrollView *mainView;
    UIView *bgView;
    // anliApply *anliView;
    //anjianshouli *anjian;
    ChooseView *chooseView;
    UIView *headerView;//头部
    UIView *fangView; //jisuanview
    UILabel *shoulityepLa;//受理类型
    UITextField *shoulityepText;
    UIButton *shoulityepBut;
    UILabel *shurujineLa;//输入金额
    UITextField *shurujineText;//输入金额的
    UIButton *jisuanBut;//计算
    UIButton *chongzhiBut;//重置
    UILabel *jieGuo;//计算结果
    UILabel *xianshijieguo;
    UIButton *peosonBtn;
    UIView *seclectColorView;
    UIButton *otherBtn;
}
@end

@implementation SSFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    mainView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.view = mainView;
    mainView.showsVerticalScrollIndicator = NO;
    mainView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    self.navigationItem.title =@"诉讼计算";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setHeaderView];
    [self anjianApplyFor:peosonBtn];
}
- (void)setHeaderView
{
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width,70)];
    
    bgView.backgroundColor=[UIColor colorWithRed:229.0/255.0 green:230.0/255.0 blue:233.0/255.0 alpha:1];
    
    // bgView.backgroundColor=[UIColor redColor];
    [self.view addSubview:bgView];
    
    peosonBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    peosonBtn.frame = CGRectMake(20,5, bgView.bounds.size.width/2-20, 30);
    peosonBtn.tag = 1;
    [peosonBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    peosonBtn.titleLabel.font=[UIFont systemFontOfSize:16.0];
    //[peosonBtn setBackgroundColor:[UIColor redColor]];
    [peosonBtn setTitle:@"案例申请费" forState:UIControlStateNormal];
    [peosonBtn addTarget:self action:@selector(anjianApplyFor:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:peosonBtn];
    
    otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    otherBtn.frame = CGRectMake( bgView.bounds.size.width/2+10, 5,  bgView.bounds.size.width/2-20, 30);
    otherBtn.titleLabel.font=[UIFont systemFontOfSize:16.0];
    [otherBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    otherBtn.tag = 2;
    [otherBtn setTitle:@"案件受理费" forState:UIControlStateNormal];
    [otherBtn addTarget:self action:@selector(anjianshouli:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:otherBtn];
    
    seclectColorView = [[UIView alloc] initWithFrame:CGRectMake(peosonBtn.frame.origin.x+20,33 , peosonBtn.bounds.size.width-40,2)];
    seclectColorView.tag = 100;
    seclectColorView.backgroundColor = [UIColor colorWithRed:57.0/255.0 green:132.0/255.0 blue:236.0/255.0 alpha:1];
    [bgView addSubview:seclectColorView];
    
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [shurujineText resignFirstResponder];
}
- (void)anjianApplyFor:(UIButton *)but
{
    [_anliView removeFromSuperview];
    _anliView =[[LawCaseApply alloc]initwithView:mainView];
    [self.view addSubview:_anliView];
    [_anjian  removeFromSuperview];
    [UIView animateWithDuration:0.02 animations:^{
        seclectColorView.frame =CGRectMake(peosonBtn.frame.origin.x+20,33, peosonBtn.bounds.size.width-40,2);
    }];
    
}
- (void)anjianshouli:(UIButton *)buttn
{
    [_anjian  removeFromSuperview];
    _anjian =[[Acceptance alloc]initWithView:mainView];
    [self.view addSubview:_anjian];
    [_anliView removeFromSuperview];
    [UIView animateWithDuration:0.02 animations:^{
        seclectColorView.frame =CGRectMake(otherBtn.frame.origin.x+20, 33,otherBtn.bounds.size.width-40 , 2);
        
        
    }];
    
}

@end
