//
//  JobLIstModel.h
//  LvLvApp
//
//  Created by Sunny on 16/4/1.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JobLIstModel : NSObject

@property (nonatomic, strong) NSString *AddressLocation;        // 地址
@property (nonatomic, strong) NSString *CompanyName;            // 公司名
@property (nonatomic, strong) NSString *Contact;                 // 联系方式
@property (nonatomic, strong) NSString *CreateTime;         // 创建时间
@property (nonatomic, strong) NSString *Experience;         // 工作经验

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *ImageLogo;
@property (nonatomic, strong) NSString *IsDelete;

@property (nonatomic, strong) NSString *JobName;            // 工作名称
@property (nonatomic, strong) NSString *Requirement;
@property (nonatomic, strong) NSString *Salary;             // 年薪
@property (nonatomic, assign) int userid;
@end
