//
//  DetailViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/4.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController<UIScrollViewDelegate,UITextViewDelegate>


@property (strong, nonatomic) NSString *userID;
@property (assign, nonatomic) int iDProperty;
@property (assign, nonatomic) int type;

@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;


@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
// @"发布时间:",@"生效日期:",@"类别:",@"发文字号:",@"时效性:"],@[@"审理法院:",@"案件类型:",@"文件性质:",@"案由:",@"审理程序:"
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *label4;
@property (weak, nonatomic) IBOutlet UILabel *label5;
@property (weak, nonatomic) IBOutlet UIButton *collectionBtn;   // 收藏


@property (weak, nonatomic) IBOutlet UILabel *titleLable;       // 标题
@property (weak, nonatomic) IBOutlet UILabel *faYuan;           // 法院
@property (weak, nonatomic) IBOutlet UILabel *shenLiFaYuan;     // 审理法院
@property (weak, nonatomic) IBOutlet UILabel *eventStal;        // 案件类型
@property (weak, nonatomic) IBOutlet UILabel *bookProperty;     // 文件性质
@property (weak, nonatomic) IBOutlet UILabel *cause;            // 案由
@property (weak, nonatomic) IBOutlet UILabel *procedure;        // 审理程序
@property (weak, nonatomic) IBOutlet UITextView *contentView;

@property (strong, nonatomic) IBOutlet UIView *detailHeaderView;

@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UIView *bgView2;

@property (strong, nonatomic) NSMutableArray *IDArr;        // 法条或案例的ID
@property (strong, nonatomic) NSMutableArray *typeArr;       // 区分是法条还是案例
@property (nonatomic,strong) NSMutableArray *dataArray;//目录数据

@property (nonatomic,strong) NSArray *emStrArr;//关键词拆分

@property (nonatomic,strong) NSMutableArray *highlightedArray;//高亮关键字数组
@end
