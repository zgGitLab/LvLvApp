//
//  NetworkApi.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/5/10.
//  Copyright © 2016年 gyy. All rights reserved.
//

#ifndef NetworkApi_h
#define NetworkApi_h

/**登录注册*/
//登录
#define KlogUrl             @"http://%@/api/User/login"
//注册
#define KregiseterUrl       @"http://%@/api/User/registeruser"
//验证码
#define KmailUrl            @"http://%@/api/User/sendemail"
//忘记密码
#define KforgetUrl          @"http://%@/api/User/updateuserpassword"
//发送短信
#define KmobilePhoneUrl     @"http://%@/api/User/sendmessage"
/**广告页*/
#define KadUrl              @"http://%@/v1/advertisement"


/**话题*/
//动态列表请求接口
#define kDynamicUrl         @"http://%@/api/T_Topicinfo/getheaderdynamic"
//热门列表
#define kHotUrl             @"http://%@/api/T_Topicinfo/getissue_header"
//我的回答
#define KMyAnswer           @"http://%@/api/User/myanswer"
//根据回答iD获取评论列表
#define kCommentListUrl     @"http://%@/api/T_Topicinfo/getcommentlistbyanswerid"
//编辑问题
#define KEditQuestionUrl    @"http://%@/api/T_Topicinfo/updateheader"
//删除问题
#define kDeleteQuestion     @"http://%@/api/T_Topicinfo/deletehearder"
//编辑回答
#define  KEditAnswerUrl     @"http://%@/api/T_Topicinfo/updateanswer"
//删除回答
#define KDeleteAnswerUrl    @"http://%@/api/T_Topicinfo/deleteanswer"
//举报
#define KReportUrl          @"http://%@/api/User/addreport"
//关注
#define kConcernLabelUrl    @"http://%@/api/T_Topicinfo/attentiontag"
//点赞接口
#define kApproveUrl         @"http://%@/api/T_Topicinfo/updateanswerresponse"
//添加标签
#define kAddLabelUrl        @"http://%@/api/T_Topicinfo/settopictag"
//话题的详细信息接口
#define kAnswerUrl          @"http://%@/api/T_Topicinfo/getheadanswerdetails"
//某个问题对应的所有回答列表信息接口
#define kAllAnswerUrl       @"http://%@/api/T_Topicinfo/getheaderanswerall"
//问题标签
#define kAnswer_LabUrl      @"http://%@/api/T_Topicinfo/getheadertaglsit"
//关注话题接口
#define kConcernUrl         @"http://%@/api/T_Topicinfo/updateisFocus"
//对某一问题回答接口
#define kAnswerQuestion     @"http://%@/api/T_Topicinfo/saveanswer"
//草稿发布的
#define KDraftPublishUrl    @"http://%@/api/T_Topicinfo/publishdraft"
//保存草稿
#define kSaveDraft          @"http://%@/api/T_Topicinfo/createdraft"
//删除草稿
#define Kdeletemydraft      @"http://%@/api/User/deletemydraft"
//发布话题接口
#define kIssueUrl           @"http://%@/api/T_Topicinfo/savetopic"
//推荐标签
#define kRecommendUrl       @"http://%@/api/T_Topicinfo/getrecommendtag"
//获取模糊查询标签列表
#define kGettaginfo         @"http://%@/api/T_Topicinfo/gettaginfo"
//添加评论信息接口
#define kCommentUrl         @"http://%@/api/T_Topicinfo/savecomment"
//收藏接口
#define kCollectUrl         @"http://%@/api/T_Topicinfo/updateanswercollection"
//通过关键字模糊查询问题
#define KsearcrIssueUrl     @"http://%@/api/T_Topicinfo/gettopicbystr"
//关键字标签查询
#define KlabelParticularUrl @"http://%@/api/T_Topicinfo/getheaderlistbytagname"
//标签详情页
#define KseacrLableUrl      @"http://%@/api/T_Topicinfo/gettaginfo"
//用户关注标签
#define KattentiontgaUrl    @"http://%@/api/T_Topicinfo/attentiontag"
//关键用户查询
#define kseacrUesrUrl       @"http://%@/api/T_Topicinfo/getuserinfobyfuzzy"

/**个人模块*/
//帮助文档
#define KHelpDoc            @"http://%@/mobile/HelpDocument"
//我关注的问题
#define kattentionIssusUrls @"http://%@/api/User/myfocusquestion"
//我评论的
#define KcommuUrl           @"http://%@/api/User/mycomment"
//我收藏的回答
#define KcollectAnwerurl    @"http://%@/api/User/mycollectionanswer"
//我收藏的案例
#define Kcollecttionver     @"http://%@/api/User/mycollecttionver"
//删除我收藏的回答
#define KdeleteAnwerurl     @"http://%@/api/User/deletecollectionanswer"
//删除我的草稿
#define kdeleteDraftUrl     @"http://%@/api/T_Topicinfo/deleteanswer"
//删除评论
#define KdeleteCommet       @"http://%@/api/T_Topicinfo/deletecomment"
//删除我收藏案例
#define KdeleteartorverUrl  @"http://%@/api/User/deleteartorver"
//我的身份认证
#define kIgetauthentication @"http://%@/api/User/getauthentication"
//我的帮助文档
#define KhelpUrl            @"http://%@/api/User/gethelpdoc"
//我举报的信息
#define KmyReportUrl        @"http://%@/api/User/getreportlist"
//我的草稿
#define KmyDraft            @"http://%@/api/User/mydraft"
//我收藏的法条
#define Kcolltionart        @"http://%@/api/User/mycollecttionart"
#define KidCollerctFaTiaoUrl @"http://%@/api/User/mycollecttionart"
//我关注的问题
#define KIndIssueUrk        @"http://%@/api/User/myfocusquestion"
//我关注的标签
#define KIndLableUrk        @"http://%@/api/User/myfocustag"
//身份验证
#define KidentityUrk        @"http://%@/api/User/authentication"
//取得身份认证
#define KuserfocususerUrl   @"http://%@/api/User/getauthentication"
//修改密码
#define KChangePwd           @"http://%@/api/User/updateuserpassword"
//关键字模糊查询回答
#define KseacrAnwerUrl      @"http://%@/api/T_Topicinfo/getanwserbystr"
//个人主页url
#define KpersonageUrl       @"http://%@/api/User/getuserbyuserid"

/**检索模块*/
//法条
#define KCaseOfLaw          @"http://%@/api/v1/search/article"
#define KExampleOfLaw       @"http://%@/api/Verdicts/getverdictslist"
#define KCaseOfLawDetail    @"http://%@/api/Articles/getarticlesdetails"
#endif /* NetworkApi_h */
