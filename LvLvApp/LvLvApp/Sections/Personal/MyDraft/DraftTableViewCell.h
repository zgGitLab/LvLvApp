//
//  DraftTableViewCell.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DraftTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *IDDraftTitle;
@property (strong, nonatomic) IBOutlet UILabel *IDraftContent;
@end
