//
//  MyCityList.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/2.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PerareaPickViewDelegat <NSObject>

- (void)selectareaStr:(NSString *)areaStr;
@end
@interface MyCityList : UIView<UIPickerViewDelegate,UIPickerViewDataSource>

@property(nonatomic,assign)id<PerareaPickViewDelegat>delegate;
@property(nonatomic,strong)NSString *provinceStr;
@property(nonatomic,strong)NSString *cityStr;
@property(nonatomic,strong)NSMutableArray *dicKeyArray;
@property(nonatomic,strong)NSDictionary *levelTwoDic;
@property(nonatomic,strong)NSArray *plistArray;
@property(nonatomic,strong)UIView *backGroundView;
- (id)init;
-(void)showInView;
@end
