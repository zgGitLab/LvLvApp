//
//  S_MyAnserTableViewCell.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/11.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "S_MyAnserTableViewCell.h"

@interface S_MyAnserTableViewCell ()
@end

@implementation S_MyAnserTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        //1.添加子控件
        _image = [[UIImageView alloc] init];
        _image.hidden = YES;
//        _image.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:_image];
        
        _nicLab = [[UILabel alloc] init];
        _nicLab.numberOfLines = 1;
//        _nicLab.backgroundColor = [UIColor redColor];
        _nicLab.font = [UIFont systemFontOfSize:11];
        _nicLab.textColor = COLOR(73, 158, 242);
        //    nicLab.text = @"adsfsfsfsf";
        [self.contentView addSubview:_nicLab];
        
        _anserTimeLab = [[UILabel alloc] init];
        _anserTimeLab.font = [UIFont systemFontOfSize:11];
        _anserTimeLab.numberOfLines = 1;
//        _anserTimeLab.backgroundColor = [UIColor grayColor];
        //    anserTimeLab.text = @"afefwaegegre";
        _anserTimeLab.textColor = COLOR(171, 171, 171);
        [self.contentView addSubview:_anserTimeLab];
        
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:16];
        _titleLab.numberOfLines = 2;
//        _titleLab.backgroundColor = [UIColor yellowColor];
        _titleLab.textColor = COLOR(85, 85, 85);
        [self.contentView addSubview:_titleLab];
        
        _contentLab = [[UILabel alloc] init];
        _contentLab.numberOfLines = 2;
//        _contentLab.backgroundColor = [UIColor redColor];
        _contentLab.font = [UIFont systemFontOfSize:14];
        _contentLab.textColor = COLOR(122, 122, 122);
        [self.contentView addSubview:_contentLab];
        
        _otherLab = [[UILabel alloc] init];
        _otherLab.numberOfLines = 1;
//        _otherLab.backgroundColor = [UIColor yellowColor];
        _otherLab.font = [UIFont systemFontOfSize:11];
        _otherLab.textColor = COLOR(171, 171, 171);
        [self.contentView addSubview:_otherLab];

    }
    return self;
}

@end
