//
//  IssueTableViewCell.h
//  LvLvApp
//
//  Created by hope on 15/11/24.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IssueViewcellDelegate <NSObject>

-(void)doIssuecellAction:(UIButton *)btn;

@end
@interface IssueTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *AttentionCount;

@property (strong, nonatomic) IBOutlet UILabel *headline;

@property(nonatomic, weak) id<IssueViewcellDelegate> delegate;
@end
