//
//  FTiaoData.h
//
//  Created by hope  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface FTiaoData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *pubDate;
@property (nonatomic, assign) double docId;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *publishDep;
@property (nonatomic, strong) NSString *effectiveness;
@property (nonatomic, strong) NSString *body;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
