//
//  mhelpeData.m
//
//  Created by hope  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "mhelpeData.h"


NSString *const kmhelpeDataID = @"ID";
NSString *const kmhelpeDataUserID = @"UserID";
NSString *const kmhelpeDataCreateDT = @"CreateDT";
NSString *const kmhelpeDataDocText = @"DocText";


@interface mhelpeData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation mhelpeData

@synthesize iDProperty = _iDProperty;
@synthesize userID = _userID;
@synthesize createDT = _createDT;
@synthesize docText = _docText;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kmhelpeDataID fromDictionary:dict] doubleValue];
            self.userID = [[self objectOrNilForKey:kmhelpeDataUserID fromDictionary:dict] doubleValue];
            self.createDT = [self objectOrNilForKey:kmhelpeDataCreateDT fromDictionary:dict];
            self.docText = [self objectOrNilForKey:kmhelpeDataDocText fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kmhelpeDataID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kmhelpeDataUserID];
    [mutableDict setValue:self.createDT forKey:kmhelpeDataCreateDT];
    [mutableDict setValue:self.docText forKey:kmhelpeDataDocText];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kmhelpeDataID];
    self.userID = [aDecoder decodeDoubleForKey:kmhelpeDataUserID];
    self.createDT = [aDecoder decodeObjectForKey:kmhelpeDataCreateDT];
    self.docText = [aDecoder decodeObjectForKey:kmhelpeDataDocText];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kmhelpeDataID];
    [aCoder encodeDouble:_userID forKey:kmhelpeDataUserID];
    [aCoder encodeObject:_createDT forKey:kmhelpeDataCreateDT];
    [aCoder encodeObject:_docText forKey:kmhelpeDataDocText];
}

- (id)copyWithZone:(NSZone *)zone
{
    mhelpeData *copy = [[mhelpeData alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.userID = self.userID;
        copy.createDT = [self.createDT copyWithZone:zone];
        copy.docText = [self.docText copyWithZone:zone];
    }
    
    return copy;
}


@end
