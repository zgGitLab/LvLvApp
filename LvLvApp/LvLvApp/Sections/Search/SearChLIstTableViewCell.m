//
//  SearChLIstTableViewCell.m
//  LvLvApp
//
//  Created by Sunny on 16/1/15.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "SearChLIstTableViewCell.h"

@implementation SearChLIstTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.resultCount = [[UILabel alloc] init];
        self.resultCount.textColor = COLOR(156, 156, 156);
        self.resultCount.font = [UIFont systemFontOfSize:12];
        self.resultCount.numberOfLines = 1;
        self.resultCount.textAlignment = NSTextAlignmentLeft;
        
        self.title = [[UILabel alloc] init];
        self.title.textColor = COLOR(81, 88, 99);
        self.title.font = [UIFont systemFontOfSize:16];
        self.title.numberOfLines = 0;
        self.title.textAlignment = NSTextAlignmentLeft;
        
        self.court = [[UILabel alloc] init];
        self.court.textColor = COLOR(156, 156, 156);
        self.court.font = [UIFont systemFontOfSize:12];
        self.court.numberOfLines = 1;
        self.court.textAlignment = NSTextAlignmentLeft;
        
        self.content = [[UILabel alloc] init];
        self.content.textColor = COLOR(116, 116, 116);
        self.content.font = [UIFont systemFontOfSize:14];
        self.content.numberOfLines = 2;
        self.content.textAlignment = NSTextAlignmentLeft;
        
        [self addSubview:self.resultCount];
        [self addSubview:self.title];
        [self addSubview:self.court];
        [self addSubview:self.content];
        
    }
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
