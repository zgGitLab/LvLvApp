//
//  EmploymentViewController.m
//  LvLvApp
//
//  Created by Mark on 16/3/23.
//  Copyright © 2016年 gyy. All rights reserved.
//
#define UIscK  [[UIScreen mainScreen]bounds].size.width
#import "EmploymentViewController.h"
#import "JobdescriptionViewController.h"
#import "SearchTopicViewController.h"
#import "JobTableViewCell.h"
#import "PublishJobViewController.h"

@interface EmploymentViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *_dataArray;
    UITableView *_tableView;
    int page;
}
@end

@implementation EmploymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title =@"招聘信息";
    _dataArray = [NSMutableArray array];
    page = 1;
    [self headView];
    [self UITable];
    [self getAllJObInfoList:@"1"];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    UIBarButtonItem *leftItem =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"返回箭头"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    
    self.navigationItem.leftBarButtonItem=leftItem;
}
- (void)backClick
{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}


- (void)getAllJObInfoList:(NSString *)pageIndex
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/getJoblist",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:@"20" forKey:@"PageSize"];
    
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
         NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        
        if ([dic[@"Code"] isEqualToNumber:@200]) {
            
            if (page == 1) {
                [_dataArray removeAllObjects];
            }
            
            for (NSDictionary *dict in dic[@"Data"]) {
                
                JobLIstModel *jobList = [[JobLIstModel alloc] init];
                [jobList setValuesForKeysWithDictionary:dict];
                [_dataArray addObject:jobList];
            }
            [_tableView reloadData];
            
            
        }else{
            [MBProgressHUD showOlnyMessage:dic[@"Message"]];
        }
        
        NSLog(@"%@",dic);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
         NSLog(@"%@",error);
    }];
}


- (void)headView
{
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width,45)];
    [self.view addSubview:view];
    view.backgroundColor =[UIColor colorWithRed:236/255.0 green:236.0/255.0 blue:237.0/255.0 alpha:1];
    UISearchBar *SearchB =[[UISearchBar alloc]initWithFrame:CGRectMake(5, 0, [[UIScreen mainScreen]bounds].size.width-90, 45)];
    SearchB.searchBarStyle = UISearchBarStyleDefault;
    SearchB.backgroundImage =[UIImage imageNamed:@"灰色背景"];
    SearchB.barTintColor =[UIColor colorWithRed:236/255.0 green:236.0/255.0 blue:237.0/255.0 alpha:1];
    SearchB.placeholder =@"搜索职位/公司";
    [view addSubview:SearchB];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor =[UIColor clearColor];
    button.frame =CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width-80,45);
    [button addTarget:self action:@selector(pushEmployme:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    UILabel *PostJobLab =[[UILabel alloc]initWithFrame:CGRectMake( [[UIScreen mainScreen]bounds].size.width-85, 0,80, 45)];
    PostJobLab.text =@"发布职位";
    PostJobLab.userInteractionEnabled =YES;
//    PostJobLab.font = [UIFont fontWithName:@"Helvetica-Bold" size:17.0];
    PostJobLab.font = [UIFont systemFontOfSize:17.0];
    PostJobLab.textColor =[UIColor colorWithRed:59/255.0 green:133/255.0 blue:238/255.0 alpha:1.0];
    UITapGestureRecognizer *top =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(PostJobAction:)];
    [top setNumberOfTapsRequired:1];
    [PostJobLab addGestureRecognizer:top];
    
    [view addSubview:PostJobLab];
}

- (void)UITable
{
   
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 45, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-109) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 10)];
    _tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:_tableView];
    
    __weak EmploymentViewController *VC = self;
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [VC getAllJObInfoList:[NSString stringWithFormat:@"%d",page]];
    }];
    // 上拉加载
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 结束刷新
        page += 1;
        [VC getAllJObInfoList:[NSString stringWithFormat:@"%d",page]];
    }];



}
#pragma  mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  _dataArray.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *idEp =@"cellEP";
    JobTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:idEp];
    if (!cell) {
       cell = [[NSBundle mainBundle] loadNibNamed:@"JobTableViewCell" owner:self options:nil][0];
    }
    JobLIstModel *listMo = _dataArray[indexPath.row];
    cell.model = listMo;
    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   return 100;
}
- (void)PostJobAction:(UITapGestureRecognizer *)sender
{
    PublishJobViewController *publishVC =[[PublishJobViewController alloc]init];
    [self.navigationController pushViewController:publishVC animated:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    JobLIstModel *jobList = _dataArray[indexPath.row];
    JobdescriptionViewController *jobV =[[JobdescriptionViewController alloc]init];
    jobV.jobID = [NSString stringWithFormat:@"%@",jobList.ID];
    [self.navigationController pushViewController:jobV animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    

}

- (void)pushEmployme:(UIButton *)sender
{
    SearchTopicViewController *seacr= [[SearchTopicViewController alloc]init];
    seacr.searchType =@"招聘信息";
    [self.navigationController pushViewController:seacr animated:YES];


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
