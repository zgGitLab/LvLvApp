//
//  PerConcernViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "PerConcernViewController.h"
#import "FriendTableViewCell.h"
#import "OtherPerMainViewController.h"
#import "PerMainPageViewController.h"
@interface PerConcernViewController ()
{
    NSArray *dataArray;
}
@end

@implementation PerConcernViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关注";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
    UIView *footView = [[UIView alloc] init];
    self.concernTab.separatorInset = UIEdgeInsetsMake(0, 5, 0, 8);
    self.concernTab.tableFooterView = footView;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    __weak PerConcernViewController *VC = self;
    [RequestManager perConcernList:self.userID andBlock:^(NSArray *dataArr, NSString *message) {
        dataArray = dataArr;
        [VC performSelectorOnMainThread:@selector(refreshData) withObject:nil waitUntilDone:YES];
       
    }];
}

- (void)refreshData
{
    [self.concernTab reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"friendcell";
    FriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil][0];
        // 如果不是自己的关注列表则隐藏关注状态
        if (![self.userID isEqualToString:[self getUserId]]) {
            cell.privateMessBtn.hidden = YES;
        }
    }
    S_perConcernListData *listData = dataArray[indexPath.row];

    [cell.privateMessBtn setImage:[UIImage imageNamed:@"已关注1"] forState:UIControlStateNormal];
    cell.privateMessBtn.selected = YES;
    [cell.privateMessBtn addTarget:self action:@selector(concernBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.titleIcon sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,listData.image]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"头像"]];
    cell.titleIcon.layer.cornerRadius = 26.5;
    cell.titleIcon.clipsToBounds = YES;
    cell.privateMessBtn.tag = listData.iDProperty;
    cell.titleStr.text = listData.nickName;
    cell.conStr.text = listData.introduction;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     S_perConcernListData *listData = dataArray[indexPath.row];
    // 如果是自己就跳到个人主页
    if ([[self getUserId] isEqualToString:[NSString stringWithFormat:@"%.0f",listData.iDProperty]]) {
        PerMainPageViewController *perVC = [[PerMainPageViewController alloc] init];
        perVC.userID = [NSString stringWithFormat:@"%.f",listData.iDProperty];
        [self.navigationController pushViewController:perVC animated:YES];
    }else{
        OtherPerMainViewController *otherVC = [[OtherPerMainViewController alloc] init];
        otherVC.mySelfImage = self.mySelfImage;
        otherVC.userID = [self getUserId];
        otherVC.otherID = [NSString stringWithFormat:@"%.0f",listData.iDProperty];
        [self.navigationController pushViewController:otherVC animated:YES];
    }
}

- (void)concernBtnClick:(UIButton *)btn
{
    NSString *otherID = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    if (btn.selected) {
        
        [RequestManager perConcernOther:[self getUserId] andOtherID:otherID andIsConcern:@"0" andBlock:^( NSString *message) {
            if ([message isEqualToString:@"成功"]) {
                [btn setImage:[UIImage imageNamed:@"关注"] forState:UIControlStateNormal];
                btn.selected = NO;
            }
            
        }];
    }else{
        [RequestManager perConcernOther:[self getUserId] andOtherID:otherID andIsConcern:@"1" andBlock:^( NSString *message) {
            
            if ([message isEqualToString:@"成功"]) {
                [btn setImage:[UIImage imageNamed:@"已关注1"] forState:UIControlStateNormal];
                btn.selected = YES;
            }
            
        }];
        
    }
    
}
- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}
@end
