//
//  S_LawDetailData.m
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_LawDetailData.h"
#import "S_LawDetailDs.h"


NSString *const kS_LawDetailDataIsCollect = @"IsCollect";
NSString *const kS_LawDetailDataDs = @"ds";


@interface S_LawDetailData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_LawDetailData

@synthesize isCollect = _isCollect;
@synthesize ds = _ds;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.isCollect = [[self objectOrNilForKey:kS_LawDetailDataIsCollect fromDictionary:dict] doubleValue];
            self.ds = [S_LawDetailDs modelObjectWithDictionary:[dict objectForKey:kS_LawDetailDataDs]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isCollect] forKey:kS_LawDetailDataIsCollect];
    [mutableDict setValue:[self.ds dictionaryRepresentation] forKey:kS_LawDetailDataDs];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.isCollect = [aDecoder decodeDoubleForKey:kS_LawDetailDataIsCollect];
    self.ds = [aDecoder decodeObjectForKey:kS_LawDetailDataDs];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_isCollect forKey:kS_LawDetailDataIsCollect];
    [aCoder encodeObject:_ds forKey:kS_LawDetailDataDs];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_LawDetailData *copy = [[S_LawDetailData alloc] init];
    
    if (copy) {

        copy.isCollect = self.isCollect;
        copy.ds = [self.ds copyWithZone:zone];
    }
    
    return copy;
}


@end
