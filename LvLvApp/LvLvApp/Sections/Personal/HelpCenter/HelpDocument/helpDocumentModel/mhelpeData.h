//
//  mhelpeData.h
//
//  Created by hope  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface mhelpeData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double iDProperty;
@property (nonatomic, assign) double userID;
@property (nonatomic, strong) NSString *createDT;
@property (nonatomic, strong) NSString *docText;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
