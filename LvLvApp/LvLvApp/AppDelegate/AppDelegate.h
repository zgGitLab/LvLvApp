//
//  AppDelegate.h
//  LawApp
//
//  Created by yfzx_sh_gaoyy on 15/10/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseCenter.h"
#import "CacheDataBase.h"
#import "CustomTabBarController.h"
#import "MBProgressHUD.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) DataBaseCenter *db;       // 银行，社保，公积金
@property (strong, nonatomic) CacheDataBase *cacheDb;   // 其他数据缓存
@property (strong, nonatomic) NSString *version;
@property (strong, nonatomic) CustomTabBarController  *customTabBar;
@property (assign, nonatomic) BOOL isShowBtn;

@property (strong, nonatomic) NSNumber *networkReachability;//切换网络后的状态值 1:蜂窝移动 2:wifi 3:未知网络 0:无网络

@property (strong, nonatomic) NSNumber *originalNetworkReachability;//原始的网络状态
- (void)getAlldata;
@end

