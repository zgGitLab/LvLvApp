//
//  CommentNumViewController.h
//  LvLvApp
//
//  Created by hope on 15/11/30.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentListModel.h"


@interface CommentNumViewController : BaseViewController
@property (nonatomic,copy) NSString *answerID;
@property (nonatomic,copy) NSString *userID;
@property (nonatomic,copy) NSString * TID;//问题编号

@end
