//
//  S_CaseDetailSelectVerdicts.h
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_CaseDetailSelectVerdicts : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, assign) double selectVerdictsIdentifier;
@property (nonatomic, strong) NSString *character;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *court;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
