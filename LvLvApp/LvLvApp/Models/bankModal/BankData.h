//
//  BankData.h
//
//  Created by IOS8  on 15/10/27
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface BankData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *beginDT;
@property (nonatomic, strong) NSString *oneYear;
@property (nonatomic, strong) NSString *fiveYear;
@property (nonatomic, strong) NSString *halfYear;
@property (nonatomic, strong) NSString *endDT;
@property (nonatomic, strong) NSString *aboveFiveYear;
@property (nonatomic, strong) NSString *iDProperty;
@property (nonatomic, strong) NSString *threeYear;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
