//
//  M_Profession.h
//  LvLvApp
//
//  Created by hope on 15/12/16.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PerareaPickViewDelegat <NSObject>
- (void)selectareaStr:(NSString *)areaStr;
@end

@interface M_Profession : UIView<UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, strong)NSArray *IDProfession;
@property(nonatomic,strong)id<PerareaPickViewDelegat>delegate;
@property(nonatomic,strong)NSString *provinceStr;
@property(nonatomic,strong)NSString *professionStr;
@property(nonatomic,strong)NSMutableArray *dicKeyArray;
@property(nonatomic,strong)NSDictionary *levelTwoDic;
@property(nonatomic,strong)NSArray *plistArray;
@property(nonatomic,strong)UIView *backGroundView;
- (id)init;
-(void)showInView;
@end
