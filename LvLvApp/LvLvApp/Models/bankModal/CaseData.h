//
//  CaseData.h
//
//  Created by IOS8  on 15/11/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CaseData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *selectVerdicts;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
