//
//  S_PerBaseInfoEductList.h
//
//  Created by IOS8  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_PerBaseInfoEductList : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *iDProperty;
@property (nonatomic, assign) double userID;
@property (nonatomic, strong) NSString *university;
@property (nonatomic, strong) NSString *institute;
@property (nonatomic, strong) NSString *admissionDate;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
