//
//  IWhistleBlowingViewController.m
//  LvLvApp
//
//  Created by hope on 15/12/22.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "IWhistleBlowingViewController.h"
#import "IWhistleBTableViewCell.h"

#import "M_reportDataModels.h"
#import "M_reportData.h"
#import "M_reportDataModels.h"

#import "M_IDGainReportBaseClass.h"
#import "M_IDGainReportData.h"
#import "M_IDGainReportDataModels.h"
@interface IWhistleBlowingViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation IWhistleBlowingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.IWhistArray =[[NSMutableArray alloc]init];
    self.navigationItem.title =@"我的举报";
    [self TableViews];
    [self myReportRequest];
}

- (void)TableViews
{
    self.IWhistleBlowingtableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-64)style:UITableViewStylePlain];
    self.IWhistleBlowingtableView.backgroundColor =[UIColor whiteColor];
    [self.IWhistleBlowingtableView setSeparatorInset:UIEdgeInsetsMake(0, 8, 0, 8)];
    self.IWhistleBlowingtableView.delegate = self;
    self.IWhistleBlowingtableView.dataSource = self;
    [self.view addSubview:self.IWhistleBlowingtableView];
    [self setExtraCellLineHidden:self.IWhistleBlowingtableView];
}

-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [UIView new];
    
    view.backgroundColor = [UIColor clearColor];
    
    [tableView setTableFooterView:view];
}
- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    [self.IWhistleBlowingtableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.IWhistArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier =@"IwHID";
    IWhistleBTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell ==nil) {
        cell =[[NSBundle mainBundle]loadNibNamed:@"IWhistleBTableViewCell" owner:self options:nil][0];
    }
    
    if (self.IWhistArray.count >indexPath.row) {
        M_IDGainReportData *mdatas =self.IWhistArray[indexPath.row];
        NSString *categorystr =[NSString stringWithFormat:@"%@",mdatas.category];
        cell.reportTime.text =mdatas.reportDate;
        cell.reportIsson.text =categorystr;
        cell.reportContent.text =mdatas.title;
        cell.reportGarbage.text =mdatas.remarkContent;
        cell.reportInHand.text =mdatas.disposeResult;
    }
    
    return cell;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem*butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    ;
    butItem.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
}
- (void)back{
    //[self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160;
}
- (void)myReportRequest
{
    NSInteger pageIndex =1;
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*userID = [defaults objectForKey:@"users"];

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:userID forKey:@"UserID"];
    [params setValue:[NSNumber numberWithInteger:pageIndex] forKey:@"PageIndex"];
    [params setValue:[NSNumber numberWithInteger:K_PAGESIZE] forKey:@"PageSize"];
    [LVHTTPRequestTool post:[NSString stringWithFormat:KmyReportUrl,K_IP] params:params success:^(id json) {
        NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        if ([dict[@"Code"] isEqualToNumber:@200]) {
            M_IDGainReportBaseClass *mdata =[[M_IDGainReportBaseClass alloc]initWithDictionary:dict];
            if (mdata.data.count == 0) {
                [self creatBlankView];
            }else{
                [self.IWhistArray addObjectsFromArray:mdata.data];
                [self.IWhistleBlowingtableView  reloadData];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)creatBlankView{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, kWidth, 20)];
    label.center = self.view.center;
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"暂无举报信息!";
    label.textColor = KGRAYCOLOR;
    label.font = KFONT16;
    [self.view addSubview:label];
}


@end
