//
//  M_CommData.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_CommData.h"


NSString *const kM_CommDataTitle = @"Title";
NSString *const kM_CommDataUserid = @"Userid";
NSString *const kM_CommDataNotifyID = @"NotifyID";
NSString *const kM_CommDataDescription = @"Description";
NSString *const kM_CommDataAUserID = @"AUserID";
NSString *const kM_CommDataCommentCount = @"commentCount";
NSString *const kM_CommDataTimagesUrl = @"TimagesUrl";
NSString *const kM_CommDataLogoUrl = @"LogoUrl";
NSString *const kM_CommDataQuestionContent = @"QuestionContent";
NSString *const kM_CommDataAnswerContent = @"AnswerContent";
NSString *const kM_CommDataCollectCount = @"collectCount";
NSString *const kM_CommDataSortTime = @"sortTime";
NSString *const kM_CommDataTimes = @"times";
NSString *const kM_CommDataNickname = @"Nickname";
NSString *const kM_CommDataTypes = @"types";
NSString *const kM_CommDataCounts = @"counts";
NSString *const kM_CommDataTUserID = @"TUserID";
NSString *const kM_CommDataTID = @"TID";
NSString *const kM_CommDataAnswerID = @"Answer_ID";
NSString *const kM_CommDataCommentID = @"CommentID";


@interface M_CommData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_CommData

@synthesize title = _title;
@synthesize userid = _userid;
@synthesize notifyID = _notifyID;
@synthesize dataDescription = _dataDescription;
@synthesize aUserID = _aUserID;
@synthesize commentCount = _commentCount;
@synthesize timagesUrl = _timagesUrl;
@synthesize logoUrl = _logoUrl;
@synthesize questionContent = _questionContent;
@synthesize answerContent = _answerContent;
@synthesize collectCount = _collectCount;
@synthesize sortTime = _sortTime;
@synthesize times = _times;
@synthesize nickname = _nickname;
@synthesize types = _types;
@synthesize counts = _counts;
@synthesize tUserID = _tUserID;
@synthesize tID = _tID;
@synthesize answerID = _answerID;
@synthesize commentID = _commentID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.title = [self objectOrNilForKey:kM_CommDataTitle fromDictionary:dict];
            self.userid = [[self objectOrNilForKey:kM_CommDataUserid fromDictionary:dict] doubleValue];
            self.notifyID = [[self objectOrNilForKey:kM_CommDataNotifyID fromDictionary:dict] doubleValue];
            self.dataDescription = [self objectOrNilForKey:kM_CommDataDescription fromDictionary:dict];
            self.aUserID = [[self objectOrNilForKey:kM_CommDataAUserID fromDictionary:dict] doubleValue];
            self.commentCount = [[self objectOrNilForKey:kM_CommDataCommentCount fromDictionary:dict] doubleValue];
            self.timagesUrl = [self objectOrNilForKey:kM_CommDataTimagesUrl fromDictionary:dict];
            self.logoUrl = [self objectOrNilForKey:kM_CommDataLogoUrl fromDictionary:dict];
            self.questionContent = [self objectOrNilForKey:kM_CommDataQuestionContent fromDictionary:dict];
            self.answerContent = [self objectOrNilForKey:kM_CommDataAnswerContent fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kM_CommDataCollectCount fromDictionary:dict] doubleValue];
            self.sortTime = [self objectOrNilForKey:kM_CommDataSortTime fromDictionary:dict];
            self.times = [self objectOrNilForKey:kM_CommDataTimes fromDictionary:dict];
            self.nickname = [self objectOrNilForKey:kM_CommDataNickname fromDictionary:dict];
            self.types = [[self objectOrNilForKey:kM_CommDataTypes fromDictionary:dict] doubleValue];
            self.counts = [[self objectOrNilForKey:kM_CommDataCounts fromDictionary:dict] doubleValue];
            self.tUserID = [[self objectOrNilForKey:kM_CommDataTUserID fromDictionary:dict] doubleValue];
            self.tID = [self objectOrNilForKey:kM_CommDataTID fromDictionary:dict];
            self.answerID = [self objectOrNilForKey:kM_CommDataAnswerID fromDictionary:dict];
            self.commentID = [self objectOrNilForKey:kM_CommDataCommentID fromDictionary:dict];

    }
    
    return self;
    
}

//- (NSDictionary *)dictionaryRepresentation
//{
//    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
//    [mutableDict setValue:self.title forKey:kM_CommDataTitle];
//    [mutableDict setValue:[NSNumber numberWithDouble:self.userid] forKey:kM_CommDataUserid];
//    [mutableDict setValue:[NSNumber numberWithDouble:self.notifyID] forKey:kM_CommDataNotifyID];
//    [mutableDict setValue:self.dataDescription forKey:kM_CommDataDescription];
//    [mutableDict setValue:[NSNumber numberWithDouble:self.aUserID] forKey:kM_CommDataAUserID];
//    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kM_CommDataCommentCount];
//    [mutableDict setValue:self.timagesUrl forKey:kM_CommDataTimagesUrl];
//    [mutableDict setValue:self.logoUrl forKey:kM_CommDataLogoUrl];
//    [mutableDict setValue:self.questionContent forKey:kM_CommDataQuestionContent];
//    [mutableDict setValue:self.answerContent forKey:kM_CommDataAnswerContent];
//    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kM_CommDataCollectCount];
//    [mutableDict setValue:self.sortTime forKey:kM_CommDataSortTime];
//    [mutableDict setValue:self.times forKey:kM_CommDataTimes];
//    [mutableDict setValue:self.nickname forKey:kM_CommDataNickname];
//    [mutableDict setValue:[NSNumber numberWithDouble:self.types] forKey:kM_CommDataTypes];
//    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kM_CommDataCounts];
//    [mutableDict setValue:[NSNumber numberWithDouble:self.tUserID] forKey:kM_CommDataTUserID];
//    [mutableDict setValue:self.tID forKey:kM_CommDataTID];
//    [mutableDict setValue:self.answerID forKey:kM_CommDataAnswerID];
//
//    return [NSDictionary dictionaryWithDictionary:mutableDict];
//}
//
//- (NSString *)description 
//{
//    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
//}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

//
//#pragma mark - NSCoding Methods
//
//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    self = [super init];
//
//    self.title = [aDecoder decodeObjectForKey:kM_CommDataTitle];
//    self.userid = [aDecoder decodeDoubleForKey:kM_CommDataUserid];
//    self.notifyID = [aDecoder decodeDoubleForKey:kM_CommDataNotifyID];
//    self.dataDescription = [aDecoder decodeObjectForKey:kM_CommDataDescription];
//    self.aUserID = [aDecoder decodeDoubleForKey:kM_CommDataAUserID];
//    self.commentCount = [aDecoder decodeDoubleForKey:kM_CommDataCommentCount];
//    self.timagesUrl = [aDecoder decodeObjectForKey:kM_CommDataTimagesUrl];
//    self.logoUrl = [aDecoder decodeObjectForKey:kM_CommDataLogoUrl];
//    self.questionContent = [aDecoder decodeObjectForKey:kM_CommDataQuestionContent];
//    self.answerContent = [aDecoder decodeObjectForKey:kM_CommDataAnswerContent];
//    self.collectCount = [aDecoder decodeDoubleForKey:kM_CommDataCollectCount];
//    self.sortTime = [aDecoder decodeObjectForKey:kM_CommDataSortTime];
//    self.times = [aDecoder decodeObjectForKey:kM_CommDataTimes];
//    self.nickname = [aDecoder decodeObjectForKey:kM_CommDataNickname];
//    self.types = [aDecoder decodeDoubleForKey:kM_CommDataTypes];
//    self.counts = [aDecoder decodeDoubleForKey:kM_CommDataCounts];
//    self.tUserID = [aDecoder decodeDoubleForKey:kM_CommDataTUserID];
//    self.tID = [aDecoder decodeObjectForKey:kM_CommDataTID];
//    self.answerID = [aDecoder decodeObjectForKey:kM_CommDataAnswerID];
//    return self;
//}
//
//- (void)encodeWithCoder:(NSCoder *)aCoder
//{
//
//    [aCoder encodeObject:_title forKey:kM_CommDataTitle];
//    [aCoder encodeDouble:_userid forKey:kM_CommDataUserid];
//    [aCoder encodeDouble:_notifyID forKey:kM_CommDataNotifyID];
//    [aCoder encodeObject:_dataDescription forKey:kM_CommDataDescription];
//    [aCoder encodeDouble:_aUserID forKey:kM_CommDataAUserID];
//    [aCoder encodeDouble:_commentCount forKey:kM_CommDataCommentCount];
//    [aCoder encodeObject:_timagesUrl forKey:kM_CommDataTimagesUrl];
//    [aCoder encodeObject:_logoUrl forKey:kM_CommDataLogoUrl];
//    [aCoder encodeObject:_questionContent forKey:kM_CommDataQuestionContent];
//    [aCoder encodeObject:_answerContent forKey:kM_CommDataAnswerContent];
//    [aCoder encodeDouble:_collectCount forKey:kM_CommDataCollectCount];
//    [aCoder encodeObject:_sortTime forKey:kM_CommDataSortTime];
//    [aCoder encodeObject:_times forKey:kM_CommDataTimes];
//    [aCoder encodeObject:_nickname forKey:kM_CommDataNickname];
//    [aCoder encodeDouble:_types forKey:kM_CommDataTypes];
//    [aCoder encodeDouble:_counts forKey:kM_CommDataCounts];
//    [aCoder encodeDouble:_tUserID forKey:kM_CommDataTUserID];
//    [aCoder encodeObject:_tID forKey:kM_CommDataTID];
//    [aCoder encodeObject:_answerID forKey:kM_CommDataAnswerID];
//}
//
//- (id)copyWithZone:(NSZone *)zone
//{
//    M_CommData *copy = [[M_CommData alloc] init];
//    
//    if (copy) {
//
//        copy.title = [self.title copyWithZone:zone];
//        copy.userid = self.userid;
//        copy.notifyID = self.notifyID;
//        copy.dataDescription = [self.dataDescription copyWithZone:zone];
//        copy.aUserID = self.aUserID;
//        copy.commentCount = self.commentCount;
//        copy.timagesUrl = [self.timagesUrl copyWithZone:zone];
//        copy.logoUrl = [self.logoUrl copyWithZone:zone];
//        copy.questionContent = [self.questionContent copyWithZone:zone];
//        copy.answerContent = [self.answerContent copyWithZone:zone];
//        copy.collectCount = self.collectCount;
//        copy.sortTime = [self.sortTime copyWithZone:zone];
//        copy.times = [self.times copyWithZone:zone];
//        copy.nickname = [self.nickname copyWithZone:zone];
//        copy.types = self.types;
//        copy.counts = self.counts;
//        copy.tUserID = self.tUserID;
//        copy.tID = [self.tID copyWithZone:zone];
//        copy.answerID = [self.answerID copyWithZone:zone];
//    }
//    
//    return copy;
//}


@end
