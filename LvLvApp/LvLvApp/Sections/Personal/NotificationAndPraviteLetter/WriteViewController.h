//
//  WriteViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/24.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WriteViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *frindListTab;
@property (nonatomic, strong) NSString *userID;
@property (strong, nonatomic) NSString *mySelfImage;
@end
