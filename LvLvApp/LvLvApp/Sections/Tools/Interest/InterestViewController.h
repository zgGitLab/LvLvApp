//
//  InterestViewController.h
//  Tools
//
//  Created by IOS8 on 15/9/11.
//  Copyright (c) 2015年 IOS8. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterestViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *myTable;

@property (strong, nonatomic) UITextField *benJInText;  // 本金
@property (strong, nonatomic) UIButton *startDateBtn;   // 开始日期
@property (strong, nonatomic) UIButton *endDateBtn;     // 结束日期
@property (strong, nonatomic) UIButton *liLvBtn;        // 利率
@property (strong, nonatomic) UIButton *jiSuanBtn;      // 计算
@property (strong, nonatomic) UIButton *chongZhiBtn;    // 重置
@property (strong, nonatomic) UIButton *chaXunBtn;      // 查询
@property (strong, nonatomic) UILabel *liXiLable;       // 利息
@property (strong, nonatomic) UILabel *countLable;      // 本利和
@end
