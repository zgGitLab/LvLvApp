//
//  FristData.h
//
//  Created by IOS8  on 15/11/3
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface FristData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *courtOrDepName;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double typeName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
