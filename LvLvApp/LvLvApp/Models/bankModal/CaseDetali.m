//
//  CaseDetali.m
//
//  Created by IOS8  on 15/11/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "CaseDetali.h"
#import "CaseData.h"


NSString *const kCaseDetaliCode = @"Code";
NSString *const kCaseDetaliMessage = @"Message";
NSString *const kCaseDetaliData = @"Data";


@interface CaseDetali ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CaseDetali

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kCaseDetaliCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kCaseDetaliMessage fromDictionary:dict];
            self.data = [CaseData modelObjectWithDictionary:[dict objectForKey:kCaseDetaliData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kCaseDetaliCode];
    [mutableDict setValue:self.message forKey:kCaseDetaliMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kCaseDetaliData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kCaseDetaliCode];
    self.message = [aDecoder decodeObjectForKey:kCaseDetaliMessage];
    self.data = [aDecoder decodeObjectForKey:kCaseDetaliData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kCaseDetaliCode];
    [aCoder encodeObject:_message forKey:kCaseDetaliMessage];
    [aCoder encodeObject:_data forKey:kCaseDetaliData];
}

- (id)copyWithZone:(NSZone *)zone
{
    CaseDetali *copy = [[CaseDetali alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
