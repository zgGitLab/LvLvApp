//
//  S_perConcernListBaseClass.m
//
//  Created by IOS8  on 15/12/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_perConcernListBaseClass.h"
#import "S_perConcernListData.h"


NSString *const kS_perConcernListBaseClassCode = @"Code";
NSString *const kS_perConcernListBaseClassMessage = @"Message";
NSString *const kS_perConcernListBaseClassData = @"Data";


@interface S_perConcernListBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_perConcernListBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_perConcernListBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_perConcernListBaseClassMessage fromDictionary:dict];
    NSObject *receivedS_perConcernListData = [dict objectForKey:kS_perConcernListBaseClassData];
    NSMutableArray *parsedS_perConcernListData = [NSMutableArray array];
    if ([receivedS_perConcernListData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_perConcernListData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_perConcernListData addObject:[S_perConcernListData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_perConcernListData isKindOfClass:[NSDictionary class]]) {
       [parsedS_perConcernListData addObject:[S_perConcernListData modelObjectWithDictionary:(NSDictionary *)receivedS_perConcernListData]];
    }

    self.data = [NSArray arrayWithArray:parsedS_perConcernListData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_perConcernListBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_perConcernListBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kS_perConcernListBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_perConcernListBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_perConcernListBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_perConcernListBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_perConcernListBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_perConcernListBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_perConcernListBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_perConcernListBaseClass *copy = [[S_perConcernListBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
