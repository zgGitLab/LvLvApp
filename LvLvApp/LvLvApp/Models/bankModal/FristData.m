//
//  FristData.m
//
//  Created by IOS8  on 15/11/3
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "FristData.h"


NSString *const kFristDataID = @"ID";
NSString *const kFristDataCourtOrDepName = @"CourtOrDepName";
NSString *const kFristDataContent = @"content";
NSString *const kFristDataTitle = @"Title";
NSString *const kFristDataTypeName = @"TypeName";


@interface FristData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FristData

@synthesize iDProperty = _iDProperty;
@synthesize courtOrDepName = _courtOrDepName;
@synthesize content = _content;
@synthesize title = _title;
@synthesize typeName = _typeName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kFristDataID fromDictionary:dict] doubleValue];
            self.courtOrDepName = [self objectOrNilForKey:kFristDataCourtOrDepName fromDictionary:dict];
            self.content = [self objectOrNilForKey:kFristDataContent fromDictionary:dict];
            self.title = [self objectOrNilForKey:kFristDataTitle fromDictionary:dict];
            self.typeName = [[self objectOrNilForKey:kFristDataTypeName fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kFristDataID];
    [mutableDict setValue:self.courtOrDepName forKey:kFristDataCourtOrDepName];
    [mutableDict setValue:self.content forKey:kFristDataContent];
    [mutableDict setValue:self.title forKey:kFristDataTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.typeName] forKey:kFristDataTypeName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kFristDataID];
    self.courtOrDepName = [aDecoder decodeObjectForKey:kFristDataCourtOrDepName];
    self.content = [aDecoder decodeObjectForKey:kFristDataContent];
    self.title = [aDecoder decodeObjectForKey:kFristDataTitle];
    self.typeName = [aDecoder decodeDoubleForKey:kFristDataTypeName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kFristDataID];
    [aCoder encodeObject:_courtOrDepName forKey:kFristDataCourtOrDepName];
    [aCoder encodeObject:_content forKey:kFristDataContent];
    [aCoder encodeObject:_title forKey:kFristDataTitle];
    [aCoder encodeDouble:_typeName forKey:kFristDataTypeName];
}

- (id)copyWithZone:(NSZone *)zone
{
    FristData *copy = [[FristData alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.courtOrDepName = [self.courtOrDepName copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.typeName = self.typeName;
    }
    
    return copy;
}


@end
