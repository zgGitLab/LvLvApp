//
//  M_messagePushSetViewController.h
//  LvLvApp
//
//  Created by hope on 15/12/21.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface M_messagePushSetViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UISwitch *swith1;
@property (weak, nonatomic) IBOutlet UISwitch *swith2;
@property (weak, nonatomic) IBOutlet UISwitch *swith3;
@property (weak, nonatomic) IBOutlet UISwitch *swith4;
@property (weak, nonatomic) IBOutlet UISwitch *swith5;


@end
