//
//  JobdescriptionViewController.h
//  LvLvApp
//
//  Created by Mark on 16/3/24.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobdescriptionViewController : BaseViewController

@property (nonatomic, strong) NSString *jobID;
@property (nonatomic, strong) UIScrollView *scrollView;

@end
