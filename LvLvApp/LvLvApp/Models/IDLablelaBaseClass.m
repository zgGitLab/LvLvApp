//
//  IDLablelaBaseClass.m
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IDLablelaBaseClass.h"
#import "IDLablelaData.h"


NSString *const kIDLablelaBaseClassCode = @"Code";
NSString *const kIDLablelaBaseClassMessage = @"Message";
NSString *const kIDLablelaBaseClassData = @"Data";


@interface IDLablelaBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IDLablelaBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kIDLablelaBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kIDLablelaBaseClassMessage fromDictionary:dict];
    NSObject *receivedIDLablelaData = [dict objectForKey:kIDLablelaBaseClassData];
    NSMutableArray *parsedIDLablelaData = [NSMutableArray array];
    if ([receivedIDLablelaData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedIDLablelaData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedIDLablelaData addObject:[IDLablelaData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedIDLablelaData isKindOfClass:[NSDictionary class]]) {
       [parsedIDLablelaData addObject:[IDLablelaData modelObjectWithDictionary:(NSDictionary *)receivedIDLablelaData]];
    }

    self.data = [NSArray arrayWithArray:parsedIDLablelaData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kIDLablelaBaseClassCode];
    [mutableDict setValue:self.message forKey:kIDLablelaBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kIDLablelaBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kIDLablelaBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kIDLablelaBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kIDLablelaBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kIDLablelaBaseClassCode];
    [aCoder encodeObject:_message forKey:kIDLablelaBaseClassMessage];
    [aCoder encodeObject:_data forKey:kIDLablelaBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    IDLablelaBaseClass *copy = [[IDLablelaBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
