//
//  JbDetailModel.h
//  LvLvApp
//
//  Created by Sunny on 16/4/5.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JbDetailModel : NSObject

@property (nonatomic,strong) NSString *AddressLocation; // 地点
@property (nonatomic,strong) NSString *CompanyName; // 公司名称
@property (nonatomic,strong) NSString *Contact; // 联系方式
@property (nonatomic,strong) NSString *CreateTime; // 创建时间
@property (nonatomic,strong) NSString *Experience; // 工作经验
@property (nonatomic,strong) NSString *ID; // 地点
@property (nonatomic,strong) NSString *ImageLogo; // 头像
@property (nonatomic,strong) NSString *IsDelete; // 是否删除
@property (nonatomic,strong) NSString *JobName; // 工作名称
@property (nonatomic,strong) NSString *Requirement; // 职位要求
@property (nonatomic,strong) NSString *Salary; // 薪资范围
@property (nonatomic,strong) NSString *userid; // 地点

@end
