//
//  SearchJobViewController.m
//  LvLvApp
//
//  Created by Sunny on 16/3/30.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "SearchJobViewController.h"
#import "JobTableViewCell.h"
#import "JobdescriptionViewController.h"
#import "EmploymentViewController.h"
@interface SearchJobViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView * _searchTableView;
    int page;
    NSMutableArray *_dataArray;
}
@end

@implementation SearchJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"招聘信息";
    _dataArray = [NSMutableArray array];
    page = 1;
    //    self.view.backgroundColor = [UIColor whiteColor];
    [self headView];
    [self UITable];
    [self searchJobList:@"1" andWit:self.str];

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    UIBarButtonItem *leftItem =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"返回箭头"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    
    self.navigationItem.leftBarButtonItem=leftItem;
}
- (void)backClick
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)headView
{
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width,45)];
    [self.view addSubview:view];
    view.backgroundColor =[UIColor colorWithRed:236/255.0 green:236.0/255.0 blue:237.0/255.0 alpha:1];
    UISearchBar *SearchB =[[UISearchBar alloc]initWithFrame:CGRectMake(5, 0, [[UIScreen mainScreen]bounds].size.width-60, 45)];
    SearchB.searchBarStyle = UISearchBarStyleDefault;
    SearchB.backgroundImage =[UIImage imageNamed:@"灰色背景"];
    SearchB.barTintColor =[UIColor colorWithRed:236/255.0 green:236.0/255.0 blue:237.0/255.0 alpha:1];
    SearchB.placeholder =@"搜索职位/公司";
    [view addSubview:SearchB];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor =[UIColor clearColor];
    button.frame =CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width-60,45);
    [button addTarget:self action:@selector(pushEmployme:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    UILabel *PostJobLab =[[UILabel alloc]initWithFrame:CGRectMake( CGRectGetMaxX(SearchB.frame)+5, 0,50, 45)];
    PostJobLab.text =@"取消";
    PostJobLab.userInteractionEnabled =YES;
    //    PostJobLab.font = [UIFont fontWithName:@"Helvetica-Bold" size:17.0];
    PostJobLab.font = [UIFont systemFontOfSize:17.0];
    PostJobLab.textColor =[UIColor colorWithRed:59/255.0 green:133/255.0 blue:238/255.0 alpha:1.0];
    UITapGestureRecognizer *top =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(PostJobAction:)];
    [top setNumberOfTapsRequired:1];
    [PostJobLab addGestureRecognizer:top];
    
    [view addSubview:PostJobLab];
}

- (void)UITable
{
    
    _searchTableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 45, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-109) style:UITableViewStylePlain];
    _searchTableView.delegate = self;
    _searchTableView.dataSource = self;
    _searchTableView.tableFooterView = [[UIView alloc] init];
    [_searchTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 10)];
    [self.view addSubview:_searchTableView];
    
    __weak SearchJobViewController *VC = self;
    _searchTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [VC searchJobList:[NSString stringWithFormat:@"%d",page] andWit:self.str];
    }];
    // 上拉加载
    _searchTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 结束刷新
        page += 1;
        [VC searchJobList:[NSString stringWithFormat:@"%d",page] andWit:self.str];
    }];
}

- (void)searchJobList:(NSString *)pageIndex andWit:(NSString *)searchStr
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/getJoblist",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:@"20" forKey:@"PageSize"];
    [parameters setObject:searchStr forKey:@"CompanyName"];
    
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        [_searchTableView.mj_header endRefreshing];
        [_searchTableView.mj_footer endRefreshing];
        
        if ([dic[@"Code"] isEqualToNumber:@200]) {
            
            if (page == 1) {
                [_dataArray removeAllObjects];
            }
            
            for (NSDictionary *dict in dic[@"Data"]) {
                
                JobLIstModel *jobList = [[JobLIstModel alloc] init];
                [jobList setValuesForKeysWithDictionary:dict];
                [_dataArray addObject:jobList];
            }
            [_searchTableView reloadData];
            
            
        }else{
            [MBProgressHUD showOlnyMessage:dic[@"Message"]];
        }
        
        NSLog(@"%@",dic);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [_searchTableView.mj_header endRefreshing];
        [_searchTableView.mj_footer endRefreshing];
        NSLog(@"%@",error);
    }];
}

#pragma  mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  _dataArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *idEp =@"cellEP";
    JobTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:idEp];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"JobTableViewCell" owner:self options:nil][0];
    }
    JobLIstModel *listMo = _dataArray[indexPath.row];
    cell.isSearchList = YES;
    cell.model = listMo;

    cell.jobNameLab.attributedText = [self getRangeStr:listMo.JobName findText:self.str];
    cell.companyName.attributedText = [self getRangeStr:listMo.CompanyName findText:self.str];
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100;
}


// 标题 和 正文 搜索到的关键字变色
- (NSMutableAttributedString *)getRangeStr:(NSString *)text findText:(NSString *)findText
{
     NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSString *temp = nil;
    for(int i = 0; i < [text length]; i++)
    {
        if (text.length - i < findText.length) {
            break;
        }
        temp = [text substringWithRange:NSMakeRange(i, findText.length)];
        
        if ([temp isEqualToString:findText]) {
            
            NSRange rang = NSMakeRange(i, findText.length);
            [attributedString addAttribute:NSForegroundColorAttributeName
                                     value:[UIColor colorWithRed:70/255.0 green:142/255.0 blue:239/255.0 alpha:1]
                                     range:NSMakeRange(rang.location, rang.length)];
        }
    }
    
    return attributedString;
}


- (void)PostJobAction:(UITapGestureRecognizer *)sender
{
//    EmploymentViewController
    for (UIViewController *Employment in self.navigationController.viewControllers) {
        if ([Employment isKindOfClass:[EmploymentViewController class]]) {
            [self.navigationController popToViewController:Employment animated:YES];
        }
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    JobdescriptionViewController *jobV =[[JobdescriptionViewController alloc]init];
//    [self.navigationController pushViewController:jobV animated:YES];
//    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
    JobLIstModel *jobList = _dataArray[indexPath.row];
    JobdescriptionViewController *jobV =[[JobdescriptionViewController alloc]init];
    jobV.jobID = [NSString stringWithFormat:@"%@",jobList.ID];
    [self.navigationController pushViewController:jobV animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)pushEmployme:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:NO];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
