


//
//  ItemView.m
//  oc
//
//  Created by lyl on 15/11/24.
//  Copyright © 2015年 miruo. All rights reserved.
//

#import "ItemView.h"
#import <QuartzCore/QuartzCore.h>

@interface ItemView()

@property (nonatomic,assign) CGFloat viewHeight;/**< item的高度 >*/
@end


@implementation ItemView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat viewWidth = CGRectGetWidth(self.frame);
    if (!_itemHeith) {
        _itemHeith = 20;
    }
    if (_itemArray.count > 0) {
        UIView *befourView = nil;
        for (NSInteger i = 0; i < _itemArray.count; i ++) {
            UILabel *itemLabel = [[UILabel alloc] init];
            itemLabel.tag = 500 + i;
            itemLabel.textAlignment = NSTextAlignmentCenter;
            itemLabel.textColor = [UIColor colorWithRed:74/255.0 green:116/255.0 blue:161/255.0 alpha:1];
            itemLabel.backgroundColor =  [UIColor colorWithRed:232/255.0 green:240/255.0 blue:254/255.0 alpha:1];
            itemLabel.font = [UIFont systemFontOfSize:13];
            itemLabel.text = _itemArray[i];
            itemLabel.userInteractionEnabled = YES;
            itemLabel.layer.cornerRadius = 2;
            itemLabel.clipsToBounds = YES;
            // 单击的 Recognizer
            MyTapGestureRecognizer* singleRecognizer;
            singleRecognizer = [[MyTapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
            singleRecognizer.label = itemLabel;
            //点击的次数
            singleRecognizer.numberOfTapsRequired = 1; // 单击
            //给self.view添加一个手势监测；
            [itemLabel addGestureRecognizer:singleRecognizer];
            [self addSubview:itemLabel];
            CGFloat itemWidth = [itemLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]}].width+10;
//            if (befourView) {
//                CGFloat maxWidht = CGRectGetMaxX(befourView.frame);
//                if ((maxWidht + itemWidth * 1.2 + 15 + 10) > viewWidth) {/**< 该换行了 >*/
//                    itemLabel.frame = CGRectMake(10, CGRectGetMaxY(befourView.frame) + 10, itemWidth * 1.2, _itemHeith);
//                }else {
//                    itemLabel.frame = CGRectMake(CGRectGetMaxX(befourView.frame) + 8, CGRectGetMinY(befourView.frame), itemWidth * 1.2, _itemHeith);
//                }
//            }else {
//                itemLabel.frame = CGRectMake(10, 10, itemWidth * 1.2, _itemHeith);
//            }
            if (befourView) {
                CGFloat maxWidht = CGRectGetMaxX(befourView.frame);
                if ((maxWidht + itemWidth + 15 + 10+20) > viewWidth) {/**< 该换行了 >*/
                    itemLabel.frame = CGRectMake(10, CGRectGetMaxY(befourView.frame) + 10, itemWidth+20, _itemHeith);
                }else {
                    itemLabel.frame = CGRectMake(CGRectGetMaxX(befourView.frame) + 8, CGRectGetMinY(befourView.frame), itemWidth+20, _itemHeith);
                }
            }else {
                itemLabel.frame = CGRectMake(10, 10, itemWidth+20, _itemHeith);
            }

            befourView = itemLabel;
        }
        self.viewHeight = CGRectGetMaxY(befourView.frame) + 10;
    }
}
//- (CGFloat)getItemViewHeight {
//    return _viewHeight;
//}


//- (void)setItemArray:(NSArray *)itemArray {
//    if (_itemArray != itemArray) {
//        _itemArray = itemArray;
//        for (NSInteger i = 0; i < _itemArray.count; i ++) {
//            
//            
//        }
//    }
//}



- (void)ItemViewWithBlock:(ItemViewBlock)block {
    self.block = block;
}

- (void)setViewHeight:(CGFloat)viewHeight {
    if (_viewHeight != viewHeight) {
        _viewHeight = viewHeight;
        if (self.block) {
            self.block([NSNumber numberWithFloat:_viewHeight]);
        }
    }
}


- (void)tapClick:(MyTapGestureRecognizer *)tap{
    if ([self.target respondsToSelector:self.selecter]) {
        NSArray *array = @[[NSString stringWithFormat:@"%lf,%lf",tap.label.frame.origin.x,tap.label.frame.origin.y],tap.label.text];
        [self.target performSelector:self.selecter withObject:array];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
