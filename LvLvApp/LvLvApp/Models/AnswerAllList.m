//
//  AnswerAllList.m
//
//  Created by hope  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "AnswerAllList.h"


NSString *const kAnswerAllListUserID = @"UserID";
NSString *const kAnswerAllListAnswerContentAll = @"AnswerContentAll";
NSString *const kAnswerAllListTimeDescrption = @"TimeDescrption";
NSString *const kAnswerAllListCounts = @"counts";
NSString *const kAnswerAllListUserIamgeUrl = @"UserIamgeUrl";
NSString *const kAnswerAllListNickName = @"NickName";
NSString *const kAnswerAllListAnswerID = @"Answer_ID";
NSString *const kAnswerAllListCollectCount = @"collectCount";
NSString *const kAnswerAllListCommentCount = @"commentCount";


@interface AnswerAllList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AnswerAllList

@synthesize userID = _userID;
@synthesize answerContentAll = _answerContentAll;
@synthesize timeDescrption = _timeDescrption;
@synthesize counts = _counts;
@synthesize userIamgeUrl = _userIamgeUrl;
@synthesize nickName = _nickName;
@synthesize answerID = _answerID;
@synthesize collectCount = _collectCount;
@synthesize commentCount = _commentCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userID = [self objectOrNilForKey:kAnswerAllListUserID fromDictionary:dict];
            self.answerContentAll = [self objectOrNilForKey:kAnswerAllListAnswerContentAll fromDictionary:dict];
            self.timeDescrption = [self objectOrNilForKey:kAnswerAllListTimeDescrption fromDictionary:dict];
            self.counts = [[self objectOrNilForKey:kAnswerAllListCounts fromDictionary:dict] doubleValue];
            self.userIamgeUrl = [self objectOrNilForKey:kAnswerAllListUserIamgeUrl fromDictionary:dict];
            self.nickName = [self objectOrNilForKey:kAnswerAllListNickName fromDictionary:dict];
            self.answerID = [self objectOrNilForKey:kAnswerAllListAnswerID fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kAnswerAllListCollectCount fromDictionary:dict] doubleValue];
            self.commentCount = [[self objectOrNilForKey:kAnswerAllListCommentCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.userID forKey:kAnswerAllListUserID];
    [mutableDict setValue:self.answerContentAll forKey:kAnswerAllListAnswerContentAll];
    [mutableDict setValue:self.timeDescrption forKey:kAnswerAllListTimeDescrption];
    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kAnswerAllListCounts];
    [mutableDict setValue:self.userIamgeUrl forKey:kAnswerAllListUserIamgeUrl];
    [mutableDict setValue:self.nickName forKey:kAnswerAllListNickName];
    [mutableDict setValue:self.answerID forKey:kAnswerAllListAnswerID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kAnswerAllListCollectCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kAnswerAllListCommentCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userID = [aDecoder decodeObjectForKey:kAnswerAllListUserID];
    self.answerContentAll = [aDecoder decodeObjectForKey:kAnswerAllListAnswerContentAll];
    self.timeDescrption = [aDecoder decodeObjectForKey:kAnswerAllListTimeDescrption];
    self.counts = [aDecoder decodeDoubleForKey:kAnswerAllListCounts];
    self.userIamgeUrl = [aDecoder decodeObjectForKey:kAnswerAllListUserIamgeUrl];
    self.nickName = [aDecoder decodeObjectForKey:kAnswerAllListNickName];
    self.answerID = [aDecoder decodeObjectForKey:kAnswerAllListAnswerID];
    self.collectCount = [aDecoder decodeDoubleForKey:kAnswerAllListCollectCount];
    self.commentCount = [aDecoder decodeDoubleForKey:kAnswerAllListCommentCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_userID forKey:kAnswerAllListUserID];
    [aCoder encodeObject:_answerContentAll forKey:kAnswerAllListAnswerContentAll];
    [aCoder encodeObject:_timeDescrption forKey:kAnswerAllListTimeDescrption];
    [aCoder encodeDouble:_counts forKey:kAnswerAllListCounts];
    [aCoder encodeObject:_userIamgeUrl forKey:kAnswerAllListUserIamgeUrl];
    [aCoder encodeObject:_nickName forKey:kAnswerAllListNickName];
    [aCoder encodeObject:_answerID forKey:kAnswerAllListAnswerID];
    [aCoder encodeDouble:_collectCount forKey:kAnswerAllListCollectCount];
    [aCoder encodeDouble:_commentCount forKey:kAnswerAllListCommentCount];
}

- (id)copyWithZone:(NSZone *)zone
{
    AnswerAllList *copy = [[AnswerAllList alloc] init];
    
    if (copy) {

        copy.userID = [self.userID copyWithZone:zone];
        copy.answerContentAll = [self.answerContentAll copyWithZone:zone];
        copy.timeDescrption = [self.timeDescrption copyWithZone:zone];
        copy.counts = self.counts;
        copy.userIamgeUrl = [self.userIamgeUrl copyWithZone:zone];
        copy.nickName = [self.nickName copyWithZone:zone];
        copy.answerID = [self.answerID copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.commentCount = self.commentCount;
    }
    
    return copy;
}


@end
