//
//  EditAnswerViewController.h
//  LvLvApp
//
//  Created by jim on 15/12/28.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^BackBlock)(void);
@interface EditQusetionViewController : BaseViewController
@property (nonatomic,copy) NSMutableString *content;
@property (nonatomic,copy) NSString *tID;
@property (nonatomic,copy) NSAttributedString *texts;
@property (nonatomic,strong) NSMutableArray *imageList;
@property (nonatomic,strong) BackBlock backBlock;
@end
