//
//  HLableListResult.h
//
//  Created by hope  on 15/12/5
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface HLableListResult : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *tID;
@property (nonatomic, strong) id userid;
@property (nonatomic, strong) id listResultDescription;
@property (nonatomic, strong) id timagesUrl;
@property (nonatomic, strong) NSString *logoUrl;
@property (nonatomic, strong) id questionContent;
@property (nonatomic, assign) double collectCount;
@property (nonatomic, strong) NSString *sortTime;
@property (nonatomic, strong) id nickname;
@property (nonatomic, assign) double counts;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *answerContent;
@property (nonatomic, assign) double commentCount;
@property (nonatomic, strong) id times;
@property (nonatomic, assign) double types;
@property (nonatomic, strong) NSString *answerID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
