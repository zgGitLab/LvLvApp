//
//  S_LISTCase.m
//
//  Created by IOS8  on 15/11/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_LISTCase.h"
#import "S_Search.h"
#import "S_List.h"


NSString *const kS_LISTCaseSearch = @"search";
NSString *const kS_LISTCaseCount = @"count";
NSString *const kS_LISTCaseList = @"list";
NSString *const kS_LISTCasePageindex = @"pageindex";
NSString *const kS_LISTCasePagesize = @"pagesize";


@interface S_LISTCase ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_LISTCase

@synthesize search = _search;
@synthesize count = _count;
@synthesize list = _list;
@synthesize pageindex = _pageindex;
@synthesize pagesize = _pagesize;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedS_Search = [dict objectForKey:kS_LISTCaseSearch];
    NSMutableArray *parsedS_Search = [NSMutableArray array];
    if ([receivedS_Search isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_Search) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_Search addObject:[S_Search modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_Search isKindOfClass:[NSDictionary class]]) {
       [parsedS_Search addObject:[S_Search modelObjectWithDictionary:(NSDictionary *)receivedS_Search]];
    }

    self.search = [NSArray arrayWithArray:parsedS_Search];
            self.count = [[self objectOrNilForKey:kS_LISTCaseCount fromDictionary:dict] doubleValue];
    NSObject *receivedS_List = [dict objectForKey:kS_LISTCaseList];
    NSMutableArray *parsedS_List = [NSMutableArray array];
    if ([receivedS_List isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_List) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_List addObject:[S_List modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_List isKindOfClass:[NSDictionary class]]) {
       [parsedS_List addObject:[S_List modelObjectWithDictionary:(NSDictionary *)receivedS_List]];
    }

    self.list = [NSArray arrayWithArray:parsedS_List];
            self.pageindex = [[self objectOrNilForKey:kS_LISTCasePageindex fromDictionary:dict] doubleValue];
            self.pagesize = [[self objectOrNilForKey:kS_LISTCasePagesize fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForSearch = [NSMutableArray array];
    for (NSObject *subArrayObject in self.search) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSearch addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSearch addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSearch] forKey:kS_LISTCaseSearch];
    [mutableDict setValue:[NSNumber numberWithDouble:self.count] forKey:kS_LISTCaseCount];
    NSMutableArray *tempArrayForList = [NSMutableArray array];
    for (NSObject *subArrayObject in self.list) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForList addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForList] forKey:kS_LISTCaseList];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pageindex] forKey:kS_LISTCasePageindex];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pagesize] forKey:kS_LISTCasePagesize];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.search = [aDecoder decodeObjectForKey:kS_LISTCaseSearch];
    self.count = [aDecoder decodeDoubleForKey:kS_LISTCaseCount];
    self.list = [aDecoder decodeObjectForKey:kS_LISTCaseList];
    self.pageindex = [aDecoder decodeDoubleForKey:kS_LISTCasePageindex];
    self.pagesize = [aDecoder decodeDoubleForKey:kS_LISTCasePagesize];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_search forKey:kS_LISTCaseSearch];
    [aCoder encodeDouble:_count forKey:kS_LISTCaseCount];
    [aCoder encodeObject:_list forKey:kS_LISTCaseList];
    [aCoder encodeDouble:_pageindex forKey:kS_LISTCasePageindex];
    [aCoder encodeDouble:_pagesize forKey:kS_LISTCasePagesize];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_LISTCase *copy = [[S_LISTCase alloc] init];
    
    if (copy) {

        copy.search = [self.search copyWithZone:zone];
        copy.count = self.count;
        copy.list = [self.list copyWithZone:zone];
        copy.pageindex = self.pageindex;
        copy.pagesize = self.pagesize;
    }
    
    return copy;
}


@end
