//
//  S_MyFansData.m
//
//  Created by IOS8  on 15/12/17
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_MyFansData.h"


NSString *const kS_MyFansDataID = @"ID";
NSString *const kS_MyFansDataIntroduction = @"Introduction";
NSString *const kS_MyFansDataIsFocus = @"IsFocus";
NSString *const kS_MyFansDataImage = @"Image";
NSString *const kS_MyFansDataNickName = @"NickName";


@interface S_MyFansData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_MyFansData

@synthesize iDProperty = _iDProperty;
@synthesize introduction = _introduction;
@synthesize isFocus = _isFocus;
@synthesize image = _image;
@synthesize nickName = _nickName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kS_MyFansDataID fromDictionary:dict] doubleValue];
            self.introduction = [self objectOrNilForKey:kS_MyFansDataIntroduction fromDictionary:dict];
            self.isFocus = [[self objectOrNilForKey:kS_MyFansDataIsFocus fromDictionary:dict] doubleValue];
            self.image = [self objectOrNilForKey:kS_MyFansDataImage fromDictionary:dict];
            self.nickName = [self objectOrNilForKey:kS_MyFansDataNickName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kS_MyFansDataID];
    [mutableDict setValue:self.introduction forKey:kS_MyFansDataIntroduction];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isFocus] forKey:kS_MyFansDataIsFocus];
    [mutableDict setValue:self.image forKey:kS_MyFansDataImage];
    [mutableDict setValue:self.nickName forKey:kS_MyFansDataNickName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kS_MyFansDataID];
    self.introduction = [aDecoder decodeObjectForKey:kS_MyFansDataIntroduction];
    self.isFocus = [aDecoder decodeDoubleForKey:kS_MyFansDataIsFocus];
    self.image = [aDecoder decodeObjectForKey:kS_MyFansDataImage];
    self.nickName = [aDecoder decodeObjectForKey:kS_MyFansDataNickName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kS_MyFansDataID];
    [aCoder encodeObject:_introduction forKey:kS_MyFansDataIntroduction];
    [aCoder encodeDouble:_isFocus forKey:kS_MyFansDataIsFocus];
    [aCoder encodeObject:_image forKey:kS_MyFansDataImage];
    [aCoder encodeObject:_nickName forKey:kS_MyFansDataNickName];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_MyFansData *copy = [[S_MyFansData alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.introduction = [self.introduction copyWithZone:zone];
        copy.isFocus = self.isFocus;
        copy.image = [self.image copyWithZone:zone];
        copy.nickName = [self.nickName copyWithZone:zone];
    }
    
    return copy;
}


@end
