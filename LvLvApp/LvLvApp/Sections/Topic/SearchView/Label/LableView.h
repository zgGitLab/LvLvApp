//
//  LableView.h
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchTopicViewController.h"
#import "LBBaseClass.h"
#import "LBData.h"
typedef void (^labelarrayBlock)(NSMutableArray *);
@protocol LableViewDelegate<NSObject>

- (void)Lableclick:(NSInteger)indxe;

@end
@interface LableView : UIView <UITableViewDataSource,UITableViewDelegate>
//- (id)initWithFrame:(CGRect)frame;

typedef void(^lableCellClick)(LableView *);
@property (nonatomic, strong)lableCellClick lableCellBack;
@property(nonatomic, unsafe_unretained) id<LableViewDelegate>LableDelegate;

@property (nonatomic, strong)NSMutableArray *labedataarray;
@property (nonatomic, strong)LBBaseClass *lableseca;
@property (nonatomic, strong)LBData *ldData;
@property (nonatomic, strong)NSString *searString;
@property (nonatomic, strong)UITableView *labTableView;
@property (nonatomic, copy)labelarrayBlock labelarrays;
@property (nonatomic, strong)NSString *lableName;
@property (nonatomic, strong)NSString *T;
@property (nonatomic, strong)NSString *IsFocus;

- (void)getlableSearText:(NSString *)strr;
- (id)initWithString:(NSString*)strr;

@end
