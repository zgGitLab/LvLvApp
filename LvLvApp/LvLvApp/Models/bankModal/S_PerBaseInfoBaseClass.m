//
//  S_PerBaseInfoBaseClass.m
//
//  Created by IOS8  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_PerBaseInfoBaseClass.h"
#import "S_PerBaseInfoJobList.h"
#import "S_PerBaseInfoEductList.h"

NSString *const kS_PerBaseInfoBaseClassImage = @"Image";
NSString *const kS_PerBaseInfoBaseClassUserID = @"UserID";
NSString *const kS_PerBaseInfoBaseClassIntroduction = @"Introduction";
NSString *const kS_PerBaseInfoBaseClassJobList = @"JobList";
NSString *const kS_PerBaseInfoBaseClassGender = @"Gender";
NSString *const kS_PerBaseInfoBaseClassTelPhone = @"TelPhone";
NSString *const kS_PerBaseInfoBaseClassNickName = @"NickName";
NSString *const kS_PerBaseInfoBaseClassEductList = @"EductList";


@interface S_PerBaseInfoBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_PerBaseInfoBaseClass

@synthesize userID = _userID;
@synthesize imageUrl = _imageUrl;
@synthesize introduction = _introduction;
@synthesize jobList = _jobList;
@synthesize gender = _gender;
@synthesize telPhone = _telPhone;
@synthesize nickName = _nickName;
@synthesize eductList = _eductList;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.imageUrl = [self objectOrNilForKey:kS_PerBaseInfoBaseClassImage fromDictionary:dict];
            self.userID = [[self objectOrNilForKey:kS_PerBaseInfoBaseClassUserID fromDictionary:dict] doubleValue];
            self.introduction = [self objectOrNilForKey:kS_PerBaseInfoBaseClassIntroduction fromDictionary:dict];
    NSObject *receivedS_PerBaseInfoJobList = [dict objectForKey:kS_PerBaseInfoBaseClassJobList];
    NSMutableArray *parsedS_PerBaseInfoJobList = [NSMutableArray array];
    if ([receivedS_PerBaseInfoJobList isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_PerBaseInfoJobList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_PerBaseInfoJobList addObject:[S_PerBaseInfoJobList modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_PerBaseInfoJobList isKindOfClass:[NSDictionary class]]) {
       [parsedS_PerBaseInfoJobList addObject:[S_PerBaseInfoJobList modelObjectWithDictionary:(NSDictionary *)receivedS_PerBaseInfoJobList]];
    }

    self.jobList = [NSArray arrayWithArray:parsedS_PerBaseInfoJobList];
            self.gender = [self objectOrNilForKey:kS_PerBaseInfoBaseClassGender fromDictionary:dict];
            self.telPhone = [self objectOrNilForKey:kS_PerBaseInfoBaseClassTelPhone fromDictionary:dict];
            self.nickName = [self objectOrNilForKey:kS_PerBaseInfoBaseClassNickName fromDictionary:dict];
    NSObject *receivedS_PerBaseInfoEductList = [dict objectForKey:kS_PerBaseInfoBaseClassEductList];
    NSMutableArray *parsedS_PerBaseInfoEductList = [NSMutableArray array];
    if ([receivedS_PerBaseInfoEductList isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_PerBaseInfoEductList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_PerBaseInfoEductList addObject:[S_PerBaseInfoEductList modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_PerBaseInfoEductList isKindOfClass:[NSDictionary class]]) {
       [parsedS_PerBaseInfoEductList addObject:[S_PerBaseInfoEductList modelObjectWithDictionary:(NSDictionary *)receivedS_PerBaseInfoEductList]];
    }

    self.eductList = [NSArray arrayWithArray:parsedS_PerBaseInfoEductList];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kS_PerBaseInfoBaseClassUserID];
    [mutableDict setValue:self.introduction forKey:kS_PerBaseInfoBaseClassIntroduction];
    NSMutableArray *tempArrayForJobList = [NSMutableArray array];
    for (NSObject *subArrayObject in self.jobList) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForJobList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForJobList addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForJobList] forKey:kS_PerBaseInfoBaseClassJobList];
    [mutableDict setValue:self.gender forKey:kS_PerBaseInfoBaseClassGender];
    [mutableDict setValue:self.telPhone forKey:kS_PerBaseInfoBaseClassTelPhone];
    [mutableDict setValue:self.nickName forKey:kS_PerBaseInfoBaseClassNickName];
    NSMutableArray *tempArrayForEductList = [NSMutableArray array];
    for (NSObject *subArrayObject in self.eductList) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForEductList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForEductList addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForEductList] forKey:kS_PerBaseInfoBaseClassEductList];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userID = [aDecoder decodeDoubleForKey:kS_PerBaseInfoBaseClassUserID];
    self.introduction = [aDecoder decodeObjectForKey:kS_PerBaseInfoBaseClassImage];
    self.imageUrl = [aDecoder decodeObjectForKey:kS_PerBaseInfoBaseClassIntroduction];
    self.jobList = [aDecoder decodeObjectForKey:kS_PerBaseInfoBaseClassJobList];
    self.gender = [aDecoder decodeObjectForKey:kS_PerBaseInfoBaseClassGender];
    self.telPhone = [aDecoder decodeObjectForKey:kS_PerBaseInfoBaseClassTelPhone];
    self.nickName = [aDecoder decodeObjectForKey:kS_PerBaseInfoBaseClassNickName];
    self.eductList = [aDecoder decodeObjectForKey:kS_PerBaseInfoBaseClassEductList];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_userID forKey:kS_PerBaseInfoBaseClassUserID];
    [aCoder encodeObject:_imageUrl forKey:kS_PerBaseInfoBaseClassImage];
    [aCoder encodeObject:_introduction forKey:kS_PerBaseInfoBaseClassIntroduction];
    [aCoder encodeObject:_jobList forKey:kS_PerBaseInfoBaseClassJobList];
    [aCoder encodeObject:_gender forKey:kS_PerBaseInfoBaseClassGender];
    [aCoder encodeObject:_telPhone forKey:kS_PerBaseInfoBaseClassTelPhone];
    [aCoder encodeObject:_nickName forKey:kS_PerBaseInfoBaseClassNickName];
    [aCoder encodeObject:_eductList forKey:kS_PerBaseInfoBaseClassEductList];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_PerBaseInfoBaseClass *copy = [[S_PerBaseInfoBaseClass alloc] init];
    
    if (copy) {

        copy.userID = self.userID;
        copy.introduction = [self.introduction copyWithZone:zone];
        copy.jobList = [self.jobList copyWithZone:zone];
        copy.gender = [self.gender copyWithZone:zone];
        copy.telPhone = [self.telPhone copyWithZone:zone];
        copy.nickName = [self.nickName copyWithZone:zone];
        copy.eductList = [self.eductList copyWithZone:zone];
    }
    
    return copy;
}


@end
