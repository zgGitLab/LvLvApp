//
//  LabelDetailViewController.h
//  LvLvApp
//
//  Created by hope on 15/11/30.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^refreshLabel)(void);
@class labelDetailHeaderView;
@interface LabelDetailViewController : UIViewController
@property (nonatomic,strong) UITableView *labetableView;
@property (nonatomic,copy) NSString * UserID;
@property (nonatomic,copy) NSString * Keywordstr;
@property (nonatomic, strong)labelDetailHeaderView *headerView;
@property (nonatomic, strong)NSMutableArray *lalabarrays;
@property (nonatomic,copy) NSString * labName;
@property (nonatomic, assign)NSInteger AttonCount;
@property (nonatomic, strong)NSString *TSS;
@property (nonatomic, strong)NSMutableArray *attlabel;
@property (nonatomic,copy) refreshLabel refreshLabel;
@end
