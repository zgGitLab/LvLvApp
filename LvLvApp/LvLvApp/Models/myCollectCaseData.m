//
//  myCollectCaseData.m
//
//  Created by hope  on 15/12/17
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "myCollectCaseData.h"


NSString *const kmyCollectCaseDataId = @"id";
NSString *const kmyCollectCaseDataJudgeDate = @"judge_date";
NSString *const kmyCollectCaseDataCourtId = @"court_id";
NSString *const kmyCollectCaseDataCourt = @"court";
NSString *const kmyCollectCaseDataDocId = @"DocId";
NSString *const kmyCollectCaseDataCreatedAt = @"created_at";
NSString *const kmyCollectCaseDataUrl = @"url";
NSString *const kmyCollectCaseDataTitle = @"title";
NSString *const kmyCollectCaseDataLevel = @"level";
NSString *const kmyCollectCaseDataReason = @"reason";
NSString *const kmyCollectCaseDataCharacter = @"character";
NSString *const kmyCollectCaseDataContent = @"content";


@interface myCollectCaseData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation myCollectCaseData

@synthesize dataIdentifier = _dataIdentifier;
@synthesize judgeDate = _judgeDate;
@synthesize courtId = _courtId;
@synthesize court = _court;
@synthesize docId = _docId;
@synthesize createdAt = _createdAt;
@synthesize url = _url;
@synthesize title = _title;
@synthesize level = _level;
@synthesize reason = _reason;
@synthesize character = _character;
@synthesize content = _content;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.dataIdentifier = [[self objectOrNilForKey:kmyCollectCaseDataId fromDictionary:dict] doubleValue];
            self.judgeDate = [self objectOrNilForKey:kmyCollectCaseDataJudgeDate fromDictionary:dict];
            self.courtId = [[self objectOrNilForKey:kmyCollectCaseDataCourtId fromDictionary:dict] doubleValue];
            self.court = [self objectOrNilForKey:kmyCollectCaseDataCourt fromDictionary:dict];
            self.docId = [[self objectOrNilForKey:kmyCollectCaseDataDocId fromDictionary:dict] doubleValue];
            self.createdAt = [self objectOrNilForKey:kmyCollectCaseDataCreatedAt fromDictionary:dict];
            self.url = [self objectOrNilForKey:kmyCollectCaseDataUrl fromDictionary:dict];
            self.title = [self objectOrNilForKey:kmyCollectCaseDataTitle fromDictionary:dict];
            self.level = [self objectOrNilForKey:kmyCollectCaseDataLevel fromDictionary:dict];
            self.reason = [self objectOrNilForKey:kmyCollectCaseDataReason fromDictionary:dict];
            self.character = [self objectOrNilForKey:kmyCollectCaseDataCharacter fromDictionary:dict];
            self.content = [self objectOrNilForKey:kmyCollectCaseDataContent fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kmyCollectCaseDataId];
    [mutableDict setValue:self.judgeDate forKey:kmyCollectCaseDataJudgeDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.courtId] forKey:kmyCollectCaseDataCourtId];
    [mutableDict setValue:self.court forKey:kmyCollectCaseDataCourt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.docId] forKey:kmyCollectCaseDataDocId];
    [mutableDict setValue:self.createdAt forKey:kmyCollectCaseDataCreatedAt];
    [mutableDict setValue:self.url forKey:kmyCollectCaseDataUrl];
    [mutableDict setValue:self.title forKey:kmyCollectCaseDataTitle];
    [mutableDict setValue:self.level forKey:kmyCollectCaseDataLevel];
    [mutableDict setValue:self.reason forKey:kmyCollectCaseDataReason];
    [mutableDict setValue:self.character forKey:kmyCollectCaseDataCharacter];
    [mutableDict setValue:self.content forKey:kmyCollectCaseDataContent];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.dataIdentifier = [aDecoder decodeDoubleForKey:kmyCollectCaseDataId];
    self.judgeDate = [aDecoder decodeObjectForKey:kmyCollectCaseDataJudgeDate];
    self.courtId = [aDecoder decodeDoubleForKey:kmyCollectCaseDataCourtId];
    self.court = [aDecoder decodeObjectForKey:kmyCollectCaseDataCourt];
    self.docId = [aDecoder decodeDoubleForKey:kmyCollectCaseDataDocId];
    self.createdAt = [aDecoder decodeObjectForKey:kmyCollectCaseDataCreatedAt];
    self.url = [aDecoder decodeObjectForKey:kmyCollectCaseDataUrl];
    self.title = [aDecoder decodeObjectForKey:kmyCollectCaseDataTitle];
    self.level = [aDecoder decodeObjectForKey:kmyCollectCaseDataLevel];
    self.reason = [aDecoder decodeObjectForKey:kmyCollectCaseDataReason];
    self.character = [aDecoder decodeObjectForKey:kmyCollectCaseDataCharacter];
    self.content = [aDecoder decodeObjectForKey:kmyCollectCaseDataContent];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_dataIdentifier forKey:kmyCollectCaseDataId];
    [aCoder encodeObject:_judgeDate forKey:kmyCollectCaseDataJudgeDate];
    [aCoder encodeDouble:_courtId forKey:kmyCollectCaseDataCourtId];
    [aCoder encodeObject:_court forKey:kmyCollectCaseDataCourt];
    [aCoder encodeDouble:_docId forKey:kmyCollectCaseDataDocId];
    [aCoder encodeObject:_createdAt forKey:kmyCollectCaseDataCreatedAt];
    [aCoder encodeObject:_url forKey:kmyCollectCaseDataUrl];
    [aCoder encodeObject:_title forKey:kmyCollectCaseDataTitle];
    [aCoder encodeObject:_level forKey:kmyCollectCaseDataLevel];
    [aCoder encodeObject:_reason forKey:kmyCollectCaseDataReason];
    [aCoder encodeObject:_character forKey:kmyCollectCaseDataCharacter];
    [aCoder encodeObject:_content forKey:kmyCollectCaseDataContent];
}

- (id)copyWithZone:(NSZone *)zone
{
    myCollectCaseData *copy = [[myCollectCaseData alloc] init];
    
    if (copy) {

        copy.dataIdentifier = self.dataIdentifier;
        copy.judgeDate = [self.judgeDate copyWithZone:zone];
        copy.courtId = self.courtId;
        copy.court = [self.court copyWithZone:zone];
        copy.docId = self.docId;
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.level = [self.level copyWithZone:zone];
        copy.reason = [self.reason copyWithZone:zone];
        copy.character = [self.character copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
    }
    
    return copy;
}


@end
