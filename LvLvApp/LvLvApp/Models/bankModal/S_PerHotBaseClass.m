//
//  S_PerHotBaseClass.m
//
//  Created by IOS8  on 15/12/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_PerHotBaseClass.h"
#import "S_PerHotData.h"


NSString *const kS_PerHotBaseClassCode = @"Code";
NSString *const kS_PerHotBaseClassMessage = @"Message";
NSString *const kS_PerHotBaseClassData = @"Data";


@interface S_PerHotBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_PerHotBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_PerHotBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_PerHotBaseClassMessage fromDictionary:dict];
    NSObject *receivedS_PerHotData = [dict objectForKey:kS_PerHotBaseClassData];
    NSMutableArray *parsedS_PerHotData = [NSMutableArray array];
    if ([receivedS_PerHotData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_PerHotData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_PerHotData addObject:[S_PerHotData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_PerHotData isKindOfClass:[NSDictionary class]]) {
       [parsedS_PerHotData addObject:[S_PerHotData modelObjectWithDictionary:(NSDictionary *)receivedS_PerHotData]];
    }

    self.data = [NSArray arrayWithArray:parsedS_PerHotData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_PerHotBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_PerHotBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kS_PerHotBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_PerHotBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_PerHotBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_PerHotBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_PerHotBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_PerHotBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_PerHotBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_PerHotBaseClass *copy = [[S_PerHotBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
