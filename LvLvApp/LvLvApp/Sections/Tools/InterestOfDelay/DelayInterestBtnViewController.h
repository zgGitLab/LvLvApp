//
//  DelayInterestBtnViewController.h
//  Tools
//
//  Created by IOS8 on 15/9/11.
//  Copyright (c) 2015年 IOS8. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DelayInterestBtnViewController : BaseViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *bgScroll;
@property (weak, nonatomic) IBOutlet UITextField *moneyText;
@property (weak, nonatomic) IBOutlet UIButton *effectDate;
@property (weak, nonatomic) IBOutlet UIButton *startDate;
@property (weak, nonatomic) IBOutlet UITextField *liLv;
@property (weak, nonatomic) IBOutlet UITextField *contractDate;
@property (weak, nonatomic) IBOutlet UIButton *finshDate;

@property (weak, nonatomic) IBOutlet UILabel *resultMoney;
@property (weak, nonatomic) IBOutlet UILabel *delayMoney;
@property (weak, nonatomic) IBOutlet UILabel *sumMoney;
@property (weak, nonatomic) IBOutlet UILabel *count;

- (IBAction)selectDate:(UIButton *)sender;

- (IBAction)computeBtn:(UIButton *)sender;

@end
