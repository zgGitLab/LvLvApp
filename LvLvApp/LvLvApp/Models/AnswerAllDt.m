//
//  AnswerAllDt.m
//
//  Created by hope  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "AnswerAllDt.h"


NSString *const kAnswerAllDtID = @"ID";
NSString *const kAnswerAllDtUserID = @"UserID";
NSString *const kAnswerAllDtIHID = @"IHID";
NSString *const kAnswerAllDtTopicName = @"TopicName";
NSString *const kAnswerAllDtUrl = @"Url";


@interface AnswerAllDt ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AnswerAllDt

@synthesize iDProperty = _iDProperty;
@synthesize userID = _userID;
@synthesize iHID = _iHID;
@synthesize topicName = _topicName;
@synthesize url = _url;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kAnswerAllDtID fromDictionary:dict] doubleValue];
            self.userID = [[self objectOrNilForKey:kAnswerAllDtUserID fromDictionary:dict] doubleValue];
            self.iHID = [self objectOrNilForKey:kAnswerAllDtIHID fromDictionary:dict];
            self.topicName = [self objectOrNilForKey:kAnswerAllDtTopicName fromDictionary:dict];
            self.url = [self objectOrNilForKey:kAnswerAllDtUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kAnswerAllDtID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kAnswerAllDtUserID];
    [mutableDict setValue:self.iHID forKey:kAnswerAllDtIHID];
    [mutableDict setValue:self.topicName forKey:kAnswerAllDtTopicName];
    [mutableDict setValue:self.url forKey:kAnswerAllDtUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kAnswerAllDtID];
    self.userID = [aDecoder decodeDoubleForKey:kAnswerAllDtUserID];
    self.iHID = [aDecoder decodeObjectForKey:kAnswerAllDtIHID];
    self.topicName = [aDecoder decodeObjectForKey:kAnswerAllDtTopicName];
    self.url = [aDecoder decodeObjectForKey:kAnswerAllDtUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kAnswerAllDtID];
    [aCoder encodeDouble:_userID forKey:kAnswerAllDtUserID];
    [aCoder encodeObject:_iHID forKey:kAnswerAllDtIHID];
    [aCoder encodeObject:_topicName forKey:kAnswerAllDtTopicName];
    [aCoder encodeObject:_url forKey:kAnswerAllDtUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    AnswerAllDt *copy = [[AnswerAllDt alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.userID = self.userID;
        copy.iHID = [self.iHID copyWithZone:zone];
        copy.topicName = [self.topicName copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
    }
    
    return copy;
}


@end
