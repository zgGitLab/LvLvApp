//
//  anliApply.m
//  LvLvApp
//
//  Created by hope on 15/10/19.
//  Copyright (c) 2015年 gyy. All rights reserved.
//
#import "ChooseView.h"
#import "LawCaseApply.h"
#import "SSFViewController.h"

#define kZSHFzhijianjULI  headerView.frame.size.height
#define kZSFjuli  chooseView.frame.size.height-155

@implementation LawCaseApply
{
    UIScrollView *mainView;
    ChooseView *chooseView;
    UIView *headerView;//头部
    UIView *calcView; //计算view
    
    UILabel *typeOfAcceptance;//受理类型
    UITextField *typeOfAcceptanceText;
    UIButton *typeOfAcceptanceBtn;
    
    UILabel *inputMoney;//输入金额
    UITextField *inputMoneyText;//输入金额的
    
    UIButton *calcBtn;//计算
    UIButton *resetBtn;//重置
    
    UILabel *calcResult;//计算结果
    UILabel *calcResultLabel;
    
}
- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 99-64, [UIScreen mainScreen].bounds.size.width,  [UIScreen mainScreen].bounds.size.height-99);
        self.backgroundColor = [UIColor whiteColor];
        //self.backgroundColor =[UIColor yellowColor];
        
        [self headerViewleiru];
        [self tableViewshu];
        [self foodViewjine];
    }
    return self;
}

- (id)initwithView:(UIScrollView *)scrollView{
    LawCaseApply *view = [self init];
    mainView = scrollView;
    return view;
}


- (void)tableViewshu
{
    _dataArr = [NSArray arrayWithObjects:@"申请执行:申请执行",@"申请保全：申请财产保全",@"申请支付:申请支付令",@"申请公示催告:申请公示催告",@"申请撤销仲裁裁决或者认定仲裁协议效力",@"申请破产",@"海事:申请设立海事赔偿责任限制基金",@"海事:申请海事强制令",@"海事:申请海事船舶优先权催告",@"海事:申请海事债权登记",@"海事:申请共同海损理算" ,nil];
    chooseView = [[ChooseView alloc]initWithFrame:CGRectMake(typeOfAcceptanceText.frame.origin.x+10, headerView.frame.size.height+5,typeOfAcceptanceText.frame.size.width, [_dataArr count] * 44+165)];
    chooseView.dataArray = _dataArr;
    chooseView.delegate = self;
    [self addSubview:chooseView];
    
}
- (void)chooseViewSelectInfo:(NSString *)info{
    [typeOfAcceptanceText setText:info];
    [UIView animateWithDuration:0.3 animations:^{
        calcView.frame =CGRectMake(10,kZSHFzhijianjULI, self.bounds.size.width-20, 200);
    }];
}
-(void)headerViewleiru
{
    headerView =[[UIView alloc]initWithFrame:CGRectMake(10,10, self.bounds.size.width-20, 50)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    [headerView setAlpha:1];
    [self addSubview:headerView];
    
    typeOfAcceptance =[[UILabel alloc]initWithFrame:CGRectMake(6, 10, 75, 30)];
    [typeOfAcceptance setText:@"申请类型"];
    [typeOfAcceptance setFont:[UIFont systemFontOfSize:16.0f]];
    [typeOfAcceptance setTextColor:[UIColor blackColor]];
    [headerView addSubview:typeOfAcceptance];
    
    typeOfAcceptanceText= [[UITextField alloc]initWithFrame:CGRectMake(typeOfAcceptance.frame.size.width+10, 10, headerView.bounds.size.width-100, 30)];
    [typeOfAcceptanceText setPlaceholder:@"请点击选择"];
    typeOfAcceptanceText.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:242.0/255.0 blue:244.0/255.0 alpha:1];
    [typeOfAcceptanceText setTextAlignment:NSTextAlignmentCenter];
    [typeOfAcceptanceText setFont:[UIFont systemFontOfSize:16.0f]];
    
    [headerView addSubview:typeOfAcceptanceText];
    
    
    typeOfAcceptanceBtn=[[UIButton alloc]initWithFrame:CGRectMake(typeOfAcceptance.frame.size.width+20, 10, headerView.bounds.size.width-100, 30)];
    [typeOfAcceptanceBtn setTintColor:[UIColor  whiteColor]];
    [typeOfAcceptanceBtn addTarget:self action:@selector(typeOfAcceptanceBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [headerView addSubview:typeOfAcceptanceBtn];
}
-(void)foodViewjine
{
    calcView =[[UIView alloc]initWithFrame:CGRectMake(10, headerView.frame.size.height+headerView.frame.origin.y-10, self.bounds.size.width-20, 200)];
    [calcView setBackgroundColor:[UIColor whiteColor]];
    [calcView setAlpha:1];
    [self addSubview:calcView];
    
    inputMoney =[[UILabel alloc]initWithFrame:CGRectMake(6, 5, 75, 30)];
    [inputMoney setText:@"输入金额"];
    [inputMoney setFont:[UIFont systemFontOfSize:16.0f]];
    [inputMoney setTextColor:[UIColor blackColor]];
    [calcView addSubview:inputMoney];
    
    inputMoneyText= [[UITextField alloc]initWithFrame:CGRectMake(typeOfAcceptance.frame.size.width+10, 5,headerView.bounds.size.width-100, 30)];
    [inputMoneyText setPlaceholder:@"请填写诉讼费金额"];
    inputMoneyText.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:242.0/255.0 blue:244.0/255.0 alpha:1];
    [inputMoneyText setTextAlignment:NSTextAlignmentCenter];
    inputMoneyText .keyboardType=UIKeyboardTypeDecimalPad;
    [inputMoneyText setFont:[UIFont systemFontOfSize:16.0]];
    inputMoneyText.delegate =self;
    [calcView addSubview:inputMoneyText];
    
    
    calcBtn=[[UIButton alloc]initWithFrame:CGRectMake(typeOfAcceptance.frame.size.width-60, 60,calcView.frame.size.width/2-20, 30)];
    [calcBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [calcBtn setTitle:@"计算" forState:UIControlStateNormal];
    [calcBtn setBackgroundImage:[UIImage imageNamed:@"计算1"] forState:UIControlStateNormal];
    calcBtn.titleLabel.font =[UIFont systemFontOfSize:18.0f];
    [calcBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [calcBtn addTarget:self action:@selector(jisuanAction:) forControlEvents:UIControlEventTouchUpInside ];
    [calcView addSubview:calcBtn];
    
    resetBtn=[[UIButton alloc]initWithFrame:CGRectMake(calcView.bounds.size.width/2+10, 60, calcView.frame.size.width/2-20, 30)];
    [resetBtn setBackgroundImage:[UIImage imageNamed:@"重置2"] forState:UIControlStateNormal];
    resetBtn.titleLabel.font =[UIFont systemFontOfSize:18.0f];
    [resetBtn setTintColor:[UIColor  whiteColor]];
    [resetBtn setTitle:@"重置" forState:UIControlStateNormal];
    [resetBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [resetBtn addTarget:self action:@selector(chongzhiAction:) forControlEvents:UIControlEventTouchUpInside ];
    [calcView addSubview:resetBtn];
    
    
    calcResult =[[UILabel alloc]initWithFrame:CGRectMake(10, 110, 80, 30)];
    [calcResult setText:@"计算结果:"];
    [calcResult setFont:[UIFont systemFontOfSize:16.0f]];
    [calcResult setTextColor:[UIColor blackColor]];
    
    [calcView addSubview:calcResult];
    
    calcResultLabel =[[UILabel alloc]initWithFrame:CGRectMake(calcResult.frame.size.width, 110, 180, 30)];
    [calcResultLabel setText:@"0.00"];
    [calcResultLabel setTextColor:[UIColor redColor]];
    [calcResultLabel setFont:[UIFont systemFontOfSize:16.0f]];
    [calcResultLabel setTextColor:[UIColor redColor]];
    [calcResultLabel setBackgroundColor:[UIColor whiteColor]];
    [calcView addSubview:calcResultLabel];
    
}
- (NSString *)stringToAcross:(NSString *)str{
    NSArray *arr =[str componentsSeparatedByString:@"-"];
    NSMutableArray *mutbaleArr =[NSMutableArray array];
    for (NSString *tempStr in arr) {
        NSMutableString *mubtaleStr =[tempStr mutableCopy];
        NSUInteger i =mubtaleStr.length;
        NSUInteger j = 0;
        while (i >6) {
            [mubtaleStr insertString:@"," atIndex:(mubtaleStr.length -6)-4 *j];
            j++;
            i-=3;
        }
        [mutbaleArr addObject:mubtaleStr];
    }
    
    return [mutbaleArr componentsJoinedByString:@"-"];
}
- (NSString *)stringToDouble:(NSString *)str
{
    NSArray *arr= [str componentsSeparatedByString:@"."];
    NSMutableString *tempStr = [NSMutableString stringWithString:arr[0]];
    long count1 = tempStr.length%3 != 0?tempStr.length/3:tempStr.length/3-1;
    for (int i = 0; i < count1; i++) {
        [tempStr insertString:@"," atIndex:tempStr.length-(3*(i+1)+i)];
    }
    
    return [NSString stringWithFormat:@"%@%@%@",tempStr,@".",arr[1]];;
}

- (NSString*)applyExecute:(NSString *)arm
{
    if ([arm doubleValue]==0) {
        return @"50.00-500.00";
    }else if ([arm doubleValue]<=10000)
    {
        return @"50.00";
    }else if (10000<[arm doubleValue] &&[arm doubleValue]<=500000)
    {
        return [NSString stringWithFormat:@"%.2f",[arm doubleValue]*0.015-100];
    }else if( 500000<[arm doubleValue] &&[arm doubleValue]<5000000)
    {
        return [NSString stringWithFormat:@"%.2f",[arm doubleValue]*0.01+2400];
    }else if (5000000<[arm doubleValue] &&[arm doubleValue]<10000000)
    {
        return [NSString stringWithFormat:@"%.2f",[arm doubleValue]*0.005+27400];
    }else
    {
        return [NSString stringWithFormat:@"%.2f",[arm doubleValue]*0.001+67400];
    }
    
}
//申请保全：申请财产保全
- (NSString *)applyPreserve:(NSString *)arm
{
    if ([arm doubleValue]<=1000) {
        return @"30.00";
    }else if ([arm doubleValue]>1000 &&[arm doubleValue]<=100000)
    {
        return [NSString stringWithFormat:@"%.2f",[arm doubleValue]*0.01+20];
    }else if([arm doubleValue]>100000 &&[arm doubleValue]<=895999)
    {
        return  [NSString stringWithFormat:@"%.2f",1020+([arm doubleValue]-100000)*0.005];
    }else
    {
        return @"5000.00";
    }
}
//申请支付:申请支付令
- (NSString *)applyPayL:(NSString *)arm
{
    if ([arm doubleValue] <= 10000) {
        return @"16.67";
    }else if([arm doubleValue] > 10000 && [arm doubleValue]<=100000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.025 - 200)/3];
    }
    else if([arm doubleValue] > 100000 && [arm doubleValue]<=200000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.02 + 300)/3];
    }else if([arm doubleValue] > 200000 && [arm doubleValue]<=500000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.015 + 1300)/3];
    }else if ([arm doubleValue] > 500000 && [arm doubleValue]<=1000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.01 + 3800)/3];
    }
    else if([arm doubleValue] > 1000000 && [arm doubleValue]<=2000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.009 + 4800)/3];
    }else if([arm doubleValue] > 2000000 && [arm doubleValue]<=5000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.008) + 6800/3];
    }else if ([arm doubleValue] > 5000000 && [arm doubleValue]<=10000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.007 + 11800)/3];
    }else if ([arm doubleValue] > 10000000 && [arm doubleValue]<=20000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.006 + 21800)/3];
    }else
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.005 + 41800)/3];
    }
}
//申请公示催告:申请公示催告
- (NSString *)applyPubictyCG:(NSString *)arm
{
    return @"100.00";
}
//申请撤销仲裁裁决或者认定仲裁协议效力
- (NSString *)applyRecallArbitration:(NSString *)arm
{
    return  @"400.00";
}
//申请破产
- (NSString *)applyBankrupt:(NSString*)arm
{
    if ([arm doubleValue] <= 10000) {
        return @"25.00";
    } else if ( [arm doubleValue] > 10000 && [arm doubleValue]<=100000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.025 - 200)/2];
    } else if ([arm doubleValue] > 100000 && [arm doubleValue]<=200000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.02 + 300)/2];
    } else if ([arm doubleValue] > 200000 && [arm doubleValue]<=500000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.015 + 1300)/2];
    } else if ([arm doubleValue] > 500000 && [arm doubleValue]<=1000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.01 + 3800)/2];
    } else if ([arm doubleValue] > 1000000 && [arm doubleValue]<=2000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.009 + 4800)/2];
    } else if ([arm doubleValue] > 2000000 && [arm doubleValue]<=5000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.008 + 6800)/2];
    } else if ([arm doubleValue] > 5000000 && [arm doubleValue]<=10000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.007 + 11800)/2];
    } else if ([arm doubleValue] > 10000000 && [arm doubleValue]<=20000000)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] * 0.006 + 21800)/2];
    } else if ([arm doubleValue]>20000000 && [arm doubleValue]<=111639995)
    {
        return [NSString stringWithFormat:@"%.2f",([arm doubleValue] *0.005 +41800)/2];
    }else
    {
        return @"300000.00 ";
    }
}
//海事:申请设立海事赔偿责任限制基金
- (NSString *)applyEstablishMaritimeCompensat:(NSString *)arm
{
    return @"1000.00-10000.00";
}
//海事:申请海事强制令
- (NSString *)applyMaritimeInjunction:(NSString *)arm
{
    return @"1000.00-5000.00";
}
//海事:申请船舶优先权催告
- (NSString*)applyShipPriorityCG:(NSString *)arm
{
    return @"1000.00-5000.00";
}
//海事:申请海事责权登记
- (NSString *)applyMaritimeZQRegister:(NSString *)arm
{
    return @"1000.00";
}
//海事:申请共同海损理算
-(NSString *)applyCommonAverageAdjustment:(NSString*)arm
{
    return @"1000.00";
}
//判断字符串是否为空
- (BOOL)isEmptyString:(NSString *)string{
    if (string == nil) {
        return YES;
    }
    if (string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}
#pragma mark 计算结果

- (void)jisuanAction:(UIButton *)but
{
    // calcView.frame =CGRectMake(10, 60, self.view.bounds.size.width-20, 200);
    [self endEditing:YES];
    [headerView resignFirstResponder];
    NSString *type = [typeOfAcceptanceText text];
    NSString *arm = [inputMoneyText text];
    if ([self isEmptyString:arm]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入讼诉费用" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;}
    if       ([type isEqualToString:@"申请执行:申请执行"])
    {
//        [calcResultLabel setText:[self applyExecute:arm]];
        calcResultLabel.text =[self stringToDouble:[self applyExecute:arm]];
    }else if ([type isEqualToString:@"申请保全：申请财产保全"])
    {
//        [calcResultLabel setText:[self applyPreserve:arm]];
        calcResultLabel.text =[self stringToDouble:[self applyPreserve:arm]];
    }else if ([type isEqualToString:@"申请支付:申请支付令"])
    {
//        [calcResultLabel setText:[self applyPayL:arm]];
        calcResultLabel.text =[self stringToDouble:[self applyPayL:arm]];
    }else if ([type isEqualToString:@"申请公示催告:申请公示催告"])
    {
//        [calcResultLabel setText:[self applyPubictyCG:arm]];
        calcResultLabel.text =[self stringToDouble:[self applyPubictyCG:arm]];
    }else if ([type isEqualToString:@"申请撤销仲裁裁决或者认定仲裁协议效力"])
    {
//        [calcResultLabel setText:[self applyRecallArbitration:arm]];
        calcResultLabel.text =[self stringToDouble:[self applyRecallArbitration:arm]];
    }else if ([type isEqualToString:@"申请破产"])
    {
//        [calcResultLabel setText:[self applyBankrupt:arm]];
        calcResultLabel.text =[self stringToDouble:[self applyBankrupt:arm]];
    }else if ([type isEqualToString:@"海事:申请设立海事赔偿责任限制基金"])
    {
        calcResultLabel.text=[self stringToAcross:[self applyEstablishMaritimeCompensat:arm]];
    }else if ([type isEqualToString:@"海事:申请海事强制令"])
    {
        [calcResultLabel setText:[self applyMaritimeInjunction:arm]];
    }else if ([type isEqualToString:@"海事:申请海事船舶优先权催告"])
    {
        [calcResultLabel setText:[self applyShipPriorityCG:arm]];
    }else if ([type isEqualToString:@"海事:申请海事债权登记"])
    {
//        [calcResultLabel setText:[self applyMaritimeZQRegister:arm]];
        calcResultLabel.text =[self stringToDouble:[self applyMaritimeZQRegister:arm]];
    }else         {
//        [calcResultLabel setText:[self applyCommonAverageAdjustment:arm]];
        calcResultLabel.text =[self stringToDouble:[self applyCommonAverageAdjustment:arm]];
    }
}
- (void)chongzhiAction:(UIButton *)butt
{
    inputMoneyText.text =nil;
    calcResultLabel.text =[NSString stringWithFormat:@"0.00"];
}
- (void)typeOfAcceptanceBtnAction:(UIButton *)buta{
    if (chooseView.showTable)
    {   [UIView animateWithDuration:0.3 animations:^{
        calcView.frame =CGRectMake(10, headerView.frame.size.height-5, self.bounds.size.width-20, 200);
    }];
        mainView.contentSize = CGSizeMake(mainView.frame.size.width, 0);
        [chooseView dismiss];
    }else{
        
        [UIView animateWithDuration:0.3 animations:^{
            calcView.frame=CGRectMake(10, kZSFjuli, self.bounds.size.width-20, 200);
            
        }];
        mainView.contentSize = CGSizeMake(mainView.frame.size.width, 750);
        [chooseView show];
    }
    NSLog(@"%@",chooseView);
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    int record = (int)textField.text.length;
    if ([textField.text rangeOfString:@"."].location != NSNotFound && [string isEqualToString:@"."]){
        return NO;
    }
    //    if ([textField.text containsString:@"."] && [string isEqualToString:@"."]) {
    //        return NO;
    //    }
    if ([string isEqualToString:@"."] && record == 0) {
        return NO;
    }
    if (record == 1 && ![string isEqualToString:@"."] && [textField.text isEqualToString:@"0"] && ![string isEqualToString:@""]) {
        return NO;
    }
    return YES;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [inputMoneyText resignFirstResponder];
    [self resignFirstResponder];
    [headerView resignFirstResponder];
    [calcView resignFirstResponder];
}




@end
