//
//  DataBaseCenter.m
//  LvLvApp
//
//  Created by IOS8 on 15/10/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "DataBaseCenter.h"
#define FMDBQuickCheck(SomeBool) { if (!(SomeBool)) { NSLog(@"Failure on line %d", __LINE__);} }
@implementation DataBaseCenter

- (id)init
{
    return self;
}
- (BOOL)getDb
{
    NSString* filePath = [self dataFilePath];
    self.db = [[FMDatabase alloc] initWithPath:filePath];
    if (![self.db open]) {
        NSLog(@"open db false");
        return false;
    }
    [self.db setShouldCacheStatements:YES];
    self.queue = [FMDatabaseQueue databaseQueueWithPath:filePath];
    FMDBQuickCheck(self.queue);
    return true;

}
- (NSString *)dataFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"lvlv.sqlite"];
}
// 创建表
- (void)createTables
{
    [self.db executeUpdate:@"create table if not exists PBCInterest(S_ID integer primary key,S_LiLv blob)"];
    [self.db executeUpdate:@"create table if not exists InsuranceFee(S_CITY text primary key,S_PerTax blob)"];
    [self.db executeUpdate:@"create table if not exists InsuranceInitial(LocalAmount double,ForeignAmount double)"];
}

#pragma mark 更新人民银行利率
- (void)searchBankData:(NSString *)iDProperty and:(BankData *)lilv
{
    if (!iDProperty)
        return;
    [self.queue inDatabase:^(FMDatabase *db) {
        NSData *temp = [NSKeyedArchiver archivedDataWithRootObject:lilv];
        FMResultSet *rs = [self.db executeQuery:@"select * from PBCInterest where S_ID = ?",iDProperty];
        if([rs next]) {
            [self.db executeUpdate:@"update PBCInterest set S_LiLv = ? where S_ID = ?",temp,iDProperty];
        }
        else
        {
            [self.db executeUpdate:@"insert into PBCInterest (S_ID,S_LiLv) values (?,?)" ,iDProperty,temp];
        }
        [rs close];
    }];
}

#pragma mark 更新各地社保公积金
- (void)searchPerTaxData:(NSString *)cityName and:(TaxData *)perTax
{
    if (!cityName)
        return;
    [self.queue inDatabase:^(FMDatabase *db) {
        NSData *temp = [NSKeyedArchiver archivedDataWithRootObject:perTax];
        FMResultSet *rs = [self.db executeQuery:@"select * from InsuranceFee where S_CITY = ?",cityName];
        if([rs next]) {
            [self.db executeUpdate:@"update InsuranceFee set S_PerTax = ? where S_CITY = ?",temp,cityName];
        }
        else
        {
            [self.db executeUpdate:@"insert into InsuranceFee (S_CITY,S_PerTax) values (?,?)" ,cityName,temp];
        }
        [rs close];
    }];
}

#pragma mark 国内外个税起征点
- (void)searchTaxThreshold:(NSString *)LocalAmount and:(NSString *)ForeignAmount
{
    if (!LocalAmount && !ForeignAmount)
        return;
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [self.db executeQuery:@"select * from InsuranceInitial"];
        if([rs next]) {
            [self.db executeUpdate:@"update InsuranceInitial set LocalAmount = ? ,ForeignAmount = ?",LocalAmount,ForeignAmount];
        }
        else
        {
            [self.db executeUpdate:@"insert into InsuranceInitial (LocalAmount,ForeignAmount) values (?,?)" ,LocalAmount,ForeignAmount];
        }
        [rs close];
    }];

}

#pragma mark 查询人民银行利率数据
- (void)getBankDataWithDay:(NSMutableArray *)list
{
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [self.db executeQuery:@"select S_LiLv from PBCInterest"];
        while([rs next])
        {
            NSData *dic = [rs dataForColumn:@"S_LiLv"];
            
            if (dic) {
                BankData *info= [NSKeyedUnarchiver unarchiveObjectWithData:dic];
                [list addObject:info];
                info = nil;
            }
        }
        [rs close];
    }];
}

#pragma mark 查询个税利率数据
- (PersonalTax *)getPerTaxWithCity:(NSString *)city
{
    __block PersonalTax *tax = nil;
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [self.db executeQuery:@"select S_PerTax from InsuranceFee where S_CITY = ?",city];
        while([rs next])
        {
            NSData *dic = [rs dataForColumn:@"S_PerTax"];
            
            if (dic) {
                TaxData *info= [NSKeyedUnarchiver unarchiveObjectWithData:dic];
//                [list addObject:info];
                
                tax = [[PersonalTax alloc] init];         
                
                tax.cityName = info.cityName;
                tax.PerRetire = info.perRetire;
                tax.PerMediccal = info.perMediccal;
                tax.PerJobless = info.perJobless;
                tax.PerInjury = info.perInjury;
                tax.PerBear = info.perBear;
                tax.PerHouse = info.perHouse;
                tax.ComRetire = info.comRetire;
                tax.ComMedical = info.comMedical;
                tax.ComJobless = info.comJobless;
                tax.ComInjury = info.comInjury;
                tax.ComBear = info.comBear;
                tax.ComHouse = info.comHouse;
                tax.InsuranceMaxAmount = info.insuranceMaxAmount;
                tax.InsuranceMinAmount = info.insuranceMinAmount;
                tax.HouseMaxAmount = info.houseMaxAmount;
                tax.HouseMinAmount = info.houseMinAmount;
                tax.OtherAmount = info.otherAmount;
                info = nil;
            }
        }
        [rs close];
    }];
    return tax;
}


#pragma mark 查询个个税所有的数据的城市名称
- (void)getPersonallncomeTax:(NSMutableArray *)list
{
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [self.db executeQuery:@"select S_PerTax from InsuranceFee;"];
        while([rs next])
        {
            NSData *dic = [rs dataForColumn:@"S_PerTax"];
            
            if (dic) {
                TaxData *taxData= [NSKeyedUnarchiver unarchiveObjectWithData:dic];
                [list addObject:taxData];
                taxData = nil;
            }
        }
        [rs close];
    }];
}

- (NSMutableArray *)getTaxThresholdData
{
    __block NSMutableArray *arr = nil;
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [self.db executeQuery:@"select * from InsuranceInitial"];
        if([rs next])
        {
            arr = [[NSMutableArray alloc] init];
            [arr addObject:[NSString stringWithFormat:@"%.2f",[rs doubleForColumn:@"LocalAmount"]]];
            [arr addObject:[NSString stringWithFormat:@"%.2f",[rs doubleForColumn:@"ForeignAmount"]]];
        }
    }];
    return arr;
}
@end
