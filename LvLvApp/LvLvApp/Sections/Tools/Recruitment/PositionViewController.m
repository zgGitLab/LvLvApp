//
//  PositionViewController.m
//  LvLvApp
//
//  Created by Mark on 16/3/25.
//  Copyright © 2016年 gyy. All rights reserved.
//
#define UIscK  [[UIScreen mainScreen]bounds].size.width
#define UIscH  [[UIScreen mainScreen]bounds].size.height
#import "PositionViewController.h"

@interface PositionViewController ()

@end

@implementation PositionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor whiteColor];
    self.title =@"发布职位";
    
    UIImageView *PositionImage =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, UIscK, UIscH)];
    
    [self.view addSubview:PositionImage];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden =NO;
    UIBarButtonItem *leftItem =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"返回箭头"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    
    self.navigationItem.leftBarButtonItem=leftItem;


}

- (void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
