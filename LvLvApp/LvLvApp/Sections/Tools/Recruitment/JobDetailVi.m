//
//  JobDetailVi.m
//  LvLvApp
//
//  Created by Sunny on 16/3/30.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "JobDetailVi.h"

@implementation JobDetailVi


- (JobDetailVi *)loadVi
{
    return [[NSBundle mainBundle] loadNibNamed:@"JobDetailVi" owner:self options:nil][0];
}
- (void)awakeFromNib {
    self.frame = CGRectMake(0, 0, SIZE.width, 225);
}

- (void)getAllDataWithModel:(JbDetailModel *)Model
{
    [self.icon sd_setImageWithURL:[NSURL URLWithString:Model.ImageLogo] placeholderImage:[UIImage imageNamed:@"logos"]];
    _jobName.text = Model.JobName;
    self.companyName.text = Model.CompanyName;
    self.salary.text = Model.Salary;
    self.experience.text = Model.Experience;
    self.Location.text = Model.AddressLocation;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
