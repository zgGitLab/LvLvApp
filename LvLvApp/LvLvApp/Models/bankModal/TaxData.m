//
//  TaxData.m
//
//  Created by IOS8  on 15/10/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "TaxData.h"


NSString *const kTaxDataPerInjury          = @"PerInjury";
NSString *const kTaxDataPerJobless         = @"PerJobless";
NSString *const kTaxDataHouseMinAmount     = @"HouseMinAmount";
NSString *const kTaxDataCityName           = @"CityName";
NSString *const kTaxDataComJobless         = @"ComJobless";
NSString *const kTaxDataInsuranceMinAmount = @"InsuranceMinAmount";
NSString *const kTaxDataComMedical         = @"ComMedical";
NSString *const kTaxDataComHouse           = @"ComHouse";
NSString *const kTaxDataPerHouse           = @"PerHouse";
NSString *const kTaxDataComRetire          = @"ComRetire";
NSString *const kTaxDataCityZimu           = @"CityZimu";
NSString *const kTaxDataPerMediccal        = @"PerMediccal";
NSString *const kTaxDataComBear            = @"ComBear";
NSString *const kTaxDataPerRetire          = @"PerRetire";
NSString *const kTaxDataOtherAmount        = @"OtherAmount";
NSString *const kTaxDataHouseMaxAmount     = @"HouseMaxAmount";
NSString *const kTaxDataID                 = @"ID";
NSString *const kTaxDataInsuranceMaxAmount = @"InsuranceMaxAmount";
NSString *const kTaxDataComInjury          = @"ComInjury";
NSString *const kTaxDataPerBear            = @"PerBear";


@interface TaxData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TaxData

@synthesize perInjury          = _perInjury;
@synthesize perJobless         = _perJobless;
@synthesize houseMinAmount     = _houseMinAmount;
@synthesize cityName           = _cityName;
@synthesize comJobless         = _comJobless;
@synthesize insuranceMinAmount = _insuranceMinAmount;
@synthesize comMedical         = _comMedical;
@synthesize comHouse           = _comHouse;
@synthesize perHouse           = _perHouse;
@synthesize comRetire          = _comRetire;
@synthesize cityZimu           = _cityZimu;
@synthesize perMediccal        = _perMediccal;
@synthesize comBear            = _comBear;
@synthesize perRetire          = _perRetire;
@synthesize otherAmount        = _otherAmount;
@synthesize houseMaxAmount     = _houseMaxAmount;
@synthesize iDProperty         = _iDProperty;
@synthesize insuranceMaxAmount = _insuranceMaxAmount;
@synthesize comInjury          = _comInjury;
@synthesize perBear            = _perBear;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.perInjury          = [[self objectOrNilForKey:kTaxDataPerInjury fromDictionary:dict] doubleValue];
            self.perJobless         = [[self objectOrNilForKey:kTaxDataPerJobless fromDictionary:dict] doubleValue];
            self.houseMinAmount     = [[self objectOrNilForKey:kTaxDataHouseMinAmount fromDictionary:dict] doubleValue];
            self.cityName           = [self objectOrNilForKey:kTaxDataCityName fromDictionary:dict];
            self.comJobless         = [[self objectOrNilForKey:kTaxDataComJobless fromDictionary:dict] doubleValue];
            self.insuranceMinAmount = [[self objectOrNilForKey:kTaxDataInsuranceMinAmount fromDictionary:dict] doubleValue];
            self.comMedical         = [[self objectOrNilForKey:kTaxDataComMedical fromDictionary:dict] doubleValue];
            self.comHouse           = [[self objectOrNilForKey:kTaxDataComHouse fromDictionary:dict] doubleValue];
            self.perHouse           = [[self objectOrNilForKey:kTaxDataPerHouse fromDictionary:dict] doubleValue];
            self.comRetire          = [[self objectOrNilForKey:kTaxDataComRetire fromDictionary:dict] doubleValue];
            self.cityZimu           = [self objectOrNilForKey:kTaxDataCityZimu fromDictionary:dict];
            self.perMediccal        = [[self objectOrNilForKey:kTaxDataPerMediccal fromDictionary:dict] doubleValue];
            self.comBear            = [[self objectOrNilForKey:kTaxDataComBear fromDictionary:dict] doubleValue];
            self.perRetire          = [[self objectOrNilForKey:kTaxDataPerRetire fromDictionary:dict] doubleValue];
            self.otherAmount        = [[self objectOrNilForKey:kTaxDataOtherAmount fromDictionary:dict] doubleValue];
            self.houseMaxAmount     = [[self objectOrNilForKey:kTaxDataHouseMaxAmount fromDictionary:dict] doubleValue];
            self.iDProperty         = [[self objectOrNilForKey:kTaxDataID fromDictionary:dict] doubleValue];
            self.insuranceMaxAmount = [[self objectOrNilForKey:kTaxDataInsuranceMaxAmount fromDictionary:dict] doubleValue];
            self.comInjury          = [[self objectOrNilForKey:kTaxDataComInjury fromDictionary:dict] doubleValue];
            self.perBear            = [[self objectOrNilForKey:kTaxDataPerBear fromDictionary:dict] doubleValue];

    }
    AppDelegate *app =[UIApplication sharedApplication].delegate;
    [app.db searchPerTaxData:self.cityName and:self];
    NSLog(@"%@",app.db);
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.perInjury] forKey:kTaxDataPerInjury];
    [mutableDict setValue:[NSNumber numberWithDouble:self.perJobless] forKey:kTaxDataPerJobless];
    [mutableDict setValue:[NSNumber numberWithDouble:self.houseMinAmount] forKey:kTaxDataHouseMinAmount];
    [mutableDict setValue:self.cityName forKey:kTaxDataCityName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.comJobless] forKey:kTaxDataComJobless];
    [mutableDict setValue:[NSNumber numberWithDouble:self.insuranceMinAmount] forKey:kTaxDataInsuranceMinAmount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.comMedical] forKey:kTaxDataComMedical];
    [mutableDict setValue:[NSNumber numberWithDouble:self.comHouse] forKey:kTaxDataComHouse];
    [mutableDict setValue:[NSNumber numberWithDouble:self.perHouse] forKey:kTaxDataPerHouse];
    [mutableDict setValue:[NSNumber numberWithDouble:self.comRetire] forKey:kTaxDataComRetire];
    [mutableDict setValue:self.cityZimu forKey:kTaxDataCityZimu];
    [mutableDict setValue:[NSNumber numberWithDouble:self.perMediccal] forKey:kTaxDataPerMediccal];
    [mutableDict setValue:[NSNumber numberWithDouble:self.comBear] forKey:kTaxDataComBear];
    [mutableDict setValue:[NSNumber numberWithDouble:self.perRetire] forKey:kTaxDataPerRetire];
    [mutableDict setValue:[NSNumber numberWithDouble:self.otherAmount] forKey:kTaxDataOtherAmount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.houseMaxAmount] forKey:kTaxDataHouseMaxAmount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kTaxDataID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.insuranceMaxAmount] forKey:kTaxDataInsuranceMaxAmount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.comInjury] forKey:kTaxDataComInjury];
    [mutableDict setValue:[NSNumber numberWithDouble:self.perBear] forKey:kTaxDataPerBear];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.perInjury          = [aDecoder decodeDoubleForKey:kTaxDataPerInjury];
    self.perJobless         = [aDecoder decodeDoubleForKey:kTaxDataPerJobless];
    self.houseMinAmount     = [aDecoder decodeDoubleForKey:kTaxDataHouseMinAmount];
    self.cityName           = [aDecoder decodeObjectForKey:kTaxDataCityName];
    self.comJobless         = [aDecoder decodeDoubleForKey:kTaxDataComJobless];
    self.insuranceMinAmount = [aDecoder decodeDoubleForKey:kTaxDataInsuranceMinAmount];
    self.comMedical         = [aDecoder decodeDoubleForKey:kTaxDataComMedical];
    self.comHouse           = [aDecoder decodeDoubleForKey:kTaxDataComHouse];
    self.perHouse           = [aDecoder decodeDoubleForKey:kTaxDataPerHouse];
    self.comRetire          = [aDecoder decodeDoubleForKey:kTaxDataComRetire];
    self.cityZimu           = [aDecoder decodeObjectForKey:kTaxDataCityZimu];
    self.perMediccal        = [aDecoder decodeDoubleForKey:kTaxDataPerMediccal];
    self.comBear            = [aDecoder decodeDoubleForKey:kTaxDataComBear];
    self.perRetire          = [aDecoder decodeDoubleForKey:kTaxDataPerRetire];
    self.otherAmount        = [aDecoder decodeDoubleForKey:kTaxDataOtherAmount];
    self.houseMaxAmount     = [aDecoder decodeDoubleForKey:kTaxDataHouseMaxAmount];
    self.iDProperty         = [aDecoder decodeDoubleForKey:kTaxDataID];
    self.insuranceMaxAmount = [aDecoder decodeDoubleForKey:kTaxDataInsuranceMaxAmount];
    self.comInjury          = [aDecoder decodeDoubleForKey:kTaxDataComInjury];
    self.perBear            = [aDecoder decodeDoubleForKey:kTaxDataPerBear];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_perInjury forKey:kTaxDataPerInjury];
    [aCoder encodeDouble:_perJobless forKey:kTaxDataPerJobless];
    [aCoder encodeDouble:_houseMinAmount forKey:kTaxDataHouseMinAmount];
    [aCoder encodeObject:_cityName forKey:kTaxDataCityName];
    [aCoder encodeDouble:_comJobless forKey:kTaxDataComJobless];
    [aCoder encodeDouble:_insuranceMinAmount forKey:kTaxDataInsuranceMinAmount];
    [aCoder encodeDouble:_comMedical forKey:kTaxDataComMedical];
    [aCoder encodeDouble:_comHouse forKey:kTaxDataComHouse];
    [aCoder encodeDouble:_perHouse forKey:kTaxDataPerHouse];
    [aCoder encodeDouble:_comRetire forKey:kTaxDataComRetire];
    [aCoder encodeObject:_cityZimu forKey:kTaxDataCityZimu];
    [aCoder encodeDouble:_perMediccal forKey:kTaxDataPerMediccal];
    [aCoder encodeDouble:_comBear forKey:kTaxDataComBear];
    [aCoder encodeDouble:_perRetire forKey:kTaxDataPerRetire];
    [aCoder encodeDouble:_otherAmount forKey:kTaxDataOtherAmount];
    [aCoder encodeDouble:_houseMaxAmount forKey:kTaxDataHouseMaxAmount];
    [aCoder encodeDouble:_iDProperty forKey:kTaxDataID];
    [aCoder encodeDouble:_insuranceMaxAmount forKey:kTaxDataInsuranceMaxAmount];
    [aCoder encodeDouble:_comInjury forKey:kTaxDataComInjury];
    [aCoder encodeDouble:_perBear forKey:kTaxDataPerBear];
}

- (id)copyWithZone:(NSZone *)zone
{
    TaxData *copy = [[TaxData alloc] init];
    
    if (copy) {

        copy.perInjury          = self.perInjury;
        copy.perJobless         = self.perJobless;
        copy.houseMinAmount     = self.houseMinAmount;
        copy.cityName           = [self.cityName copyWithZone:zone];
        copy.comJobless         = self.comJobless;
        copy.insuranceMinAmount = self.insuranceMinAmount;
        copy.comMedical         = self.comMedical;
        copy.comHouse           = self.comHouse;
        copy.perHouse           = self.perHouse;
        copy.comRetire          = self.comRetire;
        copy.cityZimu           = [self.cityZimu copyWithZone:zone];
        copy.perMediccal        = self.perMediccal;
        copy.comBear            = self.comBear;
        copy.perRetire          = self.perRetire;
        copy.otherAmount        = self.otherAmount;
        copy.houseMaxAmount     = self.houseMaxAmount;
        copy.iDProperty         = self.iDProperty;
        copy.insuranceMaxAmount = self.insuranceMaxAmount;
        copy.comInjury          = self.comInjury;
        copy.perBear            = self.perBear;
    }
    
    return copy;
}


@end
