//
//  S_PrivateLetterData.m
//
//  Created by IOS8  on 15/12/22
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_PrivateLetterData.h"


NSString *const kS_PrivateLetterDataID = @"ID";
NSString *const kS_PrivateLetterDataIntroduction = @"Introduction";
NSString *const kS_PrivateLetterDataIsFocus = @"IsFocus";
NSString *const kS_PrivateLetterDataNickName = @"NickName";
NSString *const kS_PrivateLetterDataImage = @"Image";


@interface S_PrivateLetterData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_PrivateLetterData

@synthesize iDProperty = _iDProperty;
@synthesize introduction = _introduction;
@synthesize isFocus = _isFocus;
@synthesize nickName = _nickName;
@synthesize image = _image;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kS_PrivateLetterDataID fromDictionary:dict] doubleValue];
            self.introduction = [self objectOrNilForKey:kS_PrivateLetterDataIntroduction fromDictionary:dict];
            self.isFocus = [[self objectOrNilForKey:kS_PrivateLetterDataIsFocus fromDictionary:dict] doubleValue];
            self.nickName = [self objectOrNilForKey:kS_PrivateLetterDataNickName fromDictionary:dict];
            self.image = [self objectOrNilForKey:kS_PrivateLetterDataImage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kS_PrivateLetterDataID];
    [mutableDict setValue:self.introduction forKey:kS_PrivateLetterDataIntroduction];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isFocus] forKey:kS_PrivateLetterDataIsFocus];
    [mutableDict setValue:self.nickName forKey:kS_PrivateLetterDataNickName];
    [mutableDict setValue:self.image forKey:kS_PrivateLetterDataImage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kS_PrivateLetterDataID];
    self.introduction = [aDecoder decodeObjectForKey:kS_PrivateLetterDataIntroduction];
    self.isFocus = [aDecoder decodeDoubleForKey:kS_PrivateLetterDataIsFocus];
    self.nickName = [aDecoder decodeObjectForKey:kS_PrivateLetterDataNickName];
    self.image = [aDecoder decodeObjectForKey:kS_PrivateLetterDataImage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kS_PrivateLetterDataID];
    [aCoder encodeObject:_introduction forKey:kS_PrivateLetterDataIntroduction];
    [aCoder encodeDouble:_isFocus forKey:kS_PrivateLetterDataIsFocus];
    [aCoder encodeObject:_nickName forKey:kS_PrivateLetterDataNickName];
    [aCoder encodeObject:_image forKey:kS_PrivateLetterDataImage];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_PrivateLetterData *copy = [[S_PrivateLetterData alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.introduction = [self.introduction copyWithZone:zone];
        copy.isFocus = self.isFocus;
        copy.nickName = [self.nickName copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
    }
    
    return copy;
}


@end
