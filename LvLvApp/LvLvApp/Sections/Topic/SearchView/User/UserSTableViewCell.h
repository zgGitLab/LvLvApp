//
//  UserSTableViewCell.h
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserSTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *HeadPortrait;
@property (strong, nonatomic) IBOutlet UILabel *UserNames;
@property (strong, nonatomic) IBOutlet UILabel *UserIdograph;

@end
