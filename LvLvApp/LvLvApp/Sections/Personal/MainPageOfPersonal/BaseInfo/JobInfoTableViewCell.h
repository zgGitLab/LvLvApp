//
//  JobInfoTableViewCell.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *jobTime;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *jobName;
@property (weak, nonatomic) IBOutlet UILabel *areaName;

@end
