//
//  LBData.m
//
//  Created by hope  on 15/12/1
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "LBData.h"


NSString *const kLBDataTagID = @"TagID";
NSString *const kLBDataCommentCount = @"CommentCount";
NSString *const kLBDataIsFocus = @"IsFocus";
NSString *const kLBDataQuestionCount = @"QuestionCount";
NSString *const kLBDataTagName = @"TagName";
NSString *const kLBDataFocusTag = @"FocusTag";


@interface LBData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LBData

@synthesize tagID = _tagID;
@synthesize commentCount = _commentCount;
@synthesize isFocus = _isFocus;
@synthesize questionCount = _questionCount;
@synthesize tagName = _tagName;
@synthesize focusTag = _focusTag;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tagID = [[self objectOrNilForKey:kLBDataTagID fromDictionary:dict] doubleValue];
            self.commentCount = [[self objectOrNilForKey:kLBDataCommentCount fromDictionary:dict] doubleValue];
            self.isFocus = [[self objectOrNilForKey:kLBDataIsFocus fromDictionary:dict] doubleValue];
            self.questionCount = [[self objectOrNilForKey:kLBDataQuestionCount fromDictionary:dict] doubleValue];
            self.tagName = [self objectOrNilForKey:kLBDataTagName fromDictionary:dict];
            self.focusTag = [[self objectOrNilForKey:kLBDataFocusTag fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tagID] forKey:kLBDataTagID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kLBDataCommentCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isFocus] forKey:kLBDataIsFocus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.questionCount] forKey:kLBDataQuestionCount];
    [mutableDict setValue:self.tagName forKey:kLBDataTagName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.focusTag] forKey:kLBDataFocusTag];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tagID = [aDecoder decodeDoubleForKey:kLBDataTagID];
    self.commentCount = [aDecoder decodeDoubleForKey:kLBDataCommentCount];
    self.isFocus = [aDecoder decodeDoubleForKey:kLBDataIsFocus];
    self.questionCount = [aDecoder decodeDoubleForKey:kLBDataQuestionCount];
    self.tagName = [aDecoder decodeObjectForKey:kLBDataTagName];
    self.focusTag = [aDecoder decodeDoubleForKey:kLBDataFocusTag];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_tagID forKey:kLBDataTagID];
    [aCoder encodeDouble:_commentCount forKey:kLBDataCommentCount];
    [aCoder encodeDouble:_isFocus forKey:kLBDataIsFocus];
    [aCoder encodeDouble:_questionCount forKey:kLBDataQuestionCount];
    [aCoder encodeObject:_tagName forKey:kLBDataTagName];
    [aCoder encodeDouble:_focusTag forKey:kLBDataFocusTag];
}

- (id)copyWithZone:(NSZone *)zone
{
    LBData *copy = [[LBData alloc] init];
    
    if (copy) {

        copy.tagID = self.tagID;
        copy.commentCount = self.commentCount;
        copy.isFocus = self.isFocus;
        copy.questionCount = self.questionCount;
        copy.tagName = [self.tagName copyWithZone:zone];
        copy.focusTag = self.focusTag;
    }
    
    return copy;
}


@end
