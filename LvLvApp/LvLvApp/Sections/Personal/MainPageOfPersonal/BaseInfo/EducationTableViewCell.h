//
//  EducationTableViewCell.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EducationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *startSchool;
@property (weak, nonatomic) IBOutlet UILabel *schoolName;
@property (weak, nonatomic) IBOutlet UILabel *institute;

@end
