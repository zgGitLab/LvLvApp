//
//  M_Data.h
//
//  Created by jim  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface M_Data : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *listAll;
@property (nonatomic, assign) double pagesize;
@property (nonatomic, assign) double pageindex;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
