//
//  M_BaseClass.m
//
//  Created by jim  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_BaseClass.h"
#import "M_Data.h"


NSString *const kM_BaseClassCode = @"Code";
NSString *const kM_BaseClassMessage = @"Message";
NSString *const kM_BaseClassData = @"Data";


@interface M_BaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_BaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_BaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_BaseClassMessage fromDictionary:dict];
            self.data = [M_Data modelObjectWithDictionary:[dict objectForKey:kM_BaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_BaseClassCode];
    [mutableDict setValue:self.message forKey:kM_BaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kM_BaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_BaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_BaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_BaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_BaseClassCode];
    [aCoder encodeObject:_message forKey:kM_BaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_BaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_BaseClass *copy = [[M_BaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
