//
//  AnswerAllData.h
//
//  Created by hope  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface AnswerAllData : NSObject

@property (nonatomic, strong) NSArray *dt;
@property (nonatomic, strong) NSString *dscrption;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *focusID;
@property (nonatomic, strong) NSString *answerID;
@property (nonatomic, assign) double answerTotalCount;
@property (nonatomic, strong) NSString *questionContent;
@property (nonatomic, strong) NSArray *list;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *content;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
//- (NSDictionary *)dictionaryRepresentation;

@end
