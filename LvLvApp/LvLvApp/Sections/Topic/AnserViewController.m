//
//  AnserViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/11.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "AnserViewController.h"
#import "PerCollectTableViewCell.h"
#import "S_MyAnserTableViewCell.h"
#import "GambitAnswerViewController.h"
#import "MyTapGestrpueReco.h"
#import "AnswerDetailViewController.h"
@interface AnserViewController ()
{
    NSMutableArray *dataArray;
    int page;
}
@end

@implementation AnserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray array];
    self.title = [NSString stringWithFormat:@"%@的回答",self.titleStr];
    self.ansetTab.separatorInset = UIEdgeInsetsMake(0, 5, 0, 8);
    self.ansetTab.tableFooterView = [[UIView alloc] init];
     __weak AnserViewController *VC = self;
    // 上拉加载
    self.ansetTab.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 结束刷新
        [VC infinite];
    }];
    self.ansetTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        page = 1;
         NSString *pageIndex = [NSString stringWithFormat:@"%d",page];
        [VC requestAnserData:pageIndex];
    }];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    page = 1;
    [self requestAnserData:@"1"];
}

- (void)requestAnserData:(NSString *)pageIndex
{
    __weak AnserViewController *VC = self;

    NSString *url = [NSString stringWithFormat:KMyAnswer,K_IP];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:self.userID forKey:@"UserID"];
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:[NSNumber numberWithInteger:K_PAGESIZE] forKey:@"PageSize"];
    [LVHTTPRequestTool post:url params:parameters success:^(id json) {
        [VC.ansetTab.mj_header endRefreshing];
        [VC.ansetTab.mj_footer endRefreshing];
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_MyAnserBaseClass *myAnser = [[S_MyAnserBaseClass alloc] initWithDictionary:dic];
            if ([pageIndex isEqualToString:@"1"]) {
                [dataArray removeAllObjects];
                dataArray = [NSMutableArray arrayWithArray:myAnser.data];
            }else{
                [dataArray addObjectsFromArray:myAnser.data];
            }
            
            [VC performSelectorOnMainThread:@selector(refreshAnserUI:) withObject:myAnser.data waitUntilDone:YES];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)infinite
{
    page += 1;
     NSString *pageIndex = [NSString stringWithFormat:@"%d",page];
    [self requestAnserData:pageIndex];
}

- (void)refreshAnserUI:(NSArray *)modal
{
    [self.ansetTab reloadData];
}

#pragma mark UItableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"anser";
    S_MyAnserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[S_MyAnserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.nicLab.userInteractionEnabled = YES;
        cell.titleLab.userInteractionEnabled = YES;
        cell.contentLab.userInteractionEnabled = YES;
    }
    S_MyAnserData *anserModel = dataArray[indexPath.row];
    MyTapGestrpueReco *titTapGes = [[MyTapGestrpueReco alloc] initWithTarget:self action:@selector(titClick:)];
    titTapGes.TID = anserModel.tID;
    titTapGes.tUserID = anserModel.TUserID;
    titTapGes.AnserID = anserModel.answerID;
    [cell.titleLab addGestureRecognizer:titTapGes];
    MyTapGestrpueReco *conTapGes = [[MyTapGestrpueReco alloc] initWithTarget:self action:@selector(conClick:)];
    conTapGes.TID = anserModel.tID;
    conTapGes.UserID = (int)anserModel.userid;
    conTapGes.tUserID = anserModel.TUserID;
    conTapGes.AnserID = anserModel.answerID;;
    [cell.contentLab addGestureRecognizer:conTapGes];
    
    UIImageView *imag = (UIImageView *)[cell.contentView viewWithTag:1000];
    [imag removeFromSuperview];
    
    if (anserModel.answerContent != nil) {
        anserModel.answerContent = [Utils replaceImageString:anserModel.answerContent];
    }
    if (![anserModel.logoUrl isEqualToString:@""]) {
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(SIZE.width - 80-15, 30, 80, 80)];
        [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,anserModel.logoUrl]]];
        image.tag = 1000;
        [cell.contentView addSubview:image];
        
//        NSLog(@"%@",[NSString stringWithFormat:@"http://%@/%@",K_IP,anserModel.logoUrl]);
//        image.image= [UIImage imageNamed:@"头像"];
    }
    cell.nicLab.text = anserModel.nickname;
    cell.anserTimeLab.text = anserModel.times;
    cell.titleLab.attributedText = [self attributedStringWithString:anserModel.title];
    cell.contentLab.attributedText = [self conAttributedStringWithString:anserModel.answerContent];
    cell.otherLab.text = [NSString stringWithFormat:@"赞同 %.0f · 评论 %.0f · 收藏 %.0f",anserModel.counts,anserModel.commentCount,anserModel.collectCount];
    
    // 计算昵称
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:11] , NSFontAttributeName, nil];
    CGFloat nicWith = [anserModel.nickname sizeWithAttributes:dictionary].width;
    cell.nicLab.frame = CGRectMake(SPACE, 15, nicWith, 15);
    // 计算回答时间
    NSDictionary *ansdictionary = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:11] , NSFontAttributeName, nil];
    CGFloat ansWith = [anserModel.times sizeWithAttributes:ansdictionary].width;
    cell.anserTimeLab.frame = CGRectMake(CGRectGetMaxX(cell.nicLab.frame)+10, 15, ansWith, 15);
    // 计算标题；
    if ([anserModel.logoUrl isEqualToString:@""]) {// 图片不存在

        CGSize titleSize = [self getSize:cell.titleLab.attributedText andWith:SIZE.width - 2*SPACE];
        titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
        cell.titleLab.frame = CGRectMake(SPACE, 35, SIZE.width-2*SPACE, titleSize.height);

        CGSize conSize = [self getSize:cell.contentLab.attributedText andWith:SIZE.width - 2*SPACE];
        conSize.height = conSize.height>ConHeight?ConHeight:conSize.height;
        cell.contentLab.frame = CGRectMake(SPACE, 35+titleSize.height+5, SIZE.width-2*SPACE, conSize.height);
        if ([anserModel.answerContent isEqualToString:@""]) {
            cell.otherLab.frame = CGRectMake(SPACE, CGRectGetMaxY(cell.titleLab.frame)+5, SIZE.width-2*SPACE, 15);
        }else{
            cell.otherLab.frame = CGRectMake(SPACE, CGRectGetMaxY(cell.contentLab.frame)+5, SIZE.width-2*SPACE, 15);
        }
//        cell.otherLab.frame  = CGRectMake(SPACE, CGRectGetMaxY(cell.contentLab.frame)+5, SIZE.width-2*SPACE, 15);
        
    }else{// 有图片

        CGSize titleSize = [self getSize:cell.titleLab.attributedText andWith:SIZE.width - (3*15+80)];
        titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
        cell.titleLab.frame = CGRectMake(SPACE, 35, SIZE.width-(3*15+80), titleSize.height);
        
        CGSize conSize = [self getSize:cell.contentLab.attributedText andWith:SIZE.width - (3*15+80)];
        conSize.height = conSize.height>ConHeight?ConHeight:conSize.height;
        cell.contentLab.frame = CGRectMake(SPACE, CGRectGetMaxY(cell.titleLab.frame)+5, SIZE.width-(3*15+80), conSize.height);
        if ([anserModel.answerContent isEqualToString:@""]) {
            cell.otherLab.frame = CGRectMake(SPACE, CGRectGetMaxY(cell.titleLab.frame)+5, SIZE.width-2*SPACE, 15);
        }else{
            cell.otherLab.frame = CGRectMake(SPACE, CGRectGetMaxY(cell.contentLab.frame)+5, SIZE.width-2*SPACE, 15);
        }
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    S_MyAnserData *anserModel = dataArray[indexPath.row];
    if (anserModel.answerContent != nil) {
         anserModel.answerContent = [Utils replaceImageString:anserModel.answerContent];
    }
   
    
    CGFloat cellHeight;
    // 计算标题；
    if ([anserModel.logoUrl isEqualToString:@""]) {
        
        CGSize titleSize = [self getSize:[self attributedStringWithString:anserModel.title] andWith:SIZE.width - 2*SPACE];
        titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
        CGRect titRec = CGRectMake(SPACE, 35, SIZE.width-2*SPACE, titleSize.height);
        
        CGSize conSize = [self getSize:[self attributedStringWithString:anserModel.answerContent] andWith:SIZE.width-2*SPACE];
        conSize.height = conSize.height>ConHeight?ConHeight:conSize.height;
        CGRect conRec = CGRectMake(SPACE, CGRectGetMaxY(titRec)+5, SIZE.width-2*SPACE, conSize.height);
        CGRect otheRec  = CGRectMake(SPACE, CGRectGetMaxY(conRec)+5, SIZE.width-2*SPACE, 15);
        if ([anserModel.answerContent isEqualToString:@""]) {
            cellHeight = CGRectGetMaxY(titRec) + 5+15+15;
        }else{
            cellHeight = CGRectGetMaxY(otheRec)+15;
        }
        
        
    }else{
        
        CGSize titleSize = [self getSize:[self attributedStringWithString:anserModel.title] andWith:SIZE.width - (3*15+80)];
        titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
        
        CGSize conSize = [self getSize:[self attributedStringWithString:anserModel.answerContent] andWith:SIZE.width-(3*15+80)];
        conSize.height = conSize.height>ConHeight?ConHeight:conSize.height;
        if ([anserModel.answerContent isEqualToString:@""]) {
            cellHeight = 125;
        }else{
            cellHeight = titleSize.height + 35+5+conSize.height + 5 + 15 + 15 > 125?titleSize.height + 35+5+conSize.height + 5 + 15 + 15:125;
        }
//        cellHeight =160;
    }
    
    return cellHeight;
}

- (CGSize)getSize:(NSAttributedString *)attrStr andWith:(CGFloat)with
{
    
    return [attrStr boundingRectWithSize:CGSizeMake(with, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
}
// 标题；
- (NSAttributedString *)attributedStringWithString:(NSString *)str
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 2;// 字体的行间距
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],NSParagraphStyleAttributeName:paragraphStyle};
    return [[NSAttributedString alloc] initWithString:str attributes:attributes];
}
// 内容
- (NSAttributedString *)conAttributedStringWithString:(NSString *)conStr
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 2;// 字体的行间距
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14],NSParagraphStyleAttributeName:paragraphStyle};
    return [[NSAttributedString alloc] initWithString:conStr attributes:attributes];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

// 点击昵称
- (void)nickClick:(UITapGestureRecognizer *)tapGes
{
    NSLog(@"点击昵称");
}

// 点击标题
- (void)titClick:(MyTapGestrpueReco *)tapGes
{
    GambitAnswerViewController *GamBVC = [[GambitAnswerViewController alloc] init];
    GamBVC.TID = tapGes.TID;
    GamBVC.UserID = tapGes.tUserID;
    GamBVC.Answer_ID = tapGes.AnserID;
    [self.navigationController pushViewController:GamBVC animated:YES];
}

// 点击正文
- (void)conClick:(MyTapGestrpueReco *)tapGes
{
    AnswerDetailViewController *anserKeyVC = [[AnswerDetailViewController alloc] init];
    anserKeyVC.UserID = [NSString stringWithFormat:@"%d",tapGes.UserID];
    anserKeyVC.tUserID = tapGes.tUserID;
    anserKeyVC.TID = tapGes.TID;
    anserKeyVC.answerID = tapGes.AnserID;
    
    [self.navigationController pushViewController:anserKeyVC animated:YES];
    
}


@end
