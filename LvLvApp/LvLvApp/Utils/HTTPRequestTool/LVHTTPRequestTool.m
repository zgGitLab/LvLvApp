//
//  LVHTTPRequestTool.m
//  LvLvApp
//
//  Created by 赵俊杰 on 16/5/10.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "LVHTTPRequestTool.h"
#import "AFNetworking.h"

@implementation LVHTTPRequestTool

//用来保存唯一的单例对象
static LVHTTPRequestTool * _instance;

+ (instancetype)sharedDataTool{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc]init];
    });
    return _instance;
}


#pragma mark -- GET请求

+ (void)get:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure{
    //1.创建请求管理者
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    
    //2.发送请求
    [mgr GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        };
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}


#pragma mark -- POST请求

+ (void)post:(NSString *)url params:(NSMutableDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure{
    //1.创建请求管理者
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    mgr.requestSerializer = [AFJSONRequestSerializer serializer];
    mgr.responseSerializer = [AFJSONResponseSerializer serializer];
    mgr.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json",@"text/html", nil];
    
    [mgr.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    mgr.requestSerializer.timeoutInterval = 10;
    [mgr.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    //2.发送请求
    [mgr POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(operation.responseData);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
//            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:error.userInfo[@"com.alamofire.serialization.response.error.data"] options:NSJSONReadingMutableContainers error:nil];
//            BaseDataModel *errorModel = [[BaseDataModel alloc]init];
//            [errorModel setValuesForKeysWithDictionary:dict];
            failure(error);
//            NSLog(@"%@",errorModel.Message);
        }
    }];
}


+ (void)post:(NSString *)url params:(NSMutableDictionary *)params success:(void (^)(id))success failureMessage:(void (^)(NSString *))failureMessage{
    //1.创建请求管理者
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    mgr.requestSerializer = [AFJSONRequestSerializer serializer];
    mgr.responseSerializer = [AFJSONResponseSerializer serializer];
    mgr.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json",@"text/html", nil];
    
    [mgr.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    mgr.requestSerializer.timeoutInterval = 10;
    [mgr.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    //2.发送请求
    [mgr POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(operation.responseData);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.userInfo[@"com.alamofire.serialization.response.error.data"])
        {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:error.userInfo[@"com.alamofire.serialization.response.error.data"] options:NSJSONReadingMutableContainers error:nil];
            BaseDataModel *errorModel = [[BaseDataModel alloc]init];
            [errorModel setValuesForKeysWithDictionary:dict];
            failureMessage(errorModel.Message);
        }else{
//                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:error.userInfo[@"NSLocalizedDescription"] options:NSJSONReadingMutableContainers error:nil];
                failureMessage(error.userInfo[@"NSLocalizedDescription"]);

        }
    }];
}

@end
