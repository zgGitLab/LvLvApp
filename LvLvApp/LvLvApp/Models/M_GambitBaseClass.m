//
//  M_GambitBaseClass.m
//
//  Created by jim  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_GambitBaseClass.h"
#import "M_GambitData.h"


NSString *const kM_GambitBaseClassCode = @"Code";
NSString *const kM_GambitBaseClassMessage = @"Message";
NSString *const kM_GambitBaseClassData = @"Data";


@interface M_GambitBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_GambitBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_GambitBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_GambitBaseClassMessage fromDictionary:dict];
            self.data = [M_GambitData modelObjectWithDictionary:[dict objectForKey:kM_GambitBaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_GambitBaseClassCode];
    [mutableDict setValue:self.message forKey:kM_GambitBaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kM_GambitBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_GambitBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_GambitBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_GambitBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_GambitBaseClassCode];
    [aCoder encodeObject:_message forKey:kM_GambitBaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_GambitBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_GambitBaseClass *copy = [[M_GambitBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
