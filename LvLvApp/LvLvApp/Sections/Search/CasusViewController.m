//
//  CasusViewController.m
//  案例
//
//  Created by IOS8 on 15/9/28.
//  Copyright (c) 2015年 IOS8. All rights reserved.
//

#import "CasusViewController.h"
#import "DetailViewController.h"
#import "SearchViewController.h"
#import "MJRefresh.h"

#define TEXTCOLOR            [UIColor colorWithRed:63/255.00f green:126/255.00f blue:215/255.00f alpha:1]

@interface CasusViewController ()
{
    UIView *headerView;
    UILabel *oldLaw;
    UILabel *newLaw;
    UILabel *oldExample;
    UILabel *newExample;
    NSMutableArray *dataArray;
    NSMutableArray *IDArr;
    NSMutableArray *typeArr;
    int page;
}


@end

@implementation CasusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"检索首页";
    headerView = [[NSBundle mainBundle] loadNibNamed:@"MainHeaderView" owner:self options:nil][0];
    UIButton *searchBtn = (UIButton *)[headerView viewWithTag:1];
    [searchBtn addTarget:self action:@selector(pushDetailVC) forControlEvents:UIControlEventTouchUpInside];
    oldLaw = (UILabel *)[headerView viewWithTag:2];
    newLaw = (UILabel *)[headerView viewWithTag:3];
    oldExample = (UILabel *)[headerView viewWithTag:4];
    newExample = (UILabel *)[headerView viewWithTag:5];
    IDArr = [NSMutableArray array];
    typeArr = [NSMutableArray array];
    page = 1;
    self.mainTable.separatorInset = UIEdgeInsetsMake(0, 0, 0, 8);
    __weak CasusViewController *VC = self;
    self.mainTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [VC requestData:[NSString stringWithFormat:@"%d",page]];
    }];
    // 上拉加载
    self.mainTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 结束刷新
        [self infinite];
    }];
    [self.mainTable.mj_header beginRefreshing];
    
    UIView *stausVi = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, 20)];
    stausVi.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:stausVi];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)infinite
{
    page += 1;
    [self requestData:[NSString stringWithFormat:@"%d",page]];
}


#pragma mark -- 请求数据

- (void)requestData:(NSString *)pageIndex
{
    __weak CasusViewController *VC = self;
    // 法条个和案例统计
    [RequestManager statCount:^(CountModal *modal) {
        
        [VC performSelectorOnMainThread:@selector(refreshUI:) withObject:modal waitUntilDone:YES];
    }];
    // 首页显示法条和案例6条数据
    [RequestManager firstPageDataWithPageIndex:pageIndex andBlock:^(NSArray *dataArr, NSString *info) {
        if ([pageIndex isEqualToString:@"1"]) {
            [dataArray removeAllObjects];
            dataArray = [NSMutableArray arrayWithArray:dataArr];
        }else{
            [dataArray addObjectsFromArray:dataArr];
        }
        [VC performSelectorOnMainThread:@selector(refreshTable) withObject:nil waitUntilDone:YES];
        [VC.mainTable.mj_header endRefreshing];
        [VC.mainTable.mj_footer endRefreshing];
    }];
}


- (void)refreshTable
{
    [IDArr removeAllObjects];
    [typeArr removeAllObjects];
    for ( FristData *frData in dataArray) {
        [IDArr addObject:[NSString stringWithFormat:@"%.0f",frData.iDProperty]];
        
        if (frData.typeName == 1) {
            [typeArr addObject:[NSString stringWithFormat:@"%.0f",2.0]];
        }else{
            [typeArr addObject:[NSString stringWithFormat:@"%.0f",1.0]];
        }
    }
    [self.mainTable reloadData];
}

- (void)refreshUI:(CountModal *)modal
{

    if (modal != nil) {
        NSString *oldLawText = [NSString stringWithFormat:@"%@ 篇",[self addIdentifierWithStr:modal.ArticlesCount]];
        NSString *oldExampleText = [NSString stringWithFormat:@"%@ 篇",[self addIdentifierWithStr:modal.VerdictsCount]];
//        NSString *oldLawText = [NSString stringWithFormat:@"%@ 篇, 新增法条",[self addIdentifierWithStr:modal.ArticlesCount]];
//        NSString *oldExampleText = [NSString stringWithFormat:@"%@ 篇, 新增案例",[self addIdentifierWithStr:modal.VerdictsCount]];
//        NSString *newLawText = [NSString stringWithFormat:@"%@ 篇;",[self addIdentifierWithStr:modal.A_DayAddCount]];
//        NSString *newExampleText = [NSString stringWithFormat:@"%@ 篇;",[self addIdentifierWithStr:modal.V_DayAddCount]];
        [self floatFromString:oldExampleText andFram:oldExample];
        oldLaw.frame = CGRectMake(80, oldLaw.frame.origin.y, oldExample.frame.size.width, 15);
//        [self floatFromString:newLawText andFram:newLaw];
//        [self floatFromString:newExampleText andFram:newExample];
        
        CGFloat newWith = newLaw.frame.size.width>newExample.frame.size.width?newLaw.frame.size.width:newExample.frame.size.width;
        newLaw.frame = CGRectMake(newLaw.frame.origin.x, newLaw.frame.origin.y,newWith, 15);
        newExample.frame = CGRectMake(newExample.frame.origin.x, newExample.frame.origin.y,newWith, 15);
        
        
        NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:oldLawText];
        [str1 addAttribute:NSForegroundColorAttributeName value:TEXTCOLOR range:NSMakeRange(0,[self addIdentifierWithStr:modal.ArticlesCount].length)];
        oldLaw.attributedText = str1;
        
        NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:oldExampleText];
        [str2 addAttribute:NSForegroundColorAttributeName value:TEXTCOLOR range:NSMakeRange(0,[self addIdentifierWithStr:modal.VerdictsCount].length)];
        oldExample.attributedText = str2;
        
//        NSMutableAttributedString *str3 = [[NSMutableAttributedString alloc] initWithString:newLawText];
//        [str3 addAttribute:NSForegroundColorAttributeName value:TEXTCOLOR range:NSMakeRange(0,[self addIdentifierWithStr:modal.A_DayAddCount].length)];
//        newLaw.attributedText = str3;
        
//        NSMutableAttributedString *str4 = [[NSMutableAttributedString alloc] initWithString:newExampleText];
//        [str4 addAttribute:NSForegroundColorAttributeName value:TEXTCOLOR range:NSMakeRange(0,[self addIdentifierWithStr:modal.V_DayAddCount].length)];
//        newExample.attributedText = str4;
    }
    
}

- (NSMutableString *)addIdentifierWithStr:(NSString *)tempStr
{
    NSMutableString *temp1= [NSMutableString stringWithString:tempStr];
    long count = tempStr.length%3 != 0?tempStr.length/3:tempStr.length/3-1;
    for (int i = 0; i < count; i++) {
        [temp1 insertString:@"," atIndex:temp1.length-(3*(i+1)+i)];
    }
    return temp1;
}

// 计算宽度；
- (void)floatFromString:(NSString *)str andFram:(UILabel *)lab
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:13], NSFontAttributeName, nil];
    
    CGRect rect = lab.frame;
    rect.size.width = [str sizeWithAttributes:dictionary].width + 5;
    if (lab.tag == 3) {

        rect.origin.x = CGRectGetMaxX(oldLaw.frame);
    }else if (lab.tag  == 5){
         rect.origin.x = CGRectGetMaxX(oldExample.frame);
    }
    lab.frame = rect;
}

- (void)pushDetailVC
{
//    NSString  * nsStringToOpen = [NSString  stringWithFormat: @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",@"1059306480"];
//    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:nsStringToOpen]];
//    NSString *str = [NSString stringWithFormat:@"http://itunes.apple.com/us/app/id%d", 1059306480];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    SearchViewController *searchVC = [[SearchViewController alloc] init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (SIZE.width > 400) {
        return 300;
    }else if (SIZE.width >350 && SIZE.width < 400)
    {
        return 275;
    }
    return 246;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        UILabel *titlab = [[UILabel alloc] init];
        titlab.tag = 10;
        titlab.numberOfLines = 0;
        titlab.font = [UIFont systemFontOfSize:16];
        titlab.textColor = COLOR(81, 88, 99);
        UILabel *conentLab = [[UILabel alloc] init];
        conentLab.tag = 11;
        conentLab.numberOfLines = 0;
        conentLab.font = [UIFont systemFontOfSize:12];
        conentLab.textColor = COLOR(156, 156, 156);
        [cell.contentView addSubview:titlab];
        [cell.contentView addSubview:conentLab];
    }
    
    
    UILabel *tiLab = [cell.contentView viewWithTag:10];
    UILabel *conLab = [cell.contentView viewWithTag:11];
    
    FristData *frData = [dataArray objectAtIndex:indexPath.row];
    frData.content = [frData.content stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    tiLab.text = frData.title;
    conLab.text = frData.courtOrDepName;
    // 计算标题的高度
    CGSize titSize = [frData.title boundingRectWithSize:CGSizeMake(SIZE.width-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
    // 发布部门高度
    CGSize conSize = [frData.courtOrDepName boundingRectWithSize:CGSizeMake(SIZE.width-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} context:nil].size;
    tiLab.frame = CGRectMake(10, 12, SIZE.width-20, titSize.height);
    conLab.frame = CGRectMake(10, titSize.height + 12 + 5, SIZE.width-20, conSize.height);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FristData *frData = [dataArray objectAtIndex:indexPath.row];
    DetailViewController *detailVC = [[DetailViewController alloc] init];
    detailVC.IDArr = IDArr;
    detailVC.typeArr = typeArr;
    detailVC.iDProperty = (int)indexPath.row;
    // 因为详情页是共用的，数据的案例和法条是1，案例，2法条，在里面我定义的按钮2是案例1是法条，为保持一致这边进行更改；
    if (frData.typeName == 1 ) {
        detailVC.type = 2;
    }else if (frData.typeName == 2){
        detailVC.type = 1;
    }
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FristData *frData = [dataArray objectAtIndex:indexPath.row];
    frData.content = [frData.content stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    // 计算标题的高度
    CGSize titSize = [frData.title boundingRectWithSize:CGSizeMake(SIZE.width-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
    // 发布部门高度
    CGSize conSize = [frData.courtOrDepName boundingRectWithSize:CGSizeMake(SIZE.width-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} context:nil].size;
    return titSize.height+conSize.height+24+5;
}

@end