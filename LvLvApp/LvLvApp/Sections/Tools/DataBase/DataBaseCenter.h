//
//  DataBaseCenter.h
//  LvLvApp
//
//  Created by IOS8 on 15/10/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"
#import "DataModels.h"
#import "PerTaxModals.h"
#import "PersonalTax.h"
@class BankData;
@interface DataBaseCenter : NSObject

@property(nonatomic,retain)FMDatabase* db;
@property(nonatomic,retain)FMDatabaseQueue *queue;
- (id)init;
- (BOOL)getDb;
- (NSString *)dataFilePath;
- (void)createTables;
- (void)searchBankData:(NSString *)iDProperty and:(BankData *)lilv;
- (void)getBankDataWithDay:(NSMutableArray *)list;
- (void)searchPerTaxData:(NSString *)cityName and:(TaxData *)perTax;
#pragma mark 查询个税利率数据
- (PersonalTax *)getPerTaxWithCity:(NSString *)city;
#pragma mark 国内外个税起征点
- (void)searchTaxThreshold:(NSString *)LocalAmount and:(NSString *)ForeignAmount;
#pragma mark 查询个个税所有的数据的城市名称
- (void)getPersonallncomeTax:(NSMutableArray *)list;
- (NSMutableArray *)getTaxThresholdData;

@end
