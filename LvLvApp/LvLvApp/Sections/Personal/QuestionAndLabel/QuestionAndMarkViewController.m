//
//  QuestionAndMarkViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "QuestionAndMarkViewController.h"
#import "QuestionTableViewCell.h"
#import "MarkTableViewCell.h"
#import "IDAttentionData.h"
#import "IDAttentionBaseClass.h"
#import "IDAttDataModels.h"
#import "IDLablelaBaseClass.h"
#import "IDLablelaData.h"

#import "AttionBaseClass.h"
#import "AttionData.h"
#import "AttionDataModels.h"

#import "M_AttionsBaseClass.h"
#import "M_AttionsData.h"
#import "M_AttionsDataModels.h"
#import "GambitAnswerViewController.h"
#import "LabelDetailViewController.h"
@interface QuestionAndMarkViewController ()
{
    int selectIndex;
    UIButton *lastBtn;
    
    IDLablelaData*labdata;
    MarkTableViewCell *mark;
    AttionData *attdata;
    UIView *Issueviews;
    UIView *labelViews;
     NSInteger _pageIndex;
 
}
@end

@implementation QuestionAndMarkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _pageIndex=1;
     self.IdIssueArray =[[NSMutableArray alloc]init];
     self.IdLabelArray =[[NSMutableArray alloc]init];
     [self createUI];
     self.myTable.tableFooterView = [[UIView alloc] init];
     [self.myTable setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    [self.IdIssueArray removeAllObjects];
    
    [self idIssuerequestWithPageindex:1 andPageSize:10];
    self.myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
       [self.IdIssueArray removeAllObjects];
       [self idIssuerequestWithPageindex:1 andPageSize:10];
       [self.myTable layoutSubviews];
    }];
    self.myTable .mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{       [self idIssuerequestWithPageindex:++_pageIndex andPageSize:10];
        [self.myTable layoutSubviews];
    }];



}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    
    [self.IdLabelArray removeAllObjects];
    [self idLablerequestWithPageindex:1 andPageSize:10];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem*butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    ;
    butItem.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
    
    
}
- (void)back{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)createUI
{
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 44)];
    titleView.backgroundColor =[UIColor clearColor];
    UIButton *questionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    questionBtn.frame = CGRectMake(0, 15, 50, 20);
    questionBtn.tag = 1;
    selectIndex = (int)questionBtn.tag;
    lastBtn = questionBtn;
    [questionBtn setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
    questionBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [questionBtn setTitle:@"问题" forState:UIControlStateNormal];
    [questionBtn addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *markBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    markBtn.frame = CGRectMake(70, 15, 50, 20);
    [markBtn setTitleColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
    [markBtn setTitle:@"标签" forState:UIControlStateNormal];
    [markBtn addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    markBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    markBtn.tag = 2;
    
    UILabel *colorLab = [[UILabel alloc] initWithFrame:CGRectMake(questionBtn.center.x-18, 41, 36, 3)];
    colorLab.backgroundColor = [UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1];
    colorLab.tag = 3;
    [titleView addSubview:colorLab];
    [titleView addSubview:markBtn];
    [titleView addSubview:questionBtn];
    self.navigationItem.titleView = titleView;
}

- (void)titleBtnClick:(UIButton *)btn
{
    if (btn.tag == selectIndex) {
        
        return;
    }else{
        [Issueviews removeFromSuperview];
        [labelViews removeFromSuperview];
        selectIndex = (int)btn.tag;
        [lastBtn setTitleColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:58/255.0 green:134/255.0 blue:240/255.0 alpha:1] forState:UIControlStateNormal];
        UILabel *colorLab = (UILabel *)[self.navigationItem.titleView viewWithTag:3];
        lastBtn = btn;
        [UIView animateWithDuration:.2 animations:^{
            colorLab.frame = CGRectMake(btn.center.x-18, 41, 36, 3);
        } completion:^(BOOL finished) {
            
        }];
        [self tableViews];

    }
    
    [self.myTable reloadData];
   
}

//问题请求
- (void)idIssuerequestWithPageindex:(NSInteger)pageIndex andPageSize:(NSInteger)pageSize{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    // 1.url
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:kattentionIssusUrls,K_IP]];
    // 2.请求
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",UserID]];
    [param appendString:[NSString stringWithFormat:@"&pageindex=%d",(int)pageIndex]];
    [param appendString:[NSString stringWithFormat:@"&pageSize=%d",(int)pageSize]];
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         M_AttionsBaseClass *idIss =[[M_AttionsBaseClass alloc]initWithDictionary:dict];
         [self.IdIssueArray addObjectsFromArray:idIss.data];
         [self.myTable reloadData];
        
         [self.myTable.mj_header endRefreshing];
         [self.myTable.mj_footer endRefreshing];
         [self tableViews];
         [labelViews removeFromSuperview];
     }];
}

- (void)tableViews{
    
    if(self.IdLabelArray.count ==0 && self.IdIssueArray.count ==0){
        labelViews =[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2-50, self.view.bounds.size.width, 30)];
       
        UILabel *labe =[[UILabel alloc]init];
        [labe setText:@"您暂无关注!"];
        labe.font=[UIFont systemFontOfSize:16.f];
        CGSize size = [labe.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
        NSLog(@"%lf",size.width);
        labe.frame=CGRectMake(SIZE.width/2-size.width/2, 2, size.width, 25);
        
        [labe setTextColor:[UIColor colorWithRed:158.0/255.0 green:158.0/255.0 blue:158.0/255.0 alpha:1]];
        [labelViews addSubview:labe];
       // self.myTable.tableFooterView=labelViews;
       
        [self.view  addSubview:labelViews];
    }



}
//标签的请求
- (void)idLablerequestWithPageindex:(NSInteger)pageIndex andPageSize:(NSInteger)pageSize
{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    // 1.url
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KIndLableUrk,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",UserID]];
    [param appendString:[NSString stringWithFormat:@"&pageindex=%d",(int)pageIndex]];
    [param appendString:[NSString stringWithFormat:@"&pageSize=%d",(int)pageSize]];
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         IDLablelaBaseClass *idbadata =[[IDLablelaBaseClass alloc]initWithDictionary:dict];
         [self.IdLabelArray addObjectsFromArray:idbadata.data];
         [self.myTable.mj_header endRefreshing];
         //[self.myTable.mj_footer endRefreshing];
         
         [self.myTable reloadData];
         
         
         [self tableViews];
     }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (selectIndex ==1) {
        return self.IdIssueArray.count;
    }else
    {
        return self.IdLabelArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    static NSString *identifier1 = @"cell1";
    if (selectIndex == 1) {
        
        QuestionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[NSBundle mainBundle] loadNibNamed:@"QuestionTableViewCell" owner:self options:nil][0];
        }
        if (self.IdIssueArray.count >indexPath.row) {
            self.iddata =self.IdIssueArray[indexPath.row];
            cell.IndIssue.text =self.iddata.title;
            cell.IndAttentionAnswer.text =[NSString stringWithFormat:@"关注 %ld · 回答 %ld",(long)self.iddata.tagCount,(long)self.iddata.answerCount];
            CGSize size = [cell.IndIssue.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
            NSLog(@"%lf",size.width);
            if (size.width >= (kWidth - 30)) {
                cell.IndIssue.numberOfLines=2;
            }else {
                cell.IndIssue.numberOfLines=1;
            }

        }
       
       
         return cell;

    }else{
        MarkTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier1];
        if (!cell) {
            cell = [[NSBundle mainBundle] loadNibNamed:@"MarkTableViewCell" owner:self options:nil][0];
        }

         labdata=self.IdLabelArray[indexPath.row];
        cell.IDlabletitle.text =labdata.tagName;
        cell.IDlableAtten.text =[NSString stringWithFormat:@"关注 %ld · 问题 %ld · 评论 %ld",(long)labdata.focusTag,(long)labdata.questionCount, (long)labdata.commentCount];

        if (labdata.isFocus ==0 )
        {
            [cell.IDlableAttenBut setImage:[UIImage imageNamed:@"关注"] forState:UIControlStateNormal];
            [cell.IDlableAttenBut addTarget:self action:@selector(AttentionAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.IDlableAttenBut.selected = YES;
        }else
        {
            [cell.IDlableAttenBut setImage:[UIImage imageNamed:@"已关注1"] forState:UIControlStateNormal];
            [cell.IDlableAttenBut addTarget:self action:@selector(AttentionAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.IDlableAttenBut.selected = NO;
            
        }
    
        return cell;
    }  
}
- (void)AttentionAction:(UIButton *)btn
{
  
    
    if(!btn.selected)
    {
        [self collectRequestWithT:@"0"andBtn:btn];
    
        
    }else{
        [self collectRequestWithT:@"1"andBtn:btn];
   }
}
- (void) collectRequestWithT: (NSString *)T andBtn:(UIButton *)btn{
    
    UITableViewCell *Cell = (UITableViewCell *)btn.superview.superview;
    NSIndexPath *index = [self.myTable indexPathForCell:Cell];
    IDLablelaData*dd = self.IdLabelArray[index.row];
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*count = [defaults objectForKey:@"users"];
    NSString *userID =[NSString stringWithFormat:@"%@",count];
    NSString *tagName = dd.tagName;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KattentiontgaUrl,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",userID]];
    [param appendString:[NSString stringWithFormat:@"&TagName=%@",tagName]];
    [param appendString:[NSString stringWithFormat:@"&T=%@",T]];
    
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         if ([dict[@"Code"]isEqualToNumber:@200] ) {
             
             NSLog(@"关注成功");
             btn.selected = !btn.selected;
             NSLog(@"%d",btn.selected);
             NSString *str =btn.selected?@"关注":@"已关注1";
             [btn setImage:[UIImage imageNamed:str] forState:UIControlStateNormal];
             
         }
     }];
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectIndex == 1) {
        QuestionTableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
        cell.selected = NO;
        GambitAnswerViewController *Gambit =[[GambitAnswerViewController alloc]init];
        NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
        NSString *userID= [defaults objectForKey:@"users"];
        NSString *usr =[NSString stringWithFormat:@"%@",userID];
        self.iddata =self.IdIssueArray[indexPath.row];
        NSString *tuserID =[NSString stringWithFormat:@"%d",(int)self.iddata.tUserID];
        Gambit.UserID=usr;
        Gambit.Answer_ID =@"";
        Gambit.UserID = tuserID;
        Gambit.TID =self.iddata.tID;
      
        [self.navigationController pushViewController:Gambit animated:YES];
    }else{
        
        MarkTableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
        cell.selected = NO;
        LabelDetailViewController *labeView =[[LabelDetailViewController alloc]init];
        NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
        NSString*count = [defaults objectForKey:@"users"];
        
        NSString *userID =[NSString stringWithFormat:@"%@",count];
        self.

        self.labeldata =self.IdLabelArray[indexPath.row];
        labeView.Keywordstr =self.labeldata.tagName;
        labeView.UserID =userID;
        labeView.labName =self.labeldata.tagName;
        [self.navigationController pushViewController:labeView animated:YES];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((int)self.iddata <indexPath.row) {
      self.iddata =self.IdIssueArray[indexPath.row];
    }
    
    CGSize size = [self.iddata.title boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size;
    NSLog(@"%lf",size.width);
    if (selectIndex ==1) {
   if (size.width >= (kWidth - 30)) {
            
            return 85;
        }else{
            return 65;
        }
        
    }else {
    
        return 65;
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
