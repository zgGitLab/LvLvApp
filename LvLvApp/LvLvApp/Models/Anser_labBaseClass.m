//
//  Anser_labBaseClass.m
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Anser_labBaseClass.h"
#import "Anser_labData.h"


NSString *const kAnser_labBaseClassCode = @"Code";
NSString *const kAnser_labBaseClassMessage = @"Message";
NSString *const kAnser_labBaseClassData = @"Data";


@interface Anser_labBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Anser_labBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kAnser_labBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kAnser_labBaseClassMessage fromDictionary:dict];
    NSObject *receivedAnser_labData = [dict objectForKey:kAnser_labBaseClassData];
    NSMutableArray *parsedAnser_labData = [NSMutableArray array];
    if ([receivedAnser_labData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedAnser_labData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedAnser_labData addObject:[Anser_labData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedAnser_labData isKindOfClass:[NSDictionary class]]) {
       [parsedAnser_labData addObject:[Anser_labData modelObjectWithDictionary:(NSDictionary *)receivedAnser_labData]];
    }

    self.data = [NSArray arrayWithArray:parsedAnser_labData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kAnser_labBaseClassCode];
    [mutableDict setValue:self.message forKey:kAnser_labBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kAnser_labBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kAnser_labBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kAnser_labBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kAnser_labBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kAnser_labBaseClassCode];
    [aCoder encodeObject:_message forKey:kAnser_labBaseClassMessage];
    [aCoder encodeObject:_data forKey:kAnser_labBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    Anser_labBaseClass *copy = [[Anser_labBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
