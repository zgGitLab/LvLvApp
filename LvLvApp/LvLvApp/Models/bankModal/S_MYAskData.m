//
//  S_MYAskData.m
//
//  Created by IOS8  on 15/12/16
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_MYAskData.h"


NSString *const kS_MYAskDataUserID = @"UserID";
NSString *const kS_MYAskDataAnswerCount = @"AnswerCount";
NSString *const kS_MYAskDataTitle = @"Title";
NSString *const kS_MYAskDataTagCount = @"TagCount";
NSString *const kS_MYAskDataTID = @"TID";


@interface S_MYAskData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_MYAskData

@synthesize userID = _userID;
@synthesize answerCount = _answerCount;
@synthesize title = _title;
@synthesize tagCount = _tagCount;
@synthesize tID = _tID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userID = [[self objectOrNilForKey:kS_MYAskDataUserID fromDictionary:dict] doubleValue];
            self.answerCount = [[self objectOrNilForKey:kS_MYAskDataAnswerCount fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kS_MYAskDataTitle fromDictionary:dict];
            self.tagCount = [[self objectOrNilForKey:kS_MYAskDataTagCount fromDictionary:dict] doubleValue];
            self.tID = [self objectOrNilForKey:kS_MYAskDataTID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kS_MYAskDataUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.answerCount] forKey:kS_MYAskDataAnswerCount];
    [mutableDict setValue:self.title forKey:kS_MYAskDataTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tagCount] forKey:kS_MYAskDataTagCount];
    [mutableDict setValue:self.tID forKey:kS_MYAskDataTID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userID = [aDecoder decodeDoubleForKey:kS_MYAskDataUserID];
    self.answerCount = [aDecoder decodeDoubleForKey:kS_MYAskDataAnswerCount];
    self.title = [aDecoder decodeObjectForKey:kS_MYAskDataTitle];
    self.tagCount = [aDecoder decodeDoubleForKey:kS_MYAskDataTagCount];
    self.tID = [aDecoder decodeObjectForKey:kS_MYAskDataTID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_userID forKey:kS_MYAskDataUserID];
    [aCoder encodeDouble:_answerCount forKey:kS_MYAskDataAnswerCount];
    [aCoder encodeObject:_title forKey:kS_MYAskDataTitle];
    [aCoder encodeDouble:_tagCount forKey:kS_MYAskDataTagCount];
    [aCoder encodeObject:_tID forKey:kS_MYAskDataTID];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_MYAskData *copy = [[S_MYAskData alloc] init];
    
    if (copy) {

        copy.userID = self.userID;
        copy.answerCount = self.answerCount;
        copy.title = [self.title copyWithZone:zone];
        copy.tagCount = self.tagCount;
        copy.tID = [self.tID copyWithZone:zone];
    }
    
    return copy;
}


@end
