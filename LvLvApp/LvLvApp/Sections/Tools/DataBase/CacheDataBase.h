//
//  CacheDataBase.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/10.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheDataBase : NSObject
@property(nonatomic,retain)FMDatabase* cacheDb;
@property(nonatomic,retain)FMDatabaseQueue *queue;
- (id)init;
- (BOOL)getDb;
- (NSString *)dataFilePath;
- (void)createTables;
// 插入搜索条件
- (void)insertString:(NSString *)searchString;
// 获取搜索条件
- (void)getSearchString:(NSMutableArray *)stringArr;
- (void)deleteAllRecord;
@end
