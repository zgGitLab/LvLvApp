//
//  LoginViewController.m
//  LvLvApp
//
//  Created by hope on 15/11/9.
//  Copyright (c) 2015年 gyy. All rights reserved.
//

#define Firstregist @"reg"
#define LeadingKey @"leading"

#import "LoginViewController.h"
#import "AFNetworking.h"
#import "CustomTabBarController.h"
#import "MBProgressHUD+MJ.h"
#import "MBProgressHUD.h"
#import "KeyChain.h"
#import "ForgetThePasswordViewController.h"
#import "RegisterViewController.h"


@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.title=@"登录";
    self.view.backgroundColor = [UIColor whiteColor];
    self.UserPassword.secureTextEntry=YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    NSString *homePath = NSHomeDirectory();
    NSLog(@"%@",homePath);
    
 }
- (void)keyBoardFrameChange:(NSNotification* )notification
{
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    
    int height = keyboardRect.origin.y + 45;
    if (height > SIZE.height) {
        height = SIZE.height;
    }
    [UIView animateWithDuration:0.02 animations:^{
        
        self.denglu.frame=CGRectMake(0,-(SIZE.height - height),self.view.frame.size.width,self.view.frame.size.height);
    }];
    
    
}

- (void)back
{
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

- (IBAction)Login:(id)sender {
    NSString *usernameText = [Utils removeBlankString:self.EmailOrMobile.text];
    if (usernameText.length == 0) {
        [MBProgressHUD showError:@"请输入用户名"];
        return;
    }
    // 2.密码
    NSString *pwdText = [Utils removeBlankString:self.UserPassword.text];
    if (pwdText.length == 0) {
        [MBProgressHUD showError:@"请输入密码"];
        return;
    }

    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:usernameText forKey:@"EmailOrMobile"];
    [param setObject:pwdText forKey:@"UserPassword"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:KlogUrl,K_IP] params:param success:^(id json) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        NSDictionary *dic = [dict objectForKey:@"Data"];
        NSArray *userArr = [dic objectForKey:@"users"];
        self.userID= [userArr[0] objectForKey:@"ID"];
        //登陆成功跳入页面
        if ([dict[@"Code"] isEqualToNumber:@200]) {
            [MobClick profileSignInWithPUID:[NSString stringWithFormat:@"%@",self.userID]];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:usernameText forKey:KAccount];
            [defaults setObject:pwdText forKey:KPwd];
            [defaults setObject:_userID forKey:@"users"];
            [defaults synchronize];
            NSString *success = dict[@"success"];
            [MBProgressHUD showSuccess:success];
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"removeBlankView" object:nil];
        }else {
            [MBProgressHUD hideHUD];
            [MBProgressHUD showError:dict[@"Message"]];
        }
    } failureMessage:^(NSString *errorMessage) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showError:errorMessage];
    }];
}
- (IBAction)ForgetThePassword:(id)sender {
    
    [self.view endEditing:YES];
    
    ForgetThePasswordViewController *viewForget =[[ForgetThePasswordViewController alloc]init];
    [self.navigationController pushViewController:viewForget animated:YES];
}

- (IBAction)RegisterAnAccount:(id)sender {
    RegisterViewController *vr=[[RegisterViewController alloc]init];
    [self.navigationController pushViewController:vr animated:YES];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [self.UserPassword resignFirstResponder];
    [self.EmailOrMobile resignFirstResponder];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [super viewWillAppear:YES];
    // 读取上次的配置
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.EmailOrMobile.text = [defaults objectForKey:KAccount];
//    self.UserPassword.text = [defaults objectForKey:KPwd];
    
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem*butItems =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(fanhuiBut)];
    ;
    butItems.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItems;
    
}
- (void)fanhuiBut{
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
