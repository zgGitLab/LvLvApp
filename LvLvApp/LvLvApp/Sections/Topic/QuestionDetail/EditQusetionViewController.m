//
//  EditAnswerViewController.m
//  LvLvApp
//
//  Created by jim on 15/12/28.
//  Copyright © 2015年 gyy. All rights reserved.
//
#define kButW 40
#define KButH 30
#import "EditQusetionViewController.h"

#import "TopicHomeViewController.h"
#import "LabelViewController.h"
#import "InsertTextAttAchment.h"
#import "NSAttributedString+EmojiExtension.h"
#import "GambitAnswerViewController.h"
@interface EditQusetionViewController ()<UITextViewDelegate,PerPickViewDelegat>
{
    UIView *viewadd;
    UIButton *returnBut;
    UIButton *nextBut;
    UIButton *selectBut;

    UIBarButtonItem *butItem;
    UIBarButtonItem *returnButt;
    UIButton *lablelBut;
    UITextView *_textView;
    CGRect frame1;
    UILabel *_label;

}
@property (nonatomic,strong) NSMutableArray *imageArray;
@property (nonatomic,strong) NSMutableString *ImgCode;
@end

@implementation EditQusetionViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([Utils isBlankString:_content]) {
        _content = [NSMutableString stringWithString:@""];
    }
    self.imageArray = [[NSMutableArray alloc]init];
    self.ImgCode = [NSMutableString stringWithFormat:@""];
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor=[UIColor whiteColor];
    self.navigationItem.title =@"编辑问题";
    
    
    butItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"返回"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(returnButton)];
    
    self.navigationItem.leftBarButtonItem=butItem;
    butItem.tintColor =[UIColor colorWithRed:213.0/255.0 green:213.0/255.0 blue:213.0/255.0 alpha:1];
    
    returnButt=[[UIBarButtonItem alloc] initWithTitle:@"发布"
                                                style: UIBarButtonItemStyleDone
                                               target:self
                                               action:@selector(IssueAction)];
    returnButt.enabled=YES;
    
    [returnButt setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,
                                        [UIColor colorWithRed:73.0/255.0 green:174.0/255.0 blue:242.0/255.0 alpha:1], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal]; self.navigationItem.rightBarButtonItem=returnButt;
    
    
    [self viewaddHeader];
    
    [self registerForKeyboardNotifications];
}
- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidhide:) name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
}

//键盘隐藏
- (void)keyboardDidhide:(NSNotification *)notif{
    [UIView animateWithDuration:0.2 animations:^{
        _textView.frame = CGRectMake(12,0, kWidth-24, kHeight-160+64);
    }];
    
}

//键盘显示
- (void) keyboardWasShown:(NSNotification *) notif
{
    NSDictionary *info = [notif userInfo];
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    
    NSLog(@"keyBoard:%f", keyboardSize.height);  //216
    CGFloat height = 0;
    if (IsIPhone5s||IsIPhone5c||IsIPhone5) {
        height = kHeight-keyboardSize.height-94-20-64;
    }else{
        height = kHeight-keyboardSize.height-94-20;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        _textView.frame = CGRectMake(12, 0, kWidth-24, height);
    }];
}

#pragma mark --编辑问题请求
- (void)iSSueRequestWithDict:(NSMutableDictionary *)dict{

    NSString *url = [NSString stringWithFormat:KEditQuestionUrl,K_IP];
    
    [LVHTTPRequestTool post:url params:dict success:^(id json) {
        NSMutableString *str = [[NSMutableString alloc]initWithData:json encoding:NSUTF8StringEncoding];
        NSLog(@"%@",str);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"dict   ===%@",dict);
        BOOL success = [dict[@"Code"]  isEqualToNumber:@200];
        if(success){
            NSLog(@"发布成功!");
            if (self.backBlock) {
                self.backBlock();
            }
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            NSLog(@"发布失败");
        }
    } failure:^(NSError *error) {
        
    }];
}


//返回上一页
- (void)returnButton
{
//    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark --发布编辑的问题
- (void)IssueAction
{
    _imageArray = [_textView.textStorage getImages];
    //处理图片，将图片数组转换成base64字符串
    for (int i = 0; i < _imageArray.count; i++) {
        NSData *data = UIImageJPEGRepresentation(_imageArray[i], 1);
        NSString *encodedImageStr = [data base64EncodedStringWithOptions:0];
        [_ImgCode appendString:encodedImageStr];
        
        if ((i != _imageArray.count-1)&&(_imageArray.count !=1 )) {
            [_ImgCode appendString:@"|"];
        }
    }
    NSString *info =[_textView.textStorage getPlainString];
    NSMutableString *str = [[NSMutableString alloc]initWithString:info];
    
    //把所有的0替换为11；
    for (int i = 0; i < str.length; i++) {
        NSRange range = [str rangeOfString:@"<img src=\"0.jpg\"/>"];
        
        if (range.location != NSNotFound) {
            [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"r.jpg\"/>"]];
            
        }
    }
    
    for (int i = 0; i < str.length; i++) {
        NSRange range = [str rangeOfString:@"<img src=\"r.jpg\"/>"];
        
        if (range.location != NSNotFound) {
            [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"%d.jpg\"/>",i]];
        }
    }

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:str forKey:@"Description"];//Description
    [dict setObject:kUserID forKey:@"UserID"];//UserID
    [dict setObject:_ImgCode forKey:@"ImgCodes"];//ImgCodes
    [dict setObject:self.tID forKey:@"TID"];
    [self iSSueRequestWithDict:dict];
    
}


#pragma mark --添加头视图
- (void)viewaddHeader
{

    _textView = [[UITextView alloc] initWithFrame:CGRectMake(12,0, kWidth-30, kHeight-100-64)];
    _textView.textColor=[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
    NSLog(@"%lf",_textView.frame.origin.y);

//    _textView.attributedText = self.texts;
    [self showTextView];

    _textView.font=[UIFont systemFontOfSize:15.0f];
    _textView.textAlignment = NSTextAlignmentLeft;
    _textView.autocorrectionType = UITextAutocorrectionTypeNo;
    _textView.keyboardType = UIKeyboardTypeDefault;
    
    _textView.returnKeyType = UIReturnKeyDefault;
    _textView.scrollEnabled = YES;
    _textView.delegate = self;
    _textView.inputAccessoryView = [self keyboardView];
    [self.view addSubview:_textView];
    
}

- (void)showTextView{
    NSMutableString *str = [[NSMutableString alloc]initWithFormat:@"%@",_content];
        //创建NSMutableAttributedString实例，并将content传入
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:str];
        //设置行距
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
        [style setLineSpacing:5.0f];
        //根据给定长度与style设置attStr式样
        [attStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1],NSParagraphStyleAttributeName:style,NSFontAttributeName:[UIFont systemFontOfSize:15]} range:NSMakeRange(0, str.length)];
    NSInteger mm = 0;
        for (int i = 0; i < _imageList.count; i ++) {
            if (![_imageList[i] isEqualToString:@""]) {
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,_imageList[i]]];
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                CGFloat rate = (kWidth-20)/image.size.width;
                image = [Utils thumbnailWithImageWithoutScale:image size:CGSizeMake(kWidth-20, rate*image.size.height)];
                InsertTextAttAchment *attatchment = [[InsertTextAttAchment alloc] init];
                attatchment.image = image;

                NSRange range = [str rangeOfString:@"<img src="];
                
                NSUInteger length = [@"<img src=\"/upload/answer/2016042922080299889.jpg\"/>" length];
                [str deleteCharactersInRange:NSMakeRange(range.location, length) ];
                [attStr deleteCharactersInRange:NSMakeRange(range.location+mm, length)];
                [attStr insertAttributedString:[NSAttributedString attributedStringWithAttachment:attatchment] atIndex:range.location+mm];
                mm++;
            }
        }
    _textView.attributedText = attStr;
}

- (UIView *)keyboardView{
    //自定义键盘上部的view
    UIView *keyboardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 36)];
    keyboardView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    UIButton *at = [UIButton buttonWithType:UIButtonTypeSystem];
    at.frame = CGRectMake(15, 5, 26, 26);
    [at setImage: [[UIImage imageNamed:@"收键盘"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [at addTarget:self action:@selector(collectKeyBoard:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:at];
    
    UIButton *key = [UIButton buttonWithType:UIButtonTypeSystem];
    key.frame = CGRectMake(CGRectGetMaxX(at.frame)+15, 3, 26, 26);
    [key setImage:[[UIImage imageNamed:@"图片"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [key addTarget:self action:@selector(selectPic:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:key];
    return keyboardView;
}

//选择图片
- (void)selectPic:(UIButton *)button{
    [self.view endEditing:YES];
    PerPickImageView *perPickVi = [[PerPickImageView alloc] initWithId:1];
    perPickVi.delegate = self;
    [perPickVi showInView];
}

//收键盘
- (void)collectKeyBoard:(UIButton *)button{
    [_textView resignFirstResponder];
}

#pragma mark PerPickViewDelegat;
- (void)jumpOtherView:(UIImagePickerController *)object
{
    object.allowsEditing = NO;
    [self presentViewController:object animated:YES completion:nil];
}

- (void)selectImage:(id)icon
{
    UIImage *imag = (UIImage *)icon;
    CGFloat rate = (kWidth-20)/imag.size.width;
    imag = [Utils thumbnailWithImageWithoutScale:imag size:CGSizeMake(kWidth-20, imag.size.height *rate)];
    InsertTextAttAchment *texAtt = [[InsertTextAttAchment alloc] init];
    texAtt.image = imag;

    [_textView.textStorage insertAttributedString:[NSAttributedString attributedStringWithAttachment:texAtt]
                                              atIndex:_textView.selectedRange.location];
    //Move selection location
    _textView.selectedRange = NSMakeRange(_textView.selectedRange.location + 1, _textView.selectedRange.length);
    _textView.font = [UIFont systemFontOfSize:15];
    _textView.textColor = [UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
    [_textView becomeFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView{

}

@end

