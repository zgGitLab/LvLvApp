//
//  S_PerHotData.m
//
//  Created by IOS8  on 15/12/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_PerHotData.h"


NSString *const kS_PerHotDataTID = @"TID";
NSString *const kS_PerHotDataUserid = @"Userid";
NSString *const kS_PerHotDataDescription = @"Description";
NSString *const kS_PerHotDataTimagesUrl = @"TimagesUrl";
NSString *const kS_PerHotDataLogoUrl = @"LogoUrl";
NSString *const kS_PerHotDataQuestionContent = @"QuestionContent";
NSString *const kS_PerHotDataCollectCount = @"collectCount";
NSString *const kS_PerHotDataSortTime = @"sortTime";
NSString *const kS_PerHotDataNickname = @"Nickname";
NSString *const kS_PerHotDataCounts = @"counts";
NSString *const kS_PerHotDataTitle = @"Title";
NSString *const kS_PerHotDataAnswerContent = @"AnswerContent";
NSString *const kS_PerHotDataCommentCount = @"commentCount";
NSString *const kS_PerHotDataTimes = @"times";
NSString *const kS_PerHotDataTypes = @"types";
NSString *const kS_PerHotDataAnswerID = @"Answer_ID";
NSString *const kS_PerHotDataTuserID = @"TUserID";
NSString *const kS_PerHotDataAuserID = @"AUserID";


@interface S_PerHotData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_PerHotData

@synthesize tID = _tID;
@synthesize userid = _userid;
@synthesize dataDescription = _dataDescription;
@synthesize timagesUrl = _timagesUrl;
@synthesize logoUrl = _logoUrl;
@synthesize questionContent = _questionContent;
@synthesize collectCount = _collectCount;
@synthesize sortTime = _sortTime;
@synthesize nickname = _nickname;
@synthesize counts = _counts;
@synthesize title = _title;
@synthesize answerContent = _answerContent;
@synthesize commentCount = _commentCount;
@synthesize times = _times;
@synthesize types = _types;
@synthesize answerID = _answerID;
@synthesize tUserID = _tUserID;
@synthesize aUserID = _aUserID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tID = [self objectOrNilForKey:kS_PerHotDataTID fromDictionary:dict];
            self.userid = [[self objectOrNilForKey:kS_PerHotDataUserid fromDictionary:dict] doubleValue];
            self.dataDescription = [self objectOrNilForKey:kS_PerHotDataDescription fromDictionary:dict];
            self.timagesUrl = [self objectOrNilForKey:kS_PerHotDataTimagesUrl fromDictionary:dict];
            self.logoUrl = [self objectOrNilForKey:kS_PerHotDataLogoUrl fromDictionary:dict];
            self.questionContent = [self objectOrNilForKey:kS_PerHotDataQuestionContent fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kS_PerHotDataCollectCount fromDictionary:dict] doubleValue];
            self.sortTime = [self objectOrNilForKey:kS_PerHotDataSortTime fromDictionary:dict];
            self.nickname = [self objectOrNilForKey:kS_PerHotDataNickname fromDictionary:dict];
            self.counts = [[self objectOrNilForKey:kS_PerHotDataCounts fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kS_PerHotDataTitle fromDictionary:dict];
            self.answerContent = [self objectOrNilForKey:kS_PerHotDataAnswerContent fromDictionary:dict];
            self.commentCount = [[self objectOrNilForKey:kS_PerHotDataCommentCount fromDictionary:dict] doubleValue];
            self.times = [self objectOrNilForKey:kS_PerHotDataTimes fromDictionary:dict];
            self.types = [[self objectOrNilForKey:kS_PerHotDataTypes fromDictionary:dict] doubleValue];
            self.answerID = [self objectOrNilForKey:kS_PerHotDataAnswerID fromDictionary:dict];
            self.tUserID = [self objectOrNilForKey:kS_PerHotDataTuserID fromDictionary:dict];
            self.aUserID = [self objectOrNilForKey:kS_PerHotDataAuserID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tID forKey:kS_PerHotDataTID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userid] forKey:kS_PerHotDataUserid];
    [mutableDict setValue:self.dataDescription forKey:kS_PerHotDataDescription];
    [mutableDict setValue:self.timagesUrl forKey:kS_PerHotDataTimagesUrl];
    [mutableDict setValue:self.logoUrl forKey:kS_PerHotDataLogoUrl];
    [mutableDict setValue:self.questionContent forKey:kS_PerHotDataQuestionContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kS_PerHotDataCollectCount];
    [mutableDict setValue:self.sortTime forKey:kS_PerHotDataSortTime];
    [mutableDict setValue:self.nickname forKey:kS_PerHotDataNickname];
    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kS_PerHotDataCounts];
    [mutableDict setValue:self.title forKey:kS_PerHotDataTitle];
    [mutableDict setValue:self.answerContent forKey:kS_PerHotDataAnswerContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kS_PerHotDataCommentCount];
    [mutableDict setValue:self.times forKey:kS_PerHotDataTimes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.types] forKey:kS_PerHotDataTypes];
    [mutableDict setValue:self.answerID forKey:kS_PerHotDataAnswerID];
    [mutableDict setValue:self.tUserID forKey:kS_PerHotDataTuserID];
    [mutableDict setValue:self.aUserID forKey:kS_PerHotDataAuserID];
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tID = [aDecoder decodeObjectForKey:kS_PerHotDataTID];
    self.userid = [aDecoder decodeDoubleForKey:kS_PerHotDataUserid];
    self.dataDescription = [aDecoder decodeObjectForKey:kS_PerHotDataDescription];
    self.timagesUrl = [aDecoder decodeObjectForKey:kS_PerHotDataTimagesUrl];
    self.logoUrl = [aDecoder decodeObjectForKey:kS_PerHotDataLogoUrl];
    self.questionContent = [aDecoder decodeObjectForKey:kS_PerHotDataQuestionContent];
    self.collectCount = [aDecoder decodeDoubleForKey:kS_PerHotDataCollectCount];
    self.sortTime = [aDecoder decodeObjectForKey:kS_PerHotDataSortTime];
    self.nickname = [aDecoder decodeObjectForKey:kS_PerHotDataNickname];
    self.counts = [aDecoder decodeDoubleForKey:kS_PerHotDataCounts];
    self.title = [aDecoder decodeObjectForKey:kS_PerHotDataTitle];
    self.answerContent = [aDecoder decodeObjectForKey:kS_PerHotDataAnswerContent];
    self.commentCount = [aDecoder decodeDoubleForKey:kS_PerHotDataCommentCount];
    self.times = [aDecoder decodeObjectForKey:kS_PerHotDataTimes];
    self.types = [aDecoder decodeDoubleForKey:kS_PerHotDataTypes];
    self.answerID = [aDecoder decodeObjectForKey:kS_PerHotDataAnswerID];
    self.tUserID = [aDecoder decodeObjectForKey:kS_PerHotDataTuserID];
    self.aUserID = [aDecoder decodeObjectForKey:kS_PerHotDataAuserID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tID forKey:kS_PerHotDataTID];
    [aCoder encodeDouble:_userid forKey:kS_PerHotDataUserid];
    [aCoder encodeObject:_dataDescription forKey:kS_PerHotDataDescription];
    [aCoder encodeObject:_timagesUrl forKey:kS_PerHotDataTimagesUrl];
    [aCoder encodeObject:_logoUrl forKey:kS_PerHotDataLogoUrl];
    [aCoder encodeObject:_questionContent forKey:kS_PerHotDataQuestionContent];
    [aCoder encodeDouble:_collectCount forKey:kS_PerHotDataCollectCount];
    [aCoder encodeObject:_sortTime forKey:kS_PerHotDataSortTime];
    [aCoder encodeObject:_nickname forKey:kS_PerHotDataNickname];
    [aCoder encodeDouble:_counts forKey:kS_PerHotDataCounts];
    [aCoder encodeObject:_title forKey:kS_PerHotDataTitle];
    [aCoder encodeObject:_answerContent forKey:kS_PerHotDataAnswerContent];
    [aCoder encodeDouble:_commentCount forKey:kS_PerHotDataCommentCount];
    [aCoder encodeObject:_times forKey:kS_PerHotDataTimes];
    [aCoder encodeDouble:_types forKey:kS_PerHotDataTypes];
    [aCoder encodeObject:_answerID forKey:kS_PerHotDataAnswerID];
    [aCoder encodeObject:_tUserID forKey:kS_PerHotDataTuserID];
    [aCoder encodeObject:_aUserID forKey:kS_PerHotDataAuserID];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_PerHotData *copy = [[S_PerHotData alloc] init];
    
    if (copy) {

        copy.tID = [self.tID copyWithZone:zone];
        copy.userid = self.userid;
        copy.dataDescription = [self.dataDescription copyWithZone:zone];
        copy.timagesUrl = [self.timagesUrl copyWithZone:zone];
        copy.logoUrl = [self.logoUrl copyWithZone:zone];
        copy.questionContent = [self.questionContent copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.sortTime = [self.sortTime copyWithZone:zone];
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.counts = self.counts;
        copy.title = [self.title copyWithZone:zone];
        copy.answerContent = [self.answerContent copyWithZone:zone];
        copy.commentCount = self.commentCount;
        copy.times = [self.times copyWithZone:zone];
        copy.types = self.types;
        copy.answerID = [self.answerID copyWithZone:zone];
        copy.tUserID = [self.tUserID copyWithZone:zone];
        copy.aUserID = [self.aUserID copyWithZone:zone];
    }
    
    return copy;
}


@end
