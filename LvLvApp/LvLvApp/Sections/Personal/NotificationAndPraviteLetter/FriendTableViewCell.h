//
//  FriendTableViewCell.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/24.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *privateMessBtn;
@property (weak, nonatomic) IBOutlet UIButton *titleIcon;
@property (weak, nonatomic) IBOutlet UILabel *titleStr;
@property (weak, nonatomic) IBOutlet UILabel *conStr;
@end
