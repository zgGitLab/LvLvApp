//
//  M_reportData.h
//
//  Created by hope  on 15/12/25
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface M_reportData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double userID;
@property (nonatomic, strong) NSString *reportDate;
@property (nonatomic, strong) id category;
@property (nonatomic, strong) NSString *disposeResult;
@property (nonatomic, strong) NSString *remarkContent;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
