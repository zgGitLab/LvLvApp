//
//  EditeducationInfoViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/10.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "S_PerBaseInfoEductList.h"
@interface EditeducationInfoViewController : BaseViewController
@property (strong, nonatomic) S_PerBaseInfoEductList *eduModal;
@property(assign,nonatomic) BOOL ishide;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UITextField *schoolName;
@property (weak, nonatomic) IBOutlet UITextField *categorySchool;
@property (weak, nonatomic) IBOutlet UIButton *removeBtn;
@end
