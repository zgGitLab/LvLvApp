//
//  AnswerKyeViewController.m
//  LvLvApp
//
//  Created by hope on 15/10/15.
//  Copyright (c) 2015年 gyy. All rights reserved.
//



#import <QuartzCore/QuartzCore.h>
#import "AnswerDetailViewController.h"
#import "GambitAnswerViewController.h"
#import "AnswerModel.h"
#import "commentView.h"
#import "CommentNumViewController.h"
#import "OtherPerMainViewController.h"
#import "PerMainPageViewController.h"
#import "LoginViewController.h"
#import "ShareView.h"
#import "EditAnswerViewController.h"

@interface AnswerDetailViewController ()
{
    UIButton *approveBtn;
    UIButton *collectbtn;
    BOOL isHidde;
    int _height;//键盘的高度
    AnswerModel *model;
    commentView *commentsView;
    NSString *approveFlag;
    UIView *_hideView;
    UILabel *commentCountLabel;
    NSString *reason;
    NSAttributedString *attrReason;
    NSMutableArray *imageList ;
}
@end
@implementation AnswerDetailViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    reason = [Utils isBlankString:reason]?@"":reason;
    self.UserID = [NSString stringWithFormat:@"%@",self.UserID];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    [self requestData];//请求数据
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    
    UIBarButtonItem *leftItem =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"返回箭头"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    
    self.navigationItem.leftBarButtonItem=leftItem;
    
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"分享"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(shareClick)];
    self.navigationItem.rightBarButtonItem=rightItem;
    
    //增加监听,当键盘出现或者改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    //增加监听,当键盘退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
}


#pragma mark -- 草稿保存特效
//草稿view
-  (void)addView{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    UIScrollView *v = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 20)];
    v.contentSize = CGSizeMake(kWidth,40);
    v.tag = 111;
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kWidth, 20)];
    title.tag = 11;
    title.backgroundColor = [UIColor whiteColor];
    title.text = @"草稿保存中...";
    title.font = [UIFont systemFontOfSize:13];
    title.textAlignment = NSTextAlignmentCenter;
    [v addSubview:title];
    
    UILabel *tit =[[UILabel alloc]initWithFrame:CGRectMake(0, 20, kWidth, 20)];
    tit.tag = 1;
    tit.backgroundColor = [UIColor whiteColor];
    tit.text = @"草稿保存成功";
    tit.font = [UIFont systemFontOfSize:13];
    tit.textAlignment = NSTextAlignmentCenter;
    [v addSubview:tit];
    
    app.window.windowLevel = UIWindowLevelAlert;
    [app.window addSubview:v];
    [self performSelector:@selector(changeFrame) withObject:nil afterDelay:0.5];
}
//修改特效的frame
- (void)changeFrame{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    UIScrollView *s = [app.window viewWithTag:111];
    [UIView animateKeyframesWithDuration:0.3 delay:0 options:UIViewKeyframeAnimationOptionLayoutSubviews animations:^{
        s.contentOffset = CGPointMake(0, 20);
    } completion:^(BOOL finished) {
        [self performSelector:@selector(removeView) withObject:nil afterDelay:0.5];
    }];
}

//移除草稿保存特效
- (void)removeView{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    UIScrollView *vie = [app.window viewWithTag:111];
    app.window.windowLevel = UIWindowLevelNormal;
    [UIView animateKeyframesWithDuration:0.3 delay:0 options:UIViewKeyframeAnimationOptionLayoutSubviews animations:^{
        vie.frame = CGRectMake(0, -20, kWidth, 20);
    } completion:^(BOOL finished) {
        [vie removeFromSuperview];
    }];
    
}



#pragma mark --请求数据
- (void)requestData{
    //传入的参数
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:kUserID forKey:@"UserID"];
    [parameters setObject:self.UserID forKey:@"AUserID"];
    [parameters setObject:[NSString stringWithFormat:@"%@",self.answerID] forKey:@"Answer_ID"];
    [parameters setObject:[NSString stringWithFormat:@"%@",self.TID] forKey:@"TID"];
    //发送请求
    [LVHTTPRequestTool post:[NSString stringWithFormat:kAnswerUrl,K_IP] params:parameters success:^(id json) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        NSArray *array = [dict objectForKey:@"Data"];
        NSDictionary *dic = [array lastObject];
        model = [[AnswerModel alloc]initWithDictionary:dic];
        approveFlag = [NSString stringWithFormat:@"%@",model.state];
        [self createContentView];
        [self writeComment];
    } failure:^(NSError *error) {
    }];
}

#pragma mark --评论
- (void)requestDataWithContent:(NSString *)string{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:kUserID forKey:@"UserID"];
    [parameters setObject:[NSString stringWithFormat:@"%@",self.answerID] forKey:@"Answer_ID"];
    [parameters setObject:[NSString stringWithFormat:@"%@",self.TID] forKey:@"TID"];
    [parameters setObject:string forKey:@"Content"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:kCommentUrl,K_IP] params:parameters success:^(id json) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        if ([dic[@"Code"] isEqualToNumber:@200]) {
            NSLog(@"评论成功!");
            NSInteger count = commentCountLabel.text.integerValue;
            commentCountLabel.text = [NSString stringWithFormat:@"%ld",(long)++count];
            commentsView.content.text = @"";
            [self commentCount:nil];
        }else{
            NSLog(@"评论失败");
        }
    } failure:^(NSError *error) {
        commentsView.content.text = @"";
    }];
}

#pragma mark --点赞
- (void)approveRequestWithT:(NSString *)string{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:kUserID forKey:@"UserID"];
    [parameters setObject:[NSString stringWithFormat:@"%@",self.answerID] forKey:@"Answer_ID"];
    [parameters setObject:[NSString stringWithFormat:@"%@",self.TID] forKey:@"TID"];
    [parameters setObject:string forKey:@"T"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:kApproveUrl,K_IP] params:parameters success:^(id json) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        approveFlag = string;//记录是否赞同
        if ([dic[@"Code"] isEqualToNumber:@200]) {
            NSLog(@"成功!");
            if([string isEqualToString:@"1"]){
                NSLog(@"已点赞");
            }else{
                NSLog(@"未赞同");
            }
        }else{
            NSLog(@"赞同失败");
        }
    } failure:^(NSError *error) {
    }];
}


#pragma mark --收藏
- (void)collectRequestWithT:(NSString *)T{
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:kUserID forKey:@"UserID"];
    [parameters setObject:[NSString stringWithFormat:@"%@",self.answerID] forKey:@"Answer_ID"];
    [parameters setObject:[NSString stringWithFormat:@"%@",self.TID] forKey:@"TID"];
    [parameters setObject:T forKey:@"T"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:kCollectUrl,K_IP] params:parameters success:^(id json) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        if ([dic[@"Code"] isEqualToNumber:@200]) {
            NSLog(@"成功");
            if ([dic[@"Data"][@"state"] isEqualToString:@"1"]) {
                NSLog(@"收藏成功");
            }else{
                NSLog(@"取消收藏");
            }
        }else{
            NSLog(@"失败");
        }
    } failure:^(NSError *error) {
        
    }];
    
}
- (UIButton *)addRightItemWithImage:(NSString *)imageName action:(SEL)action {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image = [UIImage imageNamed:imageName];
    button.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // 这里需要注意：由于是想让图片右移，所以left需要设置为正，right需要设置为负。正在是相反的。
    // 让按钮图片右移15
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, 15, 0, -15)];
    
    [button setImage:[UIImage imageNamed:@"返回箭头"] forState:UIControlStateNormal];
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    button.titleLabel.font = [UIFont systemFontOfSize:16];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = rightItem;
    return button;
}

//返回
- (void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];
}


//分享
- (void)shareClick
{
    if ([kUserID integerValue]!=0) {
        NSArray *imageArray = @[@"微信好友",@"微信朋友圈",@"印象笔记",@"有道云笔记",@"微博",@"QQ",@"邮件",@"复制链接"];
        NSArray *titleArray = @[@"微信好友",@"微信朋友圈",@"印象笔记",@"有道云笔记",@"微博",@"QQ",@"邮件",@"复制链接"];
        titleArray = [Utils isClientInstalled:titleArray];
        imageArray = [Utils isClientInstalled:imageArray];
        NSArray  *editArray=nil;
        NSArray  *editArray1 = nil;
        if ([self.UserID isEqualToString:kUserID]) {
            editArray = @[@"编辑",@"字体大小",@"删除",@""];
            editArray1 = @[@"编辑回答",@"字体大小",@"删除回答",@""];
        }else{
            editArray = @[@"举报",@"字体大小",@"",@""];
            editArray1 = @[@"举报回答",@"字体大小",@"",@""];
        }
        
        NSString *title = @"分享回答";
        NSString *shareText = [Utils isBlankString:model.Title]?@"":model.Title;
        
        NSString *shareUrl = [NSString stringWithFormat:@"http://%@/Mobile/CommentsPage?UserID=%@&AUserID=%@&AnswerID=%@&TID=%@",k_SHAREIP,kUserID,self.UserID,self.answerID,self.TID];
        shareText = [NSString stringWithFormat:@"分享回答《%@》%@ (分享自@律律App)",[Utils replaceImageString:shareText],shareUrl];
        UIImage *shareImage = [UIImage imageNamed:@"logos"];
        ShareView *share = [[ShareView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
        share.editAnswerBlock= ^{
            EditAnswerViewController *edit = [[EditAnswerViewController alloc]init];
            edit.title = @"编辑回答";
            edit.imageList = imageList;
            edit.content = [[NSMutableString alloc]initWithFormat:@"%@",reason];
            edit.answerID = self.answerID;
            edit.TID = self.TID;
            edit.backBlock = ^{
                [self requestData];
            };
            //保存草稿后，提示特效
            edit.draftSaveBlock = ^(NSString *str){
                NSLog(@"%@",str);
                [self addView];
            };
            [self.navigationController pushViewController:edit animated:YES];
        };
        
        share.deleteAnswerBlock = ^{
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            [param setObject:kUserID forKey:@"UserID"];
            [param setObject:[NSString stringWithFormat:@"%@",self.answerID] forKey:@"AnswerID"];
            [manager POST:[NSString stringWithFormat:KDeleteAnswerUrl,K_IP] parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSMutableString *str = [[NSMutableString alloc]initWithData:operation.responseData encoding:NSUTF8StringEncoding];
                NSLog(@"str ===%@",str);
                if (self.backBlock) {
                    self.backBlock();
                }
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"error:%@",error);
            }];
        };
        
        //举报的参数
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:kUserID forKey:@"UserID"];
        [dict setObject:self.answerID?self.answerID:@"" forKey:@"ID"];
        //举报类型 1问题，2回答，3用户
        [dict setObject:@"2" forKey:@"Category"];
        [dict setObject:reason forKey:@"Title"];
        
        share.dictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"imageArr1":imageArray,@"titleArr1":titleArray,@"imageArr2":editArray,@"titleArr2":editArray1,@"title":title,@"shareText":shareText,@"shareUrl":shareUrl,@"shareImage":shareImage,@"dict":dict,@"shareTitle":model.Title}];
        
        //分享到笔记
        share.noteDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"imageArr1":imageArray,@"titleArr1":titleArray,@"imageArr2":editArray,@"titleArr2":editArray1,@"title":title,@"shareText":[Utils replaceImageString:model.AnswerContent],@"shareUrl":shareUrl,@"shareImage":shareImage,@"dict":dict,@"shareTitle":[NSString stringWithFormat:@"%@ 回答了 %@",model.NickName,model.Title]}];
        
        share.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshUIs) name:@"refresh" object:nil];
        [APP.window addSubview:share];
    }else{
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }
}

- (void)refreshUIs{
    [self createContentView];
    [self writeComment];
    
}

//内容
- (void)createContentView{
    if (self.contentView != nil) {
        [self.contentView removeFromSuperview];
    }
    //title以下的部分
    self.contentView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-64)];
    _contentView.contentSize = CGSizeMake(kWidth, kHeight);
    _contentView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:_contentView];
    
    //头视图
    _headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 115)];
    //    [_headerView setBackgroundColor:[UIColor redColor]];
    [_contentView addSubview:_headerView];
    
    
    //头像
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,model.ImageUrl]];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 30, 30)];
    imageView.layer.cornerRadius = 15;
    imageView.layer.masksToBounds = YES;
    [imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"头像小"]];
    [_headerView addSubview:imageView];
    
    //昵称
    UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake( imageView.bounds.size.width+imageView.frame.origin.x+10, 5+5, 200, 15)];
    [label setText:model.NickName];
    [label setTextColor:[UIColor colorWithRed:60/255.0f green:60/255.0f blue:60/255.0f alpha:1]];
    [label setFont:[UIFont systemFontOfSize:15.0f]];
    [_headerView addSubview:label];
    
    //时间
    UILabel *timeLabel =[[UILabel alloc]initWithFrame:CGRectMake( imageView.bounds.size.width+imageView.frame.origin.x+10, label.bounds.size.height+label.frame.origin.y +2, 200, 15)];
    if (model.AnswerDT) {
        [timeLabel  setText:model.AnswerDT];
    }
    
    [timeLabel  setTextColor:[UIColor colorWithRed:171/255.0 green:171/255.0 blue:171/255.0 alpha:1]];
    [timeLabel  setFont:[UIFont systemFontOfSize:12.f]];
    [_headerView addSubview:timeLabel ];
    
    //赞同
    approveBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    if ([approveFlag isEqualToString:@"1"]) {
        approveBtn.selected = YES;
    }else{
        approveBtn.selected = NO;
    }
    [approveBtn setBackgroundImage:[UIImage imageNamed:@"赞同了"] forState:UIControlStateNormal];
    [approveBtn setBackgroundImage:[UIImage imageNamed:@"已赞同了"] forState:UIControlStateSelected];
    [approveBtn setFrame:CGRectMake(_headerView.frame.size.width-60,_headerView.frame.origin.y+15-3,48,26)];
    [approveBtn addTarget:self action:@selector(approveClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:approveBtn];
    
    //头像下面的线
    UIImageView *xianView =[[UIImageView alloc]initWithFrame:CGRectMake(0,50 ,_headerView.frame.size.width, 0.5)];
    [xianView setImage:[UIImage imageNamed:@"线"]];
    [_headerView addSubview:xianView];
    
    //设置titleName
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.backgroundColor = [UIColor clearColor];
    btn.frame = CGRectMake(10,xianView.frame.origin.y+15-2,_headerView.frame.size.width-20, _headerView.frame.size.height-20-45);
    [btn setTitle:model.Title forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:22.0f];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:22.0f]];
    [btn setTitleColor:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1] forState:UIControlStateNormal];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    btn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [btn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.numberOfLines = 0;
    CGFloat btnHeight = [Utils textHeightFromTextString:model.Title width:_headerView.frame.size.width-20-20 fontSize:22];
    btn.frame = CGRectMake(10,xianView.frame.origin.y+15-2,_headerView.frame.size.width-20, btnHeight);
    [_headerView addSubview:btn];
    
    //标题下面的线
    UIImageView *xImage =[[UIImageView alloc]initWithFrame:CGRectMake(10,btn.frame.origin.y+btnHeight+10,_headerView.bounds.size.width-10,0.5)];
    [xImage setImage:[UIImage imageNamed:@"线"]];
    [_headerView  addSubview:xImage];
    _headerView.frame = CGRectMake(0, 0, self.view.bounds.size.width, xImage.frame.origin.y+10);
    
    //点击事件，进入个人界面
    UIButton *clearBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    clearBtn.backgroundColor = [UIColor clearColor];
    [clearBtn addTarget:self action:@selector(clearBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    clearBtn.frame = CGRectMake(0, 0, kWidth-50, 50);
    [_headerView addSubview:clearBtn];
    
    //回答的内容
    if (model.AnswerContent != nil) {
        imageList = [Utils subArrayFromString:model.AnswerContent];
        CGFloat fontsize = [SingeData sharedInstance].answerContentSize;
        RTFTextView *labelContent = [[RTFTextView alloc]initWithFrame:CGRectMake(20-4,xImage.frame.origin.y+10,_contentView.frame.size.width-40+8, 150) andTextColor:[UIColor colorWithRed:122/255.0 green:122/255.0 blue:122/255.0 alpha:1] andFont:fontsize andText:model.AnswerContent withNumOfLines:0 andImageArray:imageList];
        UIView *hid = [[UIView alloc]initWithFrame:CGRectMake(20-4, xImage.frame.origin.y+10, kWidth, 2)];
        hid.backgroundColor = [UIColor whiteColor];
        
        [_contentView addSubview:labelContent];
        [_contentView addSubview:hid];
        
        reason = model.AnswerContent;
        attrReason = labelContent.attributedText;
        _contentView.contentSize = CGSizeMake(kWidth, CGRectGetMaxY(labelContent.frame)+50);
    }else{
        _contentView.contentSize = CGSizeMake(kWidth, CGRectGetMaxY(_headerView.frame)+50);
    }
}


#pragma mark --标题点击事件

- (void)titleClick:(UIButton *)btn{
    if ([self.isEnterFromQuestionPage isEqualToString:@"1"]) {//判断是否是从问题页进入
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        GambitAnswerViewController *vd =[[GambitAnswerViewController alloc]init];
        vd.UserID = self.tUserID;
        vd.Answer_ID = self.answerID;
        vd.TID = self.TID;
        [self.navigationController pushViewController:vd animated:YES];
    }
}

#pragma mark --点赞
- (void)approveClick:(UIButton *)btttn
{
    if ([kUserID integerValue]!=0) {
        btttn.selected = !btttn.selected;
        if (btttn.selected) {
            [self approveRequestWithT:@"1"];
        }else{
            [self approveRequestWithT:@"0"];
        }
        NSString *str =btttn.selected?@"已赞同了":@"赞同了";
        [btttn  setBackgroundImage:[UIImage imageNamed:str]forState:UIControlStateNormal];
    }else {
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }
    
}

#pragma mark --评论
- (void)writeComment
{
    if (self.write != nil) {
        [self.write removeFromSuperview];
    }
    self.write =[[UIView alloc]initWithFrame:CGRectMake(0, kHeight-50-64, kWidth, 50)];
    self.write.backgroundColor = [UIColor whiteColor];
    UIImageView *xianView =[[UIImageView alloc]initWithFrame:CGRectMake(0,0 ,kWidth, 0.5)];
    [xianView setImage:[UIImage imageNamed:@"线"]];
    [self.write addSubview:xianView];
    
    UIButton *writebut = [UIButton buttonWithType:UIButtonTypeCustom];
    [writebut setTitle:@" 写评论                   " forState:UIControlStateNormal];
    writebut.titleLabel.font=[UIFont systemFontOfSize:15.0f];
    [writebut setTitleColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:120/255.0 alpha:1] forState:UIControlStateNormal];
    [writebut setFrame:CGRectMake(0,5,_write.frame.size.width/2+20,38)];
    [writebut addTarget:self action:@selector(commentClick:) forControlEvents:UIControlEventTouchUpInside];
    [_write addSubview:writebut];
    UIImageView *writeImage=[[UIImageView alloc]initWithFrame:CGRectMake(10, 15, 18,20 )];
    [writeImage setImage:[UIImage imageNamed:@"评论"]];
    [_write addSubview:writeImage];
    
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(_headerView.frame.size.width-105-10, 10+1, 0.5, 24)];
    line.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1];
    [_write addSubview:line];
    
    collectbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([model.IsCollect isEqualToString: @"1"]) {
        collectbtn.selected = YES;
    }else{
        collectbtn.selected = NO;
    }
    [collectbtn setBackgroundImage:[UIImage imageNamed:@"未收藏"] forState:UIControlStateNormal];
    [collectbtn setBackgroundImage:[UIImage imageNamed:@"已收藏"] forState:UIControlStateSelected];
    [collectbtn setFrame:CGRectMake(_headerView.frame.size.width-90-10,12+1,18,19)];
    [collectbtn addTarget:self action:@selector(collectClick:) forControlEvents:UIControlEventTouchUpInside];
    [_write addSubview:collectbtn];
    
    
    
    
    UIButton*pinglongs= [UIButton buttonWithType:UIButtonTypeCustom];
    //    [pinglongs setBackgroundImage:[UIImage imageNamed:@"评论数"] forState:UIControlStateNormal];
    [pinglongs setImage:[UIImage imageNamed:@"评论数"] forState:UIControlStateNormal];
    //    [pinglongs setFrame:CGRectMake(_write.frame.size.width-60,15,22,18)];
    [pinglongs setFrame:CGRectMake(_write.frame.size.width-60,15,30,18)];
    [pinglongs addTarget:self action:@selector(commentCount:) forControlEvents:UIControlEventTouchUpInside];
    [_write addSubview:pinglongs];
    
    commentCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(_write.frame.size.width-30, 13, 22, 18)];
    commentCountLabel.text = [NSString stringWithFormat:@"%@",model.CommentCount];
    commentCountLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(commentCount:)];
    tap.numberOfTapsRequired = 1;
    [commentCountLabel addGestureRecognizer:tap];
    commentCountLabel.textColor = [UIColor colorWithRed:117/255.0 green:117/255.0 blue:117/255.0 alpha:1];
    [_write addSubview:commentCountLabel];
    [self.view addSubview:_write];
}
//收藏事件
- (void)collectClick:(UIButton *)btn
{
    if ([kUserID integerValue]!=0) {
        btn.selected = !btn.selected;
        
        if(btn.selected)
        {
            [self collectRequestWithT:@"1"];
        }else{
            [self collectRequestWithT:@"0"];
        }
    }else{
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }
    
}


- (void)commentCount:(UIButton *)button
{
    CommentNumViewController *commentNum = [[CommentNumViewController alloc]init];
    commentNum.userID = self.UserID;
    commentNum.answerID = self.answerID;
    commentNum.TID = self.TID;
    [self.navigationController pushViewController:commentNum animated:YES];
}

//评论事件
- (void)commentClick:(UIButton *)button{
    
    if ([kUserID integerValue]!=0) {
        //自定义键盘上部的view
        UIView *keyboardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 36)];
        keyboardView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
        //        UIButton *at = [UIButton buttonWithType:UIButtonTypeSystem];
        //        at.frame = CGRectMake(15, 5, 22, 22);
        //        [at setImage: [[UIImage imageNamed:@"圈@"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        //        [at addTarget:self action:@selector(atClick:) forControlEvents:UIControlEventTouchUpInside];
        //        [keyboardView addSubview:at];
        
        UIButton *key = [UIButton buttonWithType:UIButtonTypeSystem];
        key.frame = CGRectMake(15, 5, 26, 26);
        [key setImage:[[UIImage imageNamed:@"收键盘"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [key addTarget:self action:@selector(collectKeyBoard:) forControlEvents:UIControlEventTouchUpInside];
        [keyboardView addSubview:key];
        
        //遮罩
        if (!_hideView) {
            _hideView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
            _hideView.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:0.5];
        }
        [self.view addSubview:_hideView];
        
        
        if (commentsView == nil) {
            commentsView = [[[NSBundle mainBundle]loadNibNamed:@"commentView" owner:self options:nil]lastObject];
            
            commentsView.content.layer.borderColor = [UIColor lightGrayColor].CGColor;
//            commentsView.frame = CGRectMake(0, kHeight-200-258-60, kWidth, 200);
            commentsView.content.layer.borderWidth = 0.5;
            commentsView.content.layer.cornerRadius = 5;
            commentsView.content.inputAccessoryView = keyboardView;
            [commentsView.sendBtn addTarget:self action:@selector(sendClick:) forControlEvents:UIControlEventTouchUpInside];
            [commentsView.cancelBtn addTarget:self action:@selector(cancelClick:) forControlEvents:UIControlEventTouchUpInside];
            
            [_hideView addSubview:commentsView];
        }
        
        [commentsView.content becomeFirstResponder];
    }else{
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }
}

//at
- (void)atClick:(UIButton *)button{
    NSMutableString *str = [[NSMutableString alloc]init];
    str = [NSMutableString stringWithFormat:@"%@@",commentsView.content.text];
    commentsView.content.text = str;
}

//收键盘
- (void)collectKeyBoard:(UIButton *)button{
    [self cancelClick:nil];
}

//取消
- (void)cancelClick:(UIButton *)button{
    [commentsView.content resignFirstResponder];
    [_hideView removeFromSuperview];
    commentsView.frame = CGRectMake(0, 1000, 0, 0);
}

//发送
- (void)sendClick:(UIButton *)button{
    if ([commentsView.content.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请输入评论内容！" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    }else{
        [_hideView removeFromSuperview];
        [self requestDataWithContent:commentsView.content.text];
        commentsView.frame = CGRectMake(0, 1000, 0, 0);
        [commentsView.content resignFirstResponder];
    }
}

//点击头像部分进入他人界面
- (void)clearBtnClick:(UIButton *)btn{
    if ([self.UserID isEqualToString:kUserID]) {
        PerMainPageViewController *per = [[PerMainPageViewController alloc]init];
        per.userID = self.UserID;
        [self.navigationController pushViewController:per animated:YES];
    }else{
        OtherPerMainViewController *other = [[OtherPerMainViewController alloc]init];
        other.userID = kUserID;
        other.otherID = self.UserID;
        [self.navigationController pushViewController:other animated:YES];
    }
    
}
#pragma  mark --键盘事件
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    _height = height+60;
    commentsView.frame = CGRectMake(0, kHeight-200-_height, kWidth, 200);
    
}


- (void)keyboardWillHide:(NSNotification *)aNotification
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
