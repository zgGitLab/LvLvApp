//
//  Utils.m
//  LvLvApp
//
//  Created by hope on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "Utils.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#import "ImageCenterButton.h"
@implementation Utils
//动态 计算行高
//根据字符串的实际内容的多少 在固定的宽度和字体的大小，动态的计算出实际的高度
+ (CGFloat)textHeightFromTextString:(NSString *)text width:(CGFloat)textWidth fontSize:(CGFloat)size{
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) {
        //iOS7之后
        /*
         第一个参数: 预设空间 宽度固定  高度预设 一个最大值
         第二个参数: 行间距 如果超出范围是否截断
         第三个参数: 属性字典 可以设置字体大小
         */
        NSDictionary *dict = @{NSFontAttributeName:[UIFont systemFontOfSize:size]};
        CGRect rect = [text boundingRectWithSize:CGSizeMake(textWidth, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
        //返回计算出的行高
        return rect.size.height;
        
    }else {
        //iOS7之前
        /*
         1.第一个参数  设置的字体固定大小
         2.预设 宽度和高度 宽度是固定的 高度一般写成最大值
         3.换行模式 字符换行
         */
        CGSize textSize = [text sizeWithFont:[UIFont systemFontOfSize:size] constrainedToSize:CGSizeMake(textWidth, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];

        return textSize.height;//返回 计算出得行高
    }
}

//设置行间距
+ (void)setLinewidthWith:(id)object andWidth:(CGFloat)width andString:(NSString *)string{
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:3];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [string length])];
    UIButton *btn = nil;
    UILabel *label = nil;
    if ([object isKindOfClass:[UIButton class]]) {
        btn = (UIButton *)object;
        [btn.titleLabel setAttributedText:attributedString1];
        [btn.titleLabel sizeToFit];

    }else if([object isKindOfClass:[UILabel class]]){
        label = (UILabel *)object;
        [label setAttributedText:attributedString1];
        [label sizeToFit];
    }
}

//获取宽度
+ (CGFloat)textWidthFromTextString:(NSString *)text fontSize:(CGFloat)size size:(CGFloat)height{
//    CGSize titleSize = [text sizeWithFont:[UIFont systemFontOfSize:size] constrainedToSize:CGSizeMake(MAXFLOAT, height)];
    CGSize titleSize = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:size]} context:nil].size;
    CGFloat width = titleSize.width;
    return width;
}

+ (NSString *)replaceImageString:(NSString *)string{
    if([self isBlankString:string]){
        return @"";
    }
    
    NSString * str = [string stringByReplacingOccurrencesOfRegex:@"<img.*src=(.*?)[^>]*?>" withString:@"" range:NSMakeRange(0, string.length)];
    return str;
}

//自定义label
+ (CustomLabel *)labelWithFrame:(CGRect)frame andTextColor:(UIColor *)color andFont:(UIFont *)font andText:(NSString *)text withNumOfLines:(NSInteger)lines withTarget:(id)target andSEL:(SEL)selecter{
    CustomLabel *label = [[CustomLabel alloc]initWithFrame:frame];
//    label.textColor = color;
//    label.font = font;
//    label.text = text;
    label.numberOfLines = lines;
    label.userInteractionEnabled = YES;
    label.verticalAlignment = VerticalAlignmentTop;
    //创建NSMutableAttributedString实例，并将text传入
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:text];
    //创建NSMutableParagraphStyle实例
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    //设置行距
    [style setLineSpacing:3.0f];
//    style.lineBreakMode = NSLineBreakByTruncatingTail;
    //根据给定长度与style设置attStr式样
    [attStr addAttributes:@{NSForegroundColorAttributeName:color,NSParagraphStyleAttributeName:style,NSFontAttributeName:font} range:NSMakeRange(0, text.length)];
    //label获取attStr式样
    label.attributedText = attStr;
    
    
    // 单击的 Recognizer
    MyTapGestureRecognizer* singleRecognizer;
    singleRecognizer = [[MyTapGestureRecognizer alloc] initWithTarget:target action:selecter];
    singleRecognizer.label = label;
    //点击的次数
    singleRecognizer.numberOfTapsRequired = 1; // 单击
    //给self.view添加一个手势监测；
    [label addGestureRecognizer:singleRecognizer];
    
    return label;
}




//自定义imageview
+ (UIImageView *)imageWithFrame:(CGRect)frame andImageUrl:(NSString *)string{
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    NSURL *url = [NSURL URLWithString:string];
    [imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"AppIcon"]];
    return imageView;
}



#pragma mark 显示分享菜单

/**
 *  显示分享菜单
 *
 *  @param view 容器视图
 */
+ (void)showShareActionSheet:(UIView *)view
{
    /**
     * 在简单分享中，只要设置共有分享参数即可分享到任意的社交平台
     **/
//    __weak ViewController *theController = self;
    
    //1、创建分享参数（必要）
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    NSArray* imageArray = @[[UIImage imageNamed:@"shareImg.png"]];
    [shareParams SSDKSetupShareParamsByText:@"分享内容"
                                     images:imageArray
                                        url:[NSURL URLWithString:@"http://mob.com"]
                                      title:@"分享标题"
                                       type:SSDKContentTypeAuto];
    
    //1.2、自定义分享平台（非必要）
    NSMutableArray *activePlatforms = [NSMutableArray arrayWithArray:[ShareSDK activePlatforms]];
    //添加一个自定义的平台（非必要）
//    SSUIShareActionSheetCustomItem *item = [SSUIShareActionSheetCustomItem itemWithIcon:[UIImage imageNamed:@"Icon.png"]
//                                                                                  label:@"自定义"
//                                                                                onClick:^{
//                                                                                    
//                                                                                    //自定义item被点击的处理逻辑
//                                                                                    NSLog(@"=== 自定义item被点击 ===");
//                                                                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"自定义item被点击"
//                                                                                                                                        message:nil
//                                                                                                                                       delegate:nil
//                                                                                                                              cancelButtonTitle:@"确定"
//                                                                                                                              otherButtonTitles:nil];
//                                                                                    [alertView show];
//                                                                                }];
//    [activePlatforms addObject:item];
    
    //设置分享菜单栏样式（非必要）
//            [SSUIShareActionSheetStyle setActionSheetBackgroundColor:[UIColor colorWithRed:249/255.0 green:0/255.0 blue:12/255.0 alpha:0.5]];
//            [SSUIShareActionSheetStyle setActionSheetColor:[UIColor colorWithRed:21.0/255.0 green:21.0/255.0 blue:21.0/255.0 alpha:1.0]];
//            [SSUIShareActionSheetStyle setCancelButtonBackgroundColor:[UIColor colorWithRed:21.0/255.0 green:21.0/255.0 blue:21.0/255.0 alpha:1.0]];
//            [SSUIShareActionSheetStyle setCancelButtonLabelColor:[UIColor whiteColor]];
//            [SSUIShareActionSheetStyle setItemNameColor:[UIColor whiteColor]];
//            [SSUIShareActionSheetStyle setItemNameFont:[UIFont systemFontOfSize:10]];
//            [SSUIShareActionSheetStyle setCurrentPageIndicatorTintColor:[UIColor colorWithRed:156/255.0 green:156/255.0 blue:156/255.0 alpha:1.0]];
//            [SSUIShareActionSheetStyle setPageIndicatorTintColor:[UIColor colorWithRed:62/255.0 green:62/255.0 blue:62/255.0 alpha:1.0]];
//    
    //2、分享
    [ShareSDK showShareActionSheet:view
                             items:activePlatforms
                       shareParams:shareParams
               onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                   
                   switch (state) {
                           
                       case SSDKResponseStateBegin:
                       {
                           break;
                       }
                       case SSDKResponseStateSuccess:
                       {
                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享成功"
                                                                               message:nil
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"确定"
                                                                     otherButtonTitles:nil];
                           [alertView show];
                           break;
                       }
                       case SSDKResponseStateFail:
                       {
                           if (platformType == SSDKPlatformTypeSMS && [error code] == 201)
                           {
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                               message:@"失败原因可能是：1、短信应用没有设置帐号；2、设备不支持短信应用；3、短信应用在iOS 7以上才能发送带附件的短信。"
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"OK"
                                                                     otherButtonTitles:nil, nil];
                               [alert show];
                               break;
                           }
                           else if(platformType == SSDKPlatformTypeMail && [error code] == 201)
                           {
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                               message:@"失败原因可能是：1、邮件应用没有设置帐号；2、设备不支持邮件应用；"
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"OK"
                                                                     otherButtonTitles:nil, nil];
                               [alert show];
                               break;
                           }
                           else
                           {
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                               message:[NSString stringWithFormat:@"%@",error]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"OK"
                                                                     otherButtonTitles:nil, nil];
                               [alert show];
                               break;
                           }
                           break;
                       }
                       case SSDKResponseStateCancel:
                       {
                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享已取消"
                                                                               message:nil
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"确定"
                                                                     otherButtonTitles:nil];
                           [alertView show];
                           break;
                       }
                       default:
                           break;
                   }

                   
               }];
    
    //另附：设置跳过分享编辑页面，直接分享的平台。
    //        SSUIShareActionSheetController *sheet = [ShareSDK showShareActionSheet:view
    //                                                                         items:nil
    //                                                                   shareParams:shareParams
    //                                                           onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
    //                                                           }];
    //
    //        //删除和添加平台示例
    //        [sheet.directSharePlatforms removeObject:@(SSDKPlatformTypeWechat)];
    //        [sheet.directSharePlatforms addObject:@(SSDKPlatformTypeSinaWeibo)];
    
}

#pragma mark -获取iOS设备的型号
//获得设备型号
+ (NSString *)getCurrentDeviceModel
{
    int mib[2];
    size_t len;
    char *machine;
    
    mib[0] = CTL_HW;
    mib[1] = HW_MACHINE;
    sysctl(mib, 2, NULL, &len, NULL, 0);
    machine = malloc(len);
    sysctl(mib, 2, machine, &len, NULL, 0);
    
    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    //保留以供参考
    /*
     if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G (A1203)";
     if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G (A1241/A1324)";
     if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS (A1303/A1325)";
     if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4 (A1332)";
     if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4 (A1332)";
     if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4 (A1349)";
     if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S (A1387/A1431)";
     if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5 (A1428)";
     if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5 (A1429/A1442)";
     if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c (A1456/A1532)";
     if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c (A1507/A1516/A1526/A1529)";
     if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s (A1453/A1533)";
     if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s (A1457/A1518/A1528/A1530)";
     if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus (A1522/A1524)";
     if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6 (A1549/A1586)";
     if ([platform isEqualToString:@"iPod1,1"])   return @"iPod Touch 1G (A1213)";
     if ([platform isEqualToString:@"iPod2,1"])   return @"iPod Touch 2G (A1288)";
     if ([platform isEqualToString:@"iPod3,1"])   return @"iPod Touch 3G (A1318)";
     if ([platform isEqualToString:@"iPod4,1"])   return @"iPod Touch 4G (A1367)";
     if ([platform isEqualToString:@"iPod5,1"])   return @"iPod Touch 5G (A1421/A1509)";
     if ([platform isEqualToString:@"iPad1,1"])   return @"iPad 1G (A1219/A1337)";
     if ([platform isEqualToString:@"iPad2,1"])   return @"iPad 2 (A1395)";
     if ([platform isEqualToString:@"iPad2,2"])   return @"iPad 2 (A1396)";
     if ([platform isEqualToString:@"iPad2,3"])   return @"iPad 2 (A1397)";
     if ([platform isEqualToString:@"iPad2,4"])   return @"iPad 2 (A1395+New Chip)";
     if ([platform isEqualToString:@"iPad2,5"])   return @"iPad Mini 1G (A1432)";
     if ([platform isEqualToString:@"iPad2,6"])   return @"iPad Mini 1G (A1454)";
     if ([platform isEqualToString:@"iPad2,7"])   return @"iPad Mini 1G (A1455)";
     if ([platform isEqualToString:@"iPad3,1"])   return @"iPad 3 (A1416)";
     if ([platform isEqualToString:@"iPad3,2"])   return @"iPad 3 (A1403)";
     if ([platform isEqualToString:@"iPad3,3"])   return @"iPad 3 (A1430)";
     if ([platform isEqualToString:@"iPad3,4"])   return @"iPad 4 (A1458)";
     if ([platform isEqualToString:@"iPad3,5"])   return @"iPad 4 (A1459)";
     if ([platform isEqualToString:@"iPad3,6"])   return @"iPad 4 (A1460)";
     if ([platform isEqualToString:@"iPad4,1"])   return @"iPad Air (A1474)";
     if ([platform isEqualToString:@"iPad4,2"])   return @"iPad Air (A1475)";
     if ([platform isEqualToString:@"iPad4,3"])   return @"iPad Air (A1476)";
     if ([platform isEqualToString:@"iPad4,4"])   return @"iPad Mini 2G (A1489)";
     if ([platform isEqualToString:@"iPad4,5"])   return @"iPad Mini 2G (A1490)";
     if ([platform isEqualToString:@"iPad4,6"])   return @"iPad Mini 2G (A1491)";
     if ([platform isEqualToString:@"i386"])      return @"iPhone Simulator";
     if ([platform isEqualToString:@"x86_64"])    return @"iPhone Simulator";
     */
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone4";
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone4";
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone4";
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone4S";
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone5";
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone5";
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone5c";
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone5c";
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone5s";
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone5s";
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone6Plus";
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone6";
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone6sPlus";
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone6s";
    if ([platform isEqualToString:@"iPod1,1"])   return @"iPodTouch1G";
    if ([platform isEqualToString:@"iPod2,1"])   return @"iPodTouch2G";
    if ([platform isEqualToString:@"iPod3,1"])   return @"iPodTouch3G";
    if ([platform isEqualToString:@"iPod4,1"])   return @"iPodTouch4G";
    if ([platform isEqualToString:@"iPod5,1"])   return @"iPodTouch5G";
    if ([platform isEqualToString:@"iPad1,1"])   return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])   return @"iPad";
    if ([platform isEqualToString:@"iPad2,2"])   return @"iPad";
    if ([platform isEqualToString:@"iPad2,3"])   return @"iPad";
    if ([platform isEqualToString:@"iPad2,4"])   return @"iPad";
    if ([platform isEqualToString:@"iPad2,5"])   return @"iPad";
    if ([platform isEqualToString:@"iPad2,6"])   return @"iPad";
    if ([platform isEqualToString:@"iPad2,7"])   return @"iPad";
    if ([platform isEqualToString:@"iPad3,1"])   return @"iPad";
    if ([platform isEqualToString:@"iPad3,2"])   return @"iPad";
    if ([platform isEqualToString:@"iPad3,3"])   return @"iPad";
    if ([platform isEqualToString:@"iPad3,4"])   return @"iPad";
    if ([platform isEqualToString:@"iPad3,5"])   return @"iPad";
    if ([platform isEqualToString:@"iPad3,6"])   return @"iPad";
    if ([platform isEqualToString:@"iPad4,1"])   return @"iPad";
    if ([platform isEqualToString:@"iPad4,2"])   return @"iPad";
    if ([platform isEqualToString:@"iPad4,3"])   return @"iPad";
    if ([platform isEqualToString:@"iPad4,4"])   return @"iPad";
    if ([platform isEqualToString:@"iPad4,5"])   return @"iPad";
    if ([platform isEqualToString:@"iPad4,6"])   return @"iPad";
    if ([platform isEqualToString:@"i386"])      return @"iPhoneSimulator";
    if ([platform isEqualToString:@"x86_64"])    return @"iPhoneSimulator";
    return platform;
}

+ (BOOL) isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}
#pragma mark --分享菜单
+ (UIView *)shareMenuWithFrame:(CGRect)frame andImageArray:(NSArray *)imageArray andTitleArray:(NSArray *)titleArray andId:(id)myself andAction:(SEL)selecter andStartTag:(NSInteger)tags{
    UIView *shareView = [[UIView alloc]initWithFrame:frame];
    
    NSUInteger rows = 2.0;//行
    NSUInteger cols = 4.0;//列
    if (imageArray.count > 4) {
        rows = 2.0;
    }else{
        rows = 1.0;
    }
    CGFloat unitWidth =  shareView.frame.size.width / cols;
    CGFloat unitHeight = unitWidth;
    int i = 0;
    int m = 0;
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col ++) {
            
            NSLog(@"%d",i);
            ImageCenterButton *imageCenterButton = [[ImageCenterButton alloc] init];
            imageCenterButton.frame = CGRectMake(col * unitWidth, row * unitHeight, unitWidth, unitHeight);
            [imageCenterButton setImage:[UIImage imageNamed:imageArray[i++]] forState:UIControlStateNormal];
            [imageCenterButton setTitle:titleArray[m++] forState:UIControlStateNormal];
            [imageCenterButton setTitleColor:setColor(130, 130, 130) forState:UIControlStateNormal];
            imageCenterButton.titleLabel.font = [UIFont systemFontOfSize: 12];
            imageCenterButton.backgroundColor = [UIColor whiteColor];
            imageCenterButton.padding = 14;
            imageCenterButton.layer.borderColor = [[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1] CGColor];
            imageCenterButton.layer.borderWidth = 0.5;
            imageCenterButton.tag = tags ++;
            [imageCenterButton addTarget:myself action:selecter forControlEvents:UIControlEventTouchUpInside];
            [shareView addSubview:imageCenterButton];
        }
    }
    return shareView;
}


//判断是否按装app客户端，传入数组，返回数组
//[ShareSDK isClientInstalled:SSDKPlatformTypeSinaWeibo];
//[ShareSDK isClientInstalled:SSDKPlatformSubTypeQQFriend];
//[ShareSDK isClientInstalled:SSDKPlatformSubTypeWechatSession];
//[ShareSDK isClientInstalled:SSDKPlatformSubTypeWechatTimeline];

+(NSArray *)isClientInstalled:(NSArray *)array{
    NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:array];
//    if (![ShareSDK isClientInstalled:SSDKPlatformTypeSinaWeibo]) {
//        for (int i = 0; i < arr.count; i ++) {
//            if ([arr[i] isEqualToString:@"微博"]) {
//                [arr removeObject:arr[i]];
//                [arr addObject:@""];
//            }
//        }
//    }
    if (![ShareSDK isClientInstalled:SSDKPlatformSubTypeQQFriend]) {
        for (int i = 0; i < arr.count; i ++) {
            if ([arr[i] isEqualToString:@"QQ"]) {
                [arr removeObject:arr[i]];
                [arr addObject:@""];
            }
        }
    }
    if (![ShareSDK isClientInstalled:SSDKPlatformSubTypeWechatSession]) {
        for (int i = 0; i < arr.count; i ++) {
            if ([arr[i] isEqualToString:@"微信好友"]) {
                [arr removeObject:arr[i]];
                [arr addObject:@""];
            }
        }
    }
    if (![ShareSDK isClientInstalled:SSDKPlatformSubTypeWechatTimeline]) {
        for (int i = 0; i < arr.count; i ++) {
            if ([arr[i] isEqualToString:@"微信朋友圈"]) {
                [arr removeObject:arr[i]];
                [arr addObject:@""];
            }
        }
    }
    return arr;
}

//保持原来的长宽比，生成一个缩略图
+(UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image)
    {
        newimage = nil;
    }
    else
    {
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height)
        {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        else
        {
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return newimage;
}


//根据字符串提取图片数组
+ (NSMutableArray *)subArrayFromString:(NSString *)str{
    NSMutableArray *imageArr = [NSMutableArray array];
    if (str == nil) {
        str = @"";
    }
    NSMutableString *strings = [[NSMutableString alloc]initWithString:str];
    NSUInteger length = [@"upload/answer/2015121016061261388.jpg" length];
    for (int i = 0; i < strings.length; i ++) {
        NSRange range = [strings rangeOfString:@"upload"];
        if (range.location != NSNotFound) {
            [imageArr addObject:[strings substringWithRange:NSMakeRange(range.location, length)]];
            [strings deleteCharactersInRange:NSMakeRange(range.location, length)];
        }
    }
    return imageArr;
}



+ (NSString *)removetheSpacesAndLinebreaksFromString:(NSString *)text{
    if (text) {
        NSString *str = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        return [str stringByReplacingOccurrencesOfRegex:@"\\s+" withString:@"  "];
    }else{
        return @"";
    }
}


+ (NSString *)removeBlankString:(NSString *)text{
    if (text) {
        NSString *str = [text stringByReplacingOccurrencesOfRegex:@"\\s+" withString:@""];
        return str;
    }else {
        return @"";
    }
}

+ (NSString *)StringByreplaceImageWithString:(NSString *)str{
    return [str stringByReplacingOccurrencesOfRegex:@"<img.*src=(.*?)[^>]*?>" withString:@"[图片]" range:NSMakeRange(0, str.length)];
}

#pragma mark -- 密码验证

+ (BOOL)validatePassword:(NSString *)passWord
{
    NSString *passWordRegex = @"^[a-zA-Z0-9]{6,20}$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passWordPredicate evaluateWithObject:passWord];
}

#pragma mark -- 身份证号验证

+ (BOOL) validateIdentityCard: (NSString *)identityCard
{
    BOOL flag;
    if (identityCard.length <= 0) {
        flag = NO;
        return flag;
    }
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:identityCard];
}

#pragma mark -- 邮箱验证

+ (BOOL) validateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
@end
