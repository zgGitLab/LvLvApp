//
//  AskViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/11.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "AskViewController.h"
#import "GambitAnswerViewController.h"
#import "AskTableViewCell.h"
#import "UITableView+SDAutoTableViewCellHeight.h"
#import "UIView+SDAutoLayout.h"
@interface AskViewController ()
{
    NSMutableArray *dataArray;
    int index;
}
@end

@implementation AskViewController

- (void)viewDidLoad {
    [super viewDidLoad];// 提问
    self.title = [NSString stringWithFormat:@"%@的提问",self.myTitle];
    dataArray = [NSMutableArray array];
    self.askTab.separatorInset = UIEdgeInsetsMake(0, 5, 0, 8);
    __weak AskViewController *VC = self;
    // 上拉加载
    self.askTab.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 结束刷新
        [VC infinite];
    }];
    self.askTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        index = 1;
        NSString *pageIndex = [NSString stringWithFormat:@"%d",index];
        [VC requestAskData:pageIndex];
    }];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    index = 1;
    NSString *indexStr = [NSString stringWithFormat:@"%d",index];
    [self requestAskData:indexStr];
}

- (void)requestAskData:(NSString *)indexStr
{
    __weak AskViewController *VC = self;
    [RequestManager myAskWithUser:self.userID andPageIndex:indexStr andPageSize:@"10" andBlock:^(NSArray *dataArr, NSString *message) {
        [VC.askTab.mj_header endRefreshing];
        [VC.askTab.mj_footer endRefreshing];
        if ([indexStr isEqualToString:@"1"]) {
            [dataArray removeAllObjects];
            dataArray = [NSMutableArray arrayWithArray:dataArr];
        }else{
            [dataArray addObjectsFromArray:dataArr];
        }
        
        [VC performSelectorOnMainThread:@selector(refreshAskData) withObject:nil waitUntilDone:YES];
    }];

}

- (void)infinite
{
    index += 1;
    NSString *pageIndex = [NSString stringWithFormat:@"%d",index];
    [self requestAskData:pageIndex];

}

- (void)refreshAskData
{
    [self.askTab reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     [self.askTab startAutoCellHeightWithCellClass:[AskTableViewCell class] contentViewWidth:[UIScreen mainScreen].bounds.size.width];
    return dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"cell";
    AskTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[AskTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

    }
    S_MYAskData *askData = dataArray[indexPath.row];
    cell.askData = askData;

    return cell;
    

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    S_MYAskData *askData = dataArray[indexPath.row];
    GambitAnswerViewController *GamBVC = [[GambitAnswerViewController alloc] init];
    GamBVC.TID = askData.tID;
    GamBVC.UserID = [NSString stringWithFormat:@"%.0f",askData.userID];
    [self.navigationController pushViewController:GamBVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    S_MYAskData *askData = dataArray[indexPath.row];
    return [self.askTab cellHeightForIndexPath:indexPath model:askData keyPath:@"askData"];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 1)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01f;
}
@end

