//
//  AnwerTableViewCell.h
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnwerTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *AnwerContent;

@property (strong, nonatomic) IBOutlet UILabel *CountentHeadline;
@property (strong, nonatomic) IBOutlet UILabel *Zgp;
@end
