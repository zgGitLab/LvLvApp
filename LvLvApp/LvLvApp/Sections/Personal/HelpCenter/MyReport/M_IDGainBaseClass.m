//
//  M_IDGainBaseClass.m
//
//  Created by hope  on 15/12/25
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_IDGainBaseClass.h"
#import "M_IDGainData.h"


NSString *const kM_IDGainBaseClassCode = @"Code";
NSString *const kM_IDGainBaseClassMessage = @"Message";
NSString *const kM_IDGainBaseClassData = @"Data";


@interface M_IDGainBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_IDGainBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_IDGainBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_IDGainBaseClassMessage fromDictionary:dict];
            self.data = [M_IDGainData modelObjectWithDictionary:[dict objectForKey:kM_IDGainBaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_IDGainBaseClassCode];
    [mutableDict setValue:self.message forKey:kM_IDGainBaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kM_IDGainBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_IDGainBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_IDGainBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_IDGainBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_IDGainBaseClassCode];
    [aCoder encodeObject:_message forKey:kM_IDGainBaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_IDGainBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_IDGainBaseClass *copy = [[M_IDGainBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
