//
//  HelpDocumentViewController.m
//  LvLvApp
//
//  Created by hope on 15/12/24.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "HelpDocumentViewController.h"

#import "mhelpeBaseClass.h"
#import "mhelpeData.h"
#import "mhelpeDataModels.h"

@interface HelpDocumentViewController (){
    UIWebView *_webView;
    UITextView *helpView;
}

@end

@implementation HelpDocumentViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem*butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    ;
    butItem.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.helpeArray =[[NSMutableArray alloc]init];
    
    self.navigationItem.title =@"帮助文档";
    [self createH5];
    
//    helpView =[[UITextView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    helpView.textColor =[UIColor colorWithRed:136/255 green:138/255 blue:138/255 alpha:1];
//    helpView.editable= NO;
//    helpView.font=[UIFont systemFontOfSize:16.f];

//    [self.view addSubview:helpView];
//    [self helpDocumentRequest];

    
}
//- (void)helpDocumentRequest{;
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KhelpUrl,K_IP]];;
//
//    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
//    
//    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//    NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:received options:NSJSONReadingMutableLeaves error:nil];
//      NSString *str = [[NSString alloc]initWithData:received encoding:NSUTF8StringEncoding];
//    NSLog(@"%@",str);
//    mhelpeBaseClass *mhelpe =[[mhelpeBaseClass alloc]initWithDictionary:dict];
//    [self.helpeArray addObject:mhelpe.data];
//    //NSLog(@"%@",mhelpe);
//    if (mhelpe.data.docText ==nil ||[mhelpe.data.docText isEqualToString:@""]) {
//      
//    }else{
//     helpView.text= mhelpe.data.docText ;
//    }
//   
//}


- (void)createH5{
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-64)];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KHelpDoc,K_IMGIP]];
    [self.view addSubview:_webView];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    
}

- (void)backClick{
    if ([_webView canGoBack]) {
        [_webView goBack];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
