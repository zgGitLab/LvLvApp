//
//  RequestManager.m
//  LvLvApp
//
//  Created by IOS8 on 15/10/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "RequestManager.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "S_PerBaseInfoBaseClass.h"

#define myDelegate [UIApplication sharedApplication].delegate
@implementation RequestManager

// 人民银行数据
+ (void)requestBankDataWithUrl:(NSString *)url andBlock:(void(^)(NSString *message))block;
{
    AFHTTPRequestOperationManager *manger = [AFHTTPRequestOperationManager manager];
    
    manger.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manger GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            NSArray *arr = [dic objectForKey:@"Data"];
            if (arr.count != 0) {
                //                NSLog(@"%@",operation.responseString);
                [BankBaseClass modelObjectWithDictionary:dic];
                block(@"更新成功");
            }else{
                block(@"更新失败");
            }
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"+++++%@",error);
        block(@"更新失败");
    }];
}
// 个税数据
+ (void)requestPerTaxDataWithUrl:(NSString *)url
{
    AFHTTPRequestOperationManager *manger = [AFHTTPRequestOperationManager manager];
    
    manger.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manger GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            [TaxPer modelObjectWithDictionary:dic];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"+++++%@",error);
        
    }];
}

// 个税起征点
+ (void)requestTaxThresholdWithUrl:(NSString *)url
{
    AFHTTPRequestOperationManager *manger = [AFHTTPRequestOperationManager manager];
    manger.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manger GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            NSArray *dataArray = [dic objectForKey:@"Data"];
            if (dataArray.count != 0) {
                NSDictionary *dataDic = [dataArray objectAtIndex:0];
                NSString *LocalAmount = [dataDic objectForKey:@"LocalAmount"];
                NSString *ForeignAmount = [dataDic objectForKey:@"ForeignAmount"];
                AppDelegate *app =[UIApplication sharedApplication].delegate;
                [app.db searchTaxThreshold:LocalAmount and:ForeignAmount];
            }
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"+++++%@",error);
        
    }];
}
// 检索首页统计数量
+(void)statCount:(void(^)(CountModal *modal))block
{
    AFHTTPRequestOperationManager *manger = [AFHTTPRequestOperationManager manager];
    manger.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString *url = [NSString stringWithFormat:@"http://%@/api/Verdicts/gettotalcount",K_IP];
    
    [manger GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            NSDictionary *dict =[dic objectForKey:@"Data"];
            CountModal *modal =[CountModal modelObjectWithDictionary:dict];
            block(modal);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"+++++%@",error);
        block(nil);
        
    }];
    
}

// 检索首页最新法条和案例的6条数据
+(void)firstPageDataWithPageIndex:(NSString *)pageIndex andBlock:(void(^)(NSArray *dataArr,NSString *info))block
{
    AFHTTPRequestOperationManager *manger = [AFHTTPRequestOperationManager manager];
    [manger.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manger.requestSerializer.timeoutInterval = 10.f;
    [manger.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    NSString *url = [NSString stringWithFormat:@"http://%@/api/Verdicts/gettop3list",K_IP];
//    NSString *url = [NSString stringWithFormat:@"http://localhost:19999/api/v1/articles/toplist"];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:@"10" forKey:@"PageSize"];
    [parameters setObject:kUserID forKey:@"UserID"];
    [manger POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqualToNumber:@200]) {
            FristPage *frist = [[FristPage alloc] initWithDictionary:dic];
            block(frist.data,dic[@"Message"]);
        }else{
            block(nil,dic[@"Message"]);
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if ([error code] == -1009){
            block(nil,@"请检查网络连接");
        }else{
            block(nil,@"请求失败");
        }
    }];
    
//    [manger GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
//        if ([[dic objectForKey:@"Code"] isEqualToNumber:@200]) {
//            FristPage *frist = [[FristPage alloc] initWithDictionary:dic];
//            block(frist.data,dic[@"Message"]);
//        }else{
//            block(nil,dic[@"Message"]);
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        if ([error code] == -1009){
//            block(nil,@"请检查网络连接");
//        }else{
//            block(nil,@"请求失败");
//        }
//    }];
    
}
// http://192.168.1.10/api/Verdicts/getverdictdetails
// 案例详情页面
+(void)detailPageDataWithUrl:(double)ID andUserID:(NSString *)UserID andBlock:(void(^)(S_CaseDetailBaseClass *caseModal,NSString *info))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/Verdicts/getverdictdetails",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSString stringWithFormat:@"%.0f",ID] forKey:@"ID"];
    [parameters setObject:UserID forKey:@"UserID"];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        //        NSLog(@"------%@",operation.responseString);
        if ([[dic objectForKey:@"Code"] intValue] == 200) {
            NSDictionary *dict = [dic objectForKey:@"Data"];
            S_CaseDetailBaseClass *baseCaseClass = [[S_CaseDetailBaseClass alloc] initWithDictionary:dict];
            if (baseCaseClass.ds.selectVerdicts.count != 0) {
                block(baseCaseClass,@"请求成功");
            }else{
                //                 NSLog(@"%@",baseCaseClass);
                block(nil,@"没有找到数据");
            }
            
            
        }else{
            block(nil,@"请求失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        if ([error code] == -1009){
            block(nil,@"请检查网络连接");
            
        }else{
            block(nil,@"请求失败");
        }
    }];
}


// 法条详情页面

+(void)lawDetailPageDataWithUrl:(double)ID andUserID:(NSString *)UserID andBlock:(void(^)(S_LawDetailData *lwaModal,NSString *info))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:KCaseOfLawDetail,K_IP];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:UserID forKey:@"UserID"];
    [parameters setObject:[NSString stringWithFormat:@"%.0f",ID] forKey:@"ID"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] intValue] == 200) {
            S_LawDetailBaseClass *basClass = [[S_LawDetailBaseClass alloc] initWithDictionary:dic];
            if(basClass.data.ds.selectArticles.count != 0){
                block(basClass.data,@"请求成功");
                
            }else{
                block(nil,@"没有找到数据");
            }
        }else{
            block(nil,@"请求失败");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([error code] == -1009){
            block(nil,@"请检查网络连接");
            
        }else{
            block(nil,@"请求失败");
        }
        
    }];
}

// 案例列表数据
+(void)caseTableData:(NSString *)str andOrder:(int)order andCurrtPage:(int)currtPage andCount:(int)count andBlock:(void(^)(S_LISTCase *caseModal,NSString *info))block
{
    
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/Verdicts/getverdictslist",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 30;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:str forKey:@"txtKeyWords1"];
    [parameters setObject:[NSString stringWithFormat:@"%d",order] forKey:@"SortBy"];    // 条件
    [parameters setObject:[NSString stringWithFormat:@"%d",currtPage] forKey:@"pageindex"]; // 第几页
    [parameters setObject:[NSString stringWithFormat:@"%d",count] forKey:@"pageSize"];      // 条数
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        if ([[dic objectForKey:@"Code"] isEqualToNumber:@200]) {
            NSDictionary *dict = [dic objectForKey:@"Data"];
            S_LISTCase *caseModal = [[S_LISTCase alloc] initWithDictionary:dict];
            if (caseModal.list.count != 0) {
                block(caseModal,@"请求成功");
            }else{
                block(caseModal,@"没找到数据");
            }
            
        }else{
            block(nil,@"请求失败");
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:error.userInfo[@"com.alamofire.serialization.response.error.data"] options:NSJSONReadingMutableContainers error:nil];
        BaseDataModel *errorModel = [[BaseDataModel alloc]init];
        [errorModel setValuesForKeysWithDictionary:dict];
        
    }];
}

//  http://192.168.1.18/api/Articles/getarticleslist

// 法条列表数据
+(void)lawTableData:(NSString *)str andOrder:(int)order andCurrtPage:(int)currtPage andCount:(int)count andBlock:(void(^)(S_LawListBaseClass *listModal,NSString *info))block
{
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/Articles/getarticleslist",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 30;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:str forKey:@"txtKeyWords1"];
    [parameters setObject:[NSString stringWithFormat:@"%d",order] forKey:@"SortBy"];    // 当前页数// 条件
    [parameters setObject:[NSString stringWithFormat:@"%d",currtPage] forKey:@"pageindex"]; // 第几页
    [parameters setObject:[NSString stringWithFormat:@"%d",count] forKey:@"pageSize"];      // 条数
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        NSLog(@"%@",operation.responseString);
        if ([[dic objectForKey:@"Message"] isEqualToString:@"获取数据成功."]) {
            NSLog(@"%@",operation.responseString);
            NSDictionary *dict = [dic objectForKey:@"Data"];
            S_LawListBaseClass *lawModal = [[S_LawListBaseClass alloc] initWithDictionary:dict];
            if (lawModal.list != 0) {
                block(lawModal,@"请求成功");
            }else{
                block(lawModal,@"没有找到数据");
            }
            
        }else{
            block(nil,@"请求失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"%@",error);
        if ([error code] == -1009){
            block(nil,@"请检查网络连接");
            
        }else{
            block(nil,@"请求失败");
        }
    }];
}

// http://192.168.1.18/api/Articles/cellecttionart
// 法条收藏
+(void)collectionData:(NSString *)userID andID:(int)ID andIsYes:(int)category andIsCollect:(int)collect andBlock:(void(^)(NSString *info))block;
{
    //    NSLog(@"%d   %d",category,collect);
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/Articles/cellecttionart",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:[NSString stringWithFormat:@"%d",ID] forKey:@"ID"];
    [parameters setObject:[NSString stringWithFormat:@"%d",category] forKey:@"category"];
    [parameters setObject:[NSString stringWithFormat:@"%d",collect] forKey:@"T"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            if (collect == 0) {
                block(@"取消收藏!");
            }else{
                block(@"收藏成功!");
            }
            
            
        }else{
            block(@"收藏失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        block(@"收藏失败");
    }];
}
/**
 *  案例收藏
 *
 *  @param userID   用户id
 *  @param ID       案例id
 *  @param category 0标记为案例
 *  @param collect  0收藏1取消收藏
 *  @param block
 */
//http://192.168.1.18/api/Verdicts/cellecttionver
+(void)collectionCaseData:(NSString *)userID andID:(int)ID andIsYes:(int)category andIsCollect:(int)collect andBlock:(void(^)(NSString *info))block;
{
    //    NSLog(@"%d   %d",category,collect);
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/Verdicts/cellecttionver",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:[NSString stringWithFormat:@"%d",ID] forKey:@"ID"];
    [parameters setObject:[NSString stringWithFormat:@"%d",category] forKey:@"category"];
    [parameters setObject:[NSString stringWithFormat:@"%d",collect] forKey:@"T"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            if (collect == 0) {
                block(@"取消收藏!");
            }else{
                block(@"收藏成功!");
            }
            
            
        }else{
            block(@"收藏失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        NSLog(@"%@",error);
        block(@"收藏失败");
    }];
}


// 个人主页
+(void)mainPerInfo:(NSString *)userID andBlock:(void(^)(S_PerMain_BaseClass *s_PerModal,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/getuserbyuserid",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            NSDictionary *dict = [dic objectForKey:@"Data"];
            if (dict.count != 0) {
                //                NSDictionary *dict = [dataArr objectAtIndex:0];
                S_PerMain_BaseClass *s_PerModal = [[S_PerMain_BaseClass alloc] initWithDictionary:dict];
                block(s_PerModal,@"请求成功");
            }
            
        }else{
            block(nil,@"请求失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        
    }];
    
}

// http://192.168.1.18/api/User/getuserinfo
// 用户基本信息
+(void)PerBaseInfo:(NSString *)userID andBlock:(void(^)(S_PerBaseInfoBaseClass *s_PerModal,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/getuserinfo",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        NSDictionary *dict = [dic objectForKey:@"Data"];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            S_PerBaseInfoBaseClass *s_PerModal = [[S_PerBaseInfoBaseClass alloc] initWithDictionary:dict];
            block(s_PerModal,@"请求成功");
            
        }else{
            block(nil,@"请求失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        block(nil,@"请求失败");
        
    }];
}

// http://192.168.1.18/api/User/updateuserinfonickname
// 编辑昵称；
+(void)editNIcName:(NSString *)userID andNicName:(NSString *)nicName andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/updateuserinfonickname",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:nicName forKey:@"NickName"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"成功");
            
        }else{
            block(@"修改失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"修改失败");
        
    }];
}

// 编辑简介；
//http://192.168.1.18/api/User/updateuserinfointroduction
+(void)editInfoMessage:(NSString *)userID andInfoMessage:(NSString *)message andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/updateuserinfointroduction",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:message forKey:@"Introduction"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"成功");
            
        }else{
            block(@"修改失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"修改失败");
        
    }];
}

// 编辑联系方式；
//http://192.168.1.18/api/User/updateuserinfotelphone
+(void)editphoneNumber:(NSString *)userID andphoneNumber:(NSString *)phoneNumber andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/updateuserinfotelphone",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:phoneNumber forKey:@"TelPhone"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"成功");
            
        }else{
            block(@"修改失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"修改失败");
        
    }];
}
// 上传头像和昵称
//http://192.168.1.18/api/User/updateUserinfoimagesandgender
+(void)chagneIcomAndSex:(NSString *)UserId andIcon:(UIImage *)icon andSex:(NSString *)sexStr andBlock:(void(^)(NSString *message))block{

    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/updateUserinfoimagesandgender",K_IP];
    NSData *tempData = UIImageJPEGRepresentation(icon, 0.7);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setObject:UserId forKey:@"UserID"];
    if (sexStr) {
        [parameters setObject:sexStr forKey:@"Gender"];
    }
    
    if (icon) {
        NSString *iconStr = [tempData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        [parameters setObject:iconStr forKey:@"Iamges"];
    }
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"成功");
            
        }else{
            block(@"修改失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"修改失败");
        
    }];
}

//http://192.168.1.18/api/User/adduserjob
// 添加工作经历
+(void)addJobMessageWithUser:(NSString *)UserID andCompanyName:(NSString *)companyName andjobName:(NSString *)jobName andJobTime:(NSString *)jobTime andArea:(NSString *)area andBlock:(void(^)(NSString *message))block
{
    NSString *begStr = nil;
    NSString *endStr = nil;
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/adduserjob",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:UserID forKey:@"UserID"];
    if (![jobTime isEqualToString:@"请选择"]) {
        NSArray *arr= [jobTime componentsSeparatedByString:@"-"];
        begStr = arr[0];
        endStr = arr[1];
        [parameters setObject:begStr forKey:@"BeginYear"];
        [parameters setObject:endStr forKey:@"EndYear"];
    }
    if (![area isEqualToString:@"请选择"]) {
        [parameters setObject:area forKey:@"Location"];
    }
    
    [parameters setObject:jobName forKey:@"Position"];
    [parameters setObject:companyName forKey:@"Company"];
    
    
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"成功");
            
        }else{
            block(@"添加失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"添加失败");
        
    }];
}
//http://192.168.1.18/api/User/deleteuserJob
// 删除工作经历
+(void)removewJobMessageWithID:(NSString *)Id andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/deleteuserJob",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:Id forKey:@"ID"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"成功");
            
        }else{
            block(@"删除失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"删除失败");
        
    }];
}

//http://192.168.1.18/api/User/updateuserjob
// 修改工作经历

+(void)changeJobMessageWithUser:(NSString *)ID andCompanyName:(NSString *)companyName andjobName:(NSString *)jobName andJobTime:(NSString *)jobTime andArea:(NSString *)area andBlock:(void(^)(NSString *message))block
{
    NSString *begStr = nil;
    NSString *endStr = nil;
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/updateuserjob",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:ID forKey:@"ID"];
    if (![jobTime isEqualToString:@"请选择"]) {
        NSArray *arr= [jobTime componentsSeparatedByString:@"-"];
        begStr = arr[0];
        endStr = arr[1];
        [parameters setObject:begStr forKey:@"BeginYear"];
        [parameters setObject:endStr forKey:@"EndYear"];
    }
    if (![area isEqualToString:@"请选择"] && area!= nil) {
        [parameters setObject:area forKey:@"Location"];
    }
    
    [parameters setObject:jobName forKey:@"Position"];
    [parameters setObject:companyName forKey:@"Company"];
    
    
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"成功");
            
        }else{
            block(@"修改失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"修改失败");
        
    }];
}

//http://192.168.1.18/api/User/addedcation
// 添加教育经历
+(void)addEduMessageWithUser:(NSString *)UserID andSchoolName:(NSString *)schoolName andstartTime:(NSString *)startTime andInstitute:(NSString *)institute andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/addedcation",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:UserID forKey:@"UserID"];
    [parameters setObject:schoolName forKey:@"University"];
    [parameters setObject:startTime forKey:@"BeginYear"];
    [parameters setObject:institute forKey:@"Institute"];
    
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"成功");
            
        }else{
            block(@"添加失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"添加失败");
        
    }];
}

// 修改教育经历

//http://192.168.1.18/api/User/updataeducation
+(void)changeEduMessageWithUser:(NSString *)ID andSchoolName:(NSString *)schoolName andstartTime:(NSString *)startTime andInstitute:(NSString *)institute andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/updataeducation",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:ID forKey:@"ID"];
    [parameters setObject:schoolName forKey:@"University"];
    [parameters setObject:startTime forKey:@"BeginYear"];
    [parameters setObject:institute forKey:@"Institute"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"成功");
            
        }else{
            block(@"修改失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"修改失败");
        
    }];
}
//http://192.168.1.18/api/User/deleteeducation
// 删除教育经历
+(void)removeEduMessageWithUser:(NSString *)ID andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/deleteeducation",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:ID forKey:@"ID"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            block(@"删除成功");
            
        }else{
            block(@"删除失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"删除失败");
        
    }];
}

//http://192.168.1.18/api/User/homepage
// 我的主页
+(void)myMainPageWithUserID:(NSString *)userID andOtherID:(NSString *)otherID andIsMySelf:(BOOL)isMySelf andBlock:(void(^)(S_MyMainPageBaseClass *mainPageModal,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/homepage",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    if (!isMySelf) {
        [parameters setObject:otherID forKey:@"OtherID"];
    }
    
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            NSDictionary *dict = [dic objectForKey:@"Data"];
            S_MyMainPageBaseClass *mainPageModal = [[S_MyMainPageBaseClass alloc] initWithDictionary:dict];
            block(mainPageModal,@"删除成功");
            
        }else{
            block(nil,@"删除失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(nil,@"删除失败");
        
    }];
}


//http://192.168.1.18/api/User/dynamiuser
//个人主页的动态
+(void)perMainPageHotWithUserID:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(S_PerHotBaseClass *perPageHotModal,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/dynamiuser",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:pageSize forKey:@"PageSize"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_PerHotBaseClass *perPageHotModal = [[S_PerHotBaseClass alloc] initWithDictionary:dic];
            block(perPageHotModal,@"加载成功");
            
        }else{
            block(nil,@"加载失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(nil,@"加载失败");
        
    }];
}



//http://192.168.1.18/api/User/myanswer
// 我的回答
+(void)myAnserWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/myanswer",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:pageSize forKey:@"PageSize"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_MyAnserBaseClass *myAnser = [[S_MyAnserBaseClass alloc] initWithDictionary:dic];
            block(myAnser.data,@"加载成功");
        }else{
            block(nil,@"加载失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(nil,@"加载失败");
        
    }];
}

//http://192.168.1.18/api/User/myfocususerlist
// 用户关注列表
+(void)perConcernList:(NSString *)userID andBlock:(void(^)(NSArray *dataArr,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/myfocususerlist",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    //    [parameters setObject:pageIndex forKey:@"PageIndex"];
    //    [parameters setObject:pageSize forKey:@"PageSize"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_perConcernListBaseClass *perConcernListModal = [[S_perConcernListBaseClass alloc] initWithDictionary:dic];
            block(perConcernListModal.data,@"加载成功");
        }else{
            block(nil,@"加载失败");
        }
        //
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(nil,@"加载失败");
        
    }];
}

//http://192.168.1.18/api/User/userfocususer
// 用户关注用户
+(void)perConcernOther:(NSString *)userID andOtherID:(NSString *)otherID andIsConcern:(NSString *)isConcern andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/userfocususer",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:otherID forKey:@"TagUserID"];
    [parameters setObject:isConcern forKey:@"T"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            NSLog(@"%@",[dic objectForKey:@"Data"]);
            NSDictionary *dict = [dic objectForKey:@"Data"];
            if ([dict count]!=0) {
                block(@"成功");
            }else{
                block(@"失败");
            }
            //            if (![[dic objectForKey:@"Message"] isEqualToString:@"没有获取到用户信息."]) {
            //
            //
            //            }else{
            //
            //            }
        }else{
            block(@"失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"失败");
        
    }];
}
//http://192.168.1.18/api/User/myquestiones
// 我的提问
+(void)myAskWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/myquestiones",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:pageSize forKey:@"PageSize"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_MYAskBaseClass *myAsk = [[S_MYAskBaseClass alloc] initWithDictionary:dic];
            block(myAsk.data,@"加载成功");
        }else{
            block(nil,@"加载失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(nil,@"加载失败");
        
    }];
}
//http://192.168.1.18/api/User/myfanslist
// 我的粉丝
+(void)myFansWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/myfanslist",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:pageSize forKey:@"PageSize"];
    //    NSLog(@"%@",pageIndex);
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_MyFansBaseClass *myFans = [[S_MyFansBaseClass alloc] initWithDictionary:dic];
            if (myFans.data.count != 0) {
                block(myFans.data,@"加载成功");
            }else{
                block(myFans.data,@"没有粉丝");
            }
        }else{
            block(nil,@"加载失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(nil,@"加载失败");
        
    }];
}

//http://192.168.1.18/api/User/mynotifylist
// 通知列表
+(void)myNotficWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/mynotifylist",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:pageSize forKey:@"PageSize"];
    //    NSLog(@"%@",pageIndex);
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"----%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_NotiFiCBaseClass *notiFic = [[S_NotiFiCBaseClass alloc] initWithDictionary:dic];
            if (notiFic.data.count != 0) {
                block(notiFic.data,@"加载成功");
            }else{
                block(notiFic.data,@"没有粉丝");
            }
        }else{
            block(nil,@"加载失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(nil,@"加载失败");
        
    }];
}

//http://192.168.1.18/api/User/myfiendlist
// 私信列表
+(void)myPrivateLetterWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/myfiendlist",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:pageIndex forKey:@"PageIndex"];
    [parameters setObject:pageSize forKey:@"PageSize"];
    //    NSLog(@"%@",pageIndex);
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"----%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_PrivateLetterBaseClass *letterList = [[S_PrivateLetterBaseClass alloc] initWithDictionary:dic];
            if (letterList.data.count != 0) {
                block(letterList.data,@"加载成功");
            }else{
                block(letterList.data,@"没有粉丝");
            }
        }else{
            block(nil,@"加载失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(nil,@"加载失败");
        
    }];
}

//http://192.168.1.18/api/User/searchfriend
// 搜索好友
+(void)searchFriend:(NSString *)userID andSearchStr:(NSString *)searchStr andBlock:(void(^)(NSArray *dataArr,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/searchfriend",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:searchStr forKey:@"NickName"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"----%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_SearchFriendBaseClass *searchFriendList = [[S_SearchFriendBaseClass alloc] initWithDictionary:dic];
            if (searchFriendList.data.count != 0) {
                block(searchFriendList.data,@"加载成功");
            }else{
                block(searchFriendList.data,@"没有粉丝");
            }
        }else{
            block(nil,@"加载失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        block(nil,@"加载失败");
        
    }];
}

//http://192.168.1.18/api/User/updatenotifystatus
// 修改通知状态；
+(void)changeNotStates:(NSString *)states andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/updatenotifystatus",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:states forKey:@"NotifyID"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //         NSLog(@"----%@",operation.responseString);
        //        NSLog(@"----%@",@"修改通知状态成功");
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
    }];
}

//http://192.168.1.18/api/User/personalmessagelist
//+ (void)chatListWithRecID:(NSString *)recID andSendID:(NSString *)sendID andPage:(NSString *)page andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block
// 私信列表
+ (void)chatListWithRecID:(NSString *)recID andSendID:(NSString *)sendID andBlock:(void(^)(NSArray *dataArr,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/personalmessagelist",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:recID forKey:@"RecID"];
    [parameters setObject:sendID forKey:@"SendID"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //         NSLog(@"----%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            S_ChatListBaseClass *chatList = [[S_ChatListBaseClass alloc] initWithDictionary:dic];
            if (chatList.data.count != 0) {
                block(chatList.data,@"加载成功");
            }else{
                block(chatList.data,@"没有聊天记录");
            }
        }else{
            block(nil,@"加载失败");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(nil,@"加载失败");
    }];
}

// http://192.168.1.18/api/User/personalmessage
//category   1,表示私信，2，表示通知
// 发私信
+ (void)sendMessage:(NSString *)recID andSendID:(NSString *)sendID andTextConent:(NSString *)textConent andCategory:(NSString *)category andBlock:(void(^)(NSString *textConent,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/personalmessage",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:recID forKey:@"RecID"];
    [parameters setObject:sendID forKey:@"SendID"];
    [parameters setObject:@"my Message" forKey:@"Title"];
    [parameters setObject:textConent forKey:@"TextContent"];
    [parameters setObject:@"1" forKey:@"Category"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"----%@",operation.responseString);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
    }];
}

//http://192.168.1.18/api/User/unreadmessagecount
// 我的未读私信数
+ (void)myMessageCountWithUserID:(NSString *)userID andBlock:(void(^)(NSString *count))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/unreadmessagecount",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"----%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            NSString *count = [NSString stringWithFormat:@"%@",[self objectOrNilForKey:@"Data" fromDictionary:dic]];
            block(count);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
    }];
}

+ (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

//http://192.168.1.18/api/User/deletemessage
// 删除私信
+ (void)deleteAllMessageWithSendID:(NSString *)sendID andRecID:(NSString *)recID andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/deletemessage",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:recID forKey:@"RecID"];
    [parameters setObject:sendID forKey:@"SendID"];
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"----%@",operation.responseString);
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            block(@"删除成功");
        }else{
            block(@"删除失败");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"%@",error);
        block(@"删除失败");
    }];
}

//http://192.168.1.18/api/User/addfeedback
// 添加反馈问题
+ (void)myQuestionWithUserID:(NSString *)userID andQuestionText:(NSString *)questionText andPhoneNumber:(NSString *)phoneNUmber andImageArr:(NSArray *)imageArr andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/addfeedback",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    NSString *tempStr = nil;
    for (int i = 0; i < imageArr.count; i++) {
        UIImage *tempImage = imageArr[i];
        NSData *tempData = UIImageJPEGRepresentation(tempImage, 0.7);
        NSString *iconStr = [tempData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        if (tempStr == nil) {
            tempStr = iconStr;
        }else{
            tempStr = [NSString stringWithFormat:@"%@|%@",tempStr,iconStr];
        }
    }
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:phoneNUmber forKey:@"TelPhone"];
    [parameters setObject:questionText forKey:@"Remark"];
    if (tempStr != nil) {
        [parameters setObject:tempStr forKey:@"ImageUrl"];
    }
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"----%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            block(@"提交成功");
        }else{
            block(@"提交失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        NSLog(@"%@",error);
        block(@"提交失败");
        
    }];
}
//http://10.0.0.51/api/user/getversion
// 检查版本更新
+ (void)upadeversionWith:(NSString *)category andBlock:(void(^)(NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/user/getversion",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:category forKey:@"Category"];
    
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"----%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            NSDictionary *dict = [self objectOrNilForKey:@"Data" fromDictionary:dic];
            if (dict  == nil) {
                block(@"");
            }else{
                if ([self objectOrNilForKey:@"Version" fromDictionary:dict]) {
                    block([dict objectForKey:@"Version"]);
                }else{
                    block(@"");
                }
            }
            
        }else{
            block(@"");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        block(@"");
        
    }];
}

//#pragma mark - Helper Method
//+ (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
//{
//    id object = [dict objectForKey:aKey];
//    return [object isEqual:[NSNull null]] ? nil : object;
//}

//http://192.168.1.18/api/T_Topicinfo/getsetpushinfo
// 获取消息推送设置所有状态
+ (void)getAllStatusWithUserID:(NSString *)userID andBlock:(void (^)(Push_AllStatusBaseClass *pushModal,NSString *message))block
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/getsetpushinfo",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"----%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]] && [[dic objectForKey:@"Message"] isEqualToString:@"获取数据成功."] ) {
            NSDictionary *dict = [dic objectForKey:@"Data"];
            Push_AllStatusBaseClass *pushModal = [[Push_AllStatusBaseClass alloc] initWithDictionary:dict];
            block(pushModal,@"成功");
            
        }else{
            //            block(nil,@"失败");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        NSLog(@"%@",error);
        //        block(nil,@"失败");
        
    }];
}

//http://192.168.1.18/api/T_Topicinfo/setpushmessage
// 设置是否提醒
+ (void)setStatusWithUserID:(NSString *)userID andFocusNewAnwser:(int)focusNewAnwser andFocusMe:(int)FocusMe andPraiseMe:(int)PraiseMe andSendMe:(int)SendMe andCommentMe:(int)CommentMe
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/setpushmessage",K_IP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"UserID"];
    [parameters setObject:[NSString stringWithFormat:@"%d",focusNewAnwser] forKey:@"FocusNewAnwser"]; // 我关注的问题有了新答案
    [parameters setObject:[NSString stringWithFormat:@"%d",FocusMe] forKey:@"FocusMe"];    // 有人关注我
    [parameters setObject:[NSString stringWithFormat:@"%d",PraiseMe] forKey:@"PraiseMe"]; // 有人赞同了我
    [parameters setObject:[NSString stringWithFormat:@"%d",SendMe] forKey:@"SendMe"]; // 有人发私信给我
    [parameters setObject:[NSString stringWithFormat:@"%d",CommentMe] forKey:@"CommentMe"];// 有人评论了我
    
    //发送请求
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"----%@",operation.responseString);
        //        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        //        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]] && [[dic objectForKey:@"Message"] isEqualToString:@"获取数据成功."] ) {
        //            NSDictionary *dict = [dic objectForKey:@"Data"];
        //            Push_AllStatusBaseClass *pushModal = [[Push_AllStatusBaseClass alloc] initWithDictionary:dict];
        //            block(pushModal,@"成功");
        //            
        //        }else{
        //
        //        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        NSLog(@"%@",error);
        //        block(nil,@"失败");
        
    }];
}

@end
