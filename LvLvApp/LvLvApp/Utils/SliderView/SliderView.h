//
//  SliderView.h
//  LvLvApp
//
//  Created by jim on 15/12/27.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^Block)(void);
@interface SliderView : UIView
@property (nonatomic,strong) UISlider *slider;
@property (nonatomic,assign) CGFloat width;
@property (nonatomic,copy) Block refreshBlock;
@end
