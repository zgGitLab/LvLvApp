//
//  EditTimeView.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "EditTimeView.h"

@interface EditTimeView ()
{
    NSString *tempStr;
    NSString *tempStrTow;
}
@end

@implementation EditTimeView

-(id)initWithCount:(int)count{
    self = [super init];
    if (self) {
       NSString *path = [[NSBundle mainBundle] pathForResource:@"datePlist.plist" ofType:nil];
        self.plistArray = [NSArray arrayWithContentsOfFile:path];
//        self.plistArray=[self getPlistArrayByplistName:@"editCity"];
        self.count = count;
        //初始化背景视图，添加手势
        self.frame = CGRectMake(0, 0, SIZE.width, SIZE.height);
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.4];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
        [self addGestureRecognizer:tapGesture];
        self.backGroundView = [[UIView alloc] initWithFrame:CGRectMake(0, ([UIScreen mainScreen].bounds.size.height), SIZE.width, 0)];
        self.backGroundView.backgroundColor = [UIColor whiteColor];
        
        self.picKView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, SIZE.width, 196)];
        
        self.picKView.delegate = self;
        self.picKView.dataSource = self;
        [self.backGroundView addSubview:self.picKView];
        
        UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 44)];
        toolBar.backgroundColor = COLOR(246, 247, 249);
        toolBar.barStyle = UIBarStyleDefault;
        UIButton  *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnRight addTarget:self action:@selector(finsh) forControlEvents:UIControlEventTouchUpInside];
        [btnRight setTitleColor:COLOR(59, 57, 57) forState:UIControlStateNormal];
        [btnRight setTitle:@"完成" forState:UIControlStateNormal];
        [btnRight setFrame:CGRectMake(0, 0, 60, 30)];
        UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
        UIButton  *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnLeft addTarget:self action:@selector(docancel) forControlEvents:UIControlEventTouchUpInside];
        [btnLeft setTitleColor:COLOR(59, 57, 57) forState:UIControlStateNormal];
        [btnLeft setTitle:@"取消" forState:UIControlStateNormal];
        [btnLeft setFrame:CGRectMake(0, 0, 60, 30)];
        UIBarButtonItem *leftButton  = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
        UIBarButtonItem *fixedButton  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace target: nil action: nil];
        NSArray *array = [[NSArray alloc] initWithObjects: leftButton,fixedButton,rightButton, nil];
        [toolBar setItems: array];
        [self.backGroundView addSubview:toolBar];
        [self addSubview:self.backGroundView];
        [UIView animateWithDuration:0.25 animations:^{
            [self.backGroundView setFrame:CGRectMake(0, SIZE.height - 240, SIZE.width, 240)];
            
        } completion:^(BOOL finished) {
            
        }];
    }
    
    return self;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return self.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return self.plistArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
   
    return self.plistArray[row];
}

//- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component animated:(BOOL)animated
//{
//    
//}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    // 当滑动第一个区的时候刷新第二个区的内容
    if (component==0) {
        tempStr = self.plistArray[row];
        NSLog(@"%@",tempStr);
        
    }else{
        tempStrTow = self.plistArray[row];
         NSLog(@"%@",tempStrTow);
    }
    if (self.count == 2) {
        self.selectStr = [NSString stringWithFormat:@"%@ - %@",tempStr,tempStrTow];
    }else{
        self.selectStr = tempStr;
    }
    
}

- (void)finsh
{
    NSLog(@"%@",self.selectStr);
    [self.delegate selectTimeDate:self.selectStr];
    [self docancel];
}

- (void)tapGesture
{
    [self docancel];
}

- (void)docancel
{
    [UIView animateWithDuration:0.25 animations:^{
        [self.backGroundView setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 0)];
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

-(void)showInView
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app.window addSubview: self];
    NSDate *date = [NSDate date];
    NSDateFormatter *format=[[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy"];
    NSString *dateStr = [format stringFromDate:date];
    int selectRow = 0;
    for (int i = 0; i<self.plistArray.count; i++) {
        if ([self.plistArray[i] isEqualToString:dateStr]) {
            selectRow = i;
            tempStr = self.plistArray[i];
            tempStrTow = self.plistArray[i];
             self.selectStr = tempStr;
            if (self.count == 2) {
                self.selectStr = [NSString stringWithFormat:@"%@ - %@",tempStr,tempStrTow];
            }
        }
    }
    if (self.count == 1) {
        [self.picKView selectRow:selectRow inComponent:0 animated:NO];
    }else{
        [self.picKView selectRow:selectRow inComponent:0 animated:NO];
        [self.picKView selectRow:selectRow inComponent:1 animated:NO];
    }
    

}

@end
