//
//  S_NotiFiCData.h
//
//  Created by IOS8  on 15/12/21
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_NotiFiCData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *tID;
@property (nonatomic, assign) double userid;
@property (nonatomic, assign) double notifyID;
@property (nonatomic, strong) NSString *dataDescription;
@property (nonatomic, strong) id timagesUrl;
@property (nonatomic, strong) NSString *logoUrl;
@property (nonatomic, strong) id questionContent;
@property (nonatomic, assign) double collectCount;
@property (nonatomic, strong) NSString *sortTime;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, assign) double counts;
@property (nonatomic, strong) NSString *answerContent;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double commentCount;
@property (nonatomic, strong) NSString *times;
@property (nonatomic, assign) double types;
@property (nonatomic, strong) NSString *answerID;
@property (nonatomic, strong) NSString *AUserID;
@property (nonatomic, strong) NSString *TUserID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
