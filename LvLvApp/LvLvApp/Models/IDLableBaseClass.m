//
//  IDLableBaseClass.m
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IDLableBaseClass.h"
#import "IDLableData.h"


NSString *const kIDLableBaseClassCode = @"Code";
NSString *const kIDLableBaseClassMessage = @"Message";
NSString *const kIDLableBaseClassData = @"Data";


@interface IDLableBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IDLableBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kIDLableBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kIDLableBaseClassMessage fromDictionary:dict];
    NSObject *receivedIDLableData = [dict objectForKey:kIDLableBaseClassData];
    NSMutableArray *parsedIDLableData = [NSMutableArray array];
    if ([receivedIDLableData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedIDLableData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedIDLableData addObject:[IDLableData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedIDLableData isKindOfClass:[NSDictionary class]]) {
       [parsedIDLableData addObject:[IDLableData modelObjectWithDictionary:(NSDictionary *)receivedIDLableData]];
    }

    self.data = [NSArray arrayWithArray:parsedIDLableData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kIDLableBaseClassCode];
    [mutableDict setValue:self.message forKey:kIDLableBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kIDLableBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kIDLableBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kIDLableBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kIDLableBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kIDLableBaseClassCode];
    [aCoder encodeObject:_message forKey:kIDLableBaseClassMessage];
    [aCoder encodeObject:_data forKey:kIDLableBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    IDLableBaseClass *copy = [[IDLableBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
