//
//  HLableBaseClass.m
//
//  Created by hope  on 15/12/5
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "HLableBaseClass.h"
#import "HLableData.h"


NSString *const kHLableBaseClassCode = @"Code";
NSString *const kHLableBaseClassMessage = @"Message";
NSString *const kHLableBaseClassData = @"Data";


@interface HLableBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HLableBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kHLableBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kHLableBaseClassMessage fromDictionary:dict];
            self.data = [HLableData modelObjectWithDictionary:[dict objectForKey:kHLableBaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kHLableBaseClassCode];
    [mutableDict setValue:self.message forKey:kHLableBaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kHLableBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kHLableBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kHLableBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kHLableBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kHLableBaseClassCode];
    [aCoder encodeObject:_message forKey:kHLableBaseClassMessage];
    [aCoder encodeObject:_data forKey:kHLableBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    HLableBaseClass *copy = [[HLableBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
