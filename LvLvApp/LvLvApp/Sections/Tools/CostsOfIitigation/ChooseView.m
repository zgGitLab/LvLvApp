//
//  ChooseView.m
//  HPCostsCaluclate
//
//  Created by yfzx_sh_gaoyy on 15/9/16.
//  Copyright (c) 2015年 mark.ma. All rights reserved.
//

#import "ChooseView.h"

@implementation ChooseView
{
    NSMutableArray *tableData;
    UITableView *tableview;
}

@synthesize showTable;
@synthesize delegate;
@synthesize dataArray;

- (void)show{
    self.hidden = NO;
    showTable = YES;
    tableview.alpha = 1;
    NSInteger count = (tableData.count+1) > 4 ? 4 :(tableData.count+1);
    [UIView animateWithDuration:0.3 animations:^{
        [tableview setFrame:CGRectMake(0.0, 0.0, self.frame.size.width,count * 35+300)];
    } completion:^(BOOL finished) {
        [tableview reloadData];
    }];
}

- (void)dismiss{
    showTable = NO;
    [UIView animateWithDuration:0.3 animations:^{
        [tableview setFrame:CGRectMake(0.0, 0.0, self.frame.size.width,0.0)];
    } completion:^(BOOL finished) {
        tableview.alpha = 0;
        self.hidden = YES;
    }];
}

- (void)setDataArray:(NSArray *)_dataArray{
    dataArray = _dataArray;
    if (dataArray) {
        
        [tableData removeAllObjects];
        [tableData addObjectsFromArray:dataArray];
        
        [tableview reloadData];
    }
}


#pragma mark - UITableViewDelegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellidentifier = @"cellidentifier";
    
    UITableViewCell *cell = (UITableViewCell *)[tableview dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    
    cell.textLabel.text = tableData[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
    cell.textLabel.numberOfLines= 0;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    cell.backgroundColor =[UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableview deselectRowAtIndexPath:indexPath animated:YES];
    
    [self dismiss];
    
    if (delegate && [delegate respondsToSelector:@selector(chooseViewSelectInfo:)]) {
        [delegate chooseViewSelectInfo:[tableData objectAtIndex:indexPath.row]];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
    
}
#pragma mark - DrawUI

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        self.hidden = YES;
        
        tableData = [[NSMutableArray alloc] init];
        
        tableview = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, 0.0) style:UITableViewStylePlain];
        tableview.delegate = self;
        tableview.dataSource = self;
        tableview.alpha = 0;
        [self addSubview:tableview];
        
        if ([tableview respondsToSelector:@selector(setSeparatorInset:)]) {
            tableview.separatorInset = UIEdgeInsetsZero;
        }
        if ([tableview respondsToSelector:@selector(setLayoutMargins:)]) {
            tableview.layoutMargins = UIEdgeInsetsZero;
        }
        
        showTable = NO;
    }
    return self;
}


@end
