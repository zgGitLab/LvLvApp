//
//  SingeData.h
//  LvLvApp
//
//  Created by jim on 15/12/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingeData : NSObject
@property (nonatomic,strong) NSMutableArray *labelDataArray;
@property (nonatomic,assign) CGFloat answerContentSize;//回答内容的字体大小
+ (SingeData *)sharedInstance;

@property (nonatomic,assign) NSInteger isLogin;
@end
