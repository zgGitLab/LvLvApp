//
//  CustomLabel.h
//  LvLvApp
//
//  Created by jim on 15/12/30.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum{
    VerticalAlignmentTop = 0,
    VerticalAlignmentMidele,
    VerticalAlignmentBottom,
    VerticalAlignmentMax
}VerticalAlignment;


@interface CustomLabel : UILabel
{
    VerticalAlignment _verticalAlignment;
}

@property (nonatomic, assign)VerticalAlignment verticalAlignment;
@end
