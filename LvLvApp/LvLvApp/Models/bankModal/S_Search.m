//
//  S_Search.m
//
//  Created by IOS8  on 15/11/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_Search.h"


NSString *const kS_SearchID = @"ID";
NSString *const kS_SearchOrder = @"order";
NSString *const kS_SearchCount = @"count";
NSString *const kS_SearchName = @"name";
NSString *const kS_SearchType = @"type";
NSString *const kS_SearchParentID = @"parentID";


@interface S_Search ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_Search

@synthesize iDProperty = _iDProperty;
@synthesize order = _order;
@synthesize count = _count;
@synthesize name = _name;
@synthesize type = _type;
@synthesize parentID = _parentID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kS_SearchID fromDictionary:dict] doubleValue];
            self.order = [[self objectOrNilForKey:kS_SearchOrder fromDictionary:dict] doubleValue];
            self.count = [[self objectOrNilForKey:kS_SearchCount fromDictionary:dict] doubleValue];
            self.name = [self objectOrNilForKey:kS_SearchName fromDictionary:dict];
            self.type = [[self objectOrNilForKey:kS_SearchType fromDictionary:dict] doubleValue];
            self.parentID = [[self objectOrNilForKey:kS_SearchParentID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kS_SearchID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.order] forKey:kS_SearchOrder];
    [mutableDict setValue:[NSNumber numberWithDouble:self.count] forKey:kS_SearchCount];
    [mutableDict setValue:self.name forKey:kS_SearchName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kS_SearchType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.parentID] forKey:kS_SearchParentID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kS_SearchID];
    self.order = [aDecoder decodeDoubleForKey:kS_SearchOrder];
    self.count = [aDecoder decodeDoubleForKey:kS_SearchCount];
    self.name = [aDecoder decodeObjectForKey:kS_SearchName];
    self.type = [aDecoder decodeDoubleForKey:kS_SearchType];
    self.parentID = [aDecoder decodeDoubleForKey:kS_SearchParentID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kS_SearchID];
    [aCoder encodeDouble:_order forKey:kS_SearchOrder];
    [aCoder encodeDouble:_count forKey:kS_SearchCount];
    [aCoder encodeObject:_name forKey:kS_SearchName];
    [aCoder encodeDouble:_type forKey:kS_SearchType];
    [aCoder encodeDouble:_parentID forKey:kS_SearchParentID];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_Search *copy = [[S_Search alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.order = self.order;
        copy.count = self.count;
        copy.name = [self.name copyWithZone:zone];
        copy.type = self.type;
        copy.parentID = self.parentID;
    }
    
    return copy;
}


@end
