//
//  ConfirmViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/10.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "ConfirmViewController.h"
#import "M_Profession.h"
#import "MBProgressHUD+MJ.h"
#import "MBProgressHUD.h"
#import "CheckViewController.h"

@interface ConfirmViewController ()
{
    NSString *imaStr;
    UIView *keyboardView;
    NSData *fileData;

}

@end

@implementation ConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"身份认证";
    self.ConScrollView.alwaysBounceVertical = YES;
    self.ConScrollView.contentSize = CGSizeMake(SIZE.width, CGRectGetMaxY(self.ConBut.frame)+20);
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    //[self.ComImportComrpanys setTitle:[defaults objectForKey:@"ComImportComrpanys"] forState: UIControlStateNormal ];
    self.ComImportComrpanys.titleLabel.text =[defaults objectForKey:@"ComImportComrpanys"];
    self.ComImportName.text = [defaults objectForKey:@"ComImportName"];
    self.ComImportComrpany.text =[defaults objectForKey:@"ComImportComrpany"];

    self.ComUpImageView.image =[UIImage imageWithData:[defaults objectForKey:@"imageUrl"]];
    NSLog(@"%@",self.ComUpImageView.image);
    [self.ComImportComrpanys setTitleColor:[UIColor colorWithRed:205.0/255.0 green:205.0/255.0 blue:210.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.ComImportComrpanys addTarget:self action:@selector(ImportCompanysAction:) forControlEvents:UIControlEventTouchUpInside];
    
    keyboardView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 36)];
    [keyboardView setBackgroundColor:[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1]];
    
    
    UIButton *key = [UIButton buttonWithType:UIButtonTypeSystem];
    key.frame = CGRectMake(kWidth-26-15, 7, 26, 26);
    [key setImage:[[UIImage imageNamed:@"收键盘"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [key addTarget:self action:@selector(collectKeyBoard:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:key];
    self.ComImportComrpany.inputAccessoryView =keyboardView;
    self.ComImportName.inputAccessoryView =keyboardView;
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem*butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(returnButton)];
    ;
    butItem.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
}


- (void)returnButton
{
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)collectKeyBoard:(UIButton *)button{
    [self.ComImportComrpany resignFirstResponder];
    [self.ComImportName resignFirstResponder];
}

//选择职业
-(void)ImportCompanysAction:(UIButton *)btn
{
    [self.view endEditing:YES];
     M_Profession *ProfessionView =[[M_Profession alloc]init];
     ProfessionView.delegate =self;
    
    [ProfessionView showInView];
    NSUserDefaults *defaultss = [NSUserDefaults standardUserDefaults];
    [defaultss setObject:self.ComImportComrpanys.titleLabel.text forKey:@"ComImportComrpanys"];
    [defaultss setObject:self.ComImportName.text forKey:@"ComImportName"];
    [defaultss setObject:self.ComImportComrpany.text forKey:@"ComImportComrpany"];
    [defaultss setObject:fileData forKey:@"imageUrl"];
    [defaultss synchronize];
}

//上传资料
- (void)idIssuerequest :(NSString *)ComUpImageView andComImportName:(NSString*)ComImportName :(NSString *)ComImportComrpany andComImport:(NSString *)ComImportComrpanys andUser :(NSString *)UserID{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
        [parm setObject:UserID forKey:@"UserID"];
    [parm setObject:ComImportComrpanys forKey:@"Profession"];
    NSLog(@"%@",self.ComImportComrpanys.titleLabel.text);
    [parm setObject:ComUpImageView forKey:@"ImageUrl"];
    [parm setObject:ComImportName forKey:@"UserName"];
    [parm setObject:ComImportComrpany forKey:@"CompanyName"];
  
    [manager POST:[NSString stringWithFormat:KidentityUrk,K_IP] parameters:parm success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",operation.responseString);
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        if ([[dic objectForKey:@"Code"] isEqual:[NSNumber numberWithInt:200]]) {
            
            NSLog(@"成功");
            CheckViewController *checkView =[[CheckViewController alloc]init];
            [self.navigationController pushViewController:checkView animated:YES];
        }else{
            NSLog(@"添加失败");
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

//上传图片按钮
- (IBAction)ComReferBut:(UIButton *)sender {
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*UserID = [defaults objectForKey:@"users"];
    
    [self.view endEditing:YES];
    if (imaStr == nil) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showError:@"请上传证件照!"];
        return;
    }
    if (self.ComImportName.text ==nil) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showError:@"请输入真实姓名!"];
        return;
    }
    if (self.ComImportComrpany.text ==nil) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showError:@"请输入单位名称!"];
        return;
    }
    if (self.ComImportComrpanys.titleLabel.text
        ==nil) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showError:@"请选择身份!"];
        return;
    }
    
    [self idIssuerequest:imaStr andComImportName:self.ComImportName.text : self.ComImportComrpany.text andComImport:self.ComImportComrpanys.titleLabel.text andUser:UserID];
}

//提交资料
- (IBAction)ComUpImagAction:(id)sender {
     [self.view endEditing:YES];
     PerPickImageView *pickImage = [[PerPickImageView alloc] initWithId:1];
     pickImage.delegate = self;
     [pickImage showInView];
}

#pragma mark PerPickViewDelegat
- (void)jumpOtherView:(UIImagePickerController *)object
{
    NSLog(@"aaa");
    // 点击按钮跳转到能够利用的图片来源
    [self presentViewController:object animated:YES completion:nil];
}

- (void)selectImage:(id)icon
{
    if ([icon isKindOfClass:[UIImage class]]) {
        NSLog(@"头像");
        self.ComUpImageView.image = (UIImage *)icon;
        
        self.ComUpImageView.contentMode =  UIViewContentModeScaleToFill;
        fileData = UIImageJPEGRepresentation((UIImage *)icon, 0.5);
        imaStr = [fileData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    }
    
}

- (void)selectareaStr:(NSString *)areaStr
{
    [self.ComImportComrpanys setTitle:areaStr forState:UIControlStateNormal];

}

//消失键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
