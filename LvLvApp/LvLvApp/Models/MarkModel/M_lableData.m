//
//  M_lableData.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_lableData.h"
#import "M_lableListResult.h"


NSString *const kM_lableDataTotalTag = @"TotalTag";
NSString *const kM_lableDataTotalComment = @"TotalComment";
NSString *const kM_lableDataIsfcous = @"Isfcous";
NSString *const kM_lableDataTotalHeader = @"TotalHeader";
NSString *const kM_lableDataTotalAnswer = @"TotalAnswer";
NSString *const kM_lableDataListResult = @"listResult";


@interface M_lableData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_lableData

@synthesize totalTag = _totalTag;
@synthesize totalComment = _totalComment;
@synthesize isfcous = _isfcous;
@synthesize totalHeader = _totalHeader;
@synthesize totalAnswer = _totalAnswer;
@synthesize listResult = _listResult;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.totalTag = [[self objectOrNilForKey:kM_lableDataTotalTag fromDictionary:dict] doubleValue];
            self.totalComment = [[self objectOrNilForKey:kM_lableDataTotalComment fromDictionary:dict] doubleValue];
            self.isfcous = [[self objectOrNilForKey:kM_lableDataIsfcous fromDictionary:dict] doubleValue];
            self.totalHeader = [[self objectOrNilForKey:kM_lableDataTotalHeader fromDictionary:dict] doubleValue];
            self.totalAnswer = [[self objectOrNilForKey:kM_lableDataTotalAnswer fromDictionary:dict] doubleValue];
    NSObject *receivedM_lableListResult = [dict objectForKey:kM_lableDataListResult];
    NSMutableArray *parsedM_lableListResult = [NSMutableArray array];
    if ([receivedM_lableListResult isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedM_lableListResult) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedM_lableListResult addObject:[M_lableListResult modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedM_lableListResult isKindOfClass:[NSDictionary class]]) {
       [parsedM_lableListResult addObject:[M_lableListResult modelObjectWithDictionary:(NSDictionary *)receivedM_lableListResult]];
    }

    self.listResult = [NSArray arrayWithArray:parsedM_lableListResult];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalTag] forKey:kM_lableDataTotalTag];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalComment] forKey:kM_lableDataTotalComment];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isfcous] forKey:kM_lableDataIsfcous];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalHeader] forKey:kM_lableDataTotalHeader];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalAnswer] forKey:kM_lableDataTotalAnswer];
    NSMutableArray *tempArrayForListResult = [NSMutableArray array];
    for (NSObject *subArrayObject in self.listResult) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForListResult addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForListResult addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForListResult] forKey:kM_lableDataListResult];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.totalTag = [aDecoder decodeDoubleForKey:kM_lableDataTotalTag];
    self.totalComment = [aDecoder decodeDoubleForKey:kM_lableDataTotalComment];
    self.isfcous = [aDecoder decodeDoubleForKey:kM_lableDataIsfcous];
    self.totalHeader = [aDecoder decodeDoubleForKey:kM_lableDataTotalHeader];
    self.totalAnswer = [aDecoder decodeDoubleForKey:kM_lableDataTotalAnswer];
    self.listResult = [aDecoder decodeObjectForKey:kM_lableDataListResult];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_totalTag forKey:kM_lableDataTotalTag];
    [aCoder encodeDouble:_totalComment forKey:kM_lableDataTotalComment];
    [aCoder encodeDouble:_isfcous forKey:kM_lableDataIsfcous];
    [aCoder encodeDouble:_totalHeader forKey:kM_lableDataTotalHeader];
    [aCoder encodeDouble:_totalAnswer forKey:kM_lableDataTotalAnswer];
    [aCoder encodeObject:_listResult forKey:kM_lableDataListResult];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_lableData *copy = [[M_lableData alloc] init];
    
    if (copy) {

        copy.totalTag = self.totalTag;
        copy.totalComment = self.totalComment;
        copy.isfcous = self.isfcous;
        copy.totalHeader = self.totalHeader;
        copy.totalAnswer = self.totalAnswer;
        copy.listResult = [self.listResult copyWithZone:zone];
    }
    
    return copy;
}


@end
