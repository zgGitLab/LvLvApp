//
//  SingeData.m
//  LvLvApp
//
//  Created by jim on 15/12/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "SingeData.h"
static SingeData *sharedObj = nil;



@implementation SingeData

+ (SingeData *)sharedInstance{
    @synchronized(self) {
        if (sharedObj == nil) {
            sharedObj = [[self alloc]init];
        }
    }
    return sharedObj;
}


+ (id) allocWithZone:(NSZone *)zone
{
    @synchronized (self) {
        if (sharedObj == nil) {
            sharedObj = [super allocWithZone:zone];
            return sharedObj;
        }
    }
    return nil;
}


- (id) copyWithZone:(NSZone *)zone
{
    return self;
}


- (id)init
{
    @synchronized(self) {
        self = [super init];//往往放一些要初始化的变量.
        self.labelDataArray = [[NSMutableArray alloc]init];
        return self;
    }
}
@end
