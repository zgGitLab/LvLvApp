//
//  S_List.m
//
//  Created by IOS8  on 15/11/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_List.h"


NSString *const kS_ListId = @"id";
NSString *const kS_ListJudgeDate = @"judge_date";
NSString *const kS_ListTitleHighLighter = @"TitleHighLighter";
NSString *const kS_ListAbstract = @"Abstract";
NSString *const kS_ListCourtId = @"court_id";
NSString *const kS_ListCourt = @"court";
NSString *const kS_ListCreatedAt = @"created_at";
NSString *const kS_ListUrl = @"url";
NSString *const kS_ListTitle = @"title";
NSString *const kS_ListLevel = @"level";
NSString *const kS_ListReason = @"reason";
NSString *const kS_ListCharacter = @"character";
NSString *const kS_ListContent = @"content";


@interface S_List ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_List

@synthesize listIdentifier = _listIdentifier;
@synthesize judgeDate = _judgeDate;
@synthesize titleHighLighter = _titleHighLighter;
@synthesize abstract = _abstract;
@synthesize courtId = _courtId;
@synthesize court = _court;
@synthesize createdAt = _createdAt;
@synthesize url = _url;
@synthesize title = _title;
@synthesize level = _level;
@synthesize reason = _reason;
@synthesize character = _character;
@synthesize content = _content;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.listIdentifier = [[self objectOrNilForKey:kS_ListId fromDictionary:dict] doubleValue];
            self.judgeDate = [self objectOrNilForKey:kS_ListJudgeDate fromDictionary:dict];
            self.titleHighLighter = [self objectOrNilForKey:kS_ListTitleHighLighter fromDictionary:dict];
            self.abstract = [self objectOrNilForKey:kS_ListAbstract fromDictionary:dict];
            self.courtId = [[self objectOrNilForKey:kS_ListCourtId fromDictionary:dict] doubleValue];
            self.court = [self objectOrNilForKey:kS_ListCourt fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kS_ListCreatedAt fromDictionary:dict];
            self.url = [self objectOrNilForKey:kS_ListUrl fromDictionary:dict];
            self.title = [self objectOrNilForKey:kS_ListTitle fromDictionary:dict];
            self.level = [self objectOrNilForKey:kS_ListLevel fromDictionary:dict];
            self.reason = [self objectOrNilForKey:kS_ListReason fromDictionary:dict];
            self.character = [self objectOrNilForKey:kS_ListCharacter fromDictionary:dict];
            self.content = [self objectOrNilForKey:kS_ListContent fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.listIdentifier] forKey:kS_ListId];
    [mutableDict setValue:self.judgeDate forKey:kS_ListJudgeDate];
    [mutableDict setValue:self.titleHighLighter forKey:kS_ListTitleHighLighter];
    [mutableDict setValue:self.abstract forKey:kS_ListAbstract];
    [mutableDict setValue:[NSNumber numberWithDouble:self.courtId] forKey:kS_ListCourtId];
    [mutableDict setValue:self.court forKey:kS_ListCourt];
    [mutableDict setValue:self.createdAt forKey:kS_ListCreatedAt];
    [mutableDict setValue:self.url forKey:kS_ListUrl];
    [mutableDict setValue:self.title forKey:kS_ListTitle];
    [mutableDict setValue:self.level forKey:kS_ListLevel];
    [mutableDict setValue:self.reason forKey:kS_ListReason];
    [mutableDict setValue:self.character forKey:kS_ListCharacter];
    [mutableDict setValue:self.content forKey:kS_ListContent];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.listIdentifier = [aDecoder decodeDoubleForKey:kS_ListId];
    self.judgeDate = [aDecoder decodeObjectForKey:kS_ListJudgeDate];
    self.titleHighLighter = [aDecoder decodeObjectForKey:kS_ListTitleHighLighter];
    self.abstract = [aDecoder decodeObjectForKey:kS_ListAbstract];
    self.courtId = [aDecoder decodeDoubleForKey:kS_ListCourtId];
    self.court = [aDecoder decodeObjectForKey:kS_ListCourt];
    self.createdAt = [aDecoder decodeObjectForKey:kS_ListCreatedAt];
    self.url = [aDecoder decodeObjectForKey:kS_ListUrl];
    self.title = [aDecoder decodeObjectForKey:kS_ListTitle];
    self.level = [aDecoder decodeObjectForKey:kS_ListLevel];
    self.reason = [aDecoder decodeObjectForKey:kS_ListReason];
    self.character = [aDecoder decodeObjectForKey:kS_ListCharacter];
    self.content = [aDecoder decodeObjectForKey:kS_ListContent];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_listIdentifier forKey:kS_ListId];
    [aCoder encodeObject:_judgeDate forKey:kS_ListJudgeDate];
    [aCoder encodeObject:_titleHighLighter forKey:kS_ListTitleHighLighter];
    [aCoder encodeObject:_abstract forKey:kS_ListAbstract];
    [aCoder encodeDouble:_courtId forKey:kS_ListCourtId];
    [aCoder encodeObject:_court forKey:kS_ListCourt];
    [aCoder encodeObject:_createdAt forKey:kS_ListCreatedAt];
    [aCoder encodeObject:_url forKey:kS_ListUrl];
    [aCoder encodeObject:_title forKey:kS_ListTitle];
    [aCoder encodeObject:_level forKey:kS_ListLevel];
    [aCoder encodeObject:_reason forKey:kS_ListReason];
    [aCoder encodeObject:_character forKey:kS_ListCharacter];
    [aCoder encodeObject:_content forKey:kS_ListContent];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_List *copy = [[S_List alloc] init];
    
    if (copy) {

        copy.listIdentifier = self.listIdentifier;
        copy.judgeDate = [self.judgeDate copyWithZone:zone];
        copy.titleHighLighter = [self.titleHighLighter copyWithZone:zone];
        copy.abstract = [self.abstract copyWithZone:zone];
        copy.courtId = self.courtId;
        copy.court = [self.court copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.level = [self.level copyWithZone:zone];
        copy.reason = [self.reason copyWithZone:zone];
        copy.character = [self.character copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
    }
    
    return copy;
}


@end
