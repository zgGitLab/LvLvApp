//
//  Test3ViewController.m
//  LawApp
//
//  Created by yfzx_sh_gaoyy on 15/10/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "IndividualCenter.h"
#import "LoginViewController.h"
#import "HelpCenterViewController.h"
#import "SetViewController.h"
#import "DraftViewController.h"
#import "CollectViewController.h"
#import "CommentViewController.h"
#import "QuestionAndMarkViewController.h"
#import "EditPerInfoViewController.h"
#import "NotAndMessViewController.h"
#import "PerMainPageViewController.h"

@interface IndividualCenter ()
{
    NSString *isVip;
    NSString *headImage;
    UIButton *redBtn;
}
@end

@implementation IndividualCenter

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor              = [UIColor  whiteColor];
    self.navigationItem.title              = @"我";
    self.bgScroll.alwaysBounceVertical     = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.bgScroll.contentSize              = CGSizeMake(SIZE.width, CGRectGetMaxY(self.bottomView.frame)+60);

    UIView *bgRightView                    = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 40)];
    redBtn                                 = [[UIButton alloc] initWithFrame:CGRectMake(35, 8, 7, 7)];
//    redBtn.titleLabel.font                 = [UIFont systemFontOfSize:9];
    redBtn.userInteractionEnabled          = NO;
//    [redBtn setBackgroundColor:[UIColor redColor]];
//    redBtn.layer.cornerRadius = 2.5;
//    redBtn.clipsToBounds = YES;
    [redBtn setBackgroundImage:[UIImage imageNamed:@"红点"] forState:UIControlStateNormal];
//    [redBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    redBtn.hidden                          = YES;
    UIButton *rightBtn                     = [UIButton buttonWithType:UIButtonTypeCustom];
//    rightBtn.backgroundColor = [UIColor blueColor];
    rightBtn.frame                         = CGRectMake(13, 2, 30, 35);
    [rightBtn addTarget:self action:@selector(messageBtn:) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setImage:[UIImage imageNamed:@"信息"] forState:UIControlStateNormal];
    [bgRightView addSubview:rightBtn];
    [bgRightView addSubview:redBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bgRightView];
    
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
    NSLog(@"%@",[self getUserId]);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = COLOR(249, 250, 251);
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    __weak IndividualCenter *VC = self;
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        [self.titleBtn setTitle:@"" forState:UIControlStateNormal];
        // 个人信息
        [RequestManager mainPerInfo:[self getUserId] andBlock:^(S_PerMain_BaseClass *s_perModal,NSString *message) {
            isVip = [NSString stringWithFormat:@"%d",(int)s_perModal.isVIP];
            
            [VC performSelectorOnMainThread:@selector(refreshData:) withObject:s_perModal waitUntilDone:YES];
        }];
        
        // 未读私信数；
        [RequestManager myMessageCountWithUserID:[self getUserId] andBlock:^(NSString *count) {
            
            if ([count intValue]>0) {
                redBtn.hidden = NO;
                if ([count intValue]>9) {
                    count = @"9+";
                }
//                [redBtn setTitle:count forState:UIControlStateNormal];
            }else{
                redBtn.hidden = YES;
            }
        }];
    }else{
        [VC.titleBtn setTitle:@"请点击登录" forState:UIControlStateNormal];
        VC.nickname.text = @"";
        VC.intro.text = @"";
        [VC.userIcon setBackgroundImage:[UIImage imageNamed:@"头像"] forState:UIControlStateNormal];
        VC.userIcon.layer.cornerRadius = 26.5;
        VC.userIcon.clipsToBounds = YES;
        self.vipImage.image = nil;
        redBtn.hidden = YES;
    }
    
    self.navigationItem.leftBarButtonItem = nil;
}

- (void)refreshData:(S_PerMain_BaseClass *)modal
{
    self.nickname.text =modal.nickname;
    self.intro.text = modal.introduction;
    headImage = [NSString stringWithFormat:@"http://%@/%@",K_IP,modal.image];
    [self.userIcon sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,modal.image]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"头像"]];
    self.userIcon.layer.cornerRadius = 26.5;
    self.userIcon.clipsToBounds = YES;
    if (modal.isVIP == 1) {
        self.vipImage.image = [UIImage imageNamed:@"头像加v"];
    }
}

// 登录
- (IBAction)logIN:(UIButton *)sender {
    
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        PerMainPageViewController *editPerinfoVC = [[PerMainPageViewController alloc] init];
        editPerinfoVC.userID = [self getUserId];
        [self.navigationController pushViewController:editPerinfoVC animated:YES];
    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }
}
// 编辑资料
- (IBAction)editPersonInfo:(UIButton *)sender {
    
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        EditPerInfoViewController *editPerinfoVC = [[EditPerInfoViewController alloc] init];
        [self.navigationController pushViewController:editPerinfoVC animated:YES];
    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }
    
}
// 我关注的
- (IBAction)concernBtn:(UIButton *)sender {
    
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        QuestionAndMarkViewController *questAndMarkVC = [[QuestionAndMarkViewController alloc] init];
        [self.navigationController pushViewController:questAndMarkVC animated:YES];

    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }
}
// 我的评论
- (IBAction)commentBtn:(UIButton *)sender {
    
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        CommentViewController *commentVC = [[CommentViewController alloc] init];
        [self.navigationController pushViewController:commentVC animated:YES];
        
    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }
    
    
}
// 我的收藏
- (IBAction)collectBtn:(UIButton *)sender {
    
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        CollectViewController *collectVC = [[CollectViewController alloc] init];
        [self.navigationController pushViewController:collectVC animated:YES];
        
    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }
    
}
// 我的草稿
- (IBAction)draftBtn:(UIButton *)sender {
    
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        DraftViewController *draftVC = [[DraftViewController alloc] init];
        [self.navigationController pushViewController:draftVC animated:YES];
        
    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }
    
}
// 设置
- (IBAction)setBtn:(UIButton *)sender {
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        SetViewController *setVC = [[SetViewController alloc] init];
        setVC.VIPstr = isVip;
        [self.navigationController pushViewController:setVC animated:YES];
    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }
}
// 帮助中心
- (IBAction)helpCenter:(UIButton *)sender {

    HelpCenterViewController *helpVC = [[HelpCenterViewController alloc] init];
    [self.navigationController pushViewController:helpVC animated:YES];

}
// 消息
- (void)messageBtn:(UIButton *)btn
{
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        NotAndMessViewController *meVC = [[NotAndMessViewController alloc] init];
        meVC.mySelfImage = headImage;
        meVC.userID = [self getUserId];
        [self.navigationController pushViewController:meVC animated:YES];
        
    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }
    
}

- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}

@end
