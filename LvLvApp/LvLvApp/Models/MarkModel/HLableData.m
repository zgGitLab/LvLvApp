//
//  HLableData.m
//
//  Created by hope  on 15/12/5
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "HLableData.h"
#import "HLableListResult.h"


NSString *const kHLableDataTotalTag = @"TotalTag";
NSString *const kHLableDataTotalComment = @"TotalComment";
NSString *const kHLableDataIsfcous = @"Isfcous";
NSString *const kHLableDataTotalHeader = @"TotalHeader";
NSString *const kHLableDataTotalAnswer = @"TotalAnswer";
NSString *const kHLableDataListResult = @"listResult";


@interface HLableData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HLableData

@synthesize totalTag = _totalTag;
@synthesize totalComment = _totalComment;
@synthesize isfcous = _isfcous;
@synthesize totalHeader = _totalHeader;
@synthesize totalAnswer = _totalAnswer;
@synthesize listResult = _listResult;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.totalTag = [[self objectOrNilForKey:kHLableDataTotalTag fromDictionary:dict] doubleValue];
            self.totalComment = [[self objectOrNilForKey:kHLableDataTotalComment fromDictionary:dict] doubleValue];
            self.isfcous = [[self objectOrNilForKey:kHLableDataIsfcous fromDictionary:dict] doubleValue];
            self.totalHeader = [[self objectOrNilForKey:kHLableDataTotalHeader fromDictionary:dict] doubleValue];
            self.totalAnswer = [[self objectOrNilForKey:kHLableDataTotalAnswer fromDictionary:dict] doubleValue];
    NSObject *receivedHLableListResult = [dict objectForKey:kHLableDataListResult];
    NSMutableArray *parsedHLableListResult = [NSMutableArray array];
    if ([receivedHLableListResult isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedHLableListResult) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedHLableListResult addObject:[HLableListResult modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedHLableListResult isKindOfClass:[NSDictionary class]]) {
       [parsedHLableListResult addObject:[HLableListResult modelObjectWithDictionary:(NSDictionary *)receivedHLableListResult]];
    }

    self.listResult = [NSArray arrayWithArray:parsedHLableListResult];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalTag] forKey:kHLableDataTotalTag];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalComment] forKey:kHLableDataTotalComment];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isfcous] forKey:kHLableDataIsfcous];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalHeader] forKey:kHLableDataTotalHeader];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalAnswer] forKey:kHLableDataTotalAnswer];
    NSMutableArray *tempArrayForListResult = [NSMutableArray array];
    for (NSObject *subArrayObject in self.listResult) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForListResult addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForListResult addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForListResult] forKey:kHLableDataListResult];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.totalTag = [aDecoder decodeDoubleForKey:kHLableDataTotalTag];
    self.totalComment = [aDecoder decodeDoubleForKey:kHLableDataTotalComment];
    self.isfcous = [aDecoder decodeDoubleForKey:kHLableDataIsfcous];
    self.totalHeader = [aDecoder decodeDoubleForKey:kHLableDataTotalHeader];
    self.totalAnswer = [aDecoder decodeDoubleForKey:kHLableDataTotalAnswer];
    self.listResult = [aDecoder decodeObjectForKey:kHLableDataListResult];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_totalTag forKey:kHLableDataTotalTag];
    [aCoder encodeDouble:_totalComment forKey:kHLableDataTotalComment];
    [aCoder encodeDouble:_isfcous forKey:kHLableDataIsfcous];
    [aCoder encodeDouble:_totalHeader forKey:kHLableDataTotalHeader];
    [aCoder encodeDouble:_totalAnswer forKey:kHLableDataTotalAnswer];
    [aCoder encodeObject:_listResult forKey:kHLableDataListResult];
}

- (id)copyWithZone:(NSZone *)zone
{
    HLableData *copy = [[HLableData alloc] init];
    
    if (copy) {

        copy.totalTag = self.totalTag;
        copy.totalComment = self.totalComment;
        copy.isfcous = self.isfcous;
        copy.totalHeader = self.totalHeader;
        copy.totalAnswer = self.totalAnswer;
        copy.listResult = [self.listResult copyWithZone:zone];
    }
    
    return copy;
}


@end
