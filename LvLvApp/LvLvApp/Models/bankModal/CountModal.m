//
//  CountModal.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/3.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "CountModal.h"
// 案例首页，统计数量

@implementation CountModal

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.A_DayAddCount = [NSString stringWithFormat:@"%@",[dict objectForKey:@"A_DayAddCount"]];
        self.ArticlesCount = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ArticlesCount"]];
        self.V_DayAddCount = [NSString stringWithFormat:@"%@",[dict objectForKey:@"V_DayAddCount"]];
        self.VerdictsCount = [NSString stringWithFormat:@"%@",[dict objectForKey:@"VerdictsCount"]];
    }
    return self;
    
}


@end
