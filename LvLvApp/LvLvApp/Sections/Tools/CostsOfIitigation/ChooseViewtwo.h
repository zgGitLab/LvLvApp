//
//  ChooseViewtwo.h
//  HPCostsCaluclate
//
//  Created by hope on 15/10/21.
//  Copyright (c) 2015年 mark.ma. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ChooseViewDelegate <NSObject>

@optional
- (void)chooseViewSelectInfo:(NSString *)info;
@end
@interface ChooseViewtwo : UIView<UITableViewDataSource,UITableViewDelegate>

@property (assign, nonatomic) BOOL  showTable;//默认为NO

@property (strong, nonatomic) NSArray *dataArray;//数据源（外部初始化需要传入）
@property (assign, nonatomic) id<ChooseViewDelegate> delegate;


- (void)show;//显示选择table

- (void)dismiss;//隐藏选择table



@end
