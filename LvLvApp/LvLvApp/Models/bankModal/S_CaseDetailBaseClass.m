//
//  S_CaseDetailBaseClass.m
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_CaseDetailBaseClass.h"
#import "S_CaseDetailDs.h"


NSString *const kS_CaseDetailBaseClassIsCollect = @"IsCollect";
NSString *const kS_CaseDetailBaseClassDs = @"ds";


@interface S_CaseDetailBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_CaseDetailBaseClass

@synthesize isCollect = _isCollect;
@synthesize ds = _ds;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.isCollect = [[self objectOrNilForKey:kS_CaseDetailBaseClassIsCollect fromDictionary:dict] doubleValue];
            self.ds = [S_CaseDetailDs modelObjectWithDictionary:[dict objectForKey:kS_CaseDetailBaseClassDs]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isCollect] forKey:kS_CaseDetailBaseClassIsCollect];
    [mutableDict setValue:[self.ds dictionaryRepresentation] forKey:kS_CaseDetailBaseClassDs];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.isCollect = [aDecoder decodeDoubleForKey:kS_CaseDetailBaseClassIsCollect];
    self.ds = [aDecoder decodeObjectForKey:kS_CaseDetailBaseClassDs];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_isCollect forKey:kS_CaseDetailBaseClassIsCollect];
    [aCoder encodeObject:_ds forKey:kS_CaseDetailBaseClassDs];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_CaseDetailBaseClass *copy = [[S_CaseDetailBaseClass alloc] init];
    
    if (copy) {

        copy.isCollect = self.isCollect;
        copy.ds = [self.ds copyWithZone:zone];
    }
    
    return copy;
}


@end
