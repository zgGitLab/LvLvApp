//
//  PerPickImageView.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/1.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PerPickViewDelegat <NSObject>

- (void)jumpOtherView:(UIImagePickerController *)object;
- (void)selectImage:(id)icon;
@end

@interface PerPickImageView : UIView<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIToolbar* toolBar;
}
@property (nonatomic,assign) id<PerPickViewDelegat>delegate;
@property (nonatomic,strong) UIView *backGroundView;
@property (nonatomic,assign) int partUD;
@property (nonatomic,strong) UIImagePickerController *picker;
-(id)initWithId:(int)ID;
-(void)showInView;
@end
