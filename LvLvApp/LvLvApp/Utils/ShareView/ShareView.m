//
//  ShareView.m
//  LvLvApp
//
//  Created by jim on 15/12/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "ShareView.h"
#import "CustomActionSheet.h"
#import "SliderView.h"
#import "EditQusetionViewController.h"


@implementation ShareView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    _successStr = @"分享成功";
    if (!([self.dictionary objectForKey:@"imageArr2"] == nil || ((NSArray *)[self.dictionary objectForKey:@"imageArr2"]).count == 0)) {
        NSInteger num = 0;
        NSArray *arr1 =[self.dictionary objectForKey:@"imageArr1"];
        NSArray *imageArr1 = [[NSArray alloc]init];
        if ([arr1[4] isEqualToString:@""]) {
            imageArr1 = @[arr1[0],arr1[1],arr1[2],arr1[3]];
            num = 2;
        }else{
            imageArr1 = [self.dictionary objectForKey:@"imageArr1"];
            num = 3;
        }
        
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, kHeight-110-(kWidth/4)*num, kWidth, 50)];
        title.text = [self.dictionary objectForKey:@"title"];
        title.font = [UIFont systemFontOfSize:17];
        title.textAlignment = NSTextAlignmentCenter;
        title.backgroundColor = [UIColor whiteColor];
        title.textColor = setColor(130, 130, 130);
        [self addSubview:title];
        
        UIView *share =[Utils shareMenuWithFrame:CGRectMake(0, CGRectGetMaxY(title.frame), kWidth, kWidth/4*(num-1)) andImageArray:imageArr1 andTitleArray:imageArr1 andId:self andAction:@selector(myclick:) andStartTag:100];
        [self addSubview:share];
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 0.5)];
        line.backgroundColor = KGRAYCOLOR;
        line.alpha = 0.4;
        [share addSubview:line];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeShareView:)];
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:tap];
        //空白view
        UIView *blockView = [[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(share.frame), kWidth, 30)];
        blockView.backgroundColor = [UIColor whiteColor];
        blockView.layer.borderColor = [[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1] CGColor];
        blockView.layer.borderWidth = 0.5;
        [self addSubview:blockView];
        
        UIView *edtiView = [Utils shareMenuWithFrame:CGRectMake(0, CGRectGetMaxY(blockView.frame), kWidth, kWidth/4) andImageArray:[self.dictionary objectForKey:@"imageArr2"] andTitleArray:[self.dictionary objectForKey:@"titleArr2"] andId:self andAction:@selector(myclick:) andStartTag:110];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(edtiView.frame), kWidth, 30)];
        label.backgroundColor = [UIColor whiteColor];
        [self addSubview:label];
        [self addSubview:edtiView];
    }else{
        NSInteger num = 0;
        NSArray *arr1 =[self.dictionary objectForKey:@"imageArr1"];
        NSArray *imageArr1 = [[NSArray alloc]init];
        if ([arr1[4] isEqualToString:@""]) {
            imageArr1 = @[arr1[0],arr1[1],arr1[2],arr1[3]];
            num = 1;
        }else{
            imageArr1 = [self.dictionary objectForKey:@"imageArr1"];
            num = 2;
        }
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, kHeight-60-(kWidth/4)*num, kWidth, 50)];
        title.text = [self.dictionary objectForKey:@"title"];
        title.font = [UIFont systemFontOfSize:20];
        title.textAlignment = NSTextAlignmentCenter;
        title.backgroundColor = [UIColor whiteColor];
        title.textColor = setColor(130, 130, 130);
        [self addSubview:title];
        
        UIView *share =[Utils shareMenuWithFrame:CGRectMake(0, CGRectGetMaxY(title.frame), kWidth, kWidth/4*num) andImageArray:imageArr1 andTitleArray:imageArr1 andId:self andAction:@selector(myclick:) andStartTag:100];
        [self addSubview:share];
        
        UIView *blockView = [[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(share.frame) , kWidth, 30)];
        blockView.backgroundColor = [UIColor whiteColor];
        blockView.layer.borderColor = [[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1] CGColor];
        blockView.layer.borderWidth = 0.5;
        [self addSubview:blockView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeShareView:)];
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:tap];
    }
}

- (void)myclick:(UIButton *)btn{
    NSLog(@"%@",btn.titleLabel.text);

    NSString *title = btn.titleLabel.text;
    
    if ([title isEqualToString:@"微信好友"]) {
        _tmpDic = self.noteDictionary;
        NSLog(@"分享到微信好友");
        [self shareWithPlatform:SSDKPlatformSubTypeWechatSession];
    }else if ([title isEqualToString:@"微信朋友圈"]){
        _tmpDic = self.noteDictionary;
        NSLog(@"分享到微信朋友圈");
        [self shareWithPlatform:SSDKPlatformSubTypeWechatTimeline];
    }else if([title isEqualToString:@"印象笔记"]){
        _tmpDic = self.noteDictionary;
        NSLog(@"分享到印象笔记");
//        [self shareWithPlatform:SSDKPlatformTypeEvernote];
        [self shareWithPlatform:SSDKPlatformTypeYinXiang];
    }else if([title isEqualToString:@"有道云笔记"]){
        _tmpDic = self.noteDictionary;
        NSLog(@"分享到有道云笔记");
        [self shareWithPlatform:SSDKPlatformTypeYouDaoNote];
    }else if([title isEqualToString:@"微博"]){
        _tmpDic = self.dictionary;
        NSLog(@"分享到微博");
        //创建分享参数
        [self shareWithPlatform:SSDKPlatformTypeSinaWeibo];
    }else if([title isEqualToString:@"QQ"]){
        _tmpDic = self.noteDictionary;
        NSLog(@"分享到QQ");
        [self shareWithPlatform:SSDKPlatformTypeQQ];
    }else if([title isEqualToString:@"邮件"]){
        _tmpDic = self.dictionary;
        NSLog(@"分享到邮件");
        [self shareWithPlatform:SSDKPlatformTypeMail];
    }else if([title isEqualToString:@"复制链接"]){
        _tmpDic = self.dictionary;
        _successStr = @"已复制链接到剪贴板";
        NSLog(@"复制链接");
        [self shareWithPlatform:SSDKPlatformTypeCopy];
    }else if([title isEqualToString:@"举报问题"]||[title isEqualToString:@"举报用户"]||[title isEqualToString:@"举报回答"]){
        NSLog(@"举报");
        CustomActionSheet *sheet = [[CustomActionSheet alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
        sheet.dataSource = @[@"垃圾营销",@"不实信息",@"违法信息",@"淫秽信息",@"人身攻击",@"不宜公开讨论的内容"];
        sheet.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        sheet.dict = [self.dictionary objectForKey:@"dict"];
        [self.superview addSubview:sheet];
        
        
    }else if([title isEqualToString:@"编辑问题"]){
        NSLog(@"编辑问题");
        if (self.editQuestionBlock) {
            self.editQuestionBlock();
        }
    }else if([title isEqualToString:@"删除问题"]){
        NSLog(@"删除问题");
        if (self.deleteQuestionBlock) {
            self.deleteQuestionBlock();
        }
    }else if([title isEqualToString:@"字体大小"]){
        NSLog(@"字体大小");
        SliderView *slider = [[SliderView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
        slider.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.3];
        [self.superview addSubview:slider];
    }else if([title isEqualToString:@"编辑回答"]){
        NSLog(@"编辑回答");
        if (self.editAnswerBlock) {
            self.editAnswerBlock();
        }
        
    }else if([title isEqualToString:@"删除回答"]){
        NSLog(@"删除回答");
        if (self.deleteAnswerBlock) {
            self.deleteAnswerBlock();
        }
    }
    
    [self removeFromSuperview];
}

- (void)removeShareView:(UITapGestureRecognizer *)tap{
    [self removeFromSuperview];
}

//分享事件
- (void)shareWithPlatform:(NSUInteger)platform{
    //创建分享参数
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];

    //分享内容
    NSString *shareText = [self.tmpDic objectForKey:@"shareText"];
    if (platform == SSDKPlatformTypeSinaWeibo || platform == SSDKPlatformTypeMail) {
        
    }else if(platform == SSDKPlatformTypeYouDaoNote || platform == SSDKPlatformTypeYinXiang || platform == SSDKPlatformTypeEvernote){

    }else{
        if (shareText.length > 30) {
            shareText = [shareText substringToIndex:30];
        }else {
            shareText = [shareText substringToIndex:shareText.length];
        }
    }
    
    
    //分享图片
    UIImage *shareImage = [[UIImage alloc]init];
    if (platform == SSDKPlatformSubTypeWechatSession || platform == SSDKPlatformSubTypeWechatTimeline || platform == SSDKPlatformTypeQQ || platform == SSDKPlatformTypeYinXiang) {
        shareImage = [self.tmpDic objectForKey:@"shareImage"];
    }
    
    //分享参数
    [shareParams SSDKSetupShareParamsByText:shareText images:shareImage url:[NSURL URLWithString:[self.tmpDic objectForKey:@"shareUrl"]] title:[self.tmpDic objectForKey:@"shareTitle"] type:SSDKContentTypeAuto];
    
    //进行分享
    [ShareSDK share:platform //传入分享的平台类型
         parameters:shareParams
     onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
         // 回调处理....
         switch(state) {
             case SSDKResponseStateSuccess:
             {
                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:_successStr                      message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                 [alertView show];
                 break;
             }
             case SSDKResponseStateFail:
             {
                 NSLog(@"error:%@",error);
//                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享失败"                        message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:@"确定"otherButtonTitles:nil];
//                 [alertView show];
                 break;
             }
             case SSDKResponseStateCancel:
             {
//                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享已取消" message:nil                     delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//                 [alertView show];
                 break;
             }
             default:
                 break;
         }
         
     }];
}
@end
