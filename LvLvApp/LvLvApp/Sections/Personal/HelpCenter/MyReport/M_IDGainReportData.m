//
//  M_IDGainReportData.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_IDGainReportData.h"


NSString *const kM_IDGainReportDataUserID = @"UserID";
NSString *const kM_IDGainReportDataReportDate = @"ReportDate";
NSString *const kM_IDGainReportDataCategory = @"Category";
NSString *const kM_IDGainReportDataDisposeResult = @"DisposeResult";
NSString *const kM_IDGainReportDataTitle = @"Title";
NSString *const kM_IDGainReportDataRemarkContent = @"RemarkContent";


@interface M_IDGainReportData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_IDGainReportData

@synthesize userID = _userID;
@synthesize reportDate = _reportDate;
@synthesize category = _category;
@synthesize disposeResult = _disposeResult;
@synthesize title = _title;
@synthesize remarkContent = _remarkContent;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userID = [[self objectOrNilForKey:kM_IDGainReportDataUserID fromDictionary:dict] doubleValue];
            self.reportDate = [self objectOrNilForKey:kM_IDGainReportDataReportDate fromDictionary:dict];
            self.category = [self objectOrNilForKey:kM_IDGainReportDataCategory fromDictionary:dict];
            self.disposeResult = [self objectOrNilForKey:kM_IDGainReportDataDisposeResult fromDictionary:dict];
            self.title = [self objectOrNilForKey:kM_IDGainReportDataTitle fromDictionary:dict];
            self.remarkContent = [self objectOrNilForKey:kM_IDGainReportDataRemarkContent fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kM_IDGainReportDataUserID];
    [mutableDict setValue:self.reportDate forKey:kM_IDGainReportDataReportDate];
    [mutableDict setValue:self.category forKey:kM_IDGainReportDataCategory];
    [mutableDict setValue:self.disposeResult forKey:kM_IDGainReportDataDisposeResult];
    [mutableDict setValue:self.title forKey:kM_IDGainReportDataTitle];
    [mutableDict setValue:self.remarkContent forKey:kM_IDGainReportDataRemarkContent];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userID = [aDecoder decodeDoubleForKey:kM_IDGainReportDataUserID];
    self.reportDate = [aDecoder decodeObjectForKey:kM_IDGainReportDataReportDate];
    self.category = [aDecoder decodeObjectForKey:kM_IDGainReportDataCategory];
    self.disposeResult = [aDecoder decodeObjectForKey:kM_IDGainReportDataDisposeResult];
    self.title = [aDecoder decodeObjectForKey:kM_IDGainReportDataTitle];
    self.remarkContent = [aDecoder decodeObjectForKey:kM_IDGainReportDataRemarkContent];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_userID forKey:kM_IDGainReportDataUserID];
    [aCoder encodeObject:_reportDate forKey:kM_IDGainReportDataReportDate];
    [aCoder encodeObject:_category forKey:kM_IDGainReportDataCategory];
    [aCoder encodeObject:_disposeResult forKey:kM_IDGainReportDataDisposeResult];
    [aCoder encodeObject:_title forKey:kM_IDGainReportDataTitle];
    [aCoder encodeObject:_remarkContent forKey:kM_IDGainReportDataRemarkContent];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_IDGainReportData *copy = [[M_IDGainReportData alloc] init];
    
    if (copy) {

        copy.userID = self.userID;
        copy.reportDate = [self.reportDate copyWithZone:zone];
        copy.category = [self.category copyWithZone:zone];
        copy.disposeResult = [self.disposeResult copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.remarkContent = [self.remarkContent copyWithZone:zone];
    }
    
    return copy;
}


@end
