//
//  S_NotiFiCData.m
//
//  Created by IOS8  on 15/12/21
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_NotiFiCData.h"


NSString *const kS_NotiFiCDataTID = @"TID";
NSString *const kS_NotiFiCDataUserid = @"Userid";
NSString *const kS_NotiFiCDataNotifyID = @"NotifyID";
NSString *const kS_NotiFiCDataDescription = @"Description";
NSString *const kS_NotiFiCDataTimagesUrl = @"TimagesUrl";
NSString *const kS_NotiFiCDataLogoUrl = @"LogoUrl";
NSString *const kS_NotiFiCDataQuestionContent = @"QuestionContent";
NSString *const kS_NotiFiCDataCollectCount = @"collectCount";
NSString *const kS_NotiFiCDataSortTime = @"sortTime";
NSString *const kS_NotiFiCDataNickname = @"Nickname";
NSString *const kS_NotiFiCDataCounts = @"counts";
NSString *const kS_NotiFiCDataAnswerContent = @"AnswerContent";
NSString *const kS_NotiFiCDataTitle = @"Title";
NSString *const kS_NotiFiCDataCommentCount = @"commentCount";
NSString *const kS_NotiFiCDataTimes = @"times";
NSString *const kS_NotiFiCDataTypes = @"types";
NSString *const kS_NotiFiCDataAnswerID = @"Answer_ID";
NSString *const kS_NotiFiCDataAUserID = @"AUserID";
NSString *const kS_NotiFiCDataTUserID = @"TUserID";

@interface S_NotiFiCData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_NotiFiCData

@synthesize tID = _tID;
@synthesize userid = _userid;
@synthesize notifyID = _notifyID;
@synthesize dataDescription = _dataDescription;
@synthesize timagesUrl = _timagesUrl;
@synthesize logoUrl = _logoUrl;
@synthesize questionContent = _questionContent;
@synthesize collectCount = _collectCount;
@synthesize sortTime = _sortTime;
@synthesize nickname = _nickname;
@synthesize counts = _counts;
@synthesize answerContent = _answerContent;
@synthesize title = _title;
@synthesize commentCount = _commentCount;
@synthesize times = _times;
@synthesize types = _types;
@synthesize answerID = _answerID;
@synthesize AUserID = _AUserID;
@synthesize TUserID = _TUserID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tID = [self objectOrNilForKey:kS_NotiFiCDataTID fromDictionary:dict];
            self.userid = [[self objectOrNilForKey:kS_NotiFiCDataUserid fromDictionary:dict] doubleValue];
            self.notifyID = [[self objectOrNilForKey:kS_NotiFiCDataNotifyID fromDictionary:dict] doubleValue];
            self.dataDescription = [self objectOrNilForKey:kS_NotiFiCDataDescription fromDictionary:dict];
            self.timagesUrl = [self objectOrNilForKey:kS_NotiFiCDataTimagesUrl fromDictionary:dict];
            self.logoUrl = [self objectOrNilForKey:kS_NotiFiCDataLogoUrl fromDictionary:dict];
            self.questionContent = [self objectOrNilForKey:kS_NotiFiCDataQuestionContent fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kS_NotiFiCDataCollectCount fromDictionary:dict] doubleValue];
            self.sortTime = [self objectOrNilForKey:kS_NotiFiCDataSortTime fromDictionary:dict];
            self.nickname = [self objectOrNilForKey:kS_NotiFiCDataNickname fromDictionary:dict];
            self.counts = [[self objectOrNilForKey:kS_NotiFiCDataCounts fromDictionary:dict] doubleValue];
            self.answerContent = [self objectOrNilForKey:kS_NotiFiCDataAnswerContent fromDictionary:dict];
            self.title = [self objectOrNilForKey:kS_NotiFiCDataTitle fromDictionary:dict];
            self.commentCount = [[self objectOrNilForKey:kS_NotiFiCDataCommentCount fromDictionary:dict] doubleValue];
            self.times = [self objectOrNilForKey:kS_NotiFiCDataTimes fromDictionary:dict];
            self.types = [[self objectOrNilForKey:kS_NotiFiCDataTypes fromDictionary:dict] doubleValue];
            self.answerID = [self objectOrNilForKey:kS_NotiFiCDataAnswerID fromDictionary:dict];
            self.AUserID = [self objectOrNilForKey:kS_NotiFiCDataAUserID fromDictionary:dict];
            self.TUserID = [self objectOrNilForKey:kS_NotiFiCDataTUserID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tID forKey:kS_NotiFiCDataTID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userid] forKey:kS_NotiFiCDataUserid];
    [mutableDict setValue:[NSNumber numberWithDouble:self.notifyID] forKey:kS_NotiFiCDataNotifyID];
    [mutableDict setValue:self.dataDescription forKey:kS_NotiFiCDataDescription];
    [mutableDict setValue:self.timagesUrl forKey:kS_NotiFiCDataTimagesUrl];
    [mutableDict setValue:self.logoUrl forKey:kS_NotiFiCDataLogoUrl];
    [mutableDict setValue:self.questionContent forKey:kS_NotiFiCDataQuestionContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kS_NotiFiCDataCollectCount];
    [mutableDict setValue:self.sortTime forKey:kS_NotiFiCDataSortTime];
    [mutableDict setValue:self.nickname forKey:kS_NotiFiCDataNickname];
    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kS_NotiFiCDataCounts];
    [mutableDict setValue:self.answerContent forKey:kS_NotiFiCDataAnswerContent];
    [mutableDict setValue:self.title forKey:kS_NotiFiCDataTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kS_NotiFiCDataCommentCount];
    [mutableDict setValue:self.times forKey:kS_NotiFiCDataTimes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.types] forKey:kS_NotiFiCDataTypes];
    [mutableDict setValue:self.answerID forKey:kS_NotiFiCDataAnswerID];
    [mutableDict setValue:self.AUserID forKey:kS_NotiFiCDataAUserID];
    [mutableDict setValue:self.TUserID forKey:kS_NotiFiCDataTUserID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tID = [aDecoder decodeObjectForKey:kS_NotiFiCDataTID];
    self.userid = [aDecoder decodeDoubleForKey:kS_NotiFiCDataUserid];
    self.notifyID = [aDecoder decodeDoubleForKey:kS_NotiFiCDataNotifyID];
    self.dataDescription = [aDecoder decodeObjectForKey:kS_NotiFiCDataDescription];
    self.timagesUrl = [aDecoder decodeObjectForKey:kS_NotiFiCDataTimagesUrl];
    self.logoUrl = [aDecoder decodeObjectForKey:kS_NotiFiCDataLogoUrl];
    self.questionContent = [aDecoder decodeObjectForKey:kS_NotiFiCDataQuestionContent];
    self.collectCount = [aDecoder decodeDoubleForKey:kS_NotiFiCDataCollectCount];
    self.sortTime = [aDecoder decodeObjectForKey:kS_NotiFiCDataSortTime];
    self.nickname = [aDecoder decodeObjectForKey:kS_NotiFiCDataNickname];
    self.counts = [aDecoder decodeDoubleForKey:kS_NotiFiCDataCounts];
    self.answerContent = [aDecoder decodeObjectForKey:kS_NotiFiCDataAnswerContent];
    self.title = [aDecoder decodeObjectForKey:kS_NotiFiCDataTitle];
    self.commentCount = [aDecoder decodeDoubleForKey:kS_NotiFiCDataCommentCount];
    self.times = [aDecoder decodeObjectForKey:kS_NotiFiCDataTimes];
    self.types = [aDecoder decodeDoubleForKey:kS_NotiFiCDataTypes];
    self.answerID = [aDecoder decodeObjectForKey:kS_NotiFiCDataAnswerID];
    self.AUserID = [aDecoder decodeObjectForKey:kS_NotiFiCDataAUserID];
    self.TUserID = [aDecoder decodeObjectForKey:kS_NotiFiCDataTUserID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tID forKey:kS_NotiFiCDataTID];
    [aCoder encodeDouble:_userid forKey:kS_NotiFiCDataUserid];
    [aCoder encodeDouble:_notifyID forKey:kS_NotiFiCDataNotifyID];
    [aCoder encodeObject:_dataDescription forKey:kS_NotiFiCDataDescription];
    [aCoder encodeObject:_timagesUrl forKey:kS_NotiFiCDataTimagesUrl];
    [aCoder encodeObject:_logoUrl forKey:kS_NotiFiCDataLogoUrl];
    [aCoder encodeObject:_questionContent forKey:kS_NotiFiCDataQuestionContent];
    [aCoder encodeDouble:_collectCount forKey:kS_NotiFiCDataCollectCount];
    [aCoder encodeObject:_sortTime forKey:kS_NotiFiCDataSortTime];
    [aCoder encodeObject:_nickname forKey:kS_NotiFiCDataNickname];
    [aCoder encodeDouble:_counts forKey:kS_NotiFiCDataCounts];
    [aCoder encodeObject:_answerContent forKey:kS_NotiFiCDataAnswerContent];
    [aCoder encodeObject:_title forKey:kS_NotiFiCDataTitle];
    [aCoder encodeDouble:_commentCount forKey:kS_NotiFiCDataCommentCount];
    [aCoder encodeObject:_times forKey:kS_NotiFiCDataTimes];
    [aCoder encodeDouble:_types forKey:kS_NotiFiCDataTypes];
    [aCoder encodeObject:_answerID forKey:kS_NotiFiCDataAnswerID];
    [aCoder encodeObject:_AUserID forKey:kS_NotiFiCDataAUserID];
    [aCoder encodeObject:_TUserID forKey:kS_NotiFiCDataTUserID];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_NotiFiCData *copy = [[S_NotiFiCData alloc] init];
    
    if (copy) {

        copy.tID = [self.tID copyWithZone:zone];
        copy.userid = self.userid;
        copy.notifyID = self.notifyID;
        copy.dataDescription = [self.dataDescription copyWithZone:zone];
        copy.timagesUrl = [self.timagesUrl copyWithZone:zone];
        copy.logoUrl = [self.logoUrl copyWithZone:zone];
        copy.questionContent = [self.questionContent copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.sortTime = [self.sortTime copyWithZone:zone];
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.counts = self.counts;
        copy.answerContent = [self.answerContent copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.commentCount = self.commentCount;
        copy.times = [self.times copyWithZone:zone];
        copy.types = self.types;
        copy.answerID = [self.answerID copyWithZone:zone];
        copy.AUserID = [self.AUserID copyWithZone:zone];
        copy.TUserID = [self.TUserID copyWithZone:zone];
    }
    
    return copy;
}


@end
