//
//  CheckViewController.h
//  LvLvApp
//
//  Created by hope on 15/12/18.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIScrollView *bgScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
- (IBAction)btnClick:(UIButton *)sender;

@end
