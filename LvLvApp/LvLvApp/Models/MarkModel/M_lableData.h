//
//  M_lableData.h
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface M_lableData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double totalTag;
@property (nonatomic, assign) double totalComment;
@property (nonatomic, assign) double isfcous;
@property (nonatomic, assign) double totalHeader;
@property (nonatomic, assign) double totalAnswer;
@property (nonatomic, strong) NSArray *listResult;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
