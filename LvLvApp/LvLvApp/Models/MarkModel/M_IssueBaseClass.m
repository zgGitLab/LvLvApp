//
//  M_IssueBaseClass.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_IssueBaseClass.h"
#import "M_IssueData.h"


NSString *const kM_IssueBaseClassCode = @"Code";
NSString *const kM_IssueBaseClassMessage = @"Message";
NSString *const kM_IssueBaseClassData = @"Data";


@interface M_IssueBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_IssueBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_IssueBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_IssueBaseClassMessage fromDictionary:dict];
    NSObject *receivedM_IssueData = [dict objectForKey:kM_IssueBaseClassData];
    NSMutableArray *parsedM_IssueData = [NSMutableArray array];
    if ([receivedM_IssueData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedM_IssueData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedM_IssueData addObject:[M_IssueData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedM_IssueData isKindOfClass:[NSDictionary class]]) {
       [parsedM_IssueData addObject:[M_IssueData modelObjectWithDictionary:(NSDictionary *)receivedM_IssueData]];
    }

    self.data = [NSArray arrayWithArray:parsedM_IssueData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_IssueBaseClassCode];
    [mutableDict setValue:self.message forKey:kM_IssueBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kM_IssueBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_IssueBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_IssueBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_IssueBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_IssueBaseClassCode];
    [aCoder encodeObject:_message forKey:kM_IssueBaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_IssueBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_IssueBaseClass *copy = [[M_IssueBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
