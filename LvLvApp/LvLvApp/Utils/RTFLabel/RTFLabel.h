//
//  RTFLabel.h
//  LvLvApp
//
//  Created by jim on 15/12/10.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"
@interface RTFLabel :CustomLabel

- (id)initWithFrame:(CGRect)frame andTextColor:(UIColor *)color andFont:(CGFloat)fontSize andText:(NSString *)text withNumOfLines:(NSInteger)lines andImageArray:(NSMutableArray *)imageArray;
@end
