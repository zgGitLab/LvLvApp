//
//  DraftPublishViewController.m
//  LvLvApp
//
//  Created by hope on 15/12/17.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "DraftPublishViewController.h"
#import "InsertTextAttAchment.h"
#import "NSAttributedString+EmojiExtension.h"

@interface DraftPublishViewController ()<UITextViewDelegate,PerPickViewDelegat,UIAlertViewDelegate>
{
    UILabel *_label;
    BOOL _anonymous;
    NSArray *_imageList;//图片数组
}
@property (nonatomic,strong) NSMutableArray *imageArray;
@property (nonatomic,copy) NSMutableString *ImgCode;
@property (nonatomic,copy) NSMutableString *totalString;

@end

@implementation DraftPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden=NO;

     _anonymous = NO;
    _ImgCode = [NSMutableString string];
    _totalString = [NSMutableString string];
    self.imageArray = [NSMutableArray array];
    self.title = @"我的回答";
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    [back setTintColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1]];
    [back setWidth:34];
    self.navigationItem.leftBarButtonItem = back;
    
    UIBarButtonItem *publish = [[UIBarButtonItem alloc]initWithTitle:@"发布" style:UIBarButtonItemStylePlain target:self action:@selector(publish)];
    [publish setWidth:34];
    [publish setTintColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1]];
    self.navigationItem.rightBarButtonItem = publish;
    
    [self createTextView];
    
    _label = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 200, 20)];
    _label.enabled = NO;
    if (self.textContent) {
        _label.text = @"";
    }else{
        _label.text = @"填写回答内容";
    }
    [self showTextView];
    _label.font =  [UIFont systemFontOfSize:15];
    _label.textColor = [UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1];
    [self.textView addSubview:_label];
}



- (void)createTextView{
    //自定义键盘上部的view
    UIView *keyboardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 36)];
    
    UIButton *key = [UIButton buttonWithType:UIButtonTypeSystem];
    key.frame = CGRectMake(kWidth-26-15, 7, 26, 26);
    [key setImage:[[UIImage imageNamed:@"收键盘"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [key addTarget:self action:@selector(collectKeyBoard:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:key];
    
    UIButton *pic = [UIButton buttonWithType:UIButtonTypeSystem];
    pic.frame = CGRectMake(key.frame.origin.x-15-26, 5, 26, 26);
    [pic setImage:[[UIImage imageNamed:@"图片"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [pic addTarget:self action:@selector(picClick:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:pic];
    
    keyboardView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 10, kWidth-20, kHeight-400)];
    self.textView.inputAccessoryView = keyboardView;
    self.textView.delegate = self;
    self.textView.textColor = [UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
    self.textView.font = [UIFont systemFontOfSize:15];
    [self.textView becomeFirstResponder];
     self.textView.delegate = self;
    [self.view addSubview:self.textView];
}

//发布草稿
- (void)requestDataWithContent:(NSString *)content andStatus:(NSNumber *)status{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:self.UserID forKey:@"UserID"];
    [parm setObject:status forKey:@"Status"];
    [parm setObject:_ImgCode forKey:@"ImgCode"];
    [parm setObject:self.TID forKey:@"TID"];
    [parm setObject:content forKey:@"Content"];
    [parm setObject:self.PushanswerID forKey:@"Answer_ID"];
    [parm setObject:[NSString stringWithFormat:@"%d",_anonymous] forKey:@"IsAnonymous"];
    [manager POST:[NSString stringWithFormat:KDraftPublishUrl,K_IP] parameters:parm success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"dict :-----%@",dict);
        BOOL success =[dict[@"Code"] isEqualToNumber:@200];
        if (success) {
            NSLog(@"发布成功");
            [self.navigationController popViewControllerAnimated:YES];
        }
        NSLog(@"回答成功");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error:%@",error);
    }];
}


//删除草稿请求
- (void)requestDataWithContent{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:self.UserID forKey:@"UserID"];
    NSLog(@"%@",self.PushanswerID);
    [parm setObject:self.PushanswerID forKey:@"AnswerID"];
  
    [manager POST:[NSString stringWithFormat:Kdeletemydraft,K_IP] parameters:parm success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSMutableString *str =[[NSMutableString alloc]initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
        BOOL success =[dict[@"Code"] isEqualToNumber:@200];
        if (success) {
            NSLog(@"删除成功");
            
        }
        NSLog(@"删除成功");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error:%@",error);
    }];
}

//nick匿名发布
- (void)nickClick:(UIButton *)button{
    button.selected = !button.selected;
    _anonymous = !_anonymous;
}

//at
- (void)atClick:(UIButton *)button{
    _label.text = @"";
    NSMutableString *str = [[NSMutableString alloc]init];
    str = [NSMutableString stringWithFormat:@"%@@",self.textView.text];
    self.textView.text = str;
}

//collectKeyBoard
- (void)collectKeyBoard:(UIButton *)button{
    [self.textView resignFirstResponder];
}

//pic
- (void)picClick:(UIButton *)button{
    _label.text = @"";
    [self.view endEditing:YES];
    PerPickImageView *perPickVi = [[PerPickImageView alloc] initWithId:1];
    perPickVi.delegate = self;
    [perPickVi showInView];
}


- (void)showTextView{
    _imageList = [Utils subArrayFromString:self.textContent];
    NSMutableString *str = [[NSMutableString alloc]initWithFormat:@"%@",_textContent];
    //创建NSMutableAttributedString实例，并将content传入
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:str];
    //设置行距
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    [style setLineSpacing:5.0f];
    //根据给定长度与style设置attStr式样
    [attStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1],NSParagraphStyleAttributeName:style,NSFontAttributeName:[UIFont systemFontOfSize:15]} range:NSMakeRange(0, str.length)];
    NSInteger mm = 0;
    for (int i = 0; i < _imageList.count; i ++) {
        if (![_imageList[i] isEqualToString:@""]) {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,_imageList[i]]];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
            CGFloat rate = (kWidth-20)/image.size.width;
            image = [Utils thumbnailWithImageWithoutScale:image size:CGSizeMake(kWidth-20, rate*image.size.height)];
            InsertTextAttAchment *attatchment = [[InsertTextAttAchment alloc] init];
            attatchment.image = image;
            
            NSRange range = [str rangeOfString:@"<"];
            
            NSUInteger length = [@"<img src=\"/upload/answer/2016042922080299889.jpg\"/>" length];
            [str deleteCharactersInRange:NSMakeRange(range.location, length) ];
            [attStr deleteCharactersInRange:NSMakeRange(range.location+mm, length)];
            [attStr insertAttributedString:[NSAttributedString attributedStringWithAttachment:attatchment] atIndex:range.location+mm];
            mm++;
        }
    }
    _textView.attributedText = attStr;
}


//发布
- (void)publish{
    
    if ([Utils isBlankString:self.textView.text]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请填写发布内容" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }else {
        _imageArray = [self.textView.textStorage getImages];
        //处理图片，将图片数组转换成base64字符串
        for (int i = 0; i < _imageArray.count; i++) {
            NSData *data = UIImageJPEGRepresentation(_imageArray[i], 1);
            NSString *encodedImageStr = [data base64EncodedStringWithOptions:0];
            [_ImgCode appendString:encodedImageStr];
            if ((i != _imageArray.count-1)||(_imageArray.count !=1)) {
                [_ImgCode appendString:@"|"];
            }
        }
        NSString *info =[self.textView.textStorage getPlainString];
        NSMutableString *str = [[NSMutableString alloc]initWithString:info];
        
        //把所有的0替换为11；
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"0.jpg\"/>"];
            
            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"r.jpg\"/>"]];
                
            }
        }
        
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"r.jpg\"/>"];
            
            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"%d.jpg\"/>",i]];
            }
        }
        
        [self requestDataWithContent:str andStatus:@1];
        [self.textView resignFirstResponder];
        
    }
}

- (void)textViewDidChange:(UITextView *)textView{
    
    if ([textView.text length] == 0) {
        [_label setHidden:NO];
    }else{
        [_label setHidden:YES];
    }
}


/**
 *  保存草稿
 *
 *  @param content 回答内容
 *  @param status  保存到草稿
 */
- (void)requestDataWithContents:(NSString *)content{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:kUserID forKey:@"UserID"];
    [parm setObject:_ImgCode forKey:@"ImgCode"];
    [parm setObject:self.TID forKey:@"TID"];
    [parm setObject:content forKey:@"Content"];
    if (![Utils isBlankString:self.PushanswerID]) {
        [parm setObject:self.PushanswerID forKey:@"Answer_ID"];
    }else{
        [parm setObject:@"" forKey:@"Answer_ID"];
    }
    
    [manager POST:[NSString stringWithFormat:kSaveDraft,K_IP] parameters:parm success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"回答成功");
        [self.textView resignFirstResponder];
        [self.navigationController popViewControllerAnimated:YES];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error:%@",error);
    }];
}



//取消
- (void)back{
    if (![Utils isBlankString:self.textView.text]) {
        _imageArray = [self.textView.textStorage getImages];
        //处理图片，将图片数组转换成base64字符串
        for (int i = 0; i < _imageArray.count; i++) {
            NSData *data = UIImageJPEGRepresentation(_imageArray[i], 1);
            NSString *encodedImageStr = [data base64EncodedStringWithOptions:0];
            [_ImgCode appendString:encodedImageStr];
            if ((i != _imageArray.count-1)||(_imageArray.count !=1)) {
                [_ImgCode appendString:@"|"];
            }
        }
        NSString *info =[self.textView.textStorage getPlainString];
        NSMutableString *str = [[NSMutableString alloc]initWithString:info];
        
        //把所有的0替换为11；
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"0.jpg\"/>"];
            
            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"r.jpg\"/>"]];
                
            }
        }
        
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"r.jpg\"/>"];
            
            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"%d.jpg\"/>",i]];
            }
        }
        //保存草稿
        [self requestDataWithContents:str];

    }else{
        [self.navigationController popViewControllerAnimated:YES];
        [self.textView resignFirstResponder];
    }
    
}




-(UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;   //返回的就是已经改变的图片
}



#pragma mark PerPickViewDelegat;
- (void)jumpOtherView:(UIImagePickerController *)object
{
    object.allowsEditing = NO;
    [self presentViewController:object animated:YES completion:nil];
}

- (void)selectImage:(id)icon
{
    UIImage *imag = (UIImage *)icon;
    
    CGFloat rate = (kWidth-20)/imag.size.width;
    imag = [Utils thumbnailWithImageWithoutScale:imag size:CGSizeMake(kWidth-20, imag.size.height *rate)];
    InsertTextAttAchment *texAtt = [[InsertTextAttAchment alloc] init];
    texAtt.image = imag;
    [self.textView.textStorage insertAttributedString:[NSAttributedString attributedStringWithAttachment:texAtt]
                                              atIndex:self.textView.selectedRange.location];
    
    //Move selection location
    _textView.selectedRange = NSMakeRange(_textView.selectedRange.location + 1, _textView.selectedRange.length);
    _textView.font = [UIFont systemFontOfSize:15];
    _textView.textColor = [UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
    [_textView becomeFirstResponder];
}


#pragma mark -- textView的delegate



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
