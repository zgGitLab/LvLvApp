//
//  ChatViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/24.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *chatTable;
@property (strong, nonatomic) NSMutableArray *viewArray;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *otherID;
@property (strong, nonatomic) NSString *titleStr;
@property (strong, nonatomic) NSString *mySelfImage;
@end
