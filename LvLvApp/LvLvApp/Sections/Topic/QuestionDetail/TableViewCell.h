//
//  TableViewCell.h
//  Demozhihu
//
//  Created by hope on 15/10/12.
//  Copyright © 2015年 hope. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *HeaderImage;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

@property (strong, nonatomic) IBOutlet UIButton *nameBtn;

@property (strong, nonatomic) IBOutlet UIButton *anwer;
@property (strong, nonatomic) IBOutlet UILabel *countsLabel;
@property (strong, nonatomic) IBOutlet UILabel *answerLabel;


@end
