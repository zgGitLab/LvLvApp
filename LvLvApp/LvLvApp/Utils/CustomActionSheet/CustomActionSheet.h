//
//  CustomActionSheet.h
//  LvLvApp
//
//  Created by jim on 15/12/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomActionSheet : UIView<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) NSArray *dataSource;
@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) NSMutableDictionary *dict;
@end
