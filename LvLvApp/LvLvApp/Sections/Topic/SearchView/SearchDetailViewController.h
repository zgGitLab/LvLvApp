//
//  TwoSeacrhIssusViewController.h
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IssueView.h"
@class IssueView;
@class AnwerViews;
@class LableView;
@class UserViews;
@interface SearchDetailViewController : UIViewController

@property (nonatomic, strong)NSString *searString;

@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)IssueView *issueV;
@property (nonatomic, strong)AnwerViews *anwerV;
@property (nonatomic, strong)LableView *lableV;
@property (nonatomic, strong)UserViews *userV;
@property(nonatomic ,strong)UIScrollView *scroll2;
@property (nonatomic ,strong)NSMutableArray *issarrays;
@property (nonatomic ,strong)NSMutableArray *anwerarrays;
@property (nonatomic, strong)NSMutableArray *labearrays;
@property (nonatomic, strong)NSMutableArray *userarrayss;
@property (nonatomic, strong)NSString *Ts;


@end
