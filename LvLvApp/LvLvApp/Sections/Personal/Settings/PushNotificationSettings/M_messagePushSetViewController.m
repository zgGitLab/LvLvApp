//
//  M_messagePushSetViewController.m
//  LvLvApp
//
//  Created by hope on 15/12/21.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "M_messagePushSetViewController.h"
#import "Push_AllStatusBaseClass.h"
@interface M_messagePushSetViewController ()
@property (strong, nonatomic) IBOutlet UIScrollView *messagePushScroll;
@property (strong, nonatomic) IBOutlet UILabel *HintLabale;

@end

@implementation M_messagePushSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title =@"通知消息设置";
    self.messagePushScroll.alwaysBounceVertical = YES;
    self.messagePushScroll.contentSize = CGSizeMake(SIZE.width, CGRectGetMaxY(self.HintLabale.frame)+30);
//    [self.BelawanPatternOutlet setOn:NO];
    [self getAllStatus];
    
}
- (IBAction)switch5:(UISwitch *)sender {
    [self requese];
}
- (IBAction)switch4:(UISwitch *)sender {
    [self requese];
}
- (IBAction)switch3:(UISwitch *)sender {
    [self requese];
}
- (IBAction)switch2:(UISwitch *)sender {
    [self requese];
}
- (IBAction)switch1:(UISwitch *)sender {
    [self requese];
}

- (void)requese
{
    [RequestManager setStatusWithUserID:[self getUserId] andFocusNewAnwser:(int)self.swith1.on andFocusMe:(int)self.swith2.on andPraiseMe:(int)self.swith3.on  andSendMe:(int)self.swith4.on  andCommentMe:(int)self.swith5.on];
}

- (void)getAllStatus
{
    __weak M_messagePushSetViewController *VC= self;
    [RequestManager getAllStatusWithUserID:[self getUserId] andBlock:^(Push_AllStatusBaseClass *pushModal, NSString *message) {
        VC.swith1.on = pushModal.focusNewAnwser;
        VC.swith2.on = pushModal.focusMe;
        VC.swith3.on = pushModal.praiseMe;
        VC.swith4.on = pushModal.sendMe;
        VC.swith5.on = pushModal.commentMe;
        
    }];
}

- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}

@end
