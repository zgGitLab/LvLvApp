//
//  IndividualCountDetail.h
//  LvLvApp
//
//  Created by hope on 15/10/16.
//  Copyright (c) 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndividualCountDetail : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cityLab;              // 城市名称
@property (weak, nonatomic) IBOutlet UILabel *onlyPerTaxLab;        // 仅计算个税
@property (weak, nonatomic) IBOutlet UILabel *foreignLab;           // 是否外籍

@property (weak, nonatomic) IBOutlet UILabel *perYL;                // 个人养老比例
@property (weak, nonatomic) IBOutlet UILabel *unitYL;               // 单位养老比例
@property (weak, nonatomic) IBOutlet UILabel *perYLPay;             // 个人缴纳
@property (weak, nonatomic) IBOutlet UILabel *unitYLPay;            // 单位缴纳

@property (weak, nonatomic) IBOutlet UILabel *perMedical;           // 个人医疗比例
@property (weak, nonatomic) IBOutlet UILabel *unitMedical;          // 单位医疗比例
@property (weak, nonatomic) IBOutlet UILabel *perMedicalPay;        // 个人缴纳
@property (weak, nonatomic) IBOutlet UILabel *unitMedicalPay;       // 单位缴纳

@property (weak, nonatomic) IBOutlet UILabel *perSY;                // 个人失业比例
@property (weak, nonatomic) IBOutlet UILabel *unitSY;               // 单位比例
@property (weak, nonatomic) IBOutlet UILabel *perSYPay;             // 个人缴纳
@property (weak, nonatomic) IBOutlet UILabel *unitSYPay;            // 单位缴纳

@property (weak, nonatomic) IBOutlet UILabel *unitGS;               // 单位工伤比例
@property (weak, nonatomic) IBOutlet UILabel *unitGSPay;            // 单位缴纳

@property (weak, nonatomic) IBOutlet UILabel *unitBear;             // 单位生育比例
@property (weak, nonatomic) IBOutlet UILabel *unitBearPay;          // 单位缴纳

@property (weak, nonatomic) IBOutlet UILabel *perPublic;            // 个人公积金比例
@property (weak, nonatomic) IBOutlet UILabel *unitPublic;           // 单位公积金比例
@property (weak, nonatomic) IBOutlet UILabel *perPublicPay;         // 个人缴纳
@property (weak, nonatomic) IBOutlet UILabel *unitPublicPay;        // 单位缴纳

@property (weak, nonatomic) IBOutlet UILabel *perTax;               // 个税

@property (weak, nonatomic) IBOutlet UILabel *perAll;               // 个人小计
@property (weak, nonatomic) IBOutlet UILabel *unitAll;              // 单位小计

@property (weak, nonatomic) IBOutlet UILabel *computeMoney;
@property (weak, nonatomic) IBOutlet UILabel *afterTaxMoney;

@end
