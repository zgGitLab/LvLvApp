//
//  Utils.h
//  LvLvApp
//
//  Created by hope on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomLabel.h"
#import "MyTapGestureRecognizer.h"
@interface Utils : NSObject
//动态 计算行高
//根据字符串的实际内容的多少 在固定的宽度和字体的大小，动态的计算出实际的高度
+ (CGFloat)textHeightFromTextString:(NSString *)text width:(CGFloat)textWidth fontSize:(CGFloat)size;

//设置行间距,待完善
+ (void)setLinewidthWith:(id)object andWidth:(CGFloat)width andString:(NSString *)string;

//获取宽度，获取字符串不折行单行显示时所需要的长度
+ (CGFloat)textWidthFromTextString:(NSString *)text fontSize:(CGFloat)size size:(CGFloat)height;

+ (NSString *)replaceImageString:(NSString *)string;

//自定义label
+ (CustomLabel *)labelWithFrame:(CGRect)frame andTextColor:(UIColor *)color andFont:(UIFont *)font andText:(NSString *)text withNumOfLines:(NSInteger)lines withTarget:(id)target andSEL:(SEL)selecter;


//自定义imageview
+ (UIImageView *)imageWithFrame:(CGRect)frame andImageUrl:(NSString *)string;

//密码验证
+ (BOOL)validatePassword:(NSString *)passWord;
//身份证号验证
+ (BOOL) validateIdentityCard: (NSString *)identityCard;
//邮箱验证
+ (BOOL) validateEmail:(NSString *)email;

//获得设备型号
+ (NSString *)getCurrentDeviceModel;

//判断字符串是否为空
+ (BOOL) isBlankString:(NSString *)string;

//分享
+ (void)showShareActionSheet:(UIView *)view;
+ (UIView *)shareMenuWithFrame:(CGRect)frame andImageArray:(NSArray *)imageArray andTitleArray:(NSArray *)titleArray andId:(id)myself andAction:(SEL)selecter andStartTag:(NSInteger)tags;

+(NSArray *)isClientInstalled:(NSArray *)array;

//保持原来的长宽比，生成一个缩略图
+(UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize;
//根据字符串提取图片数组
+ (NSMutableArray *)subArrayFromString:(NSString *)str;

/**去除空格和换行，并以@“  ”代替(目前只在话题首页使用)*/
+ (NSString *)removetheSpacesAndLinebreaksFromString:(NSString *)text;
//移除所有的空白字符包括换行
+ (NSString *)removeBlankString:(NSString *)text;
/**把图片字符串替换成[图片]*/
+ (NSString *)StringByreplaceImageWithString:(NSString *)str;
@end
