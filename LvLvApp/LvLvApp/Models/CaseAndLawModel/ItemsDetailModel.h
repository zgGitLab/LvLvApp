//
//  ItemsDetailModel.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/25.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "BaseDataModel.h"

@interface ItemsDetailModel : BaseDataModel

@property (nonatomic,copy) NSString *ID;//法条编号
@property (nonatomic,copy) NSString *title;//标题
@property (nonatomic,copy) NSString *publish_dep;//发文机关
@property (nonatomic,copy) NSString *publish_num;//发布数量
@property (nonatomic,copy) NSString *pub_date;//发布年份
@property (nonatomic,copy) NSString *effective_date;//生效日期
@property (nonatomic,copy) NSString *effectiveness;//时效性
@property (nonatomic,copy) NSString *effective_level;//效力级别
@property (nonatomic,copy) NSString *category;//类别
@property (nonatomic,copy) NSString *body;//内容
@property (nonatomic,copy) NSString *area;//区域

@end
