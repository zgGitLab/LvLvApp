//
//  FTiaoData.m
//
//  Created by hope  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "FTiaoData.h"


NSString *const kFTiaoDataCategory = @"category";
NSString *const kFTiaoDataPubDate = @"pub_date";
NSString *const kFTiaoDataDocId = @"DocId";
NSString *const kFTiaoDataCreatedAt = @"created_at";
NSString *const kFTiaoDataId = @"id";
NSString *const kFTiaoDataTitle = @"title";
NSString *const kFTiaoDataPublishDep = @"publish_dep";
NSString *const kFTiaoDataEffectiveness = @"effectiveness";
NSString *const kFTiaoDataBody = @"body";


@interface FTiaoData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FTiaoData

@synthesize category = _category;
@synthesize pubDate = _pubDate;
@synthesize docId = _docId;
@synthesize createdAt = _createdAt;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize title = _title;
@synthesize publishDep = _publishDep;
@synthesize effectiveness = _effectiveness;
@synthesize body = _body;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.category = [self objectOrNilForKey:kFTiaoDataCategory fromDictionary:dict];
            self.pubDate = [self objectOrNilForKey:kFTiaoDataPubDate fromDictionary:dict];
            self.docId = [[self objectOrNilForKey:kFTiaoDataDocId fromDictionary:dict] doubleValue];
            self.createdAt = [self objectOrNilForKey:kFTiaoDataCreatedAt fromDictionary:dict];
            self.dataIdentifier = [[self objectOrNilForKey:kFTiaoDataId fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kFTiaoDataTitle fromDictionary:dict];
            self.publishDep = [self objectOrNilForKey:kFTiaoDataPublishDep fromDictionary:dict];
            self.effectiveness = [self objectOrNilForKey:kFTiaoDataEffectiveness fromDictionary:dict];
            self.body = [self objectOrNilForKey:kFTiaoDataBody fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.category forKey:kFTiaoDataCategory];
    [mutableDict setValue:self.pubDate forKey:kFTiaoDataPubDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.docId] forKey:kFTiaoDataDocId];
    [mutableDict setValue:self.createdAt forKey:kFTiaoDataCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kFTiaoDataId];
    [mutableDict setValue:self.title forKey:kFTiaoDataTitle];
    [mutableDict setValue:self.publishDep forKey:kFTiaoDataPublishDep];
    [mutableDict setValue:self.effectiveness forKey:kFTiaoDataEffectiveness];
    [mutableDict setValue:self.body forKey:kFTiaoDataBody];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.category = [aDecoder decodeObjectForKey:kFTiaoDataCategory];
    self.pubDate = [aDecoder decodeObjectForKey:kFTiaoDataPubDate];
    self.docId = [aDecoder decodeDoubleForKey:kFTiaoDataDocId];
    self.createdAt = [aDecoder decodeObjectForKey:kFTiaoDataCreatedAt];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kFTiaoDataId];
    self.title = [aDecoder decodeObjectForKey:kFTiaoDataTitle];
    self.publishDep = [aDecoder decodeObjectForKey:kFTiaoDataPublishDep];
    self.effectiveness = [aDecoder decodeObjectForKey:kFTiaoDataEffectiveness];
    self.body = [aDecoder decodeObjectForKey:kFTiaoDataBody];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_category forKey:kFTiaoDataCategory];
    [aCoder encodeObject:_pubDate forKey:kFTiaoDataPubDate];
    [aCoder encodeDouble:_docId forKey:kFTiaoDataDocId];
    [aCoder encodeObject:_createdAt forKey:kFTiaoDataCreatedAt];
    [aCoder encodeDouble:_dataIdentifier forKey:kFTiaoDataId];
    [aCoder encodeObject:_title forKey:kFTiaoDataTitle];
    [aCoder encodeObject:_publishDep forKey:kFTiaoDataPublishDep];
    [aCoder encodeObject:_effectiveness forKey:kFTiaoDataEffectiveness];
    [aCoder encodeObject:_body forKey:kFTiaoDataBody];
}

- (id)copyWithZone:(NSZone *)zone
{
    FTiaoData *copy = [[FTiaoData alloc] init];
    
    if (copy) {

        copy.category = [self.category copyWithZone:zone];
        copy.pubDate = [self.pubDate copyWithZone:zone];
        copy.docId = self.docId;
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.title = [self.title copyWithZone:zone];
        copy.publishDep = [self.publishDep copyWithZone:zone];
        copy.effectiveness = [self.effectiveness copyWithZone:zone];
        copy.body = [self.body copyWithZone:zone];
    }
    
    return copy;
}


@end
