//
//  S_MyFansData.h
//
//  Created by IOS8  on 15/12/17
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_MyFansData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *introduction;
@property (nonatomic, assign) double isFocus;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *nickName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
