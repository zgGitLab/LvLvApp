//
//  CollectViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *bgView;//回答,发条,案例view
@property (weak, nonatomic) IBOutlet UIButton *anserBtn;//回答
@property (weak, nonatomic) IBOutlet UIButton *lawBtn;//发条
@property (weak, nonatomic) IBOutlet UIButton *caseBtn;//案例
@property (weak, nonatomic) IBOutlet UILabel *colorLab;//下滑视图
@property (weak, nonatomic) IBOutlet UITableView *myTable;//表

@property (nonatomic, strong)NSMutableArray *myCollectReply;//回答
@property (nonatomic, strong)NSMutableArray *myCollectCase;//案例
@property (nonatomic, strong)NSMutableArray *myCollectFaTiao;//法条

@property (nonatomic, strong)NSString *AnswerIDStr;
@property (nonatomic, strong)NSString *HeaderIDStr;

@property (nonatomic, strong)NSString *myCollectCaseID;
@property (nonatomic, strong)NSString *mytyep;
@property (nonatomic, strong)NSString *myCollectfataoID;
@property (nonatomic, strong)NSMutableArray *CaseFataoCountID;
@property (nonatomic, strong)NSMutableArray *fataoCaseCountID;
@end
