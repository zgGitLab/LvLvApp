//
//  M_reportBaseClass.m
//
//  Created by hope  on 15/12/25
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_reportBaseClass.h"
#import "M_reportData.h"


NSString *const kM_reportBaseClassCode = @"Code";
NSString *const kM_reportBaseClassMessage = @"Message";
NSString *const kM_reportBaseClassData = @"Data";


@interface M_reportBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_reportBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_reportBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_reportBaseClassMessage fromDictionary:dict];
    NSObject *receivedM_reportData = [dict objectForKey:kM_reportBaseClassData];
    NSMutableArray *parsedM_reportData = [NSMutableArray array];
    if ([receivedM_reportData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedM_reportData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedM_reportData addObject:[M_reportData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedM_reportData isKindOfClass:[NSDictionary class]]) {
       [parsedM_reportData addObject:[M_reportData modelObjectWithDictionary:(NSDictionary *)receivedM_reportData]];
    }

    self.data = [NSArray arrayWithArray:parsedM_reportData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_reportBaseClassCode];
    [mutableDict setValue:self.message forKey:kM_reportBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kM_reportBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_reportBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_reportBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_reportBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_reportBaseClassCode];
    [aCoder encodeObject:_message forKey:kM_reportBaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_reportBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_reportBaseClass *copy = [[M_reportBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
