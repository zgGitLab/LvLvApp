//
//  UserViews.h
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchTopicViewController.h"
#import "USERData.h"
#import "UserSTableViewCell.h"

typedef void (^userrrayBlock)(NSMutableArray *);
@protocol UserViewDelegate<NSObject>

- (void)Userclick:(NSInteger)indxe;

@end

@interface UserViews :  UIView <UITableViewDataSource,UITableViewDelegate>

typedef  void (^UsercellClick)(UserViews *);
@property (nonatomic, strong)UsercellClick userceCell;
@property (nonatomic, strong)NSString *searString;
@property (nonatomic, strong)UITableView *usertableView;
@property (nonatomic, strong)NSMutableArray *userarrayss;
@property (nonatomic, strong)id<UserViewDelegate>UserDelegate;
@property (nonatomic, copy)userrrayBlock userarrays;
@property (nonatomic, strong) USERData *userData;
- (id)initWithString:(NSString*)strr;
- (void)getusrSearText:(NSString *)str;
@end
