//
//  AttionBaseClass.m
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "AttionBaseClass.h"
#import "AttionData.h"


NSString *const kAttionBaseClassCode = @"Code";
NSString *const kAttionBaseClassMessage = @"Message";
NSString *const kAttionBaseClassData = @"Data";


@interface AttionBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AttionBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kAttionBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kAttionBaseClassMessage fromDictionary:dict];
            self.data = [AttionData modelObjectWithDictionary:[dict objectForKey:kAttionBaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kAttionBaseClassCode];
    [mutableDict setValue:self.message forKey:kAttionBaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kAttionBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kAttionBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kAttionBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kAttionBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kAttionBaseClassCode];
    [aCoder encodeObject:_message forKey:kAttionBaseClassMessage];
    [aCoder encodeObject:_data forKey:kAttionBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    AttionBaseClass *copy = [[AttionBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
