//
//  S_ChatListData.m
//
//  Created by IOS8  on 15/12/23
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_ChatListData.h"


NSString *const kS_ChatListDataRecID = @"RecID";
NSString *const kS_ChatListDataSendID = @"SendID";
NSString *const kS_ChatListDataSendTime = @"SendTime";
NSString *const kS_ChatListDataTextContent = @"TextContent";
NSString *const kS_ChatListDataNickName = @"NickName";
NSString *const kS_ChatListDataTitle = @"Title";
NSString *const kS_ChatListDataImage = @"Image";


@interface S_ChatListData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_ChatListData

@synthesize recID = _recID;
@synthesize sendID = _sendID;
@synthesize sendTime = _sendTime;
@synthesize textContent = _textContent;
@synthesize nickName = _nickName;
@synthesize title = _title;
@synthesize image = _image;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.recID = [[self objectOrNilForKey:kS_ChatListDataRecID fromDictionary:dict] doubleValue];
            self.sendID = [[self objectOrNilForKey:kS_ChatListDataSendID fromDictionary:dict] doubleValue];
            self.sendTime = [self objectOrNilForKey:kS_ChatListDataSendTime fromDictionary:dict];
            self.textContent = [self objectOrNilForKey:kS_ChatListDataTextContent fromDictionary:dict];
            self.nickName = [self objectOrNilForKey:kS_ChatListDataNickName fromDictionary:dict];
            self.title = [self objectOrNilForKey:kS_ChatListDataTitle fromDictionary:dict];
            self.image = [self objectOrNilForKey:kS_ChatListDataImage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.recID] forKey:kS_ChatListDataRecID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sendID] forKey:kS_ChatListDataSendID];
    [mutableDict setValue:self.sendTime forKey:kS_ChatListDataSendTime];
    [mutableDict setValue:self.textContent forKey:kS_ChatListDataTextContent];
    [mutableDict setValue:self.nickName forKey:kS_ChatListDataNickName];
    [mutableDict setValue:self.title forKey:kS_ChatListDataTitle];
    [mutableDict setValue:self.image forKey:kS_ChatListDataImage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.recID = [aDecoder decodeDoubleForKey:kS_ChatListDataRecID];
    self.sendID = [aDecoder decodeDoubleForKey:kS_ChatListDataSendID];
    self.sendTime = [aDecoder decodeObjectForKey:kS_ChatListDataSendTime];
    self.textContent = [aDecoder decodeObjectForKey:kS_ChatListDataTextContent];
    self.nickName = [aDecoder decodeObjectForKey:kS_ChatListDataNickName];
    self.title = [aDecoder decodeObjectForKey:kS_ChatListDataTitle];
    self.image = [aDecoder decodeObjectForKey:kS_ChatListDataImage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_recID forKey:kS_ChatListDataRecID];
    [aCoder encodeDouble:_sendID forKey:kS_ChatListDataSendID];
    [aCoder encodeObject:_sendTime forKey:kS_ChatListDataSendTime];
    [aCoder encodeObject:_textContent forKey:kS_ChatListDataTextContent];
    [aCoder encodeObject:_nickName forKey:kS_ChatListDataNickName];
    [aCoder encodeObject:_title forKey:kS_ChatListDataTitle];
    [aCoder encodeObject:_image forKey:kS_ChatListDataImage];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_ChatListData *copy = [[S_ChatListData alloc] init];
    
    if (copy) {

        copy.recID = self.recID;
        copy.sendID = self.sendID;
        copy.sendTime = [self.sendTime copyWithZone:zone];
        copy.textContent = [self.textContent copyWithZone:zone];
        copy.nickName = [self.nickName copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
    }
    
    return copy;
}


@end
