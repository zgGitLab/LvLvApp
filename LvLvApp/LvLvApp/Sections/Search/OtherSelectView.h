//
//  OtherSelectView.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/2.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol doneShure <NSObject>

-(void)done:(id)sender andFilterDict:(NSMutableDictionary *)dict;

@end


@interface OtherSelectView : UIView<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    //c语言数组，数组中有5个元素，都是bool类型默认值NO
    BOOL _orOpen[7];
}

@property (strong,nonatomic)UICollectionView *collectVI;

@property (strong,nonatomic)UICollectionViewFlowLayout *flowLayout;
@property(unsafe_unretained,nonatomic)id<doneShure>delegate;
@property(assign,nonatomic)int type;//0是法条 1是案例
@property(strong,nonatomic)NSMutableArray *userStringArr;
@property(strong,nonatomic)NSMutableArray *titleArray;//分类标题数组
@property(strong,nonatomic)NSMutableArray *dataArr;
@property(strong,nonatomic)NSMutableArray *indexArr;
@property (nonatomic,strong) NSMutableDictionary *filterDict;//筛选字段

@property (nonatomic,strong) NSMutableDictionary *dataDict;//简化后的字典

-(id)initWithFrame:(CGRect)frame andType:(NSInteger)type andDataArray:(NSArray *)dataArray andFilterDict:(NSMutableDictionary *)dict;
-(void)showInView:(UIView *)view;
@end
