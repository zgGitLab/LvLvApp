//
//  ShareView.h
//  LvLvApp
//
//  Created by jim on 15/12/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ShareBlock) (void);
@interface ShareView : UIView

@property (nonatomic,strong) NSMutableDictionary *dictionary;//微博分享(content(title+链接))
@property (nonatomic,strong) NSMutableDictionary *noteDictionary;//有道云笔记、印象笔记(title+content)
@property (nonatomic,strong) NSMutableDictionary *textDictionary;//微信、微信朋友圈、QQ（image+title+content）

@property (nonatomic,strong) NSMutableDictionary *tmpDic;
@property (nonatomic,copy) ShareBlock editQuestionBlock;
@property (nonatomic,copy) ShareBlock deleteQuestionBlock;
@property (nonatomic,copy) ShareBlock editAnswerBlock;
@property (nonatomic,copy) ShareBlock deleteAnswerBlock;

@property (nonatomic,copy) NSString *successStr;//分享成功的文字提示
@end
