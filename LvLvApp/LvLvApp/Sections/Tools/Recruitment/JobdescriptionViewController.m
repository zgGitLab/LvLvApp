//
//  JobdescriptionViewController.m
//  LvLvApp
//
//  Created by Mark on 16/3/24.
//  Copyright © 2016年 gyy. All rights reserved.
//
#define UIscK  [[UIScreen mainScreen]bounds].size.width
#import "JobdescriptionViewController.h"
#import "RTFLabel.h"

#import "JobDetailVi.h"
@interface JobdescriptionViewController ()
{
    UITableView *_jobDetailTab;
    JobDetailVi *_headVI;
    NSString *tempStr;
    JbDetailModel *_jobdetail;
}

@end

@implementation JobdescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"职位详情";

    UIBarButtonItem *leftItem =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"返回箭头"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    
    self.navigationItem.leftBarButtonItem=leftItem;
    self.view.backgroundColor = [UIColor whiteColor];
    [self jobDetailRequst];
}

- (void)createUI
{
    
    
    //岗位要求和联系方式
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(15, _headVI.frame.size.height+10, SIZE.width - 30, 30)];
    titleLab.text = @"岗位要求和联系方式";
    [self.scrollView addSubview:titleLab];
    
    NSString *text = [NSString stringWithFormat:@"岗位要求:\n%@\n联系方式:\n%@",_jobdetail.Requirement,_jobdetail.Contact];
    //设置行距
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:text];;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:5];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
    
    //设置字体
    UIFont *baseFont = [UIFont systemFontOfSize:15];
    [attributedString addAttribute:NSFontAttributeName value:baseFont range:NSMakeRange(0, text.length)];//设置所有的字体
    
    CGFloat height = [attributedString boundingRectWithSize:CGSizeMake(SIZE.width - 30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
    RTFTextView *label = [[RTFTextView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(titleLab.frame)+10, kWidth - 30, height)];
    label.attributedText = attributedString;
    label.textColor = [UIColor grayColor];
    [label sizeToFit];
    label.textAlignment = NSTextAlignmentLeft;
    [self.scrollView addSubview:label];
    self.scrollView.contentSize = CGSizeMake(kWidth, height+label.frame.origin.y+100);
}

- (void)jobDetailRequst
{
    //你的接口地址
    NSString *url = [NSString stringWithFormat:@"http://%@/api/User/getJobByID",K_IP];
    //传入的参数
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:self.jobID forKey:@"ID"];
    
    [LVHTTPRequestTool post:url params:parameters success:^(id json) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        if ([dic[@"Code"] isEqualToNumber:@200]) {
            NSArray *arr = dic[@"Data"];
            if (arr.count != 0) {
                self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
                [self.view addSubview:self.scrollView];
                
                //头视图
                _headVI = [[[JobDetailVi alloc] init] loadVi];
                [self.scrollView addSubview:_headVI];
                _jobdetail = [[JbDetailModel alloc] init];
                [_jobdetail setValuesForKeysWithDictionary:dic[@"Data"][0]];
                [_headVI getAllDataWithModel:_jobdetail];
            }
            [self createUI];
        }else{
            [MBProgressHUD showOlnyMessage:dic[@"Message"]];
        }
    } failure:^(NSError *error) {
        
    }];
}


- (void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
