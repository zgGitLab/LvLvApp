//
//  CaseAndLawViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/5.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "CaseAndLawViewController.h"
#import "CasusViewController.h"
@interface CaseAndLawViewController ()
{
    UITextField *imputView;
    UIButton *leftButton;
    UIView *selectView;
}
@end

@implementation CaseAndLawViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"案例和法条列表";
    imputView = [[UITextField alloc] initWithFrame:CGRectMake(50, 30, SIZE.width-130, 30)];
    imputView.backgroundColor = [UIColor whiteColor];
    imputView.borderStyle = UITextBorderStyleRoundedRect;
//    [imputView becomeFirstResponder];
    imputView.returnKeyType = UIReturnKeyYahoo;
    
    self.navigationItem.titleView = imputView;
    
    leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = CGRectMake(0, 0, 40, 15);
    [leftButton setBackgroundImage:[UIImage imageNamed:@"法条-1"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(leftBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 40, 15);
    [rightButton setTitle:@"取消" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithRed:65/255.0 green:71/255.0 blue:83/255.0 alpha:1] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightBtn:) forControlEvents:UIControlEventTouchUpInside];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];

    
}

- (void)leftBtn:(UIButton *)btn
{
    NSLog(@"1111");
}
- (void)rightBtn:(UIButton *)btn
{
   [self.navigationController popToRootViewControllerAnimated:YES];
    
//    UINavigationController *nav=self.navigationController;
//     CasusViewController *caseVC=[nav viewControllers][0];
//    [self.navigationController pushViewController:caseVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
