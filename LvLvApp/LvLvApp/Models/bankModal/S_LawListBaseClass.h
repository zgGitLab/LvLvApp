//
//  S_LawListBaseClass.h
//
//  Created by IOS8  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_LawListBaseClass : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *search;
@property (nonatomic, assign) double count;
@property (nonatomic, strong) NSArray *list;
@property (nonatomic, assign) double pageindex;
@property (nonatomic, assign) double pagesize;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
