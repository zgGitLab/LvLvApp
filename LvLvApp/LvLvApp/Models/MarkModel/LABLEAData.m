//
//  LABLEAData.m
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "LABLEAData.h"


NSString *const kLABLEADataState = @"State";
NSString *const kLABLEADataT = @"T";


@interface LABLEAData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LABLEAData

@synthesize state = _state;
@synthesize t = _t;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.state = [[self objectOrNilForKey:kLABLEADataState fromDictionary:dict] doubleValue];
            self.t = [[self objectOrNilForKey:kLABLEADataT fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.state] forKey:kLABLEADataState];
    [mutableDict setValue:[NSNumber numberWithDouble:self.t] forKey:kLABLEADataT];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.state = [aDecoder decodeDoubleForKey:kLABLEADataState];
    self.t = [aDecoder decodeDoubleForKey:kLABLEADataT];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_state forKey:kLABLEADataState];
    [aCoder encodeDouble:_t forKey:kLABLEADataT];
}

- (id)copyWithZone:(NSZone *)zone
{
    LABLEAData *copy = [[LABLEAData alloc] init];
    
    if (copy) {

        copy.state = self.state;
        copy.t = self.t;
    }
    
    return copy;
}


@end
