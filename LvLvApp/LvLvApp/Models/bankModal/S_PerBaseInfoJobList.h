//
//  S_PerBaseInfoJobList.h
//
//  Created by IOS8  on 15/12/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_PerBaseInfoJobList : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double userID;
@property (nonatomic, strong) NSString *lacation;
@property (nonatomic, strong) NSString *endYear;
@property (nonatomic, strong) NSString *beginYear;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) id department;
@property (nonatomic, strong) NSString *iDProperty;
@property (nonatomic, strong) NSString *company;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
