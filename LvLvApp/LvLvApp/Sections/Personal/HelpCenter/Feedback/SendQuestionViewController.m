//
//  SendQuestionViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/21.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "SendQuestionViewController.h"
#import "LoginViewController.h"
@interface SendQuestionViewController ()<PerPickViewDelegat,UIAlertViewDelegate>
{
    UIImage *anserImage;
    NSMutableArray *imagArr;
    NSMutableArray *btnImageArr;
    CGFloat btnWidth;
    BOOL isRemove;
}
@end

@implementation SendQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"反馈问题";
    self.questText.placeholderText = @"请简要描述你的问题和意见";
    self.questText.placeholderColor = COLOR(171, 171, 171);
    [self.addImageBtn addTarget:self action:@selector(addQuestImage:) forControlEvents:UIControlEventTouchUpInside];
    self.bgScroll.contentSize = CGSizeMake(SIZE.width, SIZE.height-64);
    self.bgScroll.alwaysBounceVertical = YES;
    imagArr = [NSMutableArray array];
    btnImageArr = [NSMutableArray array];
    // 当键盘改变的时候，会发送通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyChangeFram:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    // 创建点击手势
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click:)];
    // 将手势添加到vi上
    [self.bgScroll addGestureRecognizer:tapGes];
}

-(void)textViewDidChange:(UITextView *)textView
{
    self.questText.placeholderColor = [textView.text isEqualToString:@""] ? COLOR(180,180,180) : [UIColor clearColor];
}

// 点击手势
- (void)click:(UITapGestureRecognizer *)tapGes
{
    [self.view endEditing:YES];
}

- (void)keyChangeFram:(NSNotification *)info
{
    // 获取键盘动画的曲线
    CGRect rect;
    // 获得键盘的freme
    [[info.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&rect];
    self.bgScroll.frame = CGRectMake(0, 0, SIZE.width, rect.origin.y-64);
}

- (void)addQuestImage:(UIButton *)btn
{
    [self.view endEditing:YES];
    if (imagArr.count >= 3) {
        K_ALERT(@"最多添加三张图片");
        return;
    }
    PerPickImageView *perPickVi = [[PerPickImageView alloc] initWithId:1];
    perPickVi.delegate = self;
    [perPickVi showInView];
}

#pragma mark PerPickViewDelegat
- (void)jumpOtherView:(UIImagePickerController *)object
{
    NSLog(@"aaa");
    object.allowsEditing = YES;
    // 点击按钮跳转到能够利用的图片来源
    [self presentViewController:object animated:YES completion:nil];
}

- (void)selectImage:(id)icon
{
    anserImage = (UIImage *)icon;
    [self createImageBtn:anserImage];
}

- (void)createImageBtn:(UIImage *)image
{
    btnWidth = (SIZE.width-4*15)/3.0;
    UIButton *imagebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [imagebtn addTarget:self action:@selector(removeImageBtn:) forControlEvents:UIControlEventTouchUpInside];
    imagebtn.tag  = imagArr.count + 1;
    imagebtn.frame = CGRectMake(15 + (imagArr.count*btnWidth)+imagArr.count*15, 15, btnWidth, btnWidth);
    [imagebtn setBackgroundImage:image forState:UIControlStateNormal];
    [self.bgImageView addSubview:imagebtn];
    [btnImageArr addObject:imagebtn];
    if (imagArr.count == 0) {
        self.addImageBtn.frame = CGRectMake(15, btnWidth+30, 91, 91);
        self.bgImageView.frame = CGRectMake(0, 177, SIZE.width, 120+btnWidth+15);
        self.bgPhioneView.frame = CGRectMake(0, 317+btnWidth+15, SIZE.width, 50);
        self.submitBtn.frame = CGRectMake(0, 405+btnWidth+15, SIZE.width, 40);
        self.bgScroll.contentSize = CGSizeMake(SIZE.width, SIZE.height-64 + btnWidth+15);
    }
    [imagArr addObject:image];
}

- (void)removeImageBtn:(UIButton *)btn
{
    UIAlertView *removAlert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否删除图片" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    removAlert.delegate = self;
    removAlert.tag = btn.tag -1;
    [removAlert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        [imagArr removeObjectAtIndex:alertView.tag];
        UIButton *btn = [self.bgImageView viewWithTag:alertView.tag+1];
        [btnImageArr removeObject:btn];
        [btn removeFromSuperview];
        if (imagArr.count == 0) {
            self.addImageBtn.frame = CGRectMake(15, 15, 91, 91);
            self.bgImageView.frame = CGRectMake(0, 177, SIZE.width, 120);
            self.bgPhioneView.frame = CGRectMake(0, 317, SIZE.width, 50);
            self.submitBtn.frame = CGRectMake(0, 405, SIZE.width, 40);
             self.bgScroll.contentSize = CGSizeMake(SIZE.width, SIZE.height-64);
        }else{
            
            for (int i = 0; i < btnImageArr.count; i++) {
                
                UIButton *temBtn = [btnImageArr objectAtIndex:i];
                temBtn.frame = CGRectMake(15 + (i*btnWidth)+i*15, 15, btnWidth, btnWidth);
                temBtn.tag = i+1;
            }
            self.addImageBtn.frame = CGRectMake(15, btnWidth+30, 91, 91);
            self.bgImageView.frame = CGRectMake(0, 177, SIZE.width, 120+btnWidth+15);
            self.bgPhioneView.frame = CGRectMake(0, 317+btnWidth+15, SIZE.width, 50);
            self.submitBtn.frame = CGRectMake(0, 405+btnWidth+15, SIZE.width, 40);
        }

    }
}

- (IBAction)submitBtnClick:(UIButton *)sender {
    [self.view endEditing:YES];
     if (![[self getUserId] isEqualToString:@"(null)"]) {
         if ([self.questText.text isEqualToString:@""]) {
             K_ALERT(@"请输入问题");
             return;
         }
         [MBProgressHUD showMessage:@"正在提交!" toView:self.view ];
         [RequestManager myQuestionWithUserID:[self getUserId] andQuestionText:self.questText.text andPhoneNumber:self.phoneNumber.text andImageArr:imagArr andBlock:^(NSString *message) {
             [MBProgressHUD hideHUDForView:self.view];
             if ([message isEqualToString:@"提交失败"]) {
                  [MBProgressHUD showOlnyMessage:message toView:self.view];
             }else{
                 [self.navigationController popViewControllerAnimated:YES];
             }
            
         }];
     }else{
         LoginViewController *login=[[LoginViewController alloc]init];;
         [self.navigationController pushViewController:login animated:YES];
     }
    
}

- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}
@end
