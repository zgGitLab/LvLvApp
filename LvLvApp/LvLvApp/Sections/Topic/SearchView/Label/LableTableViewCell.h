//
//  LableTableViewCell.h
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LableTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *lableAttention;
@property (strong, nonatomic) IBOutlet UILabel *LabetagName;

@property (strong, nonatomic) IBOutlet UILabel *AttentionProblem;
@end
