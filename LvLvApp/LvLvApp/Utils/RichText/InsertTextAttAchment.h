//
//  InsertTextAttAchment.h
//  LvLvApp
//
//  Created by jim on 15/12/16.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsertTextAttAchment : NSTextAttachment
@property(strong, nonatomic) NSString *emojiStr;
@end
