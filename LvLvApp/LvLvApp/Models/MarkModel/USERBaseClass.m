//
//  USERBaseClass.m
//
//  Created by hope  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "USERBaseClass.h"
#import "USERData.h"


NSString *const kUSERBaseClassCode = @"Code";
NSString *const kUSERBaseClassMessage = @"Message";
NSString *const kUSERBaseClassData = @"Data";


@interface USERBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation USERBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kUSERBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kUSERBaseClassMessage fromDictionary:dict];
    NSObject *receivedUSERData = [dict objectForKey:kUSERBaseClassData];
    NSMutableArray *parsedUSERData = [NSMutableArray array];
    if ([receivedUSERData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedUSERData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedUSERData addObject:[USERData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedUSERData isKindOfClass:[NSDictionary class]]) {
       [parsedUSERData addObject:[USERData modelObjectWithDictionary:(NSDictionary *)receivedUSERData]];
    }

    self.data = [NSArray arrayWithArray:parsedUSERData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kUSERBaseClassCode];
    [mutableDict setValue:self.message forKey:kUSERBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kUSERBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kUSERBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kUSERBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kUSERBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kUSERBaseClassCode];
    [aCoder encodeObject:_message forKey:kUSERBaseClassMessage];
    [aCoder encodeObject:_data forKey:kUSERBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    USERBaseClass *copy = [[USERBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
