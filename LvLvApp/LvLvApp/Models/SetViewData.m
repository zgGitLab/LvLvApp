//
//  SetViewData.m
//
//  Created by hope  on 15/12/22
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "SetViewData.h"


NSString *const kSetViewDataImageUrl = @"ImageUrl";
NSString *const kSetViewDataUserID = @"UserID";
NSString *const kSetViewDataIsCheck = @"IsCheck";
NSString *const kSetViewDataRemark = @"Remark";
NSString *const kSetViewDataProfession = @"Profession";
NSString *const kSetViewDataIsPass = @"IsPass";
NSString *const kSetViewDataCompanyName = @"CompanyName";
NSString *const kSetViewDataUserName = @"UserName";


@interface SetViewData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SetViewData

@synthesize imageUrl = _imageUrl;
@synthesize userID = _userID;
@synthesize isCheck = _isCheck;
@synthesize remark = _remark;
@synthesize profession = _profession;
@synthesize isPass = _isPass;
@synthesize companyName = _companyName;
@synthesize userName = _userName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.imageUrl = [self objectOrNilForKey:kSetViewDataImageUrl fromDictionary:dict];
            self.userID = [[self objectOrNilForKey:kSetViewDataUserID fromDictionary:dict] doubleValue];
            self.isCheck = [[self objectOrNilForKey:kSetViewDataIsCheck fromDictionary:dict] doubleValue];
            self.remark = [self objectOrNilForKey:kSetViewDataRemark fromDictionary:dict];
            self.profession = [self objectOrNilForKey:kSetViewDataProfession fromDictionary:dict];
            self.isPass = [[self objectOrNilForKey:kSetViewDataIsPass fromDictionary:dict] doubleValue];
            self.companyName = [self objectOrNilForKey:kSetViewDataCompanyName fromDictionary:dict];
            self.userName = [self objectOrNilForKey:kSetViewDataUserName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.imageUrl forKey:kSetViewDataImageUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kSetViewDataUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isCheck] forKey:kSetViewDataIsCheck];
    [mutableDict setValue:self.remark forKey:kSetViewDataRemark];
    [mutableDict setValue:self.profession forKey:kSetViewDataProfession];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isPass] forKey:kSetViewDataIsPass];
    [mutableDict setValue:self.companyName forKey:kSetViewDataCompanyName];
    [mutableDict setValue:self.userName forKey:kSetViewDataUserName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.imageUrl = [aDecoder decodeObjectForKey:kSetViewDataImageUrl];
    self.userID = [aDecoder decodeDoubleForKey:kSetViewDataUserID];
    self.isCheck = [aDecoder decodeDoubleForKey:kSetViewDataIsCheck];
    self.remark = [aDecoder decodeObjectForKey:kSetViewDataRemark];
    self.profession = [aDecoder decodeObjectForKey:kSetViewDataProfession];
    self.isPass = [aDecoder decodeDoubleForKey:kSetViewDataIsPass];
    self.companyName = [aDecoder decodeObjectForKey:kSetViewDataCompanyName];
    self.userName = [aDecoder decodeObjectForKey:kSetViewDataUserName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_imageUrl forKey:kSetViewDataImageUrl];
    [aCoder encodeDouble:_userID forKey:kSetViewDataUserID];
    [aCoder encodeDouble:_isCheck forKey:kSetViewDataIsCheck];
    [aCoder encodeObject:_remark forKey:kSetViewDataRemark];
    [aCoder encodeObject:_profession forKey:kSetViewDataProfession];
    [aCoder encodeDouble:_isPass forKey:kSetViewDataIsPass];
    [aCoder encodeObject:_companyName forKey:kSetViewDataCompanyName];
    [aCoder encodeObject:_userName forKey:kSetViewDataUserName];
}

- (id)copyWithZone:(NSZone *)zone
{
    SetViewData *copy = [[SetViewData alloc] init];
    
    if (copy) {

        copy.imageUrl = [self.imageUrl copyWithZone:zone];
        copy.userID = self.userID;
        copy.isCheck = self.isCheck;
        copy.remark = [self.remark copyWithZone:zone];
        copy.profession = [self.profession copyWithZone:zone];
        copy.isPass = self.isPass;
        copy.companyName = [self.companyName copyWithZone:zone];
        copy.userName = [self.userName copyWithZone:zone];
    }
    
    return copy;
}


@end
