//
//  FailedViewController.m
//  LvLvApp
//
//  Created by 赵俊杰 on 16/5/26.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import "FailedViewController.h"

@interface FailedViewController ()

@end

@implementation FailedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"身份认证";
    //创建约束
    [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.left);
        make.right.equalTo(self.view.right);
        make.top.equalTo(self.view.top);
        make.bottom.equalTo(self.view.bottom);
    }];
    
    [self.image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(122);
        make.height.equalTo(95);
        make.left.equalTo(self.bgScrollView.left).offset((kWidth-122)/2);
        make.top.equalTo(self.bgScrollView.top).offset(135);
    }];
    
    [self.lable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.image.bottom).offset(38);
        make.left.equalTo(self.bgScrollView.left).offset((kWidth-170)/2);
        make.height.equalTo(40);
        make.width.equalTo(170);
    }];
    
    [self.reasonContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lable.bottom).offset(29);
        make.left.equalTo(self.view.left).offset(40);
        make.right.equalTo(self.view.right).offset(-40);
        make.height.equalTo(100);
    }];
    
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(180);
        make.height.equalTo(45);
        make.left.equalTo(self.bgScrollView.left).offset((kWidth-180)/2);
        make.top.equalTo(self.reasonContent.bottom).offset(29);
        self.bgScrollView.contentSize = CGSizeMake(kWidth, CGRectGetMaxY(self.applyBtn.frame)+50);
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)applyBtnClick:(UIButton *)sender {
    NSLog(@"重新申请");
}
@end
