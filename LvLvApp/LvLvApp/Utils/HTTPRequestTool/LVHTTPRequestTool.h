//
//  LVHTTPRequestTool.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/5/10.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LVHTTPRequestTool : NSObject

+ (instancetype)sharedDataTool;

+ (void)get:(NSString *)url params:(NSDictionary *)params success:(void (^)(id json))success failure:(void (^)(NSError *error))failure;

+ (void)post:(NSString*)url params:(NSDictionary *)params success:(void (^)(id json))success failure:(void (^)(NSError *error))failure;

+ (void)post:(NSString*)url params:(NSDictionary *)params success:(void (^)(id json))success failureMessage:(void (^)(NSString *errorMessage))failureMessage;
@end
