//
//  EditTimeView.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/9.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PerTimeDatePickViewDelegat <NSObject>

- (void)selectTimeDate:(NSString *)timeDate;
@end

@interface EditTimeView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>
@property(nonatomic,assign)id<PerTimeDatePickViewDelegat>delegate;
@property(nonatomic,strong)UIPickerView *picKView;
@property(nonatomic,assign)int count;
@property(nonatomic,strong)NSString *selectStr;
@property(nonatomic,strong)NSArray *plistArray;
@property(nonatomic,strong)UIView *backGroundView;
-(id)initWithCount:(int)count;
-(void)showInView;
@end
