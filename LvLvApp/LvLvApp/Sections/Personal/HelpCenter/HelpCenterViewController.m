//
//  HelpCenterViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "HelpCenterViewController.h"
#import "SendQuestionViewController.h"

#import "IWhistleBlowingViewController.h"
#import "LoginViewController.h"
#import "HelpDocumentViewController.h"
@interface HelpCenterViewController ()

@end

@implementation HelpCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];

    self.title = @"反馈帮助中心";
}
- (IBAction)sendQuestion:(UIButton *)sender {
    // 反馈问题
    SendQuestionViewController *sendVC = [[SendQuestionViewController alloc] init];
    [self.navigationController pushViewController:sendVC animated:YES];
}
- (IBAction)reportBtn:(UIButton *)sender {
    if (![[self getUserId] isEqualToString:@"(null)"]) {
        IWhistleBlowingViewController *iwView =[[IWhistleBlowingViewController alloc]init];
        [self.navigationController pushViewController:iwView animated:YES];
        
    }else{
        LoginViewController *login=[[LoginViewController alloc]init];;
        [self.navigationController pushViewController:login animated:YES];
    }

    
}
- (IBAction)helpBook:(UIButton *)sender {
    
    HelpDocumentViewController *helpView =[[HelpDocumentViewController alloc]init];
    [self.navigationController pushViewController:helpView animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
