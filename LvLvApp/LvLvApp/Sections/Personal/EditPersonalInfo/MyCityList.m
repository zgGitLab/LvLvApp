//
//  MyCityList.m
//  LvLvApp
//
//  Created by IOS8 on 15/12/2.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "MyCityList.h"
@interface MyCityList ()
{
    int selectRow;
}
@end

@implementation MyCityList

-(id)init{
    self = [super init];
    if (self) {
         self.plistArray=[self getPlistArrayByplistName:@"editCity"];
        selectRow = 0;
        //初始化背景视图，添加手势
        self.frame = CGRectMake(0, 0, SIZE.width, SIZE.height);
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.4];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
        [self addGestureRecognizer:tapGesture];
        self.backGroundView = [[UIView alloc] initWithFrame:CGRectMake(0, ([UIScreen mainScreen].bounds.size.height), SIZE.width, 0)];
        self.backGroundView.backgroundColor = [UIColor whiteColor];
        
        UIPickerView *picKView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, SIZE.width, 196)];
        picKView.delegate = self;
        picKView.dataSource = self;
        [self.backGroundView addSubview:picKView];
        
        UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 44)];
        toolBar.backgroundColor = COLOR(246, 247, 249);
        toolBar.barStyle = UIBarStyleDefault;
        UIButton  *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnRight addTarget:self action:@selector(finsh) forControlEvents:UIControlEventTouchUpInside];
        [btnRight setTitleColor:COLOR(59, 57, 57) forState:UIControlStateNormal];
        [btnRight setTitle:@"完成" forState:UIControlStateNormal];
        [btnRight setFrame:CGRectMake(0, 0, 60, 30)];
        UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
        UIButton  *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnLeft addTarget:self action:@selector(docancel) forControlEvents:UIControlEventTouchUpInside];
        [btnLeft setTitleColor:COLOR(59, 57, 57) forState:UIControlStateNormal];
        [btnLeft setTitle:@"取消" forState:UIControlStateNormal];
        [btnLeft setFrame:CGRectMake(0, 0, 60, 30)];
        UIBarButtonItem *leftButton  = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
        UIBarButtonItem *fixedButton  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace target: nil action: nil];
        NSArray *array = [[NSArray alloc] initWithObjects: leftButton,fixedButton,rightButton, nil];
        [toolBar setItems: array];
        [self.backGroundView addSubview:toolBar];
        [self addSubview:self.backGroundView];
        [UIView animateWithDuration:0.25 animations:^{
            [self.backGroundView setFrame:CGRectMake(0, SIZE.height - 240, SIZE.width, 240)];
            
        } completion:^(BOOL finished) {
            
        }];
    }
    
    return self;
}

-(NSArray *)getPlistArrayByplistName:(NSString *)plistName{
    
    NSString *path= [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
    NSArray * array=[[NSArray alloc] initWithContentsOfFile:path];
    [self setArrayClass:array];
    return array;
}

-(void)setArrayClass:(NSArray *)array{
    _dicKeyArray=[[NSMutableArray alloc] init];
    for (NSDictionary *levelTwo in array) {
        _levelTwoDic=levelTwo;
        [_dicKeyArray addObject:[_levelTwoDic allKeys] ];
       
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSArray *rowArray=[[NSArray alloc] init];
        NSInteger pIndex = [pickerView selectedRowInComponent:0];
        NSDictionary *dic=_plistArray[pIndex];
        for (id dicValue in [dic allValues]) {
            if ([dicValue isKindOfClass:[NSArray class]]) {
                if (component%2==1) {
                    rowArray=dicValue;
                }else{
                    rowArray=_plistArray;
                }
            }
        }
    
    return rowArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *rowTitle=nil;
    NSInteger pIndex = [pickerView selectedRowInComponent:0];
    NSDictionary *dic=_plistArray[pIndex];
    if(component%2==0)
    {
        rowTitle=_dicKeyArray[row][component];
    }
    for (id aa in [dic allValues]) {
        if ([aa isKindOfClass:[NSArray class]]&&component%2==1){
            NSArray *bb=aa;
            if (bb.count>row) {
                rowTitle=aa[row];
            }
        }
    }
    return rowTitle;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    // 当滑动第一个区的时候刷新第二个区的内容
    if (component%2==0) {
        
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        self.cityStr = nil;
        NSInteger pIndex = [pickerView selectedRowInComponent:0];
        selectRow = (int)pIndex;
        self.provinceStr =_dicKeyArray[row][component];
        
    }else{
        NSInteger pIndex = [pickerView selectedRowInComponent:0];
//        selectRow = (int)pIndex;
        NSDictionary *dic=_plistArray[pIndex];
        for (id aa in [dic allValues]) {
            if ([aa isKindOfClass:[NSArray class]]&&component%2==1){
                NSArray *bb=aa;
                if (bb.count>row) {
                    self.cityStr=aa[row];
                }
            }
        }

    }

    
}

- (void)finsh
{
    if (!self.provinceStr) {
        self.provinceStr = @"北京";
    }
    if (!self.cityStr) {
        NSLog(@"%d",selectRow);
         NSDictionary *dic=_plistArray[selectRow];
        NSArray *arr = [dic objectForKey:self.provinceStr];
        self.cityStr = arr[0];
    }
    
    [self.delegate selectareaStr:[NSString stringWithFormat:@"%@  %@",self.provinceStr,self.cityStr]];

    [self docancel];
}

- (void)docancel
{
    [UIView animateWithDuration:0.25 animations:^{
        [self.backGroundView setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 0)];
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
    
}

- (void)tapGesture
{
    [self docancel];
}

-(void)showInView
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app.window addSubview: self];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
