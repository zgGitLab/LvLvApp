//
//  labelDetailHeaderView.h
//  LvLvApp
//
//  Created by hope on 15/12/5.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface labelDetailHeaderView : UIView
@property (strong, nonatomic) IBOutlet UILabel *LablePrAnComment;//问题回答评论
@property (strong, nonatomic) IBOutlet UIButton *LableAttention;//关注

@property (strong, nonatomic) IBOutlet UILabel *LableNamelabe;//标签名字
@property (strong, nonatomic) IBOutlet UILabel *LableAttentionCount;//标签总数
@property (strong, nonatomic) IBOutlet UIImageView *lableImage;

@property (strong, nonatomic) IBOutlet UIView *LAView;
@property (strong, nonatomic) IBOutlet UIImageView *backImage;
@property (strong, nonatomic)NSMutableArray *LlableArray;
@property (strong, nonatomic)NSString *UserID;
@end
