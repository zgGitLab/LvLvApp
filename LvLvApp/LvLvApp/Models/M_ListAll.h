//
//  M_ListAll.h
//
//  Created by jim  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface M_ListAll : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *tID;
@property (nonatomic, strong) id questionContent;
@property (nonatomic, strong) id logoUrl;
@property (nonatomic, strong) NSString *answerContent;
@property (nonatomic, assign) double tagCount;
@property (nonatomic, assign) double collectCount;
@property (nonatomic, assign) double counts;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double commentCount;
@property (nonatomic, assign) double aUserID;
@property (nonatomic, assign) double tUserID;
@property (nonatomic, assign) double userID;
@property (nonatomic, strong) NSString *answerID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
