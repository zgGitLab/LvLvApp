//
//  CasusViewController.h
//  案例
//
//  Created by IOS8 on 15/9/28.
//  Copyright (c) 2015年 IOS8. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CasusViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MBProgressHUDDelegate>
@property (weak, nonatomic) IBOutlet UITableView *mainTable;

@end
