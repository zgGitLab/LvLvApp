//
//  S_PrivateLetterBaseClass.m
//
//  Created by IOS8  on 15/12/22
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_PrivateLetterBaseClass.h"
#import "S_PrivateLetterData.h"


NSString *const kS_PrivateLetterBaseClassCode = @"Code";
NSString *const kS_PrivateLetterBaseClassMessage = @"Message";
NSString *const kS_PrivateLetterBaseClassData = @"Data";


@interface S_PrivateLetterBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_PrivateLetterBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_PrivateLetterBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_PrivateLetterBaseClassMessage fromDictionary:dict];
    NSObject *receivedS_PrivateLetterData = [dict objectForKey:kS_PrivateLetterBaseClassData];
    NSMutableArray *parsedS_PrivateLetterData = [NSMutableArray array];
    if ([receivedS_PrivateLetterData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_PrivateLetterData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_PrivateLetterData addObject:[S_PrivateLetterData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_PrivateLetterData isKindOfClass:[NSDictionary class]]) {
       [parsedS_PrivateLetterData addObject:[S_PrivateLetterData modelObjectWithDictionary:(NSDictionary *)receivedS_PrivateLetterData]];
    }

    self.data = [NSArray arrayWithArray:parsedS_PrivateLetterData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_PrivateLetterBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_PrivateLetterBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kS_PrivateLetterBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_PrivateLetterBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_PrivateLetterBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_PrivateLetterBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_PrivateLetterBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_PrivateLetterBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_PrivateLetterBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_PrivateLetterBaseClass *copy = [[S_PrivateLetterBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
