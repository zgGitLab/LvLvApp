//
//  S_ChatListBaseClass.m
//
//  Created by IOS8  on 15/12/23
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_ChatListBaseClass.h"
#import "S_ChatListData.h"


NSString *const kS_ChatListBaseClassCode = @"Code";
NSString *const kS_ChatListBaseClassMessage = @"Message";
NSString *const kS_ChatListBaseClassData = @"Data";


@interface S_ChatListBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_ChatListBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kS_ChatListBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kS_ChatListBaseClassMessage fromDictionary:dict];
    NSObject *receivedS_ChatListData = [dict objectForKey:kS_ChatListBaseClassData];
    NSMutableArray *parsedS_ChatListData = [NSMutableArray array];
    if ([receivedS_ChatListData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedS_ChatListData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedS_ChatListData addObject:[S_ChatListData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedS_ChatListData isKindOfClass:[NSDictionary class]]) {
       [parsedS_ChatListData addObject:[S_ChatListData modelObjectWithDictionary:(NSDictionary *)receivedS_ChatListData]];
    }

    self.data = [NSArray arrayWithArray:parsedS_ChatListData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kS_ChatListBaseClassCode];
    [mutableDict setValue:self.message forKey:kS_ChatListBaseClassMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kS_ChatListBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kS_ChatListBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kS_ChatListBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kS_ChatListBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kS_ChatListBaseClassCode];
    [aCoder encodeObject:_message forKey:kS_ChatListBaseClassMessage];
    [aCoder encodeObject:_data forKey:kS_ChatListBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_ChatListBaseClass *copy = [[S_ChatListBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
