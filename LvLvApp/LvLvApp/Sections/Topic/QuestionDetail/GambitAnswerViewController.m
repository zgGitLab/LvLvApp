//
//  GambitAnswerViewController.m
//  LvLvApp
//
//  Created by hope on 15/10/13.
//  Copyright (c) 2015年 gyy. All rights reserved.
//

#import "PerMainPageViewController.h"
#import "OtherPerMainViewController.h"
#import "AnswerDetailViewController.h"
#import "TopicHomeViewController.h"
#import "GambitAnswerViewController.h"
#import "TableViewCell.h"
#import "AnswerQuestionViewController.h"
#import "AnswerAndLabelViewController.h"
#import "LoginViewController.h"
#define kCell @"TableViewCell"
#import "CustomActionSheet.h"
#import "ShareView.h"
#import <ShareSDKExtension/ShareSDK+Extension.h>
#import "EditQusetionViewController.h"
#import "EditAnswerViewController.h"

@interface GambitAnswerViewController ()<UIActionSheetDelegate>
{
    UIButton *_arrowBtn;//右箭头
    UILabel *_concernLabel;//n人关注
    UIView *_bottomView;//回答关注view
    UIButton *_concernBtn;//关注按钮
    UIView *_headerViews;//头视图
    UIButton *_IAnswer;//我来回答
    
    AnswerAllData *answerData;
    
    NSMutableArray *_answerListArray;
    NSMutableArray *imageList;//图片数组
    
    NSString *reason;
    NSString *contenttext;
    NSAttributedString *attrText;
    BOOL flag;
    CGFloat headerHeight;
}

@end

@implementation GambitAnswerViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.hidden = NO;
    
    UIBarButtonItem *leftItem =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"返回箭头"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem=leftItem;
    
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"分享"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(shareClick:)];
    self.navigationItem.rightBarButtonItem=rightItem;

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self getData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    reason = [Utils isBlankString:reason]?@"":reason;
    [self createTableView];
    
    _indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
}



#pragma mark --请求数据

- (void)getData{
    //传入的参数
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[NSString stringWithFormat:@"%@",self.UserID] forKey:@"TUserID"];
    if ([kUserID integerValue]!=0) {
        [parameters setObject:kUserID forKey:@"UserID"];
    }
    [parameters setObject:[NSString stringWithFormat:@"%@",self.TID] forKey:@"TID"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:kAllAnswerUrl,K_IP] params:parameters success:^(id json) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        AnswerAllBaseClass *dataModel  = [[AnswerAllBaseClass alloc]initWithDictionary:dict];
        answerData = dataModel.data;
        
        //用户回答列表
        _answerListArray = [NSMutableArray arrayWithArray:answerData.list];
        
        //设置title
        self.title = [NSString stringWithFormat:@"%@个回答",[NSNumber numberWithInteger:answerData.answerTotalCount]];
        imageList = [Utils subArrayFromString:answerData.questionContent];
        [self createHeaderView];//创建头视图
        [self performSelectorOnMainThread:@selector(refreshUI) withObject:nil waitUntilDone:YES];
        [self.tableView.mj_header endRefreshing];
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
    }];
}



#pragma mark -- 关注请求

- (void)concernRequestWithT:(NSInteger)t
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:kUserID forKey:@"UserID"];
    [param setObject:[NSString stringWithFormat:@"%@",answerData.focusID] forKey:@"FocusID"];
    [param setObject:[NSString stringWithFormat:@"%@",self.TID] forKey:@"TID"];
    [param setObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:t]] forKey:@"T"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:kConcernUrl,K_IP] params:param success:^(id json) {
        NSMutableString *str = [[NSMutableString alloc]initWithData:json encoding:NSUTF8StringEncoding];
        NSLog(@"str ===%@",str);
    } failure:^(NSError *error) {
        
    }];
}



#pragma mark --刷新界面

- (void)refreshUI{
    [self.tableView reloadData];
}



#pragma mark -- 草稿保存特效

//草稿view
-  (void)addView{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    UIScrollView *v = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 20)];
    v.contentSize = CGSizeMake(kWidth,40);
    v.tag = 111;
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kWidth, 20)];
    title.tag = 11;
    title.backgroundColor = [UIColor whiteColor];
    title.text = @"草稿保存中...";
    title.font = [UIFont systemFontOfSize:13];
    title.textAlignment = NSTextAlignmentCenter;
    [v addSubview:title];
    
    UILabel *tit =[[UILabel alloc]initWithFrame:CGRectMake(0, 20, kWidth, 20)];
    tit.tag = 1;
    tit.backgroundColor = [UIColor whiteColor];
    tit.text = @"草稿保存成功";
    tit.font = [UIFont systemFontOfSize:13];
    tit.textAlignment = NSTextAlignmentCenter;
    [v addSubview:tit];
    
    app.window.windowLevel = UIWindowLevelAlert;
    [app.window addSubview:v];
    [self performSelector:@selector(changeFrame) withObject:nil afterDelay:0.5];
}



//修改特效的frame
- (void)changeFrame{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    UIScrollView *s = [app.window viewWithTag:111];
    [UIView animateKeyframesWithDuration:0.3 delay:0 options:UIViewKeyframeAnimationOptionLayoutSubviews animations:^{
        s.contentOffset = CGPointMake(0, 20);
    } completion:^(BOOL finished) {
        [self performSelector:@selector(removeView) withObject:nil afterDelay:0.5];
    }];
}



//移除草稿保存特效
- (void)removeView{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    UIScrollView *vie = [app.window viewWithTag:111];
    app.window.windowLevel = UIWindowLevelNormal;
    [UIView animateKeyframesWithDuration:0.3 delay:0 options:UIViewKeyframeAnimationOptionLayoutSubviews animations:^{
        vie.frame = CGRectMake(0, -20, kWidth, 20);
    } completion:^(BOOL finished) {
        [vie removeFromSuperview];
    }];
    
}



#pragma mark -- 导航栏左边的返回按钮

- (void)back
{
    if (self.isIssueFlag == 1) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}



#pragma mark --创建tableView

- (void)createTableView{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-64) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1];
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = [UIColor whiteColor];
    CGRect frame=CGRectMake(0, 0, 0,CGFLOAT_MIN);
    self.tableView.tableHeaderView=[[UIView alloc]initWithFrame:frame];
    
    //注册cell
    [_tableView registerNib:[UINib nibWithNibName:@"TableViewCell" bundle:nil] forCellReuseIdentifier:kCell];
    [self setExtraCellLineHidden:_tableView];
    
    //添加刷新效果
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^ {
        [weakSelf getData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
}


#pragma mark -- 隐藏多余的分割线

- (void)setExtraCellLineHidden: (UITableView *)tableView{
    UIView *views =[ [UIView alloc]init];
    views.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:views];
}



#pragma mark --UITableView的代理方法

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _answerListArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCell forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    AnswerAllList *list = [_answerListArray objectAtIndex:indexPath.row];
    [cell.HeaderImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,list.userIamgeUrl]] placeholderImage:[UIImage imageNamed:@"头像小"]];
    cell.HeaderImage.layer.cornerRadius = cell.HeaderImage.frame.size.width/2;
    cell.HeaderImage.layer.masksToBounds = YES;
    [cell.nameBtn setTitle:list.nickName forState:UIControlStateNormal];
    [cell.nameBtn addTarget:self action:@selector(nickClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.nameBtn.tag = indexPath.row;
    cell.timeLabel.text = [NSString stringWithFormat:@"%@",list.timeDescrption];
    //创建NSMutableAttributedString实例，并将text传入
    NSString *str = list.answerContentAll;
    str = [str stringByReplacingOccurrencesOfRegex:@"<img.*src=(.*?)[^>]*?>" withString:@"[图片]" range:NSMakeRange(0, str.length)];
    str = [Utils removeBlankString:str];
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    //设置行距
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    [style setLineSpacing:3.0f];
    
    //根据给定长度与style设置attStr式样
    [attStr addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:[UIFont systemFontOfSize:14]} range:NSMakeRange(0, str.length)];
    
    cell.answerLabel.attributedText = attStr;
    cell.countsLabel.text = [NSString stringWithFormat:@"赞同 %@ · 评论 %@ · 收藏 %@",[NSNumber numberWithInteger:list.counts],[NSNumber numberWithInteger:list.commentCount],[NSNumber numberWithInteger:list.collectCount]];
    
    cell.anwer.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    cell.anwer.titleLabel.numberOfLines=3;
    [cell.anwer addTarget:self action:@selector(anwerContent:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return headerHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = 93;
    AnswerAllList *list = [_answerListArray objectAtIndex:indexPath.row];
    NSString *str = [Utils replaceImageString:list.answerContentAll];
    str = [Utils removeBlankString:str];
    CGSize size = [str boundingRectWithSize:CGSizeMake(kWidth-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName :[UIFont systemFontOfSize:14]} context:nil].size;
    if(size.height >= 51 ){
        height += 51-3;
    }else{
        height += size.height;
    }
    
    return height-3;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    AnswerDetailViewController *an=[[AnswerDetailViewController alloc]init];
    AnswerAllList *list = [_answerListArray objectAtIndex:indexPath.row];
    an.answerID = list.answerID;
    an.UserID = list.userID;
    an.tUserID = [NSString stringWithFormat:@"%@",self.UserID];
    an.TID = self.TID;
    an.isEnterFromQuestionPage = @"1";
    an.backBlock = ^ {
        [self getData];
    };
    
    [self.navigationController pushViewController:an animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return _headerViews;
}



#pragma mark -- 创建头视图

- (void)createHeaderView
{
    CGFloat totalHeight=0;
    _headerViews = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 500)];
    [_headerViews setBackgroundColor:[UIColor whiteColor]];
    
    //topbutton标签
    UIButton *topButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 3, kWidth, 30)];
    [topButton addTarget:self action:@selector(topClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *backImage =[[UIImageView alloc]initWithFrame:CGRectMake(0, topButton.bounds.size.height+5,self.view.bounds.size.width, 0.5)];
    [backImage setImage:[UIImage imageNamed:@"线"]];
    [_headerViews addSubview:backImage];
    
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i < answerData.dt.count;i++ ) {
        CGFloat widths = [[answerData.dt objectAtIndex:i] boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:13]} context:nil].size.width;
        NSString *str = [NSString stringWithFormat:@"%lf",widths+20];
        [arr addObject:str];
    }
    
    CGRect frame = CGRectNull;
    for (int i = 0; i < answerData.dt.count; i ++) {
        UILabel *label = [[UILabel alloc]init];
        label.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:236.0/255.0 blue:253.0/255.0 alpha:1];
        label.textAlignment = NSTextAlignmentCenter;
        label.layer.cornerRadius = 2;
        label.clipsToBounds =YES;
        label.font = [UIFont systemFontOfSize:13];
        label.textColor = [UIColor colorWithRed:93/255.0 green:137/255.0 blue:176/255.0 alpha:1];
        
        if (i == 0) {
            label.frame = CGRectMake(10, 5, [arr[i] integerValue], 20);
            frame = label.frame;
            [label setText:[answerData.dt objectAtIndex:i]];
            [topButton addSubview:label];
        }else{
            label.frame = CGRectMake(CGRectGetMaxX(frame)+5, 5, [arr[i] integerValue], 20);
            if (CGRectGetMaxX(label.frame) > kWidth-80) {
                label.frame = CGRectMake(CGRectGetMaxX(frame)+5, 5, 40, 20);
                [label setText:@"···"];
                [topButton addSubview:label];
                break;
            }else{
                [label setText:[answerData.dt objectAtIndex:i]];
                [topButton addSubview:label];
            }
            
            frame = label.frame;
        }
    }
    
    _arrowBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    [_arrowBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_arrowBtn setEnabled:NO];
    [_arrowBtn setBackgroundImage:[UIImage imageNamed:@"箭头"] forState:UIControlStateNormal];
    [_arrowBtn setFrame:CGRectMake(kWidth-40 ,topButton.bounds.origin.y+5,11,18)];
    [topButton addSubview:_arrowBtn];
    [_headerViews addSubview:topButton];
    
    totalHeight += topButton.frame.size.height+topButton.frame.origin.y+5+12;
    
    //标题
    UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [titleButton setTitle:answerData.title forState:UIControlStateNormal];
    titleButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleButton.titleLabel.numberOfLines = 0;
    titleButton.frame = CGRectMake(0, totalHeight, kWidth, [Utils textHeightFromTextString:answerData.title width:kWidth-20 fontSize:20.0f]);
    reason = answerData.title;
    reason = [Utils isBlankString:reason]?@"":reason;
    titleButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    titleButton.titleEdgeInsets = UIEdgeInsetsMake(0,10,0,10);
    [titleButton setTitleColor:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1] forState:UIControlStateNormal];
    titleButton.titleLabel.font=[UIFont systemFontOfSize:20.f];
    titleButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    [_headerViews addSubview:titleButton];
    totalHeight += titleButton.frame.size.height+3;
    
    _concernLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, totalHeight, 80, 15)];
    _concernLabel.numberOfLines = 0;
    [_concernLabel setTextColor:[UIColor colorWithRed:180.0/255.0 green:180/255.0 blue:181/255.0 alpha:1]];
    _concernLabel.font = [UIFont systemFontOfSize:11];
    [_concernLabel setText:answerData.dscrption];
    [_headerViews addSubview:_concernLabel];
    totalHeight += _concernLabel.frame.size.height+7;
    
    NSString *string = nil;
    string = answerData.questionContent;
    CGFloat height1 = 0;
    if(![Utils isBlankString:answerData.questionContent]){
        height1 = [Utils textHeightFromTextString:[Utils replaceImageString:string] width:kWidth fontSize:16];

        //n人关注下面的内容content
        CGFloat fontsize = [SingeData sharedInstance].answerContentSize+1;
        RTFTextView *label = [[RTFTextView alloc]initWithFrame:CGRectMake(10-4, totalHeight, kWidth-20+8, height1) andTextColor:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1] andFont:fontsize andText:string withNumOfLines:0 andImageArray:imageList];
        UIView *hid = [[UIView alloc]initWithFrame:CGRectMake(10-4, totalHeight-1, kWidth, 3)];
        hid.backgroundColor = [UIColor whiteColor];
        [_headerViews addSubview:label];
        [_headerViews addSubview:hid];
        attrText = label.attributedText;
        contenttext = string;
        totalHeight += label.frame.size.height;
    }
    
    //回答关注view
    _bottomView=[[UIView alloc]initWithFrame:CGRectMake(0, totalHeight, self.view.bounds.size.width, 60+5)];
    [_bottomView setBackgroundColor:[UIColor clearColor]];
    [_headerViews addSubview:_bottomView];
    
    //横向两条线
    //上面的线
    UIView *Image=[[UIView alloc]initWithFrame:CGRectMake(0, 2,_bottomView.bounds.size.width, 0.5)];
    [Image setBackgroundColor: [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:0.7]];
    [_bottomView addSubview:Image];
    //下面的线
    if (_answerListArray.count == 0) {
        UIImageView *Image =[[UIImageView alloc]initWithFrame:CGRectMake(0, 64+5,_bottomView.bounds.size.width, 0.5)];
        Image.alpha = 0.7;
        [Image setImage:[UIImage imageNamed:@"线"]];
        [_bottomView addSubview:Image];
    }
    
    //竖直的线
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(_bottomView.bounds.size.width/2, 15+3, 0.5, 35)];
    [line setBackgroundColor: [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1]];
    [_bottomView addSubview:line];
    
    _IAnswer= [UIButton buttonWithType:UIButtonTypeCustom];
    
    //2已存草稿，1已回答，0回答
    [_IAnswer setImage:[UIImage imageNamed:@"answer"] forState:UIControlStateNormal];
    NSString *str ;
    if ([answerData.category isEqualToString:@"0"]) {
        str = @"我来回答";
    }else if([answerData.category isEqualToString:@"1"]){
        str = @"查看回答";
    }else{
        str = @"已存草稿";
    }
    
    [_IAnswer setTitle:str forState:UIControlStateNormal];
    [_IAnswer setTitleColor:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1] forState:UIControlStateNormal];
    _IAnswer.titleLabel.font = [UIFont systemFontOfSize:10];
    _IAnswer.imageEdgeInsets = UIEdgeInsetsMake(-25, 10, 0, 0);
    _IAnswer.titleEdgeInsets = UIEdgeInsetsMake(0, -18, -20, 0);
    _IAnswer.titleLabel.textColor = [UIColor blackColor];
    [_IAnswer setFrame:CGRectMake(_bottomView.bounds.size.width/4-20 ,_bottomView.bounds.size.height/4-8,55,58)];
    [_IAnswer addTarget:self action:@selector(answerClick) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:_IAnswer];
    
    _concernBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _concernBtn.titleLabel.textColor = [UIColor blackColor];
    [_concernBtn setTitle:@"关注问题" forState:UIControlStateNormal];
    [_concernBtn setTitle:@"取消关注" forState:UIControlStateSelected];
    [_concernBtn setImage:[UIImage imageNamed:@"focus"] forState:UIControlStateNormal];
    [_concernBtn setImage:[UIImage imageNamed:@"unfocus"] forState:UIControlStateSelected];
    [_concernBtn setTitleColor:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1] forState:UIControlStateNormal];
    [_concernBtn setTitleColor:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1] forState:UIControlStateSelected];
    
    
    _concernBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    _concernBtn.imageEdgeInsets = UIEdgeInsetsMake(-25, 10, 0, 0);
    _concernBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -18, -20, 0);
    _concernBtn.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentCenter;
    _concernBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [_concernBtn setFrame:CGRectMake(_bottomView.bounds.size.width/2+_bottomView.bounds.size.width/4-20-15 ,_bottomView.bounds.size.height/4-8,55,58)];
    [_concernBtn addTarget:self action:@selector(_concernBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([answerData.state isEqualToString: @"1"]) {
        _concernBtn.selected = YES;
    }else {
        _concernBtn.selected = NO;
    }
    
    [_bottomView addSubview:_concernBtn];
    
    
    totalHeight+=_bottomView.frame.size.height;
    headerHeight = totalHeight;
}



#pragma mark - 点击回答内容，显示详细回答

- (void)anwerContent:(UIButton *)btn
{
    AnswerDetailViewController *an=[[AnswerDetailViewController alloc]init];
    an.isEnterFromQuestionPage = @"1";
    UIView *views = [btn superview];
    TableViewCell *cell = (TableViewCell *)[views superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AnswerAllList *list = [_answerListArray objectAtIndex:indexPath.row];
    an.answerID = list.answerID;
    an.UserID = list.userID;
    an.TID = self.TID;
    __weak typeof(self) weakSelf = self;
    an.backBlock = ^ {
        [weakSelf getData];
    };
    an.tUserID = [NSString stringWithFormat:@"%@",self.UserID];
    [self.navigationController pushViewController:an animated:YES];
}



#pragma mark -- 进入标签详情界面

- (void)topClick:(id)sender{
    AnswerAndLabelViewController *controller = [[AnswerAndLabelViewController alloc]init];
    controller.tUserID = self.UserID;
    controller.TID = self.TID;
    controller.userID = kUserID;
    [self.navigationController pushViewController:controller animated:YES];
}



#pragma mark -- 点击昵称事件

- (void)nickClick:(UIButton *)btn{
    AnswerAllList *list = [_answerListArray objectAtIndex:btn.tag];
    NSLog(@"userid :: %@",kUserID);
    if ([list.userID isEqualToString:kUserID]) {
        PerMainPageViewController *per = [[PerMainPageViewController alloc]init];
        per.userID = kUserID;
        [self.navigationController pushViewController:per animated:YES];
    }else{
        OtherPerMainViewController *other = [[OtherPerMainViewController alloc]init];
        other.userID = kUserID;
        other.otherID = list.userID;
        [self.navigationController pushViewController:other animated:YES];
    }
}



#pragma mark -- 分享事件

- (void)shareClick:(id)sender{
    if ([kUserID integerValue]!=0) {
        NSArray *imageArray = @[@"微信好友",@"微信朋友圈",@"印象笔记",@"有道云笔记",@"微博",@"QQ",@"邮件",@"复制链接"];
        NSArray *titleArray = @[@"微信好友",@"微信朋友圈",@"印象笔记",@"有道云笔记",@"微博",@"QQ",@"邮件",@"复制链接"];
        titleArray = [Utils isClientInstalled:titleArray];
        imageArray = [Utils isClientInstalled:imageArray];
        
        NSArray  *editArray=nil;
        NSArray  *editArray1 = nil;
        if ([[NSString stringWithFormat:@"%@",self.UserID] isEqualToString:kUserID]) {
            editArray = @[@"编辑",@"删除",@"",@""];
            editArray1 = @[@"编辑问题",@"删除问题",@"",@""];
        }else{
            editArray = @[@"举报",@"",@"",@""];
            editArray1 = @[@"举报问题",@"",@"",@""];
        }
        
        NSString *title = @"分享问题";

        UIImage *shareImage = [UIImage imageNamed:@"logos"];

        NSString *shareUrl = [NSString stringWithFormat:@"http://%@/Mobile?UserID=%@&TID=%@&AnswerID=%@&AUserID=%@&TUserID=%@",k_SHAREIP,kUserID,self.TID,self.Answer_ID,@"1000000",self.UserID];
        NSString *shareText = [NSString stringWithFormat:@"分享问题《%@》%@ (分享自@律律App)",[Utils replaceImageString:answerData.title],shareUrl];

        ShareView *share = [[ShareView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
        share.editQuestionBlock = ^ {
            EditQusetionViewController *edit = [[EditQusetionViewController alloc]init];
            
            //富文本显示的字符串和数组
            edit.content = [NSMutableString stringWithFormat:@"%@",contenttext];
            edit.imageList = imageList;
            edit.tID =[NSString stringWithFormat:@"%@",self.TID];
            __weak typeof(self) weakSelf = self;
            edit.backBlock = ^ {
                [weakSelf getData];
            };
            
            [self.navigationController pushViewController:edit animated:YES];
        };
        
        share.deleteQuestionBlock = ^ {
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            
            //申明返回的结果是json类型
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            //传入的参数
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            [parameters setObject:[NSString stringWithFormat:@"%@",self.UserID] forKey:@"UserID"];
            [parameters setObject:[NSString stringWithFormat:@"%@",self.TID] forKey:@"TID"];
            
            //发送请求
            [manager POST:[NSString stringWithFormat:kDeleteQuestion,K_IP] parameters:parameters success:^ (AFHTTPRequestOperation *operation, id responseObject) {
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"%@",error);
            }];
        };
        
        //举报的参数
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:kUserID forKey:@"UserID"];
        [dict setObject:self.TID?self.TID:@"" forKey:@"ID"];
        [dict setObject:@"1" forKey:@"Category"];//举报类型 1问题，2回答，3用户
        [dict setObject:reason forKey:@"Title"];
        
        share.dictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"imageArr1":imageArray,@"titleArr1":titleArray,@"imageArr2":editArray,@"titleArr2":editArray1,@"title":title,@"shareText":shareText,@"shareUrl":shareUrl,@"shareImage":shareImage,@"dict":dict,@"shareTitle":answerData.title}];
        
        //分享到笔记传的字典
        NSString *qc = [Utils isBlankString:answerData.questionContent]?@"":answerData.questionContent;
        share.noteDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"imageArr1":imageArray,@"titleArr1":titleArray,@"imageArr2":editArray,@"titleArr2":editArray1,@"title":title,@"shareText":qc,@"shareUrl":shareUrl,@"shareImage":shareImage,@"dict":dict,@"shareTitle":answerData.title}];
        
        share.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshUIs) name:@"refresh" object:nil];
        [APP.window addSubview:share];
    }else{
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }
}



#pragma mark -- 刷新UI
- (void)refreshUIs{
    [self createHeaderView];
    [self.tableView reloadData];
}



#pragma mark -- 关注按钮事件

-(void)_concernBtnAction:(UIButton *)sender
{
    if ([kUserID integerValue]!=0) {
        NSInteger s = 0;
        sender.selected = !sender.selected;
        
        if (sender.selected) {
            s = 1;
        }else{
            s = 0;
        }
        
        [self concernRequestWithT:sender.selected];
        [self performSelector:@selector(backRefresh) withObject:nil afterDelay:0.2];
    }else{
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }
}



#pragma mark --我来回答事件

- (void)answerClick
{
    if([kUserID integerValue]!=0){
        
        BOOL flags = NO;
        AnswerAllList *list = nil;

        for (AnswerAllList *lists in _answerListArray) {
            if ([lists.userID isEqualToString:kUserID]) {
                flags = YES;
                list = lists;
                break;
            }
        }

        //如果是已经回答，则进入回答详情
        if ([answerData.category isEqualToString:@"1"]) {
            AnswerDetailViewController *an=[[AnswerDetailViewController alloc]init];
            //            an.isEnterFromQuestionPage = @"1";
            an.answerID = list.answerID;
            an.UserID = list.userID;
            an.TID = self.TID;
            __weak typeof(self) weakSelf = self;
            an.backBlock = ^ {
                [weakSelf getData];
            };
            an.tUserID = [NSString stringWithFormat:@"%@",self.UserID];
            [self.navigationController pushViewController:an animated:YES];
        }else{
            AnswerQuestionViewController *answer = [[AnswerQuestionViewController alloc]init];
            answer.title = @"我的回答";
            answer.category = answerData.category;
            answer.UserID = kUserID;
            answer.TID = self.TID;
            if (![Utils isBlankString:answerData.answerID]) {
                answer.answerID = answerData.answerID;
            }
            
            answer.answerID = answerData.answerID;
            if (![answerData.category isEqualToString:@"0"]) {
                answer.textContent = answerData.content;
            }
            
            //回答之后返回刷新
            __weak typeof(self) weakSelf = self;
            answer.backBlock = ^(void) {
                [weakSelf getData];
            };
            
            //保存草稿后，提示特效
            answer.draftSaveBlock = ^(NSString *str) {
                NSLog(@"%@",str);
                [weakSelf addView];
            };
            
            [self.navigationController pushViewController:answer animated:YES];
        }
    }else{
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }
}



- (void)backRefresh{
    [self getData];
    [self.tableView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (void)dealloc{
    _timer = nil;
}

@end
