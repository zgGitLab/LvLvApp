//
//  AskTableViewCell.h
//  LvLvApp
//
//  Created by Sunny on 16/1/18.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AskTableViewCell : UITableViewCell
@property (nonatomic, strong) S_MYAskData *askData;
@end
