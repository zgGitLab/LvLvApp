//
//  OtherPerMainViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "OtherPerMainViewController.h"
#import "PerFansViewController.h"
#import "PerConcernViewController.h"
#import "PerCollectTableViewCell.h"
#import "EssentialnformationViewController.h"
#import "S_MyAnserTableViewCell.h"
#import "AnserViewController.h"
#import "AskViewController.h"
#import "MyTapGestrpueReco.h"
#import "GambitAnswerViewController.h"
#import "AnswerDetailViewController.h"
#import "ShareView.h"
#import "ChatViewController.h"
@interface OtherPerMainViewController ()
{
    UIImageView *vipImage;
    UIView *myTitlevi;
    UIImageView *titleImag;
    UIView *perHeaderView;
    
    UILabel *titleLab;      // 昵称
    UIImageView *sexImag;   // 性别图像
    UILabel *detailLab;     // 简介
    UIButton *isConcornBtn;      // 关注
     UIButton *privatemessageBtn;      // 私信
    UIButton *agreeBtn;     // 被赞同数
    UIButton *collectionBtn;// 被收藏数
    UIButton *shareBtn;     // 被分享数
    UILabel *anserLab;      // 回答数
    UILabel *concernLab;    // 关注数
    UILabel *fansLab;       // 粉丝数
    UILabel *askLab;        // 提问数
    UIButton *anserBtn;
    UIButton *concernBtn;
    UIButton *fansBtn;
    UIButton *askBtn;
    
    UILabel *jobLab;        // 公司名称
    UILabel *zhiWuLab;      // 职务
    UILabel *phoneLab;      // 联系方式
    UIButton *moreBtn;
    
    int page;
    NSMutableArray *dataArray;
    BOOL isScuess;
    CGFloat radius;
    
    S_MyMainPageBaseClass *_model;
}
@end

@implementation OtherPerMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    radius = 26.5;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
    [self otherCreatTitleImage];
    [self otherGetAllUi];
    self.otherPerTable.separatorInset = UIEdgeInsetsMake(0, 0, 0, 8);
    self.otherPerTable.tableFooterView = [[UIView alloc] init];
    __weak OtherPerMainViewController *VC = self;
    // 上拉加载
    self.otherPerTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 结束刷新
        [VC infinite];
    }];
//    self.otherPerTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        page = 1;
//        NSString *pageIndex = [NSString stringWithFormat:@"%d",page];
//        [VC requestPerHotWithIndex:pageIndex];
//    }];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    page = 1;
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.navigationBarHidden = YES;
    NSString *pageIndex = [NSString stringWithFormat:@"%d",page];
    [self requestPerHotWithIndex:pageIndex];
}

- (void)infinite
{
    page += 1;
    NSString *pageIndex = [NSString stringWithFormat:@"%d",page];
    [self requestPerHotWithIndex:pageIndex];
}


- (void)requestPerHotWithIndex:(NSString *)index
{
    __weak OtherPerMainViewController *VC = self;
    if ([index isEqualToString:@"1"]) {
        // 他人主页
        [RequestManager myMainPageWithUserID:self.userID andOtherID:self.otherID andIsMySelf:(BOOL)NO andBlock:^(S_MyMainPageBaseClass *mainPageModal,NSString *message) {
            _model = mainPageModal;
            [VC.otherPerTable.mj_header endRefreshing];
            [VC performSelectorOnMainThread:@selector(refreshData:) withObject:mainPageModal waitUntilDone:YES];
            
        }];
    }
    // 他人动态
    [RequestManager perMainPageHotWithUserID:self.otherID andPageIndex:index andPageSize:@"10" andBlock:^(S_PerHotBaseClass *perPageHotModal, NSString *message) {
        [VC.otherPerTable.mj_header endRefreshing];
        [VC.otherPerTable.mj_footer endRefreshing];
        if ([index isEqualToString:@"1"]) {
            [dataArray removeAllObjects];
            dataArray = [NSMutableArray arrayWithArray:perPageHotModal.data];
        }else{
            [dataArray addObjectsFromArray:perPageHotModal.data];
        }
        
        //        dataArray = [NSMutableArray arrayWithArray:perPageHotModal.data];
        [VC performSelectorOnMainThread:@selector(refreshHotData:) withObject:perPageHotModal waitUntilDone:YES];
    }];
    
}


- (void)refreshHotData:(S_PerHotBaseClass *)modal
{
    [self.otherPerTable reloadData];
}

- (void)refreshData:(S_MyMainPageBaseClass *)modal
{
    
    [titleImag sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,modal.image]]placeholderImage:[UIImage imageNamed:@"头像"]];
    titleImag.layer.cornerRadius = radius;
    titleImag.clipsToBounds = YES;
    titleLab.text = modal.nickName;      // 昵称
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:titleLab.font , NSFontAttributeName, nil];
    float titWith = [modal.nickName sizeWithAttributes:dictionary].width;
    titleLab.frame = CGRectMake((SIZE.width-titWith)/2.0-15, titleLab.frame.origin.y, titWith, titleLab.frame.size.height);
    sexImag.frame = CGRectMake(CGRectGetMaxX(titleLab.frame)+10, sexImag.frame.origin.y, 15, 15);
    sexImag.image = [modal.gender isEqualToString:@"男"]?[UIImage imageNamed:@"男"]:[UIImage imageNamed:@"女"];
    detailLab.text = (NSString *)modal.introduction;     // 简介
    [agreeBtn setTitle:[NSString stringWithFormat:@" 被赞同 %.0f",modal.responseCount] forState:UIControlStateNormal];     // 被赞同数
    [collectionBtn setTitle:[NSString stringWithFormat:@" 被收藏 %.0f",modal.collectCount] forState:UIControlStateNormal];// 被收藏数
    [shareBtn setTitle:[NSString stringWithFormat:@" 被分享 %.0f",modal.shareCount] forState:UIControlStateNormal] ;     // 被分享数
    anserLab.text = [NSString stringWithFormat:@"%.0f",modal.answerCount];      // 回答数
    concernLab.text = [NSString stringWithFormat:@"%.0f",modal.questionFocus];    // 关注数
    fansLab.text = [NSString stringWithFormat:@"%.0f",modal.fansCount];       // 粉丝数
    askLab.text = [NSString stringWithFormat:@"%.0f",modal.questionCount];        // 提问数
    NSString *imagName = modal.IsFocus?@"已关注2":@"+关注";
//    if (modal.IsFocus) {
//        isScuess = YES;
        isConcornBtn.selected = modal.IsFocus?YES:NO;
//    }
    [isConcornBtn setImage:[UIImage imageNamed:imagName] forState:UIControlStateNormal];
    jobLab.text = modal.company;        // 公司名称
    zhiWuLab.text = modal.position;      // 职务
    phoneLab.text = modal.telPhone;      // 联系方式
    
    if (modal.isV == 1) {
        if (!vipImage) {
            vipImage.image = [UIImage imageNamed:@"头像加v"];
        }
    }else{
        vipImage.image = nil;
    }
}

- (void)otherCreatTitleImage
{
    perHeaderView = [[NSBundle mainBundle] loadNibNamed:@"OtherPerMainHeaderView" owner:self options:nil][0];
    self.otherPerTable.tableHeaderView = perHeaderView;
    
    //自定义导航栏
    UIView *customNav = [[UIView alloc]initWithFrame:CGRectMake(0, 20, kWidth, 44)];
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 43.5, kWidth, 0.3)];
    line.backgroundColor = [UIColor lightGrayColor];
    [customNav addSubview:line];
    customNav.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:customNav];
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(kWidth-50, 15, 40, 15);
    [rightButton setImage:[UIImage imageNamed:@"分享点"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightBtn:) forControlEvents:UIControlEventTouchUpInside];
    [customNav addSubview:rightButton];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = CGRectMake(10,2, 22, 40);
    [leftButton setImage:[UIImage imageNamed:@"返回箭头"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [customNav addSubview:leftButton];
    
    CGFloat x= kWidth/2-26;
    CGFloat y=15;
    myTitlevi = [[UIView alloc] initWithFrame:CGRectMake(x, y, 53, 53)];
    titleImag = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 53, 53)];
    titleImag.image = [UIImage imageNamed:@"头像"];
    vipImage = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleImag.frame)-16, CGRectGetMaxY(titleImag.frame)-16, 17, 17)];
    [myTitlevi addSubview:titleImag];
    [myTitlevi addSubview:vipImage];
    //    self.navigationItem.titleView = myTitlevi;
    [customNav addSubview:myTitlevi];
    
//    myTitlevi = [[UIView alloc] initWithFrame:CGRectMake(0, 18, 53, 53)];
//    titleImag = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 53, 53)];
//    titleImag.image = [UIImage imageNamed:@"头像"];
//    vipImage = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleImag.frame)-16, CGRectGetMaxY(titleImag.frame)-16, 17, 17)];
//    vipImage.image = nil;
//    [myTitlevi addSubview:titleImag];
//    [myTitlevi addSubview:vipImage];
//    self.navigationItem.titleView = myTitlevi;
//
//    self.otherPerTable.tableHeaderView = perHeaderView;
//    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    rightButton.frame = CGRectMake(0, 0, 40, 15);
//    [rightButton setImage:[UIImage imageNamed:@"分享点"] forState:UIControlStateNormal];
//    [rightButton addTarget:self action:@selector(rightBtn:) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}


- (void)back:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 分享
- (void)rightBtn:(UIButton *)btn
{
    NSArray *imageArray = @[@"微信好友",@"微信朋友圈",@"微博",@"QQ",@"邮件",@"复制链接",@"",@""];
    NSArray *titleArray = @[@"微信好友",@"微信朋友圈",@"微博",@"QQ",@"邮件",@"复制链接",@"",@""];
    titleArray = [Utils isClientInstalled:titleArray];
    imageArray = [Utils isClientInstalled:imageArray];
    NSArray  *editArray=@[@"举报",@"",@"",@""];
    NSArray  *editArray1 = @[@"举报用户",@"",@"",@""];
    NSString *title = @"分享用户";
    NSString *shareTitle = titleLab.text;
    NSString *shareText = [NSString stringWithFormat:@"%@ 的律律首页,获得%.0f个赞同、%.0f次收藏 http://%@/mobile/OthersPagePhone?UserID=%@&OtherID=%@ (分享自@律律App)",titleLab.text,_model.responseCount,_model.collectCount,k_SHAREIP,self.userID,self.otherID];
    NSString *shareUrl = [NSString stringWithFormat:@"http://%@/mobile/OthersPagePhone?UserID=%@&OtherID=%@",k_SHAREIP,self.userID,self.otherID];
    UIImage *shareImage = [UIImage imageNamed:@"logos"];
    ShareView *share = [[ShareView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:self.userID forKey:@"UserID"];
    [dict setObject:self.otherID forKey:@"ID"];
    //举报类型 1问题，2回答，3用户
    [dict setObject:@"3" forKey:@"Category"];
    [dict setObject:titleLab.text forKey:@"Title"];
    
    share.dictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"imageArr1":imageArray,@"titleArr1":titleArray,@"imageArr2":editArray,@"titleArr2":editArray1,@"title":title,@"shareText":shareText,@"shareUrl":shareUrl,@"shareTitle":shareTitle,@"shareImage":shareImage,@"dict":dict}];
    
    
    NSString *shareText1 = [NSString stringWithFormat:@"%@ 的律律首页",titleLab.text];
    share.noteDictionary =  [NSMutableDictionary dictionaryWithDictionary:@{@"imageArr1":imageArray,@"titleArr1":titleArray,@"imageArr2":editArray,@"titleArr2":editArray1,@"title":title,@"shareText":shareText1,@"shareUrl":shareUrl,@"shareTitle":shareTitle,@"shareImage":shareImage,@"dict":dict}];
    share.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app.window addSubview:share];
}

- (void)otherGetAllUi
{
    //    UIView *self.perMainTab.tableHeaderView = self.perMainTab.tableHeaderView;
    
    titleLab = (UILabel *)[perHeaderView viewWithTag:10];
    sexImag = (UIImageView *)[perHeaderView viewWithTag:11];
    detailLab = (UILabel *)[perHeaderView viewWithTag:12];
    isConcornBtn = (UIButton *)[perHeaderView viewWithTag:13];
    agreeBtn = (UIButton *)[perHeaderView viewWithTag:14];
    collectionBtn = (UIButton *)[perHeaderView viewWithTag:15];
    shareBtn = (UIButton *)[perHeaderView viewWithTag:16];
    anserLab = (UILabel *)[perHeaderView viewWithTag:17];
    concernLab = (UILabel *)[perHeaderView viewWithTag:18];
    fansLab = (UILabel *)[perHeaderView viewWithTag:19];
    askLab = (UILabel *)[perHeaderView viewWithTag:20];
    [isConcornBtn addTarget:self action:@selector(concornBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    anserBtn = (UIButton *)[perHeaderView viewWithTag:21];
    concernBtn = (UIButton *)[perHeaderView viewWithTag:22];
    fansBtn = (UIButton *)[perHeaderView viewWithTag:23];
    askBtn = (UIButton *)[perHeaderView viewWithTag:24];
    [anserBtn addTarget:self action:@selector(perNEWS:) forControlEvents:UIControlEventTouchUpInside];
    [concernBtn addTarget:self action:@selector(perNEWS:) forControlEvents:UIControlEventTouchUpInside];
    [fansBtn addTarget:self action:@selector(perNEWS:) forControlEvents:UIControlEventTouchUpInside];
    [askBtn addTarget:self action:@selector(perNEWS:) forControlEvents:UIControlEventTouchUpInside];
    
    jobLab = (UILabel *)[perHeaderView viewWithTag:25];
    zhiWuLab = (UILabel *)[perHeaderView viewWithTag:26];
    phoneLab = (UILabel *)[perHeaderView viewWithTag:28];
    moreBtn = (UIButton *)[perHeaderView viewWithTag:29];
    privatemessageBtn = (UIButton *)[perHeaderView viewWithTag:100];
    [privatemessageBtn addTarget:self action:@selector(privatemessageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [moreBtn addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

#pragma  mark 回答，关注，粉丝，提问
- (void)perNEWS:(UIButton *)btn
{
    if (btn.tag == 22) {
        // 关注
        PerConcernViewController *concerVC = [[PerConcernViewController alloc] init];
        concerVC.userID = self.otherID;
        [self.navigationController pushViewController:concerVC animated:YES];
        
    }else if (btn.tag == 23){
        // 粉丝
        PerFansViewController *fansVC = [[PerFansViewController alloc] init];
        fansVC.userID = self.otherID;
        [self.navigationController pushViewController:fansVC animated:YES];
    }else if (btn.tag == 21){
        // 我的回答
        AnserViewController *anserVC = [[AnserViewController alloc] init];
        anserVC.titleStr = titleLab.text;
        anserVC.userID = self.otherID;
        [self.navigationController pushViewController:anserVC animated:YES];
    }else{
        AskViewController *askVC = [[AskViewController alloc] init];
        askVC.myTitle = titleLab.text;
        askVC.userID = self.otherID;
        [self.navigationController pushViewController:askVC animated:YES];
    }
}
#pragma  mark 私信
- (void)privatemessageBtnClick:(UIButton *)btn
{
    ChatViewController *chatVC = [[ChatViewController alloc] init];
    chatVC.userID = self.userID;
    chatVC.otherID = self.otherID;
    chatVC.titleStr = titleLab.text;
    chatVC.mySelfImage = self.mySelfImage;
    [self.navigationController pushViewController:chatVC animated:YES];
}
#pragma  mark 关注
- (void)concornBtn:(UIButton *)btn
{
//    if (isScuess) {
        if (btn.selected) {
            [RequestManager perConcernOther:self.userID andOtherID:self.otherID andIsConcern:@"0" andBlock:^( NSString *message) {
                if ([message isEqualToString:@"成功"]) {
                    [btn setImage:[UIImage imageNamed:@"+关注"] forState:UIControlStateNormal];
                    btn.selected = NO;
                }
            }];
        }else{
            [RequestManager perConcernOther:self.userID andOtherID:self.otherID andIsConcern:@"1" andBlock:^( NSString *message) {
                if ([message isEqualToString:@"成功"]) {
                    [btn setImage:[UIImage imageNamed:@"已关注2"] forState:UIControlStateNormal];
                    btn.selected = YES;
                }
                
            }];
            
        }
//    }
}

#pragma  mark 更多资料
- (void)moreBtnClick:(UIButton *)btn
{
    EssentialnformationViewController *BasVC = [[EssentialnformationViewController alloc] init];
    BasVC.userID = self.otherID;
    [self.navigationController pushViewController:BasVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return 2;
    return dataArray.count;
}
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"anser";
    S_MyAnserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[S_MyAnserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.nicLab.userInteractionEnabled = YES;
        cell.titleLab.userInteractionEnabled = YES;
        cell.contentLab.userInteractionEnabled = YES;
    }

    UIImageView *imag = (UIImageView *)[cell.contentView viewWithTag:1000];
    [imag removeFromSuperview];
    S_PerHotData *perData = dataArray[indexPath.row];
    if (perData.answerContent != nil) {
        perData.answerContent = [Utils replaceImageString:perData.answerContent];
    }
    
    MyTapGestrpueReco *titTapGes = [[MyTapGestrpueReco alloc] initWithTarget:self action:@selector(titClick:)];
    titTapGes.TID = perData.tID;
    titTapGes.UserID = (int)perData.userid;
    titTapGes.tUserID = perData.tUserID;
    titTapGes.AnserID = perData.answerID;
    [cell.titleLab addGestureRecognizer:titTapGes];
    MyTapGestrpueReco *conTapGes = [[MyTapGestrpueReco alloc] initWithTarget:self action:@selector(conClick:)];
    conTapGes.TID = perData.tID;;
    conTapGes.UserID = (int)perData.userid;
    conTapGes.aUserID = perData.aUserID;
    conTapGes.AnserID = perData.answerID;;
    [cell.contentLab addGestureRecognizer:conTapGes];
    
    cell.nicLab.text = perData.nickname;
    cell.anserTimeLab.text = perData.dataDescription;
    cell.titleLab.attributedText = [self attributedStringWithString:perData.title];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:11] , NSFontAttributeName, nil];
    CGFloat nicWith = [perData.nickname sizeWithAttributes:dictionary].width;
    NSDictionary *dictionary1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:11] , NSFontAttributeName, nil];
    CGFloat anserTimeWith = [perData.dataDescription sizeWithAttributes:dictionary1].width;
    cell.nicLab.frame = CGRectMake(10, 15, nicWith, 15);
    cell.anserTimeLab.frame = CGRectMake(nicWith + 20, 15, anserTimeWith, 15);
    
    if (perData.types == 1 || perData.types == 3) {// 关注和提出
        
        CGSize titleSize = [self getSize:cell.titleLab.attributedText andWith:SIZE.width - 20];
        titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
        cell.titleLab.frame = CGRectMake(10, 35, SIZE.width-20, titleSize.height);
        cell.contentLab.hidden = YES;
        cell.otherLab.hidden = YES;
        
        
    }else{                                          // 赞同和回答
        
        
        cell.contentLab.hidden = NO;
        cell.otherLab.hidden = NO;
        //        cell.titleLab.text = perData.title;
        cell.contentLab.attributedText = [self conAttributedStringWithString:perData.answerContent];
        cell.otherLab.text = [NSString stringWithFormat:@"赞同 %.0f · 评论 %.0f · 收藏 %.0f",perData.counts,perData.commentCount,perData.collectCount];
        if ([perData.logoUrl isEqualToString:@""]) {// 图片不存在
            CGSize titleSize = [self getSize:cell.titleLab.attributedText andWith:SIZE.width - 20];
            titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
            cell.titleLab.frame = CGRectMake(10, 35, SIZE.width-20, titleSize.height);
            
            
            CGSize conSize = [self getSize:cell.contentLab.attributedText andWith:SIZE.width - 20];
            conSize.height = conSize.height>ConHeight?ConHeight:conSize.height;
            cell.contentLab.frame = CGRectMake(10, CGRectGetMaxY(cell.titleLab.frame)+5, SIZE.width-20, conSize.height);
            if (conSize.height == 0) {
                cell.otherLab.frame  = CGRectMake(10, CGRectGetMaxY(cell.titleLab.frame)+5, SIZE.width-20, 15);
            }else{
                cell.otherLab.frame  = CGRectMake(10, CGRectGetMaxY(cell.contentLab.frame)+5, SIZE.width-20, 15);
            }
            
            
        }else{// 有图片
            
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(SIZE.width - 90, 30, 80, 80)];
            [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,perData.logoUrl]]];
            image.tag = 1000;
            [cell.contentView addSubview:image];
            CGSize titleSize = [self getSize:cell.titleLab.attributedText andWith:SIZE.width - 105];
            titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
            cell.titleLab.frame = CGRectMake(10, 35, SIZE.width-105, titleSize.height);
            CGSize conSize = [self getSize:cell.contentLab.attributedText andWith:SIZE.width - 110];
            conSize.height = conSize.height>ConHeight?ConHeight:conSize.height;
            cell.contentLab.frame = CGRectMake(10, CGRectGetMaxY(cell.titleLab.frame)+5, SIZE.width-110, conSize.height);
            if ([perData.answerContent isEqualToString:@""]) {
                cell.otherLab.frame = CGRectMake(10, CGRectGetMaxY(cell.titleLab.frame)+5, SIZE.width-20, 15);
            }else{
                cell.otherLab.frame = CGRectMake(10, CGRectGetMaxY(cell.contentLab.frame)+5, SIZE.width-20, 15);
            }
        }

        
    }
    return cell;
}
- (CGSize)getSize:(NSAttributedString *)attrStr andWith:(CGFloat)with
{
    
    return [attrStr boundingRectWithSize:CGSizeMake(with, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
}
// 标题；
- (NSAttributedString *)attributedStringWithString:(NSString *)str
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 2;// 字体的行间距
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],NSParagraphStyleAttributeName:paragraphStyle};
    return [[NSAttributedString alloc] initWithString:str attributes:attributes];
}
// 内容
- (NSAttributedString *)conAttributedStringWithString:(NSString *)conStr
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 2;// 字体的行间距
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14],NSParagraphStyleAttributeName:paragraphStyle};
    return [[NSAttributedString alloc] initWithString:conStr attributes:attributes];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    S_PerHotData *perData = dataArray[indexPath.row];
    if (perData.answerContent != nil) {
        perData.answerContent = [Utils replaceImageString:perData.answerContent];
    }
    if (perData.types == 1 || perData.types == 3) {// 关注和提出
        CGSize titleSize = [self getSize:[self attributedStringWithString:perData.title] andWith:SIZE.width - 20];
        titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
        return titleSize.height + 50;
        
        
    }else{// 赞同和回答
        
        CGFloat cellHeight;
        // 计算标题；
        if ([perData.logoUrl isEqualToString:@""]) {
            CGSize titleSize = [self getSize:[self attributedStringWithString:perData.title] andWith:SIZE.width - 20];
            titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
            CGRect titRec = CGRectMake(10, 35, SIZE.width-20, titleSize.height);
            CGSize conSize = [self getSize:[self attributedStringWithString:perData.answerContent] andWith:SIZE.width-20];
            conSize.height = conSize.height>ConHeight?ConHeight:conSize.height;
            CGRect conRec = CGRectMake(10, CGRectGetMaxY(titRec)+5, SIZE.width-20, conSize.height);
            CGRect otheRec;
            if (conSize.height == 0) {
                otheRec = CGRectMake(10, CGRectGetMaxY(titRec)+5, SIZE.width-20, 15);
            }else{
                otheRec  = CGRectMake(10, CGRectGetMaxY(conRec)+5, SIZE.width-20, 15);
            }
            
            cellHeight = CGRectGetMaxY(otheRec)+15;
            
        }else{
            CGSize titleSize = [self getSize:[self attributedStringWithString:perData.title] andWith:SIZE.width - 105];
            titleSize.height = titleSize.height>TitHeight?TitHeight:titleSize.height;
            CGSize conSize = [self getSize:[self attributedStringWithString:perData.answerContent] andWith:SIZE.width - 110];
            conSize.height = conSize.height>ConHeight?ConHeight:conSize.height;
            if ([perData.answerContent isEqualToString:@""]) {
                cellHeight = 125;
            }else{
                cellHeight = titleSize.height + 35+5+conSize.height + 5 + 15 + 15 > 125?titleSize.height + 35+5+conSize.height + 5 + 15 + 15:125;
            }
        }
        
        return cellHeight;
    }
}

// 点击标题
- (void)titClick:(MyTapGestrpueReco *)tapGes
{
    GambitAnswerViewController *GamBVC = [[GambitAnswerViewController alloc] init];
    GamBVC.TID = tapGes.TID;
    GamBVC.UserID = [NSString stringWithFormat:@"%@",tapGes.tUserID];
    GamBVC.Answer_ID = tapGes.AnserID;
    [self.navigationController pushViewController:GamBVC animated:YES];
}

// 点击正文
- (void)conClick:(MyTapGestrpueReco *)tapGes
{
    NSLog(@"点击正文");
    AnswerDetailViewController *anserKeyVC = [[AnswerDetailViewController alloc] init];
    anserKeyVC.UserID = [NSString stringWithFormat:@"%@",tapGes.aUserID];
    anserKeyVC.TID = tapGes.TID;
    anserKeyVC.answerID = tapGes.AnserID;
    
    [self.navigationController pushViewController:anserKeyVC animated:YES];
    
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat scal = scrollView.contentOffset.y;
    CGFloat heigh = 0;
    if (scal > 26) {
        scal = 26;
    }
    if (scal <= 0) {
        scal = 0;
        heigh = 0;
    }else{
        heigh = -3;
    }
    
    titleImag.frame = CGRectMake(26.5 - (53-scal)/2.0, heigh,53 - scal,53 - scal);
    titleImag.layer.cornerRadius = titleImag.frame.size.width/2.0;
    radius = titleImag.frame.size.width/2.0;
    titleImag.clipsToBounds = YES;
    vipImage.center = CGPointMake(titleImag.center.x+20-scal+1, titleImag.center.y+15-scal+1);
    vipImage.frame = CGRectMake(CGRectGetMaxX(titleImag.frame)-16, CGRectGetMaxY(titleImag.frame)-16, 17, 17);
}

@end
