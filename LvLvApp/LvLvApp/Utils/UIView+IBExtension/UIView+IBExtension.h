//
//  UIView+UIViewController.m
//  donglele
//
//  Created by 开发者 on 16/4/18.
//  Copyright © 2016年 imdeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UIView (IBExtension)

/// 边框颜色
@property (nonatomic, strong) IBInspectable UIColor *borderColor;
/// 边框宽度
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
/// 圆角半径
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

@end
