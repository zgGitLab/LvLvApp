//
//  CollectMianTableViewCell.h
//  LvLvApp
//
//  Created by hope on 16/1/7.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectMianTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *Collectitle;
@property (strong, nonatomic) IBOutlet UILabel *SellWholesaleUnits;
@end
