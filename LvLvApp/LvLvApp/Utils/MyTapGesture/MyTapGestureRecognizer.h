//
//  MyTapGestureRecognizer.h
//  LvLvApp
//
//  Created by jim on 15/12/8.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTapGestureRecognizer : UITapGestureRecognizer
@property (nonatomic,strong) UILabel *label;
@end
