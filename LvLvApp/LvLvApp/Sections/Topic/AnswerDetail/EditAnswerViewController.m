//
//  EditAnswerViewController.m
//  LvLvApp
//
//  Created by jim on 15/12/29.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "EditAnswerViewController.h"
#import "InsertTextAttAchment.h"
#import "NSAttributedString+EmojiExtension.h"
#import "TOCropViewController.h"
@interface EditAnswerViewController ()<UITextViewDelegate,UINavigationControllerDelegate,
    UIImagePickerControllerDelegate,
    PerPickViewDelegat>
{
    NSInteger _num;
    BOOL _hasChanged;
}
@property (nonatomic,strong) NSMutableArray *imageArray;
@property (nonatomic,copy) NSMutableString *ImgCode;
@property (nonatomic,assign) NSUInteger location;  //记录变化的起始位置
@property (nonatomic,assign) CGFloat lineSapce;    //行间距
@property (nonatomic,strong) NSMutableAttributedString * locationStr;

@end

@implementation EditAnswerViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _hasChanged = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    if ([Utils isBlankString:_content]) {
        _content = [NSMutableString stringWithString:@""];
    }
    _ImgCode = [NSMutableString string];
    self.imageArray = [NSMutableArray array];
    _num = 0;
    self.location = 0;
    self.lineSapce = 5.0;
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *backBtn =[UIButton buttonWithType:UIButtonTypeSystem];
    [backBtn setTitle:@"取消" forState:UIControlStateNormal];
    [backBtn setTitleColor:setColor(0, 91, 255) forState:UIControlStateNormal];
    backBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    backBtn.frame = CGRectMake(0, 0, 40, 40);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backBtn];

    self.navigationItem.leftBarButtonItem = back;
    UIButton *publishBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [publishBtn setTitleColor:setColor(0, 91, 255) forState:UIControlStateNormal];
    [publishBtn setTitle:@"发布" forState:UIControlStateNormal];
    publishBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [publishBtn addTarget:self action:@selector(publish) forControlEvents:UIControlEventTouchUpInside];
    publishBtn.frame = CGRectMake(0, 0, 40, 40);
    UIBarButtonItem *publish = [[UIBarButtonItem alloc]initWithCustomView:publishBtn];
    self.navigationItem.rightBarButtonItem = publish;
    
    [self createTextView];
    
    //增加监听,当键盘出现或者改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    //增加监听,当键盘退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma  mark --键盘事件
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    
    self.textView.frame = CGRectMake(10, 10, kWidth-20, kHeight-height-80);
}

- (void)keyboardWillHide:(NSNotification *)aNotification{
    //获取键盘高度
//    NSDictionary *userInfo = [aNotification userInfo];
//    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect keyboardRect = [aValue CGRectValue];
//    int height = keyboardRect.size.height;
    
    self.textView.frame = CGRectMake(10, 10, kWidth-20, kHeight-20);
}



- (void)createTextView{
    //自定义键盘上部的view
    UIView *keyboardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 36)];
    
    UIButton *key = [UIButton buttonWithType:UIButtonTypeSystem];
    key.frame = CGRectMake(kWidth-26-15, 7, 26, 26);
    [key setImage:[[UIImage imageNamed:@"收键盘"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [key addTarget:self action:@selector(collectKeyBoard:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:key];
    
    UIButton *pic = [UIButton buttonWithType:UIButtonTypeSystem];
    pic.frame = CGRectMake(key.frame.origin.x-15-26, 5, 26, 26);
    [pic setImage:[[UIImage imageNamed:@"图片"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [pic addTarget:self action:@selector(picClick:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:pic];
    
    keyboardView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
//    UIButton *at = [UIButton buttonWithType:UIButtonTypeSystem];
//    at.frame = CGRectMake(pic.frame.origin.x - 15 - 22, 8, 22, 22);
//    [at setImage: [[UIImage imageNamed:@"圈@"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
//    [at addTarget:self action:@selector(atClick:) forControlEvents:UIControlEventTouchUpInside];
//    [keyboardView addSubview:at];
    
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 10, kWidth-20, kHeight-400)];
    self.textView.inputAccessoryView = keyboardView;

    [self showTextView];
    self.textView.textColor = [UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
    self.textView.font = [UIFont systemFontOfSize:15];
    [self.textView becomeFirstResponder];
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
}

- (void)showTextView{
    NSMutableString *str = [[NSMutableString alloc]initWithFormat:@"%@",_content];
    //创建NSMutableAttributedString实例，并将content传入
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:str];
    //设置行距
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    [style setLineSpacing:5.0f];
    //根据给定长度与style设置attStr式样
    [attStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1],NSParagraphStyleAttributeName:style,NSFontAttributeName:[UIFont systemFontOfSize:15]} range:NSMakeRange(0, str.length)];
    NSInteger mm = 0;
    for (int i = 0; i < _imageList.count; i ++) {
        if (![_imageList[i] isEqualToString:@""]) {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,_imageList[i]]];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
            CGFloat rate = (kWidth-20)/image.size.width;
            image = [Utils thumbnailWithImageWithoutScale:image size:CGSizeMake(kWidth-20, rate*image.size.height)];
            InsertTextAttAchment *attatchment = [[InsertTextAttAchment alloc] init];
            attatchment.image = image;
            
            NSRange range = [str rangeOfString:@"<img src"];
            //length = 53
            NSUInteger length = [@"<img src=\"/upload/answer/2016042922080299889.jpg\"/>" length];
            [str deleteCharactersInRange:NSMakeRange(range.location, length) ];
            [attStr deleteCharactersInRange:NSMakeRange(range.location+mm, length)];
            [attStr insertAttributedString:[NSAttributedString attributedStringWithAttachment:attatchment] atIndex:range.location+mm];
            mm++;
        }
    }
    _textView.attributedText = attStr;
}



//修改回答
- (void)requestDataWithContent:(NSString *)content{

    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:kUserID forKey:@"UserID"];
    [parm setObject:self.answerID forKey:@"Answer_ID"];
    [parm setObject:_ImgCode forKey:@"ImgCode"];
    [parm setObject:content forKey:@"Content"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:KEditAnswerUrl,K_IP] params:parm success:^(id json) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        if ([dic[@"Code"] isEqualToNumber:@200]) {
            NSLog(@"修改成功");
            if (self.backBlock) {
                self.backBlock();
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSLog(@"失败");
        }
    } failure:^(NSError *error) {
        
    }];
}



//at
- (void)atClick:(UIButton *)button{
    NSMutableString *str = [[NSMutableString alloc]init];
    str = [NSMutableString stringWithFormat:@"%@@",self.textView.text];
    self.textView.text = str;
}

//collectKeyBoard
- (void)collectKeyBoard:(UIButton *)button{
    [self.textView resignFirstResponder];
}

//pic
- (void)picClick:(UIButton *)button{
    [self.view endEditing:YES];
    PerPickImageView *perPickVi = [[PerPickImageView alloc] initWithId:1];
    perPickVi.delegate = self;
    [perPickVi showInView];
}

//发布
- (void)publish{

    if ([self.textView.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请填写发布内容" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }else {
        _imageArray = [self.textView.textStorage getImages];
        //处理图片，将图片数组转换成base64字符串
        for (int i = 0; i < _imageArray.count; i++) {
            NSData *data = UIImageJPEGRepresentation(_imageArray[i], 1);
            NSString *encodedImageStr = [data base64EncodedStringWithOptions:0];
            [_ImgCode appendString:encodedImageStr];
            if ((i != _imageArray.count-1)||(_imageArray.count !=1)) {
                [_ImgCode appendString:@"|"];
            }
        }
        NSString *info =[self.textView.textStorage getPlainString];
        NSMutableString *str = [[NSMutableString alloc]initWithString:info];

        //把所有的0替换为11；
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"0.jpg\"/>"];

            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"r.jpg\"/>"]];

            }
        }
        
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"r.jpg\"/>"];

            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"%d.jpg\"/>",i]];
            }
        }
        
        [self requestDataWithContent:str];
        [self.textView resignFirstResponder];
    }
}


/**
 *  保存草稿
 *
 *  @param content 回答内容
 *  @param status  保存到草稿
 */
- (void)requestDataWithContents:(NSString *)content{
    
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:kUserID forKey:@"UserID"];
    [parm setObject:_ImgCode forKey:@"ImgCode"];
    [parm setObject:self.TID forKey:@"TID"];
    [parm setObject:content forKey:@"Content"];
    if (![Utils isBlankString:self.answerID]) {
        [parm setObject:self.answerID forKey:@"Answer_ID"];
    }else{
        [parm setObject:@"" forKey:@"Answer_ID"];
    }
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:kSaveDraft,K_IP] params:parm success:^(id json) {
        [self.navigationController popViewControllerAnimated:YES];
        if (self.backBlock) {
            self.backBlock();
        }
        
        if (self.draftSaveBlock) {
            self.draftSaveBlock(@"已存草稿");
        }
    } failure:^(NSError *error) {
        
    }];
}

//取消
- (void)back{
    if (_hasChanged == NO) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        if (![Utils isBlankString:self.textView.text]) {
            _imageArray = [self.textView.textStorage getImages];
            //处理图片，将图片数组转换成base64字符串
            for (int i = 0; i < _imageArray.count; i++) {
                NSData *data = UIImageJPEGRepresentation(_imageArray[i], 1);
                NSString *encodedImageStr = [data base64EncodedStringWithOptions:0];
                [_ImgCode appendString:encodedImageStr];
                if ((i != _imageArray.count-1)||(_imageArray.count !=1)) {
                    [_ImgCode appendString:@"|"];
                }
            }
            NSString *info =[self.textView.textStorage getPlainString];
            NSMutableString *str = [[NSMutableString alloc]initWithString:info];
            
            //把所有的0替换为11；
            for (int i = 0; i < str.length; i++) {
                NSRange range = [str rangeOfString:@"<img src=\"0.jpg\"/>"];
                
                if (range.location != NSNotFound) {
                    [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"r.jpg\"/>"]];
                    
                }
            }
            
            for (int i = 0; i < str.length; i++) {
                NSRange range = [str rangeOfString:@"<img src=\"r.jpg\"/>"];
                
                if (range.location != NSNotFound) {
                    [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"%d.jpg\"/>",i]];
                }
            }
            //保存草稿
            [self requestDataWithContents:str];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}




-(UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;   //返回的就是已经改变的图片
}



#pragma mark PerPickViewDelegat;
- (void)jumpOtherView:(UIImagePickerController *)object
{
    object.allowsEditing = NO;
    [self presentViewController:object animated:YES completion:nil];
}

- (void)selectImage:(id)icon
{
    UIImage *imag = (UIImage *)icon;
    //static NSInteger i = 0;
    
    CGFloat rate = (kWidth-20)/imag.size.width;
    imag = [Utils thumbnailWithImageWithoutScale:imag size:CGSizeMake(kWidth-20, imag.size.height *rate)];
    InsertTextAttAchment *texAtt = [[InsertTextAttAchment alloc] init];
    texAtt.image = imag;
    
    // texAtt.emojiStr = [NSString stringWithFormat:@"<img src=%@%ld.jpg%@/>",@"\"",i++,@"\""];
    [self.textView.textStorage insertAttributedString:[NSAttributedString attributedStringWithAttachment:texAtt]
                                              atIndex:self.textView.selectedRange.location];
    
    //Move selection location
    self.textView.selectedRange = NSMakeRange(self.textView.selectedRange.location + 1, self.textView.selectedRange.length);

    //设置字的设置
    [self setInitLocation];
    [self.textView becomeFirstResponder];
}


//文字发生改变
- (void)textViewDidChange:(UITextView *)textView{
    _hasChanged = YES;
}


//-(void)setStyle
//{
//    //每次后拼装
//    if (self.textView.textStorage.length<self.location) {
//        [self setInitLocation];
//        return;
//    }
//    NSString * str=[self.textView.text substringFromIndex:self.location];
//    
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.lineSpacing = self.lineSapce;// 字体的行间距
//    NSDictionary *attributes=nil;
//    attributes = @{
//                   NSFontAttributeName:[UIFont systemFontOfSize:15],
//                   NSForegroundColorAttributeName:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1],
//                   NSParagraphStyleAttributeName:paragraphStyle
//                   };
//    
//    NSAttributedString * appendStr=[[NSAttributedString alloc] initWithString:str attributes:attributes];
//    [self.locationStr appendAttributedString:appendStr];
//    self.textView.attributedText =self.locationStr;
//    
//    //重新设置位置
//    self.location=self.textView.textStorage.length;
//}

-(void)setInitLocation
{
//    self.locationStr=nil;
//    self.locationStr=[[NSMutableAttributedString alloc]initWithAttributedString:self.textView.attributedText];
    self.textView.font = [UIFont systemFontOfSize:15];
    self.textView.textColor = [UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
    //重新设置位置
//    self.location=self.textView.textStorage.length;
    
}


@end
