//
//  CommentTableViewCell.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLab;//内容
@property (strong, nonatomic) IBOutlet UILabel *CommeCommeAnswer;//时间
@property (strong, nonatomic) IBOutlet UILabel *CommeTilte;//标题

@property (strong, nonatomic) IBOutlet UILabel *CommeApproverCommeCollect;//赞同评论
@end
