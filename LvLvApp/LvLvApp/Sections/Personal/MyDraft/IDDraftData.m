//
//  IDDraftData.m
//
//  Created by hope  on 15/12/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IDDraftData.h"


NSString *const kIDDraftDataAnswerID = @"AnswerID";
NSString *const kIDDraftDataTitle = @"Title";
NSString *const kIDDraftDataContent = @"Content";
NSString *const kIDDraftDataTID = @"TID";


@interface IDDraftData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IDDraftData

@synthesize answerID = _answerID;
@synthesize title = _title;
@synthesize content = _content;
@synthesize tID = _tID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.answerID = [self objectOrNilForKey:kIDDraftDataAnswerID fromDictionary:dict];
            self.title = [self objectOrNilForKey:kIDDraftDataTitle fromDictionary:dict];
            self.content = [self objectOrNilForKey:kIDDraftDataContent fromDictionary:dict];
            self.tID = [self objectOrNilForKey:kIDDraftDataTID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.answerID forKey:kIDDraftDataAnswerID];
    [mutableDict setValue:self.title forKey:kIDDraftDataTitle];
    [mutableDict setValue:self.content forKey:kIDDraftDataContent];
    [mutableDict setValue:self.tID forKey:kIDDraftDataTID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.answerID = [aDecoder decodeObjectForKey:kIDDraftDataAnswerID];
    self.title = [aDecoder decodeObjectForKey:kIDDraftDataTitle];
    self.content = [aDecoder decodeObjectForKey:kIDDraftDataContent];
    self.tID = [aDecoder decodeObjectForKey:kIDDraftDataTID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_answerID forKey:kIDDraftDataAnswerID];
    [aCoder encodeObject:_title forKey:kIDDraftDataTitle];
    [aCoder encodeObject:_content forKey:kIDDraftDataContent];
    [aCoder encodeObject:_tID forKey:kIDDraftDataTID];
}

- (id)copyWithZone:(NSZone *)zone
{
    IDDraftData *copy = [[IDDraftData alloc] init];
    
    if (copy) {

        copy.answerID = [self.answerID copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
        copy.tID = [self.tID copyWithZone:zone];
    }
    
    return copy;
}


@end
