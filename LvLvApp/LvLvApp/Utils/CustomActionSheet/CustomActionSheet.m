//
//  CustomActionSheet.m
//  LvLvApp
//
//  Created by jim on 15/12/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "CustomActionSheet.h"

@implementation CustomActionSheet

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    //定制tableview
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kHeight-50*(self.dataSource.count+1), kWidth, 50*(self.dataSource.count+1)) style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self addSubview:self.tableView];
}

#pragma mark --代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count+1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kWidth, 50)];
    cell.textLabel.text = @"我恩呢积分卡数据反馈";
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    if (indexPath.row == self.dataSource.count) {
        cell.textLabel.text = @"取消";
        cell.textLabel.textColor = setColor(58, 134, 240);
    }else{
        cell.textLabel.text = self.dataSource[indexPath.row];
        cell.textLabel.textColor = setColor(70, 70, 70);
    }
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 49, kWidth, 1)];
    view.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
    [cell addSubview:view];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != self.dataSource.count) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.selected = NO;
        NSLog(@"%@",cell.textLabel.text);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];

        [self.dict setObject:cell.textLabel.text forKey:@"Reportcontent"];
        [manager POST:[NSString stringWithFormat:KReportUrl,K_IP] parameters:self.dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSMutableString *str = [[NSMutableString alloc]initWithData:operation.responseData encoding:NSUTF8StringEncoding];
            NSLog(@"str ===%@",str);
            [self removeFromSuperview];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"error:%@",error);
            [self removeFromSuperview];
        }];

    }else{
        NSLog(@"取消举报");
        [self removeFromSuperview];
    }
}

- (void)removeShareView:(UITapGestureRecognizer *)tap{
    if(CGRectContainsPoint(self.tableView.frame, [tap locationInView:self]))
    {
    
    } else{
        [self removeFromSuperview];
    };
    
}
@end
