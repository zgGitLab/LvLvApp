//
//  CaseSelectVerdicts.m
//
//  Created by IOS8  on 15/11/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "CaseSelectVerdicts.h"


NSString *const kCaseSelectVerdictsReason = @"reason";
NSString *const kCaseSelectVerdictsContent = @"content";
NSString *const kCaseSelectVerdictsCategory = @"category";
NSString *const kCaseSelectVerdictsId = @"id";
NSString *const kCaseSelectVerdictsCharacter = @"character";
NSString *const kCaseSelectVerdictsLevel = @"level";
NSString *const kCaseSelectVerdictsTitle = @"title";
NSString *const kCaseSelectVerdictsCourt = @"court";


@interface CaseSelectVerdicts ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CaseSelectVerdicts

@synthesize reason = _reason;
@synthesize content = _content;
@synthesize category = _category;
@synthesize selectVerdictsIdentifier = _selectVerdictsIdentifier;
@synthesize character = _character;
@synthesize level = _level;
@synthesize title = _title;
@synthesize court = _court;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.reason = [self objectOrNilForKey:kCaseSelectVerdictsReason fromDictionary:dict];
            self.content = [self objectOrNilForKey:kCaseSelectVerdictsContent fromDictionary:dict];
            self.category = [self objectOrNilForKey:kCaseSelectVerdictsCategory fromDictionary:dict];
            self.selectVerdictsIdentifier = [[self objectOrNilForKey:kCaseSelectVerdictsId fromDictionary:dict] doubleValue];
            self.character = [self objectOrNilForKey:kCaseSelectVerdictsCharacter fromDictionary:dict];
            self.level = [self objectOrNilForKey:kCaseSelectVerdictsLevel fromDictionary:dict];
            self.title = [self objectOrNilForKey:kCaseSelectVerdictsTitle fromDictionary:dict];
            self.court = [self objectOrNilForKey:kCaseSelectVerdictsCourt fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.reason forKey:kCaseSelectVerdictsReason];
    [mutableDict setValue:self.content forKey:kCaseSelectVerdictsContent];
    [mutableDict setValue:self.category forKey:kCaseSelectVerdictsCategory];
    [mutableDict setValue:[NSNumber numberWithDouble:self.selectVerdictsIdentifier] forKey:kCaseSelectVerdictsId];
    [mutableDict setValue:self.character forKey:kCaseSelectVerdictsCharacter];
    [mutableDict setValue:self.level forKey:kCaseSelectVerdictsLevel];
    [mutableDict setValue:self.title forKey:kCaseSelectVerdictsTitle];
    [mutableDict setValue:self.court forKey:kCaseSelectVerdictsCourt];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.reason = [aDecoder decodeObjectForKey:kCaseSelectVerdictsReason];
    self.content = [aDecoder decodeObjectForKey:kCaseSelectVerdictsContent];
    self.category = [aDecoder decodeObjectForKey:kCaseSelectVerdictsCategory];
    self.selectVerdictsIdentifier = [aDecoder decodeDoubleForKey:kCaseSelectVerdictsId];
    self.character = [aDecoder decodeObjectForKey:kCaseSelectVerdictsCharacter];
    self.level = [aDecoder decodeObjectForKey:kCaseSelectVerdictsLevel];
    self.title = [aDecoder decodeObjectForKey:kCaseSelectVerdictsTitle];
    self.court = [aDecoder decodeObjectForKey:kCaseSelectVerdictsCourt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_reason forKey:kCaseSelectVerdictsReason];
    [aCoder encodeObject:_content forKey:kCaseSelectVerdictsContent];
    [aCoder encodeObject:_category forKey:kCaseSelectVerdictsCategory];
    [aCoder encodeDouble:_selectVerdictsIdentifier forKey:kCaseSelectVerdictsId];
    [aCoder encodeObject:_character forKey:kCaseSelectVerdictsCharacter];
    [aCoder encodeObject:_level forKey:kCaseSelectVerdictsLevel];
    [aCoder encodeObject:_title forKey:kCaseSelectVerdictsTitle];
    [aCoder encodeObject:_court forKey:kCaseSelectVerdictsCourt];
}

- (id)copyWithZone:(NSZone *)zone
{
    CaseSelectVerdicts *copy = [[CaseSelectVerdicts alloc] init];
    
    if (copy) {

        copy.reason = [self.reason copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
        copy.category = [self.category copyWithZone:zone];
        copy.selectVerdictsIdentifier = self.selectVerdictsIdentifier;
        copy.character = [self.character copyWithZone:zone];
        copy.level = [self.level copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.court = [self.court copyWithZone:zone];
    }
    
    return copy;
}


@end
