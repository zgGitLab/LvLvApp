//
//  M_IssueData.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_IssueData.h"


NSString *const kM_IssueDataTID = @"TID";
NSString *const kM_IssueDataFocusCount = @"FocusCount";
NSString *const kM_IssueDataTitle = @"Title";
NSString *const kM_IssueDataTUserID = @"TUserID";
NSString *const kM_IssueDataAnswerCount = @"AnswerCount";


@interface M_IssueData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_IssueData

@synthesize tID = _tID;
@synthesize focusCount = _focusCount;
@synthesize title = _title;
@synthesize tUserID = _tUserID;
@synthesize answerCount = _answerCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tID = [self objectOrNilForKey:kM_IssueDataTID fromDictionary:dict];
            self.focusCount = [[self objectOrNilForKey:kM_IssueDataFocusCount fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kM_IssueDataTitle fromDictionary:dict];
            self.tUserID = [[self objectOrNilForKey:kM_IssueDataTUserID fromDictionary:dict] doubleValue];
            self.answerCount = [[self objectOrNilForKey:kM_IssueDataAnswerCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tID forKey:kM_IssueDataTID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.focusCount] forKey:kM_IssueDataFocusCount];
    [mutableDict setValue:self.title forKey:kM_IssueDataTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tUserID] forKey:kM_IssueDataTUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.answerCount] forKey:kM_IssueDataAnswerCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tID = [aDecoder decodeObjectForKey:kM_IssueDataTID];
    self.focusCount = [aDecoder decodeDoubleForKey:kM_IssueDataFocusCount];
    self.title = [aDecoder decodeObjectForKey:kM_IssueDataTitle];
    self.tUserID = [aDecoder decodeDoubleForKey:kM_IssueDataTUserID];
    self.answerCount = [aDecoder decodeDoubleForKey:kM_IssueDataAnswerCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tID forKey:kM_IssueDataTID];
    [aCoder encodeDouble:_focusCount forKey:kM_IssueDataFocusCount];
    [aCoder encodeObject:_title forKey:kM_IssueDataTitle];
    [aCoder encodeDouble:_tUserID forKey:kM_IssueDataTUserID];
    [aCoder encodeDouble:_answerCount forKey:kM_IssueDataAnswerCount];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_IssueData *copy = [[M_IssueData alloc] init];
    
    if (copy) {

        copy.tID = [self.tID copyWithZone:zone];
        copy.focusCount = self.focusCount;
        copy.title = [self.title copyWithZone:zone];
        copy.tUserID = self.tUserID;
        copy.answerCount = self.answerCount;
    }
    
    return copy;
}


@end
