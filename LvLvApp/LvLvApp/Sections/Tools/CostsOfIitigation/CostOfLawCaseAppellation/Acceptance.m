//
//  Acceptance.m
//  LvLvApp
//
//  Created by hope on 15/10/19.
//  Copyright (c) 2015年 gyy. All rights reserved.
//
#define OpenHeight  headerView.frame.size.height //展开的高度
#define CloseHeight  chooseView.frame.size.height-155//收起的高度
#import "ChooseViewtwo.h"
#import "Acceptance.h"


@implementation Acceptance

{
    UIScrollView *mainView;
    ChooseViewtwo *chooseView;
    UIView *headerView;//头部
    UIView *calcView; //计算view
    
    UILabel *typeOfAcceptance;//受理类型
    UITextField *typeOfAcceptanceText;
    UIButton *typeOfAcceptanceBtn;
    
    UILabel *inputMoney;//输入金额
    UITextField *inputMoneyText;//输入金额的
    
    UIButton *calcBtn;//计算
    UIButton *resetBtn;//重置
    
    UILabel *calcResult;//计算结果
    UILabel *calcResultLabel;
    UILabel *isHalf;//是否减半
    //UISwitch *switchs;
    NSInteger seclectIndex;//是否减半
    UIButton *noHalf;//不减半
    UIButton *yesHalf;//减半
    long SeclectIndex;
    
}
- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 99-64, [UIScreen mainScreen].bounds.size.width,  [UIScreen mainScreen].bounds.size.height-99);
        self.backgroundColor = [UIColor whiteColor];
        [self headerViewleiru];
        [self tableViewshu];
        [self foodViewjine];
        chooseView = [[ChooseViewtwo alloc]initWithFrame:CGRectMake(typeOfAcceptanceText.frame.origin.x+10, headerView.frame.size.height+5,typeOfAcceptanceText.frame.size.width, [_dataArr count] * 44+165)];
        chooseView.dataArray = _dataArr;
        chooseView.delegate = self;
        [self addSubview:chooseView];
    }
    return self;
}

- (id)initWithView:(UIScrollView *)scrollView{
    Acceptance *view = [self init];
    mainView = scrollView;
    mainView.backgroundColor = [UIColor whiteColor];
    return view;
}

- (void)tableViewshu
{
    _dataArr = [NSArray arrayWithObjects:@"财产案件:财产案件",@"非财产案件:离婚案件",@"非财产案件:侵害人格权案件",@"非财产案件:其他非财产案件",@"知识产权:知识产权案件",@"劳动争议:劳动争议案件",@"行政案件:商标、专利、海事行政案件",@"行政案件:其他行政案件",@"管辖权异议:管辖权异议案件",nil];
    //默认赋值
    //[typeOfAcceptanceText setText:[_dataArr objectAtIndex:0]];
    
}
- (void)chooseViewSelectInfo:(NSString *)info{
    [typeOfAcceptanceText setText:info];
    
    [UIView animateWithDuration:0.3 animations:^{
        calcView.frame =CGRectMake(10, headerView.frame.size.height, self.bounds.size.width-20, 150);}];
}
-(void)headerViewleiru
{
    headerView =[[UIView alloc]initWithFrame:CGRectMake(10, 10, self.bounds.size.width-20, 50)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    [headerView setAlpha:1];
    [self addSubview:headerView];
    
    typeOfAcceptance =[[UILabel alloc]initWithFrame:CGRectMake(6, 10, 75, 30)];
    [typeOfAcceptance setText:@"受理类型"];
    [typeOfAcceptance setFont:[UIFont systemFontOfSize:16.0]];
    [typeOfAcceptance setTextColor:[UIColor blackColor]];
    [headerView addSubview:typeOfAcceptance];
    
    typeOfAcceptanceText= [[UITextField alloc]initWithFrame:CGRectMake(typeOfAcceptance.frame.size.width+10, 10, headerView.bounds.size.width-100, 30)];
    [typeOfAcceptanceText setPlaceholder:@"请点击选择"];
    //[typeOfAcceptanceText setBackground:[UIImage imageNamed:@"重置2"]];
    typeOfAcceptanceText.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:242.0/255.0 blue:244.0/255.0 alpha:1];
    [typeOfAcceptanceText setTextAlignment:NSTextAlignmentCenter];
    [typeOfAcceptanceText setFont:[UIFont systemFontOfSize:16.0]];
    
    [headerView addSubview:typeOfAcceptanceText];
    
    typeOfAcceptanceBtn=[[UIButton alloc]initWithFrame:CGRectMake(typeOfAcceptance.frame.size.width+10, 10, headerView.bounds.size.width-100, 30)];
    [typeOfAcceptanceBtn setTintColor:[UIColor  blackColor]];
    [typeOfAcceptanceBtn addTarget:self action:@selector(typeOfAcceptanceBtnAction:) forControlEvents:UIControlEventTouchUpInside ];
    [headerView addSubview:typeOfAcceptanceBtn];
}
-(void)foodViewjine
{
    calcView =[[UIView alloc]initWithFrame:CGRectMake(10, headerView.frame.size.height+headerView.frame.origin.y-10, self.bounds.size.width-20, 200)];
    [calcView setBackgroundColor:[UIColor whiteColor]];
    [calcView setAlpha:1];
    [self addSubview:calcView];
    
    inputMoney =[[UILabel alloc]initWithFrame:CGRectMake(6, 5, 75, 30)];
    [inputMoney setText:@"输入金额"];
    [inputMoney setFont:[UIFont systemFontOfSize:16.0]];
    [inputMoney setTextColor:[UIColor blackColor]];
    [calcView addSubview:inputMoney];
    
    inputMoneyText= [[UITextField alloc]initWithFrame:CGRectMake(typeOfAcceptance.frame.size.width+10, 5,headerView.bounds.size.width-100, 30)];
    [inputMoneyText setPlaceholder:@"请填写诉讼费金额"];
    //inputMoneyText.background=[UIImage imageNamed:@"重置2"];
    inputMoneyText .backgroundColor = [UIColor colorWithRed:241.0/255.0 green:242.0/255.0 blue:244.0/255.0 alpha:1];
    [inputMoneyText setTextAlignment:NSTextAlignmentCenter];
    inputMoneyText .keyboardType=UIKeyboardTypeDecimalPad;
    [inputMoneyText setFont:[UIFont systemFontOfSize:16.0]];
    inputMoneyText.delegate =self;
    
    [calcView addSubview:inputMoneyText];
    
    isHalf =[[UILabel alloc]initWithFrame:CGRectMake(6, inputMoney.frame.origin.y+inputMoney.bounds.size.height+15, 100, 30)];
    [isHalf setText:@"是否减半:"];
    [isHalf   setFont:[UIFont systemFontOfSize:16.0]];
    [isHalf setTextColor:[UIColor blackColor]];
    [calcView addSubview:isHalf];
    
    noHalf=[[UIButton alloc]initWithFrame:CGRectMake(inputMoneyText.frame.origin.x, inputMoney.frame.origin.y+inputMoney.bounds.size.height+15, 60, 30)];
    [noHalf setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noHalf setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noHalf setBackgroundImage:[UIImage imageNamed:@"关"] forState:UIControlStateNormal];
    noHalf.tag =0;
    [noHalf addTarget:self action:@selector(onisHalfAction:) forControlEvents:UIControlEventTouchUpInside];
    [calcView addSubview:noHalf];
    
    yesHalf=[[UIButton alloc]initWithFrame:CGRectMake(inputMoneyText.frame.origin.x, inputMoney.frame.origin.y+inputMoney.bounds.size.height+15, 60, 30)];
    [yesHalf setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    yesHalf.tag=1;
    [yesHalf setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [yesHalf addTarget:self action:@selector(onisHalfAction:) forControlEvents:UIControlEventTouchUpInside ];
    [calcView addSubview:yesHalf];
    
    calcBtn=[[UIButton alloc]initWithFrame:CGRectMake(typeOfAcceptance.frame.size.width-60, 100,calcView.frame.size.width/2-20, 30)];
    [calcBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [calcBtn setTitle:@"计算" forState:UIControlStateNormal];
    
    [calcBtn  setBackgroundImage:[UIImage imageNamed:@"计算1"] forState: UIControlStateNormal];
    [calcBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    calcBtn.titleLabel.font=[UIFont systemFontOfSize:18.0];
    [calcBtn addTarget:self action:@selector(jisuanAction:) forControlEvents:UIControlEventTouchUpInside];
    [calcView addSubview:calcBtn];
    
    resetBtn=[[UIButton alloc]initWithFrame:CGRectMake(calcView.bounds.size.width/2+10, 100, calcView.frame.size.width/2-20, 30)];
    [resetBtn setTintColor:[UIColor  whiteColor]];
    [resetBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [resetBtn setTitle:@"重置" forState:UIControlStateNormal];
    [resetBtn setBackgroundImage:[UIImage imageNamed:@"重置2"] forState:UIControlStateNormal ];
    resetBtn.titleLabel.font=[UIFont systemFontOfSize:18.0];
    [resetBtn addTarget:self action:@selector(resetAction:) forControlEvents:UIControlEventTouchUpInside ];
    [calcView addSubview:resetBtn];
    
    calcResult =[[UILabel alloc]initWithFrame:CGRectMake(6, 150, 80, 30)];
    [calcResult setText:@"计算结果:"];
    [calcResult setFont:[UIFont systemFontOfSize:16.0f]];
    [calcResult setTextColor:[UIColor blackColor]];
    [calcView addSubview:calcResult];
    
    calcResultLabel =[[UILabel alloc]initWithFrame:CGRectMake(calcResult.frame.size.width, 150, 300, 30)];
//    [calcResultLabel setText:@"0.00"];
    [calcResultLabel setFont:[UIFont systemFontOfSize:16.0f]];
    [calcResultLabel setTextColor:[UIColor redColor]];
    
    [calcView addSubview:calcResultLabel];
     calcResultLabel.text =[self stringToDouble:@"0.00"];
    
}

- (NSString *)stringToAcross:(NSString *)str{
    NSArray *arr =[str componentsSeparatedByString:@"-"];
    NSMutableArray *mutbaleArr =[NSMutableArray array];
    for (NSString *tempStr in arr) {
        NSMutableString *mubtaleStr =[tempStr mutableCopy];
        NSUInteger i =mubtaleStr.length;
        NSUInteger j = 0;
        while (i >6) {
            [mubtaleStr insertString:@"," atIndex:(mubtaleStr.length -6)-4 *j];
            j++;
            i-=3;
        }
        [mutbaleArr addObject:mubtaleStr];
    }
    
    return [mutbaleArr componentsJoinedByString:@"-"];
}

- (NSString *)stringToDouble:(NSString *)str
{
    NSArray *arr= [str componentsSeparatedByString:@"."];
    NSMutableString *tempStr = [NSMutableString stringWithString:arr[0]];
    long count1 = tempStr.length%3 != 0?tempStr.length/3:tempStr.length/3-1;
    for (int i = 0; i < count1; i++) {
        [tempStr insertString:@"," atIndex:tempStr.length-(3*(i+1)+i)];
    }
    
    return [NSString stringWithFormat:@"%@%@%@",tempStr,@".",arr[1]];;
}

- (void)onisHalfAction:(UIButton*)btttn
{
    NSLog(@"%d",btttn.selected);
    btttn.selected = !btttn.selected;
    NSString *str = btttn.selected?@"开":@"关";
    
    [btttn setBackgroundImage:[UIImage imageNamed:str] forState:UIControlStateNormal];
    NSString *type = [typeOfAcceptanceText text];
    NSString *amt = [inputMoneyText text];
    if ([self isEmptyString:amt]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入讼诉费用" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if ( btttn.selected ==NO) {
        if       ([type isEqualToString:@"财产案件:财产案件"])
        {
//            [calcResultLabel setText:[self caiChanAnJian:amt]];
            calcResultLabel.text =[self stringToDouble:[self caiChanAnJian:amt]];
        }else if ([type isEqualToString:@"非财产案件:离婚案件"])
        {
//            [calcResultLabel setText:[self liHunSuanFa:amt]];
            calcResultLabel.text =[self stringToAcross:[self liHunSuanFa:amt]];
        }else if ([type isEqualToString:@"非财产案件:侵害人格权案件"])
        {
//            [calcResultLabel setText:[self feicaichanJHRQ:amt]];
            calcResultLabel.text =[self stringToAcross:[self feicaichanJHRQ:amt]];
            
        }else if ([type isEqualToString:@"非财产案件:其他非财产案件"])
        {
//            [calcResultLabel setText:[self qitacaiChan:amt]];
            calcResultLabel.text =[self stringToDouble:[self qitacaiChan:amt]];
        }else if ([type isEqualToString:@"知识产权:知识产权案件"])
        {
//            [calcResultLabel setText:[self knowleDge:amt]];
            calcResultLabel.text =[self stringToAcross:[self knowleDge:amt]];
        }else if ([type isEqualToString:@"劳动争议:劳动争议案件"])
        {
//            [calcResultLabel setText:[self labourLaw:amt]];
            calcResultLabel.text =[self stringToDouble:[self labourLaw:amt]];
        }else if ([type isEqualToString:@"行政案件:商标、专利、海事行政案件"])
        {
//            [calcResultLabel setText:[self trademarkPatcnt:amt]];
            calcResultLabel.text =[self stringToDouble:[self trademarkPatcnt:amt]];
        }else if ([type isEqualToString:@"行政案件:其他行政案件"])
        {
//            [calcResultLabel setText:[self elseAnYou:amt]];
            calcResultLabel.text =[self stringToDouble:[self elseAnYou:amt]];
        }else
        {
//            [calcResultLabel setText:[self adminsterQ:amt]];
            calcResultLabel.text =[self stringToDouble:[self adminsterQ:amt]];
        }
        
        
    }else
    {
        NSString *type = [typeOfAcceptanceText text];
        NSString *arm1 = [inputMoneyText text];
        NSString *arm =[NSString stringWithFormat:@"%.2f",[arm1 doubleValue]];
        if ([self isEmptyString:arm]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入讼诉费用" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;}
        if       ([type isEqualToString:@"财产案件:财产案件"])
        {
//            [calcResultLabel setText:[self caiChanAnJians:arm]];
            calcResultLabel.text =[self stringToDouble:[self caiChanAnJians:arm]];
        }else if ([type isEqualToString:@"非财产案件:离婚案件"])
        {
//            [calcResultLabel setText:[self liHunSuanFas:arm]];
            calcResultLabel.text =[self stringToAcross:[self liHunSuanFas:arm]];
        }else if ([type isEqualToString:@"非财产案件:侵害人格权案件"])
        {
//            [calcResultLabel setText:[self feicaichanJHRQs:arm]];
            calcResultLabel.text =[self stringToAcross:[self feicaichanJHRQs:arm]];
            
        }else if ([type isEqualToString:@"非财产案件:其他非财产案件"])
        {
            [calcResultLabel setText:[self qitacaiChans:arm]];
//            calcResultLabel.text =[self stringToDouble:[self qitacaiChans:arm]];
        }else if ([type isEqualToString:@"知识产权:知识产权案件"])
        {
//            [calcResultLabel setText:[self knowleDges:arm]];
            calcResultLabel.text =[self stringToAcross:[self knowleDges:arm]];
        }else if ([type isEqualToString:@"劳动争议:劳动争议案件"])
        {
//            [calcResultLabel setText:[self labourLaws:arm]];
            calcResultLabel.text =[self stringToDouble:[self labourLaws:arm]];
        
        }else if ([type isEqualToString:@"行政案件:商标、专利、海事行政案件"])
        {
//            [calcResultLabel setText:[self trademarkPatcnts:arm]];
            calcResultLabel.text =[self stringToDouble:[self trademarkPatcnts:arm]];
        }else if ([type isEqualToString:@"行政案件:其他行政案件"])
        {
//            [calcResultLabel setText:[self elseAnYous:arm]];
            calcResultLabel.text =[self stringToDouble:[self elseAnYous:arm]];
        }else
        {
//            [calcResultLabel setText:[self adminsterQs:arm]];
            calcResultLabel.text =[self stringToDouble:[self adminsterQs:arm]];
        }
    }
}

//判断字符串是否为空
- (BOOL)isEmptyString:(NSString *)string{
    if (string == nil) {
        return YES;
    }
    if (string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}
//财产案件计算方法
- (NSString *)caiChanAnJians:(NSString *)amt
{
    NSString *payAmt;
    if ([amt doubleValue] >= 0 && [amt doubleValue] <= 10000) {
        payAmt = @"25.00";//如果输入的金额大于0 不超过1万，交纳费50元
    }else if([amt doubleValue] > 10000 && [amt doubleValue]<=100000){
        payAmt = [NSString stringWithFormat:@"%.2f",(([amt doubleValue] * 0.025) - 200)*0.5];//如果输入的金额大于1万 小于等于10万
//        [self addIdentifierWithStr:modal.ArticlesCount]]
    }else if([amt doubleValue] > 100000 && [amt doubleValue]<=200000){
        payAmt = [NSString stringWithFormat:@"%.2f",(([amt doubleValue] * 0.02) + 300)*0.5];//如果输入的金额大于10万 小于等于20万
    }else if([amt doubleValue] > 200000 && [amt doubleValue]<=500000){
        payAmt = [NSString stringWithFormat:@"%.2f",(([amt doubleValue] * 0.015) + 1300)*0.5];//如果输入的金额大于20万 小于等于50万
    }else if([amt doubleValue] > 500000 && [amt doubleValue]<=1000000){
        payAmt = [NSString stringWithFormat:@"%.2f",(([amt doubleValue] * 0.01) + 3800)*0.5];//如果输入的金额大于50万 小于等于100万
    }else if([amt doubleValue] > 1000000 && [amt doubleValue]<=2000000){
        payAmt = [NSString stringWithFormat:@"%.2f",(([amt doubleValue] * 0.009) + 4800)*0.5];//如果输入的金额大于100万 小于等于200万
    }else if([amt doubleValue] > 2000000 && [amt doubleValue]<=5000000){
        payAmt = [NSString stringWithFormat:@"%.2f",(([amt doubleValue] * 0.008) + 6800)*0.5];//如果输入的金额大于200万 小于等于500万
    }else if([amt doubleValue] > 5000000 && [amt doubleValue]<=10000000){
        payAmt = [NSString stringWithFormat:@"%.2f",(([amt doubleValue] * 0.007) + 11800)*0.5];//如果输入的金额大于500万 小于等于1000万
    }else if([amt doubleValue] > 10000000 && [amt doubleValue]<=20000000){
        payAmt = [NSString stringWithFormat:@"%.2f",(([amt doubleValue] * 0.006) + 21800)*0.5];//如果输入的金额大于1000万 小于等于2000万
    }else{
        payAmt = [NSString stringWithFormat:@"%.2f",(([amt doubleValue] * 0.005) + 41800)*0.5];//超过2000万以上的
    }
    return payAmt;
}

//离婚案件算法
- (NSString *)liHunSuanFas:(NSString *)amt
{
    if ([amt doubleValue]>=0 && [amt doubleValue]<=200000) {
        return  @"25.00-150.00";
    }else
    {
        return [NSString stringWithFormat:@"%.2f-%.2f",(50+([amt doubleValue]-200000)*0.005)*0.05,(300+([amt doubleValue]-200000)*0.005)*0.5];
    }
}
//非财产案件:侵害人格权案件
- (NSString *)feicaichanJHRQs:(NSString *)amt
{
    if ([amt doubleValue]<=50000.00) {
        return @"50.00-250.00";
    }else if([amt doubleValue]>50000.00 &&[amt doubleValue]<=100000.00)
    {
        return [NSString stringWithFormat:@"%.2f-%.2f",([amt doubleValue]*0.01-400)*0.5,([amt doubleValue]*0.01)*0.5];
    }else
    {
        return [NSString stringWithFormat:@"%.2f-%.2f",([amt doubleValue]*0.005+100)*0.5,([amt doubleValue]*0.05+500)*0.5];
    }
    
}
// 非财产案件，其他财产案件
- (NSString *)qitacaiChans:(NSString *)amt
{
    
    return @"25.00-50.00";
}
//知识产权案件
- (NSString *)knowleDges:(NSString *)amt
{
    if ([amt doubleValue] == 0 ) {
        return @"250.00-500.00";
    }else if ([amt doubleValue] > 0 && [amt doubleValue]<=10000)
    {
        return @"25.00";
    }else if ([amt doubleValue] > 10000 && [amt doubleValue]<=100000)
    {
        return [NSString stringWithFormat:@"%.2f",([amt doubleValue]* 0.025-200)*0.5];
    }else if ([amt doubleValue] > 100000 && [amt doubleValue]<=200000)
    {
        return [NSString stringWithFormat:@"%.2f",([amt doubleValue]* 0.02+300)*0.5];
    }else if ([amt doubleValue] > 200000 && [amt doubleValue]<=500000)
    {
        return[NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.015 + 1300)*0.5];
    }else if ([amt doubleValue] > 500000 && [amt doubleValue]<=1000000)
    {
        return [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.01 + 3800)*0.5];
    }else if ([amt doubleValue] > 1000000 && [amt doubleValue]<=2000000)
    {
        return [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.009 + 4800)*0.5];
    }else if ([amt doubleValue] > 2000000 && [amt doubleValue]<=5000000)
    {
        return [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.008 + 6800)*0.5];
    }else if ([amt doubleValue] > 5000000 && [amt doubleValue]<=10000000)
    {
        return [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.007 + 11800)*0.5];
        
    }else if ([amt doubleValue] > 10000000 && [amt doubleValue]<=20000000)
    {
        return [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.006 + 21800)*0.5];
    }else
    {
        return [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.005+ 41800)*0.5];
    }
}
//劳动法
- (NSString *)labourLaws:(NSString *)amt
{
    return @"5.00";
}
//商标专利
- (NSString *)trademarkPatcnts:(NSString *)amt
{
    return @"50.00";
    
}
//其他案由
- (NSString *)elseAnYous:(NSString *)amt
{
    return @"25.00";
}
//管辖权异议
- (NSString *)adminsterQs:(NSString *)amt
{
    return @"25.00-50.00";
}

//财产案件计算方法
- (NSString *)caiChanAnJian:(NSString *)amt
{
    NSString *payAmt;
    if ([amt doubleValue] >= 0 && [amt doubleValue] <= 10000) {
        payAmt = @"50.00";//如果输入的金额大于0 不超过1万，交纳费50元
    }else if([amt doubleValue] > 10000 && [amt doubleValue]<=100000){
        payAmt = [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.025) - 200];//如果输入的金额大于1万 小于等于10万
    }else if([amt doubleValue] > 100000 && [amt doubleValue]<=200000){
        payAmt = [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.02) + 300];//如果输入的金额大于10万 小于等于20万
    }else if([amt doubleValue] > 200000 && [amt doubleValue]<=500000){
        payAmt = [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.015) + 1300];//如果输入的金额大于20万 小于等于50万
    }else if([amt doubleValue] > 500000 && [amt doubleValue]<=1000000){
        payAmt = [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.01) + 3800];//如果输入的金额大于50万 小于等于100万
    }else if([amt doubleValue] > 1000000 && [amt doubleValue]<=2000000){
        payAmt = [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.009) + 4800];//如果输入的金额大于100万 小于等于200万
    }else if([amt doubleValue] > 2000000 && [amt doubleValue]<=5000000){
        payAmt = [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.008) + 6800];//如果输入的金额大于200万 小于等于500万
    }else if([amt doubleValue] > 5000000 && [amt doubleValue]<=10000000){
        payAmt = [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.007) + 11800];//如果输入的金额大于500万 小于等于1000万
    }else if([amt doubleValue] > 10000000 && [amt doubleValue]<=20000000){
        payAmt = [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.006) + 21800];//如果输入的金额大于1000万 小于等于2000万
    }else{
        payAmt = [NSString stringWithFormat:@"%.2f",([amt doubleValue] * 0.005) + 41800];//超过2000万以上的
    }
    return payAmt;
}

//离婚案件算法
- (NSString *)liHunSuanFa:(NSString *)amt
{
    if ([amt doubleValue]>=0 && [amt doubleValue]<=200000) {
        return  @"50.00-300.00";
    }else
    {
        return [NSString stringWithFormat:@"%.2f-%.2f",50+([amt doubleValue]-200000)*0.005,300+([amt doubleValue]-200000)*0.005];
    }
}
//非财产案件:侵害人格权案件
- (NSString *)feicaichanJHRQ:(NSString *)amt
{
    if ([amt doubleValue]<=50000.00) {
        return @"100.00-500.00";
    }else if([amt doubleValue]>50000.00 &&[amt doubleValue]<=100000.00)
    {
        return [NSString stringWithFormat:@"%.2f-%.2f",[amt doubleValue]*0.01-400,[amt doubleValue]*0.01];
    }else
    {
        return [NSString stringWithFormat:@"%.2f-%.2f",[amt doubleValue]*0.005+100,[amt doubleValue]*0.05+500];
    }
    
}
// 非财产案件，其他财产案件
- (NSString *)qitacaiChan:(NSString *)amt
{
    return @"50.00-100.00";
}
//知识产权案件
- (NSString *)knowleDge:(NSString *)amt
{
    if ([amt doubleValue] == 0 ) {
        return @"500.00-1000.00";
    }else if ([amt doubleValue] > 0 && [amt doubleValue]<=10000)
    {
        return @"50.00";
    }else if ([amt doubleValue] > 10000 && [amt doubleValue]<=100000)
    {
        return [NSString stringWithFormat:@"%.2f",[amt doubleValue]* 0.025-200];
    }else if ([amt doubleValue] > 100000 && [amt doubleValue]<=200000)
    {
        return [NSString stringWithFormat:@"%.2f",[amt doubleValue]* 0.02+300];
    }else if ([amt doubleValue] > 200000 && [amt doubleValue]<=500000)
    {
        return[NSString stringWithFormat:@"%.2f",[amt doubleValue] * 0.015 + 1300];
    }else if ([amt doubleValue] > 500000 && [amt doubleValue]<=1000000)
    {
        return [NSString stringWithFormat:@"%.2f",[amt doubleValue] * 0.01 + 3800];
    }else if ([amt doubleValue] > 1000000 && [amt doubleValue]<=2000000)
    {
        return [NSString stringWithFormat:@"%.2f",[amt doubleValue] * 0.009 + 4800];
    }else if ([amt doubleValue] > 2000000 && [amt doubleValue]<=5000000)
    {
        return [NSString stringWithFormat:@"%.2f",[amt doubleValue] * 0.008 + 6800];
    }else if ([amt doubleValue] > 5000000 && [amt doubleValue]<=10000000)
    {
        return [NSString stringWithFormat:@"%.2f",[amt doubleValue] * 0.007 + 11800];
        
    }else if ([amt doubleValue] > 10000000 && [amt doubleValue]<=20000000)
    {
        return [NSString stringWithFormat:@"%.2f",[amt doubleValue] * 0.006 + 21800];
    }else
    {
        return [NSString stringWithFormat:@"%.2f",[amt doubleValue] * 0.005+ 41800];
    }
}
//劳动法
- (NSString *)labourLaw:(NSString *)amt
{
    return @"10.00";
}
//商标专利
- (NSString *)trademarkPatcnt:(NSString *)amt
{
    return @"100.00";
    
}
//其他案由
- (NSString *)elseAnYou:(NSString *)amt
{
    return @"50.00";
}
//管辖权异议
- (NSString *)adminsterQ:(NSString *)amt
{
    return @"50.00-100.00";
}
#pragma mark - 计算按钮事件
- (void)jisuanAction:(UIButton *)but
{
    
    [self endEditing:YES];
    [headerView resignFirstResponder];
//    self.countLable.text = [self stringToDouble:countStr];
    NSString *type = [typeOfAcceptanceText text];
    NSString *amt = [inputMoneyText text];
    if ([self isEmptyString:amt]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入讼诉费用" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if ( yesHalf.selected ==NO) {
        if       ([type isEqualToString:@"财产案件:财产案件"])
        {
//            [calcResultLabel setText:[self caiChanAnJian:amt]];
            calcResultLabel.text =[self stringToDouble:[self caiChanAnJian:amt]];
        }else if ([type isEqualToString:@"非财产案件:离婚案件"])
        {
//            [calcResultLabel setText:[self liHunSuanFa:amt]];
            calcResultLabel.text =[self stringToAcross:[self liHunSuanFa:amt]];
//            calcResultLabel.text =[self stringToAcross:[self liHunSuanFa:amt]];
        }else if ([type isEqualToString:@"非财产案件:侵害人格权案件"])
        {
//            [calcResultLabel setText:[self feicaichanJHRQ:amt]];
            calcResultLabel.text =[self stringToAcross:[self feicaichanJHRQ:amt]];
//            calcResultLabel.text =[self stringToDouble:[self feicaichanJHRQ:amt]];
        }else if ([type isEqualToString:@"非财产案件:其他非财产案件"])
        {
            [calcResultLabel setText:[self qitacaiChan:amt]];
//            calcResultLabel.text =[self stringToDouble:[self qitacaiChan:amt]];
        }else if ([type isEqualToString:@"知识产权:知识产权案件"])
        {
//            [calcResultLabel setText:[self knowleDge:amt]];
//            calcResultLabel.text =[self stringToDouble:[self knowleDge:amt]];
            calcResultLabel.text =[self stringToAcross:[self knowleDge:amt]];
        }else if ([type isEqualToString:@"劳动争议:劳动争议案件"])
        {
            [calcResultLabel setText:[self labourLaw:amt]];
//            calcResultLabel.text =[self stringToDouble:[self labourLaw:amt]];
        }else if ([type isEqualToString:@"行政案件:商标、专利、海事行政案件"])
        {
//            [calcResultLabel setText:[self trademarkPatcnt:amt]];
            calcResultLabel.text =[self stringToAcross:[self trademarkPatcnt:amt]];
//            calcResultLabel.text =[self stringToDouble:[self trademarkPatcnt:amt]];
        }else if ([type isEqualToString:@"行政案件:其他行政案件"])
        {
            [calcResultLabel setText:[self elseAnYou:amt]];
        }else
        {
            [calcResultLabel setText:[self adminsterQ:amt]];
        }
        
        
    }else
    {
        NSString *type = [typeOfAcceptanceText text];
        NSString *arm1 = [inputMoneyText text];
        NSString *arm =[NSString stringWithFormat:@"%.2f",[arm1 doubleValue]];
        if ([self isEmptyString:arm]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入讼诉费用" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;}
        if       ([type isEqualToString:@"财产案件:财产案件"])
        {
//            [calcResultLabel setText:[self caiChanAnJians:arm]];
            calcResultLabel.text =[self stringToDouble:[self caiChanAnJians:amt]];

        }else if ([type isEqualToString:@"非财产案件:离婚案件"])
        {
//            [calcResultLabel setText:[self liHunSuanFas:arm]];
            calcResultLabel.text =[self stringToAcross:[self liHunSuanFas:arm]];
        }else if ([type isEqualToString:@"非财产案件:侵害人格权案件"])
        {
//            [calcResultLabel setText:[self feicaichanJHRQs:arm]];
            calcResultLabel.text =[self stringToAcross:[self feicaichanJHRQs:arm]];
        }else if ([type isEqualToString:@"非财产案件:其他非财产案件"])
        {
            [calcResultLabel setText:[self qitacaiChans:arm]];
        }else if ([type isEqualToString:@"知识产权:知识产权案件"])
        {
//            [calcResultLabel setText:[self knowleDges:arm]];
             calcResultLabel.text =[self stringToAcross:[self knowleDges:amt]];
        }else if ([type isEqualToString:@"劳动争议:劳动争议案件"])
        {
            [calcResultLabel setText:[self labourLaws:arm]];
        }else if ([type isEqualToString:@"行政案件:商标、专利、海事行政案件"])
        {
            [calcResultLabel setText:[self trademarkPatcnts:arm]];
        }else if ([type isEqualToString:@"行政案件:其他行政案件"])
        {
            [calcResultLabel setText:[self elseAnYous:arm]];
        }else
        {
            [calcResultLabel setText:[self adminsterQs:arm]];
        }
    }
    
}
- (void)resetAction:(UIButton *)butt
{
    inputMoneyText.text =nil;
    calcResultLabel.text =[NSString stringWithFormat:@"0.00"];
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [inputMoneyText resignFirstResponder];
    [headerView resignFirstResponder];
    [calcView resignFirstResponder];
    [self resignFirstResponder];
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    int record = (int)textField.text.length;
    if ([textField.text rangeOfString:@"."].location != NSNotFound && [string isEqualToString:@"."]){
        return NO;
    }
    //    if ([textField.text containsString:@"."] && [string isEqualToString:@"."]) {
    //        return NO;
    //    }
    if ([string isEqualToString:@"."] && record == 0) {
        return NO;
    }
    if (record == 1 && ![string isEqualToString:@"."] && [textField.text isEqualToString:@"0"] && ![string isEqualToString:@""]) {
        return NO;
    }
    return YES;
}


- (void)typeOfAcceptanceBtnAction:(UIButton *)buta{
    if (chooseView.showTable)
    {   [UIView animateWithDuration:0.4 animations:^{
        calcView.frame =CGRectMake(10, OpenHeight, self.bounds.size.width-20, 150);
        
    }];
        mainView.contentSize = CGSizeMake(mainView.frame.size.width, 0);
        [chooseView dismiss];
    }else{
        
        [UIView animateWithDuration:0.3 animations:^{
            calcView.frame=CGRectMake(10, CloseHeight+5, self.bounds.size.width-20, 100);
        }];
        
        [chooseView show];
        mainView.contentSize = CGSizeMake(mainView.frame.size.width, 700);
    }
    NSLog(@"%@",chooseView);
    
}

@end
