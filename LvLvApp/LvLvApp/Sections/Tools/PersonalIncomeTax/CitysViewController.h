//
//  CitysViewController.h
//  Tools
//
//  Created by IOS8 on 15/10/9.
//  Copyright © 2015年 IOS8. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitysViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *cityTable;
@end

