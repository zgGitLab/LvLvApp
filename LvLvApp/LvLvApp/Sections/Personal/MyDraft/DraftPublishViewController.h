//
//  DraftPublishViewController.h
//  LvLvApp
//
//  Created by hope on 15/12/17.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DraftPublishViewController : BaseViewController
@property (nonatomic,copy) NSString * UserID;

@property (nonatomic,copy) NSString * TID;


@property (nonatomic,strong) UITextView * textView;

@property (nonatomic,copy) NSString *textContent;
@property (nonatomic, copy)NSString *PushanswerID;
@property (nonatomic,copy) NSString *answerID;



@end
