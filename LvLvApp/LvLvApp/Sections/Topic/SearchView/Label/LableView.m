//
//  LableView.m
//  LvLvApp
//
//  Created by hope on 15/11/25.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "LableView.h"
#import "LableTableViewCell.h"
#import "LDataModels.h"
#import "LBData.h"
#import "LBBaseClass.h"


@implementation LableView

- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0.5, [UIScreen mainScreen].bounds.size.width,  [UIScreen mainScreen].bounds.size.height);
        self.backgroundColor = [UIColor whiteColor];
}
    
   [self issueTableView];
    self.labedataarray=[[NSMutableArray alloc]init];
    return self;
}
- (id)initWithString:(NSString*)str {
    self = [self init];
   
       [self request:str];
    
    self.labTableView .mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.labedataarray removeAllObjects];
        [self request:str];
        [self layoutSubviews];
        
    }];
    self.labTableView .mj_footer =[MJRefreshAutoFooter footerWithRefreshingBlock:^{
        [self.labedataarray removeAllObjects];
        [self request:str];
        [self layoutSubviews];
    }];
    [self.labedataarray removeAllObjects];
    [self.labTableView  reloadData];
    return self;
}
- (void)issueTableView
{
    self.labTableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-100)style:UITableViewStylePlain];
    self.labTableView.backgroundColor =[UIColor whiteColor];
    [self.labTableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    self.labTableView.delegate = self;
    self.labTableView.dataSource = self;
    [self addSubview:self.labTableView];
    [self setExtraCellLineHidden:self.labTableView];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.labedataarray.count;
    
}
-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    if([self numberOfSectionsInTableView:tableView]==(section+1)){
        return [UIView new];
    }
    return nil;
}
-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [UIView new];
    
    view.backgroundColor = [UIColor clearColor];
    
    [tableView setTableFooterView:view];
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier =@"LableID";
    
    
    LableTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"LableTableViewCell" owner:self options:nil][0];
    }
    if (self.labedataarray.count>indexPath.row) {
        LBData *dd =self.labedataarray[indexPath.row];
        cell.LabetagName.text =dd.tagName;
        self.lableName =[NSString stringWithFormat:@"%@",dd.tagName];
        
        cell.AttentionProblem.text =[NSString stringWithFormat:@"关注 %ld · 问题 %ld · 评论 %ld",(long)dd.focusTag,(long)dd.questionCount, (long)dd.commentCount];
        
        self.IsFocus =[NSString stringWithFormat:@"%ld",(long)dd.isFocus];
        NSLog(@"%@",self.IsFocus);
        
        if (dd.isFocus ==0) {
            [cell.lableAttention setImage:[UIImage imageNamed:@"关注"] forState:UIControlStateNormal];
            [cell.lableAttention addTarget:self action:@selector(AttentionAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.lableAttention.selected = YES;
        }else
        {
            [cell.lableAttention setImage:[UIImage imageNamed:@"已关注1"] forState:UIControlStateNormal];
            [cell.lableAttention addTarget:self action:@selector(AttentionAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.lableAttention.selected = NO;
            
        }
        }
    return cell;
    
}

- (void)AttentionAction:(UIButton *)btn
{    
    if(!btn.selected)
    {
        [self collectRequestWithT:@"0"andBtn:btn];
    }else{
        [self collectRequestWithT:@"1"andBtn:btn];
    }
}
- (void) collectRequestWithT: (NSString *)T andBtn:(UIButton *)btn{
    
    UITableViewCell *Cell = (UITableViewCell *)btn.superview.superview;
    NSIndexPath *index = [self.labTableView indexPathForCell:Cell];
    LBData *dd = self.labedataarray[index.row];
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*count = [defaults objectForKey:@"users"];
    NSString *userID =[NSString stringWithFormat:@"%@",count];
    NSString *tagName = dd.tagName;

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KattentiontgaUrl,K_IP]];


        // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";

    NSMutableString *param = [NSMutableString string];
    [param appendString:[NSString stringWithFormat:@"UserID=%@",userID]];
    [param appendString:[NSString stringWithFormat:@"&TagName=%@",tagName]];
    [param appendString:[NSString stringWithFormat:@"&T=%@",T]];
    
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);
         if ([dict[@"Code"]isEqualToNumber:@200] ) {

                NSLog(@"关注成功");
                 btn.selected = !btn.selected;
                 NSLog(@"%d",btn.selected);
                 NSString *str =btn.selected?@"关注":@"已关注1";
                 [btn setImage:[UIImage imageNamed:str] forState:UIControlStateNormal];

         }
       }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LableTableViewCell  *cell =[tableView cellForRowAtIndexPath:indexPath];

    cell.selected =NO;
    if (_LableDelegate) {
        [_LableDelegate Lableclick:indexPath.row];
    }
    
}
- (void)getlableSearText:(NSString *)strs
{
    NSLog(@"%@",strs);
    self.searString =[NSString stringWithFormat:@"%@",strs];
    NSLog(@"%@",self.searString);
    [self request:strs];
    self.labTableView .mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.labedataarray removeAllObjects];
        [self request:strs];
        [self layoutSubviews];
        
    }];
    self.labTableView .mj_footer =[MJRefreshAutoFooter footerWithRefreshingBlock:^{
        [self.labedataarray removeAllObjects];
        [self request:strs];
        [self layoutSubviews];
    }];

    [self.labedataarray removeAllObjects];

    [self.labTableView reloadData];
}
- (void)request:(NSString *)TopicNames
{
    NSLog(@"%@",TopicNames);
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString*count = [defaults objectForKey:@"users"];
    
    NSString *userID =[NSString stringWithFormat:@"%@",count];

    NSString *TopicName =TopicNames;
    NSLog(@"%@",TopicName);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KseacrLableUrl,K_IP]];
    // 2.请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    // 4.设置请求体（请求参数）
    NSMutableString *param = [NSMutableString string];
  
    [param appendString:[NSString stringWithFormat:@"&UserID=%@",userID]];
    [param appendString:[NSString stringWithFormat:@"&TopicName=%@",TopicName]];
    
    request.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    // 5.发送请求
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"data%@",data);
         if (data ==nil || connectionError)return ;
         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         NSLog(@"dict %@",dict);
         NSString *info =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"`````````%@````",info);

         LBBaseClass *lab =[[LBBaseClass alloc] initWithDictionary:dict];

         [self.labedataarray addObjectsFromArray:lab.data];
         [self.labTableView reloadData];
         if (self.labelarrays) {
             self.labelarrays(self.labedataarray);
         }
         [self.labTableView reloadData];
      [ self.labTableView .mj_header endRefreshing];
      [ self.labTableView .mj_footer endRefreshing];
     }];
    
}

@end
