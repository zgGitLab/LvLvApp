//
//  S_LawDetail.h
//
//  Created by IOS8  on 15/11/10
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_LawDetail : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *category;       // 类别
@property (nonatomic, strong) NSString *pubDate;        // 发布时间
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *publishNum; // 发文字号
@property (nonatomic, strong) NSString *effectiveness;  // 时效性
@property (nonatomic, strong) NSString *title;          // 标题
@property (nonatomic, strong) NSString *publishDep;     // 发文机关
@property (nonatomic, strong) NSString *effectiveLevel;
@property (nonatomic, strong) NSString *effectiveDate;  // 生效日期
@property (nonatomic, strong) NSString *body;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
