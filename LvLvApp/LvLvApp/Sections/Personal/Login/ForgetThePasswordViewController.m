//
//  ForgetThePasswordViewController.m
//  LvLvApp
//
//  Created by hope on 15/10/30.
//  Copyright (c) 2015年 gyy. All rights reserved.
//

#define ktextfielW  self.view.frame.size.width-70
#define ktextfileY  13
#define kX 35
#import "ForgetThePasswordViewController.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+MJ.h"
#import "LoginViewController.h"

@interface ForgetThePasswordViewController ()
{
    UITextField *accountText;
    UITextField *codeText;
    UITextField *pwdsText;
    UIButton *codeBtn;
    UIButton *sureBtn;
    UIButton *_authCodeBtn;
    UIBarButtonItem *butItem;
    NSString *code;
    int second;
    NSString *yanZhengMas;
    NSTimer *_timer;
    NSInteger _timeNum;
}
@end

@implementation ForgetThePasswordViewController

-(void)initcomponents
{
    accountText = [[UITextField alloc] initWithFrame:CGRectMake(60, ktextfileY,  ktextfielW,  45)];
    [accountText setPlaceholder:@" 请输入邮箱或手机号"];
    accountText.font=[UIFont systemFontOfSize:15.f];
    [accountText  setBorderStyle:UITextBorderStyleNone];
    [accountText setKeyboardType:UIKeyboardTypeNamePhonePad];
    accountText.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:accountText];
    UIImageView *biaoImage =[[UIImageView alloc]initWithFrame:CGRectMake(kX,accountText.frame.origin.y+10,20, 20)];
    [biaoImage setImage:[UIImage imageNamed:@"账号"]];
    [self.view addSubview:biaoImage];
    UIImageView *backImage =[[UIImageView alloc]initWithFrame:CGRectMake(kX,accountText.frame.origin.y+accountText.frame.size.height, ktextfielW, 1)];
    [backImage setImage:[UIImage imageNamed:@"线"]];
    [self.view addSubview:backImage];
    codeText = [[UITextField alloc] initWithFrame:CGRectMake(60,accountText.frame.origin.y+50,  ktextfielW,  45)];
    [codeText setBorderStyle:UITextBorderStyleNone];
    [codeText setPlaceholder:@" 请输入验证码"];
    codeText.font =[UIFont systemFontOfSize:15.0f];
    [codeText setKeyboardType:UIKeyboardTypePhonePad];
    [codeText setSecureTextEntry:NO];
    
    [self.view addSubview:codeText];
    //密码头标
    UIImageView *pwdImage =[[UIImageView alloc]initWithFrame:CGRectMake(kX,codeText.frame.origin.y+10,20, 20)];
    [pwdImage setImage:[UIImage imageNamed:@"验证码"]];
    [self.view addSubview:pwdImage];
    
    UIImageView *upassbackImage =[[UIImageView alloc]initWithFrame:CGRectMake(kX,codeText.frame.origin.y+codeText.frame.size.height,ktextfielW ,1)];
    [upassbackImage setImage:[UIImage imageNamed:@"线"]];
    [self.view addSubview:upassbackImage];
    
    _authCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_authCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    _authCodeBtn.titleLabel.font=[UIFont systemFontOfSize:15.0f];
    [_authCodeBtn setTitleColor:[UIColor colorWithRed:73.0/255.0 green:154.0/255.0 blue:242.0/255.0 alpha:1 ]forState:UIControlStateNormal];
    [_authCodeBtn setFrame:CGRectMake(backImage.frame.size.width-40,accountText.frame.origin.y+55,80 ,30)];
    [_authCodeBtn addTarget:self action:@selector(authCodeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_authCodeBtn];
    
    
    
    pwdsText= [[UITextField alloc] initWithFrame:CGRectMake(60, codeText.frame.origin.y+50,  ktextfielW,  45)];
    [pwdsText setPlaceholder:@" 密码6-20位数字或字母"];
    pwdsText.font =[UIFont systemFontOfSize:15.0f];
    [pwdsText  setBorderStyle:UITextBorderStyleNone];
    [pwdsText setKeyboardType:UIKeyboardTypeNamePhonePad];
    [pwdsText setSecureTextEntry:YES];
    accountText.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:pwdsText];
    UIImageView *pwdImages =[[UIImageView alloc]initWithFrame:CGRectMake(kX,pwdsText.frame.origin.y+10,20, 20)];
    [pwdImages setImage:[UIImage imageNamed:@"密码"]];
    [self.view addSubview:pwdImages];
    UIImageView *xianpwd =[[UIImageView alloc]initWithFrame:CGRectMake(kX,pwdsText.frame.origin.y+pwdsText.frame.size.height,ktextfielW , 1)];
    [xianpwd setImage:[UIImage imageNamed:@"线"]];
    [self.view addSubview:xianpwd];
    
    sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBtn setBackgroundImage:[UIImage imageNamed:@"确定1"] forState:UIControlStateNormal];
    [sureBtn setFrame:CGRectMake(kX,xianpwd.frame.origin.y+xianpwd.frame.size.height+15,ktextfielW,45)];
    [self.view addSubview:sureBtn];
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self  initcomponents];
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.title=@"忘记密码";
    self.view.backgroundColor=[UIColor whiteColor];
    butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    ;
    butItem.tintColor=[UIColor colorWithRed:55.0/255.0 green:130.0/255.0 blue:242.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
}

- (void)sureAction
{
    NSString *zuaccount =accountText .text;
    if (zuaccount.length==0) {
        [MBProgressHUD showError:@"请输入邮箱或手机号"];
        return;
    }
    NSString *zupwd =pwdsText.text;
    if (zupwd.length <6) {
        [MBProgressHUD showError:@"请输入6-20数字或字母密码"];
        return;
    }
    NSString *yanzhengma =[NSString stringWithFormat:@"%@",yanZhengMas];
    NSLog(@"`````````%@",yanzhengma);
    if (![codeText.text isEqualToString:yanzhengma]) {
        // 密码错误
        [MBProgressHUD showError:@"验证码错误"];
        return;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:zuaccount forKey:@"EmailOrMobile"];
    [params setValue:zupwd forKey:@"UserPassword"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:KforgetUrl,K_IP] params:params success:^(id json) {
        // 解析服务器返回的JSON数据
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        [MBProgressHUD showSuccess:dict[@"Message"]];
        if ([dict[@"Code"] isEqualToNumber:@200]) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:pwdsText.text forKey:KPwd];
            [defaults synchronize];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failureMessage:^(NSString *errorMessage) {
        [MBProgressHUD showError:errorMessage];
    }];
    
    
}


- (void)validateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if ([emailTest evaluateWithObject:email]) {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:KmailUrl,K_IP]];
        [self requestDataWithUrl:url];
    }else{
        [MBProgressHUD showError:@"请输入正确邮箱号"];
    }
}


- (void)validatePhone:(NSString *)phone
{
    NSString *phoneRegex = @"^(((13[0-9])|(15([0-3]|[5-9]))|(18[0-9]))\\d{8})|(0\\d{2}-\\d{8})|(0\\d{3}-\\d{7})$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    
    if ([phoneTest evaluateWithObject:phone]) {
        [self phonerequestDataWithUrl:[NSString stringWithFormat:KmobilePhoneUrl,K_IP]];
    }else{
        [MBProgressHUD showError:@"请输入正确的手机号"];
    }
    
}

//获取验证码
- (void)authCodeBtnAction
{
    if (accountText.text.length==0) {
        [MBProgressHUD showError:@"请输入邮箱或手机号"];
        return;
    }
    
    if ([accountText.text rangeOfString:@"@"].location != NSNotFound) {
        [self validateEmail:accountText.text];
    }else{
        [self validatePhone:accountText.text];
    }
}


#pragma mark -- 手机验证码

- (void)phonerequestDataWithUrl:(NSString *)url
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:accountText.text forKey:@"Mobile"];
    [LVHTTPRequestTool post:url params:params success:^(id json) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableLeaves error:nil];
        NSDictionary *dic = [dict objectForKey:@"Data"];
        if ([dict[@"Code"] isEqualToNumber:@200]) {
            [MBProgressHUD showSuccess:dict[@"Message"]];
            [self setTimer];
        }else {
            [MBProgressHUD showError:dict[@"Message"]];
        }
        yanZhengMas = [dic objectForKey:@"VaData"];
        NSLog(@"%@",yanZhengMas);
    } failureMessage:^(NSString *errorMessage) {
        [MBProgressHUD showError:errorMessage];
    }];
}


#pragma mark -- 短信验证码

- (void)requestDataWithUrl:(NSURL *)url
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:accountText.text forKey:@"email"];
    
    [LVHTTPRequestTool post:[NSString stringWithFormat:@"%@",url] params:params success:^(id json) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:nil];
        NSDictionary *dic = [dict objectForKey:@"Data"];
        yanZhengMas = [dic objectForKey:@"SuccessCode"];
        NSLog(@"%@",yanZhengMas);
        if ([dict[@"Code"] isEqualToNumber:@200]) {
            [self setTimer];
            [MBProgressHUD showSuccess:dict[@"Message"]];
        }else {
            [MBProgressHUD showError:dict[@"Message"]];
        }
    } failureMessage:^(NSString *errorMessage) {
        [MBProgressHUD showError:errorMessage];
    }];
}


- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


//创建定时器
- (void)setTimer{
    _authCodeBtn.enabled = NO;
    [_authCodeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    _timeNum = 60;
    _timer = [[NSTimer alloc]initWithFireDate:[NSDate distantPast] interval:1 target:self selector:@selector(countNum) userInfo:nil repeats:YES];
    [[NSRunLoop  currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    
}

- (void)countNum{
    if (_timeNum >0) {
        [_authCodeBtn setTitle:[NSString stringWithFormat:@"(%lds)重发",(long)_timeNum] forState:UIControlStateNormal];
        _timeNum --;
    }else {
        [_timer invalidate];
        _timer = nil;
        [_authCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_authCodeBtn setTitleColor:[UIColor colorWithRed:73.0/255.0 green:154.0/255.0 blue:242.0/255.0 alpha:1 ]forState:UIControlStateNormal];
        _authCodeBtn.enabled = YES;
    }
}

@end
