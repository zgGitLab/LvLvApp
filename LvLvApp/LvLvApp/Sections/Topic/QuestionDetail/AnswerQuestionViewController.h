//
//  answerQuestionViewController.h
//  LvLvApp
//
//  Created by hope on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^BackBlock)(void);
typedef void (^SaveBlock)(NSString *str);
@interface AnswerQuestionViewController : BaseViewController

@property (nonatomic,copy) NSString * UserID;

@property (nonatomic,copy) NSString * TID;

@property (nonatomic,strong) BackBlock backBlock;

@property (nonatomic,strong) SaveBlock draftSaveBlock;

@property (nonatomic,strong) UITextView * textView;

@property (nonatomic,copy) NSString *textContent;

@property (nonatomic,copy) NSString *answerID;

@property (nonatomic,copy) NSString *content;

@property (nonatomic,copy) NSString *category;

@end
