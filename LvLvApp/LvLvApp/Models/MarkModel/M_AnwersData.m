//
//  M_AnwersData.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_AnwersData.h"


NSString *const kM_AnwersDataUserID = @"UserID";
NSString *const kM_AnwersDataHeaderID = @"HeaderID";
NSString *const kM_AnwersDataResponseCount = @"ResponseCount";
NSString *const kM_AnwersDataTuserID = @"TuserID";
NSString *const kM_AnwersDataHeaderTitle = @"HeaderTitle";
NSString *const kM_AnwersDataAnswerID = @"AnswerID";
NSString *const kM_AnwersDataAnswerContent = @"AnswerContent";
NSString *const kM_AnwersDataCollectCount = @"CollectCount";
NSString *const kM_AnwersDataAUserID = @"AUserID";
NSString *const kM_AnwersDataCommentCount = @"CommentCount";


@interface M_AnwersData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_AnwersData

@synthesize userID = _userID;
@synthesize headerID = _headerID;
@synthesize responseCount = _responseCount;
@synthesize tuserID = _tuserID;
@synthesize headerTitle = _headerTitle;
@synthesize answerID = _answerID;
@synthesize answerContent = _answerContent;
@synthesize collectCount = _collectCount;
@synthesize aUserID = _aUserID;
@synthesize commentCount = _commentCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userID = [[self objectOrNilForKey:kM_AnwersDataUserID fromDictionary:dict] doubleValue];
            self.headerID = [self objectOrNilForKey:kM_AnwersDataHeaderID fromDictionary:dict];
            self.responseCount = [[self objectOrNilForKey:kM_AnwersDataResponseCount fromDictionary:dict] doubleValue];
            self.tuserID = [[self objectOrNilForKey:kM_AnwersDataTuserID fromDictionary:dict] doubleValue];
            self.headerTitle = [self objectOrNilForKey:kM_AnwersDataHeaderTitle fromDictionary:dict];
            self.answerID = [self objectOrNilForKey:kM_AnwersDataAnswerID fromDictionary:dict];
            self.answerContent = [self objectOrNilForKey:kM_AnwersDataAnswerContent fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kM_AnwersDataCollectCount fromDictionary:dict] doubleValue];
            self.aUserID = [[self objectOrNilForKey:kM_AnwersDataAUserID fromDictionary:dict] doubleValue];
            self.commentCount = [[self objectOrNilForKey:kM_AnwersDataCommentCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kM_AnwersDataUserID];
    [mutableDict setValue:self.headerID forKey:kM_AnwersDataHeaderID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.responseCount] forKey:kM_AnwersDataResponseCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tuserID] forKey:kM_AnwersDataTuserID];
    [mutableDict setValue:self.headerTitle forKey:kM_AnwersDataHeaderTitle];
    [mutableDict setValue:self.answerID forKey:kM_AnwersDataAnswerID];
    [mutableDict setValue:self.answerContent forKey:kM_AnwersDataAnswerContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kM_AnwersDataCollectCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.aUserID] forKey:kM_AnwersDataAUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kM_AnwersDataCommentCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userID = [aDecoder decodeDoubleForKey:kM_AnwersDataUserID];
    self.headerID = [aDecoder decodeObjectForKey:kM_AnwersDataHeaderID];
    self.responseCount = [aDecoder decodeDoubleForKey:kM_AnwersDataResponseCount];
    self.tuserID = [aDecoder decodeDoubleForKey:kM_AnwersDataTuserID];
    self.headerTitle = [aDecoder decodeObjectForKey:kM_AnwersDataHeaderTitle];
    self.answerID = [aDecoder decodeObjectForKey:kM_AnwersDataAnswerID];
    self.answerContent = [aDecoder decodeObjectForKey:kM_AnwersDataAnswerContent];
    self.collectCount = [aDecoder decodeDoubleForKey:kM_AnwersDataCollectCount];
    self.aUserID = [aDecoder decodeDoubleForKey:kM_AnwersDataAUserID];
    self.commentCount = [aDecoder decodeDoubleForKey:kM_AnwersDataCommentCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_userID forKey:kM_AnwersDataUserID];
    [aCoder encodeObject:_headerID forKey:kM_AnwersDataHeaderID];
    [aCoder encodeDouble:_responseCount forKey:kM_AnwersDataResponseCount];
    [aCoder encodeDouble:_tuserID forKey:kM_AnwersDataTuserID];
    [aCoder encodeObject:_headerTitle forKey:kM_AnwersDataHeaderTitle];
    [aCoder encodeObject:_answerID forKey:kM_AnwersDataAnswerID];
    [aCoder encodeObject:_answerContent forKey:kM_AnwersDataAnswerContent];
    [aCoder encodeDouble:_collectCount forKey:kM_AnwersDataCollectCount];
    [aCoder encodeDouble:_aUserID forKey:kM_AnwersDataAUserID];
    [aCoder encodeDouble:_commentCount forKey:kM_AnwersDataCommentCount];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_AnwersData *copy = [[M_AnwersData alloc] init];
    
    if (copy) {

        copy.userID = self.userID;
        copy.headerID = [self.headerID copyWithZone:zone];
        copy.responseCount = self.responseCount;
        copy.tuserID = self.tuserID;
        copy.headerTitle = [self.headerTitle copyWithZone:zone];
        copy.answerID = [self.answerID copyWithZone:zone];
        copy.answerContent = [self.answerContent copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.aUserID = self.aUserID;
        copy.commentCount = self.commentCount;
    }
    
    return copy;
}


@end
