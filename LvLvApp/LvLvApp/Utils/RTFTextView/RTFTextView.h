//
//  RTFTextView.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/4/27.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTFTextView : UITextView

- (id)initWithFrame:(CGRect)frame andTextColor:(UIColor *)color andFont:(CGFloat)fontSize andText:(NSString *)text withNumOfLines:(NSInteger)lines andImageArray:(NSMutableArray *)imageArray;

@end
