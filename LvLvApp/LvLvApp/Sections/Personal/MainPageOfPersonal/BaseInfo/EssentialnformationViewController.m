//
//  EssentialnformationViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "EssentialnformationViewController.h"
#import "JobInfoTableViewCell.h"
#import "EducationTableViewCell.h"
#import "S_PerBaseInfoModatil.h" // 用户基本信息
@interface EssentialnformationViewController ()
{
    UIButton *iconBtn;
    UILabel *nicName;
    UILabel *infoMessage;
    UILabel *sex;
    UILabel *phoneNumber;
    S_PerBaseInfoBaseClass *s_BaseInfo;
}
@end

@implementation EssentialnformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"基本信息";
    self.infoTab.tableHeaderView = [[NSBundle mainBundle] loadNibNamed:@"BasicInfoHeardView" owner:self options:nil][0];
    [self getAllLabel];
    self.infoTab.backgroundColor = [UIColor colorWithRed:235/255.0 green:236/255.0  blue:237/255.0  alpha:1];
    [self requestBaseInfo];
}

- (void)getAllLabel
{
    iconBtn = (UIButton *)[self.infoTab.tableHeaderView viewWithTag:5];
    nicName = (UILabel *)[self.infoTab.tableHeaderView viewWithTag:6];
    infoMessage = (UILabel *)[self.infoTab.tableHeaderView viewWithTag:7];
    sex = (UILabel *)[self.infoTab.tableHeaderView viewWithTag:8];
    phoneNumber = (UILabel *)[self.infoTab.tableHeaderView viewWithTag:9];
}

- (void)requestBaseInfo
{
    __weak EssentialnformationViewController *VC = self;
    [RequestManager PerBaseInfo:self.userID andBlock:^(S_PerBaseInfoBaseClass *s_PerModal, NSString *message) {
        [VC performSelectorOnMainThread:@selector(refreshData:) withObject:s_PerModal waitUntilDone:YES];
    }];
}

- (void)refreshData:(S_PerBaseInfoBaseClass *)modal
{
    s_BaseInfo = modal;
    [iconBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,s_BaseInfo.imageUrl]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"头像小"]];
    iconBtn.layer.cornerRadius = 16.5;
    iconBtn.clipsToBounds = YES;
    nicName.text = s_BaseInfo.nickName;
    infoMessage.text = s_BaseInfo.introduction;
    sex.text = s_BaseInfo.gender;
    phoneNumber.text = s_BaseInfo.telPhone;
    
    [self.infoTab reloadData];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
       return s_BaseInfo.jobList.count;
    }
    return s_BaseInfo.eductList.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionVi = [[UIView alloc] init];
    sectionVi.backgroundColor = [UIColor colorWithRed:235/255.0 green:236/255.0  blue:237/255.0  alpha:1];
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, SIZE.width, 44)];
    titleLab.backgroundColor = [UIColor whiteColor];
    titleLab.text = section == 0?@"  工作经历":@"  教育经历";
    titleLab.font = [UIFont systemFontOfSize:15];
    titleLab.textColor = [UIColor colorWithRed:89/255.0 green:87/255.0 blue:87/255.0  alpha:1];
    titleLab.textAlignment = NSTextAlignmentLeft;
    UIView *lineVi = [[UIView alloc] initWithFrame:CGRectMake(0, 54, SIZE.width, 1)];
    lineVi.backgroundColor = [UIColor colorWithRed:215/255.0 green:215/255.0  blue:215/255.0  alpha:1];
    [sectionVi addSubview:lineVi];
    [sectionVi addSubview:titleLab];
    return sectionVi;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"jobCell";
    static NSString *identifier1 = @"eduCell";
    if (indexPath.section == 0) {
        JobInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[NSBundle mainBundle] loadNibNamed:@"JobInfoTableViewCell" owner:self options:nil][0];
        }
        S_PerBaseInfoJobList *jobModal = s_BaseInfo.jobList[indexPath.row];
        if (![jobModal.beginYear isEqualToString:@""]) {
            cell.jobTime.text = [NSString stringWithFormat:@"%@ - %@",[jobModal.beginYear stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],[jobModal.endYear stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }

        cell.companyName.text = jobModal.company;
        cell.jobName.text = jobModal.position;
        cell.areaName.text = jobModal.lacation;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        EducationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier1];
        if (!cell) {
            cell = [[NSBundle mainBundle] loadNibNamed:@"EducationTableViewCell" owner:self options:nil][0];
        }
        S_PerBaseInfoEductList *eduModal = s_BaseInfo.eductList[indexPath.row];
        cell.startSchool.text = eduModal.admissionDate;
        cell.schoolName.text = eduModal.university;
        cell.institute.text = eduModal.institute;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 130;
    }
    return 100;
}

@end
