//
//  AnswerModel.m
//  LvLvApp
//
//  Created by hope on 15/11/17.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "AnswerModel.h"

@implementation AnswerModel
- (id)initWithDictionary:(NSDictionary *)dict{
    if(self = [super init]){
        if ([dict[@"NickName"]isEqual:[NSNull null]]) {
            self.NickName = @"";
        }else{
            self.NickName = dict[@"NickName"];
        }
        
        if ([dict[@"ImageUrl"]isEqual:[NSNull null]]) {
            self.ImageUrl = @"";
        }else{
             self.ImageUrl = dict[@"ImageUrl"];
        }
        
        if ([dict[@"state"]isEqual:[NSNull null]]) {
            self.state = nil;
        }else{
            self.state = dict[@"state"];
        }

        
        if ([dict[@"AnswerDT"]isEqual:[NSNull null]]) {
            self.AnswerDT = nil;
            
        }else{
            self.AnswerDT = dict[@"AnswerDT"];
        }
        
        
        if ([dict[@"Title"]isEqual:[NSNull null]]) {
            self.Title = nil;
        }else{
            self.Title = dict[@"Title"];

        }
        
        
        if ( [dict[@"AnswerContent"] isEqual:[NSNull null]]) {
            self.AnswerContent = nil;
        }else{
            self.AnswerContent = dict[@"AnswerContent"];

        }
        
        if ([dict[@"CommentCount"]isEqual:[NSNull null]]) {
            self.CommentCount = nil;
        }else{
            self.CommentCount = dict[@"CommentCount"];

        }
        
        if ([dict[@"ResponseID"] isEqual:[NSNull null]]) {
            self.ResponseID = nil;
        }else{
            self.ResponseID = dict[@"ResponseID"];

        }
        
        if ([dict[@"IsCollect"]isEqual:[NSNull null]]) {
            self.IsCollect = nil;
        }else{
            self.IsCollect = dict[@"IsCollect"];
        }
        
        if ([dict[@"ImageList"] isEqual:[NSNull null]]) {
            self.ImageList = nil;
        }else{
            self.ImageList = dict[@"ImageList"];

        }
    }
    return self;
}


@end
