//
//  addGambitViewController.m
//  LvLvApp
//
//  Created by hope on 15/10/10.
//  Copyright (c) 2015年 gyy. All rights reserved.
//
#define kButW 40
#define KButH 30
#import "IssueGambitViewController.h"
#import "TopicHomeViewController.h"
#import "LabelViewController.h"
#import "InsertTextAttAchment.h"
#import "NSAttributedString+EmojiExtension.h"
#import "GambitAnswerViewController.h"

@interface IssueGambitViewController ()<UITextViewDelegate,UITextFieldDelegate,PerPickViewDelegat>
{
    UIView *viewadd;
    UIButton *returnBut;
    UIButton *nextBut;
    UIButton *selectBut;
    UITableView *_tablewView;
    UITextField *textFields;
    UIBarButtonItem *butItem;
    UIBarButtonItem *returnButt;
    UIButton *lablelBut;
    UITextView *_textView;
    CGRect frame1;
    NSArray *labelArray;
    UIImageView *lableimage;
    UILabel *_label;
    NSInteger number;
}
@property (nonatomic,strong) NSMutableArray *imageArray;
@property (nonatomic,strong) NSMutableString *ImgCode;
@end
@implementation IssueGambitViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageArray = [[NSMutableArray alloc]init];
    self.ImgCode = [NSMutableString stringWithFormat:@""];
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor=[UIColor whiteColor];
    self.navigationItem.title =@"新问题";
    
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"labelArray"];

    butItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"返回"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(returnButton)];

    self.navigationItem.leftBarButtonItem=butItem;
    butItem.tintColor =[UIColor colorWithRed:213.0/255.0 green:213.0/255.0 blue:213.0/255.0 alpha:1];
    
    returnButt=[[UIBarButtonItem alloc] initWithTitle:@"发布  "
                                              style: UIBarButtonItemStyleDone
                                             target:self
                                               action:@selector(IssueAction)];
    
    returnButt.enabled=YES;

    [returnButt setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,
                                        [UIColor colorWithRed:73.0/255.0 green:174.0/255.0 blue:242.0/255.0 alpha:1], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal]; self.navigationItem.rightBarButtonItem=returnButt;

    
    [self viewaddHeader];

    [self registerForKeyboardNotifications];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    number = 0;
}

- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidhide:) name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
}

//键盘隐藏
- (void)keyboardDidhide:(NSNotification *)notif{
    [UIView animateWithDuration:0.2 animations:^{
        _textView.frame = CGRectMake(12, 94+6, kWidth-24, kHeight-160);
    }];
}

//键盘显示
- (void) keyboardWasShown:(NSNotification *) notif
{
    NSDictionary *info = [notif userInfo];
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    
    NSLog(@"keyBoard:%f", keyboardSize.height);  //216
    CGFloat height = 0;
//    if (IsIPhone5s||IsIPhone5c||IsIPhone5) {
//        height = kHeight-keyboardSize.height-94-64-20;
//    }else{
        height = kHeight-keyboardSize.height-94-64-20;
//    }
    
    [UIView animateWithDuration:0.2 animations:^{
        _textView.frame = CGRectMake(12, 94+6, kWidth-24, height);
    }];
    
    
}

#pragma mark --发布请求
- (void)iSSueRequestWithDict:(NSMutableDictionary *)dict{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *url = [NSString stringWithFormat:kIssueUrl,K_IP];
    [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableString *str = [[NSMutableString alloc]initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",str);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"dict   ===%@",dict);
        BOOL success = [dict[@"Code"]  isEqualToNumber:@200];
        if(success){
            NSLog(@"发布成功!");
            if (self.addBlock) {
                self.addBlock();
            }
            GambitAnswerViewController *ans = [[GambitAnswerViewController alloc]init];
            ans.UserID = kUserID;
            ans.TID = dict[@"Data"];
            ans.isIssueFlag = 1;
            [self.navigationController pushViewController:ans animated:YES];
//            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            NSLog(@"发布失败");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}


//返回上一页
- (void)returnButton
{
//    if (self.addBlock) {
//        self.addBlock();
//    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark --发布
- (void)IssueAction
{
    if ([textFields.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请输入标题:" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    }else if(labelArray.count == 0 ){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请输入标签:" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    }else
    {
        
        NSString *string = [[NSString alloc]init];
        if (labelArray.count == 1) {
            string = [NSString stringWithFormat:@"%@",labelArray[0]];
        }else if(labelArray.count == 2){
            string =[NSString stringWithFormat:@"%@|%@",labelArray[0],labelArray[1]];
        }else if(labelArray.count == 3){
            string =[NSString stringWithFormat:@"%@|%@|%@",labelArray[0],labelArray[1],labelArray[2]];
        }else if(labelArray.count == 4){
            string =[NSString stringWithFormat:@"%@|%@|%@|%@",labelArray[0],labelArray[1],labelArray[2],labelArray[3]];
        }else if(labelArray.count == 5){
            string =[NSString stringWithFormat:@"%@|%@|%@|%@|%@",labelArray[0],labelArray[1],labelArray[2],labelArray[3],labelArray[4]];
        }
        self.imageArray = [_textView.textStorage getImages];
        //处理图片，将图片数组转换成base64字符串
        for (int i = 0; i < _imageArray.count; i++) {
            NSData *data = UIImageJPEGRepresentation(_imageArray[i], 1);
            NSString *encodedImageStr = [data base64EncodedStringWithOptions:0];
            [_ImgCode appendString:encodedImageStr];
            if ((i != _imageArray.count-1)&&(_imageArray.count !=1 )) {
                [_ImgCode appendString:@"|"];
            }
        }
        NSString *info =[_textView.textStorage getPlainString];
        NSMutableString *str = [[NSMutableString alloc]initWithString:info];
        
        //把所有的0替换为11；
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"0.jpg\"/>"];
            
            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"r.jpg\"/>"]];
                
            }
        }
        
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"r.jpg\"/>"];
            
            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"%d.jpg\"/>",i]];
            }
        }

        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *textfieldtext = textFields.text;
        textfieldtext = [textfieldtext stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
        NSMutableString *title = [NSMutableString stringWithFormat:@"%@",textfieldtext];
        
        NSString *s = [title substringFromIndex:title.length-1];
        
        if ([s isEqualToString:@"?"] || [s isEqualToString:@"？"]) {
            [title replaceCharactersInRange:NSMakeRange(title.length-1, 1) withString:@"?"];
        }else{
            [title appendString:@"?"];
        }
        [dict setObject:title forKey:@"Title"];//Title
        [dict setObject:string forKey:@"Keywords"];//Keywords
        [dict setObject:str forKey:@"Description"];//Description
        [dict setObject:kUserID forKey:@"UserID"];//UserID
        [dict setObject:_ImgCode forKey:@"ImgCodes"];//ImgCodes
        [self iSSueRequestWithDict:dict];
    }
}


#pragma mark --添加头视图
- (void)viewaddHeader
{
    viewadd =[[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 200)];
    
    [self.view addSubview:viewadd];
    //标题
    textFields =[[UITextField alloc] initWithFrame:CGRectMake(15, 3, kWidth-30, 40)];
    [textFields setTextColor:[UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1]];
    textFields.font=[UIFont systemFontOfSize:17.0f];
    [textFields setPlaceholder:@"请输入标题并以问号结尾"];
    textFields.delegate=self;
    [textFields setTextColor:[UIColor blackColor]];
    [viewadd addSubview:textFields];
    
    //线
    UIImageView *titleImage =[[UIImageView alloc]initWithFrame:CGRectMake(15,textFields.frame.origin.y+textFields.frame.size.height,kWidth -30,1)];
    [titleImage setImage:[UIImage imageNamed:@"线"]];
    [viewadd addSubview:titleImage];
    
    
    //选择标签按钮
    selectBut = [UIButton buttonWithType:UIButtonTypeCustom];
    selectBut.tag = 10000;
    selectBut.titleLabel.textAlignment=NSTextAlignmentLeft;
    [selectBut setTitleColor:[UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1] forState:UIControlStateNormal];
     selectBut.titleLabel.font=[UIFont systemFontOfSize:15.0f];
    [selectBut setFrame:CGRectMake(15,titleImage.frame.origin.y+titleImage.frame.size.height,kWidth-31,45)];
    [selectBut addTarget:self action:@selector(slsetAction) forControlEvents:UIControlEventTouchUpInside];

     [viewadd addSubview: selectBut];
    //标签图片
    lableimage=[[UIImageView alloc]initWithFrame:CGRectMake(83,titleImage.frame.origin.y+titleImage.frame.size.height+16, 14, 14)];
    frame1 = lableimage.frame;
    [lableimage setImage:[UIImage imageNamed:@"标签"]];
    [viewadd addSubview:lableimage];
    
    lablelBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [lablelBut setTitle:@"选择标签" forState:UIControlStateNormal];
    lablelBut.titleLabel.textAlignment=NSTextAlignmentLeft;
    [lablelBut setTitleColor:[UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1] forState:UIControlStateNormal];
    lablelBut.titleLabel.font=[UIFont systemFontOfSize:15.0f];
    [lablelBut setFrame:CGRectMake(15,titleImage.frame.origin.y+titleImage.frame.size.height,60,45)];
    [lablelBut addTarget:self action:@selector(slsetAction) forControlEvents:UIControlEventTouchUpInside];
    [viewadd addSubview:lablelBut];
    
    
    UIButton *arrowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    arrowBtn.tag = 10001;
    arrowBtn.frame = CGRectMake(kWidth-31,selectBut.frame.origin.y+selectBut.frame.size.height/4+3,11,18);
    [arrowBtn setImage:[UIImage imageNamed:@"箭头"] forState:UIControlStateNormal];
    [arrowBtn addTarget:self action:@selector(slsetAction) forControlEvents:UIControlEventTouchUpInside];
    [viewadd addSubview:arrowBtn];
    
    UIImageView *slectImage =[[UIImageView alloc]initWithFrame:CGRectMake(15,selectBut.frame.origin.y+selectBut.frame.size.height+1,kWidth-30,1)];
    [slectImage setImage:[UIImage imageNamed:@"线"]];
    [viewadd addSubview:slectImage];
    

    _textView = [[UITextView alloc] initWithFrame:CGRectMake(12,slectImage.frame.origin.y+slectImage.frame.size.height+6, kWidth-30, kHeight-100-64)];
    _textView.textColor=[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
    NSLog(@"%lf",_textView.frame.origin.y);
    
    _label = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 200, 20)];
    _label.text = @"请输入内容(选填)";
    _label.font =  [UIFont systemFontOfSize:15];
    _label.textColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1];

    [_textView addSubview:_label];

    
    _textView.font=[UIFont systemFontOfSize:15.0f];
    _textView.textAlignment = NSTextAlignmentLeft;
    _textView.autocorrectionType = UITextAutocorrectionTypeNo;
    _textView.keyboardType = UIKeyboardTypeDefault;
    
    _textView.returnKeyType = UIReturnKeyDefault;
    _textView.scrollEnabled = YES;
    _textView.delegate = self;
    _textView.inputAccessoryView = [self keyboardView];
    [self.view addSubview:_textView];
    
}

- (UIView *)keyboardView{
    //自定义键盘上部的view
    UIView *keyboardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 36)];
    keyboardView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    UIButton *at = [UIButton buttonWithType:UIButtonTypeSystem];
    at.frame = CGRectMake(15, 5, 26, 26);
    [at setImage: [[UIImage imageNamed:@"收键盘"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [at addTarget:self action:@selector(collectKeyBoard:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:at];
    
    UIButton *key = [UIButton buttonWithType:UIButtonTypeSystem];
    key.frame = CGRectMake(CGRectGetMaxX(at.frame)+15, 3, 26, 26);
    [key setImage:[[UIImage imageNamed:@"图片"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [key addTarget:self action:@selector(selectPic:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:key];
    return keyboardView;
}

//选择图片
- (void)selectPic:(UIButton *)button{
    [self.view endEditing:YES];
    PerPickImageView *perPickVi = [[PerPickImageView alloc] initWithId:1];
    perPickVi.delegate = self;
    [perPickVi showInView];
}

//收键盘
- (void)collectKeyBoard:(UIButton *)button{
    [_textView resignFirstResponder];
}



- (void)textViewDidChange:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        _label.text = @"请输入内容(选填)";
    }else{
        _label.text = @"";
    }
}

- (void)slsetAction
{
    LabelViewController *label=[[LabelViewController alloc]init];
    label.refreshEvent = ^{
        [self.view endEditing:YES];
        //添加标签
        labelArray = [[SingeData sharedInstance]labelDataArray];
        for (int i = 0; i < 5; i ++) {
            UILabel *btn = (UILabel *)[viewadd viewWithTag:i+10];
            [btn removeFromSuperview];
        }

        if (labelArray.count == 0) {
            [viewadd addSubview: lablelBut];
            [viewadd addSubview:  lableimage];
        }else{
            [lablelBut removeFromSuperview];
            [lableimage removeFromSuperview];
        }
        
        NSMutableArray *arr = [NSMutableArray array];
        for (int i = 0; i < labelArray.count;i++ ) {
            CGFloat widths = [[labelArray objectAtIndex:i] boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:13]} context:nil].size.width;
            NSString *str = [NSString stringWithFormat:@"%lf",widths+20];
            [arr addObject:str];
        }
        CGRect frame  = CGRectNull;
        for (int i = 0; i < labelArray.count; i ++) {
            UILabel *label = nil;
            if (i == 0) {
                label = [[UILabel alloc]initWithFrame:CGRectMake(14, frame1.origin.y-3, [arr[i] integerValue], 20)];
                frame = label.frame;
                label.tag = i+10;
                [label setText:[labelArray objectAtIndex:i]];
                label.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:236.0/255.0 blue:253.0/255.0 alpha:1];
                label.textAlignment = NSTextAlignmentCenter;
                label.layer.cornerRadius = 2;
                label.clipsToBounds =YES;
                label.font = [UIFont systemFontOfSize:13];
                label.textColor = [UIColor colorWithRed:93/255.0 green:137/255.0 blue:176/255.0 alpha:1];
                [viewadd addSubview:label];
            }else{
                label = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(frame)+5, frame1.origin.y-3, [arr[i] integerValue], 20)];
                label.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:236.0/255.0 blue:253.0/255.0 alpha:1];
                label.tag = i+10;
                label.textAlignment = NSTextAlignmentCenter;
                label.layer.cornerRadius = 2;
                label.clipsToBounds =YES;
                label.font = [UIFont systemFontOfSize:13];
                label.textColor = [UIColor colorWithRed:93/255.0 green:137/255.0 blue:176/255.0 alpha:1];
                if (CGRectGetMaxX(label.frame) > kWidth-100) {
                    label.frame = CGRectMake(CGRectGetMaxX(frame)+5, frame1.origin.y-3, 40, 20);
                    [label setText:@"..."];
                    [viewadd addSubview:label];
                    break;
                }else{
                    [label setText:[labelArray objectAtIndex:i]];
                    [viewadd addSubview:label];
                }
                frame = label.frame;
            }
        }
    };
    [self.navigationController pushViewController:label animated:YES];
}



- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == textFields) {
        if (textField.text.length > 80) {
            textField.text = [textField.text substringToIndex:80];
        }
    }
}


#pragma mark PerPickViewDelegat;
- (void)jumpOtherView:(UIImagePickerController *)object
{
    [self presentViewController:object animated:YES completion:nil];
}

- (void)selectImage:(id)icon
{
    UIImage *imag = (UIImage *)icon;
    
    CGFloat rate = (kWidth-20)/imag.size.width;
    imag = [Utils thumbnailWithImageWithoutScale:imag size:CGSizeMake(kWidth-20, imag.size.height *rate)];
    InsertTextAttAchment *texAtt = [[InsertTextAttAchment alloc] init];
    texAtt.image = imag;
    
    [_textView.textStorage insertAttributedString:[NSAttributedString attributedStringWithAttachment:texAtt]
                                              atIndex:_textView.selectedRange.location];
    //Move selection location
    _textView.selectedRange = NSMakeRange(_textView.selectedRange.location + 1, _textView.selectedRange.length);
    
    //设置字的设置
    [self setInitLocation];
    [_textView becomeFirstResponder];
}

-(void)setInitLocation
{
    _textView.font = [UIFont systemFontOfSize:15];
    _textView.textColor = [UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
}
@end
