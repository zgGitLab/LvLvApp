//
//  JobTableViewCell.h
//  LvLvApp
//
//  Created by Sunny on 16/3/29.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL isSearchList;

@property (weak, nonatomic) IBOutlet UILabel *jobNameLab;

@property (weak, nonatomic) IBOutlet UILabel *moneyLab;

@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *locationLab;

@property (nonatomic,strong) JobLIstModel *model;
@end
