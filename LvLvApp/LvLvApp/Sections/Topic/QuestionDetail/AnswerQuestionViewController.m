//
//  answerQuestionViewController.m
//  LvLvApp
//
//  Created by hope on 15/11/19.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "AnswerQuestionViewController.h"
#import "InsertTextAttAchment.h"
#import "NSAttributedString+EmojiExtension.h"
@interface AnswerQuestionViewController ()<UITextViewDelegate,PerPickViewDelegat,UIAlertViewDelegate>
{
    UILabel *_label;
    BOOL _anonymous;
    NSArray *_imageList;
}
@property (nonatomic,strong) NSMutableArray *imageArray;
@property (nonatomic,copy) NSMutableString *ImgCode;
@property (nonatomic,copy) NSMutableString *totalString;
@property (nonatomic,copy) NSString *textString;


@end

@implementation AnswerQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _anonymous = NO;
    _ImgCode = [NSMutableString string];
    _totalString = [NSMutableString string];
    self.imageArray = [NSMutableArray array];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *backBtn =[UIButton buttonWithType:UIButtonTypeSystem];
    [backBtn setTitle:@"取消" forState:UIControlStateNormal];
    [backBtn setTitleColor:setColor(0, 91, 255) forState:UIControlStateNormal];
    backBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    backBtn.frame = CGRectMake(0, 0, 40, 40);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem = back;
    UIButton *publishBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [publishBtn setTitleColor:setColor(0, 91, 255) forState:UIControlStateNormal];
    [publishBtn setTitle:@"发布" forState:UIControlStateNormal];
    publishBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [publishBtn addTarget:self action:@selector(publish) forControlEvents:UIControlEventTouchUpInside];
    publishBtn.frame = CGRectMake(0, 0, 40, 40);
    UIBarButtonItem *publish = [[UIBarButtonItem alloc]initWithCustomView:publishBtn];
    self.navigationItem.rightBarButtonItem = publish;
    
    [self createTextView];
    
    _label = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 200, 20)];
    _label.enabled = NO;
    if (self.textContent) {

        _label.text = @"";
    }else{
        _label.text = @"填写回答内容";
    }
    
    _label.font =  [UIFont systemFontOfSize:15];
    _label.textColor = [UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1];
    [self.textView addSubview:_label];
    //增加监听,当键盘出现或者改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    //增加监听,当键盘退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma  mark --键盘事件
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    
    self.textView.frame = CGRectMake(10, 10, kWidth-20, kHeight-height-80);
    
}

- (void)keyboardWillHide:(NSNotification *)aNotification{
    
    self.textView.frame = CGRectMake(10, 10, kWidth-20, kHeight-20);
}

- (void)createTextView{
    //自定义键盘上部的view
    UIView *keyboardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 36)];
    
    UIButton *key = [UIButton buttonWithType:UIButtonTypeSystem];
    key.frame = CGRectMake(kWidth-26-15, 7, 26, 26);
    [key setImage:[[UIImage imageNamed:@"收键盘"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [key addTarget:self action:@selector(collectKeyBoard:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:key];
    
    UIButton *pic = [UIButton buttonWithType:UIButtonTypeSystem];
    pic.frame = CGRectMake(key.frame.origin.x-15-26, 5, 26, 26);
    [pic setImage:[[UIImage imageNamed:@"图片"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [pic addTarget:self action:@selector(picClick:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:pic];
    
    keyboardView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
//    UIButton *at = [UIButton buttonWithType:UIButtonTypeSystem];
//    at.frame = CGRectMake(pic.frame.origin.x - 15 - 22, 8, 22, 22);
//    [at setImage: [[UIImage imageNamed:@"圈@"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
//    [at addTarget:self action:@selector(atClick:) forControlEvents:UIControlEventTouchUpInside];
//    [keyboardView addSubview:at];
    
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 10, kWidth-20, kHeight-400)];
    if ([Utils isBlankString:self.textContent]) {
        _imageList = [NSArray array];
    }else{
        _imageList=[Utils subArrayFromString:self.textContent];
        [self showTextView];
    }
    
    
    self.textView.inputAccessoryView = keyboardView;
    self.textView.textColor = [UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
    self.textView.font = [UIFont systemFontOfSize:15];
    
    [self.textView becomeFirstResponder];
    self.textView.delegate = self;
//    self.textView.text = self.content;
    
    [self.view addSubview:self.textView];
}

- (void)showTextView{
    NSMutableString *str = [[NSMutableString alloc]initWithFormat:@"%@",_textContent];
    //创建NSMutableAttributedString实例，并将content传入
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:str];
    //设置行距
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    [style setLineSpacing:5.0f];
    //根据给定长度与style设置attStr式样
    [attStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1],NSParagraphStyleAttributeName:style,NSFontAttributeName:[UIFont systemFontOfSize:15]} range:NSMakeRange(0, str.length)];
    NSInteger mm = 0;
    for (int i = 0; i < _imageList.count; i ++) {
        if (![_imageList[i] isEqualToString:@""]) {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,_imageList[i]]];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
            CGFloat rate = (kWidth-20)/image.size.width;
            image = [Utils thumbnailWithImageWithoutScale:image size:CGSizeMake(kWidth-20, rate*image.size.height)];
            InsertTextAttAchment *attatchment = [[InsertTextAttAchment alloc] init];
            attatchment.image = image;
            
            NSRange range = [str rangeOfString:@"<"];
            
            NSUInteger length = [@"<img src=\"upload/answer/20160503142715338834.jpg\"/>" length];
            [str deleteCharactersInRange:NSMakeRange(range.location, length) ];
            [attStr deleteCharactersInRange:NSMakeRange(range.location+mm, length)];
            [attStr insertAttributedString:[NSAttributedString attributedStringWithAttachment:attatchment] atIndex:range.location+mm];
            mm++;
        }
    }
    self.textView.attributedText = attStr;
}


/**
 *  提交回答
 *
 *  @param content 回答内容
 *  @param status  1直接发布
 */
- (void)requestDataWithContent:(NSString *)content andStatus:(NSNumber *)status{
    
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:kUserID forKey:@"UserID"];
    [parm setObject:status forKey:@"Status"];
    [parm setObject:_ImgCode forKey:@"ImgCode"];
    [parm setObject:self.TID forKey:@"TID"];
    [parm setObject:content forKey:@"Content"];
    if (![Utils isBlankString:self.answerID]) {
        [parm setObject:self.answerID forKey:@"Answer_ID"];
    }else{
        [parm setObject:@"" forKey:@"Answer_ID"];
    }
    
    NSString *url =nil;
    if ([self.category isEqualToString:@"2"]) {
        url = [NSString stringWithFormat:KDraftPublishUrl,K_IP];
    }else{
        url =[NSString stringWithFormat:kAnswerQuestion,K_IP];
    }
    
    [LVHTTPRequestTool post:url params:parm success:^(id json) {
        NSMutableString *str = [[NSMutableString alloc]initWithData:json encoding:NSUTF8StringEncoding];
        NSLog(@"str:%@",str);
        [self.navigationController popViewControllerAnimated:YES];
        if (self.backBlock) {
            self.backBlock();
        }
    } failure:^(NSError *error) {
        
    }];
}



/**
 *  保存草稿
 *
 *  @param content 回答内容
 *  @param status  保存到草稿
 */
- (void)requestDataWithContent:(NSString *)content{

    NSMutableDictionary *parm = [[NSMutableDictionary alloc]init];
    [parm setObject:kUserID forKey:@"UserID"];
    [parm setObject:_ImgCode forKey:@"ImgCode"];
    [parm setObject:self.TID forKey:@"TID"];
    [parm setObject:content forKey:@"Content"];
    if (![Utils isBlankString:self.answerID]) {
        [parm setObject:self.answerID forKey:@"Answer_ID"];
    }else{
        [parm setObject:@"" forKey:@"Answer_ID"];
    }
    
//    NSString *url = nil;
//    if ([self.category isEqualToString:@"0"]) {
//        url = [NSString stringWithFormat:kSaveDraft,K_IP];

    [LVHTTPRequestTool post:[NSString stringWithFormat:kSaveDraft,K_IP] params:parm success:^(id json) {
        [self.navigationController popViewControllerAnimated:YES];
        if (self.backBlock) {
            self.backBlock();
        }
        
        if (self.draftSaveBlock) {
            self.draftSaveBlock(@"已存草稿");
        }
    } failure:^(NSError *error) {
        
    }];
}

//nick匿名发布
//- (void)nickClick:(UIButton *)button{
//    button.selected = !button.selected;
//    _anonymous = !_anonymous;
//}

//at
//- (void)atClick:(UIButton *)button{
//    _label.text = @"";
//    NSMutableString *str = [[NSMutableString alloc]init];
//    str = [NSMutableString stringWithFormat:@"%@@",self.textView.text];
//    self.textView.text = str;
//}

//collectKeyBoard
- (void)collectKeyBoard:(UIButton *)button{
    [self.textView resignFirstResponder];
}

//pic
- (void)picClick:(UIButton *)button{
    _label.text = @"";
    [self.view endEditing:YES];
    PerPickImageView *perPickVi = [[PerPickImageView alloc] initWithId:1];
    perPickVi.delegate = self;
    [perPickVi showInView];
}

//发布
- (void)publish{
    
    if ([Utils removeBlankString:self.textView.text].length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请填写发布内容" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }else {
        self.imageArray = [self.textView.textStorage getImages];
        //处理图片，将图片数组转换成base64字符串
        for (int i = 0; i < _imageArray.count; i++) {
            NSData *data = UIImageJPEGRepresentation(_imageArray[i], 1);
            NSString *encodedImageStr = [data base64EncodedStringWithOptions:0];
            [_ImgCode appendString:encodedImageStr];
            if ((i != _imageArray.count-1)||(_imageArray.count !=1)) {
                [_ImgCode appendString:@"|"];
            }
        }
        
        
        NSString *info =[self.textView.textStorage getPlainString];
        NSMutableString *str = [[NSMutableString alloc]initWithString:info];
        
        //把所有的0替换为r；
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"0.jpg\"/>"];
            
            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"r.jpg\"/>"]];
                
            }
        }
        //对图片进行重新标号
        for (int i = 0; i < str.length; i++) {
            NSRange range = [str rangeOfString:@"<img src=\"r.jpg\"/>"];
            if (range.location != NSNotFound) {
                [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"%d.jpg\"/>",i]];
            }
        }

        [self requestDataWithContent:str andStatus:[NSNumber numberWithInt:1]];
        [self.textView resignFirstResponder];
    }
}

//取消
- (void)back{
    if (![self.textView.text isEqualToString:@""]) {
        [self.view endEditing:YES];
        //如果是第一次回答，则保存为草稿
//        if ([self.category isEqualToString:@"0"]) {
            //处理图片，将图片数组转换成base64字符串
            _imageArray = [self.textView.textStorage getImages];
            for (int i = 0; i < _imageArray.count; i++) {
                NSData *data = UIImageJPEGRepresentation(_imageArray[i], 1);
                NSString *encodedImageStr = [data base64EncodedStringWithOptions:0];
                [_ImgCode appendString:encodedImageStr];
                if ((i != _imageArray.count-1)||(_imageArray.count !=1)) {
                    [_ImgCode appendString:@"|"];
                }
            }
            NSString *info =[self.textView.textStorage getPlainString];
            NSMutableString *str = [[NSMutableString alloc]initWithString:info];
            
            //把所有的0替换为r；
            for (int i = 0; i < str.length; i++) {
                NSRange range = [str rangeOfString:@"<img src=\"0.jpg\"/>"];
                
                if (range.location != NSNotFound) {
                    [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"r.jpg\"/>"]];
                    
                }
            }
            //对图片进行重新标号
            for (int i = 0; i < str.length; i++) {
                NSRange range = [str rangeOfString:@"<img src=\"r.jpg\"/>"];
                
                if (range.location != NSNotFound) {
                    [str replaceCharactersInRange:range withString:[NSString stringWithFormat:@"<img src=\"%d.jpg\"/>",i]];
                }
            }
            
            [self requestDataWithContent:str];
            [self.textView resignFirstResponder];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
        [self.textView resignFirstResponder];
    }
    
}




- (void)textViewDidChange:(UITextView *)textView{

    if ([textView.text length] == 0) {
        [_label setHidden:NO];
    }else{
        [_label setHidden:YES];
    }
}




#pragma mark PerPickViewDelegat;
- (void)jumpOtherView:(UIImagePickerController *)object
{
    object.allowsEditing = NO;
    [self presentViewController:object animated:YES completion:nil];
}

- (void)selectImage:(id)icon
{
    UIImage *imag = (UIImage *)icon;


    CGFloat rate = (kWidth-20)/imag.size.width;
    imag = [Utils thumbnailWithImageWithoutScale:imag size:CGSizeMake(kWidth-20, imag.size.height *rate)];
    InsertTextAttAchment *texAtt = [[InsertTextAttAchment alloc] init];
    texAtt.image = imag;

    [self.textView.textStorage insertAttributedString:[NSAttributedString attributedStringWithAttachment:texAtt]
                                          atIndex:self.textView.selectedRange.location];
    //Move selection location
    _textView.selectedRange = NSMakeRange(_textView.selectedRange.location + 1, _textView.selectedRange.length);
    _textView.font = [UIFont systemFontOfSize:15];
    _textView.textColor = [UIColor colorWithRed:81/255.0 green:88/255.0 blue:99/255.0 alpha:1];
    [_textView becomeFirstResponder];
}



-(UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;   //返回的就是已经改变的图片
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
