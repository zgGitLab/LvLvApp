//
//  M_IDGainData.h
//
//  Created by hope  on 15/12/25
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface M_IDGainData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, assign) double userID;
@property (nonatomic, assign) double isCheck;
@property (nonatomic, strong) NSString *remark;
@property (nonatomic, strong) NSString *profession;
@property (nonatomic, assign) double isPass;
@property (nonatomic, strong) NSString *companyName;
@property (nonatomic, strong) NSString *userName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
