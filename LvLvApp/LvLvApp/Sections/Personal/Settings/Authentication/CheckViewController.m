//
//  CheckViewController.m
//  LvLvApp
//
//  Created by hope on 15/12/18.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "CheckViewController.h"
#import "SetViewController.h"

@interface CheckViewController ()

@end

@implementation CheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title =@"身份认证";
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBarHidden = NO;
    UIBarButtonItem*butItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回箭头"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    ;
    butItem.tintColor=[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    self.navigationItem.leftBarButtonItem=butItem;
    
    //添加约束
    //背景图
    [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.top);
        make.left.equalTo(self.view.left);
        make.right.equalTo(self.view.right);
        make.bottom.equalTo(self.view.bottom);
    }];
    
    //图片
    [self.image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(93);
        make.height.equalTo(133);
        make.top.equalTo(self.bgScrollView.top).offset(125);
        make.left.equalTo(self.bgScrollView.left).offset((kWidth-93)/2);
    }];
    
    //文字
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.image.bottom).offset(45);
        make.width.equalTo(170);
        make.height.equalTo(25);
        make.left.equalTo(self.bgScrollView.left).offset((kWidth-170)/2);
    }];
    
    //按钮
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.label.bottom).offset(121);
        make.width.equalTo(180);
        make.height.equalTo(45);
        make.left.equalTo((kWidth-180)/2);
        
        //修改contentsize
        self.bgScrollView.contentSize = CGSizeMake(kWidth, CGRectGetMaxY(self.backBtn.frame)+30);
    }];
}


- (void)back{
    [self.navigationController  popToRootViewControllerAnimated:YES];
}

//撤回申请
- (IBAction)btnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
