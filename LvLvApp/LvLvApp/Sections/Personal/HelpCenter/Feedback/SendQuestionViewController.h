//
//  SendQuestionViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/12/21.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "BaseViewController.h"

@interface SendQuestionViewController : BaseViewController
@property (weak, nonatomic) IBOutlet LPlaceholderTextView *questText;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UIView *bgPhioneView;

@property (weak, nonatomic) IBOutlet UIView *bgImageView;
@property (weak, nonatomic) IBOutlet UIButton *addImageBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *bgScroll;
@end
