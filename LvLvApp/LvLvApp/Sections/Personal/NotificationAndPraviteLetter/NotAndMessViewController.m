//
//  NotAndMessViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/20.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "NotAndMessViewController.h"

#import "WriteViewController.h"
#import "ChatViewController.h"
#import "MyButton.h"

#import "GambitAnswerViewController.h"
#import "AnswerDetailViewController.h"
#import "PerMainPageViewController.h"
#import "OtherPerMainViewController.h"
#import "SCSwipeTableViewCell.h"
@interface NotAndMessViewController ()<SCSwipeTableViewCellDelegate>
{
    int selectIndex;
    UIButton *lastBtn;
    UIButton *writeBtn;
    NSMutableArray *notDataArray;
    NSMutableArray *privateletterArray;
    int pageIndex;
    NSMutableArray *btnArr;
    SCSwipeTableViewCell *tempCell;
}
@end

@implementation NotAndMessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //    self.title = @"通知和私信";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
    notDataArray = [NSMutableArray array];
    privateletterArray = [NSMutableArray array];
    self.view.backgroundColor = [UIColor whiteColor];
    [self createUI];
    self.bgTable.tableFooterView = [[UIView alloc] init];
    self.bgTable.separatorInset = UIEdgeInsetsMake(0, 0, 0, 8);
    
    __weak NotAndMessViewController *VC = self;
    // 上拉加载
    self.bgTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 结束刷新
        [VC infinite];
    }];
    self.bgTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 1;
        NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
        [VC notFicList:page];
        [VC privateletterRequest:page];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    pageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
    [self notFicList:page];
    [self privateletterRequest:page];
}

- (void)notFicList:(NSString *)pageIN
{
    __weak NotAndMessViewController *VC = self;
    [RequestManager myNotficWithUser:self.userID andPageIndex:pageIN andPageSize:@"10" andBlock:^(NSArray *dataArr, NSString *message) {
        [VC.bgTable.mj_header endRefreshing];
        [VC.bgTable.mj_footer endRefreshing];
        if ([pageIN isEqualToString:@"1"]) {
            [notDataArray removeAllObjects];
            notDataArray = [NSMutableArray arrayWithArray:dataArr];
        }else{
            [notDataArray addObjectsFromArray:dataArr];
        }
        [VC performSelectorOnMainThread:@selector(refreshAnserUI) withObject:nil waitUntilDone:YES];
        
    }];
    
}

- (void)infinite
{
    pageIndex += 1;
    NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
    [self notFicList:page];
    
}

- (void)refreshAnserUI
{
    [self.bgTable reloadData];
}

- (void)privateletterRequest:(NSString *)pageIN
{
    __weak NotAndMessViewController *VC = self;
    [RequestManager myPrivateLetterWithUser:self.userID andPageIndex:pageIN andPageSize:@"10" andBlock:^(NSArray *dataArr, NSString *message) {
        [VC.bgTable.mj_header endRefreshing];
        [VC.bgTable.mj_footer endRefreshing];
        if ([pageIN isEqualToString:@"1"]) {
            [privateletterArray removeAllObjects];
            privateletterArray = [NSMutableArray arrayWithArray:dataArr];
        }else{
            [privateletterArray addObjectsFromArray:dataArr];
        }
        [VC performSelectorOnMainThread:@selector(refreshAnserUI) withObject:nil waitUntilDone:YES];
        
    }];
}

- (void)writeInfo
{
    WriteViewController *writeVC = [[WriteViewController alloc] init];
    writeVC.userID = self.userID;
    writeVC.mySelfImage = self.mySelfImage;
    [self.navigationController pushViewController:writeVC animated:YES];
}

- (void)createUI
{
    writeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    writeBtn.frame = CGRectMake(0, 10, 50, 40);
    writeBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [writeBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    //    writeBtn.backgroundColor = [UIColor orangeColor];
    writeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    writeBtn.titleEdgeInsets = UIEdgeInsetsMake(5,0,0,0);
    writeBtn.hidden = YES;
    [writeBtn setTitle:@"写私信" forState:UIControlStateNormal];
    [writeBtn addTarget:self action:@selector(writeInfo) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:writeBtn];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 44)];
    UIButton *notificationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    notificationBtn.frame = CGRectMake(0, 15, 50, 20);
    notificationBtn.tag = 1;
    selectIndex = (int)notificationBtn.tag;
    lastBtn = notificationBtn;
    [notificationBtn setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
    notificationBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    //    notificationBtn.backgroundColor = [UIColor redColor];
    [notificationBtn setTitle:@"通知" forState:UIControlStateNormal];
    [notificationBtn addTarget:self action:@selector(headBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *messBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    messBtn.frame = CGRectMake(70, 15, 50, 20);
    [messBtn setTitleColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
    //    messBtn.backgroundColor = [UIColor redColor];
    [messBtn setTitle:@"私信" forState:UIControlStateNormal];
    [messBtn addTarget:self action:@selector(headBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    messBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    messBtn.tag = 2;
    
    UILabel *colorLab = [[UILabel alloc] initWithFrame:CGRectMake(notificationBtn.center.x-18, 41, 36, 3)];
    colorLab.backgroundColor = [UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1];
    colorLab.tag = 3;
    [titleView addSubview:colorLab];
    [titleView addSubview:messBtn];
    [titleView addSubview:notificationBtn];
    
    self.navigationItem.titleView = titleView;
}

- (void)headBtnClick:(UIButton *)btn
{
    if (btn.tag == 1) {
        writeBtn.hidden = YES;
        pageIndex = 1;
        NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
        [self notFicList:page];
    }else{
        writeBtn.hidden = NO;
        pageIndex = 1;
        NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
        [self privateletterRequest:page];
    }
    
    if (btn.tag == selectIndex) {
        return;
    }else{
        selectIndex = (int)btn.tag;
        [lastBtn setTitleColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:58/255.0 green:134/255.0 blue:240/255.0 alpha:1] forState:UIControlStateNormal];
        UILabel *colorLab = (UILabel *)[self.navigationItem.titleView viewWithTag:3];
        lastBtn = btn;
        [UIView animateWithDuration:.2 animations:^{
            colorLab.frame = CGRectMake(btn.center.x-18, 41, 36, 3);
        } completion:^(BOOL finished) {
            
        }];
    }
    [self.bgTable reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (selectIndex == 1) {
        return notDataArray.count;
    }
    return privateletterArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectIndex == 1) {
        S_NotiFiCData *notData = notDataArray[indexPath.row];
        if (notData.types == 2 || notData.types == 4) {   // 表示回答了问题   ,表示赞同了问题
            if ([notData.logoUrl isEqualToString:@""]) {
                return [self getHightWithStr:notData.answerContent andWidth:SIZE.width-20]+70;
            }else{
                return 125;
            }
            
            
        }else if (notData.types == 5){ // 评论    带有图片
            if ([notData.logoUrl isEqualToString:@""]) {// 没有图片
                return [self getHightWithStr:notData.answerContent andWidth:SIZE.width-20]+70;
            }else{                                      // 有图片
                return 125;
            }
        }else{                          // 6关注用户；
            return 35;
        }
        
    }else{
        return 70;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    static NSString *identifier = @"ncell";
    static NSString *identifier1 = @"mcell";
    UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
    btn1.backgroundColor = [UIColor redColor];
    [btn1 setTitle:@"删除" forState:UIControlStateNormal];
    UIButton *btn2 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 0.1, 55)];
    //    btn2.backgroundColor = [UIColor greenColor];
    [btn2 setTitle:@"" forState:UIControlStateNormal];
    btnArr = [[NSMutableArray alloc]initWithObjects:btn1,btn2, nil];
    if (selectIndex == 1) {
        
        //        SCSwipeTableViewCell *cell = (SCSwipeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
        //        if (cell == nil) {
        SCSwipeTableViewCell *cell = [[SCSwipeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                                 reuseIdentifier:@"ncell"
                                                                        withBtns:btnArr
                                                                       tableView:tableView];
        
        tempCell = cell;
        cell.delegate = self;
        MyButton *nicName = [MyButton buttonWithType:UIButtonTypeCustom];
        [nicName addTarget:self action:@selector(nicNameBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        nicName.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        nicName.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        [nicName setTitleColor:COLOR(73, 158, 242) forState:UIControlStateNormal];
        nicName.titleLabel.font = [UIFont systemFontOfSize:11];
        nicName.tag = 1;
        UILabel *timeLab = [[UILabel alloc]init];
        timeLab.textColor = COLOR(156, 156, 156);
        timeLab.font = [UIFont systemFontOfSize:11];
        timeLab.textAlignment = NSTextAlignmentLeft;
        timeLab.tag = 2;
        MyButton *titleLab = [MyButton buttonWithType:UIButtonTypeCustom];
        [titleLab setTitleColor:COLOR(81, 88, 99) forState:UIControlStateNormal];
        titleLab.titleLabel.font = [UIFont systemFontOfSize:16];
        titleLab.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        titleLab.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        titleLab.tag = 3;
        [titleLab addTarget:self action:@selector(nicNameBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        MyButton *conentLab = [MyButton buttonWithType:UIButtonTypeCustom];
        [conentLab setTitleColor: COLOR(122, 122, 122) forState:UIControlStateNormal];
        conentLab.titleLabel.font = [UIFont systemFontOfSize:14];
        conentLab.tag = 4;
        [conentLab addTarget:self action:@selector(nicNameBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        conentLab.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        conentLab.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        [cell.SCContentView addSubview:nicName];
        [cell.SCContentView addSubview:timeLab];
        [cell.SCContentView addSubview:titleLab];
        [cell.SCContentView addSubview:conentLab];
        //        }
        UIImageView *imag = (UIImageView *)[cell.contentView viewWithTag:1000];
        [imag removeFromSuperview];
        S_NotiFiCData *notData = notDataArray[indexPath.row];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:11] , NSFontAttributeName, nil];
        CGFloat strWith = [notData.nickname sizeWithAttributes:dictionary].width;
        
        
        MyButton *nicLab = [cell.SCContentView viewWithTag:1];
        conentLab.titleLabel.numberOfLines = 2;
        [nicLab setTitle:notData.nickname forState:UIControlStateNormal];
        
        titleLab.ID = notData.notifyID;
        titleLab.AnserID = notData.answerID;
        titleLab.TID = notData.tID;
        titleLab.TUserID = notData.TUserID;
        
        conentLab.ID = notData.notifyID;
        conentLab.AnserID = notData.answerID;
        conentLab.TID = notData.tID;
        conentLab.AUserID = notData.AUserID;
        
        nicLab.ID = notData.notifyID;
        nicLab.NowUserID = [NSString stringWithFormat:@"%.0f",notData.userid];
        
        timeLab.text = notData.dataDescription;
        nicLab.frame = CGRectMake(10, 15, SIZE.width-20, 10);
        timeLab.frame = CGRectMake(strWith+20, 15, SIZE.width-strWith-30, 10);
        
        if (notData.types == 2 || notData.types == 4) {   // ,表示赞同了表示回答了问题   问题
            titleLab.hidden = NO;
            conentLab.hidden = NO;
            if ([notData.logoUrl isEqualToString:@""]) {// 没有图片
                titleLab.frame = CGRectMake(10, 35, SIZE.width-20, 20);
                [titleLab setTitle:notData.title forState:UIControlStateNormal];
                [conentLab setTitle:notData.answerContent forState:UIControlStateNormal];
                conentLab.frame = CGRectMake(10, 60, SIZE.width-20, [self getHightWithStr:notData.answerContent andWidth:SIZE.width-20]);
                cell.SCContentView.frame = CGRectMake(0, 0, SIZE.width, CGRectGetMaxY(conentLab.frame)+10);
                btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, cell.SCContentView.frame.size.height);
            }else{
                
                if (notData.answerContent != nil) {
                    notData.answerContent = [Utils replaceImageString:notData.answerContent];
                }
                UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(SIZE.width - 90, 30, 80, 80)];
                [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,notData.logoUrl]]];
                image.tag = 1000;
                [cell.SCContentView addSubview:image];
                
                titleLab.frame = CGRectMake(10, 35, SIZE.width-110, 20);
                [titleLab setTitle:notData.title forState:UIControlStateNormal];
                [conentLab setTitle:notData.answerContent forState:UIControlStateNormal];
                conentLab.frame = CGRectMake(10, 60, SIZE.width-110, [self getHightWithStr:notData.answerContent andWidth:SIZE.width-110]);
                cell.SCContentView.frame = CGRectMake(0, 0, SIZE.width, CGRectGetMaxY(image.frame)+10);
                btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, cell.SCContentView.frame.size.height);
            }
            
            
        }else if (notData.types == 5){ // 评论    带有图片
            titleLab.hidden = NO;
            conentLab.hidden = NO;
            [titleLab setTitle:notData.title forState:UIControlStateNormal];
            if ([notData.logoUrl isEqualToString:@""]) {// 没有图片
                NSLog(@"asdf");
                titleLab.frame = CGRectMake(10, 35, SIZE.width-20, 20);
                //                conentLab.text = notData.answerContent;
                [conentLab setTitle:notData.answerContent forState:UIControlStateNormal];
                conentLab.frame = CGRectMake(10, 60, SIZE.width-20, [self getHightWithStr:notData.answerContent andWidth:SIZE.width-20]);
                cell.SCContentView.frame = CGRectMake(0, 0, SIZE.width, [self getHightWithStr:notData.answerContent andWidth:SIZE.width-20]+70);
                btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, cell.SCContentView.frame.size.height);
            }else{                                      // 有图片
                if (notData.answerContent != nil) {
                    notData.answerContent = [Utils replaceImageString:notData.answerContent];
                }
                UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(SIZE.width - 90, 30, 80, 80)];
                [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,notData.logoUrl]]];
                image.tag = 1000;
                [cell.SCContentView addSubview:image];
                titleLab.frame = CGRectMake(10, 35, SIZE.width-110, 20);
                //                conentLab.text = notData.answerContent;
                [conentLab setTitle:notData.answerContent forState:UIControlStateNormal];
                conentLab.frame = CGRectMake(10, 60, SIZE.width-110, [self getHightWithStr:notData.answerContent andWidth:SIZE.width-110]);
                cell.SCContentView.frame = CGRectMake(0, 0, SIZE.width, CGRectGetMaxY(image.frame)+10);
                btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, cell.SCContentView.frame.size.height);
            }
            
        }else{                          // 6关注用户；
            cell.SCContentView.frame = CGRectMake(0, 0, SIZE.width, 35);
            btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, cell.SCContentView.frame.size.height);
            titleLab.hidden = YES;
            conentLab.hidden = YES;
        }
        
        return cell;
        
    }else{
        
        SCSwipeTableViewCell *cell = (SCSwipeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier1];
        if (cell == nil) {
            cell = [[SCSwipeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:@"mcell"
                                                      withBtns:btnArr
                                                     tableView:tableView];
            tempCell = cell;
            cell.delegate = self;
            UIButton *headerImage = [UIButton buttonWithType:UIButtonTypeCustom];
            headerImage.frame = CGRectMake(10, 8, 53, 53);
            [headerImage setBackgroundImage:[UIImage imageNamed:@"头像"] forState:UIControlStateNormal];
            headerImage.tag = 200;
            headerImage.layer.cornerRadius = 26.5;
            headerImage.clipsToBounds = YES;
            UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(70, 14, SIZE.width - 80, 20)];
            titleLab.tag = 201;
            titleLab.textColor = COLOR(81, 88, 99);
            titleLab.font = [UIFont systemFontOfSize:16];
            
            UILabel *conentLab = [[UILabel alloc] initWithFrame:CGRectMake(70, 38, SIZE.width - 80, 15)];
            conentLab.tag = 202;
            conentLab.textColor = COLOR(122, 122, 122);
            conentLab.font = [UIFont systemFontOfSize:14];
            
            [cell.SCContentView addSubview:conentLab];
            [cell.SCContentView addSubview:titleLab];
            [cell.SCContentView addSubview:headerImage];
        }
        
        UIButton *pushBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        pushBtn.frame = CGRectMake(0, 0, SIZE.width, 70);
        [pushBtn addTarget:self action:@selector(pushBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        pushBtn.tag = indexPath.row+1;
        [cell.SCContentView addSubview:pushBtn];
        
        UIButton *imageBtn = (UIButton *)[cell.SCContentView viewWithTag:200];
        UILabel *titLab = (UILabel *)[cell.SCContentView viewWithTag:201];
        UILabel *connLab = (UILabel *)[cell.SCContentView viewWithTag:202];
        
        S_PrivateLetterData *letterList = privateletterArray[indexPath.row];
        [imageBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,letterList.image]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"头像"]];
        titLab.text = letterList.nickName;
        connLab.text = letterList.introduction;
        cell.SCContentView.frame = CGRectMake(0, 0, SIZE.width, 70);
        btn1.frame = CGRectMake(btn1.frame.origin.x, 0, 80, 70);
        return cell;
    }
}

- (CGFloat)getHightWithStr:(NSString *)str andWidth:(CGFloat)width
{
    CGSize titleSize = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} context:nil].size;
    titleSize.height = titleSize.height>35?35:titleSize.height;
    return titleSize.height;
    
}

- (void)pushBtnClick:(UIButton *)btn
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.isShowBtn) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SC_CELL_SHOULDCLOSE" object:nil userInfo:@{@"action":@"closeCell"}];
    }else{
        S_PrivateLetterData *letterList = privateletterArray[btn.tag-1];
        ChatViewController *chatVC = [[ChatViewController alloc] init];
        chatVC.mySelfImage = self.mySelfImage;
        chatVC.otherID = [NSString stringWithFormat:@"%.0f",letterList.iDProperty];
        chatVC.titleStr = letterList.nickName;
        chatVC.userID = self.userID;
        chatVC.otherID = [NSString stringWithFormat:@"%.0f",letterList.iDProperty];
        [self.navigationController pushViewController:chatVC animated:YES];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSIndexPath *selected = [tableView indexPathForSelectedRow];
    
    if(selected)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    if (selectIndex == 2) {
        
        S_PrivateLetterData *letterList = privateletterArray[indexPath.row];
        ChatViewController *chatVC = [[ChatViewController alloc] init];
        chatVC.mySelfImage = self.mySelfImage;
        chatVC.otherID = [NSString stringWithFormat:@"%.0f",letterList.iDProperty];
        chatVC.titleStr = letterList.nickName;
        chatVC.userID = self.userID;
        chatVC.otherID = [NSString stringWithFormat:@"%.0f",letterList.iDProperty];
        [self.navigationController pushViewController:chatVC animated:YES];
    }
}



- (void)nicNameBtnClick:(MyButton *)btn
{
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.isShowBtn) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SC_CELL_SHOULDCLOSE" object:nil userInfo:@{@"action":@"closeCell"}];
        app.isShowBtn = NO;
        return;
    }
    NSString *notID = [NSString stringWithFormat:@"%.0f",btn.ID];
    // 修改通知状态；
    [RequestManager changeNotStates:notID andBlock:^(NSString *message) {
        
    }];
    if (btn.tag == 1) {
        // 昵称
        if ([[self getUserId] isEqualToString:btn.NowUserID]) {// 跳转到个人主页
            PerMainPageViewController *perMainVC = [[PerMainPageViewController alloc] init];
            perMainVC.userID = btn.NowUserID;
            [self.navigationController pushViewController:perMainVC animated:YES];
        }else{
            // 跳转到他人主页
            OtherPerMainViewController *otherPerMainVC = [[OtherPerMainViewController alloc] init];
            otherPerMainVC.userID = [self getUserId];
            otherPerMainVC.otherID = btn.NowUserID;
            [self.navigationController pushViewController:otherPerMainVC animated:YES];
        }
    }else if (btn.tag == 3){
        // 标题
        GambitAnswerViewController *GamBVC = [[GambitAnswerViewController alloc] init];
        GamBVC.TID = btn.TID;
        GamBVC.UserID = [NSString stringWithFormat:@"%@",btn.TUserID];
        GamBVC.Answer_ID = btn.AnserID;
        [self.navigationController pushViewController:GamBVC animated:YES];
    }else{
        // 正文
        NSLog(@"点击正文");
        AnswerDetailViewController *anserKeyVC = [[AnswerDetailViewController alloc] init];
        anserKeyVC.UserID = [NSString stringWithFormat:@"%@",btn.AUserID];
        anserKeyVC.TID = btn.TID;
        anserKeyVC.answerID = btn.AnserID;
        [self.navigationController pushViewController:anserKeyVC animated:YES];
    }
    
}
- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}


#pragma mark SCSwipeTableViewCellDelegate

- (void)SCSwipeTableViewCelldidSelectBtnWithTag:(NSInteger)tag andIndexPath:(NSIndexPath *)indexpath{
    if (selectIndex == 1) {
        S_NotiFiCData *notData = notDataArray[indexpath.row];
        NSString *notID = [NSString stringWithFormat:@"%.0f",notData.notifyID];
        // 修改通知状态；
        [RequestManager changeNotStates:notID andBlock:^(NSString *message) {
            
        }];
        [notDataArray removeObjectAtIndex:indexpath.row];
        [self.bgTable deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexpath, nil] withRowAnimation:UITableViewRowAnimationTop];
        
    }else{
        
        S_PrivateLetterData *letterList = privateletterArray[indexpath.row];
        [RequestManager perConcernOther:self.userID andOtherID:[NSString stringWithFormat:@"%.0f",letterList.iDProperty] andIsConcern:@"0" andBlock:^( NSString *message) {
            if ([message isEqualToString:@"成功"]) {
                [privateletterArray removeObjectAtIndex:indexpath.row];
                [self.bgTable deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexpath, nil] withRowAnimation:UITableViewRowAnimationTop];
            }
        }];
        
    }
}

- (void)cellOptionBtnWillShow{
    NSLog(@"cellOptionBtnWillShow");
}

- (void)cellOptionBtnWillHide{
    NSLog(@"cellOptionBtnWillHide");
}

- (void)cellOptionBtnDidShow{
    NSLog(@"cellOptionBtnDidShow");
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    app.isShowBtn = YES;
}

- (void)cellOptionBtnDidHide{
    NSLog(@"cellOptionBtnDidHide");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
