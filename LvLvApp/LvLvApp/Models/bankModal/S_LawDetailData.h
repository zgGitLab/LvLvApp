//
//  S_LawDetailData.h
//
//  Created by IOS8  on 15/12/4
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class S_LawDetailDs;

@interface S_LawDetailData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double isCollect;
@property (nonatomic, strong) S_LawDetailDs *ds;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
