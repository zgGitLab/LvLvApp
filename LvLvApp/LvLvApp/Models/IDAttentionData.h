//
//  IDAttentionData.h
//
//  Created by hope  on 15/12/9
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface IDAttentionData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *tID;
@property (nonatomic, assign) double focusCount;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double answerCount;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
