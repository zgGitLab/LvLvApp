//
//  M_lableListResult.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_lableListResult.h"


NSString *const kM_lableListResultTitle = @"Title";
NSString *const kM_lableListResultUserid = @"Userid";
NSString *const kM_lableListResultNotifyID = @"NotifyID";
NSString *const kM_lableListResultDescription = @"Description";
NSString *const kM_lableListResultAUserID = @"AUserID";
NSString *const kM_lableListResultCommentCount = @"commentCount";
NSString *const kM_lableListResultTimagesUrl = @"TimagesUrl";
NSString *const kM_lableListResultLogoUrl = @"LogoUrl";
NSString *const kM_lableListResultQuestionContent = @"QuestionContent";
NSString *const kM_lableListResultAnswerContent = @"AnswerContent";
NSString *const kM_lableListResultCollectCount = @"collectCount";
NSString *const kM_lableListResultSortTime = @"sortTime";
NSString *const kM_lableListResultTimes = @"times";
NSString *const kM_lableListResultNickname = @"Nickname";
NSString *const kM_lableListResultTypes = @"types";
NSString *const kM_lableListResultCounts = @"counts";
NSString *const kM_lableListResultTUserID = @"TUserID";
NSString *const kM_lableListResultTID = @"TID";
NSString *const kM_lableListResultAnswerID = @"Answer_ID";


@interface M_lableListResult ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_lableListResult

@synthesize title = _title;
@synthesize userid = _userid;
@synthesize notifyID = _notifyID;
@synthesize listResultDescription = _listResultDescription;
@synthesize aUserID = _aUserID;
@synthesize commentCount = _commentCount;
@synthesize timagesUrl = _timagesUrl;
@synthesize logoUrl = _logoUrl;
@synthesize questionContent = _questionContent;
@synthesize answerContent = _answerContent;
@synthesize collectCount = _collectCount;
@synthesize sortTime = _sortTime;
@synthesize times = _times;
@synthesize nickname = _nickname;
@synthesize types = _types;
@synthesize counts = _counts;
@synthesize tUserID = _tUserID;
@synthesize tID = _tID;
@synthesize answerID = _answerID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.title = [self objectOrNilForKey:kM_lableListResultTitle fromDictionary:dict];
            self.userid = [self objectOrNilForKey:kM_lableListResultUserid fromDictionary:dict];
            self.notifyID = [[self objectOrNilForKey:kM_lableListResultNotifyID fromDictionary:dict] doubleValue];
            self.listResultDescription = [self objectOrNilForKey:kM_lableListResultDescription fromDictionary:dict];
            self.aUserID = [[self objectOrNilForKey:kM_lableListResultAUserID fromDictionary:dict] doubleValue];
            self.commentCount = [[self objectOrNilForKey:kM_lableListResultCommentCount fromDictionary:dict] doubleValue];
            self.timagesUrl = [self objectOrNilForKey:kM_lableListResultTimagesUrl fromDictionary:dict];
            self.logoUrl = [self objectOrNilForKey:kM_lableListResultLogoUrl fromDictionary:dict];
            self.questionContent = [self objectOrNilForKey:kM_lableListResultQuestionContent fromDictionary:dict];
            self.answerContent = [self objectOrNilForKey:kM_lableListResultAnswerContent fromDictionary:dict];
            self.collectCount = [[self objectOrNilForKey:kM_lableListResultCollectCount fromDictionary:dict] doubleValue];
            self.sortTime = [self objectOrNilForKey:kM_lableListResultSortTime fromDictionary:dict];
            self.times = [self objectOrNilForKey:kM_lableListResultTimes fromDictionary:dict];
            self.nickname = [self objectOrNilForKey:kM_lableListResultNickname fromDictionary:dict];
            self.types = [[self objectOrNilForKey:kM_lableListResultTypes fromDictionary:dict] doubleValue];
            self.counts = [[self objectOrNilForKey:kM_lableListResultCounts fromDictionary:dict] doubleValue];
            self.tUserID = [self objectOrNilForKey:kM_lableListResultTUserID fromDictionary:dict];
            self.tID = [self objectOrNilForKey:kM_lableListResultTID fromDictionary:dict];
            self.answerID = [self objectOrNilForKey:kM_lableListResultAnswerID fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.title forKey:kM_lableListResultTitle];
    [mutableDict setValue:self.userid forKey:kM_lableListResultUserid];
    [mutableDict setValue:[NSNumber numberWithDouble:self.notifyID] forKey:kM_lableListResultNotifyID];
    [mutableDict setValue:self.listResultDescription forKey:kM_lableListResultDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.aUserID] forKey:kM_lableListResultAUserID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentCount] forKey:kM_lableListResultCommentCount];
    [mutableDict setValue:self.timagesUrl forKey:kM_lableListResultTimagesUrl];
    [mutableDict setValue:self.logoUrl forKey:kM_lableListResultLogoUrl];
    [mutableDict setValue:self.questionContent forKey:kM_lableListResultQuestionContent];
    [mutableDict setValue:self.answerContent forKey:kM_lableListResultAnswerContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectCount] forKey:kM_lableListResultCollectCount];
    [mutableDict setValue:self.sortTime forKey:kM_lableListResultSortTime];
    [mutableDict setValue:self.times forKey:kM_lableListResultTimes];
    [mutableDict setValue:self.nickname forKey:kM_lableListResultNickname];
    [mutableDict setValue:[NSNumber numberWithDouble:self.types] forKey:kM_lableListResultTypes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.counts] forKey:kM_lableListResultCounts];
    [mutableDict setValue:self.tUserID forKey:kM_lableListResultTUserID];
    [mutableDict setValue:self.tID forKey:kM_lableListResultTID];
    [mutableDict setValue:self.answerID forKey:kM_lableListResultAnswerID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.title = [aDecoder decodeObjectForKey:kM_lableListResultTitle];
    self.userid = [aDecoder decodeObjectForKey:kM_lableListResultUserid];
    self.notifyID = [aDecoder decodeDoubleForKey:kM_lableListResultNotifyID];
    self.listResultDescription = [aDecoder decodeObjectForKey:kM_lableListResultDescription];
    self.aUserID = [aDecoder decodeDoubleForKey:kM_lableListResultAUserID];
    self.commentCount = [aDecoder decodeDoubleForKey:kM_lableListResultCommentCount];
    self.timagesUrl = [aDecoder decodeObjectForKey:kM_lableListResultTimagesUrl];
    self.logoUrl = [aDecoder decodeObjectForKey:kM_lableListResultLogoUrl];
    self.questionContent = [aDecoder decodeObjectForKey:kM_lableListResultQuestionContent];
    self.answerContent = [aDecoder decodeObjectForKey:kM_lableListResultAnswerContent];
    self.collectCount = [aDecoder decodeDoubleForKey:kM_lableListResultCollectCount];
    self.sortTime = [aDecoder decodeObjectForKey:kM_lableListResultSortTime];
    self.times = [aDecoder decodeObjectForKey:kM_lableListResultTimes];
    self.nickname = [aDecoder decodeObjectForKey:kM_lableListResultNickname];
    self.types = [aDecoder decodeDoubleForKey:kM_lableListResultTypes];
    self.counts = [aDecoder decodeDoubleForKey:kM_lableListResultCounts];
    self.tUserID = [aDecoder decodeObjectForKey:kM_lableListResultTUserID];
    self.tID = [aDecoder decodeObjectForKey:kM_lableListResultTID];
    self.answerID = [aDecoder decodeObjectForKey:kM_lableListResultAnswerID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_title forKey:kM_lableListResultTitle];
    [aCoder encodeObject:_userid forKey:kM_lableListResultUserid];
    [aCoder encodeDouble:_notifyID forKey:kM_lableListResultNotifyID];
    [aCoder encodeObject:_listResultDescription forKey:kM_lableListResultDescription];
    [aCoder encodeDouble:_aUserID forKey:kM_lableListResultAUserID];
    [aCoder encodeDouble:_commentCount forKey:kM_lableListResultCommentCount];
    [aCoder encodeObject:_timagesUrl forKey:kM_lableListResultTimagesUrl];
    [aCoder encodeObject:_logoUrl forKey:kM_lableListResultLogoUrl];
    [aCoder encodeObject:_questionContent forKey:kM_lableListResultQuestionContent];
    [aCoder encodeObject:_answerContent forKey:kM_lableListResultAnswerContent];
    [aCoder encodeDouble:_collectCount forKey:kM_lableListResultCollectCount];
    [aCoder encodeObject:_sortTime forKey:kM_lableListResultSortTime];
    [aCoder encodeObject:_times forKey:kM_lableListResultTimes];
    [aCoder encodeObject:_nickname forKey:kM_lableListResultNickname];
    [aCoder encodeDouble:_types forKey:kM_lableListResultTypes];
    [aCoder encodeDouble:_counts forKey:kM_lableListResultCounts];
    [aCoder encodeObject:_tUserID forKey:kM_lableListResultTUserID];
    [aCoder encodeObject:_tID forKey:kM_lableListResultTID];
    [aCoder encodeObject:_answerID forKey:kM_lableListResultAnswerID];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_lableListResult *copy = [[M_lableListResult alloc] init];
    
    if (copy) {

        copy.title = [self.title copyWithZone:zone];
        copy.userid = [self.userid copyWithZone:zone];
        copy.notifyID = self.notifyID;
        copy.listResultDescription = [self.listResultDescription copyWithZone:zone];
        copy.aUserID = self.aUserID;
        copy.commentCount = self.commentCount;
        copy.timagesUrl = [self.timagesUrl copyWithZone:zone];
        copy.logoUrl = [self.logoUrl copyWithZone:zone];
        copy.questionContent = [self.questionContent copyWithZone:zone];
        copy.answerContent = [self.answerContent copyWithZone:zone];
        copy.collectCount = self.collectCount;
        copy.sortTime = [self.sortTime copyWithZone:zone];
        copy.times = [self.times copyWithZone:zone];
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.types = self.types;
        copy.counts = self.counts;
        copy.tUserID = [self.tUserID copyWithZone:zone];
        copy.tID = [self.tID copyWithZone:zone];
        copy.answerID = [self.answerID copyWithZone:zone];
    }
    
    return copy;
}


@end
