//
//  TaxPer.m
//
//  Created by IOS8  on 15/10/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "TaxPer.h"
#import "TaxData.h"


NSString *const kTaxPerCode = @"Code";
NSString *const kTaxPerMessage = @"Message";
NSString *const kTaxPerData = @"Data";


@interface TaxPer ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TaxPer

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kTaxPerCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kTaxPerMessage fromDictionary:dict];
    NSObject *receivedTaxData = [dict objectForKey:kTaxPerData];
    NSMutableArray *parsedTaxData = [NSMutableArray array];
    if ([receivedTaxData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedTaxData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedTaxData addObject:[TaxData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedTaxData isKindOfClass:[NSDictionary class]]) {
       [parsedTaxData addObject:[TaxData modelObjectWithDictionary:(NSDictionary *)receivedTaxData]];
    }

    self.data = [NSArray arrayWithArray:parsedTaxData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kTaxPerCode];
    [mutableDict setValue:self.message forKey:kTaxPerMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kTaxPerData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kTaxPerCode];
    self.message = [aDecoder decodeObjectForKey:kTaxPerMessage];
    self.data = [aDecoder decodeObjectForKey:kTaxPerData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kTaxPerCode];
    [aCoder encodeObject:_message forKey:kTaxPerMessage];
    [aCoder encodeObject:_data forKey:kTaxPerData];
}

- (id)copyWithZone:(NSZone *)zone
{
    TaxPer *copy = [[TaxPer alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
