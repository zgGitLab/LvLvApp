//
//  LabelViewController.m
//  LvLvApp
//
//  Created by hope on 15/11/29.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "LabelViewController.h"
#import "ItemView.h"
#import "HLableDataModels .h"
#import "LDataModels.h"

@interface LabelViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArr;//推荐标签
@property (nonatomic,strong) UISearchBar *searchBar;
@property (nonatomic,strong) UIView *labelView;
@property (nonatomic,copy) NSString *labelName;//传值过来的标签的名字
@property (nonatomic,strong) UIView *selectedLabel;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) BOOL isCommentFlag;//是否显示推荐标签

@end

@implementation LabelViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isCommentFlag = YES;
    self.labelView = [[UIView alloc]init];
    self.selectedLabel = [[UIView alloc]init];
    _labelViewsheight = 44;
    [self.navigationController.navigationBar setBackgroundColor:[UIColor redColor]];
    self.view.backgroundColor = [UIColor whiteColor];

    UIBarButtonItem *butItem =[[UIBarButtonItem alloc] initWithTitle:@""
                                              style: UIBarButtonItemStylePlain
                                             target:self
                                             action:@selector(returnButton)];
    [butItem setImage:[UIImage imageNamed:@"返回"]];
    butItem.tintColor=[UIColor colorWithRed:213.0/255.0 green:213.0/255.0 blue:213.0/255.0 alpha:1];
    
    self.navigationItem.leftBarButtonItem=butItem;
    
    UIBarButtonItem * butItem1=[[UIBarButtonItem alloc] initWithTitle:@"完成"
                                              style: UIBarButtonItemStylePlain
                                             target:self
                                             action:@selector(returnButton)];
    [butItem1 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                      [UIFont fontWithName:@"Helvetica-Bold" size:17.0], NSFontAttributeName,
                                      [UIColor colorWithRed:73.0/255.0 green:174.0/255.0 blue:242.0/255.0 alpha:1], NSForegroundColorAttributeName,
                                      nil]
                            forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=butItem1;
    self.navigationController.navigationBar.barTintColor = setColor(234, 235, 236);
    
    [self addObserver:self forKeyPath:@"labelViewsheight" options:NSKeyValueObservingOptionNew context:NULL];
    //创建tableview
    [self createDataSource];
    [self createTableView];
    [self createSearchBar];
    [self createLabelView];

    
}

//当属性的值发生变化时，自动调用此方法
/* listen for changes to the earthquake list coming from our app delegate. */
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
//    [self performSelectorOnMainThread:@selector(refreshUI) withObject:nil waitUntilDone:YES];
//    if (_num != _labelViewsheight) {
        [self.tableView reloadData];
//    }
    
}

#pragma mark --定制键盘上面的view
- (UIView *)keyboardView{
    //自定义键盘上部的view
    UIView *keyboardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 36)];
    keyboardView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    UIButton *at = [UIButton buttonWithType:UIButtonTypeSystem];
    at.frame = CGRectMake(15, 5, 26, 26);
    [at setImage: [[UIImage imageNamed:@"收键盘"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [at addTarget:self action:@selector(collectKeyBoard) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:at];
    return keyboardView;
}

- (void)collectKeyBoard{
    [self.searchBar resignFirstResponder];
}

#pragma mark --请求推荐标签的数据
- (void)getData{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *url = [NSString stringWithFormat:kRecommendUrl,K_IP];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.isCommentFlag = YES;
        NSMutableString *str = [[NSMutableString alloc]initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",str);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
        NSArray *array = [dict objectForKey:@"Data"];
        [self.dataArr removeAllObjects];
        [self.dataArr addObjectsFromArray:array];
        [self performSelectorOnMainThread:@selector(refreshUI) withObject:nil waitUntilDone:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"====error%@",error);
    }];
}

- (void)refreshUI{
    [self.tableView reloadData];
}
//添加标签
- (void)addLabelWithString:(NSMutableDictionary *)dict{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer  =[AFJSONResponseSerializer serializer];
    NSString  *url = [NSString stringWithFormat:kAddLabelUrl,K_IP];
    
    [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableString *str = [[NSMutableString alloc]initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",str);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
        if ([dict[@"Code"] integerValue] == 200) {
            if (self.refreshEvents) {
                self.refreshEvents();
            }
        }else{
            NSLog(@"添加标签失败！");
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error:%@",error);
    }];
}


#pragma mark --模糊查询标签
- (void)getLabelInfo:(NSMutableDictionary *)dict{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer  =[AFJSONResponseSerializer serializer];
    NSString  *url = [NSString stringWithFormat:kGettaginfo,K_IP];
    
    [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.isCommentFlag = NO;
        NSMutableString *str = [[NSMutableString alloc]initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",str);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
        LBBaseClass *label = [[LBBaseClass alloc]initWithDictionary:dict];
        NSArray *labelArr = label.data;
        [self.dataArr removeAllObjects];
        for (LBData *str in labelArr) {
            [self.dataArr addObject:str.tagName];
        }
        [self performSelector:@selector(refreshUI) withObject:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error:%@",error);
    }];
}


#pragma mark --创建labelviews
- (void)createLabelViews{
    ItemView *view = [[ItemView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 100)];

    view.itemHeith = 20;
    view.itemArray = [[SingeData sharedInstance]labelDataArray];
    __weak typeof(ItemView) *weakItem = view;
    view.target = self;
    view.selecter = @selector(deleteBtn:);
    [view ItemViewWithBlock:^(id obj) {
        CGFloat heith = [obj integerValue];
        weakItem.frame = CGRectMake(CGRectGetMinX(weakItem.frame), CGRectGetMinY(weakItem.frame), CGRectGetWidth(weakItem.frame), heith + 10);
        if (self.labelViewsheight != heith) {
            self.labelViewsheight = heith;
        }
        
    }];
    self.selectedLabel = view;

}


#pragma mark --创建searchBar
- (void)createSearchBar{
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(80, 10, kWidth-100, 24)];
    self.searchBar.placeholder = @"搜索并添加标签";
    self.searchBar.delegate = self;
    self.navigationItem.titleView =self.searchBar;
    self.searchBar.inputAccessoryView = [self keyboardView];
}


#pragma mark 创建数据源
- (void)createDataSource
{
    self.dataArr = [[NSMutableArray alloc]init];
    [self getData];
}

#pragma mark 创建tableview
- (void)createTableView{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-64) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
}

#pragma mark --tableview的delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        if ([[SingeData sharedInstance] labelDataArray].count == 0) {
            return 0;
        }
        return 1;
    }else{
        return  self.dataArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    if (indexPath.section == 1) {
        cell.textLabel.text = self.dataArr[indexPath.row];
        cell.textLabel.textColor = setColor(89, 87, 87);
        cell.textLabel.font = [UIFont systemFontOfSize:15];
    }else{
        //当标签数组不为零的时候创建标签
        if ([[SingeData sharedInstance] labelDataArray].count != 0) {
            
            [self.selectedLabel removeFromSuperview];
            [self createLabelViews];
            [cell.contentView addSubview:self.selectedLabel];
        }

    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        [self createLabelViews];
        return _labelViewsheight;
    }else{
        return 44;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 44)];
    headView.backgroundColor = [UIColor whiteColor];
    if (section == 0) {
        UIImageView *lableimage=[[UIImageView alloc]initWithFrame:CGRectMake(12,15, 14, 14)];
        [lableimage setImage:[UIImage imageNamed:@"已选择"]];
        [headView addSubview:lableimage];
        
        UILabel *label  = [[UILabel alloc]initWithFrame:CGRectMake(30, 10, 60, 24)];
        label.text = @"已添加";
        label.textColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1];
        label.font = [UIFont systemFontOfSize:14.0f];
        [headView addSubview:label];
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 43, kWidth, 0.5)];
        line.backgroundColor = [UIColor colorWithRed:188.0/255.0 green:186.0/255.0 blue:193.0/255.0 alpha:0.8];
        [headView addSubview:line];

    }else{
        UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(12,15, 14, 14)];
        [image setImage:[UIImage imageNamed:@"标签"]];
        [headView addSubview:image];
        
        UIView *lines = [[UIView alloc]initWithFrame:CGRectMake(0, -1, kWidth, 0.5)];
        lines.backgroundColor = [UIColor colorWithRed:188.0/255.0 green:186.0/255.0 blue:193.0/255.0 alpha:0.8];
        [headView addSubview:lines];
        
        UILabel *label  = [[UILabel alloc]initWithFrame:CGRectMake(30, 10, 60, 24)];
        
        label.text = self.isCommentFlag?@"推荐标签":@"搜索结果";
        label.textColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1];
        label.font = [UIFont systemFontOfSize:14.0f];
        [headView addSubview:label];

        UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 43, kWidth, 0.5)];
        [line setBackgroundColor: [UIColor colorWithRed:188.0/255.0 green:186.0/255.0 blue:193.0/255.0 alpha:0.8]];
        [headView addSubview:line];
        [headView addSubview:label];
    }
    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        NSMutableArray *array = [[SingeData sharedInstance]labelDataArray];
        [tableView cellForRowAtIndexPath:indexPath].selected = NO;
        for (int i = 0; i < array.count; i++) {
            if ([array[i] isEqualToString:[tableView cellForRowAtIndexPath:indexPath].textLabel.text]) {
                return;
            }
        }
        if (array.count <5) {
            [[[SingeData sharedInstance]labelDataArray] addObject:self.dataArr[indexPath.row]];
//            [self createLabelViews];
            [self performSelector:@selector(refreshUI) withObject:nil];
        
        }else {
            NSString *str;
            if ([self.searchBar.text isEqualToString: @""]) {
                str = @"标签名称为空";
            }
            if (array.count >=5) {
                str = @"标签数量不能多于5个";
            }
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:str message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
        

    }else{
        [tableView cellForRowAtIndexPath:indexPath].selected = NO;
    }
}

#pragma mark --searchbar的delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
//    [self.view addSubview:self.labelView];
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
//    [self.labelView removeFromSuperview];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ((searchText != nil)||(searchText.length > 0)) {
        if(searchText.length ==0){
            [self getData];
        }else{
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:kUserID forKey:@"UserID"];
            [dict setObject:searchBar.text forKey:@"TopicName"];
            [self getLabelInfo:dict];
        }
    }
    
    
}


#pragma mark --创建labelView
- (void)createLabelView{
    self.labelView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    self.labelView.backgroundColor = [UIColor whiteColor];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.tag = 1000;
    button.frame = CGRectMake(30, 10, kWidth, 24);
    [button setTitle:@"添加新标签: " forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [button addTarget:self action:@selector(addClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"长线"]];
    imageView.frame = CGRectMake(0, 45, kWidth, 1);
    [self.labelView addSubview:imageView];
    [self.labelView addSubview:button];
}

- (void)addClick:(UIButton *)btn{
    NSMutableArray *array = [SingeData sharedInstance].labelDataArray;
    if ((![self.searchBar.text isEqualToString: @""])&&(array.count < 5)) {
        
        [[SingeData sharedInstance].labelDataArray addObject:self.searchBar.text];
        self.searchBar.text = @"";
        UIButton *btn = [self.view viewWithTag:1000];
        [btn setTitle:@"添加新标签: " forState:UIControlStateNormal];
        [self.labelView removeFromSuperview];
        [self createLabelViews];
        [self.searchBar resignFirstResponder];
        [self performSelectorOnMainThread:@selector(refreshUI) withObject:nil waitUntilDone:NO];
        
    }else{
        NSString *str;
        if ([self.searchBar.text isEqualToString: @""]) {
            str = @"标签名称为空";
        }
        if (array.count >=5) {
            str = @"标签数量不能多于5个";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:str message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}



- (void)returnButton
{
    if (self.refreshEvents) {
        NSString *string = [[NSString alloc]init];
        NSMutableArray *labelArray = [SingeData sharedInstance].labelDataArray;
        if (labelArray.count == 1) {
            string = [NSString stringWithFormat:@"%@",labelArray[0]];
        }else if(labelArray.count == 2){
            string =[NSString stringWithFormat:@"%@|%@",labelArray[0],labelArray[1]];
        }else if(labelArray.count == 3){
            string =[NSString stringWithFormat:@"%@|%@|%@",labelArray[0],labelArray[1],labelArray[2]];
        }else if(labelArray.count == 4){
            string =[NSString stringWithFormat:@"%@|%@|%@|%@",labelArray[0],labelArray[1],labelArray[2],labelArray[3]];
        }else if(labelArray.count == 5){
            string =[NSString stringWithFormat:@"%@|%@|%@|%@|%@",labelArray[0],labelArray[1],labelArray[2],labelArray[3],labelArray[4]];
        }
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:kUserID forKey:@"UserID"];
        [dict setObject:string forKey:@"TopicName"];
        [dict setObject:self.tID forKey:@"TID"];
        [self addLabelWithString:dict];
        
        
    }
    if (self.refreshEvent) {
        self.refreshEvent();
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//点击标签删除事件
- (void)deleteBtn:(NSArray *)array{

    for (UIView *view in self.view.subviews) {
        if (view.tag == 888) {
            [view removeFromSuperview];
        }
    }
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.tag = 888;
    [button setBackgroundImage:[[UIImage imageNamed:@"Topic_delete"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    NSString *str1 = array[0];
    self.labelName= array[1];
    [button addTarget:self action:@selector(del:) forControlEvents:UIControlEventTouchUpInside];
    NSRange range = [str1 rangeOfString:@","];
    CGFloat x = [[str1 substringToIndex:(range.location+range.length-1)] integerValue];
    CGFloat y = [[str1 substringFromIndex:(range.location+range.length)] integerValue];

    button.frame = CGRectMake(x, y, 63, 45);
    [self.view addSubview:button];

}

- (void)del:(UIButton *)btn{
    btn.tag = 999;
    NSMutableArray *array = [SingeData sharedInstance].labelDataArray;
    
    for (int i = 0; i < array.count; i ++) {
        if ([array[i] isEqualToString:self.labelName]) {
            [[SingeData sharedInstance].labelDataArray removeObject:array[i]];
        }
    }
    [btn removeFromSuperview];
    [self createLabelViews];
    [self performSelectorOnMainThread:@selector(refreshUI) withObject:nil waitUntilDone:NO];
    
}



- (void)viewWillDisappear:(BOOL)animated{
    [self removeObserver:self forKeyPath:@"labelViewsheight"];
}

@end
