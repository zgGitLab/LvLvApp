//
//  S_List.h
//
//  Created by IOS8  on 15/11/11
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface S_List : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double listIdentifier;
@property (nonatomic, strong) NSString *judgeDate;
@property (nonatomic, strong) NSString *titleHighLighter;
@property (nonatomic, strong) NSString *abstract;
@property (nonatomic, assign) double courtId;
@property (nonatomic, strong) NSString *court;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *character;
@property (nonatomic, strong) NSString *content;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
