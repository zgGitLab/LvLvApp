//
//  FristPage.m
//
//  Created by IOS8  on 15/11/3
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "FristPage.h"
#import "FristData.h"


NSString *const kFristPageCode = @"Code";
NSString *const kFristPageMessage = @"Message";
NSString *const kFristPageData = @"Data";


@interface FristPage ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FristPage

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kFristPageCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kFristPageMessage fromDictionary:dict];
    NSObject *receivedFristData = [dict objectForKey:kFristPageData];
        
        
        
        //NSMutableString *responseString = [NSMutableString stringWithString:[operation responseString]];
        
        //NSData *jsonData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        
     
        
        
    NSMutableArray *parsedFristData = [NSMutableArray array];
    if ([receivedFristData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedFristData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedFristData addObject:[FristData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedFristData isKindOfClass:[NSDictionary class]]) {
       [parsedFristData addObject:[FristData modelObjectWithDictionary:(NSDictionary *)receivedFristData]];
    }

    self.data = [NSArray arrayWithArray:parsedFristData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kFristPageCode];
    [mutableDict setValue:self.message forKey:kFristPageMessage];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kFristPageData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kFristPageCode];
    self.message = [aDecoder decodeObjectForKey:kFristPageMessage];
    self.data = [aDecoder decodeObjectForKey:kFristPageData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kFristPageCode];
    [aCoder encodeObject:_message forKey:kFristPageMessage];
    [aCoder encodeObject:_data forKey:kFristPageData];
}

- (id)copyWithZone:(NSZone *)zone
{
    FristPage *copy = [[FristPage alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
