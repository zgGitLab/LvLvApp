//
//  delayModel.m
//  Tools
//
//  Created by IOS8 on 15/9/15.
//  Copyright (c) 2015年 IOS8. All rights reserved.
//

#import "delayModel.h"

@implementation delayModel


-  (void)computeResult
{
    // 一般利息天数及利息；
   int days = (int)[self computeday:self.startDate endDay:self.effectDate];
    self.soInterestDays = days + self.contractDate;
    self.soInterest = self.soInterestDays * self.liLv*self.money;
    
    // 延时利息天数，及利息
    self.delayInterestDays = (int)[self computeday:self.effectDate endDay:self.finshDate] - self.contractDate;
    NSLog(@"%d",self.delayInterestDays);
    if (self.delayInterestDays<0) {
        self.delayInterest = 0.00;
        self.soInterest = self.liLv*self.money*(int)[self computeday:self.effectDate endDay:self.finshDate];
        NSLog(@"%d",(int)[self computeday:self.effectDate endDay:self.finshDate]);
    }else{
        self.delayInterest = self.delayInterestDays * self.money * self.liLv + self.delayInterestDays * 0.000175*self.money;
    }
//    self.delayInterest = self.delayInterestDays * self.money * self.liLv + self.delayInterestDays * 0.000175*self.money;
    
    // 总利和，及本利和；
    self.countInterest = self.soInterest + self.delayInterest;
    self.sum = self.money + self.countInterest;
    NSLog(@"???????%d,??????%d",self.soInterestDays,self.delayInterestDays);
}

// 计算天数，机利息
- (NSInteger)computeday:(NSString *)startday endDay:(NSString *)endday{

    startday = [NSString stringWithFormat:@"%@ %d:%d:%d",startday,00,00,00];
    endday = [NSString stringWithFormat:@"%@ %d:%d:%d",endday,23,59,59];
    NSDateFormatter *format=[[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *fromdate=[format dateFromString:startday];
    NSTimeZone *fromzone = [NSTimeZone systemTimeZone];
    NSInteger frominterval = [fromzone secondsFromGMTForDate: fromdate];
    NSDate *fromDate = [fromdate  dateByAddingTimeInterval: frominterval];
    
    NSDate *todate=[format dateFromString:endday];
    NSTimeZone *tozone = [NSTimeZone systemTimeZone];
    NSInteger tointerval = [tozone secondsFromGMTForDate: todate];
    NSDate *toDate = [todate  dateByAddingTimeInterval: tointerval];
    
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSUInteger unitFlags = NSCalendarUnitDay;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:fromDate toDate:toDate options:0];
    NSInteger days = [components day];
    return days;
}

@end
