//
//  LabelViewController.h
//  LvLvApp
//
//  Created by hope on 15/11/29.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^refreshLabel)(void);
@interface LabelViewController : UIViewController
@property (nonatomic,copy) refreshLabel refreshEvent;
@property (nonatomic,copy) refreshLabel refreshEvents;
@property (nonatomic,assign) CGFloat labelViewsheight;
@property (nonatomic,copy) NSString *tID;
@property (nonatomic,copy) NSString *userID;
@end
