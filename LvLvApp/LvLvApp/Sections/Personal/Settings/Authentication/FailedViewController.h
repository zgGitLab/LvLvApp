//
//  FailedViewController.h
//  LvLvApp
//
//  Created by 赵俊杰 on 16/5/26.
//  Copyright © 2016年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FailedViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *bgScrollView;

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *lable;
@property (weak, nonatomic) IBOutlet UITextView *reasonContent;//未通过原因

@property (weak, nonatomic) IBOutlet UIButton *applyBtn;

- (IBAction)applyBtnClick:(UIButton *)sender;//重新申请事件


@end
