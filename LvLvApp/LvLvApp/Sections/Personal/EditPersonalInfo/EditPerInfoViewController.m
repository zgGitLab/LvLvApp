//
//  EditPerInfoViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/20.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "EditPerInfoViewController.h"
#import "PerPickImageView.h"
#import "EditNicNameViewController.h"
#import "EditJobInfoViewController.h"
#import "EditeducationInfoViewController.h"
#import "S_PerBaseInfoModatil.h" // 用户基本信息

@interface EditPerInfoViewController ()<PerPickViewDelegat>
{
    S_PerBaseInfoBaseClass *s_BaseInfo;
    UIImageView *perIcon;
    UILabel *nicName;
    UILabel *perInfoMessage;
    UILabel *sex;
    UILabel *phoneNumber;
    UIView *headVi;
}
@end

@implementation EditPerInfoViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"编辑个人资料";
    headVi = [[NSBundle mainBundle] loadNibNamed:@"EditPerInfoHeaderView" owner:self options:nil][0];
    self.myTable.tableHeaderView = headVi;
    self.myTable.backgroundColor = [UIColor colorWithRed:235/255.0 green:236/255.0  blue:237/255.0  alpha:1];
    self.myTable.separatorInset = UIEdgeInsetsMake(0, -10, 0, 10);
    [self createUI];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    __weak EditPerInfoViewController *VC= self;
    [RequestManager PerBaseInfo:[self getUserId] andBlock:^(S_PerBaseInfoBaseClass *s_PerModal, NSString *message) {
        [VC performSelectorOnMainThread:@selector(refreshData:) withObject:s_PerModal waitUntilDone:YES];
    }];
   
}


- (void)refreshData:(S_PerBaseInfoBaseClass *)modal
{
    s_BaseInfo = modal;
    [perIcon sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,s_BaseInfo.imageUrl]] placeholderImage:[UIImage imageNamed:@"头像"]];
    perIcon.layer.cornerRadius = 16.5;
    perIcon.clipsToBounds = YES;
    nicName.text = s_BaseInfo.nickName;
    perInfoMessage.text = s_BaseInfo.introduction;
    sex.text = s_BaseInfo.gender;
    phoneNumber.text = s_BaseInfo.telPhone;
    [self.myTable reloadData];
}


- (NSString *)getUserId
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"users"]];
}

- (void)createUI
{
    perIcon = (UIImageView *)[headVi viewWithTag:11];
    nicName = (UILabel *)[headVi viewWithTag:20];
    perInfoMessage = (UILabel *)[headVi viewWithTag:21];
    sex = (UILabel *)[headVi viewWithTag:22];
    phoneNumber = (UILabel *)[headVi viewWithTag:23];
    
    UIButton *changeIconBtn = (UIButton *)[headVi viewWithTag:10];
    [changeIconBtn addTarget:self action:@selector(changeIcon:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *editNicNameBtn = (UIButton *)[headVi viewWithTag:12];
    [editNicNameBtn addTarget:self action:@selector(changeIcon:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *editInfoBtn = (UIButton *)[headVi viewWithTag:13];
    [editInfoBtn addTarget:self action:@selector(changeIcon:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *editSexBtn = (UIButton *)[headVi viewWithTag:14];
    [editSexBtn addTarget:self action:@selector(changeIcon:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *edittelephoneBtn = (UIButton *)[headVi viewWithTag:15];
    [edittelephoneBtn addTarget:self action:@selector(changeIcon:) forControlEvents:UIControlEventTouchUpInside];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return s_BaseInfo.jobList.count;
    }
    return s_BaseInfo.eductList.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
     UIView *bgView = [[UIView alloc] init];
     bgView.backgroundColor = [UIColor colorWithRed:235/255.0 green:236/255.0 blue:237/255.0 alpha:1];
    UIView *vi = [[UIView alloc] initWithFrame:CGRectMake(0, 10, SIZE.width, 50)];
    vi.backgroundColor= [UIColor whiteColor];
    UIImageView *imagVI = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 20, 20)];
    imagVI.image = [UIImage imageNamed:@"添加"];
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, SIZE.width-40, 50)];
    titleLab.text = section == 0?@"添加工作经历":@"添加教育经历";
    titleLab.font = [UIFont systemFontOfSize:15];
    titleLab.textColor = [UIColor colorWithRed:89/255.0 green:87/255.0 blue:87/255.0 alpha:1];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, SIZE.width, 50);
    button.tag = section+1;
    [button addTarget:self action:@selector(titBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [vi addSubview:imagVI];
    [vi addSubview:titleLab];
    [vi addSubview:button];
    [bgView addSubview:vi];

    return bgView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        UIImageView *images = [[UIImageView alloc] initWithFrame:CGRectMake(SIZE.width-19, 12, 9, 17)];
        images.image = [UIImage imageNamed:@"箭头"];
        [cell.contentView addSubview:images];
        cell.textLabel.textColor =setColor(158, 158, 159);
        cell.textLabel.font = [UIFont systemFontOfSize:14];
    }
    if (indexPath.section == 0) {
        S_PerBaseInfoJobList *jobModal = s_BaseInfo.jobList[indexPath.row];
       cell.textLabel.text = jobModal.company;
    }else{
        S_PerBaseInfoEductList *eduModal = s_BaseInfo.eductList[indexPath.row];
        cell.textLabel.text = eduModal.university;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 编辑删除工作经历和教育经历；
    if (indexPath.section == 0) {
        S_PerBaseInfoJobList *jobModal = s_BaseInfo.jobList[indexPath.row];
        EditJobInfoViewController *editJobInfoVC = [[EditJobInfoViewController alloc] init];
        editJobInfoVC.ishide = NO;
        editJobInfoVC.jobModal = jobModal;
        [self.navigationController pushViewController:editJobInfoVC animated:YES];
    }else{
        S_PerBaseInfoEductList *eduModal = s_BaseInfo.eductList[indexPath.row];
        EditeducationInfoViewController *editeducationInfoVC = [[EditeducationInfoViewController alloc] init];
        editeducationInfoVC.ishide = NO;
        editeducationInfoVC.eduModal = eduModal;
        [self.navigationController pushViewController:editeducationInfoVC animated:YES];
    }
    

}

- (void)titBtnClick:(UIButton *)sender
{
    if (sender.tag == 1) {
         NSLog(@"添加工作经历");
        EditJobInfoViewController *editJobInfoVC = [[EditJobInfoViewController alloc] init];
        editJobInfoVC.ishide = YES;
        [self.navigationController pushViewController:editJobInfoVC animated:YES];
    }else{
        
        EditeducationInfoViewController *editeducationInfoVC = [[EditeducationInfoViewController alloc] init];
        editeducationInfoVC.ishide = YES;
        [self.navigationController pushViewController:editeducationInfoVC animated:YES];
         NSLog(@"添加教育经历");
    }
   
}

#pragma mark 修改头像
- (void)changeIcon:(UIButton *)btn
{
    switch (btn.tag) {
        case 10:
        {   // 编辑头像
            PerPickImageView *perPickVi = [[PerPickImageView alloc] initWithId:1];
            perPickVi.delegate = self;
            [perPickVi showInView];

        }
            break;
        case 12:
        {
            EditNicNameViewController *editNicNameVC = [[EditNicNameViewController alloc] init];
            editNicNameVC.concentString = nicName.text;
            editNicNameVC.ID = 1;
            editNicNameVC.titleString = @"编辑昵称";
            [self.navigationController pushViewController:editNicNameVC animated:YES];
            
        }
            break;
        case 13:
        {
            EditNicNameViewController *aboutVC = [[EditNicNameViewController alloc] init];
            aboutVC.concentString = perInfoMessage.text;
            aboutVC.ID = 2;
            aboutVC.titleString = @"编辑简介";
            [self.navigationController pushViewController:aboutVC animated:YES];
            
        }
            break;
        case 14:
        {   // 编辑性别
            PerPickImageView *perSexVi = [[PerPickImageView alloc] initWithId:2];
            perSexVi.delegate = self;
            [perSexVi showInView];
            
        }
            break;
        case 15:
        {
            EditNicNameViewController *phoneVC = [[EditNicNameViewController alloc] init];
            phoneVC.concentString = phoneNumber.text;
            phoneVC.ID = 3;
            phoneVC.titleString = @"编辑联系方式";
            [self.navigationController pushViewController:phoneVC animated:YES];
            
        }
            break;

        default:
            break;
    }
   
}

#pragma mark PerPickViewDelegat
- (void)jumpOtherView:(UIImagePickerController *)object
{
    object.allowsEditing = YES;
    // 点击按钮跳转到能够利用的图片来源
    [self presentViewController:object animated:YES completion:nil];
}

- (void)selectImage:(id)icon
{
    if ([icon isKindOfClass:[UIImage class]]) {
        NSLog(@"头像");
        perIcon.image = (UIImage *)icon;
        [RequestManager chagneIcomAndSex:[self getUserId] andIcon:perIcon.image andSex:nil andBlock:^(NSString *message) {
            
        }];
    }else{
        // 性别
        sex.text = (NSString *)icon;
        
        [RequestManager chagneIcomAndSex:[self getUserId] andIcon:nil andSex:sex.text andBlock:^(NSString *message) {
            
        }];
    }
    
}


@end
