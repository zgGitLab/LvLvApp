//
//  M_lableBaseClass.m
//
//  Created by hope  on 15/12/28
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "M_lableBaseClass.h"
#import "M_lableData.h"


NSString *const kM_lableBaseClassCode = @"Code";
NSString *const kM_lableBaseClassMessage = @"Message";
NSString *const kM_lableBaseClassData = @"Data";


@interface M_lableBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation M_lableBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kM_lableBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kM_lableBaseClassMessage fromDictionary:dict];
            self.data = [M_lableData modelObjectWithDictionary:[dict objectForKey:kM_lableBaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kM_lableBaseClassCode];
    [mutableDict setValue:self.message forKey:kM_lableBaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kM_lableBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kM_lableBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kM_lableBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kM_lableBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kM_lableBaseClassCode];
    [aCoder encodeObject:_message forKey:kM_lableBaseClassMessage];
    [aCoder encodeObject:_data forKey:kM_lableBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    M_lableBaseClass *copy = [[M_lableBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
