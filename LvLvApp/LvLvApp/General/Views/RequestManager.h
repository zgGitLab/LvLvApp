//
//  RequestManager.h
//  LvLvApp
//
//  Created by IOS8 on 15/10/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S_PerMain_BaseClass.h"
#import "S_MyMainPageBaseClass.h"
#import "Push_AllStatusBaseClass.h"
@class S_PerBaseInfoBaseClass;
@interface RequestManager : NSObject

// 人民人行数据
+ (void)requestBankDataWithUrl:(NSString *)url andBlock:(void(^)(NSString *message))block;

// 个税数据
+ (void)requestPerTaxDataWithUrl:(NSString *)url;
// 个税起征点
+ (void)requestTaxThresholdWithUrl:(NSString *)url;
// 统计案例和发条的总数以及新增数量
+(void)statCount:(void(^)(CountModal *modal))block;

// 检索首页最新法条和案例的6条数据
+(void)firstPageDataWithPageIndex:(NSString *)pageIndex andBlock:(void(^)(NSArray *dataArr,NSString *info))block;

// 案例详情页面
+(void)detailPageDataWithUrl:(double)ID andUserID:(NSString *)UserID andBlock:(void(^)(S_CaseDetailBaseClass *caseModal,NSString *info))block;

// 法条详情页面
+(void)lawDetailPageDataWithUrl:(double)ID andUserID:(NSString *)UserID andBlock:(void(^)(S_LawDetailData *lwaModal,NSString *info))block;

// 案例列表数据
+(void)caseTableData:(NSString *)str andOrder:(int)order andCurrtPage:(int)currtPage andCount:(int)count andBlock:(void(^)(S_LISTCase *caseModal,NSString *info))block;


// 法条列表数据
+(void)lawTableData:(NSString *)str andOrder:(int)order andCurrtPage:(int)currtPage andCount:(int)count andBlock:(void(^)(S_LawListBaseClass *listModal,NSString *info))block;

// 法条收藏
+(void)collectionData:(NSString *)userID andID:(int)ID andIsYes:(int)category andIsCollect:(int)collect andBlock:(void(^)(NSString *info))block;
// 案例收藏
+(void)collectionCaseData:(NSString *)userID andID:(int)ID andIsYes:(int)category andIsCollect:(int)collect andBlock:(void(^)(NSString *info))block;
// 个人主页
+(void)mainPerInfo:(NSString *)userID andBlock:(void(^)(S_PerMain_BaseClass *s_PerModal,NSString *message))block;

// 用户基本信息
+(void)PerBaseInfo:(NSString *)userID andBlock:(void(^)(S_PerBaseInfoBaseClass *s_PerModal,NSString *message))block;

// 编辑昵称；
+(void)editNIcName:(NSString *)userID andNicName:(NSString *)nicName andBlock:(void(^)(NSString *message))block;

// 编辑简介；
+(void)editInfoMessage:(NSString *)userID andInfoMessage:(NSString *)message andBlock:(void(^)(NSString *message))block;

// 编辑联系方式；
+(void)editphoneNumber:(NSString *)userID andphoneNumber:(NSString *)phoneNumber andBlock:(void(^)(NSString *message))block;
// 上传头像和昵称；
+(void)chagneIcomAndSex:(NSString *)UserId andIcon:(UIImage *)icon andSex:(NSString *)sexStr andBlock:(void(^)(NSString *message))block;

// 添加工作经历
+(void)addJobMessageWithUser:(NSString *)UserID andCompanyName:(NSString *)companyName andjobName:(NSString *)jobName andJobTime:(NSString *)jobTime andArea:(NSString *)area andBlock:(void(^)(NSString *message))block;

// 删除工作经历
+(void)removewJobMessageWithID:(NSString *)Id andBlock:(void(^)(NSString *message))block;

// 修改工作经历
+(void)changeJobMessageWithUser:(NSString *)ID andCompanyName:(NSString *)companyName andjobName:(NSString *)jobName andJobTime:(NSString *)jobTime andArea:(NSString *)area andBlock:(void(^)(NSString *message))block;

//http://192.168.1.18/api/User/addedcation
// 添加教育经历
+(void)addEduMessageWithUser:(NSString *)UserID andSchoolName:(NSString *)schoolName andstartTime:(NSString *)startTime andInstitute:(NSString *)institute andBlock:(void(^)(NSString *message))block;

// 修改教育经历
+(void)changeEduMessageWithUser:(NSString *)ID andSchoolName:(NSString *)schoolName andstartTime:(NSString *)startTime andInstitute:(NSString *)institute andBlock:(void(^)(NSString *message))block;

// 删除教育经历
+(void)removeEduMessageWithUser:(NSString *)ID andBlock:(void(^)(NSString *message))block;

//http://192.168.1.18/api/User/homepage
// 我的主页
+(void)myMainPageWithUserID:(NSString *)userID andOtherID:(NSString *)otherID andIsMySelf:(BOOL)isMySelf andBlock:(void(^)(S_MyMainPageBaseClass *mainPageModal,NSString *message))block;

//个人主页的动态
+(void)perMainPageHotWithUserID:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(S_PerHotBaseClass *perPageHotModal,NSString *message))block;

// 我的回答
+(void)myAnserWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block;

// 用户关注列表
+(void)perConcernList:(NSString *)userID andBlock:(void(^)(NSArray *dataArr,NSString *message))block;

// 用户关注用户
+(void)perConcernOther:(NSString *)userID andOtherID:(NSString *)otherID andIsConcern:(NSString *)isConcern  andBlock:(void(^)(NSString *message))block;

// 我的提问
+(void)myAskWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block;

// 我的粉丝
+(void)myFansWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block;

// 通知列表
+(void)myNotficWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block;

// 私信列表
+(void)myPrivateLetterWithUser:(NSString *)userID andPageIndex:(NSString *)pageIndex andPageSize:(NSString *)pageSize andBlock:(void(^)(NSArray *dataArr,NSString *message))block;

// 搜索好友
+(void)searchFriend:(NSString *)userID andSearchStr:(NSString *)searchStr andBlock:(void(^)(NSArray *dataArr,NSString *message))block;

// 修改通知状态；
+(void)changeNotStates:(NSString *)states andBlock:(void(^)(NSString *message))block;

// 私信列表
+ (void)chatListWithRecID:(NSString *)recID andSendID:(NSString *)sendID andBlock:(void(^)(NSArray *dataArr,NSString *message))block;

// 发私信
+ (void)sendMessage:(NSString *)recID andSendID:(NSString *)sendID andTextConent:(NSString *)textConent andCategory:(NSString *)category andBlock:(void(^)(NSString *textConent,NSString *message))block;

// 我的未读私信数
+ (void)myMessageCountWithUserID:(NSString *)str andBlock:(void(^)(NSString *count))block;

// 删除私信
+ (void)deleteAllMessageWithSendID:(NSString *)sendID andRecID:(NSString *)recID andBlock:(void(^)(NSString *message))block;

// 添加反馈问题
+ (void)myQuestionWithUserID:(NSString *)userID andQuestionText:(NSString *)questionText andPhoneNumber:(NSString *)phoneNUmber andImageArr:(NSArray *)imageArr andBlock:(void(^)(NSString *message))block;

// 检查版本更新
+ (void)upadeversionWith:(NSString *)category andBlock:(void(^)(NSString *message))block;

// 消息推送设置所有状态
+ (void)getAllStatusWithUserID:(NSString *)userID andBlock:(void (^)(Push_AllStatusBaseClass *pushModal,NSString *message))block;

// 设置是否提醒
+ (void)setStatusWithUserID:(NSString *)userID andFocusNewAnwser:(int)focusNewAnwser andFocusMe:(int)FocusMe andPraiseMe:(int)PraiseMe andSendMe:(int)SendMe andCommentMe:(int)CommentMe;
@end

