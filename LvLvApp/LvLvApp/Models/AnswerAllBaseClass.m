//
//  AnswerAllBaseClass.m
//
//  Created by hope  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "AnswerAllBaseClass.h"
#import "AnswerAllData.h"


NSString *const kAnswerAllBaseClassCode = @"Code";
NSString *const kAnswerAllBaseClassMessage = @"Message";
NSString *const kAnswerAllBaseClassData = @"Data";


@interface AnswerAllBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AnswerAllBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kAnswerAllBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kAnswerAllBaseClassMessage fromDictionary:dict];
            self.data = [AnswerAllData modelObjectWithDictionary:[dict objectForKey:kAnswerAllBaseClassData]];

    }
    
    return self;
    
}



#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}





@end
