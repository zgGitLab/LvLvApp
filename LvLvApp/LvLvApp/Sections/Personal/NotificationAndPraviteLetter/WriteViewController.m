//
//  WriteViewController.m
//  LvLvApp
//
//  Created by IOS8 on 15/11/24.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import "WriteViewController.h"
#import "FriendTableViewCell.h"
#import "ChatViewController.h"
@interface WriteViewController ()
{
    UITextField *imputView;
    int pageIndex;
    NSMutableArray *friendArray;
    UITableView *resultTab;
    UITextField *searchText;
}
@end

@implementation WriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    resultTab = nil;
    self.view.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@""
                                   style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
    self.frindListTab.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.frindListTab.tableFooterView = [[UIView alloc] init];
    friendArray = [NSMutableArray array];
    self.frindListTab.separatorInset = UIEdgeInsetsMake(0, 0, 0, 10);
    pageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
    [self privateletterRequest:page];

    [self createTitleView];
    __weak WriteViewController *VC = self;
    // 上拉加载
    self.frindListTab.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 结束刷新
        [VC infinite];
    }];
    self.frindListTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 1;
        NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
        [VC privateletterRequest:page];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (void)privateletterRequest:(NSString *)pageIN
{
    __weak WriteViewController *VC = self;
    [RequestManager myPrivateLetterWithUser:self.userID andPageIndex:pageIN andPageSize:@"10" andBlock:^(NSArray *dataArr, NSString *message) {
        [VC.frindListTab.mj_header endRefreshing];
        [VC.frindListTab.mj_footer endRefreshing];
        if ([pageIN isEqualToString:@"1"]) {
            [friendArray removeAllObjects];
            friendArray = [NSMutableArray arrayWithArray:dataArr];
        }else{
            [friendArray addObjectsFromArray:dataArr];
        }
        [VC performSelectorOnMainThread:@selector(refreshAnserUI) withObject:nil waitUntilDone:YES];
        
    }];
}

- (void)infinite
{
    pageIndex += 1;
    NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
    [self privateletterRequest:page];
}
- (void)refreshAnserUI
{
    [self.frindListTab reloadData];
}

- (void)createTitleView
{
    UIView *titVi = [[UIView alloc] initWithFrame:CGRectMake(10, 30, SIZE.width-70, 26)];
    titVi.layer.cornerRadius = 5;
    titVi.clipsToBounds = YES;
    titVi.backgroundColor = [UIColor whiteColor];
    
    UIImageView *searchImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 4, 18, 18)];
    searchImage.image = [UIImage imageNamed:@"搜索图标"];
    [titVi addSubview:searchImage];
    
    searchText = [[UITextField alloc] initWithFrame:CGRectMake(30, 3, titVi.frame.size.width - 30, 20)];
    searchText.clearButtonMode = UITextFieldViewModeAlways;
    searchText.placeholder = @"请输入好友昵称";
    searchText.font = [UIFont systemFontOfSize:14];
    searchText.textColor = [UIColor colorWithRed:87/255.0 green:93/255.0 blue:102/255.0 alpha:1];
    searchText.delegate = self;
    searchText.returnKeyType = UIReturnKeyYahoo;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldChange:) name:UITextFieldTextDidChangeNotification object:nil];
    [titVi addSubview:searchText];
    self.navigationItem.titleView = titVi;
    
    [self.navigationItem setHidesBackButton:YES];
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 40, 15);
    [rightButton setTitle:@"取消" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithRed:73/255.0 green:154/255.0 blue:242/255.0 alpha:1] forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithRed:65/255.0 green:71/255.0 blue:83/255.0 alpha:1] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightBtn:) forControlEvents:UIControlEventTouchUpInside];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}

- (void)rightBtn:(UIButton *)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return friendArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"friendcell";
    FriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        
        cell = [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil][0];
        cell.titleIcon.layer.cornerRadius = 26.5;
        cell.titleIcon.clipsToBounds = YES;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.privateMessBtn.tag = indexPath.row;
    [cell.privateMessBtn addTarget:self action:@selector(privateMessBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    S_PrivateLetterData *letterList = friendArray[indexPath.row];
    [cell.titleIcon sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",K_IP,letterList.image]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"头像"]];
    cell.titleStr.text = letterList.nickName;
    cell.conStr.text = letterList.introduction;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [searchText resignFirstResponder];
    S_PrivateLetterData *letterList = friendArray[indexPath.row];
    ChatViewController *chatVC = [[ChatViewController alloc] init];
    chatVC.mySelfImage = self.mySelfImage;
    chatVC.otherID = [NSString stringWithFormat:@"%.0f",letterList.iDProperty];
    chatVC.userID = self.userID;
    chatVC.titleStr = letterList.nickName;
    [self.navigationController pushViewController:chatVC animated:YES];
   
}

- (void)privateMessBtnClick:(UIButton *)btn
{
    [searchText resignFirstResponder];
    S_PrivateLetterData *letterList = friendArray[btn.tag];
    ChatViewController *chatVC = [[ChatViewController alloc] init];
    chatVC.mySelfImage = self.mySelfImage;
    chatVC.otherID = [NSString stringWithFormat:@"%.0f",letterList.iDProperty];
    chatVC.userID = self.userID;
    chatVC.titleStr = letterList.nickName;
    [self.navigationController pushViewController:chatVC animated:YES];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UIView *resultView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, SIZE.height-64)];
    resultView.tag = 30;
    resultView.backgroundColor = [UIColor redColor];
    [self.view addSubview:resultView];
    resultTab = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SIZE.width, SIZE.height-64) style:UITableViewStylePlain];
    resultTab.delegate = self;
    resultTab.dataSource = self;
    resultTab.separatorInset = UIEdgeInsetsMake(0, 0, 0, 8);
    resultTab.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    resultTab.tableFooterView = [[UIView alloc] init];
    [resultView addSubview:resultTab];
    if (![textField.text isEqualToString:@""]) {
        [RequestManager searchFriend:self.userID andSearchStr:textField.text andBlock:^(NSArray *dataArr, NSString *message) {
            [friendArray removeAllObjects];
            friendArray = [NSMutableArray arrayWithArray:dataArr];
            [resultTab reloadData];
            if (friendArray.count == 0) {
                [MBProgressHUD showError:@"该用户暂时不是你的好友"];
            }
        }];
    }
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (resultTab != nil) {
        UIView *resultView = [self.view viewWithTag:30];
        [resultView removeFromSuperview];
        resultTab = nil;
        pageIndex = 1;
        NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
        [self privateletterRequest:page];
    }
    return YES;
}

- (void)textFieldChange:(NSNotification*)notifice{
    
    UITextField *field=[notifice object];
    if ([field.text isEqualToString:@""] && resultTab != nil) {
        UIView *resultView = [self.view viewWithTag:30];
        [resultView removeFromSuperview];
        resultTab = nil;
        pageIndex = 1;
        NSString *page = [NSString stringWithFormat:@"%d",pageIndex];
        [self privateletterRequest:page];
    }
}

- (void)dealloc
{
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UITextFieldTextDidChangeNotification" object:nil];
}

@end
