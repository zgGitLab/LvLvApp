//
//  mhelpeBaseClass.m
//
//  Created by hope  on 15/12/24
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "mhelpeBaseClass.h"
#import "mhelpeData.h"


NSString *const kmhelpeBaseClassCode = @"Code";
NSString *const kmhelpeBaseClassMessage = @"Message";
NSString *const kmhelpeBaseClassData = @"Data";


@interface mhelpeBaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation mhelpeBaseClass

@synthesize code = _code;
@synthesize message = _message;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.code = [[self objectOrNilForKey:kmhelpeBaseClassCode fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kmhelpeBaseClassMessage fromDictionary:dict];
            self.data = [mhelpeData modelObjectWithDictionary:[dict objectForKey:kmhelpeBaseClassData]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kmhelpeBaseClassCode];
    [mutableDict setValue:self.message forKey:kmhelpeBaseClassMessage];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kmhelpeBaseClassData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.code = [aDecoder decodeDoubleForKey:kmhelpeBaseClassCode];
    self.message = [aDecoder decodeObjectForKey:kmhelpeBaseClassMessage];
    self.data = [aDecoder decodeObjectForKey:kmhelpeBaseClassData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_code forKey:kmhelpeBaseClassCode];
    [aCoder encodeObject:_message forKey:kmhelpeBaseClassMessage];
    [aCoder encodeObject:_data forKey:kmhelpeBaseClassData];
}

- (id)copyWithZone:(NSZone *)zone
{
    mhelpeBaseClass *copy = [[mhelpeBaseClass alloc] init];
    
    if (copy) {

        copy.code = self.code;
        copy.message = [self.message copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
