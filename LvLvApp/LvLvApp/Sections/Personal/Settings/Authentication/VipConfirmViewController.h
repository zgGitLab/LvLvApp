//
//  VipConfirmViewController.h
//  LvLvApp
//
//  Created by hope on 15/12/16.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VipConfirmViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIButton *ConfirmApplyAlter;

@property (strong, nonatomic) IBOutlet UIScrollView *ConfirmScrollView;
@property (strong, nonatomic) IBOutlet UILabel *vipName;
@property (strong, nonatomic) IBOutlet UILabel *vipStatus;
@property (strong, nonatomic) IBOutlet UILabel *vipCompany;
@property (strong, nonatomic) IBOutlet UIImageView *vipPapers;

@property (strong, nonatomic) NSMutableArray *vipIDArray;
@end
