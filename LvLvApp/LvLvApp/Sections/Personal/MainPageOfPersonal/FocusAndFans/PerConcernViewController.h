//
//  PerConcernViewController.h
//  LvLvApp
//
//  Created by IOS8 on 15/11/26.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PerConcernViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) NSString *userID;
@property (weak, nonatomic) IBOutlet UITableView *concernTab;
@property (strong, nonatomic) NSString *mySelfImage;
@end
