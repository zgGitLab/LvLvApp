//
//  S_LawListSearch.m
//
//  Created by IOS8  on 15/11/18
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "S_LawListSearch.h"


NSString *const kS_LawListSearchName = @"name";
NSString *const kS_LawListSearchCount = @"count";
NSString *const kS_LawListSearchType = @"type";


@interface S_LawListSearch ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation S_LawListSearch

@synthesize name = _name;
@synthesize count = _count;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kS_LawListSearchName fromDictionary:dict];
            self.count = [[self objectOrNilForKey:kS_LawListSearchCount fromDictionary:dict] doubleValue];
            self.type = [[self objectOrNilForKey:kS_LawListSearchType fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kS_LawListSearchName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.count] forKey:kS_LawListSearchCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kS_LawListSearchType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kS_LawListSearchName];
    self.count = [aDecoder decodeDoubleForKey:kS_LawListSearchCount];
    self.type = [aDecoder decodeDoubleForKey:kS_LawListSearchType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_name forKey:kS_LawListSearchName];
    [aCoder encodeDouble:_count forKey:kS_LawListSearchCount];
    [aCoder encodeDouble:_type forKey:kS_LawListSearchType];
}

- (id)copyWithZone:(NSZone *)zone
{
    S_LawListSearch *copy = [[S_LawListSearch alloc] init];
    
    if (copy) {

        copy.name = [self.name copyWithZone:zone];
        copy.count = self.count;
        copy.type = self.type;
    }
    
    return copy;
}


@end
