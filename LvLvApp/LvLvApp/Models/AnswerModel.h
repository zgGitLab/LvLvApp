//
//  AnswerModel.h
//  LvLvApp
//
//  Created by hope on 15/11/17.
//  Copyright © 2015年 gyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnswerModel : NSObject
@property (nonatomic,copy) NSString * NickName;
@property (nonatomic,copy) NSString * ImageUrl;
@property (nonatomic,assign) NSNumber * state;
@property (nonatomic,copy) NSString * AnswerDT;
@property (nonatomic,copy) NSString * Title;
@property (nonatomic,copy) NSString * AnswerContent;
@property (nonatomic,copy) NSString * CommentCount;
@property (nonatomic,copy) NSString * ResponseID;
@property (nonatomic,copy) NSString * IsCollect;
@property (nonatomic,strong) NSArray * ImageList;

- (id)initWithDictionary:(NSDictionary *)dict;

//        "Code":200,
//        "Message":"获取数据成功.",
//        "Data":[
//                {
//                    "NickName":"会飞的鱼儿",
//                    "ImageUrl":"upload/users/no.jpg",
//                    "state":0,
//                    "AnswerDT":"2015-11-16 10:20:41",
//                    "Title":"测试",
//                    "AnswerContent":"aaaaa",
//                    "CommentCount":0,
//                    "ResponseID":null,
//                    "IsCollect":"0",
//                    "LogoUrl":""
//                }
//                ]
@end
